﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;

using manageAnalysis;

namespace manage5
{
    class manage5
    {
        public struct resultStruct
        {
            public int SetNumber;
            public string ImageName;
            public int ImageNumber;
            public string AType;
            public int ProgramCounter;
        };
        static void Main(string[] args)
        {
            string xmlDoc = System.IO.File.ReadAllText(@args[0]);
            string outputBaseName = args[1];
            string outputStub = args[2];
            string errMsg;
            XDocument ImageDoc = new XDocument();
            try
            {
                ImageDoc = XDocument.Parse(xmlDoc);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.ToString());
                Environment.Exit(0);
            }
            ImageData iData = new ImageData(outputBaseName);
            if (!iData.CreateEmpty("M1015K04", "4 Channel BR", "Test Project", @"c:\Instrument", @"c:\Instrument\Template",
                true, false, 4, 2, 3, 4, "1/3/2021,16:30:40", out errMsg, outputBaseName))
            {
                Console.WriteLine(String.Format("Problem loading creating empty Xdoc: {0}", errMsg));
                Environment.Exit(0);
            }
            string device = "M1015K04";
            List<string> analysisList = new List<string>();
            List<string> programList = new List<string>();
            List<string> scriptList = new List<string>();

            iData.GetDocString(out string xmlDoc1);
            string outName = outputStub + "_0.xml";
            System.IO.File.WriteAllText(outName, xmlDoc1);

            List<XElement> imList = ImageDoc.Descendants("Image").ToList();
            List<int> list1 = new List<int>();
            List<int> list2 = new List<int>();
            List<int> list3 = new List<int>();
            List<resultStruct> results1 = new List<resultStruct>();
            List<resultStruct> results2 = new List<resultStruct>();
            resultStruct oneResult = new resultStruct();
            int nn = 0;
            while (nn < imList.Count)
            {
                list1.Add(nn);
                nn++;
                if (nn >= imList.Count)
                    break;
                list2.Add(nn);
                nn++;
                if (nn >= imList.Count)
                    break;
                list3.Add(nn);
                nn++;
            }
            Console.WriteLine("\nAdding first third of images");
            for (int n = 0; n < list1.Count; n++)
            {
                bool error = false;
                string iName = imList[list1[n]].Attribute("Name").Value;
                string iTime = imList[list1[n]].Attribute("Time").Value;
                string iRotor = imList[list1[n]].Attribute("RotorPosition").Value;
                string iChip = imList[list1[n]].Attribute("ChipPosition").Value;
                string iSection = imList[list1[n]].Attribute("Section").Value;
                string iDye = imList[list1[n]].Attribute("Dye").Value;
                string iSet = imList[list1[n]].Attribute("SetNumber").Value;
                string iType = imList[list1[n]].Attribute("Type").Value;
                bool isBefore = false;
                if (iType.IndexOf("Before", StringComparison.CurrentCultureIgnoreCase) >= 0)
                    isBefore = true;
                int nSet;
                if (!Int32.TryParse(iSet, out nSet))
                    error = true;
                int nSection;
                if (!Int32.TryParse(iSection, out nSection))
                    error = true;
                //int nChip = 0;
                if (iChip.IndexOf("Right", StringComparison.CurrentCultureIgnoreCase) >= 0)
                    iChip = "Right";
                else
                    iChip = "Left";
                int nRotor;
                if (!Int32.TryParse(iRotor, out nRotor))
                    error = true;
                if (!error)
                    iData.AddImage(iName, isBefore, nSet, iDye, nSection, iChip, nRotor, iTime, out errMsg);
            }
            iData.GetDocString(out string xmlDoc2);
            outName = outputStub + "_1.xml";
            System.IO.File.WriteAllText(outName, xmlDoc2);

            if (!iData.CheckXDocument(out errMsg, out int numbBefore, out int numbAfter))
            {
                Console.WriteLine("Problem with settings in XDoc: {0}", errMsg);
                Environment.Exit(0);
            }
            //Console.WriteLine(String.Format("<1> numb images before/after {0}/{1}", numbBefore, numbAfter));
            analysisList.Add(String.Format("<1> numb images before/after {0}/{1}", numbBefore, numbAfter));

            List<XElement> iList = iData.GetImages();
            Console.WriteLine(String.Format("Total numb images: {0}", iList.Count));

            //Console.WriteLine("\n<1> Adding program for first third of images");
            analysisList.Add("\n<1> Adding program for first third of images");
            int res = iData.AddSizingAnalysis("Before", "DyeA", 0, out errMsg);
            //Console.WriteLine(String.Format("<1> {0} setups added (Before/Sizing/DyeA)", res));
            analysisList.Add(String.Format("<1> {0} setups added (Before/Sizing/DyeA)", res));
            if (res < 1 && errMsg.Length > 0)
                Console.WriteLine(String.Format("     {0}", errMsg));
            res = iData.AddSizingAnalysis("After", "DyeA", 0, out errMsg);
            //Console.WriteLine(String.Format("<1> {0} setups added (After/Sizing/DyeA)", res));
            analysisList.Add(String.Format("<1> {0} setups added (After/Sizing/DyeA)", res));
            if (res < 1 && errMsg.Length > 0)
                Console.WriteLine(String.Format("     {0}", errMsg));

            iData.GetDocString(out xmlDoc2);
            outName = outputStub + "_1A.xml";
            System.IO.File.WriteAllText(outName, xmlDoc2);

            res = iData.AddMaskingAnalysis("Before", "DyeA", "DyeB", 0, out errMsg);
            Console.WriteLine(String.Format("<1> {0} setups added (Before/Masking/DyeA/DyeB)", res));
            analysisList.Add(String.Format("<1> {0} setups added (Before/Masking/DyeA/DyeB)", res));
            if (res < 1 && errMsg.Length > 0)
                Console.WriteLine(String.Format("     {0}", errMsg));
            res = iData.AddMaskingAnalysis("After", "DyeA", "DyeB", 0, out errMsg);
            Console.WriteLine(String.Format("<1> {0} setups added (After/Masking/DyeA/DyeB)", res));
            analysisList.Add(String.Format("<1> {0} setups added (After/Masking/DyeA/DyeB)", res));
            if (res < 1 && errMsg.Length > 0)
                Console.WriteLine(String.Format("     {0}", errMsg));

            iData.GetDocString(out xmlDoc2);
            outName = outputStub + "_1B.xml";
            System.IO.File.WriteAllText(outName, xmlDoc2);

            iData.GetAnalysisSetups("Sizing", 0, false, false, out List<Tuple<string, string>> allSizing, out errMsg);
            Console.WriteLine("first third sizing setups");
            analysisList.Add("first third sizing setups");
            for (int m = 0; m < allSizing.Count; m++)
            {
                //Console.WriteLine(String.Format("{0}: {1}", m, allSizing[m].Item1));
                analysisList.Add(String.Format("{0}: {1}", m, allSizing[m].Item1));
            }
            iData.GetAnalysisSetups("Masking", 0, false, false, out List<Tuple<string, string>> allMasking, out errMsg);
            Console.WriteLine("first third masking setups");
            analysisList.Add("first third masking setups");
            for (int m = 0; m < allMasking.Count; m++)
            {
                //Console.WriteLine(String.Format("{0}: {1}", m, allMasking[m].Item1));
                analysisList.Add(String.Format("{0}: {1}", m, allMasking[m].Item1));
            }

            Console.WriteLine("\n\n<2> Adding second third of images");
            analysisList.Add("\n\n<2> Adding second third of images");
            for (int n = 0; n < list2.Count; n++)
            {
                bool error = false;
                string iName = imList[list2[n]].Attribute("Name").Value;
                string iTime = imList[list2[n]].Attribute("Time").Value;
                string iRotor = imList[list2[n]].Attribute("RotorPosition").Value;
                string iChip = imList[list2[n]].Attribute("ChipPosition").Value;
                string iSection = imList[list2[n]].Attribute("Section").Value;
                string iDye = imList[list2[n]].Attribute("Dye").Value;
                string iSet = imList[list2[n]].Attribute("SetNumber").Value;
                string iType = imList[list2[n]].Attribute("Type").Value;
                bool isBefore = false;
                if (iType.IndexOf("Before") >= 0)
                    isBefore = true;
                int nSet;
                if (!Int32.TryParse(iSet, out nSet))
                    error = true;
                int nSection;
                if (!Int32.TryParse(iSection, out nSection))
                    error = true;
                if (iChip.IndexOf("Right", StringComparison.CurrentCultureIgnoreCase) >= 0)
                    iChip = "Right";
                else
                    iChip = "Left";
                //int nChip = 0;
                //if (iChip.IndexOf("Right") >= 0)
                //    nChip = 1;
                int nRotor;
                if (!Int32.TryParse(iRotor, out nRotor))
                    error = true;
                if (!error)
                    iData.AddImage(iName, isBefore, nSet, iDye, nSection, iChip, nRotor, iTime, out errMsg);
            }
            iData.GetDocString(out xmlDoc2);
            outName = outputStub + "_2.xml";
            System.IO.File.WriteAllText(outName, xmlDoc2);

            if (!iData.CheckXDocument(out errMsg, out numbBefore, out numbAfter))
            {
                Console.WriteLine("Problem with settings in XDoc: {0}", errMsg);
                Environment.Exit(0);
            }
            Console.WriteLine(String.Format("<2> numb images before/after {0}/{1}", numbBefore, numbAfter));
            analysisList.Add(String.Format("<2> numb images before/after {0}/{1}", numbBefore, numbAfter));
            iList = iData.GetImages();
            Console.WriteLine(String.Format("<2> Total numb images: {0}", iList.Count));
            analysisList.Add(String.Format("<2> Total numb images: {0}", iList.Count));
            Console.WriteLine("\n<2> Adding programs for second third of images");
            analysisList.Add("\n < 2 > Adding programs for second third of images");

            res = iData.AddSizingAnalysis("Before", "DyeA", 0, out errMsg);
            Console.WriteLine(String.Format("<2> {0} setups added (Before/Sizing/DyeA)", res));
            analysisList.Add(String.Format("<2> {0} setups added (Before/Sizing/DyeA)", res));
            if (res < 1 && errMsg.Length > 0)
                Console.WriteLine(String.Format("     {0}", errMsg));
            res = iData.AddSizingAnalysis("After", "DyeA", 0, out errMsg);
            Console.WriteLine(String.Format("<2> {0} setups added (After/Sizing/DyeA)", res));
            analysisList.Add(String.Format("<2> {0} setups added (After/Sizing/DyeA)", res));
            if (res < 1 && errMsg.Length > 0)
                Console.WriteLine(String.Format("     {0}", errMsg));

            iData.GetDocString(out xmlDoc2);
            outName = outputStub + "_2A.xml";
            System.IO.File.WriteAllText(outName, xmlDoc2);

            res = iData.AddMaskingAnalysis("Before", "DyeA", "DyeB", 0, out errMsg);
            Console.WriteLine(String.Format("<2> {0} setups added (Before/Masking/DyeA/DyeB)", res));
            analysisList.Add(String.Format("<2> {0} setups added (Before/Masking/DyeA/DyeB)", res));
            if (res < 1 && errMsg.Length > 0)
                Console.WriteLine(String.Format("     {0}", errMsg));
            res = iData.AddMaskingAnalysis("After", "DyeA", "DyeB", 0, out errMsg);
            Console.WriteLine(String.Format("<2> {0} setups added (After/Masking/DyeA/DyeB)", res));
            analysisList.Add(String.Format("<2> {0} setups added (After/Masking/DyeA/DyeB)", res));
            if (res < 1 && errMsg.Length > 0)
                Console.WriteLine(String.Format("     {0}", errMsg));

            iData.GetDocString(out xmlDoc2);
            outName = outputStub + "_2B.xml";
            System.IO.File.WriteAllText(outName, xmlDoc2);

            iData.GetAnalysisSetups("Sizing", 0, false, false, out allSizing, out errMsg);
            Console.WriteLine("second third sizing setups");
            analysisList.Add("second third sizing setups");
            for (int m = 0; m < allSizing.Count; m++)
            {
                //Console.WriteLine(String.Format("{0}: {1}", m, allSizing[m].Item1));
                analysisList.Add(String.Format("{0}: {1}", m, allSizing[m].Item1));
            }
            iData.GetAnalysisSetups("Masking", 0, false, false, out allMasking, out errMsg);
            Console.WriteLine("second third masking setups");
            analysisList.Add("second third masking setups");
            for (int m = 0; m < allMasking.Count; m++)
            {
                //Console.WriteLine(String.Format("{0}: {1}", m, allMasking[m].Item1));
                analysisList.Add(String.Format("{0}: {1}", m, allMasking[m].Item2));
            }
            Console.WriteLine("\n\n<3> Adding last third of images");
            analysisList.Add("\n\n<3> Adding last third of images");
            for (int n = 0; n < list3.Count; n++)
            {
                bool error = false;
                string iName = imList[list3[n]].Attribute("Name").Value;
                string iTime = imList[list3[n]].Attribute("Time").Value;
                string iRotor = imList[list3[n]].Attribute("RotorPosition").Value;
                string iChip = imList[list3[n]].Attribute("ChipPosition").Value;
                string iSection = imList[list3[n]].Attribute("Section").Value;
                string iDye = imList[list3[n]].Attribute("Dye").Value;
                string iSet = imList[list3[n]].Attribute("SetNumber").Value;
                string iType = imList[list3[n]].Attribute("Type").Value;
                bool isBefore = false;
                if (iType.IndexOf("Before") >= 0)
                    isBefore = true;
                int nSet;
                if (!Int32.TryParse(iSet, out nSet))
                    error = true;
                int nSection;
                if (!Int32.TryParse(iSection, out nSection))
                    error = true;
                if (iChip.IndexOf("Right", StringComparison.CurrentCultureIgnoreCase) >= 0)
                    iChip = "Right";
                else
                    iChip = "Left";
                //int nChip = 0;
                //if (iChip.IndexOf("Right") >= 0)
                //    nChip = 1;
                int nRotor;
                if (!Int32.TryParse(iRotor, out nRotor))
                    error = true;
                if (!error)
                    iData.AddImage(iName, isBefore, nSet, iDye, nSection, iChip, nRotor, iTime, out errMsg);
            }
            iData.GetDocString(out xmlDoc2);
            outName = outputStub + "_3.xml";
            System.IO.File.WriteAllText(outName, xmlDoc2);

            if (!iData.CheckXDocument(out errMsg, out numbBefore, out numbAfter))
            {
                Console.WriteLine("Problem with settings in XDoc: {0}", errMsg);
                Environment.Exit(0);
            }
            Console.WriteLine(String.Format("<3> numb images before/after {0}/{1}", numbBefore, numbAfter));
            analysisList.Add(String.Format("<3> numb images before/after {0}/{1}", numbBefore, numbAfter));

            iList = iData.GetImages();
            Console.WriteLine(String.Format("<3> Total numb images: {0}", iList.Count));
            analysisList.Add(String.Format("<3> Total numb images: {0}", iList.Count));
            Console.WriteLine("\n<3> Adding programs for last third of images");
            res = iData.AddSizingAnalysis("Before", "DyeA", 0, out errMsg);
            Console.WriteLine(String.Format("<3> {0} setups added (Before/Sizing/DyeA)", res));
            analysisList.Add(String.Format("<3> {0} setups added (Before/Sizing/DyeA)", res));
            if (res < 1 && errMsg.Length > 0)
                Console.WriteLine(String.Format("     {0}", errMsg));
            res = iData.AddSizingAnalysis("After", "DyeA", 0, out errMsg);
            Console.WriteLine(String.Format("<3> {0} setups added (After/Sizing/DyeA)", res));
            analysisList.Add(String.Format("<3> {0} setups added (After/Sizing/DyeA)", res));
            if (res < 1 && errMsg.Length > 0)
                Console.WriteLine(String.Format("     {0}", errMsg));

            iData.GetDocString(out xmlDoc2);
            outName = outputStub + "_3A.xml";
            System.IO.File.WriteAllText(outName, xmlDoc2);

            res = iData.AddMaskingAnalysis("Before", "DyeA", "DyeB", 0, out errMsg);
            Console.WriteLine(String.Format("<3> {0} setups added (Before/Masking/DyeA/DyeB)", res));
            analysisList.Add(String.Format("<3> {0} setups added (Before/Masking/DyeA/DyeB)", res));
            if (res < 1 && errMsg.Length > 0)
                Console.WriteLine(String.Format("     {0}", errMsg));
            res = iData.AddMaskingAnalysis("After", "DyeA", "DyeB", 0, out errMsg);
            Console.WriteLine(String.Format("<3> {0} setups added (After/Masking/DyeA/DyeB)", res));
            analysisList.Add(String.Format("<3> {0} setups added (After/Masking/DyeA/DyeB)", res));
            if (res < 1 && errMsg.Length > 0)
                Console.WriteLine(String.Format("     {0}", errMsg));

            iData.GetDocString(out xmlDoc2);
            outName = outputStub + "_3B.xml";
            System.IO.File.WriteAllText(outName, xmlDoc2);

            iData.GetAnalysisSetups("Sizing", 0, false, false, out allSizing, out errMsg);
            Console.WriteLine("last third sizing setups");
            analysisList.Add("last third sizing setups");
            programList.Add("===== Sizing analysis");
            analysisList.Add("===== Sizing analysis");
            for (int m = 0; m < allSizing.Count; m++)
            {
                //Console.WriteLine(String.Format("{0}: {1}", m, allSizing[m].Item1));
                analysisList.Add(String.Format("{0}: {1}", m, allSizing[m].Item1));
                XElement prog = XElement.Parse(allSizing[m].Item1);
                string pPerformed = prog.Attribute("Performed").Value;
                string pSuccess = prog.Attribute("Success").Value;
                string pProgramC = prog.Attribute("ProgramCounter").Value;
                string pExtraOutput = prog.Attribute("ExtraOutput").Value;
                string pUseMedian = prog.Attribute("UseMedianFilter").Value;
                string pTransposeIm = prog.Attribute("TransposeImage").Value;
                string pBaseName = prog.Attribute("BaseName").Value;
                string pSubImage = prog.Attribute("SubImage").Value;
                string pFilterL = prog.Attribute("FilterLabel").Value;
                string pPrefix = prog.Attribute("TemplatePrefix").Value;
                string pImageName = prog.Attribute("ImageFileName").Value;
                string pTDirectory = prog.Attribute("TemplateDirectory").Value;
                string pWDirectory = prog.Attribute("WorkingDirectory").Value;
                bool bPerformed = false;
                bool bSuccess = true;
                int iProgC = -1;
                int iExtraOutput = -1;
                bool bUseMedian = false;
                bool bTransposeIm = true;
                int iSubImage = -1;
                int imageNumb = -1;
                bool.TryParse(pPerformed, out bPerformed);
                bool.TryParse(pSuccess, out bSuccess);
                Int32.TryParse(pProgramC, out iProgC);
                Int32.TryParse(pExtraOutput, out iExtraOutput);
                bool.TryParse(pUseMedian, out bUseMedian);
                bool.TryParse(pTransposeIm, out bTransposeIm);
                Int32.TryParse(pSubImage, out iSubImage);
                Int32.TryParse(allSizing[m].Item2, out imageNumb);
                oneResult.SetNumber = 0;
                oneResult.ImageName = pImageName;
                oneResult.ImageNumber = imageNumb;
                oneResult.AType = "Sizing";
                oneResult.ProgramCounter = iProgC;
                programList.Add(String.Format("sdpcrSizing(Err, {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9})   Performed({10}), Success({11})",
                    pWDirectory, pTDirectory, pImageName, pPrefix, pFilterL, pSubImage, pBaseName,
                    pTransposeIm, pUseMedian, pExtraOutput, pPerformed, pSuccess));
                results1.Add(oneResult);
            }
            iData.GetSizingAnalysisStrings(true, 0, true, false, false,
                out List<string> setupStrings, out errMsg);
            scriptList.Add("===== sizing strings =====");
            for (int n = 0; n < setupStrings.Count; n++)
                scriptList.Add(setupStrings[n]);
            iData.GetSizingAnalysisStrings(false, 0, true, false, false,
                out setupStrings, out errMsg);
            for (int n = 0; n < setupStrings.Count; n++)
                scriptList.Add(setupStrings[n]);

            iData.GetAnalysisSetups("Masking", 0, false, false, out allMasking, out errMsg);
            Console.WriteLine("last third masking setups");
            analysisList.Add("last third masking setups");
            for (int m = 0; m < allMasking.Count; m++)
            {
                //Console.WriteLine(String.Format("{0}: {1}", m, allMasking[m].Item1));
                analysisList.Add(String.Format("{0}: {1}", m, allMasking[m].Item1));
            }


            for (int n = 0; n < results1.Count / 2; n++)
            {
                if (!iData.UpdateAnalysisResult(results1[n].ImageName, results1[n].ImageNumber.ToString(),
                    results1[n].AType, results1[n].ProgramCounter.ToString(),
                    true, true, out errMsg))
                {
                    Console.WriteLine(String.Format("Could not update analysis {0}: {1}", n, errMsg));
                }
                if (!iData.UpdateAnalysisSelection(results1[n].ImageName, results1[n].ImageNumber.ToString(),
                    results1[n].AType, results1[n].ProgramCounter.ToString(), true, out errMsg))
                {
                    Console.WriteLine(String.Format("Could not update selection {0}: {1}", n, errMsg));
                }
            }

            iData.GetDocString(out xmlDoc2);
            outName = outputStub + "_4.xml";
            System.IO.File.WriteAllText(outName, xmlDoc2);

            programList.Add("===== Masking analysis");
            analysisList.Add("===== Masking analysis");
            for (int m = 0; m < allMasking.Count; m++)
            {
                analysisList.Add(String.Format("{0}: {1}", m, allMasking[m].Item1));
                XElement prog = XElement.Parse(allMasking[m].Item1);
                string pPerformed = prog.Attribute("Performed").Value;
                string pSuccess = prog.Attribute("Success").Value;
                string pProgramC = prog.Attribute("ProgramCounter").Value;
                string pExtraOutput = prog.Attribute("ExtraOutput").Value;
                string pUseMedian = prog.Attribute("UseMedianFilter").Value;
                string pTransposeIm = prog.Attribute("TransposeImage").Value;
                string pBaseName = prog.Attribute("BaseName").Value;
                string pSubImage = prog.Attribute("SubImage").Value;
                string pFilterL = prog.Attribute("FilterLabel").Value;
                string pPrefix = prog.Attribute("TemplatePrefix").Value;
                string pImageName = prog.Attribute("ImageFileName").Value;
                string pTDirectory = prog.Attribute("TemplateDirectory").Value;
                string pWDirectory = prog.Attribute("WorkingDirectory").Value;
                string pDilation = prog.Attribute("Dilation").Value;
                string pColorShiftX = prog.Attribute("ColorShiftX").Value;
                string pColorShiftY = prog.Attribute("ColorShiftY").Value;
                string pSizeImageNumb = prog.Attribute("SizeImageNumber").Value;
                string pSizeImageName = prog.Attribute("SizeImageName").Value;
                bool bPerformed = false;
                bool bSuccess = true;
                int iProgC = -1;
                int iExtraOutput = -1;
                bool bUseMedian = false;
                bool bTransposeIm = true;
                int iSubImage = -1;
                int imageNumb = -1;
                int iDilation = -1;
                int iColorShiftX = -1;
                int iColorShiftY = -1;
                int iSizeImageNumb = -1;
                bool.TryParse(pPerformed, out bPerformed);
                bool.TryParse(pSuccess, out bSuccess);
                Int32.TryParse(pProgramC, out iProgC);
                Int32.TryParse(pExtraOutput, out iExtraOutput);
                bool.TryParse(pUseMedian, out bUseMedian);
                bool.TryParse(pTransposeIm, out bTransposeIm);
                Int32.TryParse(pSubImage, out iSubImage);
                Int32.TryParse(allMasking[m].Item2, out imageNumb);
                Int32.TryParse(pDilation, out iDilation);
                Int32.TryParse(pColorShiftX, out iColorShiftX);
                Int32.TryParse(pColorShiftY, out iColorShiftY);
                Int32.TryParse(pSizeImageNumb, out iSizeImageNumb);

                if (!iData.GetOutputBaseName(pSizeImageName, pSizeImageNumb, "Sizing", out string outBaseName, out errMsg))
                {
                    Console.WriteLine(String.Format("Can not get sizing basename for masking analysis {0}: {1}", m, errMsg));
                }
                else
                {
                    oneResult.SetNumber = 0;
                    oneResult.ImageName = pImageName;
                    oneResult.ImageNumber = imageNumb;
                    oneResult.AType = "Masking";
                    oneResult.ProgramCounter = iProgC;
                    programList.Add(String.Format("sdpcrMasking(Err, {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13})   Performed({14}), Success({15})",
                    pWDirectory, pTDirectory, pImageName, pPrefix, pFilterL, pSubImage, outBaseName, pBaseName, iColorShiftX, iColorShiftY,
                    iDilation, pTransposeIm, pUseMedian, pExtraOutput, pPerformed, pSuccess));
                    results2.Add(oneResult);
                }
            }

            outName = outputStub + "_99.txt";
            System.IO.File.WriteAllLines(outName, analysisList);

            outName = outputStub + "_98.txt";
            System.IO.File.WriteAllLines(outName, programList);

            for (int n = results1.Count / 2; n < results1.Count; n++)
            {
                if (!iData.UpdateAnalysisResult(results1[n].ImageName, results1[n].ImageNumber.ToString(),
                    results1[n].AType, results1[n].ProgramCounter.ToString(),
                    true, true, out errMsg))
                {
                    Console.WriteLine(String.Format("Could not update analysis {0}: {1}", n, errMsg));
                }
                if (!iData.UpdateAnalysisSelection(results1[n].ImageName, results1[n].ImageNumber.ToString(),
                    results1[n].AType, results1[n].ProgramCounter.ToString(), true, out errMsg))
                {
                    Console.WriteLine(String.Format("Could not update selection {0}: {1}", n, errMsg));
                }
            }

            programList.Add("===== Masking analysis with all sizing done");
            analysisList.Add("===== Masking analysis with all sizing done");
            for (int m = 0; m < allMasking.Count; m++)
            {
                analysisList.Add(String.Format("{0}: {1}", m, allMasking[m].Item1));
                XElement prog = XElement.Parse(allMasking[m].Item1);
                string pPerformed = prog.Attribute("Performed").Value;
                string pSuccess = prog.Attribute("Success").Value;
                string pProgramC = prog.Attribute("ProgramCounter").Value;
                string pExtraOutput = prog.Attribute("ExtraOutput").Value;
                string pUseMedian = prog.Attribute("UseMedianFilter").Value;
                string pTransposeIm = prog.Attribute("TransposeImage").Value;
                string pBaseName = prog.Attribute("BaseName").Value;
                string pSubImage = prog.Attribute("SubImage").Value;
                string pFilterL = prog.Attribute("FilterLabel").Value;
                string pPrefix = prog.Attribute("TemplatePrefix").Value;
                string pImageName = prog.Attribute("ImageFileName").Value;
                string pTDirectory = prog.Attribute("TemplateDirectory").Value;
                string pWDirectory = prog.Attribute("WorkingDirectory").Value;
                string pDilation = prog.Attribute("Dilation").Value;
                string pColorShiftX = prog.Attribute("ColorShiftX").Value;
                string pColorShiftY = prog.Attribute("ColorShiftY").Value;
                string pSizeImageNumb = prog.Attribute("SizeImageNumber").Value;
                string pSizeImageName = prog.Attribute("SizeImageName").Value;
                bool bPerformed = false;
                bool bSuccess = true;
                int iProgC = -1;
                int iExtraOutput = -1;
                bool bUseMedian = false;
                bool bTransposeIm = true;
                int iSubImage = -1;
                int imageNumb = -1;
                int iDilation = -1;
                int iColorShiftX = -1;
                int iColorShiftY = -1;
                int iSizeImageNumb = -1;
                bool.TryParse(pPerformed, out bPerformed);
                bool.TryParse(pSuccess, out bSuccess);
                Int32.TryParse(pProgramC, out iProgC);
                Int32.TryParse(pExtraOutput, out iExtraOutput);
                bool.TryParse(pUseMedian, out bUseMedian);
                bool.TryParse(pTransposeIm, out bTransposeIm);
                Int32.TryParse(pSubImage, out iSubImage);
                Int32.TryParse(allMasking[m].Item2, out imageNumb);
                Int32.TryParse(pDilation, out iDilation);
                Int32.TryParse(pColorShiftX, out iColorShiftX);
                Int32.TryParse(pColorShiftY, out iColorShiftY);
                Int32.TryParse(pSizeImageNumb, out iSizeImageNumb);

                if (!iData.GetOutputBaseName(pSizeImageName, pSizeImageNumb, "Sizing", out string outBaseName, out errMsg))
                {
                    Console.WriteLine(String.Format("Can not get sizing basename for masking analysis {0}: {1}", m, errMsg));
                }
                else
                {
                    oneResult.SetNumber = 0;
                    oneResult.ImageName = pImageName;
                    oneResult.ImageNumber = imageNumb;
                    oneResult.AType = "Masking";
                    oneResult.ProgramCounter = iProgC;
                    programList.Add(String.Format("sdpcrMasking(Err, {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13})   Performed({14}), Success({15})",
                    pWDirectory, pTDirectory, pImageName, pPrefix, pFilterL, pSubImage, outBaseName, pBaseName, iColorShiftX, iColorShiftY,
                    iDilation, pTransposeIm, pUseMedian, pExtraOutput, pPerformed, pSuccess));
                }
            }
            outName = outputStub + "_97.txt";
            System.IO.File.WriteAllLines(outName, analysisList);

            outName = outputStub + "_96.txt";
            System.IO.File.WriteAllLines(outName, programList);

            iData.GetDocString(out xmlDoc2);
            outName = outputStub + "_4A.xml";
            System.IO.File.WriteAllText(outName, xmlDoc2);

            iData.GetMaskingAnalysisStrings(true, 0, true, false, false,
                out List<string> mSetupStrings, out errMsg);
            if (errMsg.Length > 0)
                Console.WriteLine(String.Format("GetMaskingAnalysisStrings: {0}", errMsg));
            scriptList.Add("===== masking strings =====");
            for (int n = 0; n < mSetupStrings.Count; n++)
                scriptList.Add(mSetupStrings[n]);
            iData.GetMaskingAnalysisStrings(false, 0, true, false, false,
                out mSetupStrings, out errMsg);
            for (int n = 0; n < mSetupStrings.Count; n++)
                scriptList.Add(mSetupStrings[n]);

            for (int n = 0; n < results2.Count / 2; n++)
            {
                //Console.WriteLine(String.Format("results2[{0}]: {1}/{2}/{3}/{4}", n, results2[n].ImageName, results2[n].ImageNumber,
                //   results2[n].AType, results2[n].ProgramCounter));
                if (!iData.UpdateAnalysisResult(results2[n].ImageName, results2[n].ImageNumber.ToString(),
                    results2[n].AType, results2[n].ProgramCounter.ToString(),
                    true, true, out errMsg))
                {
                    Console.WriteLine(String.Format("Could not update analysis {0}: {1}", n, errMsg));
                }
                if (!iData.UpdateAnalysisSelection(results2[n].ImageName, results2[n].ImageNumber.ToString(),
                    results2[n].AType, results2[n].ProgramCounter.ToString(), true, out errMsg))
                {
                    Console.WriteLine(String.Format("Could not update selection {0}: {1}", n, errMsg));
                }
            }
            iData.GetDocString(out xmlDoc2);
            outName = outputStub + "_4B.xml";
            System.IO.File.WriteAllText(outName, xmlDoc2);

            for (int n = results2.Count / 2; n < results2.Count; n++)
            {
                //Console.WriteLine(String.Format("results2[{0}]: {1}/{2}/{3}/{4}", n, results2[n].ImageName, results2[n].ImageNumber,
                //    results2[n].AType, results2[n].ProgramCounter));
                if (!iData.UpdateAnalysisResult(results2[n].ImageName, results2[n].ImageNumber.ToString(),
                    results2[n].AType, results2[n].ProgramCounter.ToString(),
                    true, true, out errMsg))
                {
                    Console.WriteLine(String.Format("Could not update analysis {0}: {1}", n, errMsg));
                }
                if (!iData.UpdateAnalysisSelection(results2[n].ImageName, results2[n].ImageNumber.ToString(),
                    results2[n].AType, results2[n].ProgramCounter.ToString(), true, out errMsg))
                {
                    Console.WriteLine(String.Format("Could not update selection {0}: {1}", n, errMsg));
                }
            }

            iData.GetDocString(out xmlDoc2);
            outName = outputStub + "_4C.xml";
            System.IO.File.WriteAllText(outName, xmlDoc2);
            string quantDye = "DyeB";
            //List<int> aSetNumbs = new List<int>();
            //aSetNumbs.Add(0);
            int[] aSetNumbs = new int[1];
            aSetNumbs[0] = 0;
            if (iData.AddQuantification(quantDye, String.Format("{0}_{1}_Quant.txt", device, quantDye), 0, aSetNumbs, "Masking", "Masking", out errMsg) < 1
                && errMsg.Length > 0)
            {
                Console.WriteLine(String.Format("AddQuantification error: {0}", errMsg));
            }
            iData.GetDocString(out xmlDoc2);
            outName = outputStub + "_5.xml";
            System.IO.File.WriteAllText(outName, xmlDoc2);

            int numbQuants = iData.GetQuantifications(true, false, false, out List<string> qSetups, out List<string> qInfo, out errMsg);
            if ( numbQuants < 1 && errMsg.Length > 0)
            {
                Console.WriteLine(String.Format("AddQuantification error: {0}", errMsg));
            }
            Console.WriteLine(String.Format("Number of quant setups {0}", numbQuants));
            analysisList.Add("quantitation setups");

            iData.GetDocString(out xmlDoc2);
            outName = outputStub + "_5B.xml";
            System.IO.File.WriteAllText(outName, xmlDoc2);

            iData.GetQuantAnalysisStrings(0, true, false, false,
                out List<string> qSetupStrings, out errMsg);
            scriptList.Add("===== quantify strings =====");
            analysisList.Add("===== quantify strings =====");
            for (int n = 0; n < qSetupStrings.Count; n++)
            {
                scriptList.Add(qSetupStrings[n]);
                analysisList.Add(qSetupStrings[n]);
            }

            for (int n = 0; n < qSetups.Count; n++)
            {
                XElement prog = XElement.Parse(qSetups[n]);
                string pProgramC = prog.Attribute("ProgramCounter").Value;
                int iProgC;
                if ( Int32.TryParse(pProgramC, out iProgC) && iProgC > 0 )
                {
                    iData.UpdateQuantificationRunStatus(iProgC, true, true);
                }
            }

            iData.GetDocString(out xmlDoc2);
            outName = outputStub + "_5C.xml";
            System.IO.File.WriteAllText(outName, xmlDoc2);

            //for ( int n = 0; n < qSetups.Count; n++)
            //{
            //    XElement prog = XElement.Parse(qSetups[n]);
            //    string pPerformed = prog.Attribute("Performed").Value;
            //    string pSuccess = prog.Attribute("Success").Value;
            //    string pProgramC = prog.Attribute("ProgramCounter").Value;
            //    string pBaseName = prog.Attribute("BaseName").Value;
            //    string pRotor = prog.Attribute("RotorPosition").Value;
            //    string pChip = prog.Attribute("ChipPosition").Value;
            //    string pSubImage = prog.Attribute("SubImage").Value;
            //    string pPrefix = prog.Attribute("TemplatePrefix").Value;
            //    string pTDirectory = prog.Attribute("TemplateDirectory").Value;
            //    string pWDirectory = prog.Attribute("WorkingDirectory").Value;
            //    string pBeforeImageNumber = prog.Attribute("BeforeImageNumber").Value;
            //    string pBeforeBaseName = prog.Attribute("BeforeBaseName").Value;
            //    string pBeforeSetNumber = prog.Attribute("BeforeSetNumber").Value;
            //    string pParameterFile = prog.Attribute("ParameterFile").Value;
                
            //    bool bPerformed = false;
            //    bool bSuccess = true;
            //    int iProgC = -1;
            //    int iRotor = -1;
            //    int iChip = -1;
            //    int iSubImage = -1;
            //    int iBeforeImageNumb = -1;
            //    int iBeforeSetNumber = -1;
            //    bool.TryParse(pPerformed, out bPerformed);
            //    bool.TryParse(pSuccess, out bSuccess);
            //    Console.WriteLine(String.Format("Adding Quants: {0} - {1}/{2}/{3}/{4}/{5}/{6}",
            //        n, pProgramC, pRotor, pChip, pSubImage, pBeforeImageNumber, pBeforeSetNumber));
            //    if (Int32.TryParse(pProgramC, out iProgC)
            //        && Int32.TryParse(pRotor, out iRotor)
            //        && Int32.TryParse(pChip, out iChip)
            //        && Int32.TryParse(pSubImage, out iSubImage)
            //        && Int32.TryParse(pBeforeImageNumber, out iBeforeImageNumb)
            //        && Int32.TryParse(pBeforeSetNumber, out iBeforeSetNumber))
            //    {
            //        List<XElement> qAfterBaseNamesList = prog.Descendants("AfterBaseNames").Descendants("AfterBaseName").ToList();
            //        Console.WriteLine(String.Format("     qAfterBaseNamesList.Count {0}", qAfterBaseNamesList.Count));
            //        if (qAfterBaseNamesList.Count > 0)
            //        {
            //            List<string> afterBaseNameArgs = new List<string>();
            //            for (int m = 0; m < qAfterBaseNamesList.Count; m++)
            //            {
            //                string aBase = qAfterBaseNamesList[m].Attribute("Name").Value;
            //                string aSet = qAfterBaseNamesList[m].Attribute("Set").Value;
            //                afterBaseNameArgs.Add(String.Format(", {0}, {1}", aBase, aSet));
            //            }
            //            StringBuilder cmd = new StringBuilder();
            //            cmd.Clear();
            //            cmd.Append(String.Format("sdpcrQuant(Err, {0}, {1}, {2}, {3}, {4}, {5}, {6}",
            //                pWDirectory, pTDirectory, pParameterFile, pPrefix, pSubImage, pBaseName, pBeforeBaseName));
            //            for (int k = 0; k < afterBaseNameArgs.Count; k++)
            //            {
            //                cmd.Append(afterBaseNameArgs[k]);
            //            }
            //            cmd.Append(")");
            //            analysisList.Add(cmd.ToString());
            //            //Console.WriteLine(String.Format("{0}: {1}", n, cmd.ToString()));
            //        }
            //    }
            //    else
            //    {
            //        Console.WriteLine(String.Format("Did not add combine step {0}", n));
            //        Console.WriteLine(prog.ToString());
            //    }
            //}
            outName = outputStub + "_95.txt";
            System.IO.File.WriteAllLines(outName, analysisList);
            iData.GetDocString(out xmlDoc2);
            outName = outputStub + "_5D.xml";
            System.IO.File.WriteAllText(outName, xmlDoc2);

            //List<int> setNumbsUsed = new List<int>();
            //setNumbsUsed.Add(0);
            int[] setNumbsUsed = new int[1];
            setNumbsUsed[0] = 0;
            int combineStepsAdded = iData.AddCombineSteps("DyeB", setNumbsUsed, out errMsg);
            Console.WriteLine(String.Format("Number of Combine Steps Added: {0}", combineStepsAdded));
            if ( combineStepsAdded < 1 && errMsg.Length > 0)
            {
                Console.WriteLine(String.Format("No combine steps added: {0}", errMsg));
            }

            iData.GetDocString(out xmlDoc2);
            outName = outputStub + "_6.xml";
            System.IO.File.WriteAllText(outName, xmlDoc2);

            //combineStepsAdded = iData.GetCombineSteps(RunStatus.All, SuccessStatus.All, out List<string> combineSetups, out errMsg);
            //if (combineStepsAdded < 1 && errMsg.Length > 0)
            //{
            //    Console.WriteLine(String.Format("No combine steps gotten: {0}", errMsg));
            //}
            //for ( int n = 0; n < combineSetups.Count; n++)
            //{
            //    Console.WriteLine(String.Format("== {0} ==", n));
            //    Console.WriteLine(combineSetups[n]);
            //}

            combineStepsAdded = iData.GetCombineAnalysisSetups(false, false,
                out List<string> cSetupStrings, out List<XElement> cSetupInfo, out errMsg);
            if (combineStepsAdded < 1 && errMsg.Length > 0)
            {
                Console.WriteLine(String.Format("No combine steps gotten: {0}", errMsg));
            }
            scriptList.Add("===== combine strings =====");
            for (int n = 0; n < cSetupStrings.Count; n++)
            {
                //scriptList.Add(cSetupInfo[n].ToString());
                scriptList.Add(cSetupStrings[n]);
            }

            outName = outputStub + "_70.txt";
            System.IO.File.WriteAllLines(outName, scriptList);

            iData.GetDocString(out xmlDoc2);
            outName = outputStub + "_7.xml";
            System.IO.File.WriteAllText(outName, xmlDoc2);

#if DEBUG
            Console.ReadLine();
#endif
        }
    }
}
