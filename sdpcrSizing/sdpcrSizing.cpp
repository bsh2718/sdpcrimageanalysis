/////////////////////////////////////////////////////////////////////////////////////////
//
// 		sdpcrSizingAnalysis - main program
//
// This program uses the opencv library. License is at the end of the file
//
// Program inputs an image of device and identifies blobs in the image which resemble the
// wells which are expected to be present.  It then aligns the image with a template 
// created from the entire device and assigns those blobs which are aligned with a well.
// The ID for each well includes its array, its section and the row and column position
// within the section. The columns are parallel to the direction of flow in the device.
// Each image is typically of only a portion of the device, and is referred to as a 
// subimage.
// 
// Usage: sdpcrSizing <Working Directory> <Template Directory> <Image Name> <Template Prefix> <Filter Label>
//       <SubImage Label> <baseName> <Rotate Image?> <Use Median Filter?> [ <Extra Output?> ]
// For boolean values <Transpose Image?> and <Use Median Filter?> use 1 for true and 0 for false

// The arguments on the command line are (string arguments cannot contain spaces)
// <Working Directory> Folder which contains the images to be analyzed.  Any output generated will be 
//      written there.
// <Template Directory> The analysis requires some files which contain information about the device which
//      is needed for the analysis.  Those files are located here.
// <Image Name> - Name of the image being input. It must be in the <Working Directory>. Thisc an be any 
//      format readable by the opencv graphics library.
// <Template Prefix> The various template files for a particular device all start with this prefix.
// <Filter Label> - Some of the template files define criteria as to what is or isn't an acceptable droplet. 
//      Since this can differ between before and after images <Filter Label> can be used to tell the
//      program to use different template files for before and after images.
// <SubImage Label> Typically a number from 1 to the number of camera positions for imaging. Combine
//          with <Prefix> to obtain SubImage file name.
// <baseName> - All of the output files start with baseName. Does not affect any output sent to std::clog.
// <Rotate Image?> - Program rotates image 90 degrees clockwise if non zero.
// <Use Median Filter?> - Program applies median filter with 3x3 kernel to input image 
// <Extra Output?> - If present and > 0, then additional output is generated for program evaluation/debuging.
//
// Template Files.  See documentation for more details about values and how to create them.
//	<Prefix>_InfoFile.txt - Device information including 
//	<Prefix>_CoordImage2.png is a 24 bit color image of the device. If a pixel is in a well, then its color
//		indicates the Array, Section, Column and Row of that well.  If a pixel is in a guard region, it
//		indicates which guard region(s) it is in.
//	<Prefix>_ArrSecImage.png is a 8 bit gray scale image of the Array and Section areas of the device.
//      If a pixel is non zero, then it is within the rectangular region which bounds all the wells of a
//      single Array and Section. 
//	<Prefix>_SubImageInfo_<SubImage Label>.txt There must be one of these for every subimage. It contains
//		information specific to the subimate such as which array/section pairs are expecte to be in the subimage.
//	<Prefix>_ParaemeterFile_<Filter Label>.txt Contains the parameters used for background correction, blob
//      sizing and the limits used to decide if a blob is a well.  <Filter Label> can be chosen to have
//      different values for before (sizing) and after (quantitation) images.  
//
// Output Files
//	<baseName>_cout.txt is a copy of the output sent to std::cout. It is generated just before the program exits
//		so its prescence could be used as an indicator that the program has finished.
//	<baseName>_000.txt is a log file for the program
//  <baseName>_112Fill.png is a color image where the background subtrated input image is overlaid with 
//		blue or green to indicate blobs which blobs were considered acceptable.  The color intensity is proportional
//		to the gray level of the background subtracted image.  Green is used for blobs which pass all the criteria
//		for a blob and yellow is used for blobs which are somewhat too large or small to be accepted.  Other blobs,
//		which are assigned but are unacceptable by a significant amount are overlayed in red;. Blobs which could not
//		be assigned are not overlaid.
//
// ===== The following four files are used by the quantitation program. =====
//	<baseName>_006BckIm8U.png is an 8 bit background subtracted image. If the input image was originally a 16 bit image, then
//		the values are scaled to fit.
//	<baseName>_110Fill.csv contains information about each of the blobs.
//		The quantitation program uses the filling.csv files from the anslysis of the before and after images
//		to determine the occupancy of the wells. 
//  <baseName>_113F-Msk.png  A 16 bit mask. The pixels of each blob have a value equal to its index number in the 
//		110Fill.csv file.
//  <baseName>_114Cont.png  A 16 bit mask.  This is similar to the 113F-Msk.png file, except that in this mask, only the
//		outline is marked. 
//
// Extra Output for diagnostic purposes is obtained by setting <Tracking Value> to be nonzero.  See trackOutputDC.hpp for details
//
//
//! [includes]
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "physicalconstants.h"
#include "statistics.hpp"
#include "blob.hpp"

#include "readinfofile.hpp"
#include "calccentroid.hpp"
#include "calcperimeter.hpp"
#include "rollingball32F.hpp"
#include "analysisParameters.hpp"
#include "guardErrors.hpp"
#include "calculatewelltemplates.hpp"
#include "calcAdaptiveThreshold.hpp"
#include "writeBlobFile.hpp"
#include "extractBlobsAdaptive.hpp"
#include "images.hpp"
#include "AngleAlignment.hpp"
#include "alignImage.hpp"
#include "blobSizing.hpp"
#include "trackOutput.hpp"
#include "median_Rotate.hpp"
#include "assignBlobs.hpp"
#include "outputSizingResults.hpp"
#include "checkSectionOne.hpp"
#include "blobSortMethods.hpp"
#include "trimDirectoryString.hpp"
#include "types.hpp"
//! [includes]

//! [namespace]
// using namespace std;
// using namespace cv;
//! [namespace]
void ShowUsage(const char* msg, const char* exe);
//int main(int Argc, char** Argv)
//int main(std::string workingDirectory, std::string templateDirectory,
//	std::string imageFileName, std::string templatePrefix, std::string filterLabel, std::string subImageLabel, std::string baseName,
//	bool rotateImage, bool useMedianFilter, unsigned int extraOutput)
int main( int Argc, char *Argv[])
{
	// std::string timeMsg;
	// timeMsg = "At Start of Program";
	// OutputTime(timeMsg);
	std::string err;
	int vSize;
	bool eightBit = true;
	TrackAnalysis track;
	track.Clear();
	std::string infoMsg;
	cv::Vec3b cValue;
	int numbRequiredArgs = 11;
	if (Argc < numbRequiredArgs)
	{
		ShowUsage("Too few arguments", Argv[0]);
		exit(0);
	}
	//! [load]
	//  defaults
	//std::string imageName("SadFish.png");
	//std::string prefix("MorbidMice");
	std::string coordImageName("PathologicalPuppies.png");
	std::string arrSecImageName("PathologicalPuppies.png");
	std::string infoFileName("HomicidalGuppies.txt");
	std::string subImageFileName("BoringBeagles.txt");
	std::string parameterFileName("BashBeetles.txt");
	//std::string outputBaseName("AntiSocialAnts");
	//std::string subImageLabel("PeculiarPenguins");
	//std::string filterLabel("KillerKitties");

	

	std::string workingDirectory;
	std::string templateDirectory;
	std::string imageFileName;
	std::string templatePrefix;
	std::string filterLabel;
	std::string subImageLabel;
	std::string baseName;
	bool rotateImage;
	bool useMedianFilter;
	unsigned int extraOutput;

	int ia = 0;
	int ie = 0;
	std::istringstream token;
	{
		// double temp;
		int itemp;
		std::string stemp;
		workingDirectory.assign(Argv[++ia]);
		templateDirectory.assign(Argv[++ia]);
		imageFileName.assign(Argv[++ia]);
		templatePrefix.assign(Argv[++ia]);
		filterLabel.assign(Argv[++ia]);
		subImageLabel.assign(Argv[++ia]);
		baseName.assign(Argv[++ia]);
		token.clear(); token.str(Argv[++ia]); token >> itemp; if (token.fail()) { ++ie; }
			else { if (itemp == 0) rotateImage = false; else rotateImage = true; }
		token.clear(); token.str(Argv[++ia]); token >> itemp; if (token.fail()) { ++ie; }
			else { if (itemp == 0) useMedianFilter = false; else useMedianFilter = true; }
		token.clear(); token.str(Argv[++ia]); token >> itemp; if (token.fail() || itemp < 0) { ++ie; }
		else { extraOutput = itemp; }
	}
	std::string prefix = templateDirectory + "\\" + templatePrefix;
	std::string imageName = workingDirectory + "\\" + imageFileName;
	std::string outputBaseName = workingDirectory + "\\" + baseName;

	//unsigned int extraOutput = 0;
	int rollingBallRadius = 100;
	double backgroundFrac = 0.40;
	double wellArea = 10.0;
	cv::Size2f wellSize2f;
	cv::Size2f wellMin;
	cv::Size2f wellMax;
	cv::Rect imageAnalysisArea;
	cv::Rect subImageArea;
	cv::Point2f wellSpacing;
	cv::Size optimalFFTSize;
	double blockSizeMult;
	double minimumPeak;
	double maxFillingRatio;

	//bool rotateImage = false;
	//bool useMedianFilter = false;

	double topFraction = 0.20;
	int numbSections = 0;
	int numbArrays = 0;

	std::vector< DeviceOrigins > sectionOrigins;
	std::vector< cv::Size > sectionDimens;
	std::vector< DeviceOrigins > arrayOrigins;

	int numbXGuards = 0;
	int numbYGuards = 0;
	std::vector< GuardInfo > xGuards;
	std::vector< GuardInfo > yGuards;
	std::vector<ArraySection> arrSecList;
	std::vector< std::vector< cv::Point > > templatePixels;
	templatePixels.clear();
	std::vector< double > templatePixelFraction;
	templatePixelFraction.clear();
	AnalysisParameters fParams;

	std::vector< ParameterLimit > pLimits;
	pLimits.clear();
	ParameterLimit newPLimit;

	// std::clog << "After input" << std::endl;
	std::string outName;
	outName = prefix;
	outName.append("_CoordImage2.png");
	coordImageName.assign(outName.c_str());

	outName = prefix;
	outName.append("_ArrSecImage.png");
	arrSecImageName.assign(outName.c_str());

	outName = prefix;
	outName.append("_InfoFile.txt");
	infoFileName.assign(outName.c_str());

	outName = prefix;
	outName.append("_SubImageInfo_");
	outName.append(subImageLabel.c_str());
	outName.append(".txt");
	subImageFileName.assign(outName.c_str());

	outName = prefix;
	outName.append("_Filter_");
	outName.append(filterLabel.c_str());
	outName.append(".txt");
	parameterFileName.assign(outName.c_str());

	std::ostringstream oString;
	std::ofstream logFile;
	std::ofstream outFile;
	std::string errorMsg;
	outName = outputBaseName;
	outName.append("_000.txt");
	logFile.open(outName.c_str());
	if (!logFile.is_open())
	{
		oString.str("");
		oString.clear();
		std::cerr << "Problems opening log file: " << outName << std::endl;
		return -1;
	}
	logFile << "========== Blob Analysis ==========\n";
	logFile << "Image Name: " << imageName.c_str();
	if (rotateImage)
		logFile << " (image is rotated after input)";
	else
		logFile << "";
	if (useMedianFilter)
		logFile << " (Median Filter with 3x3 kernel is applied to image)\n";
	else
		logFile << "/n";

	logFile << "Coord Image Name: " << coordImageName << "\n";
	logFile << "Array Section Image Name: " << arrSecImageName << "\n";
	logFile << "Device Info File Name: " << infoFileName << "\n";
	logFile << "SubImage Info File Name: " << subImageFileName << "\n";
	logFile << "Filter File Name: " << parameterFileName << "\n";
	logFile << "Base Name: " << outputBaseName << "\n";
	logFile << "Tracking Value: " << extraOutput << "\n";
	
	track.Load(extraOutput, infoMsg);
	logFile << infoMsg;

	cv::Mat image = cv::imread(imageName.c_str(), cv::IMREAD_GRAYSCALE | cv::IMREAD_ANYDEPTH); // Read the file
	unsigned int type = image.depth();
	uchar depth = type & CV_MAT_DEPTH_MASK;
	uchar chans = 1 + (type >> CV_CN_SHIFT);
	if (depth == CV_8U && chans == 1)
	{
		eightBit = true;
		logFile << "\n" << imageName << " is of type CV_8UC1\n";
	}
	else if ((depth == CV_16U) && (chans == 1))
	{
		eightBit = false;
		logFile << "\n" << imageName << " is of type CV_16UC1\n";
		for (int n = 0; n < image.total(); n++)
		{
			unsigned int ntmp = image.at<short unsigned int>(n);
			image.at<short unsigned int>(n) = (ntmp >> 4);
		}
	}
	else
	{
		oString.str("");
		oString.clear();
		oString << "Problems opening image: " << imageName << " Image is of type " << type2str(image.depth()) << " which program cannot handle.";
		err = oString.str();
		std::cerr << err << std::endl;
		logFile << err << std::endl;
		logFile.close();
		return -1;
	}
	cv::Mat coordImage = cv::imread(coordImageName.c_str(), cv::IMREAD_COLOR); // Read the file
	cv::Mat arrSecImage = cv::imread(arrSecImageName.c_str(), cv::IMREAD_GRAYSCALE | cv::IMREAD_ANYDEPTH); // Read the file

	if (image.empty())					  // Check for invalid input
	{
		err = "Could not open or find " + imageName;
		std::cerr << err << std::endl;
		logFile << err << std::endl;
		logFile.close();
		return -1;
	}
	if (coordImage.empty())					  // Check for invalid input
	{
		err = "Could not open or find " + coordImageName;
		std::cerr << err << std::endl;
		logFile << err << std::endl;
		logFile.close();
		return -1;
	}
	if (arrSecImage.empty())					  // Check for invalid input
	{
		err = "Could not open or find " + arrSecImageName;
		std::cerr << err << std::endl;
		logFile << err << std::endl;
		logFile.close();
		return -1;
	}

	cv::Mat workImage1;
	cv::Mat workImage2;
	cv::Mat workImage3;
	cv::Mat workImage4;
	cv::Mat foreground;
	MedianFilter_RotateImage(workImage1, image, useMedianFilter, rotateImage,
		track.OutputExtraImages, eightBit, outputBaseName);

	cv::Size imageSize = image.size();
	cv::Size imageCenter;
	imageCenter.width = imageSize.width / 2;
	imageCenter.height = imageSize.height / 2;
	logFile << "Input Image Size (w,h): (" << imageSize.width << "," << imageSize.height << ")\n";
	int targetSize = (int)ceil(imageSize.width * 1.2) - 1;
	optimalFFTSize.width = cv::getOptimalDFTSize(targetSize);
	while (optimalFFTSize.width % 2 > 0)
	{
		targetSize++;
		optimalFFTSize.width = cv::getOptimalDFTSize(targetSize);
		// std::cout << targetSize << " / " << optimalFFTSize.width << "\n";
	}
	targetSize = (int)ceil(imageSize.height * 1.2) - 1;
	optimalFFTSize.height = cv::getOptimalDFTSize(targetSize);
	while (optimalFFTSize.height % 2 > 0)
	{
		targetSize++;
		optimalFFTSize.height = cv::getOptimalDFTSize(targetSize);
	}

	// Open and read Device Info File (infoFileName), which was output by createmasks and modified by cropinfofile.
	// It contains information about the arrays, sections, and guards which might be included in
	// image
	if (!ReadDeviceInfo(infoFileName,
		arrayOrigins,
		sectionOrigins, sectionDimens,
		xGuards, yGuards,
		errorMsg))
	{
		err = "Error from ReadDeviceInfo: " + errorMsg;
		std::cerr << err << std::endl;
		logFile << err << std::endl;
		logFile.close();
		return -1;
	}
	numbArrays = (int)arrayOrigins.size();
	numbSections = (int)sectionOrigins.size();
	numbXGuards = (int)xGuards.size();
	numbYGuards = (int)yGuards.size();
	std::vector< GuardErrors > xGuardErrorList;
	std::vector< GuardErrors > yGuardErrorList;
	std::vector< GuardErrors > offImageList;
	SetupGuardErrorLists(xGuards, yGuards, imageSize, xGuardErrorList, yGuardErrorList, offImageList);

	int numbRows = 0;
	int numbCols = 0;

	int totalNumbWells = 0;
	for (int n = 0; n < (int)sectionDimens.size(); n++)
	{
		if (sectionDimens[n].width > numbCols)
			numbCols = sectionDimens[n].width;

		if (sectionDimens[n].height > numbRows)
			numbRows = sectionDimens[n].height;

		totalNumbWells += sectionDimens[n].width * sectionDimens[n].height;
	}
	totalNumbWells *= numbArrays;

	// Open and read SubImage data file (subImageFileName). This contains the (array, section) pairs to be analyzed in image.
	// If image is a subimage of a larger image of the entire device, then the list of pairs will not 
	// include all such pairs for the device, only those expected to be in the image.
	// It also includes:
	//     a) informatione which is particular to the subimage being analyzed 
	//		(imageAnalysizArea, SubImageArea)
	//     b) device specific information which is particular to the magnification of the subimage (mostly sizes of objects in pixels)
	//		(wellSize2f, wellSpacing)
	//     c) Initial values for fitting paramaters
	//		(pLimits)

	int numbArraysUsed;
	if (!ReadSubImageData(subImageFileName, numbArrays, arrSecList,
		numbArraysUsed, imageAnalysisArea, subImageArea,
		wellSize2f, wellArea, wellSpacing, //wellVolume,
		pLimits, blockSizeMult, minimumPeak,
		maxFillingRatio, errorMsg))
	{
		err = "Error from ReadSubImageData: " + errorMsg;
		std::cerr << err << std::endl;
		logFile << err << std::endl;
		logFile.close();
		return -1;
	}
	int blockSize = (int)floor(wellSize2f.height * blockSizeMult);
	if (blockSize % 2 == 0)
		blockSize++;

	std::vector< std::vector< int > > arrSecMatrix;
	arrSecMatrix.resize(numbArrays + 1);
	for (int n = 0; n < (int)arrSecMatrix.size(); n++)
	{
		arrSecMatrix[n].resize(numbSections + 1);
		std::fill(arrSecMatrix[n].begin(), arrSecMatrix[n].end(), 0);
	}
	for (int n = 0; n < (int)arrSecList.size(); n++)
		arrSecMatrix[arrSecList[n].Array][arrSecList[n].Section] = 1;

	if (!fParams.ReadFile(parameterFileName, wellArea, wellSize2f, errorMsg))
	{
		err = "Error from fParams.ReadFile: " + errorMsg;
		std::cerr << err << std::endl;
		logFile << err << std::endl;
		logFile.close();
		return -1;
	}
	rollingBallRadius = fParams.RollingBallRadius;
	backgroundFrac = fParams.BackgroundFrac;

	logFile << "\n     From " << infoFileName << "\n";
	logFile << "Number of Arrays per Device: " << numbArrays << "\n";
	logFile << "Number of Sections per Device: " << numbSections << "\n";
	logFile << "Number of X-Guards per Device: " << numbXGuards << "\n";
	logFile << "Number of Y-Guards per Device: " << numbYGuards << "\n";

	logFile << "Total Number of Wells per Device: " << totalNumbWells << "\n";
	logFile << "\n     From " << subImageFileName << "\n";
	logFile << "SubImage Area Origin: (" << subImageArea.x << ", " << subImageArea.y << ") (x, y)\n";
	logFile << "SubImage Area Size: (" << subImageArea.width << ", " << subImageArea.height << ") (width, height)\n";
	logFile << "Rolling Ball Radius (Background subtraction): " << rollingBallRadius << "\n";
	logFile << "Background Fraction: " << backgroundFrac << "\n";
	logFile << "Chamber Size: (" << wellSize2f.width << ", " << wellSize2f.height << ") (width, height)\n";
	logFile << "Chamber Area (pixels): " << wellArea << "\n";
	logFile << "Chamber Spacing: (" << wellSpacing.x << ", " << wellSpacing.y << ") (x,y)\n";
	for (int n = 0; n < (int)pLimits.size(); n++)
	{
		logFile << "Initial Value and Limit for variable parameter " << n << ", [" << pLimits[n].BaseValue
			<< ", " << pLimits[n].Limit << "]\n";
	}
	logFile << "FFT Image Size: (" << optimalFFTSize.width << ", " << optimalFFTSize.height << ") (width, height)\n";
	logFile << "Block Size Mult: " << blockSizeMult << "\n";
	logFile << "Minimum Peak Height of Blob: " << minimumPeak << "\n";
	logFile << "Max Filling Ratio for Blob: " << maxFillingRatio << "\n";
	logFile << std::endl;
	logFile << "Number of Arrays Used in this sub-image: " << numbArraysUsed << "\n";
	logFile << "\n";

	int set1 = 1;
	int setN = (int)fParams.ParameterSet.size() - 1;

	MinMaxLimits mDelta;
	mDelta.Min = 2.0;
	mDelta.Max = (wellSpacing.y - wellSize2f.height) + 3.0;
	fParams.Output(logFile);

	int minContourLength = (int)floor(1.5 * (fParams.ParameterSet[0].BlobWidth.Min + fParams.ParameterSet[0].BlobHeight.Min));
	int maxContourLength = (int)ceil(3.5 * (fParams.ParameterSet[0].BlobWidth.Max + fParams.ParameterSet[0].BlobHeight.Max));
	logFile << "Minimum Contour Length: " << minContourLength << "\n";
	logFile << "Maximum Contour Length: " << maxContourLength << "\n";

	cv::Mat structElementOpen = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3), cv::Point(-1, -1));

	int topNumb = (int)round(fParams.TopFraction * wellArea);
	int maxNumbWells = 0;
	int maxNumbCols = 0;
	for (int n = 0; n < (int)arrSecList.size(); n++)
	{
		int sec = arrSecList[n].Section - 1;
		maxNumbWells += sectionDimens[sec].width * sectionDimens[sec].height;
		if (sectionDimens[sec].width > maxNumbCols)
			maxNumbCols = sectionDimens[sec].width;
	}

	CalculateWellTemplates(wellSize2f, wellArea, topFraction, (int)round(wellArea + 2), templatePixels, templatePixelFraction);

	cv::Mat image32F(imageSize, CV_32F);		// Image converted to float
	cv::Mat backgroundSubImage32F(imageSize, CV_32F);  // This contains image with rolling ball background subtracted
	cv::Mat background32F(imageSize, CV_32F);	// This is the rolling ball background
	// timeMsg = "Before Background correction";
	// OutputTime(timeMsg);
	image.convertTo(image32F, CV_32F, 1.0);
	double minV;
	double maxV;
	cv::Point min_loc;
	cv::Point max_loc;
	if (track.OutputExtraCSV)
	{
		cv::minMaxLoc(image32F, &minV, &maxV, &min_loc, &max_loc);
		logFile << "Min Value in image32F is " << minV << ", at (" << min_loc.x << ", " << min_loc.y << ") (x,y)\n";
		logFile << "Max Value in image32F is " << maxV << ", at (" << max_loc.x << ", " << max_loc.y << ") (x,y)\n";
	}
	bool res3;
	if (track.OutputExtraImages)
	{
		res3 = SubtractBackground32F(image32F, rollingBallRadius, backgroundSubImage32F, background32F);
		if (!res3)
			logFile << "\nError subtracting background\n\n";

		OutputImages003_004(workImage1, workImage2, backgroundSubImage32F, background32F, eightBit, outputBaseName);
	}
	else
	{
		res3 = SubtractBackground32F(image32F, rollingBallRadius, backgroundSubImage32F);
		if (!res3)
			logFile << "\nError subtracting background\n\n";
	}
	image32F.release();
	if (track.OutputExtraCSV)
	{
		cv::minMaxLoc(backgroundSubImage32F, &minV, &maxV, &min_loc, &max_loc);
		logFile << "Min Value in backgroundSubImage32F is " << minV << ", at (" << min_loc.x << ", " << min_loc.y << ") (x,y)\n";
		logFile << "Max Value in backgroundSubImage32F is " << maxV << ", at (" << max_loc.x << ", " << max_loc.y << ") (x,y)\n";
	}

	std::vector< double > imageInten;
	imageInten.resize(backgroundSubImage32F.total());
	for (int n = 0; n < backgroundSubImage32F.total(); n++)
		imageInten[n] = backgroundSubImage32F.at<float>(n);
	sort(imageInten.begin(), imageInten.end());
	Statistics1 bckInten;
	bckInten.Clear();
	for (int n = 0; n < (int)imageInten.size() * backgroundFrac; n++)
		bckInten.Accumulate(imageInten[n]);

	bckInten.Analyze();
	double bck0 = bckInten.Ave();
	double std0 = bckInten.Std();
	double bLimit = bck0 + STDMULTFORFIRSTBACKGNDCALC * std0;
	if (track.OutputExtraCSV)
		logFile << "First Background calculation, Count0 = " << bckInten.Count() << ", bck0 = "
		<< bck0 << ", std0 = " << std0 << ", bLimit = " << bLimit << std::endl;

	if (track.OutputExtraImages)
	{
		Statistics1 rollingBallBckInten;
		rollingBallBckInten.Clear();
		for (int n = 0; n < background32F.total(); n++)
			rollingBallBckInten.Accumulate(background32F.at<float>(n));

		rollingBallBckInten.Analyze();
		logFile << "\nBackground subtraction analysis.  Rolling Ball Radius = " << rollingBallRadius << "\n";
		logFile << "     Subtracted Background = " << rollingBallBckInten.Ave() << " (" << rollingBallBckInten.Std()
			<< ").  Count = " << rollingBallBckInten.Count() << std::endl;
	}

	bckInten.Clear();
	int nn = 0;
	while (imageInten[nn] < bLimit)
	{
		bckInten.Accumulate(imageInten[nn]);
		nn++;
		if (nn >= (int)imageInten.size())
			break;
	}
	double maxInten = imageInten[(int)imageInten.size() - 1];
	bckInten.Analyze();
	double bck1 = bckInten.Ave();
	double std1 = bckInten.Std();
	if (track.OutputExtraCSV)
	{
		logFile << "initial maxInten = " << maxInten << "\n";
		logFile << "Second Background calculation, Count1 = " << bckInten.Count()
			<< ", bck1 = " << bck1 << ", std1 = " << std1 << std::endl;
	}

	backgroundSubImage32F -= bck1;
	backgroundSubImage32F = cv::max(backgroundSubImage32F, 0.0);
	int contourLevel = 255;
	if (eightBit)
	{
		workImage2 = cv::min(backgroundSubImage32F, 255.0);
		workImage2.convertTo(workImage1, CV_8U);
	}
	else
	{
		workImage2 = cv::min(backgroundSubImage32F, 65535);
		workImage2.convertTo(workImage1, CV_16U);

		int iTmp = (int)round((double)imageInten.size() * 0.90);
		if (iTmp > 0)
			contourLevel = (int)round(1.6 * imageInten[iTmp]);

		if (contourLevel > 65535)
			contourLevel = 65535;
	}

	// cv::Size backgroundSubImageSize = backgroundSubImage32F.size();
	if (track.OutputExtraCSV)
	{
		logFile << "contourLevel = " << contourLevel << "\n";
		cv::minMaxLoc(backgroundSubImage32F, &minV, &maxV, &min_loc, &max_loc);
		logFile << "Min Value in backgroundSubImage32F (2) is " << minV << ", at (" << min_loc.x << ", " << min_loc.y << ") (x,y)\n";
		logFile << "Max Value in backgroundSubImage32F (2) is " << maxV << ", at (" << max_loc.x << ", " << max_loc.y << ") (x,y)\n";
	}
	if (track.OutputExtraImages)
	{
		outName = outputBaseName;
		outName.append("_005BckSubIm.png");
		cv::imwrite(outName, workImage1);
	}

	std::vector< int > histogram;
	std::vector< double > wgtHistogram;
	double adaptiveC;
	// timeMsg = "Before Calc Threshold";
	// OutputTime(timeMsg);
	cv::Mat foreground8U;
	cv::Mat backgroundSubImage8U;
	if (eightBit)
	{
		backgroundSubImage8U = workImage1.clone();
		logFile << " eightBit is true\n" << std::endl;
	}
	else
	{
		double min, max;
		cv::minMaxLoc(workImage1, &min, &max);
		if (max > 255.0)
		{
			double scaleF = 255.0 / max;
			workImage2 = workImage1 * scaleF;
			workImage2.convertTo(backgroundSubImage8U, CV_8U);
		}
		else
			workImage1.convertTo(backgroundSubImage8U, CV_8U);

		logFile << " eightBit is false\n" << std::endl;
	}
	// rtype = type2str(backgroundSubImage8U.type()) ;
	// std::clog << "Inside sdpcrSizingAnalysis, backgroundSubImage8U is " << rtype << std::endl;

	outName = outputBaseName;
	outName.append("_006BckIm8U.png");
	cv::imwrite(outName, backgroundSubImage8U);
	// timeMsg = "After Background Correction/Before Adaptive Thresholding";
	// OutputTime(timeMsg);
	CalcAdaptiveThreshold(logFile,
		backgroundSubImage8U, fParams, 0, outputBaseName, blockSize, minimumPeak,
		adaptiveC, foreground8U, track.TrackFilling);

	// timeMsg = "After Calc Threshold";
	// OutputTime(timeMsg);

	std::vector< int > numbBlobsInPass(25);
	Statistics1D blobArea;
	int nBlobs;
	std::vector<std::vector<cv::Point> > contours;
	std::vector<std::vector<cv::Point> > trialContours;
	std::vector<Blob> blobList;
	std::vector<Blob> otherBlobList;
	std::vector<Blob> badBlobList;
	std::vector<Blob> backgroundList;
	std::vector<Blob> allBlobs;

	Statistics1 thresholdIntensities;
	Statistics1 blobIntensities;
	std::vector< bool > filterResults;
	Blob newBlob;
	cv::Moments mom;

	cv::Point wellSpacing8U;
	wellSpacing8U = wellSpacing;
	if (wellSpacing8U.x % 2 == 0)
		wellSpacing8U.x++;
	if (wellSpacing8U.y % 2 == 0)
		wellSpacing8U.y++;
	// timeMsg = "Before Extract Blobs";
	// OutputTime(timeMsg);
	nBlobs = ExtractBlobsAdaptive(logFile, adaptiveC, blockSize, minimumPeak,
		backgroundSubImage8U, backgroundSubImage32F, wellSpacing8U,
		workImage2, workImage3, minContourLength, maxContourLength,
		fParams, set1, setN,
		foreground8U, blobList, otherBlobList,
		badBlobList, backgroundList, track.OutputExtraCSV);

	int firstRowSum = numbCols * numbArrays;
	CheckSectionOne(logFile, imageSize, wellSpacing, firstRowSum, workImage2,
		blobList, otherBlobList, badBlobList, track.TrackAssignment);

	logFile << "BlobList sizes\n";
	logFile << "     blobList.size() = " << blobList.size() << "\n";
	logFile << "     otherblobList.size() = " << otherBlobList.size() << "\n";
	logFile << "     badBlobList.size() = " << badBlobList.size() << "\n";

	
	// int otherBlobListSize = otherBlobList.size();
	if (track.OutputExtraCSV)
	{
		outName = outputBaseName;
		outName.append("_010Blobs.csv");
		WriteBlobListInput(outName, blobList, wellSpacing.y * 0.5, errorMsg);

		outName = outputBaseName;
		outName.append("_011PoorBl.csv");
		WriteBlobListInput(outName, otherBlobList, wellSpacing.y * 0.5, errorMsg);

		outName = outputBaseName;
		outName.append("_012BadBl.csv");
		WriteBlobListInput(outName, badBlobList, wellSpacing.y * 0.5, errorMsg);
	}

	if (track.OutputExtraImages)
		OutputImages008_014(workImage3, trialContours, imageSize, blobList, otherBlobList, badBlobList, foreground8U, outputBaseName);
	else
	{
		outName = outputBaseName;
		outName.append("_008FG-Msk.png");
		cv::imwrite(outName, foreground8U);
	}

	// backgroundSubImage32F has float copy of background subtracted image
	// workImage1 has uchar (or unsigned short int) version of backgroundSubImage32F
	workImage1.copyTo(workImage2);
	workImage1.copyTo(workImage3);
	workImage1.copyTo(workImage4);
	// int blobListSize = (int)blobList.size();
	nBlobs = (int)blobList.size();
	logFile << "Number of Blobs found: " << nBlobs << ", Adaptive C = " << adaptiveC << " for block size = " << blockSize
		<< ". Number of unusable Blobs: " << otherBlobList.size() << "\n";

	if (nBlobs > 0)
	{
		blobArea.Clear();
		for (int b = 0; b < nBlobs; b++)
		{
			blobArea.Accumulate(blobList[b].Shape.Mom.m00);
		}
		blobArea.Analyze();
		logFile << "     Ave Blob Area = " << blobArea.Ave() << " (" << blobArea.Std() << ") Count = " << blobArea.Count() << "\n";
		logFile << "              Total Area = " << blobArea.Sum() << std::endl;
	}
	else
	{
		std::cerr << "No Blobs Found!!!" << std::endl;
		logFile << "NO BLOBS FOUND!!!" << std::endl;
		logFile.close();
		return -10;
	}

	logFile << "\n==============================\n";
	logFile << "Number of well-like blobs found with Adaptive C = " << adaptiveC << " and block size = " << blockSize << "\n";
	logFile << "     Number of blobs = " << blobList.size() << ", Number of bad blobs = " << otherBlobList.size() << "\n";
	logFile << "\n";

	if (track.OutputExtraCSV)
		OutputCSV_017_022(backgroundList, badBlobList, wellSpacing, outputBaseName);

	if (track.OutputExtraImages)
		OutputImages_020_021_015(logFile, workImage2, workImage3, workImage4, contourLevel, blobList, otherBlobList, trialContours, outputBaseName);

	logFile << "\n========== Angle Alignment ==========" << std::endl;
	int rotateResult;
	// int scaleResult;
	double rotate;
	// timeMsg = "Before Angle Alignment";
	// OutputTime(timeMsg);
	workImage1.release();
	workImage2.release();
	workImage3.release();
	workImage4.release();
	AngleAlignment(logFile, blobList,
		imageSize, optimalFFTSize, pLimits[2],
		rotate, rotateResult, errorMsg, outputBaseName, track.TrackAngle);

	logFile << errorMsg << std::endl;
	std::vector< double > results1(NUMBPARAMETERS);
	results1[0] = 0.0;
	results1[1] = 0.0;
	results1[2] = rotate;
	results1[3] = 1.0;
	// timeMsg = "After Angle Alignment";
	// OutputTime(timeMsg);
	double imageScale;
	double imageMinimum;
	if (track.TrackAngle || track.TrackAlignment)
	{
		ShiftImage(results1, backgroundSubImage32F, workImage1, imageScale, imageMinimum, errorMsg);
		outName = outputBaseName;
		outName.append("_025Angle.png");
		cv::imwrite(outName, workImage1);
	}

	double cosAngle = cos(results1[2] * 0.01745329); // angle converted from degrees to radians
	double sinAngle = sin(results1[2] * 0.01745329);
	vSize = (int)blobList.size();
	for (int n = 0; n < vSize; n++)
		blobList[n].ApplyRotation(cosAngle, sinAngle, imageSize);
	vSize = (int)badBlobList.size();
	for (int n = 0; n < vSize; n++)
		badBlobList[n].ApplyRotation(cosAngle, sinAngle, imageSize);
	vSize = (int)otherBlobList.size();
	for (int n = 0; n < vSize; n++)
		otherBlobList[n].ApplyRotation(cosAngle, sinAngle, imageSize);
	// timeMsg = "Before AlignImage";
	// OutputTime(timeMsg);
	logFile << "\n========== Shift/Scale Image ==========" << std::endl;
	int sectionOfInterest;
	std::vector< double > results2;
	AlignImage(logFile, outputBaseName, results1,
		blobList, otherBlobList, badBlobList,
		coordImage, arrSecImage, pLimits,
		arrSecMatrix, arrSecList,
		imageSize, wellSpacing, wellSize2f,
		numbArrays, numbSections, numbCols, numbRows,
		arrayOrigins, sectionOrigins, sectionOfInterest,
		results2, track.TrackAlignment, errorMsg);

	logFile << "After AlignImage" << std::endl;
	std::string blobLabel;
	if (track.OutputExtraCSV)
	{
		allBlobs.resize(blobList.size() + otherBlobList.size() + badBlobList.size() + 2);
		int allBlobsCnt = 0;
		for (int n = 0; n < blobList.size(); n++)
		{
			allBlobs[allBlobsCnt] = blobList[n];
			allBlobsCnt++;
		}
		for (int n = 0; n < otherBlobList.size(); n++)
		{
			allBlobs[allBlobsCnt] = otherBlobList[n];
			allBlobsCnt++;
		}
		for (int n = 0; n < badBlobList.size(); n++)
		{
			allBlobs[allBlobsCnt] = badBlobList[n];
			allBlobsCnt++;
		}
		logFile << "Before write _050" << std::endl;
		oString.str("");
		oString.clear();
		oString << outputBaseName << "_050BL-All.csv";
		outName = oString.str();
		WriteBlobListRotated(outName, allBlobs, wellSpacing.y * 0.5, errorMsg);
		logFile << "After write _050" << std::endl;
		std::vector< Blob > sortedBlobList = blobList;
		std::sort(sortedBlobList.begin(), sortedBlobList.end(), SortBlobsByColumnSmall);
		logFile << "Before write _051" << std::endl;
		outName = outputBaseName;
		outName.append("_051-Col.csv");
		outFile.open(outName.c_str());
		outFile << "Array,Section,Row,Col,Rotated,,Input,\n";
		std::vector< std::vector< std::vector< cv::Point2f > > > arrRowColumn;
		arrRowColumn.resize(numbArrays + 1);
		for (int nn = 1; nn <= numbArrays; nn++)
		{
			arrRowColumn[nn].resize(numbRows + 1);
			for (int mm = 0; mm <= numbRows; mm++)
			{
				arrRowColumn[nn][mm].resize(numbCols + 1);
				for (int kk = 0; kk <= numbCols; kk++)
					arrRowColumn[nn][mm][kk] = cv::Point2f(-1000., -1000.);
			}
		}
		for (int n = 0; n < sortedBlobList.size(); n++)
		{
			if (sortedBlobList[n].SectionPosition.y > 0 && sortedBlobList[n].SectionPosition.y <= numbRows
				&& sortedBlobList[n].SectionPosition.x > 0 && sortedBlobList[n].SectionPosition.x <= numbCols)
			{
				if (arrSecMatrix[sortedBlobList[n].Array][sortedBlobList[n].Section] > 0)
				{
					outFile << sortedBlobList[n].Array
						<< "," << sortedBlobList[n].Section
						<< "," << sortedBlobList[n].SectionPosition.y
						<< "," << sortedBlobList[n].SectionPosition.x
						<< "," << sortedBlobList[n].RotatedCenter2f.y
						<< "," << sortedBlobList[n].RotatedCenter2f.x
						<< "," << sortedBlobList[n].InputCenter2f.y
						<< "," << sortedBlobList[n].InputCenter2f.x
						<< std::endl;
					arrRowColumn[sortedBlobList[n].Array][sortedBlobList[n].SectionPosition.y][sortedBlobList[n].SectionPosition.x] = sortedBlobList[n].RotatedCenter2f;
				}
			}
		}
		outFile << "\n============\n";
		Statistics1 rowStats;
		for (int mm = 2; mm <= numbRows; mm++)
		{
			rowStats.Clear();
			for (int nn = 1; nn <= numbArrays; nn++)
			{
				for (int kk = 1; kk <= numbCols; kk++)
				{
					if (arrRowColumn[nn][mm][kk].y > -10. && arrRowColumn[nn][mm - 1][kk].y > -10.)
					{
						rowStats.Accumulate(arrRowColumn[nn][mm][kk].y - arrRowColumn[nn][mm - 1][kk].y);
					}
				}
			}
			rowStats.Analyze();
			outFile << mm << "," << rowStats.Ave() << "," << rowStats.Std() << "," << rowStats.Count() << "\n";
		}

		outFile.close();
	}
	else
	{
		allBlobs.clear();
	}
	// timeMsg = "After Align Image";
	// OutputTime(timeMsg);
	logFile << "\nFinal Alignment Results: " << results2[0] << " / " << results2[1] << " / " << results2[2] << " / " << results2[3] << std::endl;

	CreateRegImage(results2, subImageArea, backgroundSubImage32F, true,
		workImage2, workImage3, imageScale, imageMinimum, errorMsg);
	if (track.OutputExtraImages)
	{
		logFile << "After CreateRegImage: " << errorMsg << std::endl;
		logFile << "imageScale/Minimum: " << imageScale << " / " << imageMinimum << std::endl;
		outName = outputBaseName;
		outName.append("_091Device.png");
		cv::imwrite(outName, workImage2);	// workImage2 has registration image
	}
	// timeMsg = "Before Sizing";
	// OutputTime(timeMsg);

	cv::Rect searchRect;
	cv::Size searchRectSize;
	searchRectSize.width = (int)round(wellSize2f.width);
	searchRectSize.height = (int)round(wellSize2f.height);
	if (searchRectSize.width % 2 == 0)
		searchRectSize.width++;
	if (searchRectSize.height % 2 == 0)
		searchRectSize.height++;
	cv::Point searchRectOffset;
	searchRectOffset.x = (searchRectSize.width - 1) / 2;
	searchRectOffset.y = (searchRectSize.height - 1) / 2;
	std::vector< Blob > prelimBlobList;
	std::vector< Blob > blobsNotFound;
	double scaledWellArea = wellArea / pow(results2[3], 2);
	BlobShape emptyShape;
	emptyShape.Clear();
	emptyShape.Threshold = -1;
	emptyShape.Status = 3;

	std::vector< bool > blobsFound;
	logFile << "\nEvaluating Filling" << std::endl;

	logFile << "     Number of blobs = " << blobList.size() << std::endl;
	if (track.TrackFilling)
	{
		oString.str("");
		oString.clear();
		oString << outputBaseName << "_102Fill-D.csv";
		outName = oString.str();
	}
	else
		outName.clear();

	// timeMsg = "Before Calc filling";
	// OutputTime(timeMsg);
	logFile << "Before CalculateFilling, blobList.size() = " << blobList.size() << std::endl;
	double minRingAve = minimumPeak * 0.25;
	int numbCalc = CalculateFilling(logFile, backgroundSubImage32F, blobList,
		topNumb, outName, scaledWellArea, minRingAve, maxFillingRatio,
		wellSpacing,
		templatePixels, templatePixelFraction);
	logFile << "number of blobs for which filling was calculated: " << numbCalc << std::endl;

	logFile << "Before WriteSizingFile" << std::endl;
	oString.str("");
	oString.clear();
	oString << outputBaseName << "_110Fill.csv";
	outName = oString.str();

	// timeMsg = "Before Write Sizing File";
	// OutputTime(timeMsg);
	logFile << "List Sizes:" << blobList.size() << " / " << otherBlobList.size() << " / " << badBlobList.size() << std::endl;
	WriteSizingFile(blobList, otherBlobList, badBlobList,
		results2, sectionOfInterest,
		minRingAve, maxFillingRatio, fParams, 1,
		backgroundSubImage32F, foreground8U,
		outName, imageScale, imageMinimum,
		workImage2, workImage3, workImage4, track.TrackFilling);

	// timeMsg = "After Write Sizing File";
	// OutputTime(timeMsg);

	oString.str("");
	oString.clear();
	oString << outputBaseName << "_112Fill.png";
	outName = oString.str();
	cv::imwrite(outName, workImage2);

	oString.str("");
	oString.clear();
	oString << outputBaseName << "_113F_Msk.png";
	outName = oString.str();
	cv::imwrite(outName, workImage3);

	oString.str("");
	oString.clear();
	oString << outputBaseName << "_114Cont.png";
	outName = oString.str();
	cv::imwrite(outName, workImage4);

	logFile.close();

	OutputSizingResults(arrSecList, blobList, otherBlobList, outputBaseName);
	// timeMsg = "At End of Program";
	// OutputTime(timeMsg);
	logFile.close();
	return 0;
}
void ShowUsage(const char* msg, const char* exe)
{
	std::clog << ">>>> " << msg << std::endl;
	std::clog << "Usage: " << exe << " <Working Directory> <Template Directory> <Image Name> <Prefix> <FilterLabel> <SubImage Label>\n";
	std::clog << "<Output BaseName> <Transpose Image ? > <Use Median Filter ? > <Extra Output>\n";	// 
	std::clog << "For boolean values <Transpose Image?> and <Use Median Filter?> use 1 for true and 0 for false\n";
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// By downloading, copying, installing or using the software you agree to this license. If you do not agree to this license, 
// do not download, install, copy or use the software.
//
// License Agreement 
// For Open Source Computer Vision Library 
// (3-clause BSD License)
//
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions
// are met:
// •	Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
// •	Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer
//			in the documentation and/or other materials provided with the distribution.
// •	Neither the names of the copyright holders nor the names of the contributors may be used to endorse or promote products derived
//			from this software without specific prior written permission.
//
//	This software is provided by the copyright holders and contributors “as is” and any express or implied warranties, including, but not 
//	limited to, the implied warranties of merchantability and fitness for a particular purpose are disclaimed. In no event shall copyright
//	holders or contributors be liable for any direct, indirect, incidental, special, exemplary, or consequential damages (including, but 
//	not limited to, procurement of substitute goods or services; loss of use, data, or profits; or business interruption) however caused 
//	and on any theory of liability, whether in contract, strict liability, or tort (including negligence or otherwise) arising in any way
//	out of the use of this software, even if advised of the possibility of such damage.
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

