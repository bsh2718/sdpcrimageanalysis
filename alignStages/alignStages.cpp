
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "physicalconstants.h"
#include "statistics.hpp"

struct HoughLines
{
    int votes;
    double rho;
    double theta;
    double thetaX;
    double XIntercept;
    double YIntercept;
};
struct LinePairs
{
    int Index1;
    int Votes1;
    double Intercept1;
    int Index2;
    int Votes2;
    double Intercept2;
    double Delta;  
};
std::string type2str(int type)
{
    std::string r;

    uchar depth = type & CV_MAT_DEPTH_MASK;
    uchar chans = 1 + (type >> CV_CN_SHIFT);

    switch (depth)
    {
    case CV_8U:  r = "8U"; break;
    case CV_8S:  r = "8S"; break;
    case CV_16U: r = "16U"; break;
    case CV_16S: r = "16S"; break;
    case CV_32S: r = "32S"; break;
    case CV_32F: r = "32F"; break;
    case CV_64F: r = "64F"; break;
    default:     r = "User"; break;
    }

    r += "C";
    r += (chans + '0');

    return r;
}

void ShowUsage(const char* msg, const char* exe)
{
    std::clog << ">>>> " << msg << std::endl;
    std::clog << "Usage: " << exe << " <Working Directory> <Image Name> <Alignment Threshold> <mm Per Pixel> <DistanceToCenter(mm)> <Vert Bar Thickness(mm)>\n";
    std::clog << "<Horz Bar Thickness (mm)> <Steps Per Revolution > <mm Per Rotation (translation) > [<Base Name>]\n";	// 
    std::clog << "\n";
}

int main(int Argc, char** Argv)
{
    std::ostringstream oString;
    std::ofstream logFile;
    std::string errorMsg;
    std::string outName;
    int numbRequiredArgs = 10;
    if (Argc < numbRequiredArgs)
    {
        ShowUsage("Too few arguments", Argv[0]);
        exit(0);
    }
    std::string workingDirectory;
    std::string imageName;
    int threshold;
    double mmPerPixel;
    double distanceToCenter_mm;
    double vertBarThickness_mm;
    double horzBarThickness_mm;
    int stepsPerRevolution;
    double mmPerRevolution;
    std::string baseName;
    std::string basePath;
    bool outputFiles = false;
    double rhoStep = 2.0;                       // 3.0
    double thetaStep = RADIANSPERDEGREE * 0.7;  // * 1.0
    double distanceToCenter;
    int vBarThickness;
    int hBarThickness;
    double mmPerStep;
    int ia = 0;
    int ie = 0;
    std::istringstream token;
    {
        double temp;
        int itemp;
        //std::string stemp;
        workingDirectory.assign(Argv[++ia]);
        imageName.assign(Argv[++ia]);

        token.clear(); token.str(Argv[++ia]); token >> itemp; if (token.fail() || itemp <= 0.0) { ++ie; }
        else { threshold = itemp; }

        token.clear(); token.str(Argv[++ia]); token >> temp; if (token.fail() || temp <= 0.001) { ++ie; }
        else { mmPerPixel = temp; }

        token.clear(); token.str(Argv[++ia]); token >> temp; if (token.fail() || temp <= 0.0) { ++ie; }
        else { distanceToCenter_mm = temp; }

        token.clear(); token.str(Argv[++ia]); token >> temp; if (token.fail() || temp <= 0.0) { ++ie; }
        else { vertBarThickness_mm = temp; }

        token.clear(); token.str(Argv[++ia]); token >> temp; if (token.fail() || temp <= 0.0) { ++ie; }
        else { horzBarThickness_mm = temp; }

        token.clear(); token.str(Argv[++ia]); token >> itemp; if (token.fail() || itemp <= 0.0) { ++ie; }
        else { stepsPerRevolution = itemp; }

        token.clear(); token.str(Argv[++ia]); token >> temp; if (token.fail() || temp <= 0.0) { ++ie; }
        else { mmPerRevolution = temp; }

        if (++ia < Argc)
        {
            baseName.assign(Argv[ia]);
            outputFiles = true;
        }
        else
            baseName = "";
    }
    std::vector< std::string > logOutput;
    logOutput.clear();
    distanceToCenter = distanceToCenter_mm / mmPerPixel;
    vBarThickness = (int)round(vertBarThickness_mm / mmPerPixel);
    hBarThickness = (int)round(horzBarThickness_mm / mmPerPixel);
    mmPerStep = mmPerRevolution / stepsPerRevolution;
    
    if (outputFiles)
    {
        basePath = workingDirectory + "\\" + baseName;
        outName = basePath;
        outName.append("_.csv");
        logFile.open(outName.c_str());
        if (!logFile.is_open())
        {
            std::cerr << "Problems opening log file: " << outName << std::endl;
            exit(-1);
        }
        logFile << "logFile," << outName << std::endl;
    }
    double rotAngle = 0.0;
    double rotSteps = 0;
    double xSteps = 0;
    double ySteps = 0;
    std::string inputImageName = workingDirectory + "\\" + imageName;
    cv::Mat image = cv::imread(inputImageName, cv::IMREAD_GRAYSCALE | cv::IMREAD_ANYDEPTH);
    if (image.empty())
    {
        std::cerr << "image file is either empty or could not be found: " << inputImageName << std::endl;
        if (outputFiles)
            logFile << "image file is either empty or could not be found: " << inputImageName << std::endl;
        exit(-2);
    }
    bool eightBit = false;
    unsigned int type = image.depth();
    uchar depth = type & CV_MAT_DEPTH_MASK;
    uchar chans = 1 + (type >> CV_CN_SHIFT);
    if (depth == CV_8U && chans == 1)
    {
        eightBit = true;
        oString.str("");
        oString.clear();
        oString << " image type, CV_8UC1";
        if (outputFiles)
            logOutput.push_back(oString.str());
    }
    else if ((depth == CV_16U) && (chans == 1))
    {
        eightBit = false;
        oString.str("");
        oString.clear();
        oString << "image type, CV_16UC1";
        if (outputFiles)
            logOutput.push_back(oString.str());
        for (int n = 0; n < image.total(); n++)
        {
            unsigned int ntmp = image.at<short unsigned int>(n);
            image.at<short unsigned int>(n) = (ntmp >> 4);
        }
    }
    else
    {
        oString.str("");
        oString.clear();
        oString << "Problems opening image: " << inputImageName << " Image is of type " << type2str(image.depth()) << " which program cannot handle.";
        errorMsg = oString.str();        
        ShowUsage(errorMsg.c_str(), Argv[0]);
        if (outputFiles)
        {
            logFile << errorMsg << std::endl;
            logFile.close();
        }
        exit(-3);
    }

    cv::Mat workImage1;
    cv::Mat data;
    int width = image.cols;
    int height = image.rows;
    int total = width * height;
    cv::threshold(image, workImage1, threshold, 255, cv::THRESH_BINARY);
    double nzFrac = (double)cv::countNonZero(workImage1) / (double)total;
    if (nzFrac < 0.05)
    {
        oString.str("");
        oString.clear();
        oString << "Fraction of bright pixesls in image (" << nzFrac << ") is too small";
        errorMsg = oString.str();
        if (outputFiles)
        {
            logFile << errorMsg << std::endl;
            logFile.close();
        }
        exit(-10);

    }
    if (eightBit)
    {
        image.convertTo(data, CV_8U);
    }
    else
    {
        double minVal;
        double maxVal;
        cv::minMaxIdx(image, &minVal, &maxVal);
        if (maxVal <= 255)
            image.convertTo(data, CV_8UC1);
        else if (maxVal - minVal <= 255)
        {
            image = image - minVal;
            image.convertTo(data, CV_8UC1);
        }
        else
        {
            double scale = 255.0 / (maxVal - minVal);
            image = (image - minVal) * scale;
            image.convertTo(data, CV_8UC1);
        }
    }
    if (outputFiles)
    {
        outName = basePath + "_01.png";
        cv::imwrite(outName.c_str(), image);
    }
    

    //int blocksize;
    //if (vBarThickness > hBarThickness)
    //    blocksize = vBarThickness;
    //else
    //    blocksize = hBarThickness;

    //if (blocksize % 2 == 0)
    //    blocksize++;

    //cv::adaptiveThreshold(data, workImage1, 255, cv::ADAPTIVE_THRESH_MEAN_C, cv::THRESH_BINARY, blocksize, 0);
    logFile << "H/VBarThickness, " << hBarThickness << ", " << vBarThickness << std::endl;
    logFile << "image.cols/rows, " << image.cols << ", " << image.rows << std::endl;
    double maxSize = (hBarThickness * image.cols) + (vBarThickness * (image.rows - hBarThickness));
    double maxFrac = maxSize / (image.total());
    image.release();

    double oT = cv::threshold(data, workImage1, threshold, 255, cv::THRESH_OTSU);
    cv::Mat hist; // (1, 256, CV_32SC1);
    int histSize = 256;
    float range[] = { 0, 256 };
    const float* histRange = { range };
    bool uniform = true;
    bool accumulate = false;
    cv::calcHist(&data, 1, 0, cv::Mat(), hist, 1, &histSize, &histRange, uniform, accumulate);
   /* type = hist.depth();
    std::string histType = type2str(type);
    std::cout << histType << std::endl;
    std::cout << hist.total() << std::endl;*/

    float sum = 0;
    int oThreshold = (int)ceil(oT);
    if (oThreshold > 255 || oThreshold < 0)
    {
        oString.str("");
        oString.clear();
        oString << "Otsu thresholding failed";
        errorMsg = oString.str();
        if (outputFiles)
        {
            logFile << errorMsg << std::endl;
            logFile.close();
        }
        exit(-11);
    }
    for (int n = oThreshold; n < 256; n++)
    {
        sum += hist.at<float>(cv::Point(n, 1));
        //if (outputFiles)
        //    logFile << n << "," << sum << "," << hist.at<float>(cv::Point(n, 1)) << std::endl;
    }
    if ( outputFiles)
        logFile << "sum/maxSize, " << sum << ", " << maxSize << std::endl;
    if (sum > 1.2 * maxSize)
    {
        float sum2 = 0;
        int newThreshold = 256;
        while (sum2 < 1.2 * maxSize )
        {
            newThreshold--;
            if (newThreshold < 0)
                break;
            sum2 += hist.at<float>(newThreshold);
            if (outputFiles)
                logFile << "newThreshold/sum2, " << newThreshold << ", " << sum2 << "\n";
        }

        if (newThreshold > oThreshold)
        {
            cv::threshold(data, workImage1, ((double)newThreshold - 0.5), 255, cv::THRESH_BINARY);
            if ( outputFiles)
                logFile << "Threshold increased from " << oThreshold << " (Otsu) to " << newThreshold << "\n";
        }
        else
        {
            if (outputFiles)
                logFile << "Otsu threshold = " << oThreshold << "(" << oT << ")\n";
        }
    }
    else
    {
        if (outputFiles)
            logFile << "Otsu threshold = " << oThreshold << "(" << oT << ")\n";
    }

    int v = height - 1;
    int h = width - 1;
    for (int n = 0; n < workImage1.cols; n++)
    {
        workImage1.at<uchar>(0, n) = 0;
        workImage1.at<uchar>(v, n) = 0;
    }
    for (int n = 0; n < workImage1.rows; n++)
    {
        workImage1.at<uchar>(n, 0) = 0;
        workImage1.at<uchar>(n, h) = 0;
    }
    workImage1.copyTo(data);

    // floodFill filss in area around wells
    cv::floodFill(data, cv::Point(0, 0), cv::Scalar(255));

    // Invert floodfilled image, this leaves non zero values for holes disconnected from edge
    cv::bitwise_not(data, data);

    // Combine the two images to get the closed foreground
    // cv::bitwise_or(WorkImage2, WorkImage3, Foreground8U );
    cv::Mat workImage2 = (workImage1 | data);
    cv::Mat mask;
    cv::Mat structElement = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(7, 7), cv::Point(-1, -1));
    cv::morphologyEx(workImage2, mask, cv::MORPH_OPEN, structElement, cv::Point(-1, -1), 2);
    if (outputFiles)
    {
        outName = basePath + "_02.png";
        cv::imwrite(outName.c_str(), data);
        outName = basePath + "work1.png";
        cv::imwrite(outName.c_str(), workImage1);
        outName = basePath + "_mask1.png";
        cv::imwrite(outName.c_str(), mask);
    }
    std::vector<std::vector<cv::Point> > trialContours;
    trialContours.clear();

    cv::Mat WorkImage2 = mask.clone();
    cv::findContours(WorkImage2, trialContours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE, cv::Point(0, 0));
    int nCont = (int)trialContours.size();
    if (nCont < 1)
    {
        if (outputFiles)
        {
            logFile << "No blobs found in image";
            logFile.close();
        }
        std::cout << "No blobs found in image" << std::endl;
        exit(-4);
    }
    int maxPixelCount = -1;
    int maxContour = -1;
    for (int n = 0; n < nCont; n++)
    {
        cv::Rect boundingRect = cv::boundingRect(trialContours[n]);
        if (boundingRect.height > 2 * hBarThickness && boundingRect.x < 10 && boundingRect.width > width - 20)
        {
            cv::Moments mom;
            mom = moments(trialContours[n]);
            if (mom.m00 > maxPixelCount)
            {
                maxPixelCount = (int)mom.m00;
                maxContour = n;
            }
        }
    }
    if (maxContour < 0)
    {
        if (outputFiles)
        {
            logFile << "No acceptable blob found in image";
            logFile.close();
        }
        std::cout << "No acceptable blob found in image" << std::endl;;
        exit(-5);
    }

    cv::Mat linesV;
    cv::Mat linesH;
    std::vector< cv::Vec3d> line3dV;
    std::vector< cv::Vec3d> line3dH;
    std::vector<cv::Point> pointList;
    int widthLimit = width - 10;
    int heightLimit = height - 10;
    for (int n = 0; n < trialContours[maxContour].size(); n++)
    {
        if (trialContours[maxContour][n].x > 9 && trialContours[maxContour][n].x < widthLimit
            && trialContours[maxContour][n].y > 9 && trialContours[maxContour][n].y < heightLimit)
        {
            pointList.push_back(trialContours[maxContour][n]);
        }
    }

    mask.setTo(0);
    for (int n = 0; n < pointList.size(); n++)
        mask.at<uchar>(pointList[n]) = 255;
    if (outputFiles)
    {
        outName = basePath + "_mask2.png";
        cv::imwrite(outName.c_str(), mask);
    }
    cv::HoughLinesPointSet(pointList, linesV, 50, 50, 10, width - 10, rhoStep,
        -PIDIVIDEDBYSIX, PIDIVIDEDBYSIX, thetaStep); // -CV_PI / 6.0, CV_PI / 6.0, CV_PI / 180.);
    linesV.copyTo(line3dV);
    cv::HoughLinesPointSet(pointList, linesH, 50, 50, 10, height - 10, rhoStep,
        PIDIVIDEDBYTWO - PIDIVIDEDBYSIX, PIDIVIDEDBYTWO + PIDIVIDEDBYSIX, thetaStep);
        //CV_PI * (1.0 / 2.0 - 1.0 / 6.0), CV_PI * (1.0 / 2.0 + 1.0 / 6.0), CV_PI / 180.);
    linesH.copyTo(line3dH);
    std::vector<LinePairs> vertLinePairs;
    std::vector<LinePairs> horzLinePairs;
    vertLinePairs.clear();
    horzLinePairs.clear(); 
    LinePairs tmpLinePair;

    std::vector<HoughLines> vertHoughLines;
    std::vector<HoughLines> horzHoughLines;
    vertHoughLines.clear();
    horzHoughLines.clear();
    HoughLines tmpHL;
    
    double xInterceptSeparationUpper = vBarThickness * 1.15; // 1.15 allows for image to be rotated by up to 22.5 degrees and also up 6% error in magnification
    double xInterceptSeparationLower = vBarThickness * 0.94; // 0.94 allows for 6% error in magnification
    double yInterceptSeparationUpper = hBarThickness * 1.15;
    double yInterceptSeparationLower = hBarThickness * 0.94;
    double xInterceptLowerLimit = width / 12;
    double xInterceptUpperLimit = (11 * width) / 12;
    double yInterceptLowerLimit = height / 3;
    double yInterceptUpperLimit = height - 5;
    //std::cout << "xInterceptLimits: " << xInterceptSeparationUpper << " / " << xInterceptSeparationLower << std::endl;
    //std::cout << "yInterceptLimits: " << yInterceptSeparationUpper << " / " << yInterceptSeparationLower << std::endl;
    if (outputFiles)
    {
        oString.str("");
        oString.clear();
        oString << "RhoStep: " << rhoStep << ",  ThetaStep: " <<  thetaStep;
        logOutput.push_back(oString.str());
        oString.str("");
        oString.clear();
        oString << "xInterecptSeparationUpper/Lower: " << xInterceptSeparationUpper << " / " << xInterceptSeparationLower;
        logOutput.push_back(oString.str());
        oString.str("");
        oString.clear();
        oString << "xInterecptUpper/LowerLimit: " << xInterceptUpperLimit << " / " << xInterceptLowerLimit;
        logOutput.push_back(oString.str());
        oString.str("");
        oString.clear();
        oString << "yInterecptSeparationUpper/Lower: " << yInterceptSeparationUpper << " / " << yInterceptSeparationLower;
        logOutput.push_back(oString.str());
        logOutput.push_back("votes,rho,theta,thetaX,x-inter,y-inter");
        oString.str("");
        oString.clear();
        oString << "yInterecptUpper/LowerLimit: " << yInterceptUpperLimit << " / " << yInterceptLowerLimit;
        logOutput.push_back(oString.str());
        logOutput.push_back("\nvote,rho,theta,thetaX,x-inter,y-inter");
    }
    double leftVertIntercept;
    double rightVertIntercept;
    double leftVertTheta;
    double rightVertTheta;
    double topHorzIntercept;
    double botHorzIntercept;
    double topHorzTheta{};
    double botHorzTheta;
    bool foundVLines = false;
    bool foundTopHLine = false;
    bool foundBotHLine = false;
    for (int n = 0; n < line3dV.size(); n++)
    {
        tmpHL.votes = (int)line3dV[n].val[0];
        tmpHL.rho = line3dV[n].val[1];
        tmpHL.theta = line3dV[n].val[2];
        if (abs(cos(tmpHL.theta)) > 0.00000001)
            tmpHL.XIntercept = tmpHL.rho / cos(tmpHL.theta);
        else
            tmpHL.XIntercept = width + 10;
        if (abs(sin(tmpHL.theta)) > 0.00000001)
            tmpHL.YIntercept = tmpHL.rho / sin(tmpHL.theta);
        else
            tmpHL.YIntercept = height + 10;
        if (line3dV[n].val[2] > PIDIVIDEDBYFOUR)
            tmpHL.thetaX = line3dV[n].val[2] - PIDIVIDEDBYTWO;
        else
            tmpHL.thetaX = line3dV[n].val[2];

        vertHoughLines.push_back(tmpHL);
        oString.str("");
        oString.clear();
        oString << tmpHL.votes << "," << tmpHL.rho << "," << tmpHL.theta << "," << tmpHL.thetaX << "," << tmpHL.XIntercept << "," << tmpHL.YIntercept;
        logOutput.push_back(oString.str());
    }
    int largestVertVotes = 0;
    for (int n = 0; n < vertHoughLines.size() - 1; n++)
    {
        if (vertHoughLines[n].XIntercept > xInterceptLowerLimit && vertHoughLines[n].XIntercept < xInterceptUpperLimit)
        {
            if (largestVertVotes <= 0)
                largestVertVotes = vertHoughLines[n].votes;
            else if (vertHoughLines[n].votes < largestVertVotes / 3)
                break;
            for (int m = n + 1; m < vertHoughLines.size(); m++)
            {
                if (vertHoughLines[m].XIntercept > xInterceptLowerLimit && vertHoughLines[m].XIntercept < xInterceptUpperLimit)
                {
                    if (vertHoughLines[m].votes < largestVertVotes / 3)
                        break;

                    tmpLinePair.Delta = fabs(vertHoughLines[n].XIntercept - vertHoughLines[m].XIntercept);
                    if (tmpLinePair.Delta > xInterceptSeparationLower && tmpLinePair.Delta < xInterceptSeparationUpper)
                    {
                        tmpLinePair.Index1 = n;
                        tmpLinePair.Index2 = m;
                        tmpLinePair.Votes1 = vertHoughLines[n].votes;
                        tmpLinePair.Votes2 = vertHoughLines[m].votes;
                        tmpLinePair.Intercept1 = vertHoughLines[n].XIntercept;
                        tmpLinePair.Intercept2 = vertHoughLines[m].XIntercept;
                        vertLinePairs.push_back(tmpLinePair);
                    }
                }
            }
        }
    }
    std::vector< bool > vertLeftIdx;
    std::vector< bool > vertRightIdx;
    vertLeftIdx.resize(vertHoughLines.size());
    vertRightIdx.resize(vertHoughLines.size());
    std::fill(vertLeftIdx.begin(), vertLeftIdx.end(), false);
    std::fill(vertRightIdx.begin(), vertRightIdx.end(), false);
    for (int n = 0; n < vertLinePairs.size(); n++)
    {
        if (vertLinePairs[n].Intercept1 < vertLinePairs[n].Intercept2)
        {
            vertLeftIdx[vertLinePairs[n].Index1] = true;
            vertRightIdx[vertLinePairs[n].Index2] = true;
        }
        else
        {
            vertRightIdx[vertLinePairs[n].Index1] = true;
            vertLeftIdx[vertLinePairs[n].Index2] = true;
        }
    }
    Statistics1 topInterceptStat;
    Statistics1 topThetaXStat;
    Statistics1 botInterceptStat;
    Statistics1 botThetaXStat;
    topInterceptStat.Clear();
    topThetaXStat.Clear();
    botInterceptStat.Clear();
    botThetaXStat.Clear();
    Statistics1 leftInterceptStat;
    Statistics1 leftThetaXStat;
    Statistics1 rightInterceptStat;
    Statistics1 rightThetaXStat;
    leftInterceptStat.Clear();
    leftThetaXStat.Clear();
    rightInterceptStat.Clear();
    rightThetaXStat.Clear();
    for (int m = 0; m < vertHoughLines.size(); m++)
    {
        if (vertLeftIdx[m])
        {
            leftInterceptStat.Accumulate(vertHoughLines[m].XIntercept);
            leftThetaXStat.Accumulate(vertHoughLines[m].thetaX);
        }
        if (vertRightIdx[m])
        {
            rightInterceptStat.Accumulate(vertHoughLines[m].XIntercept);
            rightThetaXStat.Accumulate(vertHoughLines[m].thetaX);
        }
    }
    logOutput.push_back("");
    leftInterceptStat.Analyze();
    leftThetaXStat.Analyze();
    rightInterceptStat.Analyze();
    rightThetaXStat.Analyze();
    if ( leftInterceptStat.Count() > 0 && rightInterceptStat.Count() > 0)
    {
        leftVertIntercept = leftInterceptStat.Ave();
        rightVertIntercept = rightInterceptStat.Ave();
        leftVertTheta = leftThetaXStat.Ave();
        rightVertTheta = rightThetaXStat.Ave();
        foundVLines = true;

        for (int n = 0; n < line3dH.size(); n++)
        {
            tmpHL.votes = (int)line3dH[n].val[0];
            tmpHL.rho = line3dH[n].val[1];
            tmpHL.theta = line3dH[n].val[2];
            if (abs(cos(tmpHL.theta)) > 0.00000001)
                tmpHL.XIntercept = tmpHL.rho / cos(tmpHL.theta);
            else
                tmpHL.XIntercept = width + 10;
            if (abs(sin(tmpHL.theta)) > 0.00000001)
                tmpHL.YIntercept = tmpHL.rho / sin(tmpHL.theta);
            else
                tmpHL.YIntercept = height + 10;
            if (line3dH[n].val[2] > PIDIVIDEDBYFOUR)
                tmpHL.thetaX = line3dH[n].val[2] - PIDIVIDEDBYTWO;
            else
                tmpHL.thetaX = line3dH[n].val[2];

            horzHoughLines.push_back(tmpHL);
            oString.str("");
            oString.clear();
            oString << tmpHL.votes << "," << tmpHL.rho << "," << tmpHL.theta << "," << tmpHL.thetaX << "," << tmpHL.XIntercept << "," << tmpHL.YIntercept;
            logOutput.push_back(oString.str());
        }
        int largestHorzVotes = 0;
        for (int n = 0; n < horzHoughLines.size() - 1; n++)
        {
            if (horzHoughLines[n].YIntercept > yInterceptLowerLimit && horzHoughLines[n].YIntercept < yInterceptUpperLimit)
            {
                if (largestHorzVotes <= 0)
                    largestHorzVotes = horzHoughLines[n].votes;
                else if (horzHoughLines[n].votes < largestHorzVotes / 3)
                    break;
                for (int m = n + 1; m < horzHoughLines.size(); m++)
                {
                    if (horzHoughLines[m].YIntercept > yInterceptLowerLimit && horzHoughLines[m].YIntercept < yInterceptUpperLimit)
                    {
                        if (horzHoughLines[m].votes < largestHorzVotes / 3)
                            break;

                        tmpLinePair.Delta = fabs(horzHoughLines[n].YIntercept - horzHoughLines[m].YIntercept);
                        if (tmpLinePair.Delta > yInterceptSeparationLower && tmpLinePair.Delta < yInterceptSeparationUpper)
                        {
                            tmpLinePair.Index1 = n;
                            tmpLinePair.Index2 = m;
                            tmpLinePair.Votes1 = horzHoughLines[n].votes;
                            tmpLinePair.Votes2 = horzHoughLines[m].votes;
                            tmpLinePair.Intercept1 = horzHoughLines[n].YIntercept;
                            tmpLinePair.Intercept2 = horzHoughLines[m].YIntercept;
                            horzLinePairs.push_back(tmpLinePair);
                        }
                    }
                }
            }
        }
        std::vector< bool > horzTopIdx;
        std::vector< bool > horzBotIdx;
        horzTopIdx.resize(horzHoughLines.size());
        horzBotIdx.resize(horzHoughLines.size());
        std::fill(horzTopIdx.begin(), horzTopIdx.end(), false);
        std::fill(horzBotIdx.begin(), horzBotIdx.end(), false);
        for (int n = 0; n < horzLinePairs.size(); n++)
        {
            if (horzLinePairs[n].Intercept1 < horzLinePairs[n].Intercept2)
            {
                horzTopIdx[horzLinePairs[n].Index1] = true;
                horzBotIdx[horzLinePairs[n].Index2] = true;
            }
            else
            {
                horzBotIdx[horzLinePairs[n].Index1] = true;
                horzTopIdx[horzLinePairs[n].Index2] = true;
            }
        }
        for (int m = 0; m < horzHoughLines.size(); m++)
        {
            if (horzTopIdx[m])
            {
                topInterceptStat.Accumulate(horzHoughLines[m].YIntercept);
                topThetaXStat.Accumulate(horzHoughLines[m].thetaX);
            }
            if (horzBotIdx[m])
            {
                botInterceptStat.Accumulate(horzHoughLines[m].YIntercept);
                botThetaXStat.Accumulate(horzHoughLines[m].thetaX);
            }
        }
        if (topInterceptStat.Count() > 0 && botInterceptStat.Count() > 0)
        {
            topHorzIntercept = topInterceptStat.Ave();
            botHorzIntercept = botInterceptStat.Ave();
            topHorzTheta = topThetaXStat.Ave();
            botHorzTheta = botThetaXStat.Ave();
            foundTopHLine = true;
            foundBotHLine = true;
        }
        else
        {
            topInterceptStat.Clear();
            topThetaXStat.Clear();
            int upperLimit = height - (int)round(hBarThickness * 1.15);
            for (int n = 0; n < horzHoughLines.size() - 1; n++)
            {
                if (horzHoughLines[n].YIntercept > upperLimit)
                {
                    topInterceptStat.Accumulate(horzHoughLines[n].YIntercept);
                    topThetaXStat.Accumulate(horzHoughLines[n].thetaX);
                }
            }
            topInterceptStat.Analyze();
            topThetaXStat.Analyze();
            if (topInterceptStat.Count() > 0)
            {
                topHorzIntercept = topInterceptStat.Ave();
                topHorzTheta = topThetaXStat.Ave();
                foundTopHLine = true;
            }
        }   
    }
    if (foundVLines && foundTopHLine)
    {
        std::string argNames[11] = { "exe", "Working Directory", "Image Name",
            "Align Threshold", "mm/Pixel", "Dist to Center", "V Bar Thickness",
            "H Bar Thickness", "Steps/Revolution", "mm/Rotation", "Base Name" };
        double theta;
        if ( !foundBotHLine)
            theta = 0.3333333333 * (leftVertTheta + rightVertTheta + topHorzTheta);
        else
            theta = 0.25 * (leftVertTheta + rightVertTheta + topHorzTheta + botHorzTheta);
        double topCenterOfHBar = topHorzIntercept + width * 0.5 * sin(topHorzTheta) / cos(topHorzTheta);
        double botCenterOfVbar = 0.5 * (
            (leftVertIntercept - height * sin(leftVertTheta)) / cos(leftVertTheta) 
            + (rightVertIntercept - height * sin(rightVertTheta)) / cos(rightVertTheta));
        double hshift = width * 0.5 - (botCenterOfVbar + sin(theta) * distanceToCenter);
        double vshift = ((double)height - 200) - (topCenterOfHBar + hBarThickness + distanceToCenter*(1.0 - cos(theta)));
        rotAngle = theta;
        rotSteps = -(rotAngle / TWOPI) * stepsPerRevolution;
        xSteps = -hshift * mmPerPixel / mmPerStep;
        ySteps = vshift * mmPerPixel / mmPerStep;
        std::cout << rotSteps << "," << xSteps << "," << ySteps << std::endl;
        if (outputFiles)
        {
            logFile << rotSteps << "," << xSteps << "," << ySteps << std::endl;
            logFile << "=====,Argv[]," << std::endl;
            for (int n = 0; n < Argc; n++)
            {
                logFile << argNames[n] << "," << Argv[n] << std::endl;
            }
            logFile << "Distances are in mm" << std::endl;
            logFile << "image path," << inputImageName << std::endl;
            logFile << std::endl;
            logFile << "=====,Intermediate results" << std::endl;
            logFile << "topHInter," << topHorzIntercept << ",leftVInter," << leftVertIntercept << ",rightVInter," << rightVertIntercept << std::endl;
            logFile << "theta," << theta << ",topCofHBar," << topCenterOfHBar << ",botCofVBar," << botCenterOfVbar << ",hshift," << hshift << ",vshift," << vshift << std::endl;
            logFile << "=====,Statistics" << std::endl;
            logFile << ",Ave,Std,Count" << std::endl;
            logFile << "leftInterceptStat," << leftInterceptStat.Ave() << "," << leftInterceptStat.Std() << "," << leftInterceptStat.Count() << std::endl;
            logFile << "leftThetaXStat," << leftThetaXStat.Ave() << "," << leftThetaXStat.Std() << "," << leftThetaXStat.Count() << std::endl;
            logFile << "rightInterceptStat," << rightInterceptStat.Ave() << "," << rightInterceptStat.Std() << "," << rightInterceptStat.Count() << std::endl;
            logFile << "rightThetaXStat," << rightThetaXStat.Ave() << "," << rightThetaXStat.Std() << "," << rightThetaXStat.Count() << std::endl;
            logFile << "topInterceptStat," << topInterceptStat.Ave() << "," << topInterceptStat.Std() << "," << topInterceptStat.Count() << std::endl;
            logFile << "topThetaXStat," << topThetaXStat.Ave() << "," << topThetaXStat.Std() << "," << topThetaXStat.Count() << std::endl;
            if (botInterceptStat.Count() > 0)
            {
                logFile << "botInterceptStat," << botInterceptStat.Ave() << "," << botInterceptStat.Std() << "," << botInterceptStat.Count() << std::endl;
                logFile << "botThetaXStat," << botThetaXStat.Ave() << "," << botThetaXStat.Std() << "," << botThetaXStat.Count() << std::endl;
            }

            logFile << "=====,Hough Line information (vert then horz)" << std::endl;
            for (int n = 0; n < logOutput.size(); n++)
                logFile << logOutput[n] << std::endl;
            logFile << std::endl;

            logFile << "\n===,vertLinePairs\n";
            logFile << "Index1,Votes1,Intercept1,Index2,Votes2,Intercept2,Delta" << std::endl;
            for (int n = 0; n < vertLinePairs.size(); n++)
            {
                logFile << vertLinePairs[n].Index1 << ","
                    << vertLinePairs[n].Votes1 << ","
                    << vertLinePairs[n].Intercept1 << ","
                    << vertLinePairs[n].Index2 << ","
                    << vertLinePairs[n].Votes2 << ","
                    << vertLinePairs[n].Intercept2 << ","
                    << vertLinePairs[n].Delta << std::endl;
            }
            logFile << "\n===,horzLinePairs\n";
            logFile << "Index1,Votes1,Intercept1,Index2,Votes2,Intercept2,Delta" << std::endl;
            for (int n = 0; n < horzLinePairs.size(); n++)
            {
                logFile << horzLinePairs[n].Index1 << ","
                    << horzLinePairs[n].Votes1 << ","
                    << horzLinePairs[n].Intercept1 << ","
                    << horzLinePairs[n].Index2 << ","
                    << horzLinePairs[n].Votes2 << ","
                    << horzLinePairs[n].Intercept2 << ","
                    << horzLinePairs[n].Delta << std::endl;
            }
            logFile.close();
        }
        exit(0);
    }
    else
    {
        std::cout << "Could not find all the edges" << std::endl;
        std::cout << std::boolalpha << "Found Vert edges: " << foundVLines << ", found Top Horz edge: " << foundTopHLine
            << ", found Bot Horz edgd: " << foundBotHLine << std::endl;
        if (outputFiles)
        {
            logFile << "Could not find all the edges" << std::endl;
            logFile << "==========" << std::endl;
            for (int n = 0; n < Argc; n++)
            {
                logFile << Argv[n] << std::endl;
            }
            logFile << inputImageName << std::endl;
            logFile << "==========" << std::endl;
            for (int n = 0; n < logOutput.size(); n++)
                logFile << logOutput[n] << std::endl;
            logFile.close();
        }
        exit(-11);
    }

}