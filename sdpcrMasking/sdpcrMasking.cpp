/////////////////////////////////////////////////////////////////////////////////////////
//
// sdpcrMaskSizing
//
// Program inputs an image of device and identifies blobs in the image which resemble the
// wells which are expected to be present.  It then places a mask on the image.  The mask
// is created from an image taken of the device in the same position, but with a different
// color.  Each blob in the input image which is masked by the mask image is considered a 
// droplet in a well and is assigned the same array/section/row/column as the mask region.
//  
// The columns are parallel to the direction of flow in the device. Each image is typically
// of only a portion of the device, and is referred to as a subimage.
// 
// sdpcrMaskSizing <Input Image Name> <Transpose Image?> <Use Median Filter?>
//		<Prefix> <Filter Label> <SubImage Label> <Mask Image Base Name>
//		<Output Base Name>  <X Color Shift>  <Y Color Shift> [<Track Program>]
//
// The arguments on the command line are (string arguments cannot contain spaces)
//
// <Input Image Name> Name of image being analyzed
// <Transpose Image?> - Program transposes image (interchanges rows and columns) if non zero.
// <Use Median Filter?> - Program applies median filter with 3x3 kernel to input image
// <Prefix> - The program reads files with information about the device.  The names of all of
//           template files start with <Prefix>.  If the template files are in a different directory, then
//           the prefix must include the path.
// <Filter Label> - S for Before images, Q for after images.  Allows for different filter criteria for
//           the two types of images. Combined with <Prefix> to obtain filter file name.
// <SubImage Label> Typically a number from 1 to the number of camera positions for imaging. Combine
//          with <Prefix> to obtain SubImage file name.
// <Mask Image Base Name> BaseName associated with the mask image
// <Output Base Name> - All of the output files start with BaseName. Does not affect any output sent to std::clog. 
// <X Color Shift> - Amount of horizontal shift the maskimage before applying to input image.  To account for 
//			any systematic shift due to the different orientation of the LEDs
// <Y Color Shift> - Same as <X Color Shift> for vertical direction
// <Track Program> - If present and non zero, additional information is output.
//
// The program will input the following mask files
//		<Mask Image BaseName>_110Fill.csv
//		<Mask Image BaseName>_113F_Msk.png
//		<Mask Image BaseName>_114Cont.png
//
// Output files
//	<Output Base Name>_cout.csv is a copy of the output sent to std::cout. It is generated just before the 
//		program exits so its prescence could be used as an indicator that the program has finished.
//	<Output Base Name>_300.txt is a log file for the program
//  <Output Base Name>_112Fill.png is a color image where the background subtrated input image is overlaid with 
//		blue or green to indicate blobs which blobs were considered acceptable.  The color intensity is proportional
//		to the gray level of the background subtracted image.  Blue is used for blobs which pass all the criteria
//		for a blob and green is used for blobs which are somewhat too large or small to be accepted.  Other blobs,
//		including those which could not be located on a well are not overlaid.
//
// ===== The following four files are used by the quantitation program. =====
//	<Output Base Name>_006BckIm8U.png is an 8 bit background subtracted image. If the input image was originally 
//		a 16 bit image, then the values are scaled to fit. 
//	<Output Base Name>_110Fill.csv contains information about each of the blobs.
//		The quantitation program uses the filling.csv files from the anslysis of the before and after images
//		to determine the occupancy of the wells. The filling info comes from the _110Fill.csv file obtained
//		from the analysis of the mask image. This file is used by the quantitation program.
//  <Output Base Name>_113F-Msk.png  A 16 bit mask. The pixels of each blob have a value equal to its 
//		index number in the 310Fill.csv file. This image is used by the quantitation program.
//  <Output Base Name>_114Cont.png  A 16 bit mask.  This is similar to the 113F-Msk.png file, except that in this mask, only the
//		outline is marked.  This image is used by the quantitation program.
//
//
// Additional output is generated if <Track Program> is present and greater than 0.  Some additional information
// is written into the log file and the followind extra files are created.
//
// <Tracking Value> == 1 (track.OutputExtraImages)
//  <Output Base Name>_001Rotated.png is the input image after being rotated (only output if the image is rotated)
//	<Output Base Name>_303RB-Bck.png is the background removed by rolling ball method.
//	<Output Base Name>_304RB-SubIm.png is the image after the the rolling ball method.
//	<Output Base Name>_305BckSubIm.png is image after rolling ball and flat background subtracted.  
//		Unlike <Output Base Name>_306BckIm8U.png which is always 8 bit, <Output Base Name>_305BckSubIm.png 
//		will have the same bit depth as the input image.
//  <Output Base Name>_307TotBck.png is the total background (includes rolling ball and flat background)
//  <Output Base Name>_308FG-Msk.png is is a mask of the regions identified as part of the foreground by 
//		ExtractBlobsAdaptive()
//  <Output Base Name>_315_Overlay.png overlays the _113F_Msk onto the _312Fill.png and colors the pixels
//		which are non zero in both images.  _315_Overlay.png is a color image, where cValue[0] and cValue[1]
//		have the value they do in _312Fill.png and cValue[2] is set to 126. 
//  <Output Base Name>_InputMask_320.png is the input mask image
//  <Output Base Name>_ShiftedMask_321.png is the shifted mask image
//
// <Tracking Value> == 2 (track.OutputExtraCSV)
//  <Output Base Name>_325_PixelInten.csv  contains the pixel locations and intensities for some of the blobs
//  <Output Base Name>_324_MskAlign.csv contains information about the locations of some of the bounding boxes
//		from the mask and how they align with the blobs in the input image.
//
// The numbering scheme for the file names has many files with a number between 300 and 399 in its name.  The
// important exceptions are the four files used in quantitation (006, 110, 113 and 114).  The quantitation program
// derives their name by appending suffixes to the <Output File BaseName>.  For consistency, the suffixes (which 
// includes the number), must match the suffixes used by the sdpcrSizingAnalysis program. The files contain the
// same type of information about the image regardless of whether sdpcrMaskSizing or sdpcrSizingAnalysis is used.
// By using the same suffixes, the quantitation program does not need to know which progra created the files.
// The other two exceptions are _001Rotated.png and _112Fill.png.  The 001 image is generated by the same procedure
// in both sizing programs and so its output always uses the same suffix.  The 112 image is often examined in 
// concert with the 110 file, and so the numbering was not changed so as to keep the files grouped together when
// the directory is sorted by name.
//
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "physicalconstants.h"
#include "statistics.hpp"
#include "blob.hpp"
#include "readinfofile.hpp"
#include "calccentroid.hpp"
#include "calcperimeter.hpp"
#include "blobSizing.hpp"
#include "wellExclusionRules.hpp"
#include "dropletMatch.hpp"
#include "rollingball32F.hpp"
#include "calcAdaptiveThreshold.hpp"
#include "extractBlobsAdaptive.hpp"
#include "trimRectangle.hpp"
#include "outputMaskingResults.hpp"
#include "median_Rotate.hpp"
#include "trimDirectoryString.hpp"
#include "types.hpp"
#include "trackOutput.hpp"
//! [includes] 	

//! [namespace]
// using namespace std;
//using namespace cv;
//! [namespace]
void ShowUsage(const char* msg, const char* exe);
//int main(std::string workingDirectory, std::string templateDirectory,
//	std::string imageFileName, std::string templatePrefix, std::string filterLabel, std::string subImageLabel,
//	std::string baseNameInput, std::string baseNameOutput, int colorShiftX, int colorShiftY, int dilation,
//	bool rotateImage, bool useMedianFilter, unsigned int extraOutput)
int main(int Argc, char* Argv[])
{
	std::string err = "";
	int numbRequiredArgs = 15;
	if (Argc < numbRequiredArgs)
	{
		ShowUsage("Too few arguments", Argv[0]);
		exit(0);
	}
	//! [load]
	//  defaults
	// std::string bckSuffix("_306BckIm8U.png");
	//std::string imageName("SadFish.png"); 
	//bool rotateImage = false;
	//bool useMedianFilter = false;
	TrackAnalysis track;
	//unsigned int extraOutput = 0;
	std::string infoMsg;

	std::string workingDirectory;
	std::string templateDirectory;
	std::string imageFileName;
	std::string templatePrefix;
	std::string filterLabel;
	std::string subImageLabel;
	std::string baseNameInput;
	std::string baseNameOutput;
	int colorShiftX;
	int colorShiftY;
	int dilation;
	bool rotateImage;
	bool useMedianFilter;
	unsigned int extraOutput;




	//cv::Point ColorShift;
	//int dilation = 1;
	std::string csvSuffix("_110Fill.csv");
	std::string mskSuffix("_113F_Msk.png");
	std::string cntSuffix("_114Cont.png");
	std::string inputSizingFileName("PeculiarPenguins.csv");
	std::string inputMaskName("AntiSocialAnts.png");
	std::string inputContourMaskName("SadFish.png");

	int ia = 0;
	int ie = 0;
	std::istringstream token;
	{
		// double temp;
		int itemp;
		std::string stemp;
		workingDirectory.assign(Argv[++ia]);
		templateDirectory.assign(Argv[++ia]);
		imageFileName.assign(Argv[++ia]);
		templatePrefix.assign(Argv[++ia]);
		filterLabel.assign(Argv[++ia]);
		subImageLabel.assign(Argv[++ia]);
		baseNameInput.assign(Argv[++ia]);
		baseNameOutput.assign(Argv[++ia]);
		token.clear(); token.str(Argv[++ia]); token >> itemp; if (token.fail() ) { ++ie; }
			else { colorShiftX = itemp; }
		token.clear(); token.str(Argv[++ia]); token >> itemp; if (token.fail()) { ++ie; }
			else { colorShiftY = itemp; }
		token.clear(); token.str(Argv[++ia]); token >> itemp; if (token.fail() || itemp < 0) { ++ie; }
			else { dilation = itemp; }
		token.clear(); token.str(Argv[++ia]); token >> itemp; if (token.fail()) { ++ie; }
			else { if (itemp == 0) rotateImage = false; else rotateImage = true; }
		token.clear(); token.str(Argv[++ia]); token >> itemp; if (token.fail()) { ++ie; }
			else { if (itemp == 0) useMedianFilter = false; else useMedianFilter = true; }
		token.clear(); token.str(Argv[++ia]); token >> itemp; if (token.fail() || itemp < 0) { ++ie; }
			else { extraOutput = itemp; }
	}

	workingDirectory = trimDirectoryString(workingDirectory);
	templateDirectory = trimDirectoryString(templateDirectory);
	std::string prefix = templateDirectory + "\\" + templatePrefix;
	std::string imageName = workingDirectory + "\\" + imageFileName;
	std::string inputBaseName = workingDirectory + "\\" + baseNameInput;
	std::string outputBaseName = workingDirectory + "\\" + baseNameOutput;


	int numbArrays;
	int numbSections;
	// double wellVolume = 6.5;  // value is in nL

	cv::Size2f wellSize2f;
	cv::Point2f wellSpacing;
	cv::Point ColorShift = cv::Point(colorShiftX, colorShiftY);
	double wellArea;

	cv::Vec3b cValue;
	std::ostringstream oString;
	std::ofstream logFile;
	std::ofstream coutFile;
	std::ofstream outFile;
	// std::ostringstream hString1;
	// std::ostringstream hString2;
	// std::ostringstream hString3;

	std::string outName;
	outName = outputBaseName;
	outName.append("_300.txt");
	logFile.open(outName.c_str());

	if (!logFile.is_open())
	{
		oString.str("");
		oString.clear();
		oString << "Problems opening log file: " << outName;
		std::cerr << oString.str() << std::endl;
		return -1;
	}

	std::string infoFileName = prefix;
	infoFileName.append("_InfoFile.txt");

	std::string subImageFileName = prefix;
	subImageFileName.append("_SubImageInfo_");
	subImageFileName.append(subImageLabel.c_str());
	subImageFileName.append(".txt");

	std::string parameterFileName = prefix;
	parameterFileName.append("_Filter_");
	parameterFileName.append(filterLabel.c_str());
	parameterFileName.append(".txt");

	inputSizingFileName = inputBaseName;
	inputSizingFileName.append(csvSuffix);

	inputMaskName = inputBaseName;
	inputMaskName.append(mskSuffix);

	inputContourMaskName = inputBaseName;
	inputContourMaskName.append(cntSuffix);

	logFile << "========== Blob Masking Analysis ==========\n";
	logFile << "Image Name: " << imageName.c_str();
	if (rotateImage)
		logFile << " (image is trsnsposed after input)";
	else
		logFile << "";
	if (useMedianFilter)
		logFile << " (Median Filter with 3x3 kernel is applied to image)\n";
	else
		logFile << "/n";
	logFile << "Output Base Name: " << outputBaseName << "\n";
	logFile << "Color Shift (x,y) (" << ColorShift.x << ", " << ColorShift.y << ")\n";
	logFile << "Info File Name: " << infoFileName << "\n";
	logFile << "SubImage Info File Name: " << subImageFileName << "\n";
	logFile << "Input Sizing File Name: " << inputSizingFileName << "\n";
	logFile << "Input Sizing Mask Image Name: " << inputMaskName << "\n";
	logFile << "Input Sizing Contour Image Name: " << inputContourMaskName << "\n";
	track.Load(extraOutput, infoMsg);
	logFile << infoMsg;

	cv::Mat image = cv::imread(imageName.c_str(), cv::IMREAD_GRAYSCALE | cv::IMREAD_ANYDEPTH); // Read the file
	cv::Size imageSize = image.size();
	unsigned int type = image.depth();
	uchar depth = type & CV_MAT_DEPTH_MASK;
	uchar chans = 1 + (type >> CV_CN_SHIFT);
	bool eightBit;
	bool shiftFour = false;
	std::string errorMsg;
	if (depth == CV_8U && chans == 1)
	{
		eightBit = true;
		logFile << "\n" << imageName << " is of type CV_8UC1\n";
	}
	else if ((depth == CV_16U) && (chans == 1))
	{
		eightBit = false;
		logFile << "\n" << imageName << " is of type CV_16UC1";
		for (int n = 0; n < image.total(); n++)
		{
			unsigned int ntmp = image.at<short unsigned int>(n);
			image.at<short unsigned int>(n) = (ntmp >> 4);
		}
	}
	else
	{
		oString.str("");
		oString.clear();
		oString << "Problems opening image: " << imageName << " Image is of type " << type2str(image.depth()) << " which program cannot handle.";
		err = oString.str();
		std::cerr << err << std::endl;
		logFile << err << std::endl;
		logFile.close();
		return -1;
	}
	cv::Mat workImage1;
	cv::Mat workImage2;
	cv::Mat workImage3;
	cv::Mat workImage4;
	cv::Mat inputMask;
	cv::Mat shiftedMask;
	cv::Mat shiftedContourMask;
	inputMask = cv::imread(inputMaskName.c_str(), cv::IMREAD_ANYDEPTH); // Read the file
	if (inputMask.empty())					  // Check for invalid input
	{
		err = "Could not open or find " + inputMaskName;
		std::cerr << err << std::endl;
		logFile << err << std::endl;
		logFile.close();
		return -1;
	}
	workImage2 = cv::imread(inputContourMaskName.c_str(), cv::IMREAD_ANYDEPTH); // Read the file
	if (workImage2.empty())					  // Check for invalid input
	{
		err = "Could not open or find " + inputContourMaskName;
		std::cerr << err << std::endl;
		logFile << err << std::endl;
		logFile.close();
		return -1;
	}
	if (inputMask.cols != workImage2.cols || inputMask.rows != workImage2.rows)
	{
		oString.str("");
		oString.clear();
		oString << inputMaskName << " and " << inputContourMaskName << " must be the same size.";
		err = oString.str();
		std::cerr << err << std::endl;
		logFile << err << std::endl;
		logFile.close();
		return -1;
	}
	std::vector< int > clippedBlobs;
	clippedBlobs.clear();
	// std::cout << "clippedBlobs\n";
	// std::cout << "color shift: " << colorShift << std::endl;
	if (ColorShift.x == 0 && ColorShift.y == 0)
	{
		inputMask.copyTo(shiftedMask);
		workImage2.copyTo(shiftedContourMask);
	}
	else
	{
		shiftedMask.create(inputMask.size(), CV_16U);
		shiftedContourMask.create(workImage2.size(), CV_16U);
		shiftedMask.setTo(0);
		shiftedContourMask.setTo(0);
		cv::Rect rect1;
		cv::Rect rect2;
		if (ColorShift.x < 0)
		{
			rect1.x = -ColorShift.x;
			rect2.x = 0;
			rect1.width = inputMask.cols + ColorShift.x;
			rect2.width = rect1.width;
		}
		else if (ColorShift.x > 0)
		{
			rect1.x = 0;
			rect2.x = ColorShift.x;
			rect1.width = inputMask.cols - ColorShift.x;
			rect2.width = rect1.width;
		}
		else
		{
			rect1.x = 0;
			rect2.x = 0;
			rect1.width = inputMask.cols;
			rect2.width = rect1.width;
		}

		if (ColorShift.y < 0)
		{
			rect1.y = -ColorShift.y;
			rect2.y = 0;
			rect1.height = inputMask.rows + ColorShift.y;
			rect2.height = rect1.height;
		}
		else if (ColorShift.y > 0)
		{
			rect1.y = 0;
			rect2.y = ColorShift.y;
			rect1.height = inputMask.rows - ColorShift.y;
			rect2.height = rect1.height;
		}
		else
		{
			rect1.y = 0;
			rect2.y = 0;
			rect1.height = inputMask.rows;
			rect2.height = rect1.height;
		}
		// std::cout << "rect1/rect2: " << rect1 <<  "/ " << rect2 << std::endl;
		inputMask(rect1).copyTo(shiftedMask(rect2));
		// std::cout << "  nonZero input/shifted " << cv::countNonZero(inputMask) << " / " << cv::countNonZero(shiftedMask) << std::endl;
		workImage2(rect1).copyTo(shiftedContourMask(rect2));
		if (track.OutputExtraImages)
		{
			outName = outputBaseName;
			outName.append("_310_InputMask.png");
			cv::imwrite(outName, inputMask);
			// outName = outputBaseName;
			// outName.append("_ShiftedMask_306.png");
			// cv::imwrite( outName, shiftedMask );	
			// outName = outputBaseName;
			// outName.append("_InputContours_307.png");  
			// cv::imwrite( outName, workImage2 );
			// outName = outputBaseName;
			// outName.append("_ShiftedContours_308.png");
			// cv::imwrite( outName, shiftedContourMask );
		}

		clippedBlobs.push_back(-1);
		int cB = 0;
		int last;
		for (int r = 0; r < shiftedMask.rows; r++)
		{
			int tmp = shiftedMask.at<unsigned short int>(cv::Point(0, r));
			if (tmp != 0)
			{
				if (clippedBlobs[cB] != tmp)
				{
					clippedBlobs.push_back(tmp);
					cB++;
				}
			}
		}
		last = shiftedMask.cols - 1;
		for (int r = 0; r < shiftedMask.rows; r++)
		{
			int tmp = shiftedMask.at<unsigned short int>(cv::Point(last, r));
			if (tmp != 0)
			{
				if (clippedBlobs[cB] != tmp)
				{
					clippedBlobs.push_back(tmp);
					cB++;
				}
			}
		}
		for (int c = 1; c < shiftedMask.cols - 1; c++)
		{
			int tmp = shiftedMask.at<unsigned short int>(cv::Point(c, 0));
			if (tmp != 0)
			{
				if (clippedBlobs[cB] != tmp)
				{
					clippedBlobs.push_back(tmp);
					cB++;
				}
			}
		}
		last = shiftedMask.rows - 1;
		for (int c = 1; c < shiftedMask.cols - 1; c++)
		{
			int tmp = shiftedMask.at<unsigned short int>(cv::Point(c, last));
			if (tmp != 0)
			{
				if (clippedBlobs[cB] != tmp)
				{
					clippedBlobs.push_back(tmp);
					cB++;
				}
			}
		}
		if (clippedBlobs.size() > 0)
		{
			clippedBlobs.erase(clippedBlobs.begin(), clippedBlobs.begin() + 1);
		}

	}
	// std::cout << std::endl;
// Open and read Device Info File (infoFileName), which was output by createmasks and modified by cropinfofile.
// It contains information about the arrays, sections, and guards which might be included in
// image
	std::vector< DeviceOrigins > sectionOrigins;
	std::vector< cv::Size > sectionDimens;
	std::vector< DeviceOrigins > arrayOrigins;
	std::vector< GuardInfo > xGuards;
	std::vector< GuardInfo > yGuards;
	if (!ReadDeviceInfo(infoFileName,
		arrayOrigins,
		sectionOrigins, sectionDimens,
		xGuards, yGuards,
		errorMsg))
	{
		oString.str("");
		oString.clear();
		oString << "Error from ReadDeviceInfo: " << errorMsg;
		err = oString.str();
		std::cerr << err << std::endl;
		logFile << err << std::endl;
		logFile.close();
		return -1;
	}
	numbArrays = (int)arrayOrigins.size();
	numbSections = (int)sectionOrigins.size();
	// logFile << "arrayOrigins.size() = " << numbArrays << "\n";
	// logFile << "sectionOrigins.size() = " << numbSections << "\n";
	std::vector<ArraySection> arrSecList;
	double blockSizeMult;
	double minimumPeak;
	double maxFillingRatio;
	int numbArraysUsed;
	cv::Rect imageAnalysisArea;
	cv::Rect subImageArea;
	std::vector< ParameterLimit > pLimits;
	if (!ReadSubImageData(subImageFileName, numbArrays, arrSecList,
		numbArraysUsed, imageAnalysisArea, subImageArea,
		wellSize2f, wellArea, wellSpacing,
		pLimits, blockSizeMult, minimumPeak, maxFillingRatio,
		errorMsg))
	{
		oString.str("");
		oString.clear();
		oString << "Error from ReadSubImageData: " << errorMsg;
		err = oString.str();
		std::cerr << err << std::endl;
		logFile << err << std::endl;
		logFile.close();
		return -1;
	}
	int blockSize = (int)floor(wellSize2f.height * blockSizeMult);
	if (blockSize % 2 == 0)
		blockSize++;
	logFile << "\nNumber of Arrays: " << numbArrays << "\n";
	logFile << "Number of Sections: " << numbSections << std::endl;
	std::vector< std::vector< int > > arrSecMatrix;
	arrSecMatrix.resize(numbArrays + 1);
	for (int n = 0; n < (int)arrSecMatrix.size(); n++)
	{
		arrSecMatrix[n].resize(numbSections + 1);
		for (int m = 0; m < (int)arrSecMatrix[n].size(); m++)
			arrSecMatrix[n][m] = 0;
	}
	for (int n = 0; n < (int)arrSecList.size(); n++)
		arrSecMatrix[arrSecList[n].Array][arrSecList[n].Section] = 1;

	AnalysisParameters fParams;
	if (!fParams.ReadFile(parameterFileName, wellArea, wellSize2f, errorMsg))
	{
		oString.str("");
		oString.clear();
		oString << "Error from fParams.ReadFile: " << errorMsg;
		err = oString.str();
		std::cerr << err << std::endl;
		logFile << err << std::endl;
		logFile.close();
		return -1;
	}
	// fParamsSmall = fParams;
	// fParamsSmall.BlobArea[0] *= 0.80;
	int rollingBallRadius = (int)round(fParams.RollingBallRadius);
	double backgroundFrac = fParams.BackgroundFrac;
	std::vector< BlobSummary > inputBlobList;
	std::vector< BlobSummary > blobList;
	std::vector< double > inputResults;
	double inputMinRingAve;
	double inputMaxFillingRatio;
	int inputNumb;
	MinMaxGroup inputParameters;

	if (!ReadSizingFile(inputSizingFileName, inputBlobList, inputNumb, inputResults, inputMinRingAve, inputMaxFillingRatio, inputParameters, errorMsg))
	{
		oString.str("");
		oString.clear();
		oString << "Problems opening / reading input sizing file : " << inputSizingFileName << "\n" << errorMsg;
		err = oString.str();
		std::cerr << err << std::endl;
		logFile << err << std::endl;
		logFile.close();
		return -1;
	}
	logFile << "\n     From " << subImageFileName << "\n";
	logFile << "Rolling Ball Radius (Background subtraction): " << rollingBallRadius << "\n";
	logFile << "Background Fraction: " << backgroundFrac << "\n";
	logFile << "Chamber Size: (" << wellSize2f.width << ", " << wellSize2f.height << ") (width, height)\n";
	logFile << "Chamber Spacing: (" << wellSpacing.x << ", " << wellSpacing.y << ") (x,y)\n";
	// logFile << "Chamber Volume: (" << wellVolume << ") (nL)\n";
	for (int n = 0; n < (int)pLimits.size(); n++)
	{
		logFile << "Initial Value and Limit for variable parameter " << n << ", [" << pLimits[n].BaseValue
			<< ", " << pLimits[n].Limit << "]\n";
	}
	logFile << "Block Size Mult: " << blockSizeMult << "\n";
	logFile << "Minimum Peak Height of Blob: " << minimumPeak << "\n";
	logFile << "Max Filling Ratio for Blob: " << maxFillingRatio << "\n";
	logFile << std::endl;
	logFile << "Number of Arrays Used in this sub-image: " << numbArraysUsed << "\n";
	logFile << "\n";
	MedianFilter_RotateImage(workImage1, image, useMedianFilter, rotateImage,
		track.OutputExtraImages, eightBit, outputBaseName);

	std::vector< cv::Vec3b > colorScale;
	// int maxColorScale = SetupColorScale(colorScale);
	cv::Mat image32F(imageSize, CV_32F);		// Image converted to float
	cv::Mat backgroundSubImage32F(imageSize, CV_32F);  // This contains image with rolling ball background subtracted
	cv::Mat background32F(imageSize, CV_32F);	// This is the rolling ball background

	image.convertTo(image32F, CV_32F, 1.0);
	double minV;
	double maxV;
	cv::Point min_loc;
	cv::Point max_loc;
	if (track.OutputExtraCSV)
	{
		cv::minMaxLoc(image32F, &minV, &maxV, &min_loc, &max_loc);
		logFile << "Min Value in image32F is " << minV << ", at (" << min_loc.x << ", " << min_loc.y << ") (x,y)\n";
		logFile << "Max Value in image32F is " << maxV << ", at (" << max_loc.x << ", " << max_loc.y << ") (x,y)\n";
	}
	bool res3 = SubtractBackground32F(image32F, rollingBallRadius, backgroundSubImage32F, background32F);

	if (!res3)
	{
		logFile << "\nError subtracting background\n\n";
	}

	if (track.OutputExtraCSV)
	{
		cv::minMaxLoc(backgroundSubImage32F, &minV, &maxV, &min_loc, &max_loc);
		logFile << "Min Value in backgroundSubImage32F is " << minV << ", at (" << min_loc.x << ", " << min_loc.y << ") (x,y)\n";
		logFile << "Max Value in backgroundSubImage32F is " << maxV << ", at (" << max_loc.x << ", " << max_loc.y << ") (x,y)\n";
	}
	if (track.OutputExtraImages)
	{
		int tot = (int)image.total();
		if (eightBit)
			workImage1.create(imageSize, CV_8UC1);
		else
			workImage1.create(imageSize, CV_16UC1);

		if (eightBit)
		{
			for (int m = 0; m < tot; m++)
				workImage1.at<uchar>(m) = (uchar)round(background32F.at<float>(m));
			//workImage1.at<uchar>(m) = (short unsigned int)round( background32F.at<float>(m) );
		}
		else
		{
			for (int m = 0; m < tot; m++)
				workImage1.at<short unsigned int>(m) = (short unsigned int)round(background32F.at<float>(m));
		}
		outName = outputBaseName;
		outName.append("_303RB-Bck.png");
		cv::imwrite(outName, workImage1);

		workImage2 = cv::max(backgroundSubImage32F, 0);
		if (eightBit)
		{
			workImage2.convertTo(workImage1, CV_8U);
		}
		else
		{
			workImage2.convertTo(workImage1, CV_16U);
		}
		outName = outputBaseName;
		outName.append("_304RB-SubIm.png");
		cv::imwrite(outName, workImage1);
	}

	int nSeg = 10;
	double invNSeg = 1.0 / (double)nSeg;
	int w8 = (int)floor(imageSize.width * invNSeg);
	int h8 = (int)floor(imageSize.height * invNSeg);
	int w0;
	int h0;
	if (imageSize.width - w8 * nSeg <= 1)
		w0 = 0;
	else
		w0 = (imageSize.width - w8 * nSeg) / 2;

	if (imageSize.height - h8 * nSeg <= 1)
		h0 = 0;
	else
		h0 = (imageSize.height - h8 * nSeg) / 2;

	int tNumb = w8 * h8;
	Statistics1 rollingBallBckInten;
	rollingBallBckInten.Clear();
	Statistics1 regBckInten;
	regBckInten.Clear();
	double sum;
	for (int w = 0; w < nSeg; w++)
	{
		for (int h = 0; h < nSeg; h++)
		{
			sum = 0.0;
			int wn = w0 + (w * w8);
			int hn = h0 + (h * h8);
			for (int i = 0; i < w8; i++)
			{
				for (int j = 0; j < h8; j++)
				{
					sum += background32F.at<float>(j + hn, i + wn);
					rollingBallBckInten.Accumulate(background32F.at<float>(j + hn, i + wn));
				}
			}
			sum /= tNumb;
			regBckInten.Accumulate(sum);
		}
	}
	rollingBallBckInten.Analyze();
	regBckInten.Analyze();
	background32F.release();

	logFile << "\nBackground subtraction analysis.  Rolling Ball Radius = " << rollingBallRadius << "\n";
	logFile << "     Subtracted Background = " << rollingBallBckInten.Ave() << " (" << rollingBallBckInten.Std() << ").  Count = " << rollingBallBckInten.Count() << std::endl;
	logFile << "     Region Backgrounds = " << regBckInten.Ave() << " (" << regBckInten.Std() << ").  Count = " << regBckInten.Count() << std::endl;

	std::vector< double > imageInten;
	imageInten.resize(backgroundSubImage32F.total());
	for (int n = 0; n < backgroundSubImage32F.total(); n++)
		imageInten[n] = backgroundSubImage32F.at<float>(n);
	sort(imageInten.begin(), imageInten.end());
	Statistics1 bckInten;
	bckInten.Clear();
	for (int n = 0; n < (int)imageInten.size() * backgroundFrac; n++)
		bckInten.Accumulate(imageInten[n]);

	bckInten.Analyze();
	double bck0 = bckInten.Ave();
	double std0 = bckInten.Std();
	double bLimit = bck0 + STDMULTFORFIRSTBACKGNDCALC * std0;
	logFile << "First Background calculation, Count0 = " << bckInten.Count() << ", bck0 = "
		<< bck0 << ", std0 = " << std0 << ", bLimit = " << bLimit << std::endl;

	bckInten.Clear();
	int nn = 0;
	while (imageInten[nn] < bLimit)
	{
		bckInten.Accumulate(imageInten[nn]);
		nn++;
		if (nn >= (int)imageInten.size())
			break;
	}
	double maxInten = imageInten[(int)imageInten.size() - 1];
	logFile << "initial maxInten = " << maxInten << "\n";
	bckInten.Analyze();
	double bck1 = bckInten.Ave();
	double std1 = bckInten.Std();

	logFile << "Second Background calculation, Count1 = " << bckInten.Count()
		<< ", bck1 = " << bck1 << ", std1 = " << std1 << std::endl;

	backgroundSubImage32F -= bck1;
	backgroundSubImage32F = cv::max(backgroundSubImage32F, 0.0);
	if (track.OutputExtraImages)
	{
		workImage1 = image32F - backgroundSubImage32F;
		workImage1.convertTo(workImage2, CV_8U);
		outName = outputBaseName;
		outName.append("_307TotBck.png");
		cv::imwrite(outName, workImage2);
	}
	image32F.release();
	int contourLevel = 255;
	if (eightBit)
	{
		workImage2 = cv::min(backgroundSubImage32F, 255.0);
		workImage2.convertTo(workImage1, CV_8U);
	}
	else
	{
		workImage2 = cv::min(backgroundSubImage32F, 65535);
		workImage2.convertTo(workImage1, CV_16U);

		int iTmp = (int)((double)imageInten.size() * 0.90);
		if (iTmp > 0)
			contourLevel = (int)(1.6 * imageInten[iTmp]);

		if (contourLevel > 65535)
			contourLevel = 65535;
	}

	std::string rtype = type2str(workImage1.type());
	// cv::Size backgroundSubImageSize = backgroundSubImage32F.size();

	if (track.OutputExtraCSV)
	{
		logFile << "contourLevel = " << contourLevel << "\n";
		cv::minMaxLoc(backgroundSubImage32F, &minV, &maxV, &min_loc, &max_loc);
		logFile << "Min Value in backgroundSubImage32F (2) is " << minV << ", at (" << min_loc.x << ", " << min_loc.y << ") (x,y)\n";
		logFile << "Max Value in backgroundSubImage32F (2) is " << maxV << ", at (" << max_loc.x << ", " << max_loc.y << ") (x,y)\n";
	}
	if (track.OutputExtraImages)
	{
		outName = outputBaseName;
		outName.append("_305BckSubIm.png");
		cv::imwrite(outName, workImage1);
	}

	std::vector< int > histogram;
	std::vector< double > wgtHistogram;
	double adaptiveC;
	// timeMsg = "Before Calc Threshold";
	// OutputTime(timeMsg);
	cv::Mat foreground8U;
	cv::Mat backgroundSubImage8U;
	if (eightBit)
	{
		backgroundSubImage8U = workImage1.clone();
		logFile << " eightBit is true\n" << std::endl;
	}
	else
	{
		double min, max;
		cv::minMaxLoc(workImage1, &min, &max);
		if (max > 255.0)
		{
			double scaleF = 255.0 / max;
			workImage2 = workImage1 * scaleF;
			workImage2.convertTo(backgroundSubImage8U, CV_8U);
		}
		else
			workImage1.convertTo(backgroundSubImage8U, CV_8U);

		logFile << " eightBit is false\n" << std::endl;
	}
	rtype = type2str(backgroundSubImage8U.type());
	// std::clog << "Inside sdpcrSizingAnalysis, backgroundSubImage8U is " << rtype << std::endl;

	outName = outputBaseName;
	outName.append("_006BckIm8U.png");
	cv::imwrite(outName, backgroundSubImage8U);

	CalcAdaptiveThreshold(logFile,
		backgroundSubImage8U, fParams, 0, outputBaseName, blockSize, minimumPeak,
		adaptiveC, foreground8U, true);
	if (track.OutputExtraImages)
	{
		outName = outputBaseName;
		outName.append("_308FG-Msk.png");
		cv::imwrite(outName, foreground8U);
	}
	blobList.clear();

	for (int n = 0; n < inputBlobList.size(); n++)
	{
		bool clipped = false;
		for (int m = 0; m < clippedBlobs.size(); m++)
		{
			int idx = inputMask.at<short unsigned int>(inputBlobList[n].InputCenter2f);
			if (clippedBlobs[m] == idx)
			{
				clipped = true;
				break;
			}
		}
		if (!clipped)
		{
			if (inputBlobList[n].ShapeSummary.FilterValue == 1)
				blobList.push_back(inputBlobList[n]);
		}
	}
	Statistics1 maskedIntensities;
	std::vector< cv::Point > pList;
	cv::Rect box;
	cv::Size boxSize;
	boxSize.width = (int)round(wellSpacing.x + 4);
	boxSize.height = (int)round(wellSpacing.y + 4);
	if (boxSize.width % 2 == 0)
	{
		if (boxSize.width > wellSpacing.x + 4)
			boxSize.width--;
		else
			boxSize.width++;
	}
	if (boxSize.height % 2 == 0)
	{
		if (boxSize.height > wellSpacing.y + 4)
			boxSize.height--;
		else
			boxSize.height++;
	}
	cv::Point offset;
	offset.x = (boxSize.width - 1) / 2;
	offset.y = (boxSize.height - 1) / 2;
	cv::Point center;
	logFile << "boxSize: " << boxSize << std::endl;
	Statistics1 totalBlobIntensities;
	Statistics1 aveBlobIntensities;
	totalBlobIntensities.Clear();
	aveBlobIntensities.Clear();
	logFile << "inputBlobList.size() = " << inputBlobList.size() << std::endl;
	logFile << "blobList.size() = " << blobList.size() << std::endl;
	if (track.OutputExtraCSV)
	{
		outName = outputBaseName;
		outName.append("_324_MskAlign.csv");
		outFile.open(outName.c_str());
		outFile << ",Bef Color Shft,,Aft Color Shft,,Box,,,,Blob Stats\n";
		outFile << "n,Input Center,,Input Center,,X0,Y0,Width,Height,Numb,X Ave,X Std,Y Ave,Y Std\n";
		outFile << ",x,y,x,y\n";
	}

	StatisticsXY trkStats;
	bool trk;
	cv::Mat boxRect(boxSize, CV_8U);
	cv::Mat dilatedBoxRect(boxSize, CV_8U);
	std::vector< cv::Point > boxLocations;
	cv::Mat structElement;
	if (dilation > 0)
		structElement = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(2 * dilation + 1, 2 * dilation + 1), cv::Point(-1, -1));
	else
		structElement.create(cv::Size(3, 3), CV_8U);

	for (int n = 0; n < blobList.size(); n++)
	{
		trk = track.OutputExtraCSV && (n % 40 == 0);
		cv::Point2f origCenter = blobList[n].InputCenter2f;
		blobList[n].InputCenter2f.x += ColorShift.x;
		blobList[n].InputCenter2f.y += ColorShift.y;
		blobList[n].RotatedCenter2f.x += ColorShift.x;
		blobList[n].RotatedCenter2f.y += ColorShift.y;
		blobList[n].AdjustedCenter2f.x += ColorShift.x;
		blobList[n].AdjustedCenter2f.y += ColorShift.y;
		blobList[n].DeviceCenter.x += ColorShift.x;
		blobList[n].DeviceCenter.y += ColorShift.y;
		center.x = (int)round(blobList[n].InputCenter2f.x);
		center.y = (int)round(blobList[n].InputCenter2f.y);
		if (trk)
			trkStats.Clear();

		box.x = center.x - offset.x;
		box.y = center.y - offset.y;
		bool trim = TrimRectangle(box, boxSize, shiftedMask.size());
		if (trk)
		{
			outFile << n << "," << origCenter.x << "," << origCenter.y
				<< "," << blobList[n].InputCenter2f.x << "," << blobList[n].InputCenter2f.y << ","
				<< box.x << "," << box.y << "," << box.width << "," << box.height;
		}
		blobList[n].ShapeSummary.InputCenter2f = blobList[n].InputCenter2f;
		maskedIntensities.Clear();
		int idx = shiftedMask.at<unsigned short int>(blobList[n].InputCenter2f);
		cv::Mat roi = shiftedMask(box);

		pList.clear();
		if (dilation < 1)
		{
			for (int r = 0; r < box.height; r++)
			{
				for (int c = 0; c < box.width; c++)
				{
					if (roi.at<short unsigned int>(cv::Point(c, r)) == idx)
					{
						pList.push_back(cv::Point(c + box.x, r + box.y));
						if (trk)
						{
							// logFile << "\t\t\t\t" << c << "\t" << r << "\t" << cv::Point(c+box.x, r+box.y) << "\n";
							trkStats.Accumulate(c + box.x, r + box.y);

						}
					}
				}
			}
		}
		else
		{
			boxRect.setTo(0);
			for (int r = 0; r < box.height; r++)
			{
				for (int c = 0; c < box.width; c++)
				{
					if (roi.at<short unsigned int>(cv::Point(c, r)) == idx)
					{
						boxRect.at<uchar>(cv::Point(c, r)) = 1;
					}
				}
			}
			cv::dilate(boxRect, dilatedBoxRect, structElement);
			boxLocations.clear();
			cv::findNonZero(dilatedBoxRect, boxLocations);
			for (int p = 0; p < boxLocations.size(); p++)
			{
				pList.push_back(cv::Point(boxLocations[p].x + box.x, boxLocations[p].y + box.y));
				if (trk)
				{
					// logFile << "\t\t\t\t" << c << "\t" << r << "\t" << cv::Point(c+box.x, r+box.y) << "\n";
					trkStats.Accumulate(boxLocations[p].x + box.x, boxLocations[p].y + box.y);
				}
			}
		}
		if (trk)
		{
			trkStats.Analyze();
			outFile << "," << trkStats.Count()
				<< "," << trkStats.AveX() << "," << trkStats.StdX()
				<< "," << trkStats.AveY() << "," << trkStats.StdY() << "\n";
		}
		// roi = backgroundSubImage32F(box);
		blobList[n].ShapeSummary.PixelList.resize(pList.size());
		blobList[n].ShapeSummary.PixelIntensities.resize(pList.size());
		for (int m = 0; m < pList.size(); m++)
		{
			double inten = backgroundSubImage32F.at<float>(pList[m]);
			maskedIntensities.Accumulate(inten);
			blobList[n].ShapeSummary.PixelIntensities[m] = inten;
			blobList[n].ShapeSummary.PixelList[m].x = pList[m].x;
			blobList[n].ShapeSummary.PixelList[m].y = pList[m].y;
		}
		maskedIntensities.Analyze();
		blobList[n].ShapeSummary.MaskedBlobIntensities = maskedIntensities;
		aveBlobIntensities.Accumulate(maskedIntensities.Ave());
		totalBlobIntensities.Accumulate(maskedIntensities.Sum());
		if (trk)
		{
			logFile << "For blob " << n << "\n";
			logFile << "maskedIntensities: Count " << maskedIntensities.Count()
				<< ", Intensities: " << maskedIntensities.Ave() << "(" << maskedIntensities.Std() << ") Sum = "
				<< maskedIntensities.Sum() << "\n";
		}
	}
	if (track.OutputExtraCSV)
		outFile.close();

	aveBlobIntensities.Analyze();
	totalBlobIntensities.Analyze();
	double aveThreshold1 = aveBlobIntensities.Ave() - 2.0 * aveBlobIntensities.Std();
	double aveThreshold2 = aveBlobIntensities.Ave() - 2.5 * aveBlobIntensities.Std();
	double totalThreshold1 = totalBlobIntensities.Ave() - 2.0 * totalBlobIntensities.Std();
	double totalThreshold2 = totalBlobIntensities.Ave() - 2.5 * totalBlobIntensities.Std();
	for (int n = 0; n < blobList.size(); n++)
	{
		double ave = blobList[n].ShapeSummary.MaskedBlobIntensities.Ave;
		double tot = blobList[n].ShapeSummary.MaskedBlobIntensities.Sum;
		if ((ave < aveThreshold1 && tot < totalThreshold1) || ave < aveThreshold2 || tot < totalThreshold2)
		{
			if (blobList[n].ShapeSummary.FilterValue == 1)
				blobList[n].ShapeSummary.FilterValue = -1;
		}
	}

	// logFile << "Before WriteSizingFile" << std::endl;
	oString.str("");
	oString.clear();
	oString << outputBaseName << "_110Fill.csv";
	outName = oString.str();
	WriteSizingFile(blobList, inputResults,
		inputMinRingAve, inputMaxFillingRatio, fParams, 1, backgroundSubImage32F,
		outName, workImage1);

	oString.str("");
	oString.clear();
	oString << outputBaseName << "_112Fill.png";
	outName = oString.str();
	cv::imwrite(outName, workImage1);

	oString.str("");
	oString.clear();
	oString << outputBaseName << "_113F_Msk.png";
	outName = oString.str();
	cv::imwrite(outName, shiftedMask);

	oString.str("");
	oString.clear();
	oString << outputBaseName << "_114Cont.png";
	outName = oString.str();
	cv::imwrite(outName, shiftedContourMask);

	if (track.OutputExtraImages)
	{
		for (int n = 0; n < workImage1.total(); n++)
		{
			if (shiftedMask.at<unsigned short int>(n) > 0)
			{
				cValue = workImage1.at<cv::Vec3b>(n);
				cValue[2] = 126;
				workImage1.at<cv::Vec3b>(n) = cValue;
			}
		}
		outName = outputBaseName;
		outName.append("_315_Overlay.png");
		cv::imwrite(outName, workImage1);
	}

	if (track.OutputExtraCSV)
	{
		outName = outputBaseName;
		outName.append("_325_PixelInten.csv");
		outFile.open(outName.c_str());

		if (outFile.is_open())
		{
			int skip = 25;
			std::ostringstream hString1;
			std::ostringstream hString2;
			hString1.str("");
			hString2.str("");
			hString1.clear();
			hString2.clear();
			hString1 << "Index,Array,Section";
			hString2 << ",,";
			hString1 << ",Section,";
			hString2 << ",Row,Col";
			hString1 << ",Input,Image";
			hString2 << ",Row,Col";
			hString1 << ",Numb,Row,Col,Intensities";
			hString2 << ",Pixels,,,";
			outFile << hString1.str() << "\n";
			outFile << hString2.str() << std::endl;

			for (int n = 0; n < blobList.size(); n += skip)
			{
				outFile << n << "," << blobList[n].Array << "," << blobList[n].Section
					<< "," << blobList[n].SectionPosition.y << "," << blobList[n].SectionPosition.x
					<< "," << blobList[n].ShapeSummary.InputCenter2f.y
					<< "," << blobList[n].ShapeSummary.InputCenter2f.x
					<< "," << blobList[n].ShapeSummary.PixelIntensities.size() << "\n";
				int pSize = (int)blobList[n].ShapeSummary.PixelIntensities.size();
				for (int m = 0; m < pSize; m++)
				{
					outFile << ",,,,,,,," << blobList[n].ShapeSummary.PixelList[m].y << "," << blobList[n].ShapeSummary.PixelList[m].x
						<< "," << blobList[n].ShapeSummary.PixelIntensities[m]
						<< "\n";
				}
			}
		}
		outFile.close();
	}

	OutputMaskingResults(arrSecList, inputBlobList, blobList, outputBaseName);
	logFile.close();
	return 0;
}

void ShowUsage(const char* msg, const char* exe)
{
	std::clog << ">>>> " << msg << std::endl;
	std::clog << "Usage: " << exe << " <Working Directory> <Template Directory> <Image Name> <Prefix> <FilterLabel> <SubImage Label>\n";
	std::clog << "<Sizing BaseName> <Output BaseName> <Color Shift X> <Color Shift Y> <Dilation>\n";
	std::clog << "<Transpose Image ? > <Use Median Filter ? > <Extra Output>\n";	// 
	std::clog << "For boolean values <Transpose Image?> and <Use Median Filter?> use 1 for true and 0 for false\n";
}
