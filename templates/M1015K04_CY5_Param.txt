# Shrinkage Limit
0.80
# Before Exclusion Factor
1.50
# Positive/Negative Ratio
2.00
# Ratio Default Divider
1.50
# Run Sigma Multiplier
2.50
# Exclusion Rules
# Array Section LeftCol RightCol TopRow BotRow Corner
0 0 0 0 0 0 0 0
