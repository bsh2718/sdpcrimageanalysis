﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.ExceptionServices;
using System.Data;

namespace IThree
{
    public class IdxThree
    {
        int XSize;
        bool Xzero;
        int YSize;
        bool Yzero;
        int ZSize;
        bool Zzero;
        int XMult;
        int YMult;
        public int Base;
        public int Size;
        public IdxThree()
        {
            XSize = 4; Xzero = false; YSize = 2; Yzero = true; ZSize = 3; Zzero = false;
            if (Xzero)
                Base = 0;
            else
                Base = YSize * ZSize;
            if (!Yzero)
                Base += ZSize;
            if (!Zzero)
                Base++;

            XMult = YSize * ZSize;
            YMult = ZSize;
            Size = XSize * YSize * ZSize;
        }
        public IdxThree(int XS, bool XZ, int YS, bool YZ, int ZS, bool ZZ)
        {
            XSize = XS;
            Xzero = XZ;
            YSize = YS;
            Yzero = YZ;
            ZSize = ZS;
            Zzero = ZZ;

            if (Xzero)
                Base = 0;
            else
                Base = YSize * ZSize;
            if (!Yzero)
                Base += ZSize;
            if (!Zzero)
                Base++;

            XMult = YSize * ZSize;
            YMult = ZSize;
            Size = XSize * YSize * ZSize;
        }
        ~IdxThree() { }

        public int IndexOut(int X, int Y, int Z)
        {
            return X * XMult + Y * YMult + Z - Base;
        }
        public bool DecodeIndex(int Index, out int X, out int Y, out int Z)
        {
            int tx = (int)Math.Floor((double)Index / XMult);
            int ty = (int)Math.Floor(((double)Index - tx * XMult) / YMult);
            int tz = Index - tx * XMult - ty * YMult;
            X = 0;
            Y = 0;
            Z = 0;
            if (tx < 0 || ty < 0 || tz < 0)
                return false;
            if (Xzero)
            {
                if (tx + 1 > XSize)
                    return false;
            }
            else
            {
                tx++;
                if (tx > XSize)
                    return false;
            }
            if (Yzero)
            {
                if (ty + 1 > YSize)
                    return false;
            }
            else
            {
                ty++;
                if (ty > YSize)
                    return false;
            }
            if (Zzero)
            {
                if (tz + 1 > ZSize)
                    return false;
            }
            else
            {
                tz++;
                if (tz > ZSize)
                    return false;
            }
            X = tx;
            Y = ty;
            Z = tz;
            return true;
        }
    };
}
