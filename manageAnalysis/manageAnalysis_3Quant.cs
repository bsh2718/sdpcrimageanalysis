﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.ExceptionServices;
using System.Data;


namespace manageAnalysis
{
    using IThree;
    using System.Collections.Specialized;
    using System.Runtime.Remoting.Messaging;
    using System.Security.Cryptography;

    public partial class ImageData
    {

        public int AddQuantification(string QuantDye, string QuantParameterFileName, int BeforeSetNumb, int[] AfterSetNumbersArray,
            string BeforeAnalysisType, string AfterAnalysisType, out string Err)
        {
            char[] zero = new char[] { '0' };
            Err = "";
            int newAnalysis = 0;
            XElement quantNode = ImageDoc.Descendants("Quantifications").FirstOrDefault();
            int newProgramCounter = 0;
            if (quantNode != null)
            {
                List<int> afterSetNumbers = AfterSetNumbersArray.ToList();
                string beforeType = "Masking";
                if (BeforeAnalysisType.IndexOf("Sizing", StringComparison.CurrentCultureIgnoreCase) >= 0)
                    beforeType = "Sizing";

                string afterType = "Masking";
                if (AfterAnalysisType.IndexOf("Sizing", StringComparison.CurrentCultureIgnoreCase) >= 0)
                    afterType = "Sizing";
                if (QuantDye.Length < 2)
                {
                    Err = "No Quant Dye";
                    return 0;
                }
                if (QuantParameterFileName.Length < 2)
                {
                    Err = "No QuantParameterFileName";
                    return 0;
                }
                if (BeforeSetNumb < 0)
                {
                    Err = "Before Set Numb must be >= 0";
                    return 0;
                }
                for (int m = 0; m < AfterSetNumbersArray.Length; m++)
                {
                    if (AfterSetNumbersArray[m] < 0)
                    {
                        Err = "All After Set Numbs must be >= 0";
                        return 0;
                    }
                }
                Array.Sort(AfterSetNumbersArray, 0, AfterSetNumbersArray.Count());
                IEnumerable<XElement> sortedQuantIEnum = from el in ImageDoc.Descendants("ImagesSortedByDye").Descendants("Dye")
                                                         where el.Attribute("DyeName") != null
                                                         && el.Attribute("DyeName").Value.IndexOf(QuantDye, StringComparison.CurrentCultureIgnoreCase) >= 0
                                                         select el;

                if (sortedQuantIEnum.Count() < 1)
                {
                    Err = String.Format("No SortedImage node with Quant Dye: {0} were found", QuantDye);
                    return 0;
                }

                IEnumerable<XElement> imageBeforeIEnum = from el in sortedQuantIEnum.Descendants("Index").Descendants("SortedImage")
                                                         where el.Attribute("Type") != null
                                                         && el.Attribute("Type").Value.IndexOf("Before", StringComparison.CurrentCultureIgnoreCase) >= 0
                                                         && el.Attribute("SetNumber") != null
                                                         && Int32.TryParse(el.Attribute("SetNumber").Value, out int sizingSet)
                                                         && sizingSet == BeforeSetNumb
                                                         && el.Attribute("Ignored") != null
                                                         && bool.TryParse(el.Attribute("Ignored").Value, out bool igs)
                                                         && !igs
                                                         select el;

                IEnumerable<XElement> imageAfterIEnum = from el in sortedQuantIEnum.Descendants("Index").Descendants("SortedImage")
                                                        where el.Attribute("Type") != null
                                                        && el.Attribute("Type").Value.IndexOf("After", StringComparison.CurrentCultureIgnoreCase) >= 0
                                                        && el.Attribute("SetNumber") != null
                                                        && Int32.TryParse(el.Attribute("SetNumber").Value, out int maskingSet)
                                                        && el.Attribute("Ignored") != null
                                                        && bool.TryParse(el.Attribute("Ignored").Value, out bool igm)
                                                        && !igm
                                                        select el;
                if (imageBeforeIEnum.Count() < 1)
                {
                    Err = String.Format("No before images with Dye = {0}", QuantDye);
                    return 0;
                }
                if (imageAfterIEnum.Count() < 1)
                {
                    Err = String.Format("No after images with Dye = {0}", QuantDye);
                    return 0;
                }
                List<XElement> imageBeforeList = imageBeforeIEnum.ToList();
                List<XElement> imageAfterList = imageAfterIEnum.ToList();
                for (int m = 0; m < imageBeforeList.Count; m++)
                {
                    List<int> afterNumbers = new List<int>(0);
                    List<int> afterImageNumbers = new List<int>(0);
                    //bool addSetup = false;
                    XElement xele = new XElement("Unused");
                    newProgramCounter = 0;
                    if (imageBeforeList[m].Parent.Attribute("Value") != null
                        && Int32.TryParse(imageBeforeList[m].Parent.Attribute("Value").Value, out int beforeIdx)
                        && imageBeforeList[m].Attribute("ImageNumber") != null
                        && Int32.TryParse(imageBeforeList[m].Attribute("ImageNumber").Value, out int beforeNumber)
                        )
                    {
                        bool beforeDuplicate = false;
                        for (int mm = 0; mm < imageBeforeList.Count; mm++)
                        {
                            if (mm != m
                                && imageBeforeList[mm].Parent.Attribute("Value") != null
                                && Int32.TryParse(imageBeforeList[mm].Parent.Attribute("Value").Value, out int otherIdx))
                            {
                                if (otherIdx == beforeIdx)
                                {
                                    beforeDuplicate = true;
                                    break;
                                }
                            }
                        }

                        if (!beforeDuplicate)
                        {
                            RCSid.DecodeIndex(beforeIdx, out int r, out int c, out int s);
                            List<XElement> beforeImages = GetImageByImageNumber(beforeNumber);
                            List<XElement> beforeSetups = new List<XElement>();
                            if (beforeImages.Count == 1)
                            {
                                IEnumerable<XElement> beforeImageSetupsIEnum = from el in beforeImages[0].Descendants("AnalysisSetup").Descendants("Analysis")
                                                                               where el.Attribute("Type") != null
                                                                               && el.Attribute("Type").Value.IndexOf(beforeType, StringComparison.CurrentCultureIgnoreCase) >= 0
                                                                               && el.Attribute("BaseName") != null
                                                                               && el.Attribute("BaseName").Value.Length > 4
                                                                               && el.Attribute("Selected") != null
                                                                               && bool.TryParse(el.Attribute("Selected").Value, out bool selB)
                                                                               && selB
                                                                               select el;
                                if (beforeImageSetupsIEnum.Count() == 1)
                                {
                                    beforeSetups = beforeImageSetupsIEnum.ToList();
                                    for (int n = 0; n < imageAfterList.Count; n++)
                                    {
                                        if (imageAfterList[n].Parent.Attribute("Value") != null
                                            && Int32.TryParse(imageAfterList[n].Parent.Attribute("Value").Value, out int afterIdx)
                                            && imageAfterList[n].Attribute("ImageNumber") != null
                                            && Int32.TryParse(imageAfterList[n].Attribute("ImageNumber").Value, out int afterNumber)
                                            )
                                        {
                                            if (beforeIdx == afterIdx)
                                            {
                                                afterNumbers.Add(afterNumber);
                                            }
                                        }
                                    }

                                    if (afterNumbers.Count > 0)
                                    {
                                        List<XElement> afterSetups = new List<XElement>(0);
                                        int[] afterSetsCount = new int[AfterSetNumbersArray.Count()];
                                        for (int k = 0; k < AfterSetNumbersArray.Count(); k++)
                                        {
                                            afterSetsCount[k] = 0;
                                            for (int n = 0; n < afterNumbers.Count; n++)
                                            {
                                                List<XElement> afterImageTmp = GetImageByImageNumber(afterNumbers[n]);
                                                if (afterImageTmp.Count == 1)
                                                {
                                                    if (afterImageTmp[0].Attribute("SetNumber") != null
                                                        && Int32.TryParse(afterImageTmp[0].Attribute("SetNumber").Value, out int afterTmpSetNumb)
                                                        && afterTmpSetNumb == AfterSetNumbersArray[k]
                                                        )
                                                    {
                                                        IEnumerable<XElement> afterImageTmpSetup = from el in afterImageTmp[0].Descendants("AnalysisSetup").Descendants("Analysis")
                                                                                                   where el.Attribute("Type") != null
                                                                                                   && el.Attribute("Type").Value.IndexOf(afterType, StringComparison.CurrentCultureIgnoreCase) >= 0
                                                                                                   && el.Attribute("BaseName") != null
                                                                                                   && el.Attribute("BaseName").Value.Length > 4
                                                                                                   && el.Attribute("Selected") != null
                                                                                                   && bool.TryParse(el.Attribute("Selected").Value, out bool selA)
                                                                                                   && selA
                                                                                                   select el;
                                                        if (afterImageTmpSetup.Count() == 1)
                                                        {
                                                            afterSetups.Add(afterImageTmpSetup.First());
                                                            afterSetsCount[k]++;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (afterSetups.Count > 0)
                                        {
                                            int found = afterSetups.Count;
                                            int cnt = 0;
                                            for (int k = 0; k < afterSetsCount.Count(); k++)
                                            {
                                                cnt += afterSetsCount[k];
                                                if (afterSetsCount[k] > 1)
                                                {
                                                    cnt = -1;
                                                    break;
                                                }
                                            }
                                            if (cnt != found)
                                            {
                                                afterSetups.Clear();
                                            }
                                        }
                                        if (afterSetups.Count > 0)
                                        {
                                            newProgramCounter = ProgramCounter + 1;
                                            List<string> aBaseNames = new List<string>();
                                            List<int> aSetNumbers = new List<int>();
                                            List<int> aImageNumbers = new List<int>();
                                            string beforeBaseName = beforeSetups[0].Attribute("BaseName").Value;
                                            string cp = "Left";
                                            if (c > 0)
                                                cp = "Right";
                                            XElement quant = new XElement("Quantify",
                                                new XAttribute("WorkingDirectory", WorkingDirectory),
                                                new XAttribute("TemplateDirectory", TemplateDirectory),
                                                new XAttribute("ParameterFile", QuantParameterFileName),
                                                new XAttribute("TemplatePrefix", Device),
                                                new XAttribute("RotorPosition", r.ToString()),
                                                new XAttribute("ChipPosition", cp),
                                                new XAttribute("SubImage", s.ToString()),
                                                new XAttribute("BaseName", String.Format("{0}_{1}_P{2:D4}",
                                                        BaseNameStub, RotorChipSubLabel(r, c, s), newProgramCounter)),
                                                new XAttribute("BeforeSetNumber", BeforeSetNumb.ToString()),
                                                new XAttribute("BeforeBaseName", beforeBaseName),
                                                new XAttribute("BeforeImageNumber", beforeNumber.ToString("D3")),
                                                new XAttribute("ProgramCounter", newProgramCounter.ToString("D4")),
                                                new XAttribute("Success", "false"),
                                                new XAttribute("Performed", "false")
                                               );
                                            aBaseNames.Clear();
                                            aSetNumbers.Clear();
                                            aImageNumbers.Clear();
                                            for (int k = 0; k < afterSetups.Count; k++)
                                            {
                                                if (afterSetups[k].Attribute("Selected") != null
                                                        && bool.TryParse(afterSetups[k].Attribute("Selected").Value, out bool selA)
                                                        && selA
                                                        && afterSetups[k].Attribute("BaseName") != null
                                                        && afterSetups[k].Attribute("BaseName").Value.Length > 4
                                                        && afterSetups[k].Parent.Parent.Attribute("ImageNumber") != null
                                                        && Int32.TryParse(afterSetups[k].Parent.Parent.Attribute("ImageNumber").Value, out int aNumb)
                                                        && afterSetups[k].Parent.Parent.Attribute("SetNumber") != null
                                                        && Int32.TryParse(afterSetups[k].Parent.Parent.Attribute("SetNumber").Value, out int aSet))
                                                {
                                                    aBaseNames.Add(afterSetups[k].Attribute("BaseName").Value);
                                                    aSetNumbers.Add(aSet);
                                                    aImageNumbers.Add(aNumb);
                                                }

                                            }
                                            if (aBaseNames.Count > 0)
                                            {
                                                XElement aftNamesNode = CollectionNode("AfterBaseNames", "AfterBaseName");
                                                List<XElement> aftList = new List<XElement>();
                                                for (int j = 0; j < aBaseNames.Count; j++)
                                                {
                                                    XElement aftItem = new XElement("AfterBaseName",
                                                        new XAttribute("Name", aBaseNames[j]),
                                                        new XAttribute("Set", aSetNumbers[j].ToString()),
                                                        new XAttribute("ImageNumber", aImageNumbers[j].ToString())
                                                        );
                                                    aftNamesNode.Add(aftItem);
                                                    aftList.Add(aftItem);
                                                }
                                                quant.Add(aftNamesNode);
                                                bool duplicate = false;
                                                IEnumerable<XElement> existingQIEnum = from el in quantNode.Descendants("Quantify")
                                                                                       where el.Attribute("ParameterFile").Value.IndexOf(QuantParameterFileName) >= 0
                                                                                       && el.Attribute("SubImage").Value.IndexOf(s.ToString()) >= 0
                                                                                       && el.Attribute("BeforeSetNumber").Value.IndexOf(BeforeSetNumb.ToString()) >= 0
                                                                                       && el.Attribute("BeforeBaseName").Value.IndexOf(beforeBaseName) >= 0
                                                                                       && el.Attribute("BeforeImageNumber").Value.IndexOf(beforeNumber.ToString()) >= 0
                                                                                       select el;
                                                if (existingQIEnum != null && existingQIEnum.Count() > 0)
                                                {
                                                    List<XElement> existingQList = existingQIEnum.ToList();
                                                    {
                                                        for (int j = 0; j < existingQList.Count; j++)
                                                        {
                                                            List<XElement> existingAfterNamesList = existingQList[j].Descendants("AfterBaseName").ToList();
                                                            if (existingAfterNamesList.Count == aftList.Count)
                                                            {
                                                                bool match = true;
                                                                for (int jj = 0; jj < existingAfterNamesList.Count; jj++)
                                                                {
                                                                    if (existingAfterNamesList[jj].Attribute("Name").Value.IndexOf(aftList[jj].Attribute("Name").Value) >= 0)
                                                                    {
                                                                        int v1;
                                                                        int w1;
                                                                        if (Int32.TryParse(existingAfterNamesList[jj].Attribute("Set").Value, out v1)
                                                                            && Int32.TryParse(aftList[jj].Attribute("Set").Value, out w1))
                                                                        {
                                                                            if (v1 != w1)
                                                                            {
                                                                                match = false;
                                                                                break;
                                                                            }
                                                                            else
                                                                            {
                                                                                if (Int32.TryParse(existingAfterNamesList[jj].Attribute("ImageNumber").Value, out v1)
                                                                                    && Int32.TryParse(aftList[jj].Attribute("ImageNumber").Value, out w1))
                                                                                {
                                                                                    if (v1 != w1)
                                                                                    {
                                                                                        match = false;
                                                                                        break;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            match = false;
                                                                            break;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        match = false;
                                                                        break;
                                                                    }

                                                                }
                                                                if (match)
                                                                {
                                                                    duplicate = true;
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                if (duplicate)
                                                {

                                                }
                                                else
                                                {
                                                    quantNode.Add(quant);
                                                    ProgramCounter = newProgramCounter;
                                                    SetProgramCounter(newProgramCounter);
                                                    newAnalysis++;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return newAnalysis;
        }
        public int GetQuantInfoArray(out string[] QuantInfo, out string Err)
        {
            int res = GetQuantInfo(out List<string> quantInfoList, out Err);
            QuantInfo = quantInfoList.ToArray();
            return res;
        }
        public int GetQuantInfo(out List<string> QuantInfo, out string Err)
        {
            return GetQuantifications(true, true, true, out List<string> QuantString, out QuantInfo, out Err);
        }
        public int GetQuantifications(bool All, bool RStatus, bool SStatus, out List<string> QuantSetups, out List<string> QuantInfo, out string Err)
        {
            Err = "";
            QuantSetups = new List<string>();
            QuantInfo = new List<string>();
            Err = "";
            //Setups = new List<Tuple<string, string>>();
            //Tuple<string, string> newTuple;
            if (_XDocLoaded)
            {
                List<XElement> qsetup = ImageDoc.Descendants("Quantifications").Descendants("Quantify").ToList();
                if (qsetup.Count > 0)
                {
                    for (int n = 0; n < qsetup.Count; n++)
                    {
                        if (qsetup[n].Attribute("BeforeImageNumber") != null
                            && qsetup[n].Attribute("BeforeBaseName") != null
                            && qsetup[n].Attribute("ParameterFile") != null
                            && qsetup[n].Attribute("ProgramCounter") != null
                            && qsetup[n].Attribute("BeforeSetNumber") != null
                            && qsetup[n].Attribute("RotorPosition") != null
                            && qsetup[n].Attribute("ChipPosition") != null
                            && qsetup[n].Attribute("SubImage") != null
                            && qsetup[n].Attribute("Performed") != null
                            && qsetup[n].Attribute("Success") != null
                            && qsetup[n].Attribute("BaseName") != null)
                        {
                            string beforeImageN = qsetup[n].Attribute("BeforeImageNumber").Value;
                            string beforeBaseName = qsetup[n].Attribute("BeforeBaseName").Value;
                            string parameterFile = qsetup[n].Attribute("ParameterFile").Value;
                            string spc = qsetup[n].Attribute("ProgramCounter").Value;
                            string bSetN = qsetup[n].Attribute("BeforeSetNumber").Value;
                            string rotorP = qsetup[n].Attribute("RotorPosition").Value;
                            string chipP = qsetup[n].Attribute("ChipPosition").Value;
                            string subIm = qsetup[n].Attribute("SubImage").Value;
                            string performed = qsetup[n].Attribute("Performed").Value;
                            string success = qsetup[n].Attribute("Success").Value;
                            string basename = qsetup[n].Attribute("BaseName").Value;
                            if (Int32.TryParse(beforeImageN, out _) && Int32.TryParse(bSetN, out _)
                               && Int32.TryParse(spc, out _) && beforeBaseName != null && parameterFile != null)
                            {
                                IEnumerable<XElement> qList = qsetup[n].Descendants("AfterBaseNames").Descendants("AfterBaseName");
                                string per;
                                string suc;
                                bool bPer;
                                bool bSuc;
                                if (!All)
                                {
                                    per = qsetup[n].Attribute("Performed").Value;
                                    suc = qsetup[n].Attribute("Success").Value;
                                    if (bool.TryParse(per, out bPer) && bool.TryParse(suc, out bSuc))
                                    {
                                        if (qList.Count() > 0)
                                        {
                                            if (RStatus == bPer)
                                                bPer = true;
                                            else
                                                bPer = false;
                                            if (SStatus == bSuc)
                                                bSuc = true;
                                            else
                                                bSuc = false;
                                        }
                                        if (bPer && bSuc)
                                            All = true;
                                    }
                                }

                                if (All)
                                {
                                    if (beforeImageN != null)
                                    {
                                        QuantSetups.Add(qsetup[n].ToString());
                                        QuantInfo.Add(String.Format("{0},{1},{2},{3},{4},{5},{6},{7}",
                                            spc, basename, rotorP, chipP, subIm, parameterFile, performed, success));
                                    }
                                }
                                else
                                {
                                    Err += String.Format("No <AfterBaseName> nodes in quant setup {0}: {1} .  ", n, qsetup[n].ToString());
                                }
                            }
                            else
                            {
                                Err += String.Format("Error in quant setup {0}: {1} .  ", n, qsetup[n].ToString());
                            }
                        }
                    }
                }
            }
            return QuantSetups.Count;
            //String.Format("sdpcrQuantLib( Err, {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7} BaseNamesAfter, SetNumbersAfter)",
            //    WorkingDirectory, TemplateDirectory, ParameterFile, Device, S.ToString(), baseNameOut, aBaseNames, aSetNumbers)
        }
        public int GetQuantAnalysisArray(int BeforeSetNumb, bool All, bool RStatus, bool SStatus,
            out string[] SetupStrings, out string Err)
        {
            int res = GetQuantAnalysisStrings(BeforeSetNumb, All, RStatus, SStatus,
                out List<string> setupStringsList, out Err);
            SetupStrings = setupStringsList.ToArray();
            return res;
        }
        public int GetQuantAnalysisStrings(int BeforeSetNumb, bool All, bool RStatus, bool SStatus,
            out List<string> SetupStrings, out string Err)
        {
            Err = "";
            SetupStrings = new List<string>();
            //Tuple<string, string> newTuple;
            if (_XDocLoaded)
            {
                List<XElement> quantSetupList = ImageDoc.Descendants("Quantifications").Descendants("Quantify").ToList();
                if (quantSetupList.Count > 0)
                {
                    for (int n = 0; n < quantSetupList.Count; n++)
                    {
                        string pPerformed = quantSetupList[n].Attribute("Performed").Value;
                        string pSuccess = quantSetupList[n].Attribute("Success").Value;
                        string pProgramC = quantSetupList[n].Attribute("ProgramCounter").Value;
                        string pBaseName = quantSetupList[n].Attribute("BaseName").Value;
                        string pRotor = quantSetupList[n].Attribute("RotorPosition").Value;
                        string pChip = quantSetupList[n].Attribute("ChipPosition").Value;
                        string pSubImage = quantSetupList[n].Attribute("SubImage").Value;
                        string pPrefix = quantSetupList[n].Attribute("TemplatePrefix").Value;
                        string pTDirectory = quantSetupList[n].Attribute("TemplateDirectory").Value;
                        string pWDirectory = quantSetupList[n].Attribute("WorkingDirectory").Value;
                        string pBeforeImageNumber = quantSetupList[n].Attribute("BeforeImageNumber").Value;
                        string pBeforeBaseName = quantSetupList[n].Attribute("BeforeBaseName").Value;
                        string pBeforeSetNumber = quantSetupList[n].Attribute("BeforeSetNumber").Value;
                        string pParameterFile = quantSetupList[n].Attribute("ParameterFile").Value;

                        bool bPerformed;
                        bool bSuccess;
                        int iBeforeSetNumber;
                        if (bool.TryParse(pPerformed, out bPerformed)
                            && bool.TryParse(pSuccess, out bSuccess)
                            && Int32.TryParse(pProgramC, out _)
                            && Int32.TryParse(pRotor, out _)
                            && (pChip.IndexOf("Left", StringComparison.CurrentCultureIgnoreCase) >= 0 ||
                                pChip.IndexOf("Right", StringComparison.CurrentCultureIgnoreCase) >= 0)
                            && Int32.TryParse(pSubImage, out _)
                            && Int32.TryParse(pBeforeImageNumber, out _)
                            && Int32.TryParse(pBeforeSetNumber, out iBeforeSetNumber)
                            && iBeforeSetNumber == BeforeSetNumb)
                        {
                            if (RStatus == bPerformed)
                                bPerformed = true;
                            else
                                bPerformed = false;
                            if (SStatus == bSuccess)
                                bSuccess = true;
                            else
                                bSuccess = false;

                            if ((bSuccess && bPerformed) || All)
                            {
                                StringBuilder cmd = new StringBuilder();
                                cmd.Clear();
                                cmd.Append(String.Format("sdpcrQuant.exe {0} {1} {2} {3} {4} {5} {6}",
                                    pWDirectory, pTDirectory, pParameterFile, pPrefix, pSubImage, pBaseName, pBeforeBaseName));
                                List<XElement> qAfterBaseNamesList = quantSetupList[n].Descendants("AfterBaseNames").Descendants("AfterBaseName").ToList();
                                if (qAfterBaseNamesList.Count > 0)
                                {
                                    //List<string> afterBaseNameArgs = new List<string>();
                                    for (int m = 0; m < qAfterBaseNamesList.Count; m++)
                                    {
                                        string aBase = qAfterBaseNamesList[m].Attribute("Name").Value;
                                        string aSet = qAfterBaseNamesList[m].Attribute("Set").Value;
                                        cmd.Append(String.Format(" {0} {1}", aBase, aSet));
                                    }
                                    cmd.Append(String.Format(" | {0}", pProgramC));
                                    SetupStrings.Add(cmd.ToString());
                                }
                            }
                            //sdpcrQuantLib(std::string & Err, std::string WorkingDirectory, std::string TemplateDirectory,
                            //std::string ParameterFile, std::string Prefix, std::string SubImageLabel,
                            //std::string BaseNameOutput, std::string BaseNameInput,
                            //std::vector < std::string > BaseNamesAfter, const std::vector<int> SetNumbersAfter = std::vector<int>());

                        }
                    }
                }
            }
            return SetupStrings.Count;
        }
        public int GetQuantAnalysisSetups(int BeforeSetNumb, bool RStatus, bool SStatus,
            out List<string> QuantSetupStrings, out List<XElement> QuantSetupInfo, out string Err)
        {
            Err = "";
            QuantSetupStrings = new List<string>();
            QuantSetupInfo = new List<XElement>();
            //Tuple<string, string> newTuple;
            if (_XDocLoaded)
            {
                List<XElement> quantSetupList = ImageDoc.Descendants("Quantifications").Descendants("Quantify").ToList();
                if (quantSetupList.Count > 0)
                {
                    for (int n = 0; n < quantSetupList.Count; n++)
                    {
                        string pPerformed = quantSetupList[n].Attribute("Performed").Value;
                        string pSuccess = quantSetupList[n].Attribute("Success").Value;
                        string pProgramCounter = quantSetupList[n].Attribute("ProgramCounter").Value;
                        string pBaseName = quantSetupList[n].Attribute("BaseName").Value;
                        string pRotor = quantSetupList[n].Attribute("RotorPosition").Value;
                        string pChip = quantSetupList[n].Attribute("ChipPosition").Value;
                        string pSubImage = quantSetupList[n].Attribute("SubImage").Value;
                        string pPrefix = quantSetupList[n].Attribute("TemplatePrefix").Value;
                        string pTDirectory = quantSetupList[n].Attribute("TemplateDirectory").Value;
                        string pWDirectory = quantSetupList[n].Attribute("WorkingDirectory").Value;
                        string pBeforeImageNumber = quantSetupList[n].Attribute("BeforeImageNumber").Value;
                        string pBeforeBaseName = quantSetupList[n].Attribute("BeforeBaseName").Value;
                        string pBeforeSetNumber = quantSetupList[n].Attribute("BeforeSetNumber").Value;
                        string pParameterFile = quantSetupList[n].Attribute("ParameterFile").Value;
                        bool bPerformed;
                        bool bSuccess;
                        int iBeforeSetNumber;
                        if (bool.TryParse(pPerformed, out bPerformed)
                            && bool.TryParse(pSuccess, out bSuccess)
                            && Int32.TryParse(pProgramCounter, out _)
                            && Int32.TryParse(pRotor, out _)
                            && Int32.TryParse(pChip, out _)
                            && Int32.TryParse(pSubImage, out _)
                            && Int32.TryParse(pBeforeImageNumber, out _)
                            && Int32.TryParse(pBeforeSetNumber, out iBeforeSetNumber)
                            && iBeforeSetNumber == BeforeSetNumb)
                        {
                            if (RStatus == bPerformed)
                                bPerformed = true;
                            else
                                bPerformed = false;
                            if (SStatus == bSuccess)
                                bSuccess = true;
                            else
                                bSuccess = false;

                            if (bSuccess && bPerformed)
                            {
                                StringBuilder cmd = new StringBuilder();
                                cmd.Clear();
                                cmd.Append(String.Format("sdpcrQuant.exe {0} {1} {2} {3} {4} {5} {6}",
                                    pWDirectory, pTDirectory, pParameterFile, pPrefix, pSubImage, pBaseName, pBeforeBaseName));
                                List<XElement> qAfterBaseNamesList = quantSetupList[n].Descendants("AfterBaseNames").Descendants("AfterBaseName").ToList();
                                if (qAfterBaseNamesList.Count > 0)
                                {
                                    XElement afterBaseNameArgs = new XElement("AfterBaseNames", new XAttribute("CollectionType", "AfterBaseName"));
                                    for (int m = 0; m < qAfterBaseNamesList.Count; m++)
                                    {
                                        string aBase = qAfterBaseNamesList[m].Attribute("Name").Value;
                                        string aSet = qAfterBaseNamesList[m].Attribute("Set").Value;
                                        cmd.Append(String.Format(" {0} {1}", aBase, aSet));
                                        cmd.Append(String.Format(" | {0}", pProgramCounter));
                                        afterBaseNameArgs.Add(XElement.Parse(qAfterBaseNamesList[m].ToString()));
                                    }
                                    int count = QuantSetupStrings.Count;
                                    XElement newSetup = new XElement("SetupInfo",
                                            new XAttribute("ScriptIndex", count.ToString()),
                                            new XAttribute("ProgramCounter", pProgramCounter),
                                            new XAttribute("Success", pSuccess),
                                            new XAttribute("Performed", pPerformed),
                                            new XAttribute("RotorPosition", pRotor),
                                            new XAttribute("ChipPosition", pChip),
                                            new XAttribute("SubImage", pSubImage),
                                            new XAttribute("BaseName", pBaseName),
                                            new XAttribute("ParameterFile", pParameterFile),
                                            new XAttribute("BeforeBaseName", pBeforeBaseName),
                                            new XAttribute("BeforeImageNumber", pBeforeImageNumber),
                                            new XAttribute("BeforeSetNumber", pBeforeSetNumber)
                                            );
                                    newSetup.Add(afterBaseNameArgs);
                                    QuantSetupInfo.Add(newSetup);
                                    QuantSetupStrings.Add(cmd.ToString());
                                }
                            }
                            //sdpcrQuantLib(std::string & Err, std::string WorkingDirectory, std::string TemplateDirectory,
                            //std::string ParameterFile, std::string Prefix, std::string SubImageLabel,
                            //std::string BaseNameOutput, std::string BaseNameInput,
                            //std::vector < std::string > BaseNamesAfter, const std::vector<int> SetNumbersAfter = std::vector<int>());

                        }
                    }
                }
            }
            return QuantSetupStrings.Count;
        }
        public bool UpdateQuantificationRunStatus(int ProgramNumber, bool NewPerformed, bool NewSuccess)
        {
            IEnumerable<XElement> qIEnum = from el in ImageDoc.Descendants("Quantifications").Descendants("Quantify")
                                           where el.Attribute("ProgramCounter").Value.IndexOf(ProgramNumber.ToString()) >= 0
                                           select el;
            if (qIEnum.Count() == 1)
            {
                List<XElement> qList = qIEnum.ToList();
                qList[0].Attribute("Performed").Value = NewPerformed.ToString();
                qList[0].Attribute("Success").Value = NewSuccess.ToString();
                return true;
            }
            return false;
        }

        public bool AlternativeQuantSetup(int ProgramNumber, string NewParameterFile)
        {
            char[] digits = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            if (_XDocLoaded && NewParameterFile.Length > 3)
            {
                IEnumerable<XElement> quantSetupIEnum = from el in ImageDoc.Descendants("Quantifications").Descendants("Quantify")
                                                        where el.Attribute("ProgramCounter") != null
                                                        select el;
                if (quantSetupIEnum.Count() > 0)
                {
                    for (int n = 0; n < quantSetupIEnum.Count(); n++)
                    {
                        int pc;
                        if (Int32.TryParse(quantSetupIEnum.ToList()[n].Attribute("ProgramCounter").Value, out pc))
                        {
                            if (pc == ProgramNumber)
                            {
                                XElement newAnalysis = XElement.Parse(quantSetupIEnum.ToList()[n].ToString());
                                if (newAnalysis.Attribute("ParamterFile") != null
                                    && newAnalysis.Attribute("ProgramCounter") != null
                                    && newAnalysis.Attribute("BaseName") != null)
                                {
                                    int newProgramCounter = ProgramCounter + 1;
                                    newAnalysis.Attribute("ParamterFile").Value = NewParameterFile;
                                    newAnalysis.Attribute("ProgramCounter").Value = newProgramCounter.ToString("D4");
                                    string pcNumb = newProgramCounter.ToString("D4");
                                    string newBaseName = newAnalysis.Attribute("BaseName").Value.TrimEnd(digits) + pcNumb;
                                    newAnalysis.Attribute("ProgramConter").Value = pcNumb;
                                    newAnalysis.Attribute("BaseName").Value = newBaseName;
                                    newAnalysis.Attribute("Performed").Value = "false";
                                    newAnalysis.Attribute("Success").Value = "false";
                                    ProgramCounter = newProgramCounter;
                                    SetProgramCounter(ProgramCounter);
                                    quantSetupIEnum.ToList()[n].Parent.Add(newAnalysis);
                                }
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
    };
}
