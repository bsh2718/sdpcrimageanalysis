﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.ExceptionServices;
using System.Data;

namespace manageAnalysis
{
    using IThree;
    using System.Collections.Specialized;
    using System.Runtime.Remoting.Messaging;
    using System.Security.Cryptography;

    public partial class ImageData
    {
        public bool GetImageDataArray(int ImageNumber, out string ImageName, out bool Before, out int RotorPosition,
            out bool Left, out int Section, out String Dye, out int SetNumber,
            out string Attributes, out string[] Setups, out int SizingProgramCounter, out int MaskingProgramCounter,
            out bool[] SetupState)
        {
            bool res = GetImageData(ImageNumber, out ImageName, out Before, out RotorPosition,
                out Left, out Section, out Dye, out SetNumber,
                out Attributes, out List<string> setupList, out SizingProgramCounter, out MaskingProgramCounter,
                out SetupState);
            Setups = setupList.ToArray();
            return res;
        }
        public bool GetImageData(int ImageNumber, out string ImageName, out bool Before, out int RotorPosition,
            out bool Left, out int Section, out String Dye, out int SetNumber,
            out string Attributes, out List<string> Setups, out int SizingProgramCounter, out int MaskingProgramCounter,
            out bool[] SetupState)
        {
            ImageName = "";
            Attributes = "";
            Setups = new List<string>(0);
            Before = true;
            RotorPosition = 0;
            Left = true;
            Section = 0;
            Dye = "";
            SetNumber = -1;
            SizingProgramCounter = -1;
            MaskingProgramCounter = -1;
            SetupState = new bool[6];
            if (!_XDocLoaded)
                return false;

            List<XElement> iList = ImageDoc.Descendants("Image").ToList();
            if (iList.Count < 1)
                return false;
            int target = -1;
            for (int n = 0; n < iList.Count; n++)
            {
                if (iList[n].Attribute("ImageNumber") != null)
                {
                    if (Int32.TryParse(iList[n].Attribute("ImageNumber").Value, out int w1))
                    {
                        if (w1 == ImageNumber)
                        {
                            target = n;
                            break;
                        }
                    }
                }
            }
            if (target < 0)
                return false;
            XElement im = iList[target];
            if (im.Attribute("Type") != null && im.Attribute("RotorPosition") != null
                && im.Attribute("ChipPosition") != null && im.Attribute("Section") != null
                && im.Attribute("Dye") != null && im.Attribute("Name") != null && im.Attribute("SetNumber") != null
                && im.Attribute("ImageNumber") != null && im.Attribute("JNumber") != null && im.Attribute("Time") != null)
            {
                if (Int32.TryParse(im.Attribute("RotorPosition").Value, out int rp)
                    && Int32.TryParse(im.Attribute("Section").Value, out int sec)
                    && (im.Attribute("Type").Value.IndexOf("Before", StringComparison.CurrentCultureIgnoreCase) >= 0 ||
                    im.Attribute("Type").Value.IndexOf("After", StringComparison.CurrentCultureIgnoreCase) >= 0)
                    && (im.Attribute("ChipPosition").Value.IndexOf("Left", StringComparison.CurrentCultureIgnoreCase) >= 0 ||
                    im.Attribute("ChipPosition").Value.IndexOf("Right", StringComparison.CurrentCultureIgnoreCase) >= 0)
                    && Int32.TryParse(im.Attribute("ImageNumber").Value, out int im_numb)
                    && Int32.TryParse(im.Attribute("JNumber").Value, out int j_numb)
                    && Int32.TryParse(im.Attribute("SetNumber").Value, out int s_numb)
                    && im.Attribute("Dye").Value.Length > 1 && im.Attribute("Name").Value.Length > 5
                    && im_numb > 0 && j_numb > 0 && s_numb >= 0)
                {
                    ImageName = im.Attribute("Name").Value;
                    if (im.Attribute("Type").Value.IndexOf("Before") >= 0)
                        Before = true;
                    else
                        Before = false;
                    RotorPosition = rp;
                    if (im.Attribute("ChipPosition").Value.IndexOf("Left") >= 0)
                        Left = true;
                    else
                        Left = false;
                    Section = sec;
                    Dye = im.Attribute("Dye").Value;
                    SetNumber = s_numb;
                    List<XElement> sets = im.Descendants("AnalysisSetup").Descendants("Analysis").ToList();
                    if (sets.Count > 0)
                    {
                        for (int m = 0; m < sets.Count; m++)
                        {
                            if (sets[m].Attribute("Type") != null && sets[m].Attribute("Selected") != null
                                && sets[m].Attribute("Performed") != null && sets[m].Attribute("Sucess") != null
                                && sets[m].Attribute("ExtraOutput") != null && sets[m].Attribute("UseMedianFilter") != null
                                && sets[m].Attribute("TransposeImage") != null && sets[m].Attribute("BaseName") != null
                                && sets[m].Attribute("FilterLabel") != null && sets[m].Attribute("TemplatePrefix") != null
                                && sets[m].Attribute("ProgramCounter") != null)
                            {
                                if ((sets[m].Attribute("Type").Value.IndexOf("Sizing", StringComparison.CurrentCultureIgnoreCase) >= 0 ||
                                    sets[m].Attribute("Type").Value.IndexOf("Masking", StringComparison.CurrentCultureIgnoreCase) >= 0)
                                    && bool.TryParse(sets[m].Attribute("Selected").Value, out bool bSelected)
                                    && bool.TryParse(sets[m].Attribute("Performed").Value, out bool bPerformed)
                                    && bool.TryParse(sets[m].Attribute("Success").Value, out bool bSuccess)
                                    && Int32.TryParse(sets[m].Attribute("ExtraOutput").Value, out int extraOutput)
                                    && bool.TryParse(sets[m].Attribute("UseMedianFilter").Value, out bool bUseMedian)
                                    && bool.TryParse(sets[m].Attribute("TransposeImage").Value, out bool bTranspose)
                                    && sets[m].Attribute("BaseName").Value.Length > 3
                                    && sets[m].Attribute("FilterLabel").Value.Length > 1
                                    && sets[m].Attribute("TemplatePrefix").Value.Length > 1
                                    && Int32.TryParse(sets[m].Attribute("ProgramCounter").Value, out int pc)
                                    && pc > 0)
                                {
                                    if (sets[m].Attribute("Type").Value.IndexOf("Sizing", StringComparison.CurrentCultureIgnoreCase) >= 0)
                                    {
                                        if (SizingProgramCounter < 0 || bSelected)
                                        {
                                            SizingProgramCounter = pc;
                                            SetupState[0] = bPerformed;
                                            SetupState[1] = bSuccess;
                                            SetupState[2] = bSelected;

                                        }
                                    }
                                    else
                                    {
                                        if (MaskingProgramCounter < 0 || bSelected)
                                        {
                                            MaskingProgramCounter = pc;
                                            SetupState[3] = bPerformed;
                                            SetupState[4] = bSuccess;
                                            SetupState[5] = bSelected;
                                        }
                                    }

                                    Setups.Add(String.Format(
                                        "ProgC:,{0},Type:,{1},Base:,{2},SubIm:,{3},Templ:,{4},Filter:,{5},Status:,{6},{7},{8}",
                                        pc, sets[m].Attribute("Type").Value, sets[m].Attribute("BaseName").Value,
                                        s_numb, sets[m].Attribute("TemplatePrefix").Value,
                                        sets[m].Attribute("FilterLabel").Value,
                                        bPerformed.ToString(), bSuccess.ToString(), bSelected.ToString()));
                                }
                            }
                        }
                    }
                    Attributes = String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}",
                        ImageName, im.Attribute("Type").Value, im.Attribute("Time").Value,
                        j_numb.ToString("D3"), im_numb.ToString("D3"), rp.ToString(),
                        im.Attribute("ChipPosition").Value, sec.ToString(), Dye,
                        SetNumber.ToString()
                        );
                    return true;
                }
            }
            return false;
        }
        public bool IgnoreImage(int INumber, string Comment, out string Err)
        {
            Err = "";
            if (_XDocLoaded)
            {
                IEnumerable<XElement> imIEnum = from el in ImageDoc.Descendants("Image")
                                                where el.Attribute("ImageNumber") != null
                                                && Int32.TryParse(el.Attribute("ImageNumber").Value, out int w1)
                                                && w1 == INumber
                                                select el;
                if (bool.TryParse(Comment, out bool etmp) && !etmp)
                {
                    Err = "Invalid Comment. Comment cannot be string which evaluates to false";
                    return false;
                }
                if (imIEnum.Count() != 1)
                {
                    Err = String.Format("Number of images found != 1, {0} images found with ImageNumber = {1}", imIEnum, INumber);
                    return false;
                }
                XElement imXE = imIEnum.FirstOrDefault();
                if (Comment.Length < 1)
                    imXE.Attribute("Ignored").Value = "true";
                else
                    imXE.Attribute("Ignored").Value = Comment;


                IEnumerable<XElement> sortedIEnum = ImageDoc.Descendants("ImagesSortedByDye").Descendants("Index").Descendants("SortedImage");
                if (sortedIEnum != null)
                {
                    IEnumerable<XElement> target = from el in sortedIEnum
                                                   where el.Attribute("ImageNumber") != null
                                                   && Int32.TryParse(el.Attribute("ImageNumber").Value, out int inumb)
                                                   && inumb == INumber
                                                   select el;
                    if (target.Count() == 1 && target.First().Attributes("Ignored") != null)
                    {
                        if (Comment.Length < 1)
                            target.First().Attribute("Ignored").Value = "true";
                        else
                            target.First().Attribute("Ignored").Value = Comment;
                    }
                }

                return true;
            }
            else
            {
                Err = "Image data not loaded";
                return false;
            }
        }

        
        public List<XElement> Find(int SetNumber, string Type, int Rotor, int Chip, int Section, string Dye)
        {
            if (_XDocLoaded)
            {
                IEnumerable<XElement> xel = from el in ImageDoc.Descendants("Image")
                                            where el.Attribute("SetNumber").ToString().IndexOf(SetNumber.ToString()) >= 0
                                            && el.Attribute("Type").ToString().IndexOf(Type.ToString()) >= 0
                                            && el.Attribute("RotorPosition").ToString().IndexOf(Rotor.ToString()) >= 0
                                            && el.Attribute("ChipPosition").ToString().IndexOf(Chip.ToString()) >= 0
                                            && el.Attribute("Section").ToString().IndexOf(Section.ToString()) >= 0
                                            && el.Attribute("Dye").ToString().IndexOf(Dye) >= 0
                                            select el;
                return xel.ToList();
            }
            else
                return new List<XElement>();
        }
        public List<XElement> GetImages(string ImageType = null)
        {
            List<XElement> result = new List<XElement>();
            if (_XDocLoaded)
            {
                if (ImageType != null)
                {
                    IEnumerable<XElement> xel = from el in ImageDoc.Descendants("Image")
                                                where el.Attribute("Type") != null
                                                && el.Attribute("Type").Value.IndexOf(ImageType) >= 0
                                                && el.Attribute("Ignored") != null
                                                && bool.TryParse(el.Attribute("Ignored").Value, out bool ig)
                                                && !ig
                                                select el;
                    // Console.WriteLine(String.Format("final result.Count: {0}", xel.Count()));
                    return xel.ToList();
                }
                else
                {
                    IEnumerable<XElement> xel = from el in ImageDoc.Descendants("Image")
                                                select el;
                    //Console.WriteLine(String.Format("final result.Count: {0}", xel.Count()));
                    return xel.ToList();
                }
            }
            //Console.WriteLine(String.Format("final result.Count: {0}", result.Count));
            return result;
        }
        public List<XElement> GetImagesOneAttribute(string Attribute, string Value)
        {
            if (_XDocLoaded)
            {
                IEnumerable<XElement> xel = from el in ImageDoc.Descendants("Image")
                                            where el.Attribute(Attribute) != null
                                            && el.Attribute(Attribute).Value.IndexOf(Value) >= 0
                                            && el.Attribute("Ignored") != null
                                            && bool.TryParse(el.Attribute("Ignored").Value, out bool ig)
                                            && !ig
                                            select el;
                return xel.ToList();
            }
            else
                return new List<XElement>();
        }
        public List<XElement> GetImageByImageNumber(int ImageNumber)
        {
            if (_XDocLoaded)
            {
                List<XElement> selectedImage = new List<XElement>();
                List<XElement> allImagesList = ImageDoc.Descendants("Image").ToList();
                if (allImagesList.Count < 1)
                    return new List<XElement>();

                for (int n = 0; n < allImagesList.Count; n++)
                {
                    var vIm = allImagesList[n].Attribute("ImageNumber");
                    if (vIm != null)
                    {
                        string sIm = vIm.Value;
                        if (Int32.TryParse(sIm, out int iNumb) && iNumb == ImageNumber)
                        {
                            selectedImage.Add(allImagesList[n]);
                        }
                    }
                    else
                        return new List<XElement>();
                }
                if (selectedImage.Count == 1)
                    return selectedImage;
                else
                    return new List<XElement>();
            }
            else
                return new List<XElement>();
        }
        public List<XElement> GetImagesTwoAttributes(string Attribute1, string Value1, string Attribute2, string Value2)
        {
            if (_XDocLoaded)
            {
                IEnumerable<XElement> xel = from el in ImageDoc.Descendants("Image")
                                            where el.Attribute(Attribute1) != null
                                            && el.Attribute(Attribute2) != null
                                            && el.Attribute(Attribute1).Value.IndexOf(Value1) >= 0
                                            && el.Attribute(Attribute2).Value.IndexOf(Value2) >= 0
                                            && el.Attribute("Ignored") != null
                                            && bool.TryParse(el.Attribute("Ignored").Value, out bool ig)
                                            && !ig
                                            select el;
                return xel.ToList();
            }
            else
                return new List<XElement>();
        }
        public List<XElement> GetImagesThreeAttributes(string Attribute1, string Value1, string Attribute2, string Value2, string Attribute3, int Value3)
        {
            if (_XDocLoaded)
            {
                List<XElement> selectedImages = new List<XElement>();
                List<XElement> selectedForTwoImages = GetImagesTwoAttributes(Attribute1, Value1, Attribute2, Value2);
                if (selectedForTwoImages.Count < 1)
                    return new List<XElement>();

                for (int n = 0; n < selectedForTwoImages.Count; n++)
                {
                    var vIm = selectedForTwoImages[n].Attribute(Attribute3);
                    if (vIm != null)
                    {
                        string sIm = vIm.Value;
                        if (Int32.TryParse(sIm, out int iNumb) && iNumb == Value3)
                        {
                            selectedImages.Add(selectedForTwoImages[n]);
                        }
                    }
                    else
                        return new List<XElement>();
                }
                return selectedImages;
            }
            else
                return new List<XElement>();
        }
        public List<XElement> FindAttribute(List<XElement> ImageList, string Att, int Value)
        {
            List<XElement> xel = new List<XElement>();
            for (int n = 0; n < ImageList.Count; n++)
            {
                XAttribute val = ImageList[n].Attribute(Att);
                if (val != null)
                {
                    if (val.Value.IndexOf(Value.ToString()) >= 0)
                        xel.Add(ImageList[n]);
                }
            }
            return xel;
        }

        public bool GetImageCount(out int NumbBeforeImages, out int NumbAfterImages)
        {
            if (_XDocLoaded && _settingsLoaded)
            {
                List<XElement> imageListBefore = GetImages("Before");
                List<XElement> imageListAfter = GetImages("After");
                NumbBeforeImages = imageListBefore.Count;
                NumbAfterImages = imageListAfter.Count;
                Console.WriteLine(String.Format("numb images: {0}/{1}", imageListBefore.Count, imageListAfter.Count));
                return true;
            }
            else
            {
                Console.WriteLine(String.Format("_XDocLoaded({0}). _settingsLoaded({1})", _XDocLoaded, _settingsLoaded));
                NumbBeforeImages = 0;
                NumbAfterImages = 0;
                return false;
            }
        }
        
        public bool AddImage(string ImageName, bool Before, int SetNumber, string Dye, int Section, string ChipPositionString, int RotorPosition,
            string TimeIn, out string Err)
        {
            Err = "";
            bool error = false;
            string bString = "";
            string jString = "";
            if (_XDocLoaded || _settingsLoaded)
            {
                if (ImageName.Length < 6)
                {
                    Err += "Image name is too short";
                    error = true;
                }
                if (Before)
                    bString = "Before";
                else
                    bString = "After";

                if (SetNumber < 0)
                {
                    Err += String.Format("SetNumber must be >= 0. {0}/ ", SetNumber);
                    error = true;
                }
                if (Dye.Length < 1)
                {
                    Err += "Dye string is empty. / ";
                    error = true;
                }
                if (Section < 1 || Section > NumbSections)
                {
                    Err += String.Format("Invalid Section value. {0}-{1} / ", Section, NumbSections);
                    error = true;
                }
                int chipPosition = -1;
                if (ChipPositionString.IndexOf("Left", StringComparison.CurrentCultureIgnoreCase) >= 0)
                {
                    chipPosition = 0;
                }
                else if (ChipPositionString.IndexOf("Right", StringComparison.CurrentCultureIgnoreCase) >= 0)
                {
                    chipPosition = 1;
                }
                else
                {
                    Err += String.Format("Invalid ChipPosition. {0} / ", ChipPositionString);
                    error = true;
                }

                if (RotorPosition < 1 || RotorPosition > 4)
                {
                    Err += String.Format("Invalid RotorPosition. {0} / ", RotorPosition);
                    error = true;
                }
                if (TimeIn.Length < 1)
                {
                    Err += "Empty Time String. / ";
                    error = true;
                }
                if (!error)
                {
                    List<XElement> im = ImageDoc.Descendants("Images").ToList();
                    if (im.Count != 1)
                    {
                        Err += "Problem with <Images> node in ImageDoc";
                        error = true;
                    }
                    else
                    {
                        string pattern = @"_J(\d+)_";
                        List<System.Text.RegularExpressions.Match> jStrList = new List<System.Text.RegularExpressions.Match>();
                        foreach (System.Text.RegularExpressions.Match jStr in
                            System.Text.RegularExpressions.Regex.Matches(ImageName, pattern))
                        {
                            jStrList.Add(jStr);
                        }
                        if (jStrList.Count == 1)
                        {
                            int jNumb;
                            if (Int32.TryParse(jStrList[0].Groups[1].Value, out jNumb))
                                jString = jNumb.ToString("D3");
                            else
                                jString = "000";
                        }
                        else
                            jString = "000";
                        int newImageCounter = ImageCounter + 1;
                        XElement newIM = new XElement("Image",
                            new XAttribute("Name", ImageName),
                            new XAttribute("Type", bString),
                            new XAttribute("SetNumber", SetNumber.ToString()),
                            new XAttribute("Dye", Dye),
                            new XAttribute("Section", Section.ToString()),
                            new XAttribute("ChipPosition", ChipPositionString),
                            new XAttribute("RotorPosition", RotorPosition.ToString()),
                            new XAttribute("ImageNumber", newImageCounter.ToString("D3")),
                            new XAttribute("JNumber", jString),
                            new XAttribute("Time", TimeIn),
                            new XAttribute("Ignored", "false")
                            );
                        XElement newSetup = new XElement("AnalysisSetup",
                            new XAttribute("CollectionType", "Analysis"));
                        newIM.Add(newSetup);
                        im[0].Add(newIM);

                        if (ImageDoc.Descendants("ImagesSortedByDye") != null)
                        {
                            List<XElement> collated = ImageDoc.Descendants("ImagesSortedByDye").ToList();
                            bool addToSortedList = true;
                            if (collated.Count == 1)
                            {
                                IEnumerable<XElement> dyeIEnum = from el in collated[0].Descendants("Dye")
                                                                 where el.Attribute("DyeName") != null
                                                                 && el.Attribute("DyeName").Value.IndexOf(Dye, StringComparison.CurrentCultureIgnoreCase) >= 0
                                                                 select el;
                                if (dyeIEnum.Count() == 0)
                                {
                                    XElement newDye = NewSortByDyeNode(Dye);
                                    if (newDye != null)
                                        collated[0].Add(newDye);
                                    else
                                        addToSortedList = false;
                                }

                                if (addToSortedList)
                                {
                                    IEnumerable<XElement> sortedDyeIEnum = from el in ImageDoc.Descendants("ImagesSortedByDye").Descendants("Dye")
                                                                           where el.Attribute("DyeName") != null
                                                                           && el.Attribute("DyeName").Value.IndexOf(Dye, StringComparison.CurrentCultureIgnoreCase) >= 0
                                                                           select el;
                                    if (sortedDyeIEnum != null && sortedDyeIEnum.Count() == 1)
                                    {
                                        XElement sortedDye = sortedDyeIEnum.First();
                                        int idx = RCSid.IndexOut(RotorPosition, chipPosition, Section);
                                        //Console.WriteLine(String.Format("RCSid {0}: {1} - {2}/{3}/{4} -- {5}/{6}",
                                        //    newImageCounter, idx, RotorPosition, chipPosition, Section, RCSid.Base, RCSid.Size));
                                        IEnumerable<XElement> sortedDyeRCSIEnum = from el in sortedDye.Descendants("Index")
                                                                                  where el.Attribute("Value") != null
                                                                                  && Int32.TryParse(el.Attribute("Value").Value, out int val)
                                                                                  && val == idx
                                                                                  select el;
                                        if (sortedDyeRCSIEnum != null && sortedDyeRCSIEnum.Count() == 1)
                                        {
                                            XElement rcsNode = sortedDyeRCSIEnum.First();
                                            XElement imInfo = new XElement("SortedImage",
                                                new XAttribute("Name", ImageName),
                                                new XAttribute("ImageNumber", newImageCounter.ToString("D3")),
                                                new XAttribute("RotorPosition", RotorPosition.ToString()),
                                                new XAttribute("ChipPosition", ChipPositionString),
                                                new XAttribute("Section", Section.ToString()),
                                                new XAttribute("Type", bString),
                                                new XAttribute("SetNumber", SetNumber.ToString()),
                                                new XAttribute("Ignored", "false")
                                                );
                                            rcsNode.Add(imInfo);
                                        }
                                    }
                                }
                            }
                        }
                        ImageCounter = newImageCounter;
                        SetImageCounter(newImageCounter);
                    }
                }
            }
            return !error;
        }
    };
}
