﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.ExceptionServices;
using System.Data;

namespace IThree
{
    public class IdxThree
    {
        int XSize;
        bool Xzero;
        int YSize;
        bool Yzero;
        int ZSize;
        bool Zzero;
        int XMult;
        int YMult;
        public int Base;
        public int Size;
        public IdxThree()
        {
            XSize = 4; Xzero = false; YSize = 2; Yzero = true; ZSize = 3; Zzero = false;
            if (Xzero)
                Base = 0;
            else
                Base = YSize * ZSize;
            if (!Yzero)
                Base += ZSize;
            if (!Zzero)
                Base++;

            XMult = YSize * ZSize;
            YMult = ZSize;
            Size = XSize * YSize * ZSize;
        }
        public IdxThree(int XS, bool XZ, int YS, bool YZ, int ZS, bool ZZ)
        {
            XSize = XS;
            Xzero = XZ;
            YSize = YS;
            Yzero = YZ;
            ZSize = ZS;
            Zzero = ZZ;

            if (Xzero)
                Base = 0;
            else
                Base = YSize * ZSize;
            if (!Yzero)
                Base += ZSize;
            if (!Zzero)
                Base++;

            XMult = YSize * ZSize;
            YMult = ZSize;
            Size = XSize * YSize * ZSize;
        }
        ~IdxThree() { }

        public int IndexOut(int X, int Y, int Z)
        {
            return X * XMult + Y * YMult + Z - Base;
        }
        public bool DecodeIndex(int Index, out int X, out int Y, out int Z)
        {
            int tx = (int)Math.Floor((double)Index / XMult);
            int ty = (int)Math.Floor(((double)Index - tx * XMult) / YMult);
            int tz = Index - tx * XMult - ty * YMult;
            X = 0;
            Y = 0;
            Z = 0;
            if (tx < 0 || ty < 0 || tz < 0)
                return false;
            if (Xzero)
            {
                if (tx + 1 > XSize)
                    return false;
            }
            else
            {
                tx++;
                if (tx > XSize)
                    return false;
            }
            if (Yzero)
            {
                if (ty + 1 > YSize)
                    return false;
            }
            else
            {
                ty++;
                if (ty > YSize)
                    return false;
            }
            if (Zzero)
            {
                if (tz + 1 > ZSize)
                    return false;
            }
            else
            {
                tz++;
                if (tz > ZSize)
                    return false;
            }
            X = tx;
            Y = ty;
            Z = tz;
            return true;
        }
    };
}
namespace manageAnalysis
{
    using IThree;
    using System.Collections.Specialized;
    using System.Runtime.Remoting.Messaging;
    using System.Security.Cryptography;

    public class ImageData
    {
        char[] delim = new char[] { ' ', '\n', '\t' };
        protected XDocument ImageDoc;
        bool _XDocLoaded;
        bool _settingsLoaded;
        IdxThree RCSid;
        public string Device
        {
            get; protected set;
        }
        public int NumbArrays
        {
            get; protected set;
        }
        public int NumbSections
        {
            get; protected set;
        }
        public string WorkingDirectory
        {
            get; protected set;
        }
        public string TemplateDirectory
        {
            get; protected set;
        }
        public int ProgramCounter
        {
            get; protected set;
        }
        public int ImageCounter
        {
            get; protected set;
        }
        public bool UseMedianFilter
        {
            get; protected set;
        }
        public bool RotateImage
        {
            get; protected set;
        }
        public int ExtraOutput
        {
            get; protected set;
        }
        public string BaseNameStub
        {
            get; protected set;
        }

        public int ListXELemntToStringArray(List<XElement> XList, out string[] SArray)
        {
            List<string> sList = new List<string>(0);

            for (int n = 0; n < XList.Count; n++)
                sList.Add(XList[n].ToString());

            SArray = sList.ToArray();
            return SArray.Count();
        }
        public bool GetImageDataArray(int ImageNumber, out string ImageName, out bool Before, out int RotorPosition,
            out bool Left, out int Section, out String Dye, out int SetNumber,
            out string Attributes, out string[] Setups, out int SizingProgramCounter, out int MaskingProgramCounter,
            out bool[] SetupState)
        {
            bool res = GetImageData(ImageNumber, out ImageName, out Before, out RotorPosition,
                out Left, out Section, out Dye, out SetNumber,
                out Attributes, out List<string> setupList, out SizingProgramCounter, out MaskingProgramCounter,
                out SetupState);
            Setups = setupList.ToArray();
            return res;
        }
        public bool GetImageData(int ImageNumber, out string ImageName, out bool Before, out int RotorPosition,
            out bool Left, out int Section, out String Dye, out int SetNumber,
            out string Attributes, out List<string> Setups, out int SizingProgramCounter, out int MaskingProgramCounter,
            out bool[] SetupState)
        {
            ImageName = "";
            Attributes = "";
            Setups = new List<string>(0);
            Before = true;
            RotorPosition = 0;
            Left = true;
            Section = 0;
            Dye = "";
            SetNumber = -1;
            SizingProgramCounter = -1;
            MaskingProgramCounter = -1;
            SetupState = new bool[6];
            if (!_XDocLoaded)
                return false;

            List<XElement> iList = ImageDoc.Descendants("Image").ToList();
            if (iList.Count < 1)
                return false;
            int target = -1;
            for (int n = 0; n < iList.Count; n++)
            {
                if (iList[n].Attribute("ImageNumber") != null)
                {
                    if (Int32.TryParse(iList[n].Attribute("ImageNumber").Value, out int w1))
                    {
                        if (w1 == ImageNumber)
                        {
                            target = n;
                            break;
                        }
                    }
                }
            }
            if (target < 0)
                return false;
            XElement im = iList[target];
            if (im.Attribute("Type") != null && im.Attribute("RotorPosition") != null
                && im.Attribute("ChipPosition") != null && im.Attribute("Section") != null
                && im.Attribute("Dye") != null && im.Attribute("Name") != null && im.Attribute("SetNumber") != null
                && im.Attribute("ImageNumber") != null && im.Attribute("JNumber") != null && im.Attribute("Time") != null)
            {
                if (Int32.TryParse(im.Attribute("RotorPosition").Value, out int rp)
                    && Int32.TryParse(im.Attribute("Section").Value, out int sec)
                    && (im.Attribute("Type").Value.IndexOf("Before", StringComparison.CurrentCultureIgnoreCase) >= 0 ||
                    im.Attribute("Type").Value.IndexOf("After", StringComparison.CurrentCultureIgnoreCase) >= 0)
                    && (im.Attribute("ChipPosition").Value.IndexOf("Left", StringComparison.CurrentCultureIgnoreCase) >= 0 ||
                    im.Attribute("ChipPosition").Value.IndexOf("Right", StringComparison.CurrentCultureIgnoreCase) >= 0)
                    && Int32.TryParse(im.Attribute("ImageNumber").Value, out int im_numb)
                    && Int32.TryParse(im.Attribute("JNumber").Value, out int j_numb)
                    && Int32.TryParse(im.Attribute("SetNumber").Value, out int s_numb)
                    && im.Attribute("Dye").Value.Length > 1 && im.Attribute("Name").Value.Length > 5
                    && im_numb > 0 && j_numb > 0 && s_numb >= 0)
                {
                    ImageName = im.Attribute("Name").Value;
                    if (im.Attribute("Type").Value.IndexOf("Before") >= 0)
                        Before = true;
                    else
                        Before = false;
                    RotorPosition = rp;
                    if (im.Attribute("ChipPosition").Value.IndexOf("Left") >= 0)
                        Left = true;
                    else
                        Left = false;
                    Section = sec;
                    Dye = im.Attribute("Dye").Value;
                    SetNumber = s_numb;
                    List<XElement> sets = im.Descendants("AnalysisSetup").Descendants("Analysis").ToList();
                    if (sets.Count > 0)
                    {
                        for (int m = 0; m < sets.Count; m++)
                        {
                            if (sets[m].Attribute("Type") != null && sets[m].Attribute("Selected") != null
                                && sets[m].Attribute("Performed") != null && sets[m].Attribute("Sucess") != null
                                && sets[m].Attribute("ExtraOutput") != null && sets[m].Attribute("UseMedianFilter") != null
                                && sets[m].Attribute("TransposeImage") != null && sets[m].Attribute("BaseName") != null
                                && sets[m].Attribute("FilterLabel") != null && sets[m].Attribute("TemplatePrefix") != null
                                && sets[m].Attribute("ProgramCounter") != null)
                            {
                                if ((sets[m].Attribute("Type").Value.IndexOf("Sizing", StringComparison.CurrentCultureIgnoreCase) >= 0 ||
                                    sets[m].Attribute("Type").Value.IndexOf("Masking", StringComparison.CurrentCultureIgnoreCase) >= 0)
                                    && bool.TryParse(sets[m].Attribute("Selected").Value, out bool bSelected)
                                    && bool.TryParse(sets[m].Attribute("Performed").Value, out bool bPerformed)
                                    && bool.TryParse(sets[m].Attribute("Success").Value, out bool bSuccess)
                                    && Int32.TryParse(sets[m].Attribute("ExtraOutput").Value, out int extraOutput)
                                    && bool.TryParse(sets[m].Attribute("UseMedianFilter").Value, out bool bUseMedian)
                                    && bool.TryParse(sets[m].Attribute("TransposeImage").Value, out bool bTranspose)
                                    && sets[m].Attribute("BaseName").Value.Length > 3
                                    && sets[m].Attribute("FilterLabel").Value.Length > 1
                                    && sets[m].Attribute("TemplatePrefix").Value.Length > 1
                                    && Int32.TryParse(sets[m].Attribute("ProgramCounter").Value, out int pc)
                                    && pc > 0)
                                {
                                    if (sets[m].Attribute("Type").Value.IndexOf("Sizing", StringComparison.CurrentCultureIgnoreCase) >= 0)
                                    {
                                        if (SizingProgramCounter < 0 || bSelected)
                                        {
                                            SizingProgramCounter = pc;
                                            SetupState[0] = bPerformed;
                                            SetupState[1] = bSuccess;
                                            SetupState[2] = bSelected;

                                        }
                                    }
                                    else
                                    {
                                        if (MaskingProgramCounter < 0 || bSelected)
                                        {
                                            MaskingProgramCounter = pc;
                                            SetupState[3] = bPerformed;
                                            SetupState[4] = bSuccess;
                                            SetupState[5] = bSelected;
                                        }
                                    }

                                    Setups.Add(String.Format(
                                        "ProgC:,{0},Type:,{1},Base:,{2},SubIm:,{3},Templ:,{4},Filter:,{5},Status:,{6},{7},{8}",
                                        pc, sets[m].Attribute("Type").Value, sets[m].Attribute("BaseName").Value,
                                        s_numb, sets[m].Attribute("TemplatePrefix").Value,
                                        sets[m].Attribute("FilterLabel").Value,
                                        bPerformed.ToString(), bSuccess.ToString(), bSelected.ToString()));
                                }
                            }
                        }
                    }
                    Attributes = String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}",
                        ImageName, im.Attribute("Type").Value, im.Attribute("Time").Value,
                        j_numb.ToString("D3"), im_numb.ToString("D3"), rp.ToString(),
                        im.Attribute("ChipPosition").Value, sec.ToString(), Dye,
                        SetNumber.ToString()
                        );
                    return true;
                }
            }
            return false;
        }
        public bool IgnoreImage(int INumber, string Comment, out string Err)
        {
            Err = "";
            if (_XDocLoaded)
            {
                IEnumerable<XElement> imIEnum = from el in ImageDoc.Descendants("Image")
                                                where el.Attribute("ImageNumber") != null
                                                && Int32.TryParse(el.Attribute("ImageNumber").Value, out int w1)
                                                && w1 == INumber
                                                select el;
                if (bool.TryParse(Comment, out bool etmp) && !etmp)
                {
                    Err = "Invalid Comment. Comment cannot be string which evaluates to false";
                    return false;
                }
                if (imIEnum.Count() != 1)
                {
                    Err = String.Format("Number of images found != 1, {0} images found with ImageNumber = {1}", imIEnum, INumber);
                    return false;
                }
                XElement imXE = imIEnum.FirstOrDefault();
                if (Comment.Length < 1)
                    imXE.Attribute("Ignored").Value = "true";
                else
                    imXE.Attribute("Ignored").Value = Comment;


                IEnumerable<XElement> sortedIEnum = ImageDoc.Descendants("ImagesSortedByDye").Descendants("Index").Descendants("SortedImage");
                if (sortedIEnum != null)
                {
                    IEnumerable<XElement> target = from el in sortedIEnum
                                                   where el.Attribute("ImageNumber") != null
                                                   && Int32.TryParse(el.Attribute("ImageNumber").Value, out int inumb)
                                                   && inumb == INumber
                                                   select el;
                    if (target.Count() == 1 && target.First().Attributes("Ignored") != null)
                    {
                        if (Comment.Length < 1)
                            target.First().Attribute("Ignored").Value = "true";
                        else
                            target.First().Attribute("Ignored").Value = Comment;
                    }
                }

                return true;
            }
            else
            {
                Err = "Image data not loaded";
                return false;
            }
        }

        public ImageData(string BaseName = null)
        {
            ImageDoc = new XDocument();
            _XDocLoaded = false;
            _settingsLoaded = false;
            Device = "None";
            WorkingDirectory = "None";
            TemplateDirectory = "None";
            ProgramCounter = 0;
            ImageCounter = 0;
            UseMedianFilter = true;
            RotateImage = false;
            NumbArrays = 0;
            NumbSections = 0;
            ExtraOutput = 0;
            if (BaseName != null)
                BaseNameStub = BaseName;
            else
            {
                DateTime now = DateTime.Now;
                BaseNameStub = now.ToString("SDPCRyyyyMMdd_HHmmss");
            }
        }
        ~ImageData() { }

        public bool Load(string DocString, out string Message)
        {
            _settingsLoaded = false;
            try
            {
                ImageDoc = XDocument.Parse(DocString);
            }
            catch (Exception ex)
            {
                _XDocLoaded = false;
                Message = ex.ToString();
                return false;
            }
            _XDocLoaded = true;
            return LoadSettings(out Message);
        }
        public bool CreateEmpty(string DeviceIn, string DeviceType, string ProjectIn, string WorkingDirectoryIn, string TemplateDirectoryIn,
            bool UseMedianFilterIn, bool RotateImageIn, int NumbRotorPositions, int NumbChipPositions, int NumbSections, int NumbArrays, string DateTimeIn, out string Err, string BaseName = null)
        {
            XDocument imManage = new XDocument();
            var rootElement = new XElement("SDPCRAnalysis");
            imManage.Add(rootElement);
            char[] delim = new char[] { ' ', ',', '\n', '\t' };
            string line = DateTimeIn.Trim(delim);
            string line2;
            string line3;
            RCSid = new IdxThree(NumbRotorPositions, false, NumbChipPositions, true, NumbSections, false);
            XElement xset = SimpleNode("Settings");
            var xel = new XElement("Date", new XAttribute("Value", line));
            xset.Add(xel);
            line = ProjectIn.Trim(delim);
            xel = new XElement("Project", new XAttribute("Name", line));
            xset.Add(xel);
            line = WorkingDirectoryIn.Trim(delim);
            xel = new XElement("Directory",
                new XAttribute("Type", "Working"),
                new XAttribute("Name", line));
            xset.Add(xel);
            line = TemplateDirectoryIn.Trim(delim);
            xel = new XElement("Directory",
                new XAttribute("Type", "Template"),
                new XAttribute("Name", line));
            xset.Add(xel);
            var xrotor = new XElement("Rotor",
                new XAttribute("NumbRotorPositions", NumbRotorPositions.ToString()),
                new XAttribute("NumbChipPositions", NumbChipPositions.ToString()),
                new XAttribute("RCSid_Size", RCSid.Size.ToString()));
            xset.Add(xrotor);

            line = DeviceIn.Trim(delim);
            line2 = DeviceType.Trim(delim);
            if (RotateImageIn)
                line3 = "true";
            else
                line3 = "false";


            xel = new XElement("Device",
                new XAttribute("CatNumber", line),
                new XAttribute("Type", line2),
                new XAttribute("NumbSections", NumbSections.ToString()),
                new XAttribute("NumbArrays", NumbArrays.ToString()),
                new XAttribute("RotateImage", line3)
                );
            xset.Add(xel);
            if (UseMedianFilterIn)
                line3 = "true";
            else
                line3 = "false";
            xel = new XElement("UseMedianFilter", new XAttribute("Value", line3));
            xset.Add(xel);
            xel = new XElement("ProgramCounter", new XAttribute("Value", "0"));
            xset.Add(xel);
            xel = new XElement("ImageCounter", new XAttribute("Value", "0"));
            xset.Add(xel);
            //xel = new XElement("Dyes", new XAttribute("SizingDye", "DyeA"));
            //xel.Add(new XElement("QuantDye", new XAttribute("Name", "DyeB")));
            //xset.Add(xel);
            rootElement.Add(xset);
            var ximag = CollectionNode("Images", "Image");
            var xquan = CollectionNode("Quantifications", "Quantify");
            var xcomb = CollectionNode("CombineSections", "Combine");
            var xcollate = CollectionNode("ImagesSortedByDye", "Dye");
            rootElement.Add(ximag);
            rootElement.Add(xquan);
            rootElement.Add(xcomb);
            rootElement.Add(xcollate);

            ImageDoc = imManage;
            if (BaseName != null)
                BaseNameStub = BaseName;
            else
            {
                DateTime now = DateTime.Now;
                BaseNameStub = now.ToString("SDPCRyyyyMMdd_HHmmss");
            }
            _XDocLoaded = true;
            return CheckXDocument(out Err, out int _, out int _);
        }
        public bool GetDocString(out string DocString)
        {
            if (_XDocLoaded)
            {
                DocString = ImageDoc.ToString();
                return true;
            }
            else
            {
                DocString = "";
                return false;
            }
        }
        public void ClearDocString()
        {
            if (_XDocLoaded)
            {
                _XDocLoaded = false;
                ImageDoc = new XDocument();
            }
        }
        public List<XElement> GetSetting(string Setting)
        {
            List<XElement> sList = new List<XElement>();
            List<XElement> st = ImageDoc.Descendants("Settings").ToList();
            for (int n = 0; n < st.Count; n++)
            {
                List<XElement> s = st[n].Elements(Setting).ToList();
                if (s.Count > 0)
                {
                    for (int m = 0; m < s.Count; m++)
                        sList.Add(s[m]);
                }
            }
            return sList;
        }
        protected bool SetProgramCounter(int NewCounter)
        {
            List<XElement> st = ImageDoc.Descendants("Settings").ToList();
            if (st.Count < 1)
                return false;
            for (int n = 0; n < st.Count; n++)
            {
                List<XElement> s = st[n].Elements("ProgramCounter").ToList();
                if (s.Count == 1)
                {
                    s[0].Attribute("Value").Value = NewCounter.ToString("D4");
                    //Console.WriteLine(String.Format("s[0] = {0}", s[0].ToString()));
                    return true;
                }
            }
            return false;
        }
        protected bool SetImageCounter(int NewCounter)
        {
            List<XElement> st = ImageDoc.Descendants("Settings").ToList();
            if (st.Count < 1)
                return false;
            for (int n = 0; n < st.Count; n++)
            {
                List<XElement> s = st[n].Elements("ImageCounter").ToList();
                if (s.Count == 1)
                {
                    s[0].Attribute("Value").Value = NewCounter.ToString("D3");
                    //Console.WriteLine(String.Format("s[0] = {0}", s[0].ToString()));
                    return true;
                }
            }
            return false;
        }
        public List<XElement> Find(int SetNumber, string Type, int Rotor, int Chip, int Section, string Dye)
        {
            if (_XDocLoaded)
            {
                IEnumerable<XElement> xel = from el in ImageDoc.Descendants("Image")
                                            where el.Attribute("SetNumber").ToString().IndexOf(SetNumber.ToString()) >= 0
                                            && el.Attribute("Type").ToString().IndexOf(Type.ToString()) >= 0
                                            && el.Attribute("RotorPosition").ToString().IndexOf(Rotor.ToString()) >= 0
                                            && el.Attribute("ChipPosition").ToString().IndexOf(Chip.ToString()) >= 0
                                            && el.Attribute("Section").ToString().IndexOf(Section.ToString()) >= 0
                                            && el.Attribute("Dye").ToString().IndexOf(Dye) >= 0
                                            select el;
                return xel.ToList();
            }
            else
                return new List<XElement>();
        }
        public List<XElement> GetImages(string ImageType = null)
        {
            List<XElement> result = new List<XElement>();
            if (_XDocLoaded)
            {
                if (ImageType != null)
                {
                    IEnumerable<XElement> xel = from el in ImageDoc.Descendants("Image")
                                                where el.Attribute("Type") != null
                                                && el.Attribute("Type").Value.IndexOf(ImageType) >= 0
                                                && el.Attribute("Ignored") != null
                                                && bool.TryParse(el.Attribute("Ignored").Value, out bool ig)
                                                && !ig
                                                select el;
                    // Console.WriteLine(String.Format("final result.Count: {0}", xel.Count()));
                    return xel.ToList();
                }
                else
                {
                    IEnumerable<XElement> xel = from el in ImageDoc.Descendants("Image")
                                                select el;
                    //Console.WriteLine(String.Format("final result.Count: {0}", xel.Count()));
                    return xel.ToList();
                }
            }
            //Console.WriteLine(String.Format("final result.Count: {0}", result.Count));
            return result;
        }
        public List<XElement> GetImagesOneAttribute(string Attribute, string Value)
        {
            if (_XDocLoaded)
            {
                IEnumerable<XElement> xel = from el in ImageDoc.Descendants("Image")
                                            where el.Attribute(Attribute) != null
                                            && el.Attribute(Attribute).Value.IndexOf(Value) >= 0
                                            && el.Attribute("Ignored") != null
                                            && bool.TryParse(el.Attribute("Ignored").Value, out bool ig)
                                            && !ig
                                            select el;
                return xel.ToList();
            }
            else
                return new List<XElement>();
        }
        public List<XElement> GetImageByImageNumber(int ImageNumber)
        {
            if (_XDocLoaded)
            {
                List<XElement> selectedImage = new List<XElement>();
                List<XElement> allImagesList = ImageDoc.Descendants("Image").ToList();
                if (allImagesList.Count < 1)
                    return new List<XElement>();

                for (int n = 0; n < allImagesList.Count; n++)
                {
                    var vIm = allImagesList[n].Attribute("ImageNumber");
                    if (vIm != null)
                    {
                        string sIm = vIm.Value;
                        if (Int32.TryParse(sIm, out int iNumb) && iNumb == ImageNumber)
                        {
                            selectedImage.Add(allImagesList[n]);
                        }
                    }
                    else
                        return new List<XElement>();
                }
                if (selectedImage.Count == 1)
                    return selectedImage;
                else
                    return new List<XElement>();
            }
            else
                return new List<XElement>();
        }
        public List<XElement> GetImagesTwoAttributes(string Attribute1, string Value1, string Attribute2, string Value2)
        {
            if (_XDocLoaded)
            {
                IEnumerable<XElement> xel = from el in ImageDoc.Descendants("Image")
                                            where el.Attribute(Attribute1) != null
                                            && el.Attribute(Attribute2) != null
                                            && el.Attribute(Attribute1).Value.IndexOf(Value1) >= 0
                                            && el.Attribute(Attribute2).Value.IndexOf(Value2) >= 0
                                            && el.Attribute("Ignored") != null
                                            && bool.TryParse(el.Attribute("Ignored").Value, out bool ig)
                                            && !ig
                                            select el;
                return xel.ToList();
            }
            else
                return new List<XElement>();
        }
        public List<XElement> GetImagesThreeAttributes(string Attribute1, string Value1, string Attribute2, string Value2, string Attribute3, int Value3)
        {
            if (_XDocLoaded)
            {
                List<XElement> selectedImages = new List<XElement>();
                List<XElement> selectedForTwoImages = GetImagesTwoAttributes(Attribute1, Value1, Attribute2, Value2);
                if (selectedForTwoImages.Count < 1)
                    return new List<XElement>();

                for (int n = 0; n < selectedForTwoImages.Count; n++)
                {
                    var vIm = selectedForTwoImages[n].Attribute(Attribute3);
                    if (vIm != null)
                    {
                        string sIm = vIm.Value;
                        if (Int32.TryParse(sIm, out int iNumb) && iNumb == Value3)
                        {
                            selectedImages.Add(selectedForTwoImages[n]);
                        }
                    }
                    else
                        return new List<XElement>();
                }
                return selectedImages;
            }
            else
                return new List<XElement>();
        }
        public List<XElement> FindAttribute(List<XElement> ImageList, string Att, int Value)
        {
            List<XElement> xel = new List<XElement>();
            for (int n = 0; n < ImageList.Count; n++)
            {
                XAttribute val = ImageList[n].Attribute(Att);
                if (val != null)
                {
                    if (val.Value.IndexOf(Value.ToString()) >= 0)
                        xel.Add(ImageList[n]);
                }
            }
            return xel;
        }

        protected string RotorChipSubLabel(int Rotor, int Chip, int Section, bool Before, int Set = 0)
        {
            string tmp;
            if (Before)
            {
                tmp = String.Format("B{0}{1}{2}", Rotor, Chip, Section);
            }
            else
            {
                tmp = String.Format("A{0}{1}{2}{3}", Set, Rotor, Chip, Section);
            }
            return tmp;
        }
        protected string RotorChipSubLabel(int Rotor, int Chip, int Section)
        {
            return String.Format("Q{0}{1}{2}", Rotor, Chip, Section);
        }

        protected static XElement CollectionNode(string Name, string Type)
        {
            XElement xe = new XElement(Name, new XAttribute("CollectionType", Type));
            return xe;
        }
        protected static XElement SimpleNode(string Name)
        {
            XElement xe = new XElement(Name);
            return xe;
        }

        public bool CheckXDocument(out string Err, out int NumbBeforeImages, out int NumbAfterImages)
        {
            NumbBeforeImages = 0;
            NumbAfterImages = 0;
            Err = "";
            if (!_XDocLoaded)
            {
                Err = "XDoc not loaded";
                return false;
            }
            if (!LoadSettings(out string errMsg))
            {
                Err = errMsg;
                return false;
            }
            Console.WriteLine("Before GetImageCount");
            GetImageCount(out NumbBeforeImages, out NumbAfterImages);
            return true;
        }
        protected bool LoadSettings(out string Err)
        {
            Err = "";
            if (!_XDocLoaded)
            {
                Err = "XDoc not loaded";
                return false;
            }
            bool error = false;
            List<XElement> xDevice = GetSetting("Device");
            List<XElement> xDirectories = GetSetting("Directory");
            List<XElement> xUseMedian = GetSetting("UseMedianFilter");
            List<XElement> xCounter = GetSetting("ProgramCounter");
            List<XElement> xImage = GetSetting("ImageCounter");
            if (xDevice.Count() != 1)
            {
                Err = "Problem with Device settings node. ";
                error = true;
            }
            if (xDirectories.Count < 1)
            {
                Err += "Problem with Directory settings node. ";
                error = true;
            }
            if (xUseMedian.Count() != 1)
            {
                Err += "Problem with UseMedianFilter settings node. ";
                error = true;
            }
            if (xCounter.Count() != 1)
            {
                Err += "Problem with program counter in settings node";
                error = true;
            }
            if (xImage.Count() != 1)
            {
                Err += "Problem with image counter in settings node";
                error = true;
            }
            if (error)
            {
                _settingsLoaded = false;
                return false;
            }
            string s1;
            string s2;
            int v1;
            bool b1;
            Device = xDevice[0].Attribute("CatNumber").Value;
            if (Device == null)
            {
                Err = "CatNumber not found in Device Node. ";
                error = true;
            }

            if (Int32.TryParse(xDevice[0].Attribute("NumbArrays").Value, out v1))
            {
                if (v1 >= 1)
                    NumbArrays = v1;
                else
                {
                    Err += "Problem with number of number of arrays in Device node. ";
                    error = true;
                }
            }
            else
            {
                Err += "Problem with NumbArryas attribute in Device node. ";
                error = true;
            }
            if (Int32.TryParse(xDevice[0].Attribute("NumbSections").Value, out v1))
            {
                if (v1 >= 1)
                    NumbSections = v1;
                else
                {
                    Err += "Problem with number of number of sections in Device node. ";
                    error = true;
                }
            }
            else
            {
                Err += "Problem with NumbSections attribute in Device node. ";
                error = true;
            }
            if (Int32.TryParse(xCounter[0].Attribute("Value").Value, out v1))
            {
                if (v1 >= 0)
                    ProgramCounter = v1;
                else
                {
                    Err += "ProgramCounter attribute in Device Node must be >= 0. ";
                    error = true;
                }
            }
            else
            {
                Err += "Problem with ProgramCounter attribute in Device node. ";
                error = true;
            }
            if (Int32.TryParse(xImage[0].Attribute("Value").Value, out v1))
            {
                if (v1 >= 0)
                    ImageCounter = v1;
                else
                {
                    Err += "ImageCounter attribute in Device Node must be >= 0. ";
                    error = true;
                }
            }
            else
            {
                Err += "Problem with ImageCounter attribute in Device node. ";
                error = true;
            }
            s1 = xDevice[0].Attribute("RotateImage").Value;
            if (s1 == null)
            {
                Err += "RotateImage attribute not found in Device node. ";
                error = true;
            }
            else if (bool.TryParse(s1, out b1))
            {
                RotateImage = b1;
            }
            else
            {
                Err += "Problem with RotateImage attribute in Device node. ";
                error = true;
            }
            if (bool.TryParse(xUseMedian[0].Attribute("Value").Value, out b1))
            {
                UseMedianFilter = b1;
            }
            else
            {
                Err += "Problems with UseMedianFilter value in Settings node. ";
                error = true;
            }
            s1 = "";
            s2 = "";
            for (int n = 0; n < xDirectories.Count(); n++)
            {
                if (xDirectories[n].Attribute("Type").Value.IndexOf("Working") >= 0)
                {
                    if (s1.Length < 1)
                        s1 = xDirectories[n].Attribute("Name").Value;
                }
                else if (xDirectories[n].Attribute("Type").Value.IndexOf("Template") >= 0)
                {
                    if (s2.Length < 1)
                        s2 = xDirectories[n].Attribute("Name").Value;
                }
                if (s1.Length > 0 && s2.Length > 0)
                {
                    WorkingDirectory = s1;
                    TemplateDirectory = s2;
                    break;
                }
            }
            if (s1.Length < 1 || s2.Length < 1)
            {
                Err += "Problems with one of the directory values in Settings node. ";
                error = true;
            }
            if (error)
                _settingsLoaded = false;
            else
                _settingsLoaded = true;
            return _settingsLoaded;
        }
        public List<string> GetSettings()
        {
            List<string> set = new List<string>();
            if (_XDocLoaded && _settingsLoaded)
            {
                set.Add(String.Format("Device: {0}", Device));
                set.Add(String.Format("RotateImage: {0}", RotateImage));
                set.Add(String.Format("NumbArrays: {0}", NumbArrays));
                set.Add(String.Format("WorkingDirectory: {0}", WorkingDirectory));
                set.Add(String.Format("TemplateDirectory: {0}", TemplateDirectory));
                set.Add(String.Format("ProgramCounter: {0}", ProgramCounter));
                set.Add(String.Format("UseMedianFilter: {0}", UseMedianFilter.ToString()));
                return set;
            }
            else
                return set;
        }
        public bool GetImageCount(out int NumbBeforeImages, out int NumbAfterImages)
        {
            if (_XDocLoaded && _settingsLoaded)
            {
                List<XElement> imageListBefore = GetImages("Before");
                List<XElement> imageListAfter = GetImages("After");
                NumbBeforeImages = imageListBefore.Count;
                NumbAfterImages = imageListAfter.Count;
                Console.WriteLine(String.Format("numb images: {0}/{1}", imageListBefore.Count, imageListAfter.Count));
                return true;
            }
            else
            {
                Console.WriteLine(String.Format("_XDocLoaded({0}). _settingsLoaded({1})", _XDocLoaded, _settingsLoaded));
                NumbBeforeImages = 0;
                NumbAfterImages = 0;
                return false;
            }
        }
        public int AddSizingAnalysis(string ImageType, string SizingDye, int SetNumber, out string Err)
        {
            int newAnalysis = 0;
            Err = "";
            string type;
            bool isBefore = false;
            string fil_suffix;
            if (ImageType.IndexOf("Before") >= 0)
            {
                type = "Before";
                fil_suffix = "_B";
                isBefore = true;
            }
            else if (ImageType.IndexOf("After") >= 0)
            {
                type = "After";
                fil_suffix = "_A";
                isBefore = false;
            }
            else
            {
                Err = "ImageType must be either Before or After";
                return 0;
            }

            if ( SizingDye.Length < 2 )
            {
                Err = "No Sizing Dye";
                return 0;
            }
            if ( SetNumber < 0 )
            {
                Err = "SetNumber must be >= 0";
                return 0;
            }
            List<XElement> imageList = GetImages(type);
            for (int n = 0; n < imageList.Count(); n++)
            {
                int sn;
                if (Int32.TryParse(imageList[n].Attribute("SetNumber").Value, out sn))
                {
                    //Console.WriteLine(String.Format("SetNumber/sn: {0}/{1}, type = {2}", SetNumber, sn, type));
                    if (sn == SetNumber)
                    {
                        //if (FindTypeInfo(imageList[n], "Sizing", out int pCounter, out bool performed, out bool success))
                        //{
                        //Console.WriteLine(String.Format("image {0}, Dye:{1}, requested dye: {2}", n, imageList[n].Attribute("Dye").Value, SizingDye));
                        if (imageList[n].Attribute("Dye").Value.IndexOf(SizingDye) >= 0)
                        {
                            int r;
                            int c;
                            int s;
                            int id;
                            //string cs = "";
                            string imName = imageList[n].Attribute("Name").Value;
                            if (Int32.TryParse(imageList[n].Attribute("RotorPosition").Value, out r)
                                && imageList[n].Attribute("ChipPosition") != null
                                && Int32.TryParse(imageList[n].Attribute("Section").Value, out s)
                                && Int32.TryParse(imageList[n].Attribute("JNumber").Value, out id)
                                )
                            {
                                if (imageList[n].Attribute("ChipPosition").Value.IndexOf("Left") >= 0)
                                    c = 0;
                                else
                                    c = 1;
                                int newProgramCounter = ProgramCounter + 1;
                                string fil = SizingDye + fil_suffix;
                                var xele = new XElement("Analysis",
                                    new XAttribute("ProgramCounter", String.Format("{0:D4}", newProgramCounter)),
                                    new XAttribute("Type", "Sizing"),
                                    new XAttribute("WorkingDirectory", WorkingDirectory),
                                    new XAttribute("TemplateDirectory", TemplateDirectory),
                                    new XAttribute("ImageFileName", imageList[n].Attribute("Name").Value),
                                    new XAttribute("TemplatePrefix", Device),
                                    new XAttribute("FilterLabel", fil),
                                    new XAttribute("SubImage", imageList[n].Attribute("Section").Value),
                                    new XAttribute("BaseName", String.Format("{0}_J{1:D3}_{2}_P{3:D4}",
                                        BaseNameStub, id, RotorChipSubLabel(r, c, s, isBefore), newProgramCounter)),
                                    new XAttribute("TransposeImage", RotateImage.ToString()),
                                    new XAttribute("UseMedianFilter", UseMedianFilter.ToString()),
                                    new XAttribute("ExtraOutput", "0"),
                                    new XAttribute("Performed", "false"),
                                    new XAttribute("Success", "false"),
                                    new XAttribute("Selected", "false")
                                    );

                                var docA = ImageDoc.Descendants("Images").Descendants("Image");
                                //Console.WriteLine(String.Format("docA.Count: {0}", docA.Count()));
                                List<XElement> xdocA = docA.ToList();
                                XElement resultsX = null;
                                for (int m = 0; m < xdocA.Count; m++)
                                {
                                    if (xdocA[m].Attribute("Type").Value.IndexOf(type) >= 0 &&
                                        xdocA[m].Attribute("Name").Value.IndexOf(imName) >= 0)
                                    {
                                        List<XElement> docB = xdocA[m].Descendants("AnalysisSetup").ToList();
                                        //Console.WriteLine(String.Format("docB.Count: {0}", docB.Count));
                                        if (docB.Count() > 0)
                                        {
                                            bool duplicate = false;
                                            for (int k = 0; k < docB.Count; k++)
                                            {
                                                List<XElement> rEl = docB[k].Elements("Analysis").ToList();
                                                if (rEl.Count() > 0)
                                                {
                                                    for (int j = 0; j < rEl.Count; j++)
                                                    {
                                                        if ((xele.Attribute("Type").Value == rEl[j].Attribute("Type").Value)
                                                            && (xele.Attribute("WorkingDirectory").Value == rEl[j].Attribute("WorkingDirectory").Value)
                                                            && (xele.Attribute("TemplateDirectory").Value == rEl[j].Attribute("TemplateDirectory").Value)
                                                            && (xele.Attribute("ImageFileName").Value == rEl[j].Attribute("ImageFileName").Value)
                                                            && (xele.Attribute("TemplatePrefix").Value == rEl[j].Attribute("TemplatePrefix").Value)
                                                            && (xele.Attribute("FilterLabel").Value == rEl[j].Attribute("FilterLabel").Value)
                                                            && (xele.Attribute("SubImage").Value == rEl[j].Attribute("SubImage").Value)
                                                            && (xele.Attribute("TransposeImage").Value == rEl[j].Attribute("TransposeImage").Value)
                                                            && (xele.Attribute("UseMedianFilter").Value == rEl[j].Attribute("UseMedianFilter").Value)
                                                            && (xele.Attribute("ExtraOutput").Value == rEl[j].Attribute("ExtraOutput").Value))
                                                        {
                                                            duplicate = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                                if (duplicate)
                                                    break;
                                            }
                                            if (!duplicate)
                                                resultsX = docB.FirstOrDefault();
                                            break;
                                        }
                                    }
                                }

                                if (resultsX != null)
                                {
                                    resultsX.Add(xele);
                                    ProgramCounter = newProgramCounter;
                                    SetProgramCounter(newProgramCounter);
                                    newAnalysis++;
                                }
                            }
                            else
                            {
                                Err += String.Format("Problem with image attributes for image {0}.  ", n);
                                Console.WriteLine(String.Format("Problem with image attributes for image {0}.  ", n));
                                Console.WriteLine(String.Format("     Name: {0}", imageList[n].Attribute("Name").Value));
                                Console.WriteLine(String.Format("     Rotor: {0}", imageList[n].Attribute("RotorPosition").Value));
                                Console.WriteLine(String.Format("     Chip: {0}", imageList[n].Attribute("ChipPosition").Value));
                                Console.WriteLine(String.Format("     Section: {0}", imageList[n].Attribute("Section").Value));
                                Console.WriteLine(String.Format("     ImageNumber: {0}", imageList[n].Attribute("ImageNumber").Value));
                            }
                        }
                        //}
                        //else
                        //{
                        //    Err += String.Format("Could not get Type info from image {0}.  ", n);
                        //    Console.WriteLine(String.Format("Could not get Type info from image {0}.  ", n));
                        //}
                    }
                }
                else
                {
                    Err += String.Format("Could not read SetNumber from image {0}.  ", n);
                    Console.WriteLine(String.Format("Could not read SetNumber from image {0}.  ", n));
                }
            }
            return newAnalysis;
        }
        public int AddMaskingAnalysis(string ImageType, string SizingDye, string MaskingDye, int SetNumb, out string Err,
            int ColorShiftX = 0, int ColorShiftY = 0, int Dilation = 1)
        {
            int newAnalysis = 0;
            Err = "";
            string type;
            bool isBefore = false;
            string fil_suffix;
            if (ImageType.IndexOf("Before", StringComparison.CurrentCultureIgnoreCase) >= 0)
            {
                type = "Before";
                fil_suffix = "_B";
                isBefore = true;
            }
            else if (ImageType.IndexOf("After", StringComparison.CurrentCultureIgnoreCase) >= 0)
            {
                type = "After";
                fil_suffix = "_A";
                isBefore = false;
            }
            else
            {
                Err = "ImageType must be either Before or After";
                return 0;
            }
            if (SizingDye.Length < 2)
            {
                Err = "No Sizing Dye";
                return 0;
            }
            if ( MaskingDye.Length < 2)
            {
                Err = "No Masking Dye";
                return 0;
            }
            if (SetNumb < 0)
            {
                Err = "SetNumber must be >= 0";
                return 0;
            }
            //int XS, bool XZ, int YS, bool YZ, int ZS, bool ZZ

            IEnumerable<XElement> sortedSizingIEnum = from el in ImageDoc.Descendants("ImagesSortedByDye").Descendants("Dye")
                                                      where el.Attribute("DyeName") != null
                                                      && el.Attribute("DyeName").Value.IndexOf(SizingDye, StringComparison.CurrentCultureIgnoreCase) >= 0
                                                      select el;
            IEnumerable<XElement> sortedMaskingIEnum = from el in ImageDoc.Descendants("ImagesSortedByDye").Descendants("Dye")
                                                     where el.Attribute("DyeName") != null
                                                     && el.Attribute("DyeName").Value.IndexOf(MaskingDye, StringComparison.CurrentCultureIgnoreCase) >= 0
                                                    
                                                     select el;
            if ( sortedSizingIEnum.Count() < 1 )
            {
                Err = String.Format("No SortedImage node with Sizing Dye: {0} were found", SizingDye);
                return 0;
            }
            if (sortedMaskingIEnum.Count() < 1 )
            {
                Err = String.Format("No SortedImage node with Masking Dye: {0} were found", MaskingDye);
                return 0;
            }
            IEnumerable<XElement> imageSizingIEnum = from el in sortedSizingIEnum.Descendants("Index").Descendants("SortedImage")
                                                     where el.Attribute("Type") != null
                                                     && el.Attribute("Type").Value.IndexOf(type, StringComparison.CurrentCultureIgnoreCase) >= 0
                                                     && el.Attribute("SetNumber") != null
                                                     && Int32.TryParse(el.Attribute("SetNumber").Value, out int sizingSet)
                                                     && sizingSet == SetNumb
                                                     && el.Attribute("Ignored") != null
                                                     && bool.TryParse(el.Attribute("Ignored").Value, out bool igs)
                                                     && !igs
                                                     select el;

            IEnumerable<XElement> imageMaskingIEnum = from el in sortedMaskingIEnum.Descendants("Index").Descendants("SortedImage")
                                                      where el.Attribute("Type") != null
                                                      && el.Attribute("Type").Value.IndexOf(type, StringComparison.CurrentCultureIgnoreCase) >= 0
                                                      && el.Attribute("SetNumber") != null
                                                      && Int32.TryParse(el.Attribute("SetNumber").Value, out int maskingSet)
                                                      && maskingSet == SetNumb
                                                      && el.Attribute("Ignored") != null
                                                      && bool.TryParse(el.Attribute("Ignored").Value, out bool igm)
                                                      && !igm
                                                      select el;
            if ( imageSizingIEnum.Count() < 1 )
            {
                Err = String.Format("No images with Sizing Dye: {0} were found", SizingDye);
                return 0;
            }
            if (imageMaskingIEnum.Count() < 1)
            {
                Err = "";
                return 0;
            }
            List<XElement> imageSizingList = imageSizingIEnum.ToList();
            List<XElement> imageMaskingList = imageMaskingIEnum.ToList();
            for ( int m = 0; m < imageMaskingList.Count; m++)
            {
                bool addSetup = false;
                XElement xele = new XElement("Unused");
                XElement maskingImage = new XElement("Unused");
                int newProgramCounter = 0;
                if ( imageMaskingList[m].Parent.Attribute("Value") != null 
                    && Int32.TryParse(imageMaskingList[m].Parent.Attribute("Value").Value, out int maskingIdx) 
                    && imageMaskingList[m].Attribute("ImageNumber") != null
                    && Int32.TryParse(imageMaskingList[m].Attribute("ImageNumber").Value, out int maskingNumber)
                    )
                {
                    for ( int n = 0; n < imageSizingList.Count; n++)
                    {
                        if (imageSizingList[n].Parent.Attribute("Value") != null
                            && Int32.TryParse(imageSizingList[n].Parent.Attribute("Value").Value, out int sizingIdx)
                            && imageSizingList[n].Attribute("ImageNumber") != null
                            && Int32.TryParse(imageSizingList[n].Attribute("ImageNumber").Value, out int sizingNumber)
                            )
                        {
                            if ( maskingIdx == sizingIdx)
                            {
                                RCSid.DecodeIndex(maskingIdx, out int r, out int c, out int s);
                                List<XElement> maskingImages = GetImageByImageNumber(maskingNumber);
                                List<XElement> sizingImages = GetImageByImageNumber(sizingNumber);
                                if ( maskingImages.Count == 1 && sizingImages.Count == 1)
                                {
                                    maskingImage = maskingImages[0];
                                    XElement sizingImage = sizingImages[0];
                                    if (maskingImage.Attribute("JNumber") != null && Int32.TryParse(maskingImage.Attribute("JNumber").Value, out int jNumb))
                                    {
                                        newProgramCounter = ProgramCounter + 1;
                                        string fil = MaskingDye + fil_suffix;
                                        xele = new XElement("Analysis",
                                            new XAttribute("ProgramCounter", String.Format("{0:D4}", newProgramCounter)),
                                            new XAttribute("Type", "Masking"),
                                            new XAttribute("WorkingDirectory", WorkingDirectory),
                                            new XAttribute("TemplateDirectory", TemplateDirectory),
                                            new XAttribute("ImageFileName", maskingImage.Attribute("Name").Value),
                                            new XAttribute("TemplatePrefix", Device),
                                            new XAttribute("FilterLabel", fil),
                                            new XAttribute("SubImage", maskingImage.Attribute("Section").Value),
                                            new XAttribute("SizeImageName", sizingImage.Attribute("Name").Value),
                                            new XAttribute("SizeImageNumber", sizingNumber),
                                            new XAttribute("BaseName", String.Format("{0}_J{1:D3}_{2}_P{3:D4}",
                                                BaseNameStub, jNumb, RotorChipSubLabel(r, c, s, isBefore), newProgramCounter)),
                                            new XAttribute("ColorShiftX", ColorShiftX.ToString()),
                                            new XAttribute("ColorShiftY", ColorShiftY.ToString()),
                                            new XAttribute("Dilation", Dilation.ToString()),
                                            new XAttribute("TransposeImage", RotateImage.ToString()),
                                            new XAttribute("UseMedianFilter", UseMedianFilter.ToString()),
                                            new XAttribute("ExtraOutput", "0"),
                                            new XAttribute("Performed", "false"),
                                            new XAttribute("Success", "false"),
                                            new XAttribute("Selected", "false")
                                            );
                                        addSetup = true;
                                        bool duplicate = false;
                                        if ( maskingImage.Descendants("AnalysisSetup") != null )
                                        {
                                            List<XElement> existingSetups = maskingImage.Descendants("AnalysisSetup").Descendants("Analysis").ToList();
                                            for (int e = 0; e < existingSetups.Count; e++)
                                            {
                                                if ((xele.Attribute("WorkingDirectory").Value == existingSetups[e].Attribute("WorkingDirectory").Value)
                                                    && (xele.Attribute("TemplateDirectory").Value == existingSetups[e].Attribute("TemplateDirectory").Value)
                                                    && (xele.Attribute("ImageFileName").Value == existingSetups[e].Attribute("ImageFileName").Value)
                                                    && (xele.Attribute("TemplatePrefix").Value == existingSetups[e].Attribute("TemplatePrefix").Value)
                                                    && (xele.Attribute("FilterLabel").Value == existingSetups[e].Attribute("FilterLabel").Value)
                                                    && (xele.Attribute("TransposeImage").Value == existingSetups[e].Attribute("TransposeImage").Value)
                                                    && (xele.Attribute("UseMedianFilter").Value == existingSetups[e].Attribute("UseMedianFilter").Value)
                                                    && (xele.Attribute("ExtraOutput").Value == existingSetups[e].Attribute("ExtraOutput").Value)
                                                    && (xele.Attribute("ColorShiftX").Value == existingSetups[e].Attribute("ColorShiftX").Value)
                                                    && (xele.Attribute("ColorShiftY").Value == existingSetups[e].Attribute("ColorShiftY").Value)
                                                    && (xele.Attribute("Dilation").Value == existingSetups[e].Attribute("Dilation").Value)
                                                    )
                                                {
                                                    duplicate = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if ( duplicate )
                                        {
                                            addSetup = false;
                                        }
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
                if ( addSetup)
                {
                    IEnumerable<XElement> maskingIEnum = maskingImage.Descendants("AnalysisSetup");
                    if (maskingIEnum.Count() == 1)
                    {
                        maskingIEnum.First().Add(xele);
                        ProgramCounter = newProgramCounter;
                        SetProgramCounter(newProgramCounter);
                        newAnalysis++;
                    }
                }
            }
            return newAnalysis;
        }
        public bool UpdateAnalysisResult(string ImageName, string ImageNumber, string AType, string ProgramCounter,
            bool NewPerformed, bool NewSuccess, out string Err)
        {
            Err = "";
            //Console.WriteLine(String.Format("UpdateAnalysisResult: {0}/{1}/{2}/{3}", ImageName, ImageNumber, AType, ProgramCounter));
            char[] zero = new char[] { '0' };
            string trimmedNumber = ImageNumber.TrimStart(zero);
            IEnumerable<XElement> imageIEnum = from el in ImageDoc.Descendants("Image")
                                               where el.Attribute("Name").Value.IndexOf(ImageName) >= 0
                                               && el.Attribute("ImageNumber").Value.IndexOf(trimmedNumber) >= 0
                                               && el.Attribute("Ignored") != null
                                                && bool.TryParse(el.Attribute("Ignored").Value, out bool ig)
                                                && !ig
                                               select el;
            if (imageIEnum.Count() < 1)
            {
                Err = "Image not found";
                return false;
            }
            else if (imageIEnum.Count() > 1)
            {
                Err = "Duplicate Images found";
                return false;
            }

            List<XElement> imageList = imageIEnum.ToList();

            string trimmedCounter = ProgramCounter.TrimStart(zero);
            IEnumerable<XElement> analysisSetupIEnum = from els in imageList[0].Descendants("AnalysisSetup").Descendants("Analysis")
                                                       where els.Attribute("Type").Value.IndexOf(AType) >= 0
                                                       && els.Attribute("ProgramCounter").Value.IndexOf(trimmedCounter) >= 0
                                                       && els.Attribute("ImageFileName").Value.IndexOf(ImageName) >= 0
                                                       select els;
            if (analysisSetupIEnum.Count() < 1)
            {
                Err = "Analysis setup not found";
                return false;
            }
            else if (analysisSetupIEnum.Count() > 1)
            {
                Err = "Duplicate setups found";
                return false;
            }
            XElement analysisSetup0 = analysisSetupIEnum.FirstOrDefault();
            analysisSetup0.Attribute("Performed").Value = NewPerformed.ToString();
            analysisSetup0.Attribute("Success").Value = NewSuccess.ToString();
            return true;
        }
        public bool UpdateAnalysisSelection(string ImageName, string ImageNumber, string AType, string ProgramCounter,
            bool NewSelection, out string Err)
        {
            Err = "";
            //Console.WriteLine(String.Format("UpdateAnalysisResult: {0}/{1}/{2}/{3}", ImageName, ImageNumber, AType, ProgramCounter));
            char[] zero = new char[] { '0' };
            string trimmedNumber = ImageNumber.TrimStart(zero);
            IEnumerable<XElement> imageIEnum = from el in ImageDoc.Descendants("Image")
                                               where el.Attribute("Name").Value.IndexOf(ImageName) >= 0
                                               && el.Attribute("ImageNumber").Value.IndexOf(trimmedNumber) >= 0
                                               && el.Attribute("Ignored") != null
                                               && bool.TryParse(el.Attribute("Ignored").Value, out bool ig)
                                               && !ig
                                               select el;
            if (imageIEnum.Count() < 1)
            {
                Err = "Image not found";
                return false;
            }
            else if (imageIEnum.Count() > 1)
            {
                Err = "Duplicate Images found";
                return false;
            }

            List<XElement> imageList = imageIEnum.ToList();

            string trimmedCounter = ProgramCounter.TrimStart(zero);
            IEnumerable<XElement> analysisSetupIEnum = from els in imageList[0].Descendants("AnalysisSetup").Descendants("Analysis")
                                                       where els.Attribute("Type").Value.IndexOf(AType) >= 0
                                                       && els.Attribute("ImageFileName").Value.IndexOf(ImageName) >= 0
                                                       select els;
            if (analysisSetupIEnum.Count() < 1)
            {
                Err = "Analysis setup not found";
                return false;
            }
            List<XElement> analysisSetupList = analysisSetupIEnum.ToList();
            string spc = ProgramCounter.ToString();
            if (NewSelection)
            {
                for (int n = 0; n < analysisSetupList.Count; n++)
                {
                    if (analysisSetupList[n].Attribute("ProgramCounter").Value.IndexOf(trimmedCounter) >= 0)
                        analysisSetupList[n].Attribute("Selected").Value = "true";
                    else
                        analysisSetupList[n].Attribute("Selected").Value = "false";
                }
            }
            else
            {
                for (int n = 0; n < analysisSetupList.Count; n++)
                {
                    if (analysisSetupList[n].Attribute("ProgramCounter").Value.IndexOf(trimmedCounter) >= 0)
                    {
                        analysisSetupList[n].Attribute("Selected").Value = "false";
                        break;
                    }
                }
            }
            return true;
        }
        public bool GetOutputBaseName(string ImageName, string ImageNumber, string AType, out string OutBaseName, out string Err)
        {
            Err = "";
            OutBaseName = "";
            char[] zero = new char[] { '0' };
            string trimmedNumber = ImageNumber.TrimStart(zero);
            IEnumerable<XElement> sz = from el in ImageDoc.Descendants("Image")
                                       where el.Attribute("Name").Value.IndexOf(ImageName) >= 0
                                       && el.Attribute("ImageNumber").Value.IndexOf(trimmedNumber) >= 0
                                       && el.Attribute("Ignored") != null
                                       && bool.TryParse(el.Attribute("Ignored").Value, out bool ig)
                                       && !ig
                                       select el;
            if (sz.Count() < 1)
            {
                Err = "Image not found";
                return false;
            }
            else if (sz.Count() > 1)
            {
                Err = "Duplicate Images found";
                return false;
            }

            IEnumerable<XElement> st = from els in sz.Descendants("AnalysisSetup").Descendants("Analysis")
                                       where els.Attribute("Type").Value.IndexOf(AType) >= 0
                                       select els;
            if (st.Count() < 1)
            {
                Err = String.Format("No Analysis setups found for type {0} for image {1}/{2}", AType, ImageName, ImageNumber);
                return false;
            }
            List<XElement> stX = st.ToList();
            int nTarget = -1;
            for (int n = 0; n < stX.Count; n++)
            {
                //if (stX[n].Attribute("Selected").Value.IndexOf(pc.ToString()) >= 0)
                //{
                //    nTarget = n;
                //    break;
                //}
                if (bool.TryParse(stX[n].Attribute("Selected").Value, out bool selected))
                {
                    nTarget = n;
                    break;
                }
            }
            if (nTarget < 0)
            {
                Err = String.Format("No analysis setup selected");
                return false;
            }

            OutBaseName = stX[nTarget].Attribute("BaseName").Value;
            return true;
        }
        public bool GetImageDoc(out string ImageDocument)
        {
            if (_XDocLoaded)
            {
                ImageDocument = ImageDoc.ToString();
                return true;
            }
            else
            {
                ImageDocument = "";
                return false;
            }
        }
        public int GetAnalysisInfoArray(string AnalysisType, int SetNumb, out string[] AnalysisInfo, out string Err)
        {
            int res = GetAnalysisInfo(AnalysisType, SetNumb, out List<string> analysisInfoList, out Err);
            AnalysisInfo = analysisInfoList.ToArray();
            return res;
        }
        public int GetAnalysisInfo(string AnalysisType, int SetNumb, out List<string> AnalysisInfo, out string Err)
        {
            Err = "";
            AnalysisInfo = new List<string>(0);
            //Tuple<string, string> newTuple;
            if (_XDocLoaded)
            {
                IEnumerable<XElement> analysisSetupIEnum 
                    = ImageDoc.Descendants("Images").Descendants("AnalysisSetup").Descendants("Analysis");
                if ( analysisSetupIEnum != null && analysisSetupIEnum.Count() > 0 )
                {
                    List<XElement> analysisList = analysisSetupIEnum.ToList();
                    for (int m = 0; m < analysisList.Count; m++)
                    {
                        if (analysisList[m].Attribute("Type") != null &&
                            analysisList[m].Attribute("Type").Value.IndexOf(AnalysisType, StringComparison.CurrentCultureIgnoreCase) >= 0)
                        {
                            XElement pp = analysisList[m].Parent.Parent;
                            if (pp.Attribute("ImageNumber") != null && pp.Attribute("SetNumber") != null
                                && pp.Attribute("Name") != null
                                && Int32.TryParse(pp.Attribute("ImageNumber").Value, out int imageNumb)
                                && Int32.TryParse(pp.Attribute("SetNumber").Value, out int setN)
                                && analysisList[m].Attribute("ProgramCounter") != null
                                && Int32.TryParse(analysisList[m].Attribute("ProgramCounter").Value, out int pc)
                                && analysisList[m].Attribute("Performed") != null
                                && bool.TryParse(analysisList[m].Attribute("Performed").Value, out bool bPerformed)
                                && analysisList[m].Attribute("Success") != null
                                && bool.TryParse(analysisList[m].Attribute("Success").Value, out bool bSuccess)
                                && analysisList[m].Attribute("Selected") != null
                                && bool.TryParse(analysisList[m].Attribute("Selected").Value, out bool bSelected)
                                )
                            {
                                if (pp.Attribute("Ignored") == null
                                    || ( bool.TryParse(pp.Attribute("Ignored").Value, out bool ig) && !ig))
                                {
                                    string imageName = pp.Attribute("Name").Value;
                                    AnalysisInfo.Add(String.Format("{0},{1},{2},{3},{4},{5},{6}",
                                        imageName, imageNumb, AnalysisType, pc, bPerformed.ToString(),
                                        bSuccess.ToString(), bSelected.ToString()));
                                }
                            }
                        }
                    }
                }
            }
            return AnalysisInfo.Count;
        }
        public int GetAnalysisSetups(string AnalysisType, int SetNumb, bool RStatus, bool SucStatus,
            out List<Tuple<string, string>> Setups, out string Err)
        {
            Err = "";
            Setups = new List<Tuple<string, string>>();
            //Tuple<string, string> newTuple;
            if (_XDocLoaded)
            {
                List<XElement> analysisSetupList = ImageDoc.Descendants("AnalysisSetup").ToList();
                if (analysisSetupList.Count > 0)
                {
                    for (int n = 0; n < analysisSetupList.Count; n++)
                    {
                        List<XElement> analysisList = analysisSetupList[n].Descendants("Analysis").ToList();
                        if (analysisList.Count > 0)
                        {
                            for (int m = 0; m < analysisList.Count; m++)
                            {
                                if (analysisList[m].Attribute("Type").Value.IndexOf(AnalysisType, StringComparison.CurrentCultureIgnoreCase) >= 0)
                                {
                                    XElement pp = analysisList[m].Parent.Parent;
                                    string imageN = pp.Attribute("ImageNumber").Value;
                                    string setN = pp.Attribute("SetNumber").Value;
                                    string per = analysisList[m].Attribute("Performed").Value;
                                    string suc = analysisList[m].Attribute("Success").Value;
                                    bool bPer;
                                    bool bSuc;
                                    int iSetN;
                                    if (bool.TryParse(per, out bPer) && bool.TryParse(suc, out bSuc)
                                        && Int32.TryParse(imageN, out _) && Int32.TryParse(setN, out iSetN))
                                    {

                                        if (RStatus == bPer)
                                            bPer = true;
                                        else
                                            bPer = false;
                                        if (SucStatus == bSuc)
                                            bSuc = true;
                                        else
                                            bSuc = false;
                                        bool outp = bPer && bSuc;
                                        if (iSetN != SetNumb)
                                            outp = false;
                                        if (imageN != null && outp)
                                        {
                                            Setups.Add(new Tuple<string, string>(analysisList[m].ToString(), imageN));
                                        }
                                    }
                                    else
                                    {
                                        Err += String.Format(
                                            "snumb == null, ana[{0}].parent: {1}\n          ans[{0}].parent.parent: {2}\n==========\n",
                                            m, analysisList[m].Parent.ToString(), analysisList[m].Parent.Parent.ToString());
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return Setups.Count;
        }
        public int GetSizingAnalysisArray(bool Before, int SetNumb, bool All, bool RStatus, bool SStatus,
            out string[] SetupStrings, out string Err)
        {
            int res = GetSizingAnalysisStrings(Before, SetNumb, All, RStatus, SStatus,
                out List<string> setupStrings, out Err);
            SetupStrings = setupStrings.ToArray();
            return res;
        }
        public int GetSizingAnalysisStrings(bool Before, int SetNumb, bool All, bool RStatus, bool SStatus,
            out List<string> SetupStrings, out string Err)
        {
            Err = "";
            SetupStrings = new List<string>();
            //Tuple<string, string> newTuple;
            if (_XDocLoaded)
            {
                string imageType = "";
                if (Before)
                    imageType = "Before";
                else
                    imageType = "After";
                IEnumerable<XElement> imageIEnum = from el in ImageDoc.Descendants("Images").Descendants("Image")
                                                   where el.Attribute("Type") != null
                                                   && el.Attribute("Type").Value.IndexOf(imageType, StringComparison.CurrentCultureIgnoreCase) >= 0
                                                   select el;
                if (imageIEnum.Count() > 0)
                {
                    List<XElement> analysisSetupList = imageIEnum.Descendants("AnalysisSetup").ToList();
                    if (analysisSetupList.Count > 0)
                    {
                        for (int n = 0; n < analysisSetupList.Count; n++)
                        {
                            List<XElement> analysisList = analysisSetupList[n].Descendants("Analysis").ToList();
                            if (analysisList.Count > 0)
                            {
                                for (int m = 0; m < analysisList.Count; m++)
                                {
                                    if (analysisList[m].Attribute("Type") != null
                                        && analysisList[m].Attribute("Type").Value.IndexOf("Sizing", StringComparison.CurrentCultureIgnoreCase) >= 0
                                        && analysisList[m].Attribute("ProgramCounter") != null
                                        && Int32.TryParse(analysisList[m].Attribute("ProgramCounter").Value, out int pc)
                                        && analysisList[m].Parent.Parent.Attribute("ImageNumber") != null
                                        && Int32.TryParse(analysisList[m].Parent.Parent.Attribute("ImageNumber").Value, out int im)
                                        )
                                    {
                                        XElement pp = analysisList[m].Parent.Parent;
                                        string setN = pp.Attribute("SetNumber").Value;
                                        string per = analysisList[m].Attribute("Performed").Value;
                                        string suc = analysisList[m].Attribute("Success").Value;
                                        bool bPer;
                                        bool bSuc;
                                        int iSetN;
                                        if (bool.TryParse(per, out bPer) && bool.TryParse(suc, out bSuc)
                                            && Int32.TryParse(setN, out iSetN))
                                        {
                                            if (RStatus == bPer)
                                                bPer = true;
                                            else
                                                bPer = false;
                                            if (SStatus == bSuc)
                                                bSuc = true;
                                            else
                                                bSuc = false;
                                            bool outp = (bPer && bSuc || All);
                                            if (iSetN != SetNumb)
                                                outp = false;
                                            if (outp)
                                            {
                                                string pExtraOutput = analysisList[m].Attribute("ExtraOutput").Value;
                                                string pUseMedian = analysisList[m].Attribute("UseMedianFilter").Value;
                                                string pTransposeIm = analysisList[m].Attribute("TransposeImage").Value;
                                                string pBaseName = analysisList[m].Attribute("BaseName").Value;
                                                string pSubImage = analysisList[m].Attribute("SubImage").Value;
                                                string pFilterL = analysisList[m].Attribute("FilterLabel").Value;
                                                string pPrefix = analysisList[m].Attribute("TemplatePrefix").Value;
                                                string pImageName = analysisList[m].Attribute("ImageFileName").Value;
                                                string pTDirectory = analysisList[m].Attribute("TemplateDirectory").Value;
                                                string pWDirectory = analysisList[m].Attribute("WorkingDirectory").Value;
                                                int iExtraOutput;
                                                bool bUseMedian;
                                                bool bTransposeIm;
                                                int iSubImage;

                                                if (Int32.TryParse(pExtraOutput, out iExtraOutput)
                                                    && bool.TryParse(pUseMedian, out bUseMedian)
                                                    && bool.TryParse(pTransposeIm, out bTransposeIm)
                                                    && Int32.TryParse(pSubImage, out iSubImage))
                                                {
                                                    SetupStrings.Add(String.Format("sdpcrSizing.exe {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} | {10} | {11}",
                                                        pWDirectory, pTDirectory, pImageName, pPrefix, pFilterL, iSubImage.ToString(),
                                                        pBaseName, Convert.ToInt32(bTransposeIm), Convert.ToInt32(bUseMedian), iExtraOutput,
                                                        pc.ToString("D4"), im.ToString("D3")));
                                                    //String.Format("sdpcrSizing(Err, {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9})   Performed({10}), Success({11})",
                                                    //pWDirectory, pTDirectory, pImageName, pPrefix, pFilterL, pSubImage, pBaseName,
                                                    //pTransposeIm, pUseMedian, pExtraOutput, pPerformed, pSuccess));
                                                }
                                                else
                                                {
                                                    Err += String.Format(
                                                        "snumb == null, ana[{0}].parent: {1}\n          ans[{0}].parent.parent: {2}\n==========\n",
                                                        m, analysisList[m].Parent.ToString(), analysisList[m].Parent.Parent.ToString());
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return SetupStrings.Count;
        }
        public int GetMaskingAnalysisArray(bool Before, int SetNumb, bool All, bool RStatus, bool SStatus,
            out string[] SetupStrings, out string Err)
        {
            int res = GetMaskingAnalysisStrings(Before, SetNumb, All, RStatus, SStatus,
                out List<string> setupStringsList, out Err);
            SetupStrings = setupStringsList.ToArray();
            return res;
        }
        public int GetMaskingAnalysisStrings(bool Before, int SetNumb, bool All, bool RStatus, bool SStatus,
            out List<string> SetupStrings, out string Err)
        {
            Err = "";
            SetupStrings = new List<string>();
            char[] zero = new char[] { '0' };
            if (_XDocLoaded)
            {
                string imageType = "";
                if (Before)
                    imageType = "Before";
                else
                    imageType = "After";
                IEnumerable<XElement> imageIEnum = from el in ImageDoc.Descendants("Images").Descendants("Image")
                                                   where el.Attribute("Type") != null
                                                   && el.Attribute("Type").Value.IndexOf(imageType, StringComparison.CurrentCultureIgnoreCase) >= 0
                                                   select el;
                if (imageIEnum.Count() > 0)
                {
                    List<XElement> analysisSetupList = imageIEnum.Descendants("AnalysisSetup").ToList();
                    if (analysisSetupList.Count > 0)
                    {
                        for (int n = 0; n < analysisSetupList.Count; n++)
                        {
                            List<XElement> analysisList = analysisSetupList[n].Descendants("Analysis").ToList();
                            if (analysisList.Count > 0)
                            {
                                for (int m = 0; m < analysisList.Count; m++)
                                {
                                    if (analysisList[m].Attribute("Type") != null
                                        && analysisList[m].Attribute("Type").Value.IndexOf("Masking", StringComparison.CurrentCultureIgnoreCase) >= 0
                                        && analysisList[m].Attribute("ProgramCounter") != null
                                        && Int32.TryParse(analysisList[m].Attribute("ProgramCounter").Value, out int pc)
                                        && analysisList[m].Parent.Parent.Attribute("ImageNumber") != null
                                        && Int32.TryParse(analysisList[m].Parent.Parent.Attribute("ImageNumber").Value, out int im)
                                        )
                                    {
                                        XElement pp = analysisList[m].Parent.Parent;
                                        string setN = pp.Attribute("SetNumber").Value;
                                        string per = analysisList[m].Attribute("Performed").Value;
                                        string suc = analysisList[m].Attribute("Success").Value;
                                        bool bPer;
                                        bool bSuc;
                                        int iSetN;
                                        string pJNumb = pp.Attribute("JNumber").Value;
                                        if (bool.TryParse(per, out bPer) && bool.TryParse(suc, out bSuc)
                                            && Int32.TryParse(setN, out iSetN))
                                        {
                                            if (RStatus == bPer)
                                                bPer = true;
                                            else
                                                bPer = false;
                                            if (SStatus == bSuc)
                                                bSuc = true;
                                            else
                                                bSuc = false;
                                            bool outp = (bPer && bSuc || All);
                                            if (iSetN != SetNumb)
                                                outp = false;
                                            if (outp)
                                            {
                                                string pExtraOutput = analysisList[m].Attribute("ExtraOutput").Value;
                                                string pUseMedian = analysisList[m].Attribute("UseMedianFilter").Value;
                                                string pTransposeIm = analysisList[m].Attribute("TransposeImage").Value;
                                                string pBaseName = analysisList[m].Attribute("BaseName").Value;
                                                string pSubImage = analysisList[m].Attribute("SubImage").Value;
                                                string pFilterL = analysisList[m].Attribute("FilterLabel").Value;
                                                string pPrefix = analysisList[m].Attribute("TemplatePrefix").Value;
                                                string pImageName = analysisList[m].Attribute("ImageFileName").Value;
                                                string pTDirectory = analysisList[m].Attribute("TemplateDirectory").Value;
                                                string pWDirectory = analysisList[m].Attribute("WorkingDirectory").Value;
                                                string pColorShiftX = analysisList[m].Attribute("ColorShiftX").Value;
                                                string pColorShiftY = analysisList[m].Attribute("ColorShiftY").Value;
                                                string pDilation = analysisList[m].Attribute("Dilation").Value;
                                                string pSizingImageNumber = analysisList[m].Attribute("SizeImageNumber").Value;
                                                int iExtraOutput;
                                                bool bUseMedian;
                                                bool bTransposeIm;
                                                int iSubImage;
                                                int iColorShiftX;
                                                int iColorShiftY;
                                                int iDilation;
                                                int iSizingImageNumber;
                                                string pSizingBaseName;
                                                if (Int32.TryParse(pExtraOutput, out iExtraOutput)
                                                    && bool.TryParse(pUseMedian, out bUseMedian)
                                                    && bool.TryParse(pTransposeIm, out bTransposeIm)
                                                    && Int32.TryParse(pSubImage, out iSubImage)
                                                    && Int32.TryParse(pColorShiftX, out iColorShiftX)
                                                    && Int32.TryParse(pColorShiftY, out iColorShiftY)
                                                    && Int32.TryParse(pDilation, out iDilation)
                                                    && Int32.TryParse(pSizingImageNumber, out iSizingImageNumber)
                                                    )
                                                {
                                                    List<XElement> sizingImList = GetImageByImageNumber(iSizingImageNumber);
                                                    if (sizingImList.Count == 1)
                                                    {
                                                        //string spc = "";
                                                        IEnumerable<XElement> sizingAnalysisSetupIEnum = from els in sizingImList[0].Descendants("AnalysisSetup").Descendants("Analysis")
                                                                                                         where els.Attribute("Type") != null
                                                                                                         && els.Attribute("Type").Value.IndexOf("Sizing", StringComparison.CurrentCultureIgnoreCase) >= 0
                                                                                                         && els.Attribute("Selected") != null
                                                                                                         && bool.TryParse(els.Attribute("Selected").Value, out bool sel)
                                                                                                         && sel
                                                                                                         select els;
                                                        if (sizingAnalysisSetupIEnum.Count() == 1)
                                                        {
                                                            pSizingBaseName = sizingAnalysisSetupIEnum.First().Attribute("BaseName").Value;
                                                            SetupStrings.Add(String.Format("sdpcrMasking.exe {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} {12} {13} | {14} | {15}",
                                                                pWDirectory, pTDirectory, pImageName, pPrefix, pFilterL, iSubImage.ToString(),
                                                                pSizingBaseName, pBaseName, iColorShiftX, iColorShiftY, iDilation,
                                                                Convert.ToInt32(bTransposeIm), Convert.ToInt32(bUseMedian), iExtraOutput,
                                                                pc.ToString("D4"), im.ToString("D3")));
                                                        }
                                                        else if (sizingAnalysisSetupIEnum.Count() > 1)
                                                        {
                                                            Err += String.Format("sizingAnalysisSetupIEnum.Count() != 1 for Image JNumb = {0}. + ",
                                                                pJNumb);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    Err += String.Format("sizingImList.Count != 1 for Image JNumb = {0}. + ",
                                                                   pJNumb);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return SetupStrings.Count;
        }
        public int GetMaskingAnalysisSetups(int SetNumb, bool RStatus, bool SStatus,
            out List<string> SetupStrings, out List<XElement> SetupInfo, out string Err)
        {
            Err = "";
            SetupStrings = new List<string>();
            SetupInfo = new List<XElement>();
            char[] zero = new char[] { '0' };
            if (_XDocLoaded)
            {
                List<XElement> analysisSetupList = ImageDoc.Descendants("AnalysisSetup").ToList();
                if (analysisSetupList.Count > 0)
                {
                    for (int n = 0; n < analysisSetupList.Count; n++)
                    {
                        List<XElement> analysisList = analysisSetupList[n].Descendants("Analysis").ToList();
                        if (analysisList.Count > 0)
                        {
                            for (int m = 0; m < analysisList.Count; m++)
                            {
                                if (analysisList[m].Attribute("Type") != null
                                    && analysisList[m].Attribute("Type").Value.IndexOf("Masking", StringComparison.CurrentCultureIgnoreCase) >= 0
                                    && analysisList[m].Attribute("ProgramCounter") != null
                                        && Int32.TryParse(analysisList[m].Attribute("ProgramCounter").Value, out int pc)
                                        && analysisList[m].Parent.Parent.Attribute("ImageNumber") != null
                                        && Int32.TryParse(analysisList[m].Parent.Parent.Attribute("ImageNumber").Value, out int im)
                                        )
                                {
                                    XElement pp = analysisList[m].Parent.Parent;
                                    string setN = pp.Attribute("SetNumber").Value;
                                    string pPerformed = analysisList[m].Attribute("Performed").Value;
                                    string pSuccess = analysisList[m].Attribute("Success").Value;
                                    bool bPer;
                                    bool bSuc;
                                    int iSetN;
                                    string pJNumb = pp.Attribute("JNumber").Value;
                                    if (bool.TryParse(pPerformed, out bPer) && bool.TryParse(pSuccess, out bSuc)
                                        && Int32.TryParse(setN, out iSetN))
                                    {
                                        if (RStatus == bPer)
                                            bPer = true;
                                        else
                                            bPer = false;
                                        if (SStatus == bSuc)
                                            bSuc = true;
                                        else
                                            bSuc = false;
                                        bool outp = bPer && bSuc;
                                        if (iSetN != SetNumb)
                                            outp = false;
                                        if (outp)
                                        {
                                            string pExtraOutput = analysisList[m].Attribute("ExtraOutput").Value;
                                            string pUseMedian = analysisList[m].Attribute("UseMedianFilter").Value;
                                            string pTransposeIm = analysisList[m].Attribute("TransposeImage").Value;
                                            string pProgramCounter = analysisList[m].Attribute("ProgramCounter").Value;
                                            string pBaseName = analysisList[m].Attribute("BaseName").Value;
                                            string pSubImage = analysisList[m].Attribute("SubImage").Value;
                                            string pFilterL = analysisList[m].Attribute("FilterLabel").Value;
                                            string pPrefix = analysisList[m].Attribute("TemplatePrefix").Value;
                                            string pImageName = analysisList[m].Attribute("ImageFileName").Value;
                                            string pTDirectory = analysisList[m].Attribute("TemplateDirectory").Value;
                                            string pWDirectory = analysisList[m].Attribute("WorkingDirectory").Value;
                                            string pColorShiftX = analysisList[m].Attribute("ColorShiftX").Value;
                                            string pColorShiftY = analysisList[m].Attribute("ColorShiftY").Value;
                                            string pDilation = analysisList[m].Attribute("Dilation").Value;
                                            string pSizingImageNumber = analysisList[m].Attribute("SizeImageNumber").Value;
                                            string pRotor = analysisList[n].Parent.Parent.Attribute("RotorPosition").Value;
                                            string pChip = analysisList[n].Parent.Parent.Attribute("ChipPosition").Value;
                                            int iExtraOutput;
                                            bool bUseMedian;
                                            bool bTransposeIm;
                                            int iSubImage;
                                            int iColorShiftX;
                                            int iColorShiftY;
                                            int iDilation;
                                            int iSizingImageNumber;
                                            int iRotor;
                                            string pSizingBaseName;
                                            if (Int32.TryParse(pExtraOutput, out iExtraOutput)
                                                && bool.TryParse(pUseMedian, out bUseMedian)
                                                && bool.TryParse(pTransposeIm, out bTransposeIm)
                                                && Int32.TryParse(pSubImage, out iSubImage)
                                                && Int32.TryParse(pColorShiftX, out iColorShiftX)
                                                && Int32.TryParse(pColorShiftY, out iColorShiftY)
                                                && Int32.TryParse(pDilation, out iDilation)
                                                && Int32.TryParse(pSizingImageNumber, out iSizingImageNumber)
                                                && Int32.TryParse(pRotor, out iRotor)
                                                && (pChip.IndexOf("Left", StringComparison.CurrentCultureIgnoreCase) >= 0 
                                                || pChip.IndexOf("Right", StringComparison.CurrentCultureIgnoreCase) >= 0)
                                                )
                                            {
                                                List<XElement> sizingImList = GetImageByImageNumber(iSizingImageNumber);
                                                if (sizingImList.Count == 1)
                                                {
                                                    //string spc = "";
                                                    IEnumerable<XElement> sizingSetupIEnum = from el in sizingImList[0].Descendants("AnalysisSetup").Descendants("Analysis")
                                                                                             where el.Attribute("Type") != null
                                                                                             && el.Attribute("Type").Value.IndexOf("Sizing", StringComparison.CurrentCultureIgnoreCase) >= 0
                                                                                             && el.Attribute("Ignored") != null
                                                                                             && bool.TryParse(el.Attribute("Ignored").Value, out bool ig)
                                                                                             && !ig
                                                                                             select el;
                                                    if (sizingSetupIEnum.Count() == 1)
                                                    {
                                                        if (sizingSetupIEnum.First().Attribute("Success") != null
                                                            && bool.TryParse(sizingSetupIEnum.First().Attribute("Success").Value, out bool sel)
                                                            && sel)
                                                        {
                                                            int count = SetupStrings.Count;
                                                            pSizingBaseName = sizingSetupIEnum.First().Attribute("BaseName").Value;
                                                            SetupStrings.Add(String.Format("sdpcrMasking.exe {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} {12} {13} | {14} | {15}",
                                                                pWDirectory, pTDirectory, pImageName, pPrefix, pFilterL, iSubImage.ToString(),
                                                                pSizingBaseName, pBaseName, iColorShiftX, iColorShiftY, iDilation,
                                                                Convert.ToInt32(bTransposeIm), Convert.ToInt32(bUseMedian), iExtraOutput,
                                                                pc.ToString("D4"), im.ToString("D3")));

                                                            XElement info = new XElement("SetupInfo",
                                                                new XAttribute("ScriptIndex", count.ToString()),
                                                                new XAttribute("ProgramCounter", pProgramCounter),
                                                                new XAttribute("RotorPosition", pRotor),
                                                                new XAttribute("ChipPosition", pChip),
                                                                new XAttribute("SubImage", pSubImage),
                                                                new XAttribute("SizeImageName", pProgramCounter),
                                                                new XAttribute("SizeImageNumber", pProgramCounter),
                                                                new XAttribute("BaseName", pProgramCounter),
                                                                new XAttribute("Performed", pPerformed),
                                                                new XAttribute("Success", pSuccess)
                                                                );
                                                            SetupInfo.Add(XElement.Parse(info.ToString()));
                                                            //                     < Analysis ProgramCounter = "0023" Type = "Masking" WorkingDirectory = "c:\Instrument"
                                                            //                     TemplateDirectory = "c:\Instrument\Template" 
                                                            //                     RotorPosition, ChipPosition,  SubImage = "3" SizeImageName = "XY5-000000024_J017_R1LS3_L0F0_aft0.png"
                                                            //                     SizeImageNumber = "14" BaseName = "XY5-24Test_J018_A0103_P0023" Performed = "true" Success = "true" />

                                                            //                   </ AnalysisSetup >
                                                            //                   XY5 - 000000024_J004_R1LS2_L1F1_bef0.png M1015K04 DyeB_B 2 XY5 - 24Test_J003_B102_P0013 XY5-24Test_J004_B102_P0017 0 0
                                                        }
                                                        else
                                                        {
                                                            Err += String.Format("sizingSetupIEnum.[Success] false for Image JNumb = {0}. + ",
                                                                pJNumb);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Err += String.Format("sizingSetupIEnum.Count() != 1 for Image JNumb = {0}. + ",
                                                                pJNumb);
                                                    }
                                                }
                                                else
                                                {
                                                    Err += String.Format("sizingImList.Count != 1 for Image JNumb = {0}. + ",
                                                               pJNumb);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return SetupStrings.Count;
        }
        public bool AddImage(string ImageName, bool Before, int SetNumber, string Dye, int Section, string ChipPositionString, int RotorPosition,
            string TimeIn, out string Err)
        {
            Err = "";
            bool error = false;
            string bString = "";
            string jString = "";
            if (_XDocLoaded || _settingsLoaded)
            {
                if (ImageName.Length < 6)
                {
                    Err += "Image name is too short";
                    error = true;
                }
                if (Before)
                    bString = "Before";
                else
                    bString = "After";

                if (SetNumber < 0)
                {
                    Err += String.Format("SetNumber must be >= 0. {0}/ ", SetNumber);
                    error = true;
                }
                if (Dye.Length < 1)
                {
                    Err += "Dye string is empty. / ";
                    error = true;
                }
                if (Section < 1 || Section > NumbSections)
                {
                    Err += String.Format("Invalid Section value. {0}-{1} / ", Section, NumbSections);
                    error = true;
                }
                int chipPosition = -1;
                if (ChipPositionString.IndexOf("Left", StringComparison.CurrentCultureIgnoreCase) >= 0)
                {
                    chipPosition = 0;
                }
                else if (ChipPositionString.IndexOf("Right", StringComparison.CurrentCultureIgnoreCase) >= 0)
                {
                    chipPosition = 1;
                }
                else
                {
                    Err += String.Format("Invalid ChipPosition. {0} / ", ChipPositionString);
                    error = true;
                }

                if (RotorPosition < 1 || RotorPosition > 4)
                {
                    Err += String.Format("Invalid RotorPosition. {0} / ", RotorPosition);
                    error = true;
                }
                if (TimeIn.Length < 1)
                {
                    Err += "Empty Time String. / ";
                    error = true;
                }
                if (!error)
                {
                    List<XElement> im = ImageDoc.Descendants("Images").ToList();
                    if (im.Count != 1)
                    {
                        Err += "Problem with <Images> node in ImageDoc";
                        error = true;
                    }
                    else
                    {
                        string pattern = @"_J(\d+)_";
                        List<System.Text.RegularExpressions.Match> jStrList = new List<System.Text.RegularExpressions.Match>();
                        foreach (System.Text.RegularExpressions.Match jStr in
                            System.Text.RegularExpressions.Regex.Matches(ImageName, pattern))
                        {
                            jStrList.Add(jStr);
                        }
                        if (jStrList.Count == 1)
                        {
                            int jNumb;
                            if (Int32.TryParse(jStrList[0].Groups[1].Value, out jNumb))
                                jString = jNumb.ToString("D3");
                            else
                                jString = "000";
                        }
                        else
                            jString = "000";
                        int newImageCounter = ImageCounter + 1;
                        XElement newIM = new XElement("Image",
                            new XAttribute("Name", ImageName),
                            new XAttribute("Type", bString),
                            new XAttribute("SetNumber", SetNumber.ToString()),
                            new XAttribute("Dye", Dye),
                            new XAttribute("Section", Section.ToString()),
                            new XAttribute("ChipPosition", ChipPositionString),
                            new XAttribute("RotorPosition", RotorPosition.ToString()),
                            new XAttribute("ImageNumber", newImageCounter.ToString("D3")),
                            new XAttribute("JNumber", jString),
                            new XAttribute("Time", TimeIn),
                            new XAttribute("Ignored", "false")
                            );
                        XElement newSetup = new XElement("AnalysisSetup",
                            new XAttribute("CollectionType", "Analysis"));
                        newIM.Add(newSetup);
                        im[0].Add(newIM);

                        if (ImageDoc.Descendants("ImagesSortedByDye") != null)
                        {
                            List<XElement> collated = ImageDoc.Descendants("ImagesSortedByDye").ToList();
                            bool addToSortedList = true;
                            if (collated.Count == 1)
                            {
                                IEnumerable<XElement> dyeIEnum = from el in collated[0].Descendants("Dye")
                                                                 where el.Attribute("DyeName") != null
                                                                 && el.Attribute("DyeName").Value.IndexOf(Dye, StringComparison.CurrentCultureIgnoreCase) >= 0
                                                                 select el;
                                if (dyeIEnum.Count() == 0)
                                {
                                    XElement newDye = NewSortByDyeNode(Dye);
                                    if (newDye != null)
                                        collated[0].Add(newDye);
                                    else
                                        addToSortedList = false;
                                }

                                if (addToSortedList)
                                {
                                    IEnumerable<XElement> sortedDyeIEnum = from el in ImageDoc.Descendants("ImagesSortedByDye").Descendants("Dye")
                                                                           where el.Attribute("DyeName") != null
                                                                           && el.Attribute("DyeName").Value.IndexOf(Dye, StringComparison.CurrentCultureIgnoreCase) >= 0
                                                                           select el;
                                    if (sortedDyeIEnum != null && sortedDyeIEnum.Count() == 1)
                                    {
                                        XElement sortedDye = sortedDyeIEnum.First();
                                        int idx = RCSid.IndexOut(RotorPosition, chipPosition, Section);
                                        //Console.WriteLine(String.Format("RCSid {0}: {1} - {2}/{3}/{4} -- {5}/{6}",
                                        //    newImageCounter, idx, RotorPosition, chipPosition, Section, RCSid.Base, RCSid.Size));
                                        IEnumerable<XElement> sortedDyeRCSIEnum = from el in sortedDye.Descendants("Index")
                                                                                  where el.Attribute("Value") != null
                                                                                  && Int32.TryParse(el.Attribute("Value").Value, out int val)
                                                                                  && val == idx
                                                                                  select el;
                                        if (sortedDyeRCSIEnum != null && sortedDyeRCSIEnum.Count() == 1)
                                        {
                                            XElement rcsNode = sortedDyeRCSIEnum.First();
                                            XElement imInfo = new XElement("SortedImage",
                                                new XAttribute("Name", ImageName),
                                                new XAttribute("ImageNumber", newImageCounter.ToString("D3")),
                                                new XAttribute("RotorPosition", RotorPosition.ToString()),
                                                new XAttribute("ChipPosition", ChipPositionString),
                                                new XAttribute("Section", Section.ToString()),
                                                new XAttribute("Type", bString),
                                                new XAttribute("SetNumber", SetNumber.ToString()),
                                                new XAttribute("Ignored", "false")
                                                );
                                            rcsNode.Add(imInfo);
                                        }
                                    }
                                }
                            }
                        }
                        ImageCounter = newImageCounter;
                        SetImageCounter(newImageCounter);
                    }
                }
            }
            return !error;
        }
        XElement NewSortByDyeNode(string Dye)
        {
            IEnumerable<XElement> rotorIEnum = ImageDoc.Descendants("Settings").Descendants("Rotor");
            if (rotorIEnum != null && rotorIEnum.Count() == 1)
            {
                if (rotorIEnum.Attributes("RCSid_Size") != null && rotorIEnum.Attributes("RCSid_Size").Count() == 1
                    && Int32.TryParse(rotorIEnum.Attributes("RCSid_Size").First().Value, out int rcsidSize))
                {
                    XElement node = new XElement("Dye",
                        new XAttribute("DyeName", Dye));
                    for (int n = 0; n < rcsidSize; n++)
                    {
                        node.Add(new XElement("Index", new XAttribute("Value", n.ToString())));
                    }
                    return node;
                }
                return null;
            }
            else
                return null;
        }
        public int AddQuantification(string QuantDye, string QuantParameterFileName, int BeforeSetNumb, int[] AfterSetNumbersArray,
            string BeforeAnalysisType, string AfterAnalysisType, out string Err)
        {
            char[] zero = new char[] { '0' };
            Err = "";
            int newAnalysis = 0;
            XElement quantNode = ImageDoc.Descendants("Quantifications").FirstOrDefault();
            int newProgramCounter = 0;
            if (quantNode != null)
            {
                List<int> afterSetNumbers = AfterSetNumbersArray.ToList();
                string beforeType = "Masking";
                if (BeforeAnalysisType.IndexOf("Sizing", StringComparison.CurrentCultureIgnoreCase) >= 0)
                    beforeType = "Sizing";

                string afterType = "Masking";
                if (AfterAnalysisType.IndexOf("Sizing", StringComparison.CurrentCultureIgnoreCase) >= 0)
                    afterType = "Sizing";
                if (QuantDye.Length < 2)
                {
                    Err = "No Quant Dye";
                    return 0;
                }
                if (QuantParameterFileName.Length < 2)
                {
                    Err = "No QuantParameterFileName";
                    return 0;
                }
                if (BeforeSetNumb < 0)
                {
                    Err = "Before Set Numb must be >= 0";
                    return 0;
                }
                for (int m = 0; m < AfterSetNumbersArray.Length; m++)
                {
                    if (AfterSetNumbersArray[m] < 0)
                    {
                        Err = "All After Set Numbs must be >= 0";
                        return 0;
                    }
                }
                Array.Sort(AfterSetNumbersArray, 0, AfterSetNumbersArray.Count());
                IEnumerable<XElement> sortedQuantIEnum = from el in ImageDoc.Descendants("ImagesSortedByDye").Descendants("Dye")
                                                         where el.Attribute("DyeName") != null
                                                         && el.Attribute("DyeName").Value.IndexOf(QuantDye, StringComparison.CurrentCultureIgnoreCase) >= 0
                                                         select el;

                if (sortedQuantIEnum.Count() < 1)
                {
                    Err = String.Format("No SortedImage node with Quant Dye: {0} were found", QuantDye);
                    return 0;
                }

                IEnumerable<XElement> imageBeforeIEnum = from el in sortedQuantIEnum.Descendants("Index").Descendants("SortedImage")
                                                         where el.Attribute("Type") != null
                                                         && el.Attribute("Type").Value.IndexOf("Before", StringComparison.CurrentCultureIgnoreCase) >= 0
                                                         && el.Attribute("SetNumber") != null
                                                         && Int32.TryParse(el.Attribute("SetNumber").Value, out int sizingSet)
                                                         && sizingSet == BeforeSetNumb
                                                         && el.Attribute("Ignored") != null
                                                         && bool.TryParse(el.Attribute("Ignored").Value, out bool igs)
                                                         && !igs
                                                         select el;

                IEnumerable<XElement> imageAfterIEnum = from el in sortedQuantIEnum.Descendants("Index").Descendants("SortedImage")
                                                        where el.Attribute("Type") != null
                                                        && el.Attribute("Type").Value.IndexOf("After", StringComparison.CurrentCultureIgnoreCase) >= 0
                                                        && el.Attribute("SetNumber") != null
                                                        && Int32.TryParse(el.Attribute("SetNumber").Value, out int maskingSet)
                                                        && el.Attribute("Ignored") != null
                                                        && bool.TryParse(el.Attribute("Ignored").Value, out bool igm)
                                                        && !igm
                                                        select el;
                if (imageBeforeIEnum.Count() < 1)
                {
                    Err = String.Format("No before images with Dye = {0}", QuantDye);
                    return 0;
                }
                if (imageAfterIEnum.Count() < 1)
                {
                    Err = String.Format("No after images with Dye = {0}", QuantDye);
                    return 0;
                }
                List<XElement> imageBeforeList = imageBeforeIEnum.ToList();
                List<XElement> imageAfterList = imageAfterIEnum.ToList();
                for (int m = 0; m < imageBeforeList.Count; m++)
                {
                    List<int> afterNumbers = new List<int>(0);
                    List<int> afterImageNumbers = new List<int>(0);
                    //bool addSetup = false;
                    XElement xele = new XElement("Unused");
                    newProgramCounter = 0;
                    if (imageBeforeList[m].Parent.Attribute("Value") != null
                        && Int32.TryParse(imageBeforeList[m].Parent.Attribute("Value").Value, out int beforeIdx)
                        && imageBeforeList[m].Attribute("ImageNumber") != null
                        && Int32.TryParse(imageBeforeList[m].Attribute("ImageNumber").Value, out int beforeNumber)
                        )
                    {
                        RCSid.DecodeIndex(beforeIdx, out int r, out int c, out int s);
                        List<XElement> beforeImages = GetImageByImageNumber(beforeNumber);
                        List<XElement> beforeSetups = new List<XElement>();
                        if (beforeImages.Count == 1)
                        {
                            IEnumerable<XElement> beforeImageSetupsIEnum = from el in beforeImages[0].Descendants("AnalysisSetup").Descendants("Analysis")
                                                                      where el.Attribute("Type") != null
                                                                      && el.Attribute("Type").Value.IndexOf(beforeType, StringComparison.CurrentCultureIgnoreCase) >= 0
                                                                      && el.Attribute("BaseName") != null
                                                                      && el.Attribute("BaseName").Value.Length > 4
                                                                      && el.Attribute("Selected") != null
                                                                      && bool.TryParse(el.Attribute("Selected").Value, out bool selB)
                                                                      && selB
                                                                      select el;
                            if (beforeImageSetupsIEnum.Count() == 1)
                            {
                                beforeSetups = beforeImageSetupsIEnum.ToList();
                                for (int n = 0; n < imageAfterList.Count; n++)
                                {
                                    if (imageAfterList[n].Parent.Attribute("Value") != null
                                        && Int32.TryParse(imageAfterList[n].Parent.Attribute("Value").Value, out int afterIdx)
                                        && imageAfterList[n].Attribute("ImageNumber") != null
                                        && Int32.TryParse(imageAfterList[n].Attribute("ImageNumber").Value, out int afterNumber)
                                        )
                                    {
                                        if (beforeIdx == afterIdx)
                                        {
                                            afterNumbers.Add(afterNumber);
                                        }
                                    }
                                }

                                if (afterNumbers.Count > 0)
                                {
                                    List<XElement> afterSetups = new List<XElement>(0);
                                    int[] afterSetsCount = new int[AfterSetNumbersArray.Count()];
                                    for (int k = 0; k < AfterSetNumbersArray.Count(); k++)
                                    {
                                        afterSetsCount[k] = 0;
                                        for (int n = 0; n < afterNumbers.Count; n++)
                                        {
                                            List<XElement> afterImageTmp = GetImageByImageNumber(afterNumbers[n]);
                                            if (afterImageTmp.Count == 1)
                                            {
                                                if (afterImageTmp[0].Attribute("SetNumber") != null
                                                    && Int32.TryParse(afterImageTmp[0].Attribute("SetNumber").Value, out int afterTmpSetNumb)
                                                    && afterTmpSetNumb == AfterSetNumbersArray[k]
                                                    )
                                                {
                                                    IEnumerable<XElement> afterImageTmpSetup = from el in afterImageTmp[0].Descendants("AnalysisSetup").Descendants("Analysis")
                                                                                               where el.Attribute("Type") != null
                                                                                               && el.Attribute("Type").Value.IndexOf(afterType, StringComparison.CurrentCultureIgnoreCase) >= 0
                                                                                               && el.Attribute("BaseName") != null
                                                                                               && el.Attribute("BaseName").Value.Length > 4
                                                                                               && el.Attribute("Selected") != null
                                                                                               && bool.TryParse(el.Attribute("Selected").Value, out bool selA)
                                                                                               && selA
                                                                                               select el;
                                                    if (afterImageTmpSetup.Count() == 1)
                                                    {
                                                        afterSetups.Add(afterImageTmpSetup.First());
                                                        afterSetsCount[k]++;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (afterSetups.Count > 0)
                                    {
                                        int found = afterSetups.Count;
                                        int cnt = 0;
                                        for (int k = 0; k < afterSetsCount.Count(); k++)
                                        {
                                            cnt += afterSetsCount[k];
                                            if (afterSetsCount[k] > 1)
                                            {
                                                cnt = -1;
                                                break;
                                            }
                                        }
                                        if ( cnt != found)
                                        {
                                            afterSetups.Clear();
                                        }
                                    }
                                    if (afterSetups.Count > 0)
                                    {
                                        newProgramCounter = ProgramCounter + 1;
                                        List<string> aBaseNames = new List<string>();
                                        List<int> aSetNumbers = new List<int>();
                                        List<int> aImageNumbers = new List<int>();
                                        string beforeBaseName = beforeSetups[0].Attribute("BaseName").Value;
                                        string cp = "Left";
                                        if (c > 0)
                                            cp = "Right";
                                        XElement quant = new XElement("Quantify",
                                            new XAttribute("WorkingDirectory", WorkingDirectory),
                                            new XAttribute("TemplateDirectory", TemplateDirectory),
                                            new XAttribute("ParameterFile", QuantParameterFileName),
                                            new XAttribute("TemplatePrefix", Device),
                                            new XAttribute("RotorPosition", r.ToString()),
                                            new XAttribute("ChipPosition", cp),
                                            new XAttribute("SubImage", s.ToString()),
                                            new XAttribute("BaseName", String.Format("{0}_{1}_P{2:D4}",
                                                    BaseNameStub, RotorChipSubLabel(r, c, s), newProgramCounter)),
                                            new XAttribute("BeforeSetNumber", BeforeSetNumb.ToString()),
                                            new XAttribute("BeforeBaseName", beforeBaseName),
                                            new XAttribute("BeforeImageNumber", beforeNumber.ToString("D3")),
                                            new XAttribute("ProgramCounter", newProgramCounter.ToString("D4")),
                                            new XAttribute("Success", "false"),
                                            new XAttribute("Performed", "false")
                                           );
                                        aBaseNames.Clear();
                                        aSetNumbers.Clear();
                                        aImageNumbers.Clear();
                                        for (int k = 0; k < afterSetups.Count; k++)
                                        {
                                            if (afterSetups[k].Attribute("Selected") != null
                                                    && bool.TryParse(afterSetups[k].Attribute("Selected").Value, out bool selA)
                                                    && selA
                                                    && afterSetups[k].Attribute("BaseName") != null
                                                    && afterSetups[k].Attribute("BaseName").Value.Length > 4
                                                    && afterSetups[k].Parent.Parent.Attribute("ImageNumber") != null
                                                    && Int32.TryParse(afterSetups[k].Parent.Parent.Attribute("ImageNumber").Value, out int aNumb)
                                                    && afterSetups[k].Parent.Parent.Attribute("SetNumber") != null
                                                    && Int32.TryParse(afterSetups[k].Parent.Parent.Attribute("SetNumber").Value, out int aSet))
                                            {
                                                aBaseNames.Add(afterSetups[k].Attribute("BaseName").Value);
                                                aSetNumbers.Add(aSet);
                                                aImageNumbers.Add(aNumb);
                                            }

                                        }
                                        if (aBaseNames.Count > 0)
                                        {
                                            XElement aftNamesNode = CollectionNode("AfterBaseNames", "AfterBaseName");
                                            List<XElement> aftList = new List<XElement>();
                                            for (int j = 0; j < aBaseNames.Count; j++)
                                            {
                                                XElement aftItem = new XElement("AfterBaseName",
                                                    new XAttribute("Name", aBaseNames[j]),
                                                    new XAttribute("Set", aSetNumbers[j].ToString()),
                                                    new XAttribute("ImageNumber", aImageNumbers[j].ToString())
                                                    );
                                                aftNamesNode.Add(aftItem);
                                                aftList.Add(aftItem);
                                            }
                                            quant.Add(aftNamesNode);
                                            bool duplicate = false;
                                            IEnumerable<XElement> existingQIEnum = from el in quantNode.Descendants("Quantify")
                                                                             where el.Attribute("ParameterFile").Value.IndexOf(QuantParameterFileName) >= 0
                                                                             && el.Attribute("SubImage").Value.IndexOf(s.ToString()) >= 0
                                                                             && el.Attribute("BeforeSetNumber").Value.IndexOf(BeforeSetNumb.ToString()) >= 0
                                                                             && el.Attribute("BeforeBaseName").Value.IndexOf(beforeBaseName) >= 0
                                                                             && el.Attribute("BeforeImageNumber").Value.IndexOf(beforeNumber.ToString()) >= 0
                                                                             select el;
                                            if (existingQIEnum != null && existingQIEnum.Count() > 0)
                                            {
                                                List<XElement> existingQList = existingQIEnum.ToList();
                                                {
                                                    for (int j = 0; j < existingQList.Count; j++)
                                                    {
                                                        List<XElement> existingAfterNamesList = existingQList[j].Descendants("AfterBaseName").ToList();
                                                        if (existingAfterNamesList.Count == aftList.Count)
                                                        {
                                                            bool match = true;
                                                            for (int jj = 0; jj < existingAfterNamesList.Count; jj++)
                                                            {
                                                                if (existingAfterNamesList[jj].Attribute("Name").Value.IndexOf(aftList[jj].Attribute("Name").Value) >= 0)
                                                                {
                                                                    int v1;
                                                                    int w1;
                                                                    if (Int32.TryParse(existingAfterNamesList[jj].Attribute("Set").Value, out v1)
                                                                        && Int32.TryParse(aftList[jj].Attribute("Set").Value, out w1))
                                                                    {
                                                                        if (v1 != w1)
                                                                        {
                                                                            match = false;
                                                                            break;
                                                                        }
                                                                        else
                                                                        {
                                                                            if (Int32.TryParse(existingAfterNamesList[jj].Attribute("ImageNumber").Value, out v1)
                                                                                && Int32.TryParse(aftList[jj].Attribute("ImageNumber").Value, out w1))
                                                                            {
                                                                                if (v1 != w1)
                                                                                {
                                                                                    match = false;
                                                                                    break;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        match = false;
                                                                        break;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    match = false;
                                                                    break;
                                                                }

                                                            }
                                                            if (match)
                                                            {
                                                                duplicate = true;
                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if (duplicate)
                                            {

                                            }
                                            else
                                            {
                                                quantNode.Add(quant);
                                                ProgramCounter = newProgramCounter;
                                                SetProgramCounter(newProgramCounter);
                                                newAnalysis++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return newAnalysis;
        }
        public int GetQuantInfoArray(out string[] QuantInfo, out string Err)
        {
            int res = GetQuantInfo(out List<string> quantInfoList, out Err);
            QuantInfo = quantInfoList.ToArray();
            return res;
        }
        public int GetQuantInfo(out List<string> QuantInfo, out string Err)
        {
            return GetQuantifications(true, true, true, out List<string> QuantString, out QuantInfo, out Err);
        }
        public int GetQuantifications(bool All, bool RStatus, bool SStatus, out List<string> QuantSetups, out List<string> QuantInfo, out string Err)
        {
            Err = "";
            QuantSetups = new List<string>();
            QuantInfo = new List<string>();
            Err = "";
            //Setups = new List<Tuple<string, string>>();
            //Tuple<string, string> newTuple;
            if (_XDocLoaded)
            {
                List<XElement> qsetup = ImageDoc.Descendants("Quantifications").Descendants("Quantify").ToList();
                if (qsetup.Count > 0)
                {
                    for (int n = 0; n < qsetup.Count; n++)
                    {
                        if (qsetup[n].Attribute("BeforeImageNumber") != null
                            && qsetup[n].Attribute("BeforeBaseName") != null
                            && qsetup[n].Attribute("ParameterFile") != null
                            && qsetup[n].Attribute("ProgramCounter") != null
                            && qsetup[n].Attribute("BeforeSetNumber") != null
                            && qsetup[n].Attribute("RotorPosition") != null
                            && qsetup[n].Attribute("ChipPosition") != null
                            && qsetup[n].Attribute("SubImage") != null
                            && qsetup[n].Attribute("Performed") != null
                            && qsetup[n].Attribute("Success") != null
                            && qsetup[n].Attribute("BaseName") != null)
                        {
                            string beforeImageN = qsetup[n].Attribute("BeforeImageNumber").Value;
                            string beforeBaseName = qsetup[n].Attribute("BeforeBaseName").Value;
                            string parameterFile = qsetup[n].Attribute("ParameterFile").Value;
                            string spc = qsetup[n].Attribute("ProgramCounter").Value;
                            string bSetN = qsetup[n].Attribute("BeforeSetNumber").Value;
                            string rotorP = qsetup[n].Attribute("RotorPosition").Value;
                            string chipP = qsetup[n].Attribute("ChipPosition").Value;
                            string subIm = qsetup[n].Attribute("SubImage").Value;
                            string performed = qsetup[n].Attribute("Performed").Value;
                            string success = qsetup[n].Attribute("Success").Value;
                            string basename = qsetup[n].Attribute("BaseName").Value;
                            if (Int32.TryParse(beforeImageN, out _) && Int32.TryParse(bSetN, out _)
                               && Int32.TryParse(spc, out _) && beforeBaseName != null && parameterFile != null)
                            {
                                IEnumerable<XElement> qList = qsetup[n].Descendants("AfterBaseNames").Descendants("AfterBaseName");
                                string per;
                                string suc;
                                bool bPer;
                                bool bSuc;
                                if (!All)
                                {
                                    per = qsetup[n].Attribute("Performed").Value;
                                    suc = qsetup[n].Attribute("Success").Value;
                                    if (bool.TryParse(per, out bPer) && bool.TryParse(suc, out bSuc))
                                    {
                                        if (qList.Count() > 0)
                                        {
                                            if (RStatus == bPer)
                                                bPer = true;
                                            else
                                                bPer = false;
                                            if (SStatus == bSuc)
                                                bSuc = true;
                                            else
                                                bSuc = false;
                                        }
                                        if (bPer && bSuc)
                                            All = true;
                                    }
                                }

                                if (All)
                                {
                                    if (beforeImageN != null)
                                    {
                                        QuantSetups.Add(qsetup[n].ToString());
                                        QuantInfo.Add(String.Format("{0},{1},{2},{3},{4},{5},{6},{7}",
                                            spc, basename, rotorP, chipP, subIm, parameterFile, performed, success));
                                    }
                                }
                                else
                                {
                                    Err += String.Format("No <AfterBaseName> nodes in quant setup {0}: {1} .  ", n, qsetup[n].ToString());
                                }
                            }
                            else
                            {
                                Err += String.Format("Error in quant setup {0}: {1} .  ", n, qsetup[n].ToString());
                            }
                        }
                    }
                }
            }
            return QuantSetups.Count;
            //String.Format("sdpcrQuantLib( Err, {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7} BaseNamesAfter, SetNumbersAfter)",
            //    WorkingDirectory, TemplateDirectory, ParameterFile, Device, S.ToString(), baseNameOut, aBaseNames, aSetNumbers)
        }
        public int GetQuantAnalysisArray(int BeforeSetNumb, bool All, bool RStatus, bool SStatus,
            out string[] SetupStrings, out string Err)
        {
            int res = GetQuantAnalysisStrings(BeforeSetNumb, All, RStatus, SStatus,
                out List<string> setupStringsList, out Err);
            SetupStrings = setupStringsList.ToArray();
            return res;
        }
        public int GetQuantAnalysisStrings(int BeforeSetNumb, bool All, bool RStatus, bool SStatus,
            out List<string> SetupStrings, out string Err)
        {
            Err = "";
            SetupStrings = new List<string>();
            //Tuple<string, string> newTuple;
            if (_XDocLoaded)
            {
                List<XElement> quantSetupList = ImageDoc.Descendants("Quantifications").Descendants("Quantify").ToList();
                if (quantSetupList.Count > 0)
                {
                    for (int n = 0; n < quantSetupList.Count; n++)
                    {
                        string pPerformed = quantSetupList[n].Attribute("Performed").Value;
                        string pSuccess = quantSetupList[n].Attribute("Success").Value;
                        string pProgramC = quantSetupList[n].Attribute("ProgramCounter").Value;
                        string pBaseName = quantSetupList[n].Attribute("BaseName").Value;
                        string pRotor = quantSetupList[n].Attribute("RotorPosition").Value;
                        string pChip = quantSetupList[n].Attribute("ChipPosition").Value;
                        string pSubImage = quantSetupList[n].Attribute("SubImage").Value;
                        string pPrefix = quantSetupList[n].Attribute("TemplatePrefix").Value;
                        string pTDirectory = quantSetupList[n].Attribute("TemplateDirectory").Value;
                        string pWDirectory = quantSetupList[n].Attribute("WorkingDirectory").Value;
                        string pBeforeImageNumber = quantSetupList[n].Attribute("BeforeImageNumber").Value;
                        string pBeforeBaseName = quantSetupList[n].Attribute("BeforeBaseName").Value;
                        string pBeforeSetNumber = quantSetupList[n].Attribute("BeforeSetNumber").Value;
                        string pParameterFile = quantSetupList[n].Attribute("ParameterFile").Value;

                        bool bPerformed;
                        bool bSuccess;
                        int iBeforeSetNumber;
                        if (bool.TryParse(pPerformed, out bPerformed)
                            && bool.TryParse(pSuccess, out bSuccess)
                            && Int32.TryParse(pProgramC, out _)
                            && Int32.TryParse(pRotor, out _)
                            && (pChip.IndexOf("Left", StringComparison.CurrentCultureIgnoreCase) >= 0 ||
                                pChip.IndexOf("Right", StringComparison.CurrentCultureIgnoreCase) >= 0)
                            && Int32.TryParse(pSubImage, out _)
                            && Int32.TryParse(pBeforeImageNumber, out _)
                            && Int32.TryParse(pBeforeSetNumber, out iBeforeSetNumber)
                            && iBeforeSetNumber == BeforeSetNumb)
                        {
                            if (RStatus == bPerformed)
                                bPerformed = true;
                            else
                                bPerformed = false;
                            if (SStatus == bSuccess)
                                bSuccess = true;
                            else
                                bSuccess = false;

                            if ((bSuccess && bPerformed) || All)
                            {
                                StringBuilder cmd = new StringBuilder();
                                cmd.Clear();
                                cmd.Append(String.Format("sdpcrQuant.exe {0} {1} {2} {3} {4} {5} {6}",
                                    pWDirectory, pTDirectory, pParameterFile, pPrefix, pSubImage, pBaseName, pBeforeBaseName));
                                List<XElement> qAfterBaseNamesList = quantSetupList[n].Descendants("AfterBaseNames").Descendants("AfterBaseName").ToList();
                                if (qAfterBaseNamesList.Count > 0)
                                {
                                    //List<string> afterBaseNameArgs = new List<string>();
                                    for (int m = 0; m < qAfterBaseNamesList.Count; m++)
                                    {
                                        string aBase = qAfterBaseNamesList[m].Attribute("Name").Value;
                                        string aSet = qAfterBaseNamesList[m].Attribute("Set").Value;
                                        cmd.Append(String.Format(" {0} {1}", aBase, aSet));
                                    }
                                    cmd.Append(String.Format(" | {0}", pProgramC));
                                    SetupStrings.Add(cmd.ToString());
                                }
                            }
                            //sdpcrQuantLib(std::string & Err, std::string WorkingDirectory, std::string TemplateDirectory,
                            //std::string ParameterFile, std::string Prefix, std::string SubImageLabel,
                            //std::string BaseNameOutput, std::string BaseNameInput,
                            //std::vector < std::string > BaseNamesAfter, const std::vector<int> SetNumbersAfter = std::vector<int>());

                        }
                    }
                }
            }
            return SetupStrings.Count;
        }
        public int GetQuantAnalysisSetups(int BeforeSetNumb, bool RStatus, bool SStatus,
            out List<string> QuantSetupStrings, out List<XElement> QuantSetupInfo, out string Err)
        {
            Err = "";
            QuantSetupStrings = new List<string>();
            QuantSetupInfo = new List<XElement>();
            //Tuple<string, string> newTuple;
            if (_XDocLoaded)
            {
                List<XElement> quantSetupList = ImageDoc.Descendants("Quantifications").Descendants("Quantify").ToList();
                if (quantSetupList.Count > 0)
                {
                    for (int n = 0; n < quantSetupList.Count; n++)
                    {
                        string pPerformed = quantSetupList[n].Attribute("Performed").Value;
                        string pSuccess = quantSetupList[n].Attribute("Success").Value;
                        string pProgramCounter = quantSetupList[n].Attribute("ProgramCounter").Value;
                        string pBaseName = quantSetupList[n].Attribute("BaseName").Value;
                        string pRotor = quantSetupList[n].Attribute("RotorPosition").Value;
                        string pChip = quantSetupList[n].Attribute("ChipPosition").Value;
                        string pSubImage = quantSetupList[n].Attribute("SubImage").Value;
                        string pPrefix = quantSetupList[n].Attribute("TemplatePrefix").Value;
                        string pTDirectory = quantSetupList[n].Attribute("TemplateDirectory").Value;
                        string pWDirectory = quantSetupList[n].Attribute("WorkingDirectory").Value;
                        string pBeforeImageNumber = quantSetupList[n].Attribute("BeforeImageNumber").Value;
                        string pBeforeBaseName = quantSetupList[n].Attribute("BeforeBaseName").Value;
                        string pBeforeSetNumber = quantSetupList[n].Attribute("BeforeSetNumber").Value;
                        string pParameterFile = quantSetupList[n].Attribute("ParameterFile").Value;
                        bool bPerformed;
                        bool bSuccess;
                        int iBeforeSetNumber;
                        if (bool.TryParse(pPerformed, out bPerformed)
                            && bool.TryParse(pSuccess, out bSuccess)
                            && Int32.TryParse(pProgramCounter, out _)
                            && Int32.TryParse(pRotor, out _)
                            && Int32.TryParse(pChip, out _)
                            && Int32.TryParse(pSubImage, out _)
                            && Int32.TryParse(pBeforeImageNumber, out _)
                            && Int32.TryParse(pBeforeSetNumber, out iBeforeSetNumber)
                            && iBeforeSetNumber == BeforeSetNumb)
                        {
                            if (RStatus == bPerformed)
                                bPerformed = true;
                            else
                                bPerformed = false;
                            if (SStatus == bSuccess)
                                bSuccess = true;
                            else
                                bSuccess = false;

                            if (bSuccess && bPerformed)
                            {
                                StringBuilder cmd = new StringBuilder();
                                cmd.Clear();
                                cmd.Append(String.Format("sdpcrQuant.exe {0} {1} {2} {3} {4} {5} {6}",
                                    pWDirectory, pTDirectory, pParameterFile, pPrefix, pSubImage, pBaseName, pBeforeBaseName));
                                List<XElement> qAfterBaseNamesList = quantSetupList[n].Descendants("AfterBaseNames").Descendants("AfterBaseName").ToList();
                                if (qAfterBaseNamesList.Count > 0)
                                {
                                    XElement afterBaseNameArgs = new XElement("AfterBaseNames", new XAttribute("CollectionType", "AfterBaseName"));
                                    for (int m = 0; m < qAfterBaseNamesList.Count; m++)
                                    {
                                        string aBase = qAfterBaseNamesList[m].Attribute("Name").Value;
                                        string aSet = qAfterBaseNamesList[m].Attribute("Set").Value;
                                        cmd.Append(String.Format(" {0} {1}", aBase, aSet));
                                        cmd.Append(String.Format(" | {0}", pProgramCounter));
                                        afterBaseNameArgs.Add(XElement.Parse(qAfterBaseNamesList[m].ToString()));
                                    }
                                    int count = QuantSetupStrings.Count;
                                    XElement newSetup = new XElement("SetupInfo",
                                            new XAttribute("ScriptIndex", count.ToString()),
                                            new XAttribute("ProgramCounter", pProgramCounter),
                                            new XAttribute("Success", pSuccess),
                                            new XAttribute("Performed", pPerformed),
                                            new XAttribute("RotorPosition", pRotor),
                                            new XAttribute("ChipPosition", pChip),
                                            new XAttribute("SubImage", pSubImage),
                                            new XAttribute("BaseName", pBaseName),
                                            new XAttribute("ParameterFile", pParameterFile),
                                            new XAttribute("BeforeBaseName", pBeforeBaseName),
                                            new XAttribute("BeforeImageNumber", pBeforeImageNumber),
                                            new XAttribute("BeforeSetNumber", pBeforeSetNumber)
                                            );
                                    newSetup.Add(afterBaseNameArgs);
                                    QuantSetupInfo.Add(newSetup);
                                    QuantSetupStrings.Add(cmd.ToString());
                                }
                            }
                            //sdpcrQuantLib(std::string & Err, std::string WorkingDirectory, std::string TemplateDirectory,
                            //std::string ParameterFile, std::string Prefix, std::string SubImageLabel,
                            //std::string BaseNameOutput, std::string BaseNameInput,
                            //std::vector < std::string > BaseNamesAfter, const std::vector<int> SetNumbersAfter = std::vector<int>());

                        }
                    }
                }
            }
            return QuantSetupStrings.Count;
        }
        public bool UpdateQuantificationRunStatus(int ProgramNumber, bool NewPerformed, bool NewSuccess)
        {
            IEnumerable<XElement> qIEnum = from el in ImageDoc.Descendants("Quantifications").Descendants("Quantify")
                                           where el.Attribute("ProgramCounter").Value.IndexOf(ProgramNumber.ToString()) >= 0
                                           select el;
            if (qIEnum.Count() == 1)
            {
                List<XElement> qList = qIEnum.ToList();
                qList[0].Attribute("Performed").Value = NewPerformed.ToString();
                qList[0].Attribute("Success").Value = NewSuccess.ToString();
                return true;
            }
            return false;
        }
        public int AddCombineSteps(string QuantDye, int[] AfterSetNumbersArray, out string Err)
        {
            char[] digits = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            char[] letterP = new char[] { 'P' };
            char[] vBar = new char[] { '|' };
            Err = "";
            for (int m = 0; m < AfterSetNumbersArray.Length; m++)
            {
                if (AfterSetNumbersArray[m] < 0)
                {
                    Err = "All After Set Numbs must be >= 0";
                    return 0;
                }
            }
            IEnumerable<XElement> combineNodeIEnum = ImageDoc.Descendants("CombineSections");
            if (combineNodeIEnum.Count() != 1)
            {
                Err = "Problem with CombineSections node in xml document. Doesn't exist, or multiples.";
            }
            XElement combineNode = combineNodeIEnum.FirstOrDefault();
            List<int> afterSetNumbers = AfterSetNumbersArray.ToList();
            int newAnalysis = 0;
            afterSetNumbers.Sort();
            int lastIdx = afterSetNumbers.Count - 1;
            int largestSetNumber = afterSetNumbers[lastIdx];
            IdxThree SetRCid = new IdxThree(largestSetNumber + 1, true, 4, false, 2, true);
            List<List<string>> SetRotorChip = new List<List<string>>();
            for (int n = 0; n < SetRCid.Size; n++)
                SetRotorChip.Add(new List<string>());
            //Console.WriteLine(String.Format("{0}/{1}/{2}:  {3}", largestSetNumber + 1, SetRCid.Size, SetRCid.Base, SetRotorChip.Count));
            //Console.Out.Flush();
            for (int n = 0; n < SetRotorChip.Count; n++)
                SetRotorChip[n].Clear();
            List<XElement> quantifyNodes = ImageDoc.Descendants("Quantifications").Descendants("Quantify").ToList();
            string csvName = "";
            for (int n = 0; n < quantifyNodes.Count; n++)
            {
                string sPerformed = quantifyNodes[n].Attribute("Performed").Value;
                string sSuccess = quantifyNodes[n].Attribute("Success").Value;
                string baseNameStub = quantifyNodes[n].Attribute("BaseName").Value; //
                string sRotor = quantifyNodes[n].Attribute("RotorPosition").Value;
                string sChip = quantifyNodes[n].Attribute("ChipPosition").Value;
                string sSubImage = quantifyNodes[n].Attribute("SubImage").Value;
                List<XElement> afterBaseNameNodes = quantifyNodes[n].Descendants("AfterBaseNames").Descendants("AfterBaseName").ToList();
                int r;
                int c;
                int s;
                bool bPerformed;
                bool bSuccess;
                if (afterBaseNameNodes.Count > 0 && Int32.TryParse(sRotor, out r) 
                    && (sChip.IndexOf("Left", StringComparison.CurrentCultureIgnoreCase) >= 0  
                        || sChip.IndexOf("Right", StringComparison.CurrentCultureIgnoreCase) >= 0)
                    && Int32.TryParse(sSubImage, out s)
                    && r > 0 && r < 5 && s > 0 && s <= NumbSections && baseNameStub != null && baseNameStub.Length > 5
                    && bool.TryParse(sPerformed, out bPerformed) && bool.TryParse(sSuccess, out bSuccess) && bPerformed && bSuccess)
                {
                    for (int k = 0; k < afterBaseNameNodes.Count; k++)
                    {
                        int iSet;
                        string sset = afterBaseNameNodes[k].Attribute("Set").Value;
                        if (sset != null && Int32.TryParse(sset, out iSet))
                        {
                            if (sChip.IndexOf("Left", StringComparison.CurrentCultureIgnoreCase) >= 0)
                                c = 0;
                            else
                                c = 1;
                            csvName = String.Format("{0}|{1}|{2}", baseNameStub, iSet, s);
                            int idx = SetRCid.IndexOut(iSet, r, c);
                            //Console.WriteLine(String.Format("idx: {0}/{1}/{2}/{3}", idx, iSet, r, c));
                            SetRotorChip[idx].Add(csvName);
                        }
                        //BaseName << "_" << ImageNumber << "_210_" << Array << "_" << Section << "_Bef_Aft.csv
                        //sdpcrMultiQuant(std::string& Err, std::string WorkingDirectory, std::string BaseName, std::vector< std::string > AfterNames);
                    }
                }
            }
            for (int n = 0; n < SetRotorChip.Count; n++)
            {
                if (SetRotorChip[n].Count > 0)
                {
                    List<string> csvNameBegin = new List<string>(SetRotorChip[n].Count);
                    List<string> csvNameEnd = new List<string>(SetRotorChip[n].Count);
                    string combineBase = "";
                    for (int k = 0; k < SetRotorChip[n].Count; k++)
                    {
                        string[] csvStubs = SetRotorChip[n][k].Split(vBar);

                        if (csvStubs.Length > 2)
                        {
                            if (k == 0)
                            {
                                int itmp = csvStubs[0].LastIndexOf('Q');
                                string stmp = csvStubs[0];
                                
                                char[] stmpArray = stmp.ToCharArray();
                                stmpArray[itmp] = 'C';
                                stmp = new string(stmpArray);
                                itmp = stmp.LastIndexOf('P') - 2;
                                string rtmp = stmp.Substring(0, itmp);
                                combineBase = String.Format("{0}_{1}_", rtmp, csvStubs[1]);
                                //combineBase = String.Format("{0}{1}_",
                                    //csvStubs[0].TrimEnd(digits).TrimEnd(letterP), csvStubs[1]);
                            }
                            csvNameBegin.Add(String.Format("{0}_{1}_210_", csvStubs[0], csvStubs[1]));
                            csvNameEnd.Add(String.Format("_{0}_Bef_Aft.csv", csvStubs[2]));
                        }
                    }

                    for (int a = 1; a <= NumbArrays; a++)
                    {
                        //int ia = a - 1;
                        int newProgramCounter = ProgramCounter + 1;
                        string baseName = combineBase + a.ToString() + "_P" + newProgramCounter.ToString("D4");
                        XElement combine = new XElement("Combine",
                            new XAttribute("WorkingDirectory", WorkingDirectory),
                            new XAttribute("BaseName", baseName),
                            new XAttribute("ProgramCounter", newProgramCounter.ToString("D4")),
                            new XAttribute("Performed", "false"),
                            new XAttribute("Success", "false")
                            );
                        XElement afterNames = CollectionNode("AfterNames", "AfterName");
                        for (int m = 0; m < csvNameBegin.Count; m++)
                        {
                            string tmp = csvNameBegin[m] + a.ToString() + csvNameEnd[m];
                            XElement aName = new XElement("AfterName", new XAttribute("Name", tmp));
                            afterNames.Add(aName);
                        }
                        combine.Add(afterNames);
                        combineNode.Add(combine);
                        ProgramCounter = newProgramCounter;
                        SetProgramCounter(newProgramCounter);
                        newAnalysis++;
                    }
                }
            }
            return newAnalysis;
        }
        public int GetCombineAnalysisArray(bool All, bool RStatus, bool SStatus, out string[] CombineStrings, out string Err)
        {
            int res = GetCombineAnalysisStrings(All, RStatus, SStatus, out List<string> combineStringsList, out Err);
            CombineStrings = combineStringsList.ToArray();
            return res;
        }
        public int GetCombineAnalysisStrings(bool All, bool RStatus, bool SStatus, out List<string> CombineStrings, out string Err)
        {
            Err = "";
            CombineStrings = new List<string>();
            StringBuilder cmd = new StringBuilder();
            List<XElement> combineList = ImageDoc.Descendants("CombineSections").Descendants("Combine").ToList();
            for (int n = 0; n < combineList.Count; n++)
            {
                string sPerformed = combineList[n].Attribute("Performed").Value;
                string sSuccess = combineList[n].Attribute("Success").Value;
                string sProgramC = combineList[n].Attribute("ProgramCounter").Value;
                bool bPerformed;
                bool bSuccess;

                if (bool.TryParse(sPerformed, out bPerformed) && bool.TryParse(sSuccess, out bSuccess))
                {
                    //bool outp = true;
                    if (RStatus == bPerformed)
                        bPerformed = true;
                    else
                        bPerformed = false;
                    if (SStatus == bSuccess)
                        bSuccess = true;
                    else
                        bSuccess = false;
                    if ( (bPerformed && bSuccess ) || All )
                    {
                        cmd.Clear();
                        string pWDirectory = combineList[n].Attribute("WorkingDirectory").Value;
                        string pBaseName = combineList[n].Attribute("BaseName").Value;

                        if (pWDirectory != null && pWDirectory.Length > 3
                            && pBaseName != null && pBaseName.Length > 3)
                        {
                            cmd.Append(String.Format("sdpcrQuantCombine.exe {0} {1}", pWDirectory, pBaseName));
                            List<XElement> afterNamesList = combineList[n].Descendants("AfterNames").Descendants("AfterName").ToList();
                            if (afterNamesList.Count > 0)
                            {
                                for (int m = 0; m < afterNamesList.Count; m++)
                                {
                                    string aName = afterNamesList[m].Attribute("Name").Value;
                                    cmd.Append(String.Format(" {0}", aName));
                                }
                                cmd.Append(String.Format(" | {0}", sProgramC));
                                CombineStrings.Add(cmd.ToString());
                            }

                        }
                    }
                }
            }
            return CombineStrings.Count;
        }
        //sdpcrMultiQuant(std::string& Err, std::string WorkingDirectory, std::string BaseName, std::vector< std::string > AfterNames);
        public bool UpdateCombineRunStatus(int ProgramNumber, bool NewPerformed, bool NewSuccess)
        {
            if (_XDocLoaded)
            {
                IEnumerable<XElement> combineIEnum = from el in ImageDoc.Descendants("CombineSections").Descendants("Combine")
                                                     where el.Attribute("ProgramCounter") != null
                                                     select el;
                if (combineIEnum.Count() > 0)
                {
                    for (int n = 0; n < combineIEnum.Count(); n++)
                    {
                        int pc;
                        if (Int32.TryParse(combineIEnum.ToList()[n].Attribute("ProgramCounter").Value, out pc))
                        {
                            if (pc == ProgramNumber)
                            {
                                if (combineIEnum.ToList()[n].Attribute("Performed") != null && combineIEnum.ToList()[n].Attribute("Success") != null)
                                {
                                    combineIEnum.ToList()[n].Attribute("Performed").Value = NewPerformed.ToString();
                                    combineIEnum.ToList()[n].Attribute("Success").Value = NewSuccess.ToString();
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }
        public int GetCombineAnalysisSetups(bool RStatus, bool SStatus, out List<string> CombineStrings, 
            out List<XElement> CombineSetupInfo, out string Err)
        {
            Err = "";
            CombineStrings = new List<string>();
            CombineSetupInfo = new List<XElement>();
            StringBuilder cmd = new StringBuilder();
            List<XElement> combineList = ImageDoc.Descendants("CombineSections").Descendants("Combine").ToList();
            for (int n = 0; n < combineList.Count; n++)
            {
                string sPerformed = combineList[n].Attribute("Performed").Value;
                string sSuccess = combineList[n].Attribute("Success").Value;
                bool bPerformed;
                bool bSuccess;

                if (bool.TryParse(sPerformed, out bPerformed) && bool.TryParse(sSuccess, out bSuccess))
                {
                    
                    if (RStatus == bPerformed)
                        bPerformed = true;
                    else
                        bPerformed = false;
                    if (SStatus == bSuccess)
                        bSuccess = true;
                    else
                        bSuccess = false;
                    if (bPerformed && bSuccess)
                    {
                        cmd.Clear();
                        string pWDirectory = combineList[n].Attribute("WorkingDirectory").Value;
                        string pBaseName = combineList[n].Attribute("BaseName").Value;
                        string pProgramCounter = combineList[n].Attribute("ProgramCounter").Value;
                        if (pWDirectory != null && pWDirectory.Length > 3
                            && pBaseName != null && pBaseName.Length > 3
                            && pProgramCounter != null)
                        {
                            cmd.Append(String.Format("sdpcrQuantCombine.exe {0} {1}", pWDirectory, pBaseName));
                            List<XElement> afterNamesList = combineList[n].Descendants("AfterNames").Descendants("AfterName").ToList();
                            if (afterNamesList.Count > 0)
                            {
                                for (int m = 0; m < afterNamesList.Count; m++)
                                {
                                    string aName = afterNamesList[m].Attribute("Name").Value;
                                    cmd.Append(String.Format(" {0}", aName));
                                }
                                int count = CombineStrings.Count;
                                CombineStrings.Add(cmd.ToString());
                                XElement newSetup = new XElement("SetupInfo",
                                    new XAttribute("ScriptIndex", count.ToString()),
                                    new XAttribute("ProgramCounter", pProgramCounter),
                                    new XAttribute("Success", sSuccess),
                                    new XAttribute("Performed", sPerformed)
                                    );
                                CombineSetupInfo.Add(newSetup);
                            }
                        }
                    }
                }
            }
            return CombineStrings.Count;
        }
        public bool GetAnalysis(int ProgramNumber, out string AnalysisString, out string Err)
        {
            Err = "";
            AnalysisString = "";
            if (_XDocLoaded)
            {
                IEnumerable<XElement> imageSetupIEnum = from el in ImageDoc.Descendants("Images").Descendants("Image")
                                                     .Descendants("AnalysisSetup").Descendants("Analysis")
                                                        where el.Attribute("ProgramCounter") != null
                                                        select el;
                if (imageSetupIEnum.Count() > 0)
                {
                    for (int n = 0; n < imageSetupIEnum.Count(); n++)
                    {
                        int pc;
                        if (Int32.TryParse(imageSetupIEnum.ToList()[n].Attribute("ProgramCounter").Value, out pc))
                        {
                            if (pc == ProgramNumber)
                            {
                                AnalysisString = imageSetupIEnum.ToList()[n].ToString();
                                return true;
                            }
                        }
                    }
                }
                IEnumerable<XElement> quantSetupIEnum = from el in ImageDoc.Descendants("Quantifications").Descendants("Quantify")
                                                        where el.Attribute("ProgramCounter") != null
                                                        select el;
                if (quantSetupIEnum.Count() > 0)
                {
                    for (int n = 0; n < quantSetupIEnum.Count(); n++)
                    {
                        int pc;
                        if (Int32.TryParse(quantSetupIEnum.ToList()[n].Attribute("ProgramCounter").Value, out pc))
                        {
                            if (pc == ProgramNumber)
                            {
                                AnalysisString = quantSetupIEnum.ToList()[n].ToString();
                                return true;
                            }
                        }
                    }
                }

                IEnumerable<XElement> combineSetupIEnum = from el in ImageDoc.Descendants("CombineSections").Descendants("Combine")
                                                          where el.Attribute("ProgramCounter") != null
                                                          select el;
                if (combineSetupIEnum.Count() > 0)
                {
                    for (int n = 0; n < combineSetupIEnum.Count(); n++)
                    {
                        int pc;
                        if (Int32.TryParse(combineSetupIEnum.ToList()[n].Attribute("ProgramCounter").Value, out pc))
                        {
                            if (pc == ProgramNumber)
                            {
                                AnalysisString = combineSetupIEnum.ToList()[n].ToString();
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
        public bool AlternativeSetup(string ImageName, int ProgramNumber, bool ChangeFilterLabel, bool ChangeExtraOutput,
            bool ChangeUseMedianFilter, string NewFilterLabel, int NewExtraOutput = 0, bool NewUseMedianFilter = true)
        {
            char[] digits = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            if (ChangeFilterLabel && NewFilterLabel.Length < 3)
                ChangeFilterLabel = false;
            if (_XDocLoaded && (ChangeFilterLabel || ChangeExtraOutput || ChangeUseMedianFilter) )
            {
                IEnumerable<XElement> imageSetupIEnum = from el in ImageDoc.Descendants("Images").Descendants("Image")
                                                     .Descendants("AnalysisSetup").Descendants("Analysis")
                                                        where el.Attribute("Name") != null
                                                        && el.Attribute("Name").Value.IndexOf(ImageName) >= 0
                                                        && el.Attribute("ProgramCounter") != null
                                                        select el;
                if (imageSetupIEnum.Count() > 0)
                {
                    for (int n = 0; n < imageSetupIEnum.Count(); n++)
                    {
                        int pc;
                        if (Int32.TryParse(imageSetupIEnum.ToList()[n].Attribute("ProgramCounter").Value, out pc))
                        {
                            if (pc == ProgramNumber)
                            {
                                XElement newAnalysis = XElement.Parse(imageSetupIEnum.ToList()[n].ToString());
                                int newProgramCounter = ProgramCounter + 1;
                                if ( newAnalysis.Attribute("ProgramConter") != null
                                    && newAnalysis.Attribute("FilterLabel") != null
                                    && newAnalysis.Attribute("ExtraOutput") != null
                                    && newAnalysis.Attribute("UseMedianFilter") != null
                                    && newAnalysis.Attribute("BaseName") != null
                                    )
                                {
                                    if (ChangeFilterLabel)
                                        newAnalysis.Attribute("FilterLabel").Value = NewFilterLabel;
                                    if (ChangeExtraOutput)
                                        newAnalysis.Attribute("ExtraOutput").Value = NewExtraOutput.ToString();
                                    if (ChangeUseMedianFilter)
                                        newAnalysis.Attribute("UseMedianFilter").Value = NewUseMedianFilter.ToString();
                                    string pcNumb = newProgramCounter.ToString("D4");
                                    string newBaseName = newAnalysis.Attribute("BaseName").Value.TrimEnd(digits) + pcNumb;
                                    newAnalysis.Attribute("ProgramConter").Value = pcNumb;
                                    newAnalysis.Attribute("BaseName").Value = newBaseName;
                                    newAnalysis.Attribute("Performed").Value = "false";
                                    newAnalysis.Attribute("Success").Value = "false";
                                    ProgramCounter = newProgramCounter;
                                    SetProgramCounter(ProgramCounter);
                                    imageSetupIEnum.ToList()[n].Parent.Add(newAnalysis);
                                    return true;
                                }  
                            }
                        }
                    }
                }
            }
            return false;
        }

        public bool AlternativeQuantSetup(int ProgramNumber, string NewParameterFile)
        {
            char[] digits = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            if (_XDocLoaded && NewParameterFile.Length > 3)
            {
                IEnumerable<XElement> quantSetupIEnum = from el in ImageDoc.Descendants("Quantifications").Descendants("Quantify")
                                                        where el.Attribute("ProgramCounter") != null
                                                        select el;
                if (quantSetupIEnum.Count() > 0)
                {
                    for (int n = 0; n < quantSetupIEnum.Count(); n++)
                    {
                        int pc;
                        if (Int32.TryParse(quantSetupIEnum.ToList()[n].Attribute("ProgramCounter").Value, out pc))
                        {
                            if (pc == ProgramNumber)
                            {
                                XElement newAnalysis = XElement.Parse(quantSetupIEnum.ToList()[n].ToString());
                                if (newAnalysis.Attribute("ParamterFile") != null
                                    && newAnalysis.Attribute("ProgramCounter") != null
                                    && newAnalysis.Attribute("BaseName") != null)
                                {
                                    int newProgramCounter = ProgramCounter + 1;
                                    newAnalysis.Attribute("ParamterFile").Value = NewParameterFile;
                                    newAnalysis.Attribute("ProgramCounter").Value = newProgramCounter.ToString("D4");
                                    string pcNumb = newProgramCounter.ToString("D4");
                                    string newBaseName = newAnalysis.Attribute("BaseName").Value.TrimEnd(digits) + pcNumb;
                                    newAnalysis.Attribute("ProgramConter").Value = pcNumb;
                                    newAnalysis.Attribute("BaseName").Value = newBaseName;
                                    newAnalysis.Attribute("Performed").Value = "false";
                                    newAnalysis.Attribute("Success").Value = "false";
                                    ProgramCounter = newProgramCounter;
                                    SetProgramCounter(ProgramCounter);
                                    quantSetupIEnum.ToList()[n].Parent.Add(newAnalysis);
                                }
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        public bool AlternativeCombineSetup(int ProgramNumber, List<int> ExcludedSections)
        {
            char[] digits = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            char[] under = new char[] { '_' };
            if (_XDocLoaded && ExcludedSections.Count > 0)
            {
                bool cont = false;
                for (int n = 0; n < ExcludedSections.Count; n++)
                {
                    if (ExcludedSections[n] > 0 && ExcludedSections[n] <= NumbSections)
                    {
                        cont = true;
                        break;
                    }
                }
                if (cont)
                {
                    IEnumerable<XElement> combineSetupIEnum = from el in ImageDoc.Descendants("CombineSections").Descendants("Combine")
                                                              where el.Attribute("ProgramCounter") != null
                                                              select el;
                    if (combineSetupIEnum.Count() > 0)
                    {
                        for (int n = 0; n < combineSetupIEnum.Count(); n++)
                        {
                            int pc;
                            if (Int32.TryParse(combineSetupIEnum.ToList()[n].Attribute("ProgramCounter").Value, out pc))
                            {
                                if (pc == ProgramNumber)
                                {
                                    int newProgramCounter = ProgramCounter + 1;
                                    List<XElement> oldAfterNamesList 
                                        = combineSetupIEnum.ToList()[n].Descendants("AfterNames").Descendants("AfterName").ToList();
                                    List<XElement> newAfterNamesList = new List<XElement>();
                                    for (int m = 0; m < oldAfterNamesList.Count; m++)
                                    {
                                        bool addName = true;
                                        string pCSVName = oldAfterNamesList[m].Attribute("Name").Value;
                                        int pos = pCSVName.IndexOf("_Bef_Aft.csv");
                                        if (pos > 3)
                                        {
                                            string sect = pCSVName.Substring(pos - 2, 2).Trim(under);
                                            int iSect;
                                            if (Int32.TryParse(sect, out iSect))
                                            {
                                                for (int k = 0; k < ExcludedSections.Count; k++)
                                                {
                                                    if (iSect == ExcludedSections[k])
                                                    {
                                                        addName = false;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        if (addName)
                                        {
                                            newAfterNamesList.Add(oldAfterNamesList[m]);
                                        }
                                    }
                                    if ( newAfterNamesList.Count > 0 && combineSetupIEnum.ToList()[n].Attribute("BaseName") != null)
                                    {
                                        string newBaseName = (combineSetupIEnum.ToList()[n].Attribute("BaseName").Value).TrimEnd(digits);
                                        XElement newAnalysis = new XElement("Combined",
                                            new XAttribute("Performed", "false"),
                                            new XAttribute("Success", "false"),
                                            new XAttribute("ProgramCounter", newProgramCounter.ToString("D4")),
                                            new XAttribute("BaseName", newBaseName),
                                            new XAttribute("WorkingDirectory", WorkingDirectory)
                                            );
                                        XElement newAfterNames = new XElement("AfterNames", new XAttribute("CollectionType", "AfterName"));
                                        for ( int m = 0; m < newAfterNamesList.Count; m++)
                                            newAfterNames.Add(newAfterNamesList[m]);

                                        newAnalysis.Add(newAfterNames);
                                        combineSetupIEnum.ToList()[n].Parent.Add(newAnalysis);
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }
    };
}
