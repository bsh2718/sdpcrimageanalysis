﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.ExceptionServices;
using System.Data;

namespace manageAnalysis
{
    using IThree;
    using System.Collections.Specialized;
    using System.Runtime.Remoting.Messaging;
    using System.Security.Cryptography;

    public partial class ImageData
    {
        char[] delim = new char[] { ' ', '\n', '\t' };
        protected XDocument ImageDoc;
        bool _XDocLoaded;
        bool _settingsLoaded;
        IdxThree RCSid;
        public string Device
        {
            get; protected set;
        }
        public int NumbArrays
        {
            get; protected set;
        }
        public int NumbSections
        {
            get; protected set;
        }
        public string WorkingDirectory
        {
            get; protected set;
        }
        public string TemplateDirectory
        {
            get; protected set;
        }
        public int ProgramCounter
        {
            get; protected set;
        }
        public int ImageCounter
        {
            get; protected set;
        }
        public bool UseMedianFilter
        {
            get; protected set;
        }
        public bool RotateImage
        {
            get; protected set;
        }
        public int ExtraOutput
        {
            get; protected set;
        }
        public string BaseNameStub
        {
            get; protected set;
        }

        public int ListXELementToStringArray(List<XElement> XList, out string[] SArray)
        {
            List<string> sList = new List<string>(0);

            for (int n = 0; n < XList.Count; n++)
                sList.Add(XList[n].ToString());

            SArray = sList.ToArray();
            return SArray.Count();
        }

        public ImageData(string BaseName = null)
        {
            ImageDoc = new XDocument();
            _XDocLoaded = false;
            _settingsLoaded = false;
            Device = "None";
            WorkingDirectory = "None";
            TemplateDirectory = "None";
            ProgramCounter = 0;
            ImageCounter = 0;
            UseMedianFilter = true;
            RotateImage = false;
            NumbArrays = 0;
            NumbSections = 0;
            ExtraOutput = 0;
            if (BaseName != null)
                BaseNameStub = BaseName;
            else
            {
                DateTime now = DateTime.Now;
                BaseNameStub = now.ToString("SDPCRyyyyMMdd_HHmmss");
            }
        }
        ~ImageData() { }

        public bool Load(string DocString, out string Message)
        {
            _settingsLoaded = false;
            try
            {
                ImageDoc = XDocument.Parse(DocString);
            }
            catch (Exception ex)
            {
                _XDocLoaded = false;
                Message = ex.ToString();
                return false;
            }
            _XDocLoaded = true;
            return LoadSettings(out Message);
        }
        public bool CreateEmpty(string DeviceIn, string DeviceType, string ProjectIn, string WorkingDirectoryIn, string TemplateDirectoryIn,
            bool UseMedianFilterIn, bool RotateImageIn, int NumbRotorPositions, int NumbChipPositions, int NumbSections, int NumbArrays, string DateTimeIn, out string Err, string BaseName = null)
        {
            XDocument imManage = new XDocument();
            var rootElement = new XElement("SDPCRAnalysis");
            imManage.Add(rootElement);
            char[] delim = new char[] { ' ', ',', '\n', '\t' };
            string line = DateTimeIn.Trim(delim);
            string line2;
            string line3;
            RCSid = new IdxThree(NumbRotorPositions, false, NumbChipPositions, true, NumbSections, false);
            XElement xset = SimpleNode("Settings");
            var xel = new XElement("Date", new XAttribute("Value", line));
            xset.Add(xel);
            line = ProjectIn.Trim(delim);
            xel = new XElement("Project", new XAttribute("Name", line));
            xset.Add(xel);
            line = WorkingDirectoryIn.Trim(delim);
            xel = new XElement("Directory",
                new XAttribute("Type", "Working"),
                new XAttribute("Name", line));
            xset.Add(xel);
            line = TemplateDirectoryIn.Trim(delim);
            xel = new XElement("Directory",
                new XAttribute("Type", "Template"),
                new XAttribute("Name", line));
            xset.Add(xel);
            var xrotor = new XElement("Rotor",
                new XAttribute("NumbRotorPositions", NumbRotorPositions.ToString()),
                new XAttribute("NumbChipPositions", NumbChipPositions.ToString()),
                new XAttribute("RCSid_Size", RCSid.Size.ToString()));
            xset.Add(xrotor);

            line = DeviceIn.Trim(delim);
            line2 = DeviceType.Trim(delim);
            if (RotateImageIn)
                line3 = "true";
            else
                line3 = "false";

            xel = new XElement("Device",
                new XAttribute("CatNumber", line),
                new XAttribute("Type", line2),
                new XAttribute("NumbSections", NumbSections.ToString()),
                new XAttribute("NumbArrays", NumbArrays.ToString()),
                new XAttribute("RotateImage", line3)
                );
            xset.Add(xel);
            if (UseMedianFilterIn)
                line3 = "true";
            else
                line3 = "false";
            xel = new XElement("UseMedianFilter", new XAttribute("Value", line3));
            xset.Add(xel);
            xel = new XElement("ProgramCounter", new XAttribute("Value", "0"));
            xset.Add(xel);
            xel = new XElement("ImageCounter", new XAttribute("Value", "0"));
            xset.Add(xel);
            //xel = new XElement("Dyes", new XAttribute("SizingDye", "DyeA"));
            //xel.Add(new XElement("QuantDye", new XAttribute("Name", "DyeB")));
            //xset.Add(xel);
            rootElement.Add(xset);
            var ximag = CollectionNode("Images", "Image");
            var xquan = CollectionNode("Quantifications", "Quantify");
            var xcomb = CollectionNode("CombineSections", "Combine");
            var xcollate = CollectionNode("ImagesSortedByDye", "Dye");
            rootElement.Add(ximag);
            rootElement.Add(xquan);
            rootElement.Add(xcomb);
            rootElement.Add(xcollate);

            ImageDoc = imManage;
            if (BaseName != null)
                BaseNameStub = BaseName;
            else
            {
                DateTime now = DateTime.Now;
                BaseNameStub = now.ToString("SDPCRyyyyMMdd_HHmmss");
            }
            _XDocLoaded = true;
            return CheckXDocument(out Err, out int _, out int _);
        }
        public bool GetDocString(out string DocString)
        {
            if (_XDocLoaded)
            {
                DocString = ImageDoc.ToString();
                return true;
            }
            else
            {
                DocString = "";
                return false;
            }
        }
        public void ClearDocString()
        {
            if (_XDocLoaded)
            {
                _XDocLoaded = false;
                ImageDoc = new XDocument();
            }
        }
        public List<XElement> GetSetting(string Setting)
        {
            List<XElement> sList = new List<XElement>();
            List<XElement> st = ImageDoc.Descendants("Settings").ToList();
            for (int n = 0; n < st.Count; n++)
            {
                List<XElement> s = st[n].Elements(Setting).ToList();
                if (s.Count > 0)
                {
                    for (int m = 0; m < s.Count; m++)
                        sList.Add(s[m]);
                }
            }
            return sList;
        }
        protected bool SetProgramCounter(int NewCounter)
        {
            List<XElement> st = ImageDoc.Descendants("Settings").ToList();
            if (st.Count < 1)
                return false;
            for (int n = 0; n < st.Count; n++)
            {
                List<XElement> s = st[n].Elements("ProgramCounter").ToList();
                if (s.Count == 1)
                {
                    s[0].Attribute("Value").Value = NewCounter.ToString("D4");
                    //Console.WriteLine(String.Format("s[0] = {0}", s[0].ToString()));
                    return true;
                }
            }
            return false;
        }
        protected bool SetImageCounter(int NewCounter)
        {
            List<XElement> st = ImageDoc.Descendants("Settings").ToList();
            if (st.Count < 1)
                return false;
            for (int n = 0; n < st.Count; n++)
            {
                List<XElement> s = st[n].Elements("ImageCounter").ToList();
                if (s.Count == 1)
                {
                    s[0].Attribute("Value").Value = NewCounter.ToString("D3");
                    //Console.WriteLine(String.Format("s[0] = {0}", s[0].ToString()));
                    return true;
                }
            }
            return false;
        }

        protected string RotorChipSubLabel(int Rotor, int Chip, int Section, bool Before, int Set = 0)
        {
            string tmp;
            if (Before)
            {
                tmp = String.Format("B{0}{1}{2}", Rotor, Chip, Section);
            }
            else
            {
                tmp = String.Format("A{0}{1}{2}{3}", Set, Rotor, Chip, Section);
            }
            return tmp;
        }
        protected string RotorChipSubLabel(int Rotor, int Chip, int Section)
        {
            return String.Format("Q{0}{1}{2}", Rotor, Chip, Section);
        }

        protected static XElement CollectionNode(string Name, string Type)
        {
            XElement xe = new XElement(Name, new XAttribute("CollectionType", Type));
            return xe;
        }
        protected static XElement SimpleNode(string Name)
        {
            XElement xe = new XElement(Name);
            return xe;
        }

        public bool CheckXDocument(out string Err, out int NumbBeforeImages, out int NumbAfterImages)
        {
            NumbBeforeImages = 0;
            NumbAfterImages = 0;
            Err = "";
            if (!_XDocLoaded)
            {
                Err = "XDoc not loaded";
                return false;
            }
            if (!LoadSettings(out string errMsg))
            {
                Err = errMsg;
                return false;
            }
            Console.WriteLine("Before GetImageCount");
            GetImageCount(out NumbBeforeImages, out NumbAfterImages);
            return true;
        }
        protected bool LoadSettings(out string Err)
        {
            Err = "";
            if (!_XDocLoaded)
            {
                Err = "XDoc not loaded";
                return false;
            }
            bool error = false;
            List<XElement> xDevice = GetSetting("Device");
            List<XElement> xDirectories = GetSetting("Directory");
            List<XElement> xUseMedian = GetSetting("UseMedianFilter");
            List<XElement> xCounter = GetSetting("ProgramCounter");
            List<XElement> xImage = GetSetting("ImageCounter");
            if (xDevice.Count() != 1)
            {
                Err = "Problem with Device settings node. ";
                error = true;
            }
            if (xDirectories.Count < 1)
            {
                Err += "Problem with Directory settings node. ";
                error = true;
            }
            if (xUseMedian.Count() != 1)
            {
                Err += "Problem with UseMedianFilter settings node. ";
                error = true;
            }
            if (xCounter.Count() != 1)
            {
                Err += "Problem with program counter in settings node";
                error = true;
            }
            if (xImage.Count() != 1)
            {
                Err += "Problem with image counter in settings node";
                error = true;
            }
            if (error)
            {
                _settingsLoaded = false;
                return false;
            }
            string s1;
            string s2;
            int v1;
            bool b1;
            Device = xDevice[0].Attribute("CatNumber").Value;
            if (Device == null)
            {
                Err = "CatNumber not found in Device Node. ";
                error = true;
            }

            if (Int32.TryParse(xDevice[0].Attribute("NumbArrays").Value, out v1))
            {
                if (v1 >= 1)
                    NumbArrays = v1;
                else
                {
                    Err += "Problem with number of number of arrays in Device node. ";
                    error = true;
                }
            }
            else
            {
                Err += "Problem with NumbArryas attribute in Device node. ";
                error = true;
            }
            if (Int32.TryParse(xDevice[0].Attribute("NumbSections").Value, out v1))
            {
                if (v1 >= 1)
                    NumbSections = v1;
                else
                {
                    Err += "Problem with number of number of sections in Device node. ";
                    error = true;
                }
            }
            else
            {
                Err += "Problem with NumbSections attribute in Device node. ";
                error = true;
            }
            if (Int32.TryParse(xCounter[0].Attribute("Value").Value, out v1))
            {
                if (v1 >= 0)
                    ProgramCounter = v1;
                else
                {
                    Err += "ProgramCounter attribute in Device Node must be >= 0. ";
                    error = true;
                }
            }
            else
            {
                Err += "Problem with ProgramCounter attribute in Device node. ";
                error = true;
            }
            if (Int32.TryParse(xImage[0].Attribute("Value").Value, out v1))
            {
                if (v1 >= 0)
                    ImageCounter = v1;
                else
                {
                    Err += "ImageCounter attribute in Device Node must be >= 0. ";
                    error = true;
                }
            }
            else
            {
                Err += "Problem with ImageCounter attribute in Device node. ";
                error = true;
            }
            s1 = xDevice[0].Attribute("RotateImage").Value;
            if (s1 == null)
            {
                Err += "RotateImage attribute not found in Device node. ";
                error = true;
            }
            else if (bool.TryParse(s1, out b1))
            {
                RotateImage = b1;
            }
            else
            {
                Err += "Problem with RotateImage attribute in Device node. ";
                error = true;
            }
            if (bool.TryParse(xUseMedian[0].Attribute("Value").Value, out b1))
            {
                UseMedianFilter = b1;
            }
            else
            {
                Err += "Problems with UseMedianFilter value in Settings node. ";
                error = true;
            }
            s1 = "";
            s2 = "";
            for (int n = 0; n < xDirectories.Count(); n++)
            {
                if (xDirectories[n].Attribute("Type").Value.IndexOf("Working") >= 0)
                {
                    if (s1.Length < 1)
                        s1 = xDirectories[n].Attribute("Name").Value;
                }
                else if (xDirectories[n].Attribute("Type").Value.IndexOf("Template") >= 0)
                {
                    if (s2.Length < 1)
                        s2 = xDirectories[n].Attribute("Name").Value;
                }
                if (s1.Length > 0 && s2.Length > 0)
                {
                    WorkingDirectory = s1;
                    TemplateDirectory = s2;
                    break;
                }
            }
            if (s1.Length < 1 || s2.Length < 1)
            {
                Err += "Problems with one of the directory values in Settings node. ";
                error = true;
            }
            if (error)
                _settingsLoaded = false;
            else
                _settingsLoaded = true;
            return _settingsLoaded;
        }
        public List<string> GetSettings()
        {
            List<string> set = new List<string>();
            if (_XDocLoaded && _settingsLoaded)
            {
                set.Add(String.Format("Device: {0}", Device));
                set.Add(String.Format("RotateImage: {0}", RotateImage));
                set.Add(String.Format("NumbArrays: {0}", NumbArrays));
                set.Add(String.Format("WorkingDirectory: {0}", WorkingDirectory));
                set.Add(String.Format("TemplateDirectory: {0}", TemplateDirectory));
                set.Add(String.Format("ProgramCounter: {0}", ProgramCounter));
                set.Add(String.Format("UseMedianFilter: {0}", UseMedianFilter.ToString()));
                return set;
            }
            else
                return set;
        }
        
        public bool GetOutputBaseName(string ImageName, string ImageNumber, string AType, out string OutBaseName, out string Err)
        {
            Err = "";
            OutBaseName = "";
            char[] zero = new char[] { '0' };
            string trimmedNumber = ImageNumber.TrimStart(zero);
            IEnumerable<XElement> sz = from el in ImageDoc.Descendants("Image")
                                       where el.Attribute("Name").Value.IndexOf(ImageName) >= 0
                                       && el.Attribute("ImageNumber").Value.IndexOf(trimmedNumber) >= 0
                                       && el.Attribute("Ignored") != null
                                       && bool.TryParse(el.Attribute("Ignored").Value, out bool ig)
                                       && !ig
                                       select el;
            if (sz.Count() < 1)
            {
                Err = "Image not found";
                return false;
            }
            else if (sz.Count() > 1)
            {
                Err = "Duplicate Images found";
                return false;
            }

            IEnumerable<XElement> st = from els in sz.Descendants("AnalysisSetup").Descendants("Analysis")
                                       where els.Attribute("Type").Value.IndexOf(AType) >= 0
                                       select els;
            if (st.Count() < 1)
            {
                Err = String.Format("No Analysis setups found for type {0} for image {1}/{2}", AType, ImageName, ImageNumber);
                return false;
            }
            List<XElement> stX = st.ToList();
            int nTarget = -1;
            for (int n = 0; n < stX.Count; n++)
            {
                //if (stX[n].Attribute("Selected").Value.IndexOf(pc.ToString()) >= 0)
                //{
                //    nTarget = n;
                //    break;
                //}
                if (bool.TryParse(stX[n].Attribute("Selected").Value, out bool selected))
                {
                    nTarget = n;
                    break;
                }
            }
            if (nTarget < 0)
            {
                Err = String.Format("No analysis setup selected");
                return false;
            }

            OutBaseName = stX[nTarget].Attribute("BaseName").Value;
            return true;
        }
        public bool GetImageDoc(out string ImageDocument)
        {
            if (_XDocLoaded)
            {
                ImageDocument = ImageDoc.ToString();
                return true;
            }
            else
            {
                ImageDocument = "";
                return false;
            }
        }
        
        XElement NewSortByDyeNode(string Dye)
        {
            IEnumerable<XElement> rotorIEnum = ImageDoc.Descendants("Settings").Descendants("Rotor");
            if (rotorIEnum != null && rotorIEnum.Count() == 1)
            {
                if (rotorIEnum.Attributes("RCSid_Size") != null && rotorIEnum.Attributes("RCSid_Size").Count() == 1
                    && Int32.TryParse(rotorIEnum.Attributes("RCSid_Size").First().Value, out int rcsidSize))
                {
                    XElement node = new XElement("Dye",
                        new XAttribute("DyeName", Dye));
                    for (int n = 0; n < rcsidSize; n++)
                    {
                        node.Add(new XElement("Index", new XAttribute("Value", n.ToString())));
                    }
                    return node;
                }
                return null;
            }
            else
                return null;
        }
        
        public bool GetAnalysis(int ProgramNumber, out string AnalysisString, out string Err)
        {
            Err = "";
            AnalysisString = "";
            if (_XDocLoaded)
            {
                IEnumerable<XElement> imageSetupIEnum = from el in ImageDoc.Descendants("Images").Descendants("Image")
                                                     .Descendants("AnalysisSetup").Descendants("Analysis")
                                                        where el.Attribute("ProgramCounter") != null
                                                        select el;
                if (imageSetupIEnum.Count() > 0)
                {
                    for (int n = 0; n < imageSetupIEnum.Count(); n++)
                    {
                        int pc;
                        if (Int32.TryParse(imageSetupIEnum.ToList()[n].Attribute("ProgramCounter").Value, out pc))
                        {
                            if (pc == ProgramNumber)
                            {
                                AnalysisString = imageSetupIEnum.ToList()[n].ToString();
                                return true;
                            }
                        }
                    }
                }
                IEnumerable<XElement> quantSetupIEnum = from el in ImageDoc.Descendants("Quantifications").Descendants("Quantify")
                                                        where el.Attribute("ProgramCounter") != null
                                                        select el;
                if (quantSetupIEnum.Count() > 0)
                {
                    for (int n = 0; n < quantSetupIEnum.Count(); n++)
                    {
                        int pc;
                        if (Int32.TryParse(quantSetupIEnum.ToList()[n].Attribute("ProgramCounter").Value, out pc))
                        {
                            if (pc == ProgramNumber)
                            {
                                AnalysisString = quantSetupIEnum.ToList()[n].ToString();
                                return true;
                            }
                        }
                    }
                }

                IEnumerable<XElement> combineSetupIEnum = from el in ImageDoc.Descendants("CombineSections").Descendants("Combine")
                                                          where el.Attribute("ProgramCounter") != null
                                                          select el;
                if (combineSetupIEnum.Count() > 0)
                {
                    for (int n = 0; n < combineSetupIEnum.Count(); n++)
                    {
                        int pc;
                        if (Int32.TryParse(combineSetupIEnum.ToList()[n].Attribute("ProgramCounter").Value, out pc))
                        {
                            if (pc == ProgramNumber)
                            {
                                AnalysisString = combineSetupIEnum.ToList()[n].ToString();
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }  
    };
}
