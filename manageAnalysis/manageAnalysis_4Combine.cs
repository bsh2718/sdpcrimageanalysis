﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.ExceptionServices;
using System.Data;

namespace manageAnalysis
{
    using IThree;
    using System.Collections.Specialized;
    using System.Runtime.Remoting.Messaging;
    using System.Security.Cryptography;

    public partial class ImageData
    {
        public int AddCombineSteps(string QuantDye, int[] AfterSetNumbersArray, out string Err)
        {
            char[] digits = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            char[] letterP = new char[] { 'P' };
            char[] vBar = new char[] { '|' };
            Err = "";
            for (int m = 0; m < AfterSetNumbersArray.Length; m++)
            {
                if (AfterSetNumbersArray[m] < 0)
                {
                    Err = "All After Set Numbs must be >= 0";
                    return 0;
                }
            }
            IEnumerable<XElement> combineNodeIEnum = ImageDoc.Descendants("CombineSections");
            if (combineNodeIEnum.Count() != 1)
            {
                Err = "Problem with CombineSections node in xml document. Doesn't exist, or multiples.";
            }
            XElement combineNode = combineNodeIEnum.FirstOrDefault();
            List<int> afterSetNumbers = AfterSetNumbersArray.ToList();
            int newAnalysis = 0;
            afterSetNumbers.Sort();
            int lastIdx = afterSetNumbers.Count - 1;
            int largestSetNumber = afterSetNumbers[lastIdx];
            IdxThree SetRCid = new IdxThree(largestSetNumber + 1, true, 4, false, 2, true);
            List<List<string>> SetRotorChip = new List<List<string>>();
            for (int n = 0; n < SetRCid.Size; n++)
                SetRotorChip.Add(new List<string>());
            //Console.WriteLine(String.Format("{0}/{1}/{2}:  {3}", largestSetNumber + 1, SetRCid.Size, SetRCid.Base, SetRotorChip.Count));
            //Console.Out.Flush();
            for (int n = 0; n < SetRotorChip.Count; n++)
                SetRotorChip[n].Clear();
            List<XElement> quantifyNodes = ImageDoc.Descendants("Quantifications").Descendants("Quantify").ToList();
            string csvName = "";
            for (int n = 0; n < quantifyNodes.Count; n++)
            {
                string sPerformed = quantifyNodes[n].Attribute("Performed").Value;
                string sSuccess = quantifyNodes[n].Attribute("Success").Value;
                string baseNameStub = quantifyNodes[n].Attribute("BaseName").Value; //
                string sRotor = quantifyNodes[n].Attribute("RotorPosition").Value;
                string sChip = quantifyNodes[n].Attribute("ChipPosition").Value;
                string sSubImage = quantifyNodes[n].Attribute("SubImage").Value;
                List<XElement> afterBaseNameNodes = quantifyNodes[n].Descendants("AfterBaseNames").Descendants("AfterBaseName").ToList();
                int r;
                int c;
                int s;
                bool bPerformed;
                bool bSuccess;
                if (afterBaseNameNodes.Count > 0 && Int32.TryParse(sRotor, out r) 
                    && (sChip.IndexOf("Left", StringComparison.CurrentCultureIgnoreCase) >= 0  
                        || sChip.IndexOf("Right", StringComparison.CurrentCultureIgnoreCase) >= 0)
                    && Int32.TryParse(sSubImage, out s)
                    && r > 0 && r < 5 && s > 0 && s <= NumbSections && baseNameStub != null && baseNameStub.Length > 5
                    && bool.TryParse(sPerformed, out bPerformed) && bool.TryParse(sSuccess, out bSuccess) && bPerformed && bSuccess)
                {
                    for (int k = 0; k < afterBaseNameNodes.Count; k++)
                    {
                        int iSet;
                        string sset = afterBaseNameNodes[k].Attribute("Set").Value;
                        if (sset != null && Int32.TryParse(sset, out iSet))
                        {
                            if (sChip.IndexOf("Left", StringComparison.CurrentCultureIgnoreCase) >= 0)
                                c = 0;
                            else
                                c = 1;
                            csvName = String.Format("{0}|{1}|{2}", baseNameStub, iSet, s);
                            int idx = SetRCid.IndexOut(iSet, r, c);
                            //Console.WriteLine(String.Format("idx: {0}/{1}/{2}/{3}", idx, iSet, r, c));
                            SetRotorChip[idx].Add(csvName);
                        }
                        //BaseName << "_" << ImageNumber << "_210_" << Array << "_" << Section << "_Bef_Aft.csv
                        //sdpcrMultiQuant(std::string& Err, std::string WorkingDirectory, std::string BaseName, std::vector< std::string > AfterNames);
                    }
                }
            }
            for (int n = 0; n < SetRotorChip.Count; n++)
            {
                if (SetRotorChip[n].Count > 0)
                {
                    List<string> csvNameBegin = new List<string>(SetRotorChip[n].Count);
                    List<string> csvNameEnd = new List<string>(SetRotorChip[n].Count);
                    string combineBase = "";
                    for (int k = 0; k < SetRotorChip[n].Count; k++)
                    {
                        string[] csvStubs = SetRotorChip[n][k].Split(vBar);

                        if (csvStubs.Length > 2)
                        {
                            if (k == 0)
                            {
                                int itmp = csvStubs[0].LastIndexOf('Q');
                                string stmp = csvStubs[0];
                                
                                char[] stmpArray = stmp.ToCharArray();
                                stmpArray[itmp] = 'C';
                                stmp = new string(stmpArray);
                                itmp = stmp.LastIndexOf('P') - 2;
                                string rtmp = stmp.Substring(0, itmp);
                                combineBase = String.Format("{0}_{1}_", rtmp, csvStubs[1]);
                                //combineBase = String.Format("{0}{1}_",
                                    //csvStubs[0].TrimEnd(digits).TrimEnd(letterP), csvStubs[1]);
                            }
                            csvNameBegin.Add(String.Format("{0}_{1}_210_", csvStubs[0], csvStubs[1]));
                            csvNameEnd.Add(String.Format("_{0}_Bef_Aft.csv", csvStubs[2]));
                        }
                    }

                    for (int a = 1; a <= NumbArrays; a++)
                    {
                        //int ia = a - 1;
                        int newProgramCounter = ProgramCounter + 1;
                        string baseName = combineBase + a.ToString() + "_P" + newProgramCounter.ToString("D4");
                        XElement combine = new XElement("Combine",
                            new XAttribute("WorkingDirectory", WorkingDirectory),
                            new XAttribute("BaseName", baseName),
                            new XAttribute("ProgramCounter", newProgramCounter.ToString("D4")),
                            new XAttribute("Performed", "false"),
                            new XAttribute("Success", "false")
                            );
                        XElement afterNames = CollectionNode("AfterNames", "AfterName");
                        for (int m = 0; m < csvNameBegin.Count; m++)
                        {
                            string tmp = csvNameBegin[m] + a.ToString() + csvNameEnd[m];
                            XElement aName = new XElement("AfterName", new XAttribute("Name", tmp));
                            afterNames.Add(aName);
                        }
                        combine.Add(afterNames);
                        combineNode.Add(combine);
                        ProgramCounter = newProgramCounter;
                        SetProgramCounter(newProgramCounter);
                        newAnalysis++;
                    }
                }
            }
            return newAnalysis;
        }
        public int GetCombineAnalysisArray(bool All, bool RStatus, bool SStatus, out string[] CombineStrings, out string Err)
        {
            int res = GetCombineAnalysisStrings(All, RStatus, SStatus, out List<string> combineStringsList, out Err);
            CombineStrings = combineStringsList.ToArray();
            return res;
        }
        public int GetCombineAnalysisStrings(bool All, bool RStatus, bool SStatus, out List<string> CombineStrings, out string Err)
        {
            Err = "";
            CombineStrings = new List<string>();
            StringBuilder cmd = new StringBuilder();
            List<XElement> combineList = ImageDoc.Descendants("CombineSections").Descendants("Combine").ToList();
            for (int n = 0; n < combineList.Count; n++)
            {
                string sPerformed = combineList[n].Attribute("Performed").Value;
                string sSuccess = combineList[n].Attribute("Success").Value;
                string sProgramC = combineList[n].Attribute("ProgramCounter").Value;
                bool bPerformed;
                bool bSuccess;

                if (bool.TryParse(sPerformed, out bPerformed) && bool.TryParse(sSuccess, out bSuccess))
                {
                    //bool outp = true;
                    if (RStatus == bPerformed)
                        bPerformed = true;
                    else
                        bPerformed = false;
                    if (SStatus == bSuccess)
                        bSuccess = true;
                    else
                        bSuccess = false;
                    if ( (bPerformed && bSuccess ) || All )
                    {
                        cmd.Clear();
                        string pWDirectory = combineList[n].Attribute("WorkingDirectory").Value;
                        string pBaseName = combineList[n].Attribute("BaseName").Value;

                        if (pWDirectory != null && pWDirectory.Length > 3
                            && pBaseName != null && pBaseName.Length > 3)
                        {
                            cmd.Append(String.Format("sdpcrQuantCombine.exe {0} {1}", pWDirectory, pBaseName));
                            List<XElement> afterNamesList = combineList[n].Descendants("AfterNames").Descendants("AfterName").ToList();
                            if (afterNamesList.Count > 0)
                            {
                                for (int m = 0; m < afterNamesList.Count; m++)
                                {
                                    string aName = afterNamesList[m].Attribute("Name").Value;
                                    cmd.Append(String.Format(" {0}", aName));
                                }
                                cmd.Append(String.Format(" | {0}", sProgramC));
                                CombineStrings.Add(cmd.ToString());
                            }

                        }
                    }
                }
            }
            return CombineStrings.Count;
        }
        //sdpcrMultiQuant(std::string& Err, std::string WorkingDirectory, std::string BaseName, std::vector< std::string > AfterNames);
        public bool UpdateCombineRunStatus(int ProgramNumber, bool NewPerformed, bool NewSuccess)
        {
            if (_XDocLoaded)
            {
                IEnumerable<XElement> combineIEnum = from el in ImageDoc.Descendants("CombineSections").Descendants("Combine")
                                                     where el.Attribute("ProgramCounter") != null
                                                     select el;
                if (combineIEnum.Count() > 0)
                {
                    for (int n = 0; n < combineIEnum.Count(); n++)
                    {
                        int pc;
                        if (Int32.TryParse(combineIEnum.ToList()[n].Attribute("ProgramCounter").Value, out pc))
                        {
                            if (pc == ProgramNumber)
                            {
                                if (combineIEnum.ToList()[n].Attribute("Performed") != null && combineIEnum.ToList()[n].Attribute("Success") != null)
                                {
                                    combineIEnum.ToList()[n].Attribute("Performed").Value = NewPerformed.ToString();
                                    combineIEnum.ToList()[n].Attribute("Success").Value = NewSuccess.ToString();
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }
        public int GetCombineAnalysisSetups(bool RStatus, bool SStatus, out List<string> CombineStrings, 
            out List<XElement> CombineSetupInfo, out string Err)
        {
            Err = "";
            CombineStrings = new List<string>();
            CombineSetupInfo = new List<XElement>();
            StringBuilder cmd = new StringBuilder();
            List<XElement> combineList = ImageDoc.Descendants("CombineSections").Descendants("Combine").ToList();
            for (int n = 0; n < combineList.Count; n++)
            {
                string sPerformed = combineList[n].Attribute("Performed").Value;
                string sSuccess = combineList[n].Attribute("Success").Value;
                bool bPerformed;
                bool bSuccess;

                if (bool.TryParse(sPerformed, out bPerformed) && bool.TryParse(sSuccess, out bSuccess))
                {
                    
                    if (RStatus == bPerformed)
                        bPerformed = true;
                    else
                        bPerformed = false;
                    if (SStatus == bSuccess)
                        bSuccess = true;
                    else
                        bSuccess = false;
                    if (bPerformed && bSuccess)
                    {
                        cmd.Clear();
                        string pWDirectory = combineList[n].Attribute("WorkingDirectory").Value;
                        string pBaseName = combineList[n].Attribute("BaseName").Value;
                        string pProgramCounter = combineList[n].Attribute("ProgramCounter").Value;
                        if (pWDirectory != null && pWDirectory.Length > 3
                            && pBaseName != null && pBaseName.Length > 3
                            && pProgramCounter != null)
                        {
                            cmd.Append(String.Format("sdpcrQuantCombine.exe {0} {1}", pWDirectory, pBaseName));
                            List<XElement> afterNamesList = combineList[n].Descendants("AfterNames").Descendants("AfterName").ToList();
                            if (afterNamesList.Count > 0)
                            {
                                for (int m = 0; m < afterNamesList.Count; m++)
                                {
                                    string aName = afterNamesList[m].Attribute("Name").Value;
                                    cmd.Append(String.Format(" {0}", aName));
                                }
                                int count = CombineStrings.Count;
                                CombineStrings.Add(cmd.ToString());
                                XElement newSetup = new XElement("SetupInfo",
                                    new XAttribute("ScriptIndex", count.ToString()),
                                    new XAttribute("ProgramCounter", pProgramCounter),
                                    new XAttribute("Success", sSuccess),
                                    new XAttribute("Performed", sPerformed)
                                    );
                                CombineSetupInfo.Add(newSetup);
                            }
                        }
                    }
                }
            }
            return CombineStrings.Count;
        }
        public bool AlternativeCombineSetup(int ProgramNumber, List<int> ExcludedSections)
        {
            char[] digits = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            char[] under = new char[] { '_' };
            if (_XDocLoaded && ExcludedSections.Count > 0)
            {
                bool cont = false;
                for (int n = 0; n < ExcludedSections.Count; n++)
                {
                    if (ExcludedSections[n] > 0 && ExcludedSections[n] <= NumbSections)
                    {
                        cont = true;
                        break;
                    }
                }
                if (cont)
                {
                    IEnumerable<XElement> combineSetupIEnum = from el in ImageDoc.Descendants("CombineSections").Descendants("Combine")
                                                              where el.Attribute("ProgramCounter") != null
                                                              select el;
                    if (combineSetupIEnum.Count() > 0)
                    {
                        for (int n = 0; n < combineSetupIEnum.Count(); n++)
                        {
                            int pc;
                            if (Int32.TryParse(combineSetupIEnum.ToList()[n].Attribute("ProgramCounter").Value, out pc))
                            {
                                if (pc == ProgramNumber)
                                {
                                    int newProgramCounter = ProgramCounter + 1;
                                    List<XElement> oldAfterNamesList 
                                        = combineSetupIEnum.ToList()[n].Descendants("AfterNames").Descendants("AfterName").ToList();
                                    List<XElement> newAfterNamesList = new List<XElement>();
                                    for (int m = 0; m < oldAfterNamesList.Count; m++)
                                    {
                                        bool addName = true;
                                        string pCSVName = oldAfterNamesList[m].Attribute("Name").Value;
                                        int pos = pCSVName.IndexOf("_Bef_Aft.csv");
                                        if (pos > 3)
                                        {
                                            string sect = pCSVName.Substring(pos - 2, 2).Trim(under);
                                            int iSect;
                                            if (Int32.TryParse(sect, out iSect))
                                            {
                                                for (int k = 0; k < ExcludedSections.Count; k++)
                                                {
                                                    if (iSect == ExcludedSections[k])
                                                    {
                                                        addName = false;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        if (addName)
                                        {
                                            newAfterNamesList.Add(oldAfterNamesList[m]);
                                        }
                                    }
                                    if ( newAfterNamesList.Count > 0 && combineSetupIEnum.ToList()[n].Attribute("BaseName") != null)
                                    {
                                        string newBaseName = (combineSetupIEnum.ToList()[n].Attribute("BaseName").Value).TrimEnd(digits);
                                        XElement newAnalysis = new XElement("Combined",
                                            new XAttribute("Performed", "false"),
                                            new XAttribute("Success", "false"),
                                            new XAttribute("ProgramCounter", newProgramCounter.ToString("D4")),
                                            new XAttribute("BaseName", newBaseName),
                                            new XAttribute("WorkingDirectory", WorkingDirectory)
                                            );
                                        XElement newAfterNames = new XElement("AfterNames", new XAttribute("CollectionType", "AfterName"));
                                        for ( int m = 0; m < newAfterNamesList.Count; m++)
                                            newAfterNames.Add(newAfterNamesList[m]);

                                        newAnalysis.Add(newAfterNames);
                                        combineSetupIEnum.ToList()[n].Parent.Add(newAnalysis);
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }
    };
}
