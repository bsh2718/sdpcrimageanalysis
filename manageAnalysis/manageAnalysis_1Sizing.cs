﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.ExceptionServices;
using System.Data;

namespace manageAnalysis
{
    using IThree;
    using System.Collections.Specialized;
    using System.Runtime.Remoting.Messaging;
    using System.Security.Cryptography;

    public partial class ImageData
    { 
        public int AddSizingAnalysis(string ImageType, string SizingDye, int SetNumber, out string Err)
        {
            int newAnalysis = 0;
            Err = "";
            string type;
            bool isBefore = false;
            string fil_suffix;
            if (ImageType.IndexOf("Before") >= 0)
            {
                type = "Before";
                fil_suffix = "_B";
                isBefore = true;
            }
            else if (ImageType.IndexOf("After") >= 0)
            {
                type = "After";
                fil_suffix = "_A";
                isBefore = false;
            }
            else
            {
                Err = "ImageType must be either Before or After";
                return 0;
            }

            if ( SizingDye.Length < 2 )
            {
                Err = "No Sizing Dye";
                return 0;
            }
            if ( SetNumber < 0 )
            {
                Err = "SetNumber must be >= 0";
                return 0;
            }
            List<XElement> imageList = GetImages(type);
            for (int n = 0; n < imageList.Count(); n++)
            {
                int sn;
                if (Int32.TryParse(imageList[n].Attribute("SetNumber").Value, out sn))
                {
                    //Console.WriteLine(String.Format("SetNumber/sn: {0}/{1}, type = {2}", SetNumber, sn, type));
                    if (sn == SetNumber)
                    {
                        //if (FindTypeInfo(imageList[n], "Sizing", out int pCounter, out bool performed, out bool success))
                        //{
                        //Console.WriteLine(String.Format("image {0}, Dye:{1}, requested dye: {2}", n, imageList[n].Attribute("Dye").Value, SizingDye));
                        if (imageList[n].Attribute("Dye").Value.IndexOf(SizingDye) >= 0)
                        {
                            int r;
                            int c;
                            int s;
                            int id;
                            //string cs = "";
                            string imName = imageList[n].Attribute("Name").Value;
                            if (Int32.TryParse(imageList[n].Attribute("RotorPosition").Value, out r)
                                && imageList[n].Attribute("ChipPosition") != null
                                && Int32.TryParse(imageList[n].Attribute("Section").Value, out s)
                                && Int32.TryParse(imageList[n].Attribute("JNumber").Value, out id)
                                )
                            {
                                if (imageList[n].Attribute("ChipPosition").Value.IndexOf("Left") >= 0)
                                    c = 0;
                                else
                                    c = 1;
                                int newProgramCounter = ProgramCounter + 1;
                                string fil = SizingDye + fil_suffix;
                                var xele = new XElement("Analysis",
                                    new XAttribute("ProgramCounter", String.Format("{0:D4}", newProgramCounter)),
                                    new XAttribute("Type", "Sizing"),
                                    new XAttribute("WorkingDirectory", WorkingDirectory),
                                    new XAttribute("TemplateDirectory", TemplateDirectory),
                                    new XAttribute("ImageFileName", imageList[n].Attribute("Name").Value),
                                    new XAttribute("TemplatePrefix", Device),
                                    new XAttribute("FilterLabel", fil),
                                    new XAttribute("SubImage", imageList[n].Attribute("Section").Value),
                                    new XAttribute("BaseName", String.Format("{0}_J{1:D3}_{2}_P{3:D4}",
                                        BaseNameStub, id, RotorChipSubLabel(r, c, s, isBefore), newProgramCounter)),
                                    new XAttribute("TransposeImage", RotateImage.ToString()),
                                    new XAttribute("UseMedianFilter", UseMedianFilter.ToString()),
                                    new XAttribute("ExtraOutput", "0"),
                                    new XAttribute("Performed", "false"),
                                    new XAttribute("Success", "false"),
                                    new XAttribute("Selected", "false")
                                    );

                                var docA = ImageDoc.Descendants("Images").Descendants("Image");
                                //Console.WriteLine(String.Format("docA.Count: {0}", docA.Count()));
                                List<XElement> xdocA = docA.ToList();
                                XElement resultsX = null;
                                for (int m = 0; m < xdocA.Count; m++)
                                {
                                    if (xdocA[m].Attribute("Type").Value.IndexOf(type) >= 0 &&
                                        xdocA[m].Attribute("Name").Value.IndexOf(imName) >= 0)
                                    {
                                        List<XElement> docB = xdocA[m].Descendants("AnalysisSetup").ToList();
                                        //Console.WriteLine(String.Format("docB.Count: {0}", docB.Count));
                                        if (docB.Count() > 0)
                                        {
                                            bool duplicate = false;
                                            for (int k = 0; k < docB.Count; k++)
                                            {
                                                List<XElement> rEl = docB[k].Elements("Analysis").ToList();
                                                if (rEl.Count() > 0)
                                                {
                                                    for (int j = 0; j < rEl.Count; j++)
                                                    {
                                                        if ((xele.Attribute("Type").Value == rEl[j].Attribute("Type").Value)
                                                            && (xele.Attribute("WorkingDirectory").Value == rEl[j].Attribute("WorkingDirectory").Value)
                                                            && (xele.Attribute("TemplateDirectory").Value == rEl[j].Attribute("TemplateDirectory").Value)
                                                            && (xele.Attribute("ImageFileName").Value == rEl[j].Attribute("ImageFileName").Value)
                                                            && (xele.Attribute("TemplatePrefix").Value == rEl[j].Attribute("TemplatePrefix").Value)
                                                            && (xele.Attribute("FilterLabel").Value == rEl[j].Attribute("FilterLabel").Value)
                                                            && (xele.Attribute("SubImage").Value == rEl[j].Attribute("SubImage").Value)
                                                            && (xele.Attribute("TransposeImage").Value == rEl[j].Attribute("TransposeImage").Value)
                                                            && (xele.Attribute("UseMedianFilter").Value == rEl[j].Attribute("UseMedianFilter").Value)
                                                            && (xele.Attribute("ExtraOutput").Value == rEl[j].Attribute("ExtraOutput").Value))
                                                        {
                                                            duplicate = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                                if (duplicate)
                                                    break;
                                            }
                                            if (!duplicate)
                                                resultsX = docB.FirstOrDefault();
                                            break;
                                        }
                                    }
                                }

                                if (resultsX != null)
                                {
                                    resultsX.Add(xele);
                                    ProgramCounter = newProgramCounter;
                                    SetProgramCounter(newProgramCounter);
                                    newAnalysis++;
                                }
                            }
                            else
                            {
                                Err += String.Format("Problem with image attributes for image {0}.  ", n);
                                Console.WriteLine(String.Format("Problem with image attributes for image {0}.  ", n));
                                Console.WriteLine(String.Format("     Name: {0}", imageList[n].Attribute("Name").Value));
                                Console.WriteLine(String.Format("     Rotor: {0}", imageList[n].Attribute("RotorPosition").Value));
                                Console.WriteLine(String.Format("     Chip: {0}", imageList[n].Attribute("ChipPosition").Value));
                                Console.WriteLine(String.Format("     Section: {0}", imageList[n].Attribute("Section").Value));
                                Console.WriteLine(String.Format("     ImageNumber: {0}", imageList[n].Attribute("ImageNumber").Value));
                            }
                        }
                        //}
                        //else
                        //{
                        //    Err += String.Format("Could not get Type info from image {0}.  ", n);
                        //    Console.WriteLine(String.Format("Could not get Type info from image {0}.  ", n));
                        //}
                    }
                }
                else
                {
                    Err += String.Format("Could not read SetNumber from image {0}.  ", n);
                    Console.WriteLine(String.Format("Could not read SetNumber from image {0}.  ", n));
                }
            }
            return newAnalysis;
        }
        
        public bool UpdateAnalysisResult(string ImageName, string ImageNumber, string AType, string ProgramCounter,
            bool NewPerformed, bool NewSuccess, out string Err)
        {
            Err = "";
            //Console.WriteLine(String.Format("UpdateAnalysisResult: {0}/{1}/{2}/{3}", ImageName, ImageNumber, AType, ProgramCounter));
            char[] zero = new char[] { '0' };
            string trimmedNumber = ImageNumber.TrimStart(zero);
            IEnumerable<XElement> imageIEnum = from el in ImageDoc.Descendants("Image")
                                               where el.Attribute("Name").Value.IndexOf(ImageName) >= 0
                                               && el.Attribute("ImageNumber").Value.IndexOf(trimmedNumber) >= 0
                                               && el.Attribute("Ignored") != null
                                                && bool.TryParse(el.Attribute("Ignored").Value, out bool ig)
                                                && !ig
                                               select el;
            if (imageIEnum.Count() < 1)
            {
                Err = "Image not found";
                return false;
            }
            else if (imageIEnum.Count() > 1)
            {
                Err = "Duplicate Images found";
                return false;
            }

            List<XElement> imageList = imageIEnum.ToList();

            string trimmedCounter = ProgramCounter.TrimStart(zero);
            IEnumerable<XElement> analysisSetupIEnum = from els in imageList[0].Descendants("AnalysisSetup").Descendants("Analysis")
                                                       where els.Attribute("Type").Value.IndexOf(AType) >= 0
                                                       && els.Attribute("ProgramCounter").Value.IndexOf(trimmedCounter) >= 0
                                                       && els.Attribute("ImageFileName").Value.IndexOf(ImageName) >= 0
                                                       select els;
            if (analysisSetupIEnum.Count() < 1)
            {
                Err = "Analysis setup not found";
                return false;
            }
            else if (analysisSetupIEnum.Count() > 1)
            {
                Err = "Duplicate setups found";
                return false;
            }
            XElement analysisSetup0 = analysisSetupIEnum.FirstOrDefault();
            analysisSetup0.Attribute("Performed").Value = NewPerformed.ToString();
            analysisSetup0.Attribute("Success").Value = NewSuccess.ToString();
            return true;
        }
        public bool UpdateAnalysisSelection(string ImageName, string ImageNumber, string AType, string ProgramCounter,
            bool NewSelection, out string Err)
        {
            Err = "";
            //Console.WriteLine(String.Format("UpdateAnalysisResult: {0}/{1}/{2}/{3}", ImageName, ImageNumber, AType, ProgramCounter));
            char[] zero = new char[] { '0' };
            string trimmedNumber = ImageNumber.TrimStart(zero);
            IEnumerable<XElement> imageIEnum = from el in ImageDoc.Descendants("Image")
                                               where el.Attribute("Name").Value.IndexOf(ImageName) >= 0
                                               && el.Attribute("ImageNumber").Value.IndexOf(trimmedNumber) >= 0
                                               && el.Attribute("Ignored") != null
                                               && bool.TryParse(el.Attribute("Ignored").Value, out bool ig)
                                               && !ig
                                               select el;
            if (imageIEnum.Count() < 1)
            {
                Err = "Image not found";
                return false;
            }
            else if (imageIEnum.Count() > 1)
            {
                Err = "Duplicate Images found";
                return false;
            }

            List<XElement> imageList = imageIEnum.ToList();

            string trimmedCounter = ProgramCounter.TrimStart(zero);
            IEnumerable<XElement> analysisSetupIEnum = from els in imageList[0].Descendants("AnalysisSetup").Descendants("Analysis")
                                                       where els.Attribute("Type").Value.IndexOf(AType) >= 0
                                                       && els.Attribute("ImageFileName").Value.IndexOf(ImageName) >= 0
                                                       select els;
            if (analysisSetupIEnum.Count() < 1)
            {
                Err = "Analysis setup not found";
                return false;
            }
            List<XElement> analysisSetupList = analysisSetupIEnum.ToList();
            string spc = ProgramCounter.ToString();
            if (NewSelection)
            {
                for (int n = 0; n < analysisSetupList.Count; n++)
                {
                    if (analysisSetupList[n].Attribute("ProgramCounter").Value.IndexOf(trimmedCounter) >= 0)
                        analysisSetupList[n].Attribute("Selected").Value = "true";
                    else
                        analysisSetupList[n].Attribute("Selected").Value = "false";
                }
            }
            else
            {
                for (int n = 0; n < analysisSetupList.Count; n++)
                {
                    if (analysisSetupList[n].Attribute("ProgramCounter").Value.IndexOf(trimmedCounter) >= 0)
                    {
                        analysisSetupList[n].Attribute("Selected").Value = "false";
                        break;
                    }
                }
            }
            return true;
        }
        
		public int GetAnalysisInfoArray(string AnalysisType, int SetNumb, out string[] AnalysisInfo, out string Err)
        {
            int res = GetAnalysisInfo(AnalysisType, SetNumb, out List<string> analysisInfoList, out Err);
            AnalysisInfo = analysisInfoList.ToArray();
            return res;
        }
        public int GetAnalysisInfo(string AnalysisType, int SetNumb, out List<string> AnalysisInfo, out string Err)
        {
            Err = "";
            AnalysisInfo = new List<string>(0);
            //Tuple<string, string> newTuple;
            if (_XDocLoaded)
            {
                IEnumerable<XElement> analysisSetupIEnum 
                    = ImageDoc.Descendants("Images").Descendants("AnalysisSetup").Descendants("Analysis");
                if ( analysisSetupIEnum != null && analysisSetupIEnum.Count() > 0 )
                {
                    List<XElement> analysisList = analysisSetupIEnum.ToList();
                    for (int m = 0; m < analysisList.Count; m++)
                    {
                        if (analysisList[m].Attribute("Type") != null &&
                            analysisList[m].Attribute("Type").Value.IndexOf(AnalysisType, StringComparison.CurrentCultureIgnoreCase) >= 0)
                        {
                            XElement pp = analysisList[m].Parent.Parent;
                            if (pp.Attribute("ImageNumber") != null && pp.Attribute("SetNumber") != null
                                && pp.Attribute("Name") != null
                                && Int32.TryParse(pp.Attribute("ImageNumber").Value, out int imageNumb)
                                && Int32.TryParse(pp.Attribute("SetNumber").Value, out int setN)
                                && analysisList[m].Attribute("ProgramCounter") != null
                                && Int32.TryParse(analysisList[m].Attribute("ProgramCounter").Value, out int pc)
                                && analysisList[m].Attribute("Performed") != null
                                && bool.TryParse(analysisList[m].Attribute("Performed").Value, out bool bPerformed)
                                && analysisList[m].Attribute("Success") != null
                                && bool.TryParse(analysisList[m].Attribute("Success").Value, out bool bSuccess)
                                && analysisList[m].Attribute("Selected") != null
                                && bool.TryParse(analysisList[m].Attribute("Selected").Value, out bool bSelected)
                                )
                            {
                                if (pp.Attribute("Ignored") == null
                                    || ( bool.TryParse(pp.Attribute("Ignored").Value, out bool ig) && !ig))
                                {
                                    string imageName = pp.Attribute("Name").Value;
                                    AnalysisInfo.Add(String.Format("{0},{1},{2},{3},{4},{5},{6}",
                                        imageName, imageNumb, AnalysisType, pc, bPerformed.ToString(),
                                        bSuccess.ToString(), bSelected.ToString()));
                                }
                            }
                        }
                    }
                }
            }
            return AnalysisInfo.Count;
        }
        public int GetAnalysisSetups(string AnalysisType, int SetNumb, bool RStatus, bool SucStatus,
            out List<Tuple<string, string>> Setups, out string Err)
        {
            Err = "";
            Setups = new List<Tuple<string, string>>();
            //Tuple<string, string> newTuple;
            if (_XDocLoaded)
            {
                List<XElement> analysisSetupList = ImageDoc.Descendants("AnalysisSetup").ToList();
                if (analysisSetupList.Count > 0)
                {
                    for (int n = 0; n < analysisSetupList.Count; n++)
                    {
                        List<XElement> analysisList = analysisSetupList[n].Descendants("Analysis").ToList();
                        if (analysisList.Count > 0)
                        {
                            for (int m = 0; m < analysisList.Count; m++)
                            {
                                if (analysisList[m].Attribute("Type").Value.IndexOf(AnalysisType, StringComparison.CurrentCultureIgnoreCase) >= 0)
                                {
                                    XElement pp = analysisList[m].Parent.Parent;
                                    string imageN = pp.Attribute("ImageNumber").Value;
                                    string setN = pp.Attribute("SetNumber").Value;
                                    string per = analysisList[m].Attribute("Performed").Value;
                                    string suc = analysisList[m].Attribute("Success").Value;
                                    bool bPer;
                                    bool bSuc;
                                    int iSetN;
                                    if (bool.TryParse(per, out bPer) && bool.TryParse(suc, out bSuc)
                                        && Int32.TryParse(imageN, out _) && Int32.TryParse(setN, out iSetN))
                                    {

                                        if (RStatus == bPer)
                                            bPer = true;
                                        else
                                            bPer = false;
                                        if (SucStatus == bSuc)
                                            bSuc = true;
                                        else
                                            bSuc = false;
                                        bool outp = bPer && bSuc;
                                        if (iSetN != SetNumb)
                                            outp = false;
                                        if (imageN != null && outp)
                                        {
                                            Setups.Add(new Tuple<string, string>(analysisList[m].ToString(), imageN));
                                        }
                                    }
                                    else
                                    {
                                        Err += String.Format(
                                            "snumb == null, ana[{0}].parent: {1}\n          ans[{0}].parent.parent: {2}\n==========\n",
                                            m, analysisList[m].Parent.ToString(), analysisList[m].Parent.Parent.ToString());
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return Setups.Count;
        }
        public int GetSizingAnalysisArray(bool Before, int SetNumb, bool All, bool RStatus, bool SStatus,
            out string[] SetupStrings, out string Err)
        {
            int res = GetSizingAnalysisStrings(Before, SetNumb, All, RStatus, SStatus,
                out List<string> setupStrings, out Err);
            SetupStrings = setupStrings.ToArray();
            return res;
        }
        public int GetSizingAnalysisStrings(bool Before, int SetNumb, bool All, bool RStatus, bool SStatus,
            out List<string> SetupStrings, out string Err)
        {
            Err = "";
            SetupStrings = new List<string>();
            //Tuple<string, string> newTuple;
            if (_XDocLoaded)
            {
                string imageType = "";
                if (Before)
                    imageType = "Before";
                else
                    imageType = "After";
                IEnumerable<XElement> imageIEnum = from el in ImageDoc.Descendants("Images").Descendants("Image")
                                                   where el.Attribute("Type") != null
                                                   && el.Attribute("Type").Value.IndexOf(imageType, StringComparison.CurrentCultureIgnoreCase) >= 0
                                                   select el;
                if (imageIEnum.Count() > 0)
                {
                    List<XElement> analysisSetupList = imageIEnum.Descendants("AnalysisSetup").ToList();
                    if (analysisSetupList.Count > 0)
                    {
                        for (int n = 0; n < analysisSetupList.Count; n++)
                        {
                            List<XElement> analysisList = analysisSetupList[n].Descendants("Analysis").ToList();
                            if (analysisList.Count > 0)
                            {
                                for (int m = 0; m < analysisList.Count; m++)
                                {
                                    if (analysisList[m].Attribute("Type") != null
                                        && analysisList[m].Attribute("Type").Value.IndexOf("Sizing", StringComparison.CurrentCultureIgnoreCase) >= 0
                                        && analysisList[m].Attribute("ProgramCounter") != null
                                        && Int32.TryParse(analysisList[m].Attribute("ProgramCounter").Value, out int pc)
                                        && analysisList[m].Parent.Parent.Attribute("ImageNumber") != null
                                        && Int32.TryParse(analysisList[m].Parent.Parent.Attribute("ImageNumber").Value, out int im)
                                        )
                                    {
                                        XElement pp = analysisList[m].Parent.Parent;
                                        string setN = pp.Attribute("SetNumber").Value;
                                        string per = analysisList[m].Attribute("Performed").Value;
                                        string suc = analysisList[m].Attribute("Success").Value;
                                        bool bPer;
                                        bool bSuc;
                                        int iSetN;
                                        if (bool.TryParse(per, out bPer) && bool.TryParse(suc, out bSuc)
                                            && Int32.TryParse(setN, out iSetN))
                                        {
                                            if (RStatus == bPer)
                                                bPer = true;
                                            else
                                                bPer = false;
                                            if (SStatus == bSuc)
                                                bSuc = true;
                                            else
                                                bSuc = false;
                                            bool outp = (bPer && bSuc || All);
                                            if (iSetN != SetNumb)
                                                outp = false;
                                            if (outp)
                                            {
                                                string pExtraOutput = analysisList[m].Attribute("ExtraOutput").Value;
                                                string pUseMedian = analysisList[m].Attribute("UseMedianFilter").Value;
                                                string pTransposeIm = analysisList[m].Attribute("TransposeImage").Value;
                                                string pBaseName = analysisList[m].Attribute("BaseName").Value;
                                                string pSubImage = analysisList[m].Attribute("SubImage").Value;
                                                string pFilterL = analysisList[m].Attribute("FilterLabel").Value;
                                                string pPrefix = analysisList[m].Attribute("TemplatePrefix").Value;
                                                string pImageName = analysisList[m].Attribute("ImageFileName").Value;
                                                string pTDirectory = analysisList[m].Attribute("TemplateDirectory").Value;
                                                string pWDirectory = analysisList[m].Attribute("WorkingDirectory").Value;
                                                int iExtraOutput;
                                                bool bUseMedian;
                                                bool bTransposeIm;
                                                int iSubImage;

                                                if (Int32.TryParse(pExtraOutput, out iExtraOutput)
                                                    && bool.TryParse(pUseMedian, out bUseMedian)
                                                    && bool.TryParse(pTransposeIm, out bTransposeIm)
                                                    && Int32.TryParse(pSubImage, out iSubImage))
                                                {
                                                    SetupStrings.Add(String.Format("sdpcrSizing.exe {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} | {10} | {11}",
                                                        pWDirectory, pTDirectory, pImageName, pPrefix, pFilterL, iSubImage.ToString(),
                                                        pBaseName, Convert.ToInt32(bTransposeIm), Convert.ToInt32(bUseMedian), iExtraOutput,
                                                        pc.ToString("D4"), im.ToString("D3")));
                                                    //String.Format("sdpcrSizing(Err, {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9})   Performed({10}), Success({11})",
                                                    //pWDirectory, pTDirectory, pImageName, pPrefix, pFilterL, pSubImage, pBaseName,
                                                    //pTransposeIm, pUseMedian, pExtraOutput, pPerformed, pSuccess));
                                                }
                                                else
                                                {
                                                    Err += String.Format(
                                                        "snumb == null, ana[{0}].parent: {1}\n          ans[{0}].parent.parent: {2}\n==========\n",
                                                        m, analysisList[m].Parent.ToString(), analysisList[m].Parent.Parent.ToString());
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return SetupStrings.Count;
        }
        
        public bool AlternativeSetup(string ImageName, int ProgramNumber, bool ChangeFilterLabel, bool ChangeExtraOutput,
            bool ChangeUseMedianFilter, string NewFilterLabel, int NewExtraOutput = 0, bool NewUseMedianFilter = true)
        {
            char[] digits = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            if (ChangeFilterLabel && NewFilterLabel.Length < 3)
                ChangeFilterLabel = false;
            if (_XDocLoaded && (ChangeFilterLabel || ChangeExtraOutput || ChangeUseMedianFilter) )
            {
                IEnumerable<XElement> imageSetupIEnum = from el in ImageDoc.Descendants("Images").Descendants("Image")
                                                     .Descendants("AnalysisSetup").Descendants("Analysis")
                                                        where el.Attribute("Name") != null
                                                        && el.Attribute("Name").Value.IndexOf(ImageName) >= 0
                                                        && el.Attribute("ProgramCounter") != null
                                                        select el;
                if (imageSetupIEnum.Count() > 0)
                {
                    for (int n = 0; n < imageSetupIEnum.Count(); n++)
                    {
                        int pc;
                        if (Int32.TryParse(imageSetupIEnum.ToList()[n].Attribute("ProgramCounter").Value, out pc))
                        {
                            if (pc == ProgramNumber)
                            {
                                XElement newAnalysis = XElement.Parse(imageSetupIEnum.ToList()[n].ToString());
                                int newProgramCounter = ProgramCounter + 1;
                                if ( newAnalysis.Attribute("ProgramConter") != null
                                    && newAnalysis.Attribute("FilterLabel") != null
                                    && newAnalysis.Attribute("ExtraOutput") != null
                                    && newAnalysis.Attribute("UseMedianFilter") != null
                                    && newAnalysis.Attribute("BaseName") != null
                                    )
                                {
                                    if (ChangeFilterLabel)
                                        newAnalysis.Attribute("FilterLabel").Value = NewFilterLabel;
                                    if (ChangeExtraOutput)
                                        newAnalysis.Attribute("ExtraOutput").Value = NewExtraOutput.ToString();
                                    if (ChangeUseMedianFilter)
                                        newAnalysis.Attribute("UseMedianFilter").Value = NewUseMedianFilter.ToString();
                                    string pcNumb = newProgramCounter.ToString("D4");
                                    string newBaseName = newAnalysis.Attribute("BaseName").Value.TrimEnd(digits) + pcNumb;
                                    newAnalysis.Attribute("ProgramConter").Value = pcNumb;
                                    newAnalysis.Attribute("BaseName").Value = newBaseName;
                                    newAnalysis.Attribute("Performed").Value = "false";
                                    newAnalysis.Attribute("Success").Value = "false";
                                    ProgramCounter = newProgramCounter;
                                    SetProgramCounter(ProgramCounter);
                                    imageSetupIEnum.ToList()[n].Parent.Add(newAnalysis);
                                    return true;
                                }  
                            }
                        }
                    }
                }
            }
            return false;
        }
    };
}
