﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.ExceptionServices;
using System.Data;

namespace manageAnalysis
{
    using IThree;
    using System.Collections.Specialized;
    using System.Runtime.Remoting.Messaging;
    using System.Security.Cryptography;

    public partial class ImageData
    {
        public int AddMaskingAnalysis(string ImageType, string SizingDye, string MaskingDye, int SetNumb, out string Err,
            int ColorShiftX = 0, int ColorShiftY = 0, int Dilation = 1)
        {
            int newAnalysis = 0;
            Err = "";
            string tmpErr = "";
            string type;
            bool isBefore = false;
            string fil_suffix;
            if (ImageType.IndexOf("Before", StringComparison.CurrentCultureIgnoreCase) >= 0)
            {
                type = "Before";
                fil_suffix = "_B";
                isBefore = true;
            }
            else if (ImageType.IndexOf("After", StringComparison.CurrentCultureIgnoreCase) >= 0)
            {
                type = "After";
                fil_suffix = "_A";
                isBefore = false;
            }
            else
            {
                Err = "ImageType must be either Before or After";
                return 0;
            }
            if (SizingDye.Length < 2)
            {
                Err = "No Sizing Dye";
                return 0;
            }
            if (MaskingDye.Length < 2)
            {
                Err = "No Masking Dye";
                return 0;
            }
            if (SetNumb < 0)
            {
                Err = "SetNumber must be >= 0";
                return 0;
            }
            //int XS, bool XZ, int YS, bool YZ, int ZS, bool ZZ

            IEnumerable<XElement> sortedSizingIEnum = from el in ImageDoc.Descendants("ImagesSortedByDye").Descendants("Dye")
                                                      where el.Attribute("DyeName") != null
                                                      && el.Attribute("DyeName").Value.IndexOf(SizingDye, StringComparison.CurrentCultureIgnoreCase) >= 0
                                                      select el;
            IEnumerable<XElement> sortedMaskingIEnum = from el in ImageDoc.Descendants("ImagesSortedByDye").Descendants("Dye")
                                                       where el.Attribute("DyeName") != null
                                                       && el.Attribute("DyeName").Value.IndexOf(MaskingDye, StringComparison.CurrentCultureIgnoreCase) >= 0
                                                       select el;
            if (sortedSizingIEnum.Count() < 1)
            {
                Err = String.Format("No SortedImage node with Sizing Dye: {0} were found", SizingDye);
                return 0;
            }
            if (sortedMaskingIEnum.Count() < 1)
            {
                Err = String.Format("No SortedImage node with Masking Dye: {0} were found", MaskingDye);
                return 0;
            }
            IEnumerable<XElement> imageSizingIEnum = from el in sortedSizingIEnum.Descendants("Index").Descendants("SortedImage")
                                                     where el.Attribute("Type") != null
                                                     && el.Attribute("Type").Value.IndexOf(type, StringComparison.CurrentCultureIgnoreCase) >= 0
                                                     && el.Attribute("SetNumber") != null
                                                     && Int32.TryParse(el.Attribute("SetNumber").Value, out int sizingSet)
                                                     && sizingSet == SetNumb
                                                     && el.Attribute("Ignored") != null
                                                     && bool.TryParse(el.Attribute("Ignored").Value, out bool igs)
                                                     && !igs
                                                     select el;

            IEnumerable<XElement> imageMaskingIEnum = from el in sortedMaskingIEnum.Descendants("Index").Descendants("SortedImage")
                                                      where el.Attribute("Type") != null
                                                      && el.Attribute("Type").Value.IndexOf(type, StringComparison.CurrentCultureIgnoreCase) >= 0
                                                      && el.Attribute("SetNumber") != null
                                                      && Int32.TryParse(el.Attribute("SetNumber").Value, out int maskingSet)
                                                      && maskingSet == SetNumb
                                                      && el.Attribute("Ignored") != null
                                                      && bool.TryParse(el.Attribute("Ignored").Value, out bool igm)
                                                      && !igm
                                                      select el;

            Err += String.Format("imageSizing/MaskingEnum.Count() =  {0}/{1} | ", imageSizingIEnum.Count(), imageMaskingIEnum.Count());
            if (imageSizingIEnum.Count() < 1)
            {
                Err = String.Format("No images with Sizing Dye: {0} were found", SizingDye);
                return 0;
            }
            if (imageMaskingIEnum.Count() < 1)
            {
                Err = "No Masking images found";
                return 0;
            }
            List<XElement> imageSizingList = imageSizingIEnum.ToList();
            List<XElement> imageMaskingList = imageMaskingIEnum.ToList();
            for (int m = 0; m < imageMaskingList.Count; m++)
            {
                bool addSetup = false;
                XElement xele = new XElement("Unused");
                XElement maskingImage = new XElement("Unused");
                int newProgramCounter = 0;
                if (imageMaskingList[m].Parent.Attribute("Value") != null
                    && Int32.TryParse(imageMaskingList[m].Parent.Attribute("Value").Value, out int maskingIdx)
                    && imageMaskingList[m].Attribute("ImageNumber") != null
                    && Int32.TryParse(imageMaskingList[m].Attribute("ImageNumber").Value, out int maskingNumber)
                    )
                {
                    List<int> sizingImagesFound = new List<int>();
                    for (int n = 0; n < imageSizingList.Count; n++)
                    {
                        if (imageSizingList[n].Parent.Attribute("Value") != null
                            && Int32.TryParse(imageSizingList[n].Parent.Attribute("Value").Value, out int sizingIdx)
                            && imageSizingList[n].Attribute("ImageNumber") != null
                            && Int32.TryParse(imageSizingList[n].Attribute("ImageNumber").Value, out int sizingNumber)
                            )
                        {
                            if (maskingIdx == sizingIdx)
                            {
                                sizingImagesFound.Add(sizingNumber);
                            }
                        }
                    }
                    if (sizingImagesFound.Count == 1)
                    {
                        RCSid.DecodeIndex(maskingIdx, out int r, out int c, out int s);
                        List<XElement> maskingImages = GetImageByImageNumber(maskingNumber);
                        List<XElement> sizingImages = GetImageByImageNumber(sizingImagesFound[0]);
                        if (maskingImages.Count == 1 && sizingImages.Count == 1)
                        {
                            maskingImage = maskingImages[0];
                            XElement sizingImage = sizingImages[0];
                            if (maskingImage.Attribute("JNumber") != null && Int32.TryParse(maskingImage.Attribute("JNumber").Value, out int jNumb))
                            {
                                newProgramCounter = ProgramCounter + 1;
                                string fil = MaskingDye + fil_suffix;
                                xele = new XElement("Analysis",
                                    new XAttribute("ProgramCounter", String.Format("{0:D4}", newProgramCounter)),
                                    new XAttribute("Type", "Masking"),
                                    new XAttribute("WorkingDirectory", WorkingDirectory),
                                    new XAttribute("TemplateDirectory", TemplateDirectory),
                                    new XAttribute("ImageFileName", maskingImage.Attribute("Name").Value),
                                    new XAttribute("TemplatePrefix", Device),
                                    new XAttribute("FilterLabel", fil),
                                    new XAttribute("SubImage", maskingImage.Attribute("Section").Value),
                                    new XAttribute("SizeImageName", sizingImage.Attribute("Name").Value),
                                    new XAttribute("SizeImageNumber", sizingImagesFound[0]),
                                    new XAttribute("BaseName", String.Format("{0}_J{1:D3}_{2}_P{3:D4}",
                                        BaseNameStub, jNumb, RotorChipSubLabel(r, c, s, isBefore), newProgramCounter)),
                                    new XAttribute("ColorShiftX", ColorShiftX.ToString()),
                                    new XAttribute("ColorShiftY", ColorShiftY.ToString()),
                                    new XAttribute("Dilation", Dilation.ToString()),
                                    new XAttribute("TransposeImage", RotateImage.ToString()),
                                    new XAttribute("UseMedianFilter", UseMedianFilter.ToString()),
                                    new XAttribute("ExtraOutput", "0"),
                                    new XAttribute("Performed", "false"),
                                    new XAttribute("Success", "false"),
                                    new XAttribute("Selected", "false")
                                    );
                                addSetup = true;
                                bool duplicate = false;
                                if (maskingImage.Descendants("AnalysisSetup") != null)
                                {
                                    List<XElement> existingSetups = maskingImage.Descendants("AnalysisSetup").Descendants("Analysis").ToList();
                                    for (int e = 0; e < existingSetups.Count; e++)
                                    {
                                        if ((xele.Attribute("WorkingDirectory").Value == existingSetups[e].Attribute("WorkingDirectory").Value)
                                            && (xele.Attribute("TemplateDirectory").Value == existingSetups[e].Attribute("TemplateDirectory").Value)
                                            && (xele.Attribute("ImageFileName").Value == existingSetups[e].Attribute("ImageFileName").Value)
                                            && (xele.Attribute("TemplatePrefix").Value == existingSetups[e].Attribute("TemplatePrefix").Value)
                                            && (xele.Attribute("FilterLabel").Value == existingSetups[e].Attribute("FilterLabel").Value)
                                            && (xele.Attribute("TransposeImage").Value == existingSetups[e].Attribute("TransposeImage").Value)
                                            && (xele.Attribute("UseMedianFilter").Value == existingSetups[e].Attribute("UseMedianFilter").Value)
                                            && (xele.Attribute("ExtraOutput").Value == existingSetups[e].Attribute("ExtraOutput").Value)
                                            && (xele.Attribute("ColorShiftX").Value == existingSetups[e].Attribute("ColorShiftX").Value)
                                            && (xele.Attribute("ColorShiftY").Value == existingSetups[e].Attribute("ColorShiftY").Value)
                                            && (xele.Attribute("Dilation").Value == existingSetups[e].Attribute("Dilation").Value)
                                            )
                                        {
                                            duplicate = true;
                                            break;
                                        }
                                    }
                                }
                                if (duplicate)
                                {
                                    addSetup = false;
                                    if (Err.Length < 10)
                                        Err += tmpErr;
                                    Err += String.Format("Preexisiting setup found for masking number {0} | ", maskingNumber);
                                }
                            }
                            else
                            {

                            }
                        }
                        else
                        {

                        }
                    }
                    else
                    {

                    }
                }
                if (addSetup)
                {
                    IEnumerable<XElement> maskingIEnum = maskingImage.Descendants("AnalysisSetup");
                    if (maskingIEnum.Count() == 1)
                    {
                        maskingIEnum.First().Add(xele);
                        ProgramCounter = newProgramCounter;
                        SetProgramCounter(newProgramCounter);
                        newAnalysis++;
                    }
                }
            }
            return newAnalysis;
        }


        public int GetMaskingAnalysisArray(bool Before, int SetNumb, bool All, bool RStatus, bool SStatus,
            out string[] SetupStrings, out string Err)
        {
            int res = GetMaskingAnalysisStrings(Before, SetNumb, All, RStatus, SStatus,
                out List<string> setupStringsList, out Err);
            SetupStrings = setupStringsList.ToArray();
            return res;
        }
        public int GetMaskingAnalysisStrings(bool Before, int SetNumb, bool All, bool RStatus, bool SStatus,
            out List<string> SetupStrings, out string Err)
        {
            Err = "";
            SetupStrings = new List<string>();
            char[] zero = new char[] { '0' };
            if (_XDocLoaded)
            {
                string imageType = "";
                if (Before)
                    imageType = "Before";
                else
                    imageType = "After";
                IEnumerable<XElement> imageIEnum = from el in ImageDoc.Descendants("Images").Descendants("Image")
                                                   where el.Attribute("Type") != null
                                                   && el.Attribute("Type").Value.IndexOf(imageType, StringComparison.CurrentCultureIgnoreCase) >= 0
                                                   select el;
                if (imageIEnum.Count() > 0)
                {
                    List<XElement> analysisSetupList = imageIEnum.Descendants("AnalysisSetup").ToList();
                    if (analysisSetupList.Count > 0)
                    {
                        for (int n = 0; n < analysisSetupList.Count; n++)
                        {
                            List<XElement> analysisList = analysisSetupList[n].Descendants("Analysis").ToList();
                            if (analysisList.Count > 0)
                            {
                                for (int m = 0; m < analysisList.Count; m++)
                                {
                                    if (analysisList[m].Attribute("Type") != null
                                        && analysisList[m].Attribute("Type").Value.IndexOf("Masking", StringComparison.CurrentCultureIgnoreCase) >= 0
                                        && analysisList[m].Attribute("ProgramCounter") != null
                                        && Int32.TryParse(analysisList[m].Attribute("ProgramCounter").Value, out int pc)
                                        && analysisList[m].Parent.Parent.Attribute("ImageNumber") != null
                                        && Int32.TryParse(analysisList[m].Parent.Parent.Attribute("ImageNumber").Value, out int im)
                                        )
                                    {
                                        XElement pp = analysisList[m].Parent.Parent;
                                        string setN = pp.Attribute("SetNumber").Value;
                                        string per = analysisList[m].Attribute("Performed").Value;
                                        string suc = analysisList[m].Attribute("Success").Value;
                                        bool bPer;
                                        bool bSuc;
                                        int iSetN;
                                        string pJNumb = pp.Attribute("JNumber").Value;
                                        if (bool.TryParse(per, out bPer) && bool.TryParse(suc, out bSuc)
                                            && Int32.TryParse(setN, out iSetN))
                                        {
                                            if (RStatus == bPer)
                                                bPer = true;
                                            else
                                                bPer = false;
                                            if (SStatus == bSuc)
                                                bSuc = true;
                                            else
                                                bSuc = false;
                                            bool outp = (bPer && bSuc || All);
                                            if (iSetN != SetNumb)
                                                outp = false;
                                            if (outp)
                                            {
                                                string pExtraOutput = analysisList[m].Attribute("ExtraOutput").Value;
                                                string pUseMedian = analysisList[m].Attribute("UseMedianFilter").Value;
                                                string pTransposeIm = analysisList[m].Attribute("TransposeImage").Value;
                                                string pBaseName = analysisList[m].Attribute("BaseName").Value;
                                                string pSubImage = analysisList[m].Attribute("SubImage").Value;
                                                string pFilterL = analysisList[m].Attribute("FilterLabel").Value;
                                                string pPrefix = analysisList[m].Attribute("TemplatePrefix").Value;
                                                string pImageName = analysisList[m].Attribute("ImageFileName").Value;
                                                string pTDirectory = analysisList[m].Attribute("TemplateDirectory").Value;
                                                string pWDirectory = analysisList[m].Attribute("WorkingDirectory").Value;
                                                string pColorShiftX = analysisList[m].Attribute("ColorShiftX").Value;
                                                string pColorShiftY = analysisList[m].Attribute("ColorShiftY").Value;
                                                string pDilation = analysisList[m].Attribute("Dilation").Value;
                                                string pSizingImageNumber = analysisList[m].Attribute("SizeImageNumber").Value;
                                                int iExtraOutput;
                                                bool bUseMedian;
                                                bool bTransposeIm;
                                                int iSubImage;
                                                int iColorShiftX;
                                                int iColorShiftY;
                                                int iDilation;
                                                int iSizingImageNumber;
                                                string pSizingBaseName;
                                                if (Int32.TryParse(pExtraOutput, out iExtraOutput)
                                                    && bool.TryParse(pUseMedian, out bUseMedian)
                                                    && bool.TryParse(pTransposeIm, out bTransposeIm)
                                                    && Int32.TryParse(pSubImage, out iSubImage)
                                                    && Int32.TryParse(pColorShiftX, out iColorShiftX)
                                                    && Int32.TryParse(pColorShiftY, out iColorShiftY)
                                                    && Int32.TryParse(pDilation, out iDilation)
                                                    && Int32.TryParse(pSizingImageNumber, out iSizingImageNumber)
                                                    )
                                                {
                                                    List<XElement> sizingImList = GetImageByImageNumber(iSizingImageNumber);
                                                    if (sizingImList.Count == 1)
                                                    {
                                                        //string spc = "";
                                                        IEnumerable<XElement> sizingAnalysisSetupIEnum = from els in sizingImList[0].Descendants("AnalysisSetup").Descendants("Analysis")
                                                                                                         where els.Attribute("Type") != null
                                                                                                         && els.Attribute("Type").Value.IndexOf("Sizing", StringComparison.CurrentCultureIgnoreCase) >= 0
                                                                                                         && els.Attribute("Selected") != null
                                                                                                         && bool.TryParse(els.Attribute("Selected").Value, out bool sel)
                                                                                                         && sel
                                                                                                         select els;
                                                        if (sizingAnalysisSetupIEnum.Count() == 1)
                                                        {
                                                            pSizingBaseName = sizingAnalysisSetupIEnum.First().Attribute("BaseName").Value;
                                                            SetupStrings.Add(String.Format("sdpcrMasking.exe {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} {12} {13} | {14} | {15}",
                                                                pWDirectory, pTDirectory, pImageName, pPrefix, pFilterL, iSubImage.ToString(),
                                                                pSizingBaseName, pBaseName, iColorShiftX, iColorShiftY, iDilation,
                                                                Convert.ToInt32(bTransposeIm), Convert.ToInt32(bUseMedian), iExtraOutput,
                                                                pc.ToString("D4"), im.ToString("D3")));
                                                        }
                                                        else if (sizingAnalysisSetupIEnum.Count() > 1)
                                                        {
                                                            Err += String.Format("sizingAnalysisSetupIEnum.Count() != 1 for Image JNumb = {0}. + ",
                                                                pJNumb);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    Err += String.Format("sizingImList.Count != 1 for Image JNumb = {0}. + ",
                                                                   pJNumb);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return SetupStrings.Count;
        }
        public int GetMaskingAnalysisSetups(int SetNumb, bool RStatus, bool SStatus,
            out List<string> SetupStrings, out List<XElement> SetupInfo, out string Err)
        {
            Err = "";
            SetupStrings = new List<string>();
            SetupInfo = new List<XElement>();
            char[] zero = new char[] { '0' };
            if (_XDocLoaded)
            {
                List<XElement> analysisSetupList = ImageDoc.Descendants("AnalysisSetup").ToList();
                if (analysisSetupList.Count > 0)
                {
                    for (int n = 0; n < analysisSetupList.Count; n++)
                    {
                        List<XElement> analysisList = analysisSetupList[n].Descendants("Analysis").ToList();
                        if (analysisList.Count > 0)
                        {
                            for (int m = 0; m < analysisList.Count; m++)
                            {
                                if (analysisList[m].Attribute("Type") != null
                                    && analysisList[m].Attribute("Type").Value.IndexOf("Masking", StringComparison.CurrentCultureIgnoreCase) >= 0
                                    && analysisList[m].Attribute("ProgramCounter") != null
                                        && Int32.TryParse(analysisList[m].Attribute("ProgramCounter").Value, out int pc)
                                        && analysisList[m].Parent.Parent.Attribute("ImageNumber") != null
                                        && Int32.TryParse(analysisList[m].Parent.Parent.Attribute("ImageNumber").Value, out int im)
                                        )
                                {
                                    XElement pp = analysisList[m].Parent.Parent;
                                    string setN = pp.Attribute("SetNumber").Value;
                                    string pPerformed = analysisList[m].Attribute("Performed").Value;
                                    string pSuccess = analysisList[m].Attribute("Success").Value;
                                    bool bPer;
                                    bool bSuc;
                                    int iSetN;
                                    string pJNumb = pp.Attribute("JNumber").Value;
                                    if (bool.TryParse(pPerformed, out bPer) && bool.TryParse(pSuccess, out bSuc)
                                        && Int32.TryParse(setN, out iSetN))
                                    {
                                        if (RStatus == bPer)
                                            bPer = true;
                                        else
                                            bPer = false;
                                        if (SStatus == bSuc)
                                            bSuc = true;
                                        else
                                            bSuc = false;
                                        bool outp = bPer && bSuc;
                                        if (iSetN != SetNumb)
                                            outp = false;
                                        if (outp)
                                        {
                                            string pExtraOutput = analysisList[m].Attribute("ExtraOutput").Value;
                                            string pUseMedian = analysisList[m].Attribute("UseMedianFilter").Value;
                                            string pTransposeIm = analysisList[m].Attribute("TransposeImage").Value;
                                            string pProgramCounter = analysisList[m].Attribute("ProgramCounter").Value;
                                            string pBaseName = analysisList[m].Attribute("BaseName").Value;
                                            string pSubImage = analysisList[m].Attribute("SubImage").Value;
                                            string pFilterL = analysisList[m].Attribute("FilterLabel").Value;
                                            string pPrefix = analysisList[m].Attribute("TemplatePrefix").Value;
                                            string pImageName = analysisList[m].Attribute("ImageFileName").Value;
                                            string pTDirectory = analysisList[m].Attribute("TemplateDirectory").Value;
                                            string pWDirectory = analysisList[m].Attribute("WorkingDirectory").Value;
                                            string pColorShiftX = analysisList[m].Attribute("ColorShiftX").Value;
                                            string pColorShiftY = analysisList[m].Attribute("ColorShiftY").Value;
                                            string pDilation = analysisList[m].Attribute("Dilation").Value;
                                            string pSizingImageNumber = analysisList[m].Attribute("SizeImageNumber").Value;
                                            string pRotor = analysisList[n].Parent.Parent.Attribute("RotorPosition").Value;
                                            string pChip = analysisList[n].Parent.Parent.Attribute("ChipPosition").Value;
                                            int iExtraOutput;
                                            bool bUseMedian;
                                            bool bTransposeIm;
                                            int iSubImage;
                                            int iColorShiftX;
                                            int iColorShiftY;
                                            int iDilation;
                                            int iSizingImageNumber;
                                            int iRotor;
                                            string pSizingBaseName;
                                            if (Int32.TryParse(pExtraOutput, out iExtraOutput)
                                                && bool.TryParse(pUseMedian, out bUseMedian)
                                                && bool.TryParse(pTransposeIm, out bTransposeIm)
                                                && Int32.TryParse(pSubImage, out iSubImage)
                                                && Int32.TryParse(pColorShiftX, out iColorShiftX)
                                                && Int32.TryParse(pColorShiftY, out iColorShiftY)
                                                && Int32.TryParse(pDilation, out iDilation)
                                                && Int32.TryParse(pSizingImageNumber, out iSizingImageNumber)
                                                && Int32.TryParse(pRotor, out iRotor)
                                                && (pChip.IndexOf("Left", StringComparison.CurrentCultureIgnoreCase) >= 0
                                                || pChip.IndexOf("Right", StringComparison.CurrentCultureIgnoreCase) >= 0)
                                                )
                                            {
                                                List<XElement> sizingImList = GetImageByImageNumber(iSizingImageNumber);
                                                if (sizingImList.Count == 1)
                                                {
                                                    //string spc = "";
                                                    IEnumerable<XElement> sizingSetupIEnum = from el in sizingImList[0].Descendants("AnalysisSetup").Descendants("Analysis")
                                                                                             where el.Attribute("Type") != null
                                                                                             && el.Attribute("Type").Value.IndexOf("Sizing", StringComparison.CurrentCultureIgnoreCase) >= 0
                                                                                             && el.Attribute("Ignored") != null
                                                                                             && bool.TryParse(el.Attribute("Ignored").Value, out bool ig)
                                                                                             && !ig
                                                                                             select el;
                                                    if (sizingSetupIEnum.Count() == 1)
                                                    {
                                                        if (sizingSetupIEnum.First().Attribute("Success") != null
                                                            && bool.TryParse(sizingSetupIEnum.First().Attribute("Success").Value, out bool sel)
                                                            && sel)
                                                        {
                                                            int count = SetupStrings.Count;
                                                            pSizingBaseName = sizingSetupIEnum.First().Attribute("BaseName").Value;
                                                            SetupStrings.Add(String.Format("sdpcrMasking.exe {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} {12} {13} | {14} | {15}",
                                                                pWDirectory, pTDirectory, pImageName, pPrefix, pFilterL, iSubImage.ToString(),
                                                                pSizingBaseName, pBaseName, iColorShiftX, iColorShiftY, iDilation,
                                                                Convert.ToInt32(bTransposeIm), Convert.ToInt32(bUseMedian), iExtraOutput,
                                                                pc.ToString("D4"), im.ToString("D3")));

                                                            XElement info = new XElement("SetupInfo",
                                                                new XAttribute("ScriptIndex", count.ToString()),
                                                                new XAttribute("ProgramCounter", pProgramCounter),
                                                                new XAttribute("RotorPosition", pRotor),
                                                                new XAttribute("ChipPosition", pChip),
                                                                new XAttribute("SubImage", pSubImage),
                                                                new XAttribute("SizeImageName", pProgramCounter),
                                                                new XAttribute("SizeImageNumber", pProgramCounter),
                                                                new XAttribute("BaseName", pProgramCounter),
                                                                new XAttribute("Performed", pPerformed),
                                                                new XAttribute("Success", pSuccess)
                                                                );
                                                            SetupInfo.Add(XElement.Parse(info.ToString()));
                                                            //                     < Analysis ProgramCounter = "0023" Type = "Masking" WorkingDirectory = "c:\Instrument"
                                                            //                     TemplateDirectory = "c:\Instrument\Template" 
                                                            //                     RotorPosition, ChipPosition,  SubImage = "3" SizeImageName = "XY5-000000024_J017_R1LS3_L0F0_aft0.png"
                                                            //                     SizeImageNumber = "14" BaseName = "XY5-24Test_J018_A0103_P0023" Performed = "true" Success = "true" />

                                                            //                   </ AnalysisSetup >
                                                            //                   XY5 - 000000024_J004_R1LS2_L1F1_bef0.png M1015K04 DyeB_B 2 XY5 - 24Test_J003_B102_P0013 XY5-24Test_J004_B102_P0017 0 0
                                                        }
                                                        else
                                                        {
                                                            Err += String.Format("sizingSetupIEnum.[Success] false for Image JNumb = {0}. + ",
                                                                pJNumb);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Err += String.Format("sizingSetupIEnum.Count() != 1 for Image JNumb = {0}. + ",
                                                                pJNumb);
                                                    }
                                                }
                                                else
                                                {
                                                    Err += String.Format("sizingImList.Count != 1 for Image JNumb = {0}. + ",
                                                               pJNumb);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return SetupStrings.Count;
        }
    };
}
