﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace parseImageData
{
    class Program
    {
        public static XElement CollectionNode(string Name, string Type)
        {
            XElement xe = new XElement(Name, new XAttribute("CollectionType", Type));
            return xe;
        }
        public static XElement SimpleNode(string Name)
        {
            XElement xe = new XElement(Name);
            return xe;
        }
        static void Main(string[] args)
        {
            if (args.Length < 5)
            {
                Console.WriteLine("Too few arguments");
                Environment.Exit(0);
            }
            int arraysPerChip = 4;
            int sectionsPerArray = 3;
            string templateDirectory;
            string workingDirectory;
            string imageInfoFile;
            string basename;
            List<string> quantDyes = new List<string>();

            if (!Int32.TryParse(args[0], out arraysPerChip))
            {
                Console.WriteLine("Problems reading arraysPerChip");
                Environment.Exit(0);
            }
            templateDirectory = args[1];
            workingDirectory = args[2];
            imageInfoFile = args[3];
            basename = args[4];

            string outputName = workingDirectory + "\\" + basename + ".xml";

            string imageInfoName = workingDirectory + "\\" + imageInfoFile;
            string dateLine = "";
            string projectLine = "";
            string headerLine = "";
            List<string> imageLines = new List<string>();
            using (System.IO.StreamReader file = new System.IO.StreamReader(imageInfoName))
            {
                string line;
                if ((dateLine = file.ReadLine()) != null)
                {
                    if ((projectLine = file.ReadLine()) != null)
                    {
                        if ((headerLine = file.ReadLine()) != null)
                        {
                            while ((line = file.ReadLine()) != null)
                                imageLines.Add(line);
                        }
                        else
                        {
                            Console.WriteLine("Problems reading header from image info file");
                            Environment.Exit(0);
                        }
                    }
                    else
                    {
                        Console.WriteLine("Problems reading project line from image info file");
                        Environment.Exit(0);
                    }
                }
                else
                {
                    Console.WriteLine("Problems reading date from image info file");
                    Environment.Exit(0);
                }
            }
            if (imageLines.Count < 1)
            {
                Console.WriteLine("No image data in file");
                Environment.Exit(0);
            }
            string[] headTokens = headerLine.Split(',');
            if (headTokens.Length < 15)
            {
                Console.WriteLine("Header line has too few entries");
                Environment.Exit(0);
            }
            int nameN = -1;
            int timeN = -1;
            int imageN = -1;
            int rotorN = -1;
            int chipN = -1;
            int subN = -1;
            int deviceN = -1;
            int dyeN = -1;
            int ledN = -1;
            int filterN = -1;
            int gainN = -1;
            int exposureN = -1;
            int setTypeN = -1;
            int setNumberN = -1;

            for (int n = 0; n < headTokens.Length; n++)
            {
                if (headTokens[n].IndexOf("File Name") >= 0)
                    nameN = n;

                else if (headTokens[n].IndexOf("Time") >= 0)
                    timeN = n;

                else if (headTokens[n].IndexOf("Image No") >= 0)
                    imageN = n;

                else if (headTokens[n].IndexOf("Rotor") >= 0)
                    rotorN = n;

                else if (headTokens[n].IndexOf("Chip") >= 0)
                    chipN = n;

                else if (headTokens[n].IndexOf("SubImage") >= 0)
                    subN = n;

                else if (headTokens[n].IndexOf("Device") >= 0)
                    deviceN = n;

                else if (headTokens[n].IndexOf("Dye") >= 0)
                    dyeN = n;

                else if (headTokens[n].IndexOf("LED") >= 0)
                    ledN = n;

                else if (headTokens[n].IndexOf("EmFilter") >= 0)
                    filterN = n;

                else if (headTokens[n].IndexOf("Gain") >= 0)
                    gainN = n;

                else if (headTokens[n].IndexOf("Exposure") >= 0)
                    exposureN = n;

                else if (headTokens[n].IndexOf("Set Type") >= 0)
                    setTypeN = n;

                else if (headTokens[n].IndexOf("Set No") >= 0)
                    setNumberN = n;
            }
            if (nameN < 0 || timeN < 0 || imageN < 0 || rotorN < 0 || chipN < 0 || subN < 0 || deviceN < 0 || dyeN < 0
                || ledN < 0 || filterN < 0 || gainN < 0 || exposureN < 0 || setTypeN < 0 || setNumberN < 0)
            {
                Console.WriteLine("Problem with header line in info file");
                Environment.Exit(0);
            }

            string[] lineTokens = imageLines[0].Split(',');
            if (lineTokens.Length < 15)
            {
                Console.WriteLine("First image line has too few entries");
                Environment.Exit(0);
            }

            XDocument imManage = new XDocument();
            var rootElement = new XElement("SDPCRAnalysis");
            imManage.Add(rootElement);
            char[] delim = new char[]{ ' ', ',', '\n', '\t' };
            dateLine = dateLine.Trim(delim);
            projectLine = projectLine.Trim(delim);
            XElement xset = SimpleNode("Settings");
            var xel = new XElement("Date", new XAttribute("Value", dateLine));
            xset.Add(xel);
            xel = new XElement("Project", new XAttribute("Name", projectLine));
            xset.Add(xel);
            xel = new XElement("Directory",
                new XAttribute("Type", "Working"),
                new XAttribute("Name", workingDirectory));
            xset.Add(xel);
            xel = new XElement("Directory",
                new XAttribute("Type", "Template"),
                new XAttribute("Name", templateDirectory));
            xset.Add(xel);
            xel = new XElement("Device",
                new XAttribute("CatNumber", lineTokens[deviceN]),
                new XAttribute("Type", "4 Channel BR"),
                new XAttribute("NumbArrays", arraysPerChip.ToString()),
                new XAttribute("NumbSections", sectionsPerArray.ToString()),
                new XAttribute("RotateImage", "false")
                ) ;
            xset.Add(xel);
            xel = new XElement("UseMedianFilter", new XAttribute("Value", "true"));
            xset.Add(xel);
            xel = new XElement("ProgramCounter", new XAttribute("Value", "0"));
            xset.Add(xel);
            //xel = new XElement("Dyes", new XAttribute("SizingDye", "DyeA"));
            //xel.Add(new XElement("QuantDye", new XAttribute("Name", "DyeB")));
            //xset.Add(xel);
            rootElement.Add(xset);

            var xims = CollectionNode("Images", "Image");
            for (int n = 0; n < imageLines.Count; n++)
            {
                lineTokens = imageLines[n].Split(',');
                if (lineTokens.Length < 15)
                {
                    Console.WriteLine(String.Format("Line {0} has too few entries", n));
                }
                else
                {
                    var xim = new XElement("Image",
                        new XAttribute("Name", lineTokens[nameN]),
                        new XAttribute("Time", lineTokens[timeN]),
                        new XAttribute("ImageNumber", lineTokens[imageN]),
                        new XAttribute("RotorPosition", lineTokens[rotorN]),
                        new XAttribute("ChipPosition", lineTokens[chipN]),
                        new XAttribute("Section", lineTokens[subN]),
                        new XAttribute("Dye", lineTokens[dyeN]),
                        new XAttribute("SetNumber", lineTokens[setNumberN]),
                        new XAttribute("Type", lineTokens[setTypeN]));
                    var xprog = CollectionNode("AnalysisTypes", "AnalysisType");
                    var xana = new XElement("AnalysisType",
                        new XAttribute("Type", "Sizing"),
                        new XAttribute("Performed", "false"),
                        new XAttribute("Success", "false"),
                        new XAttribute("ProgramCounter", "0"));
                    xprog.Add(xana);
                    xana = new XElement("AnalysisType",
                        new XAttribute("Type", "Masking"),
                        new XAttribute("Performed", "false"),
                        new XAttribute("Success", "false"),
                        new XAttribute("ProgramCounter", "0"));
                    xprog.Add(xana);
                    xana = new XElement("AnalysisType",
                        new XAttribute("Type", "Quant"),
                        new XAttribute("Performed", "false"),
                        new XAttribute("Success", "false"),
                        new XAttribute("ProgramCounter", "0"));
                    xprog.Add(xana);
                    xim.Add(xprog);
                    xim.Add(CollectionNode("AnalysisSetup", "Analysis"));
                    xims.Add(xim);
                }
            }
            rootElement.Add(xims);
            var xquan = CollectionNode("Quantifications", "Quantify");
            var xcomb = CollectionNode("Quantifications", "Combine");
            rootElement.Add(xquan);
            rootElement.Add(xcomb);
            string xDoc = imManage.ToString();
            System.IO.File.WriteAllText(@outputName, xDoc);

        }
    }
}
