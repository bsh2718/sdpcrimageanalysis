#pragma once

#ifdef SDPCRIMAGEANALYSISLV_EXPORTS
#define SDPCRIMAGEANALYSISLV_API __declspec(dllexport)
#else
#define SDPCRIMAGEANALYSISLV_API __declspec(dllimport)
#endif

#include <string>
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

SDPCRIMAGEANALYSISLV_API int callSdpcrSizingLib(char Err[], char* WorkingDirectory, char* TemplateDirectory,
	char* ImageFileName, char* TemplatePrefix, char* FilterLabel, char* SubImageLabel, char* BaseName,
	unsigned int TransposeImageInt = 1, unsigned int UseMedianFilterInt = 1, unsigned int ExtraOutput = 0);


SDPCRIMAGEANALYSISLV_API int callSdpcrMaskingLib(char Err[], char* WorkingDirectory, char* TemplateDirectory,
	char* ImageFileName, char* TemplatePrefix, char* FilterLabel, char* SubImageLabel,
	char* InputBaseName, char* BaseName, int ColorShiftX, int ColorShiftY, int Dilation = 1,
	unsigned int TransposeImageInt = 1, unsigned int UseMedianFilterInt = 1, unsigned int ExtraOutput = 0);

SDPCRIMAGEANALYSISLV_API int callSdpcrQuantLib(char Err[], char* WorkingDirectory, char* TemplateDirectory,
	char* ParameterFile, char* Prefix, char* SubImageLabel,
	char* BaseNameOutput, char* BaseNameInput, char* ConcatenatedBaseNamesAfter);

SDPCRIMAGEANALYSISLV_API int callSdpcrMultiQuant(char Err[], char* WorkingDirectory, char* TemplateDirectory,
	char* ParameterName, char* BaseName, char* ConcatenatedAfterNames);

int sdpcrSizingLib(std::string& Err, std::string WorkingDirectory, std::string TemplateDirectory,
	std::string ImageFileName, std::string TemplatePrefix, std::string FilterLabel, std::string SubImageLabel, std::string BaseName,
	bool TransposeImage = false, bool UseMedianFilter = true, unsigned int ExtraOutput = 0);

int sdpcrMaskingLib(std::string& Err, std::string WorkingDirectory, std::string TemplateDirectory,
	std::string ImageFileName, std::string TemplatePrefix, std::string FilterLabel, std::string SubImageLabel,
	std::string InputBaseName, std::string BaseName, cv::Point ColorShift, int Dilation = 1,
	bool TransposeImage = false, bool UseMedianFilter = false, unsigned int extraOutput = 0);

int sdpcrQuantLib(std::string& Err, std::string WorkingDirectory, std::string TemplateDirectory,
	std::string ParameterFile, std::string Prefix, std::string SubImageLabel,
	std::string BaseNameOutput, std::string BaseNameInput, std::vector< std::string > BaseNamesAfter);

int sdpcrMultiQuant(std::string& Err, std::string WorkingDirectory, std::string TemplateDirectory,
	std::string ParameterName, std::string BaseName, std::vector< std::string > AfterNames);