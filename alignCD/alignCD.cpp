
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <iomanip>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "physicalconstants.h"
#include "statistics.hpp"

struct HoughLines
{
    int votes;
    double rho;
    double theta;
    double thetaX;
    double x0;
    double y0;
    double YMidPoint;
    double XIntercept;
    double YIntercept;
};
struct LinePairs
{
    int Index1;
    int Votes1;
    double Intercept1;
    int Index2;
    int Votes2;
    double Intercept2;
    double Delta;  
};
bool SortHoughLines(HoughLines A, HoughLines B)
{
    return A.votes > B.votes;
}
std::string type2str(int type)
{
    std::string r;

    uchar depth = type & CV_MAT_DEPTH_MASK;
    uchar chans = 1 + (type >> CV_CN_SHIFT);

    switch (depth)
    {
    case CV_8U:  r = "8U"; break;
    case CV_8S:  r = "8S"; break;
    case CV_16U: r = "16U"; break;
    case CV_16S: r = "16S"; break;
    case CV_32S: r = "32S"; break;
    case CV_32F: r = "32F"; break;
    case CV_64F: r = "64F"; break;
    default:     r = "User"; break;
    }

    r += "C";
    r += (chans + '0');

    return r;
}

void ShowUsage(const char* msg, const char* exe)
{
    std::clog << ">>>> " << msg << std::endl;
    std::clog << "Usage: " << exe << " <Working Directory> <Image Name> <Alignment Threshold> <mm Per Pixel> <Horz Bar Length(mm)>\n";
    std::clog << "<Horz Bar Thickness (mm)> <Steps Per Revolution > <Rotate Image?> [<Base Name>]\n";	// 
    std::clog << "\n";
}

int main(int Argc, char** Argv)
{
    std::ostringstream oString;
    std::ofstream logFile;
    std::string errorMsg;
    std::string outName;
    int numbRequiredArgs = 9;
    if (Argc < numbRequiredArgs)
    {
        ShowUsage( "Too Few Arguements", Argv[0]);
        exit(0);
    }
    std::string workingDirectory;
    std::string imageName;
    int threshold;
    double mmPerPixel;
    //double distanceToCenter_mm;
    double horzBarLength_mm;
    double horzBarThickness_mm;
    int stepsPerRevolution;
    std::string baseName;
    std::string basePath;
    bool outputFiles = false;
    double rhoStep = 2.0;                       // 3.0
    double thetaStep = RADIANSPERDEGREE * 0.7;  // * 1.0
    //double distanceToCenter;
    int hBarLength;
    int hBarThickness;
    int hBarLengthLowerLimit;
    int hBarThicknessLowerLimit;
    int hBarLengthUpperLimit;
    int hBarThicknessUpperLimit;
    bool rotateImage;
    int ia = 0;
    int ie = 0;
    std::istringstream token;
    {
        double temp;
        int itemp;
        //std::string stemp;
        workingDirectory.assign(Argv[++ia]);
        imageName.assign(Argv[++ia]);

        token.clear(); token.str(Argv[++ia]); token >> itemp; if (token.fail() || itemp <= 0.0) { ++ie; }
        else { threshold = itemp; }

        token.clear(); token.str(Argv[++ia]); token >> temp; if (token.fail() || temp <= 0.001) { ++ie; }
        else { mmPerPixel = temp; }

        //token.clear(); token.str(Argv[++ia]); token >> temp; if (token.fail() || temp <= 0.0) { ++ie; }
        //else { distanceToCenter_mm = temp; }

        token.clear(); token.str(Argv[++ia]); token >> temp; if (token.fail() || temp <= 0.0) { ++ie; }
        else { horzBarLength_mm = temp; }

        token.clear(); token.str(Argv[++ia]); token >> temp; if (token.fail() || temp <= 0.0) { ++ie; }
        else { horzBarThickness_mm = temp; }

        token.clear(); token.str(Argv[++ia]); token >> itemp; if (token.fail() || itemp <= 0.0) { ++ie; }
        else { stepsPerRevolution = itemp; }

        token.clear(); token.str(Argv[++ia]); token >> itemp; if (token.fail()) { ++ie; }
        else { if (itemp == 0) rotateImage = false; else rotateImage = true; }

        if (++ia < Argc)
        {
            baseName.assign(Argv[ia]);
            outputFiles = true;
        }
        else
            baseName = "";
    }
    std::vector< std::string > logOutput;
    logOutput.clear();
    //distanceToCenter = distanceToCenter_mm / mmPerPixel;
    hBarLength = (int)round(horzBarLength_mm / mmPerPixel);
    hBarThickness = (int)round(horzBarThickness_mm / mmPerPixel);
    hBarLengthLowerLimit = (int)round(hBarLength * 0.70);
    hBarThicknessLowerLimit = (int)round(hBarThickness * 0.70);
    hBarLengthUpperLimit = (int)round(hBarLength * 1.15);
    hBarThicknessUpperLimit = (int)round(hBarLength * 0.35) + hBarThickness;
    if (hBarThicknessUpperLimit < hBarThickness * 2.0)
        hBarThicknessUpperLimit = (int)round(hBarThickness * 2.0);

    //std::cout << "ia/ie " << ia << "/" << ie << std::endl;
    //std::cout << "distanceToCenter: " << distanceToCenter << std::endl;
    //std::cout << "hBarLength: " << hBarLength << std::endl;
    //std::cout << "hBarThickness: " << hBarThickness << std::endl;
    //std::cout << "stepsPerRevolution: " << stepsPerRevolution << std::endl;
    //std::cout << "rotateImage: " << std::boolalpha <<  rotateImage << std::endl;
    //std::cout << "mmPerPixel: " << mmPerPixel << std::endl;
    //std::cout << "threshold: " << threshold << std::endl;
    if (outputFiles)
    {
        basePath = workingDirectory + "\\" + baseName;
        outName = basePath;
        outName.append(".csv");
        logFile.open(outName.c_str());
        if (!logFile.is_open())
        {
            for (int of = 1; of < 20; of++)
            {
                oString.str("");
                oString.clear();
                oString << basePath << "_" << of << ".csv";
                outName = oString.str();
                logFile.open(outName.c_str());
                if (logFile.is_open())
                    break;
            }
        }
        if (logFile.is_open())
            logFile << "logFile," << outName << std::endl;
        else
            outputFiles = false;
        //std::cout << outName << std::endl;
    }
    double rotAngle = 0.0;
    double rotSteps = 0;
    double xSteps = 0;
    double ySteps = 0;
    std::string inputImageName = workingDirectory + "\\" + imageName;
    cv::Mat image = cv::imread(inputImageName, cv::IMREAD_GRAYSCALE | cv::IMREAD_ANYDEPTH);
    if (image.empty())
    {
        std::cerr << "0, -4, image file is either empty or could not be found: " << inputImageName << std::endl;
        if (outputFiles)
            logFile << "image file is either empty or could not be found: " << inputImageName << std::endl;
        exit(0);
    }
    
    //std::cout << "after rotate image" << std::endl;

    bool eightBit = false;
    unsigned int type = image.depth();
    uchar depth = type & CV_MAT_DEPTH_MASK;
    uchar chans = 1 + (type >> CV_CN_SHIFT);
    if (depth == CV_8U && chans == 1)
    {
        eightBit = true;
        oString.str("");
        oString.clear();
        oString << " image type, CV_8UC1";
        if (outputFiles)
            logOutput.push_back(oString.str());
    }
    else if ((depth == CV_16U) && (chans == 1))
    {
        eightBit = false;
        oString.str("");
        oString.clear();
        oString << "image type, CV_16UC1";
        if (outputFiles)
            logOutput.push_back(oString.str());
        for (int n = 0; n < image.total(); n++)
        {
            unsigned int ntmp = image.at<short unsigned int>(n);
            image.at<short unsigned int>(n) = (ntmp >> 4);
        }
    }
    else
    {
        oString.str("");
        oString.clear();
        oString << "Problems opening image: " << inputImageName << " Image is of type " << type2str(image.depth()) << " which program cannot handle.";
        errorMsg = oString.str();
        std::cout << errorMsg << std::endl;
        if (outputFiles)
        {
            logFile << errorMsg << std::endl;
            logFile.close();
        }
        exit(0);
    }

    cv::Mat workImage1;
    if (rotateImage)
    {
        //std::cout << "rotating image " << std::endl;
        workImage1 = image.t();
        int nrows = workImage1.rows;
        int mcols = workImage1.cols;
        int mcolsLimit = mcols / 2;
        for (int n = 0; n < workImage1.rows; n++)
        {
            for (int m = 0; m < mcolsLimit; m++)
            {
                int mm = mcols - m - 1;
                if (eightBit)
                {
                    int tr = workImage1.at<uchar>(cv::Point(m, n));
                    workImage1.at<uchar>(cv::Point(m, n)) = workImage1.at<uchar>(cv::Point(mm, n));
                    workImage1.at<uchar>(cv::Point(mm, n)) = tr;
                }
                else
                {
                    int tr = workImage1.at<short unsigned int>(cv::Point(m, n));
                    workImage1.at<short unsigned int>(cv::Point(m, n)) = workImage1.at<short unsigned int>(cv::Point(mm, n));
                    workImage1.at<short unsigned int>(cv::Point(mm, n)) = tr;
                }
            }
        }
        image = workImage1.clone();
    }
    cv::Mat data;
    int width = image.cols;
    int height = image.rows;
    int total = width * height;
    double halfWidth = 0.5 * (double)width;
    cv::threshold(image, workImage1, threshold, 255, cv::THRESH_BINARY);
    if (outputFiles)
    {
        outName = baseName;
        outName.append("_input.png");
        cv::imwrite(outName.c_str(), image);
        outName = baseName;
        outName.append("_mask.png");
        cv::imwrite(outName.c_str(), workImage1);
    }
    double nzFrac = (double)cv::countNonZero(workImage1) / (double)total;
    double reqFrac = ((double)hBarLength * hBarThickness) / (double)(total * 2.0);
    if (nzFrac < reqFrac)
    {
        oString.str("");
        oString.clear();
        oString << "Fraction of bright pixesls in image (" << nzFrac << ") is too small, must be at least (" << reqFrac << ")";
        errorMsg = oString.str();
        if (outputFiles)
        {
            logFile << errorMsg << std::endl;
            logFile.close();
        }
        std::cout << "Too few bright pixels" << std::endl;
        exit(0);

    }
    if (eightBit)
    {
        image.convertTo(data, CV_8U);
    }
    else
    {
        double minVal;
        double maxVal;
        cv::minMaxIdx(image, &minVal, &maxVal);
        if (maxVal <= 255)
            image.convertTo(data, CV_8UC1);
        else if (maxVal - minVal <= 255)
        {
            image = image - minVal;
            image.convertTo(data, CV_8UC1);
        }
        else
        {
            double scale = 255.0 / (maxVal - minVal);
            image = (image - minVal) * scale;
            image.convertTo(data, CV_8UC1);
        }
    }
    if (outputFiles)
    {
        outName = basePath + "_01.png";
        cv::imwrite(outName.c_str(), image);
    }

    logFile << "H/VBarThickness, " << hBarThickness << ", " << hBarLength << std::endl;
    logFile << "image.cols/rows, " << image.cols << ", " << image.rows << std::endl;
    double barArea = ((double)hBarThickness * hBarLength);
    double maxFrac = barArea / (image.total());
    image.release();

    double oT = cv::threshold(data, workImage1, threshold, 255, cv::THRESH_OTSU);
    cv::Mat hist; // (1, 256, CV_32SC1);
    int histSize = 256;
    float range[] = { 0, 256 };
    const float* histRange = { range };
    bool uniform = true;
    bool accumulate = false;
    cv::calcHist(&data, 1, 0, cv::Mat(), hist, 1, &histSize, &histRange, uniform, accumulate);

    float sum = 0;
    int oThreshold = (int)ceil(oT);
    if (oThreshold > 255 || oThreshold < 0)
    {
        oString.str("");
        oString.clear();
        oString << "Otsu thresholding failed";
        errorMsg = oString.str();
        if (outputFiles)
        {
            logFile << errorMsg << std::endl;
            logFile.close();
        }
        std::cout << "Otsu thersholding failed" << std::endl;
        exit(0);
    }
    for (int n = oThreshold; n < 256; n++)
    {
        sum += hist.at<float>(cv::Point(n, 1));
        //if (outputFiles)
        //    logFile << n << "," << sum << "," << hist.at<float>(cv::Point(n, 1)) << std::endl;
    }
    if (outputFiles)
        logFile << "sum/maxSize, " << sum << ", " << barArea << std::endl;
    if (sum > 1.2 * barArea)
    {
        float sum2 = 0;
        int newThreshold = 256;
        while (sum2 < 1.2 * barArea)
        {
            newThreshold--;
            if (newThreshold < 0)
                break;
            sum2 += hist.at<float>(newThreshold);
            if (outputFiles)
                logFile << "newThreshold/sum2, " << newThreshold << ", " << sum2 << "\n";
        }

        if (newThreshold > oThreshold)
        {
            cv::threshold(data, workImage1, ((double)newThreshold - 0.5), 255, cv::THRESH_BINARY);
            if (outputFiles)
                logFile << "Threshold increased from " << oThreshold << " (Otsu) to " << newThreshold << "\n";
        }
        else
        {
            if (outputFiles)
                logFile << "Otsu threshold = " << oThreshold << "(" << oT << ")\n";
        }
    }
    else
    {
        if (outputFiles)
            logFile << "Otsu threshold = " << oThreshold << "(" << oT << ")\n";
    }

    int v = height - 1;
    int h = width - 1;
    for (int n = 0; n < workImage1.cols; n++)
    {
        workImage1.at<uchar>(0, n) = 0;
        workImage1.at<uchar>(v, n) = 0;
    }
    for (int n = 0; n < workImage1.rows; n++)
    {
        workImage1.at<uchar>(n, 0) = 0;
        workImage1.at<uchar>(n, h) = 0;
    }
    workImage1.copyTo(data);

    // floodFill filss in area around wells
    cv::floodFill(data, cv::Point(0, 0), cv::Scalar(255));

    // Invert floodfilled image, this leaves non zero values for holes disconnected from edge
    cv::bitwise_not(data, data);

    // Combine the two images to get the closed foreground
    // cv::bitwise_or(WorkImage2, WorkImage3, Foreground8U );
    cv::Mat workImage2 = (workImage1 | data);
    cv::Mat mask;
    cv::Mat structElement = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(7, 7), cv::Point(-1, -1));
    cv::morphologyEx(workImage2, mask, cv::MORPH_OPEN, structElement, cv::Point(-1, -1), 2);
    if (outputFiles)
    {
        outName = basePath + "_02.png";
        cv::imwrite(outName.c_str(), data);
        outName = basePath + "work1.png";
        cv::imwrite(outName.c_str(), workImage1);
        outName = basePath + "_mask1.png";
        cv::imwrite(outName.c_str(), mask);
    }
    std::vector<std::vector<cv::Point> > trialContours;
    trialContours.clear();

    cv::Mat WorkImage2 = mask.clone();
    cv::findContours(WorkImage2, trialContours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE, cv::Point(0, 0));
    int nCont = (int)trialContours.size();
    if (nCont < 1)
    {
        if (outputFiles)
        {
            logFile << "No blobs found in image";
            logFile.close();
        }
        std::cout << "No blobs found in image" << std::endl;
        exit(-4);
    }
    int maxPixelCount = -1;
    int maxContour = -1;
    cv::Rect boundingR;
    for (int n = 0; n < nCont; n++)
    {
        cv::Rect bRect = cv::boundingRect(trialContours[n]);
        //std::cout << n << " bRect.w/h/x/y = " << bRect.width << "/" << bRect.height << "/" << bRect.x << "/" << bRect.y << std::endl;
        if (bRect.width > hBarLengthLowerLimit && bRect.width < hBarLengthUpperLimit
            && bRect.height > hBarThicknessLowerLimit && bRect.height < hBarThicknessUpperLimit)
        {
            cv::Moments mom;
            mom = moments(trialContours[n]);
            if (mom.m00 > maxPixelCount)
            {
                maxPixelCount = (int)mom.m00;
                maxContour = n;
                boundingR = bRect;
            }
        }
    }
    if (maxContour < 0)
    {
        if (outputFiles)
        {
            logFile << "No acceptable blob found in image";
            logFile.close();
        }
        std::cout << "No acceptable blob found in image" << std::endl;
        std::cout << "Length Limits: " << hBarLengthLowerLimit << " / " << hBarLengthUpperLimit << std::endl;
        std::cout << "Thickness Limits: " << hBarThicknessLowerLimit << " / " << hBarThicknessUpperLimit << std::endl;
        exit(-5);
    }

    cv::Mat linesH;
    std::vector< cv::Vec3d> line3dH;
    std::vector<cv::Point> pointList;
    int colUpperLimit = width - 6;
    int rowUpperLimit = height - 10;
    int rowLowerLimit = height / 2;
    for (int n = 0; n < trialContours[maxContour].size(); n++)
    {
        if (trialContours[maxContour][n].x > 5 && trialContours[maxContour][n].x < colUpperLimit
            && trialContours[maxContour][n].y > rowLowerLimit && trialContours[maxContour][n].y < rowUpperLimit)
        {
            pointList.push_back(trialContours[maxContour][n]);
        }
    }

    mask.setTo(0);
    for (int n = 0; n < pointList.size(); n++)
        mask.at<uchar>(pointList[n]) = 255;
    if (outputFiles)
    {
        outName = basePath + "_mask2.png";
        cv::imwrite(outName.c_str(), mask);
    }

    cv::HoughLinesPointSet(pointList, linesH, 50, 50, 10.0, (double)(height - 10.), rhoStep,
        PIDIVIDEDBYTWO - PIDIVIDEDBYSIX, PIDIVIDEDBYTWO + PIDIVIDEDBYSIX, thetaStep);
    //CV_PI * (1.0 / 2.0 - 1.0 / 6.0), CV_PI * (1.0 / 2.0 + 1.0 / 6.0), CV_PI / 180.);
    linesH.copyTo(line3dH);

    std::vector<HoughLines> horzHoughLines;
    horzHoughLines.clear();
    HoughLines tmpHL;

    for (int n = 0; n < line3dH.size(); n++)
    {
        tmpHL.votes = (int)line3dH[n].val[0];
        tmpHL.rho = line3dH[n].val[1];
        tmpHL.theta = line3dH[n].val[2];
        tmpHL.x0 = tmpHL.rho * cos(tmpHL.theta);
        tmpHL.y0 = tmpHL.rho * sin(tmpHL.theta);
        if (tmpHL.theta > 1.570796326794894 && tmpHL.theta < 1.570796326794899)
            tmpHL.YMidPoint = tmpHL.y0;
        else
            tmpHL.YMidPoint = tmpHL.y0 - (halfWidth - tmpHL.x0) / tan(tmpHL.theta);
        if (tmpHL.rho > height / 2 && tmpHL.votes > hBarLength / 4)
            horzHoughLines.push_back(tmpHL);
        oString.str("");
        oString.clear();
        oString << tmpHL.votes << "," << tmpHL.rho << "," << tmpHL.theta << "," << tmpHL.YMidPoint << "," << tmpHL.x0 << "," << tmpHL.x0;
        logOutput.push_back(oString.str());
    }
    HoughLines topHLine;
    HoughLines botHLine;
    bool foundHLines = false;
    if (horzHoughLines.size() > 1)
    {
        std::sort(horzHoughLines.begin(), horzHoughLines.end(), SortHoughLines);
        for (int m = 0; m < horzHoughLines.size() - 1; m++)
        {
            std::vector< HoughLines> altHLines;
            altHLines.clear();
            double baseTheta = horzHoughLines[m].theta;
            double baseYMid = horzHoughLines[m].YMidPoint;
            double deltaY = hBarThickness / sin(horzHoughLines[m].theta);
            for (int n = m + 1; n < horzHoughLines.size(); n++)
            {
                if (fabs(horzHoughLines[n].theta - baseTheta) < 2.0 * RADIANSPERDEGREE
                    && fabs(fabs(horzHoughLines[n].YMidPoint - baseYMid) - deltaY) < 10.0)
                {
                    altHLines.push_back(horzHoughLines[n]);
                }
            }
            if (altHLines.size() > 0)
            {
                double bWidthMax = (hBarLength * sin(baseTheta) + hBarThickness * cos(baseTheta)) * 1.10;
                double bWidthMin = bWidthMax * 0.70;
                if (boundingR.width > bWidthMin && boundingR.width < bWidthMax)
                {
                    std::sort(altHLines.begin(), altHLines.end(), SortHoughLines);
                    foundHLines = true;
                    if (horzHoughLines[m].rho > altHLines[0].rho)
                    {
                        topHLine = altHLines[0];
                        botHLine = horzHoughLines[m];
                    }
                    else
                    {
                        botHLine = altHLines[0];
                        topHLine = horzHoughLines[m];
                    }
                    break;
                }
            }
        }
    }
    std::string argNames[11] = { "exe", "Working Directory", "Image Name",
    "Align Threshold", "mm/Pixel", "Dist to Center", "H Bar Length",
    "H Bar Thickness", "Steps/Revolution", "Rotate Image?", "Base Name"};
    if (foundHLines)
    {
        double thetaBar = 0.5 * (topHLine.theta + botHLine.theta);
        rotAngle = PIDIVIDEDBYTWO - thetaBar;
        rotSteps = (rotAngle / TWOPI) * stepsPerRevolution;
        std::cout << std::setprecision(2) << std::fixed << rotSteps << std::endl;
        if (outputFiles)
        {
            logFile << rotSteps << "," << rotAngle << std::endl;
            logFile << "=====,Argv[]," << std::endl;
            for (int n = 0; n < Argc; n++)
            {
                logFile << argNames[n] << "," << Argv[n] << std::endl;
            }
            logFile << "Distances are in mm" << std::endl;
            logFile << "Rho Step," << rhoStep << ",pixels" << std::endl;
            logFile << "Theta Step," << thetaStep << ",radians" << std::endl;
            logFile << "image path," << inputImageName << std::endl;
            logFile << std::endl;

            logFile << "=====,Hough Line information horz)" << std::endl;
            for (int n = 0; n < logOutput.size(); n++)
                logFile << logOutput[n] << std::endl;
            logFile << std::endl;
            logFile.close();
        }
        exit(0);
    }
    else
    {
        std::cout << "Could not find all the edges" << std::endl;
        if (outputFiles)
        {
            logFile << "Could not find all the edges" << std::endl;
            logFile << "=====,Argv[]," << std::endl;
            for (int n = 0; n < Argc; n++)
            {
                logFile << argNames[n] << "," << Argv[n] << std::endl;
            }
            logFile << "Distances are in mm" << std::endl;
            logFile << "Rho Step," << rhoStep << ",pixels" << std::endl;
            logFile << "Theta Step," << thetaStep << ",radians" << std::endl;
            logFile << "image path," << inputImageName << std::endl;
            logFile << std::endl;

            logFile << "=====,Hough Line information horz)" << std::endl;
            for (int n = 0; n < logOutput.size(); n++)
                logFile << logOutput[n] << std::endl;
            logFile << std::endl;
            logFile.close();
        }
        exit(0);
    }
}