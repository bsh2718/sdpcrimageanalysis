REM sdpcrMasking.exe <Working Directory> <Template Directory> <Image Name> <Prefix> <FilterLabel> <SubImage Label>
REM     <Sizing BaseName> <Output BaseName> <Color Shift X> <Color Shift Y> <Dilation> <Transpose Image ? > <Use Median Filter ? > <Extra Output>
REM     For boolean values <Transpose Image?> and <Use Median Filter?> use 1 for true and 0 for false
REM     <Color Shift X> and <Color Shift Y> could depend on which LED is used to create the mask and which LED is used to take the image being analyzed
REM     It is hoped that both of these will turn to be zero. 
REM     <Extra Output> is usually 0, setting != 0 produces output for debugging purposes.

..\bin\sdpcrMasking.exe ..\00000154-XY2_test ..\templates 00000154-XY2_R1_J008_S1_L5_bef.png M1015K04 CY5_B 1 00000154-XY2_J007_B101_P0001 00000154-XY2_J008_B101_P0007 0 0 1 0 1 0
..\bin\sdpcrMasking.exe ..\00000154-XY2_test ..\templates 00000154-XY2_R1_J010_S2_L5_bef.png M1015K04 CY5_B 2 00000154-XY2_J009_B102_P0002 00000154-XY2_J010_B102_P0008 0 0 1 0 1 0
..\bin\sdpcrMasking.exe ..\00000154-XY2_test ..\templates 00000154-XY2_R1_J012_S3_L5_bef.png M1015K04 CY5_B 3 00000154-XY2_J011_B103_P0003 00000154-XY2_J012_B103_P0009 0 0 1 0 1 0
..\bin\sdpcrMasking.exe ..\00000154-XY2_test ..\templates 00000154-XY2_R1_J014_S1_L5_aft.png M1015K04 CY5_A 1 00000154-XY2_J013_A101_P0004 00000154-XY2_J014_A101_P0010 0 0 1 0 1 0
..\bin\sdpcrMasking.exe ..\00000154-XY2_test ..\templates 00000154-XY2_R1_J016_S2_L5_aft.png M1015K04 CY5_A 2 00000154-XY2_J015_A102_P0005 00000154-XY2_J016_A102_P0011 0 0 1 0 1 0
..\bin\sdpcrMasking.exe ..\00000154-XY2_test ..\templates 00000154-XY2_R1_J018_S3_L5_aft.png M1015K04 CY5_A 3 00000154-XY2_J017_A103_P0006 00000154-XY2_J018_A103_P0012 0 0 1 0 1 0

