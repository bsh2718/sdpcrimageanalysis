REM sdpcrSizing.exe <Working Directory> <Template Directory> <Image Name> <Prefix> <FilterLabel> <SubImage Label>
REM     <Output BaseName> <Transpose Image ? > <Use Median Filter ? > <Extra Output>
REM	For boolean values <Transpose Image?> and <Use Median Filter?> use 1 for true and 0 for false
REM	<Extra Output> is usually 0, setting != 0 produces output for debugging purposes.

..\bin\sdpcrSizing.exe ..\00000154-XY2_test ..\templates 00000154-XY2_R1_J007_S1_L1_bef.png M1015K04 ROX_B 1 00000154-XY2_J007_B101_P0001 0 1 0
..\bin\sdpcrSizing.exe ..\00000154-XY2_test ..\templates 00000154-XY2_R1_J009_S2_L1_bef.png M1015K04 ROX_B 2 00000154-XY2_J009_B102_P0002 0 1 0
..\bin\sdpcrSizing.exe ..\00000154-XY2_test ..\templates 00000154-XY2_R1_J011_S3_L1_bef.png M1015K04 ROX_B 3 00000154-XY2_J011_B103_P0003 0 1 0
..\bin\sdpcrSizing.exe ..\00000154-XY2_test ..\templates 00000154-XY2_R1_J013_S1_L1_aft.png M1015K04 ROX_A 1 00000154-XY2_J013_A101_P0004 0 1 0
..\bin\sdpcrSizing.exe ..\00000154-XY2_test ..\templates 00000154-XY2_R1_J015_S2_L1_aft.png M1015K04 ROX_A 2 00000154-XY2_J015_A102_P0005 0 1 0
..\bin\sdpcrSizing.exe ..\00000154-XY2_test ..\templates 00000154-XY2_R1_J017_S3_L1_aft.png M1015K04 ROX_A 3 00000154-XY2_J017_A103_P0006 0 1 0

