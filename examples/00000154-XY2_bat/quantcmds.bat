REM sdpcrQuant.exe <Working Directory> <Template Directory> <Quant Parameter File> <Prefix> <SubImage Label>
REM      <Output BaseName> <Before BaseName>
REM      <After BaseName 0 > <After Set Number 0> [[<After BaseName 1> <After Set Number 1>] ...]
REM
REM Example here is for one after image (end point analysis). In that case the set number is just set equal to 0 (it must be included)
REM For real time analysis, there will be more than one after image per before image.  In that case, they are listed in the order they
REM were taken with the last image (endpoint image) listed last.  Each after image must have a different set number and they should increase.

..\bin\sdpcrQuant.exe ..\00000154-XY2_test ..\templates M1015K04_CY5_Param.txt M1015K04 1 00000154-XY2_Q101_P0013 00000154-XY2_J008_B101_P0007 00000154-XY2_J014_A101_P0010 0 
..\bin\sdpcrQuant.exe ..\00000154-XY2_test ..\templates M1015K04_CY5_Param.txt M1015K04 2 00000154-XY2_Q102_P0014 00000154-XY2_J010_B102_P0008 00000154-XY2_J016_A102_P0011 0 
..\bin\sdpcrQuant.exe ..\00000154-XY2_test ..\templates M1015K04_CY5_Param.txt M1015K04 3 00000154-XY2_Q103_P0015 00000154-XY2_J012_B103_P0009 00000154-XY2_J018_A103_P0012 0 


