150.00		Rolling Ball Radius 
0.45		Background Fraction
0.20		Top Fraction (used for sizing)
# Set 0
3.00  4.50	Well Height Deltas (minus, plus)
4.00  6.50	Well Width Deltas (minus, plus)
0.40  1.90	Area Fraction (min, max)
0.60  1.10	Circularity (min, max)
0.70  3.00	Aspect Ratio (min, max)
# Set 1
3.00  4.00	Well Height Deltas (minus, plus)
4.00  5.00	Well Width Deltas (minus, plus)
0.50  1.65	Area Fraction (min, max)
0.60  1.10	Circularity (min, max)
0.80  2.45	Aspect Ratio (min, max)
# Set 2
3.00  5.00	Well Height Deltas (minus, plus)
4.00  6.00	Well Width Deltas (minus, plus)
0.45  1.65	Area Fraction (min, max)
0.60  1.10	Circularity (min, max)
0.70  2.60	Aspect Ratio (min, max)
# Set 3
3.00  4.50	Well Height Deltas (minus, plus)
4.00  6.50	Well Width Deltas (minus, plus)
0.40  1.90	Area Fraction (min, max)
0.60  1.10	Circularity (min, max)
0.70  3.00	Aspect Ratio (min, max)
