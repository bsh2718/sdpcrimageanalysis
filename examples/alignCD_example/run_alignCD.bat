REM alignCD <working directory> <image file name> <threshold> <mmPerPixel> <barWidth> <barHeight> <steps/revolution> <Rotate Image?> [<Base Name>]
alignCD c:\terminal\CDWells testMark_Rot0.png 600 0.01 8.0 2.0 3200 1 
alignCD c:\terminal\CDWells testMark_Rot1.png 600 0.01 8.0 2.0 3200 1 
alignCD c:\terminal\CDWells testMark_Rot2.png 600 0.01 8.0 2.0 3200 1 
alignCD c:\terminal\CDWells testMark_Rot3.png 600 0.01 8.0 2.0 3200 1 
alignCD c:\terminal\CDWells testMark_Rot4.png 600 0.01 8.0 2.0 3200 1 

REM If debugging output is desired, then append a base name as the last argument, so the first command might be
REM      alignCD c:\terminal\CDWells testMark_Rot0.png 600 0.01 8.0 2.0 3200 1 test0
REM
