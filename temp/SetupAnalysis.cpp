// SetupAnalysis.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

#include "SetupAnalysis.hpp"

int splitString(std::string inputString, char Delimiter, std::vector< std::string >& Tokens)
{
	Tokens.clear();
	std::string tok;
	std::stringstream s(inputString);
	while (getline(s, tok, Delimiter))
	{
		Tokens.push_back(tok);
	}
	return (int)Tokens.size();
}
int RotorChipSubIndex(ImageInfo Im)
{
	return Im.RotorPosition * 6 + Im.ChipPosition *3 + Im.SubImage - 7;
}
int RotorChipSubIndex(int Rotor, int Chip, int SubImage)
{
	return Rotor * 6 + Chip * 3 + SubImage;
}
bool DecodeRCSIndex(int RCSID, int &Rotor, int &Chip, int &SubImage)
{
	int t = (int)floor(RCSID /6);
	int c = (int)floor((RCSID - 6 * t)/3);
	int s = RCSID - 6 * t - 3 * c;
	if (t < 0 || c < 0 || s < 0 || t > 3 || c > 1 || s > 2)
		return false;
	Rotor = t + 1;
	Chip = c;
	SubImage = s + 1;
	return true;
}
std::string RotorChipSubLabel(ImageInfo Im, bool Before)
{
	std::ostringstream stream;
	stream.str(""); stream.clear();
	if (Before)
		stream << "B";
	else
		stream << "A";
	stream << Im.RotorPosition << Im.ChipPosition << Im.SubImage;
	return stream.str();
}
std::string RotorChipSubLabel(ImageInfo Im)
{
	std::ostringstream stream;
	stream.str(""); stream.clear();
	stream << "Q";
	stream << Im.RotorPosition << Im.ChipPosition << Im.SubImage;
	return stream.str();
}
int RCSSize = 24;
int ArrayRotorChipIndex(int Array, int Rotor, int Chip)
{
	return Array * 8 + Rotor * 2 + Chip;
}
bool DecodeARCIndex(int ARCIndex, int &Array, int &Rotor, int &Chip)
{
	int t = (int)floor(ARCIndex/8);
	int r = (int)floor(ARCIndex - t * 8/2)
	int c = ARCIndex - t * 8 - r * 2;
	if (t < 0 || c < 0 || r < 0 || c > 1 || r > 3)
		return false;
	Array = t + 1;
	Rotor = r + 1;
	Chip = c;
}
int ARCSize = 32;
bool SortIdxSetName(const IdxSetName& A, const IdxSetName& B)
{
	return A.SetNumber < B.SetNumber;
}

int main(int Argc, char *Argv[])
{
	if (Argc < 8)
	{
		std::cout << "Two Few parameters";
		exit(0);
	}
	std::string templatePath("c:\\Instrument\\Templates");
    std::string path("c:\\Instrument\\Very\\SadFish");
    std::string imageLogName("PathologicalPuppies.csv");
    std::string outputBaseName("HomicidalGuppies");
    std::string sizingDye("KillerKitten");
    std::vector< std::string > quantDyes;
    quantDyes.clear();
	int arraysPerChip = 4;
	int numbRotorPositions = 4;
	int numbChipsPerPosition = 2;
	int ia = 0;
	int ie = 0;
	std::istringstream token;
	{
		// std::cout << "Before istringstream\n" << std::endl;
		// double temp;
		int itemp;
		std::string stemp;
		token.clear(); token.str(Argv[++ia]); token >> itemp; if(token.fail()){++ie;} else {if ( itemp > 0) arraysPerChip = itemp ; else t++ie; }
		templatePath.assign(Argv[++ia]);
		path.assign(Argv[++ia]);
		imageLogName.assign(Argv[++ia]);
		outputBaseName.assign(Argv[++ia]);
		sizingDye.assign(Argv[++ia]);
		for (int n = ia + 1; n < Argc; n++)
			quantDyes.push_back(Argv[n]);
	}
	if ( ie > 0 )
	
	std::string logName = path + "\\" + imageLogName;
    std::cout << "logName: " << logName << "\n";

	std::ifstream imageLog;
	std::ofstream outputLog;
	std::ofstream analysisScript;

	std::string outputName = path + "\\" + outputBaseName + ".txt";
	std::string scriptName = path + "\\" + outputBaseName + ".csv";
	std::cout << "scriptName: " << scriptName << "\n";
	imageLog.open(logName.c_str());
	if (!imageLog.is_open())
	{
		std::cout << "could not open " << logName << "\n";
		exit(0);
	}
	std::string dateLine;
	std::string projectLine;
	std::string headerLine;
	std::vector< std::string > imageLines;
	imageLines.clear();
	getline(imageLog, dateLine);
	getline(imageLog, projectLine);
	getline(imageLog, headerLine);
	if (imageLog.bad())
	{
		std::cout << "problems reading header information from " << logName << "\n";
		exit(0);
	}
	if (imageLog.eof())
	{
		std::cout << logName << " has no image data in it\n";
		exit(0);
	}
	while (!imageLog.eof())
	{
		std::string line;
		getline(imageLog, line);
		imageLines.push_back(line);
	}
	std::vector< std::string > headTokens;
	int headCount = splitString(headerLine, ',', headTokens);
	if (headCount < 15)
	{
		std::cout << "Header line has too few entries\n";
		exit(0);
	}
	int nameN = -1;
	int timeN = -1;
	int imageN = -1;
	int rotorN = -1;
	int chipN = -1;
	int subN = -1;
	int deviceN = -1;
	int dyeN = -1;
	int ledN = -1;
	int filterN = -1;
	int gainN = -1;
	int exposureN = -1;
	int setTypeN = -1;
	int setNumberN = -1;

	for (int n = 0; n < headTokens.size(); n++)
	{
		if (headTokens[n].find("File Name") != std::string::npos)
			nameN = n;
		else if (headTokens[n].find("Time") != std::string::npos)
			timeN = n;
		else if (headTokens[n].find("Image No") != std::string::npos)
			imageN = n;
		else if (headTokens[n].find("Rotor") != std::string::npos)
			rotorN = n;
		else if (headTokens[n].find("Chip") != std::string::npos)
			chipN = n;
		else if (headTokens[n].find("SubImage") != std::string::npos)
			subN = n;
		else if (headTokens[n].find("Device") != std::string::npos)
			deviceN = n;
		else if (headTokens[n].find("Dye") != std::string::npos)
			dyeN = n;
		else if (headTokens[n].find("LED") != std::string::npos)
			ledN = n;
		else if (headTokens[n].find("EmFilter") != std::string::npos)
			filterN = n;
		else if (headTokens[n].find("Gain") != std::string::npos)
			gainN = n;
		else if (headTokens[n].find("Exposure") != std::string::npos)
			exposureN = n;
		else if (headTokens[n].find("Set Type") != std::string::npos)
			setTypeN = n;
		else if (headTokens[n].find("Set No") != std::string::npos)
			setNumberN = n;
	}
	if (nameN < 0 || timeN < 0 || imageN < 0 || rotorN < 0 || chipN < 0 || subN < 0 || deviceN < 0 || dyeN < 0
		|| ledN < 0 || filterN < 0 || gainN < 0 || exposureN < 0 || setTypeN < 0 || setNumberN < 0)
	{
		std::cout << "Header line is missing one or more entries\n";
		exit(0);
	}

	ImageInfo newInfo;
	newInfo.SizingBaseName = "";
	newInfo.MaskingBaseName = "";
	newInfo.AType = AnalysisType::None;
	newInfo.QuantID = -1;

	std::vector< ImageInfo > beforeImages;
	beforeImages.clear();
	std::vector< ImageInfo > afterImages;
	afterImages.clear();
	std::istringstream tok;
	for (int n = 0; n < imageLines.size(); n++)
	{
		std::vector< std::string > imageTokens;
		int imageCount = splitString(imageLines[n], ',', imageTokens);
		if (imageCount > 14)
		{
			newInfo.Name = imageTokens[nameN];
			newInfo.Time = imageTokens[timeN];
			tok.clear(); tok.str(imageTokens[imageN]); tok >> newInfo.ImageNumber;
			tok.clear(); tok.str(imageTokens[rotorN]); tok >> newInfo.RotorPosition;
			if (imageTokens[chipN].find("Left") != std::string::npos)
				newInfo.ChipPosition = 0;
			else
				newInfo.ChipPosition = 1;
			tok.clear(); tok.str(imageTokens[subN]); tok >> newInfo.SubImage;
			newInfo.Device = imageTokens[deviceN];
			newInfo.Dye = imageTokens[dyeN];
			if (imageTokens[setTypeN].find("Before") != std::string::npos)
				newInfo.Type = 0;
			else
				newInfo.Type = 1;
			tok.clear(); tok.str(imageTokens[setNumberN]); tok >> newInfo.SetNumber;

			if (newInfo.RotorPosition > 0 && newInfo.RotorPosition < 5 &&
				newInfo.SubImage > 0 && newInfo.SubImage < 4)
			{
				newInfo.RCSIndex = RotorChipSubIndex(newInfo);
				if (newInfo.Type == 0)
					beforeImages.push_back(newInfo);
				else
					afterImages.push_back(newInfo);
			}
		}
		else if (imageLines[n].length() > 10)
		{
			std::cout << "too few entries on line " << n << "\n";
			std::cout << "     " << imageLines[n] << "\n";
		}

	}
	int maxBeforeSet = -1;
	int maxAfterSet = -1;
	std::vector< int > afterSets;
	afterSets.clear();
	std::cout << "#before/after: " << beforeImages.size() << "/" << afterImages.size() << "\n";
	std::cout << "Before Images\n";
	for (int n = 0; n < beforeImages.size(); n++)
	{
		std::cout << beforeImages[n].Name << "/" << beforeImages[n].ImageNumber << "/"
			<< beforeImages[n].RotorPosition << "/" << beforeImages[n].ChipPosition << "/"
			<< beforeImages[n].SubImage << "/" << beforeImages[n].Device << "/"
			<< beforeImages[n].Dye << "/" << beforeImages[n].Type << "/"
			<< beforeImages[n].SetNumber << "/" << beforeImages[n].Time << "\n";
		if (beforeImages[n].SetNumber > maxBeforeSet)
			maxBeforeSet = beforeImages[n].SetNumber;
	}
	std::cout << "\nAfter Images\n";
	for (int n = 0; n < afterImages.size(); n++)
	{
		std::cout << afterImages[n].Name << "/" << afterImages[n].ImageNumber << "/"
			<< afterImages[n].RotorPosition << "/" << afterImages[n].ChipPosition << "/"
			<< afterImages[n].SubImage << "/" << afterImages[n].Device << "/"
			<< afterImages[n].Dye << "/" << afterImages[n].Type << "/"
			<< afterImages[n].SetNumber << "/" << afterImages[n].Time << "\n";
		if (afterSets.size() < 1)
			afterSets.push_back(afterImages[n].SetNumber);
		else
		{
			int nn = afterImages[n].SetNumber;
			for (int m = 0; m < afterSets.size(); m++)
			{
				if (afterSets[m] == nn)
				{
					nn = -1;
					break;
				}
			}
			if (nn >= 0)
				afterSets.push_back(nn);
		}
		if (afterImages[n].SetNumber > maxAfterSet)
			maxAfterSet = afterImages[n].SetNumber;
	}
	sort(afterSets.begin(), afterSets.end());
	int numbAfterSets = maxAfterSet + 1;
	int numbQuantDyes = (int)quantDyes.size();
	std::vector< int > sizingBeforeRotorChipSub;
	sizingBeforeRotorChipSub.resize(24);
	std::vector< std::vector< int > > maskingBeforeRotorChipSub;
	std::vector< std::vector< int > > sizingAfterRotorChipSub;
	std::vector< std::vector< std::vector< int > > > maskingAfterRotorChipSub;
	std::fill( sizingBeforeRotorChipSub.begin(), sizingBeforeRotorChipSub.end(), -1 );
	maskingBeforeRotorChipSub.resize(numbQuantDyes);
	for (int n = 0; n < numbQuantDyes; n++)
	{
		maskingBeforeRotorChipSub[n].resize(24);
		std::fill(maskingBeforeRotorChipSub[n].begin(), maskingBeforeRotorChipSub[n].end(), -1);
	}
	sizingAfterRotorChipSub.resize(numbAfterSets);
	maskingAfterRotorChipSub.resize(numbAfterSets);
	for (int n = 0; n < numbAfterSets; n++)
	{
		sizingAfterRotorChipSub[n].resize(24);
		std::fill(sizingAfterRotorChipSub[n].begin(), sizingAfterRotorChipSub[n].end(), -1);
		maskingAfterRotorChipSub[n].resize(numbQuantDyes);
		for (int m = 0; m < numbQuantDyes; m++)
		{
			maskingAfterRotorChipSub[n][m].resize(24);
			std::fill(maskingAfterRotorChipSub[n][m].begin(), maskingAfterRotorChipSub[n][m].end(), -1);
		}
	}
	for (int n = 0; n < beforeImages.size(); n++)
	{
		if (beforeImages[n].SetNumber == maxBeforeSet)
		{
			if (beforeImages[n].Dye.find(sizingDye) != std::string::npos)
			{
				beforeImages[n].AType = AnalysisType::Sizing;
				sizingBeforeRotorChipSub[beforeImages[n].RCSIndex] = n;

			}
			for (int m = 0; m < quantDyes.size(); m++)
			{
				if (beforeImages[n].Dye.find(quantDyes[m]) != std::string::npos)
				{
					if (beforeImages[n].AType == AnalysisType::Sizing)
						beforeImages[n].AType = AnalysisType::Both;
					else
						beforeImages[n].AType = AnalysisType::Masking;
					maskingBeforeRotorChipSub[m][beforeImages[n].RCSIndex] = n;
				}
			}
		}
	}

	for (int n = 0; n < afterImages.size(); n++)
	{
		if (afterImages[n].Dye.find(sizingDye) != std::string::npos)
		{
			afterImages[n].AType = AnalysisType::Sizing;
			sizingAfterRotorChipSub[afterImages[n].SetNumber][afterImages[n].RCSIndex] = n;
		}
		for (int m = 0; m < quantDyes.size(); m++)
		{
			if (afterImages[n].Dye.find(quantDyes[m]) != std::string::npos)
			{
				if (afterImages[n].AType == AnalysisType::Sizing)
					afterImages[n].AType = AnalysisType::Both;
				else
					afterImages[n].AType = AnalysisType::Masking;
				maskingAfterRotorChipSub[m][afterImages[n].SetNumber][afterImages[n].RCSIndex] = n;
			}
		}
	}
	int id;
	for (int n = 0; n < beforeImages.size(); n++)
	{
		int rcsID = beforeImages[n].RCSIndex;
		if (beforeImages[n].AType == AnalysisType::Masking)
		{
			if (sizingBeforeRotorChipSub[rcsID] < 0)
			{
				beforeImages[n].AType = AnalysisType::None;
				for (int m = 0; m < numbQuantDyes; m++)
				{
					maskingBeforeRotorChipSub[m][rcsID] = -1;
				
					for (int k = 0; k < numbAfterSets; k++)
					{
						id = maskingAfterRotorChipSub[m][k][rcsID];
						afterImages[id].AType = AnalysisType::None;
						maskingAfterRotorChipSub[m][k][rcsID] = -1;
					}
				}
				for (int k = 0; k < numbAfterSets; k++)
				{
					id = sizingAfterRotorChipSub[k][rcsID];
					afterImages[id].AType = AnalysisType::None;
					sizingAfterRotorChipSub[k][rcsID] = -1;
				}
			}
		}
	}
	SizingParams newSizing;
	MaskingParams newMasking;
	QuantParams newQuant;
	QuantCombineParams newCombine;
	std::vector< SizingParams > sizingList;
	std::vector< MaskingParams > maskingList;
	std::vector< QuantParams > quantList;
	std::vector< QuantCombineParams > combineList;
	sizingList.clear();
	maskingList.clear();
	quantList.clear();
	combineList.clear();

	std::ostringstream wStream;
	int programCounter = 0;
	for (int n = 0; n < beforeImages.size(); n++)
	{
		if (beforeImages[n].AType == AnalysisType::Sizing || beforeImages[n].AType == AnalysisType::Both)
		{
			newSizing.WorkingDirectory = path;
			newSizing.TemplateDirectory = templatePath;
			newSizing.ImageFileName = beforeImages[n].Name;
			newSizing.TemplatePrefix = beforeImages[n].Device;
			wStream.str(""); wStream.clear();
			wStream << beforeImages[n].Dye << "_B";
			newSizing.FilterLabel = wStream.str();
			wStream.str(""); wStream.clear();
			wStream << beforeImages[n].SubImage;
			newSizing.SubImageLabel = wStream.str();
			wStream.str(""); wStream.clear();
			programCounter++;
			wStream << outputBaseName << "_J" << beforeImages[n].ImageNumber 
				<< RotorChipSubLabel(beforeImages[n], true) << "_P" << programCounter;
			newSizing.BaseName = wStream.str();
			newSizing.Before = true;
			newSizing.Index = n;
			sizingList.push_back(newSizing);

			beforeImages[n].SizingBaseName = newSizing.BaseName;
		}
	}
	for (int n = 0; n < beforeImages.size(); n++)
	{
		if (beforeImages[n].AType == AnalysisType::Masking)
		{
			int rcsID = beforeImages[n].RCSIndex;
			bool foundSizing = false;
			for (int m = 0; m < beforeImages.size(); m++)
			{
				if (beforeImages[m].RCSIndex == rcsID
					&& (beforeImages[m].AType == AnalysisType::Both || beforeImages[m].AType == AnalysisType::Sizing))
				{
					newMasking.InputBaseName = beforeImages[m].SizingBaseName;
					foundSizing = true;
				}
			}
			if (foundSizing)
			{
				newMasking.WorkingDirectory = path;
				newMasking.TemplateDirectory = templatePath;
				newMasking.ImageFileName = beforeImages[n].Name;
				newMasking.TemplatePrefix = beforeImages[n].Device;
				wStream.str(""); wStream.clear();
				wStream << beforeImages[n].Dye << "_B";
				newMasking.FilterLabel = wStream.str();
				wStream.str(""); wStream.clear();
				wStream << beforeImages[n].SubImage;
				newMasking.SubImageLabel = wStream.str();
				wStream.str(""); wStream.clear();
				programCounter++;
				wStream << outputBaseName << "_J" << beforeImages[n].ImageNumber 
					<< RotorChipSubLabel(beforeImages[n], true) << "_P" << programCounter;
				newMasking.BaseName = wStream.str();
				newMasking.Before = true;
				newMasking.Index = n;
				maskingList.push_back(newMasking);
				beforeImages[n].SizingBaseName = newMasking.InputBaseName;
				beforeImages[n].MaskingBaseName = newMasking.BaseName;
			}
		}
	}
	for (int n = 0; n < afterImages.size(); n++)
	{
		if (afterImages[n].AType == AnalysisType::Sizing || afterImages[n].AType == AnalysisType::Both)
		{

			newSizing.WorkingDirectory = path;
			newSizing.TemplateDirectory = templatePath;
			newSizing.ImageFileName = afterImages[n].Name;
			newSizing.TemplatePrefix = afterImages[n].Device;
			wStream.str(""); wStream.clear();
			wStream << afterImages[n].Dye << "_A";
			newSizing.FilterLabel = wStream.str();
			wStream.str(""); wStream.clear();
			wStream << afterImages[n].SubImage;
			newSizing.SubImageLabel = wStream.str();
			wStream.str(""); wStream.clear();
			programCounter++;
			wStream << outputBaseName << "_J" << afterImages[n].ImageNumber 
				<< RotorChipSubLabel(afterImages[n], false) << "_P" << programCounter;
			newSizing.BaseName = wStream.str();
			newSizing.Before = false;
			newSizing.Index = n;
			sizingList.push_back(newSizing);
			afterImages[n].SizingBaseName = newSizing.BaseName;
		}
	}
	for (int n = 0; n < afterImages.size(); n++)
	{
		if (afterImages[n].AType == AnalysisType::Masking)
		{
			int rcsID = afterImages[n].RCSIndex;
			bool foundSizing = false;
			for (int m = 0; m < afterImages.size(); m++)
			{
				if (afterImages[m].RCSIndex == rcsID
					&& (afterImages[m].AType == AnalysisType::Both || afterImages[m].AType == AnalysisType::Sizing))
				{
					newMasking.InputBaseName = afterImages[m].SizingBaseName;
					foundSizing = true;
				}
			}
			if (foundSizing)
			{
				newMasking.WorkingDirectory = path;
				newMasking.TemplateDirectory = templatePath;
				newMasking.ImageFileName = afterImages[n].Name;
				newMasking.TemplatePrefix = afterImages[n].Device;
				wStream.str(""); wStream.clear();
				wStream << afterImages[n].Dye << "_A";
				newMasking.FilterLabel = wStream.str();
				wStream.str(""); wStream.clear();
				wStream << afterImages[n].SubImage;
				newMasking.SubImageLabel = wStream.str();
				wStream.str(""); wStream.clear();
				programCounter++;
				wStream << outputBaseName << "_J" << afterImages[n].ImageNumber 
					<< RotorChipSubLabel(afterImages[n], false) << "_P" << programCounter;
				newMasking.BaseName = wStream.str();
				newMasking.Before = false;
				newMasking.Index = n;

				maskingList.push_back(newMasking);
				afterImages[n].SizingBaseName = newMasking.InputBaseName;
				afterImages[n].MaskingBaseName = newMasking.BaseName;
			}
		}
	}
	analysisScript.open(scriptName.c_str());
	if (!analysisScript.is_open())
	{
		std::cout << "Could not open " << scriptName << "\n";
	}
	
	std::cout << "\n\n Sizing Analysis Parameters\n";
	analysisScript << sizingList[0].Header();
	for (int n = 0; n < sizingList.size(); n++)
	{
		std::cout << sizingList[n].ToString() << "\n";
		analysisScript << "Sizing," << sizingList[n].ToString() << "\n";
	}
	std::cout << "==========\n Masking Analysis Parameters\n";
	analysisScript << maskingList[0].Header();
	for (int n = 0; n < maskingList.size(); n++)
	{
		std::cout << maskingList[n].ToString() << "\n";
		analysisScript << "Masking," << maskingList[n].ToString() << "\n";
	}

	for (int n = 0; n < beforeImages.size(); n++)
	{
		if (beforeImages[n].AType == AnalysisType::Both || beforeImages[n].AType == AnalysisType::Masking)
		{
			newQuant.RCSIndex = RotorChipSubIndex(beforeImages[n]);
			int r;
			int c;
			int s;
			DecodeRCSIndex(newQuant.RCSIndex, r, c, s);
			newQuant.WorkingDirectory = path;
			newQuant.TemplateDirectory = templatePath;
			newQuant.ParameterFile = beforeImages[n].Device + "_" + beforeImages[n].Dye + "_Params.txt";
			newQuant.Prefix = beforeImages[n].Device;
			newQuant.Rotor = r;
			newQuant.Chip = c;
			wStream.str(""); wStream.clear();
			wStream << beforeImages[n].SubImage;
			newQuant.SubImageLabel = wStream.str();
			newQuant.Dye = beforeImages[n].Dye;
			if (beforeImages[n].AType == AnalysisType::Both)
				newQuant.BaseNameInput = beforeImages[n].SizingBaseName;
			else
				newQuant.BaseNameInput = beforeImages[n].MaskingBaseName;
			newQuant.BaseNameInputID = n;
			programCounter++;
			wStream << outputBaseName << "_J" << beforeImages[n].ImageNumber
				<< RotorChipSubLabel(beforeImages[n]) << "_P" << programCounter;
			newQuant.BaseNameOutput = wStream.str();

			quantList.push_back(newQuant);
		}
	}
	IdxSetName isn;
	for (int n = 0; n < afterImages.size(); n++)
	{
		if (afterImages[n].AType == AnalysisType::Both || afterImages[n].AType == AnalysisType::Masking)
		{
			for (int m = 0; m < quantList.size(); m++)
			{
				if (quantList[m].RCSIndex == afterImages[n].RCSIndex && quantList[m].Dye == afterImages[n].Dye)
				{
					isn.Index = n;
					isn.SetNumber = afterImages[n].SetNumber;
					if (beforeImages[n].AType == AnalysisType::Both)
						isn.Name = afterImages[n].SizingBaseName;
					else
						isn.Name = afterImages[n].MaskingBaseName;
					quantList[m].AfterBaseNames.push_back(isn);
	
					break;
				}
			}
		}
	}
	for (int n = 0; n < quantList.size(); n++)
	{
		if (quantList[n].AfterBaseNames.size() > 1)
			sort(quantList[n].AfterBaseNames.begin(), quantList[n].AfterBaseNames.end(), SortIdxSetName);
	}
	std::cout << "==========\n Quant Analysis Parameters\n";
	analysisScript << quantList[0].Header();
	for (int n = 0; n < quantList.size(); n++)
	{
		std::cout << quantList[n].ToString(true) << "\n";
		analysisScript << "Quant," << quantList[n].ToString(false) << "\n";
	}
	int qSize = numbAfterSets * numbRotorPositions * numbChipsPerPosition;
	
	for ( int n = 0 ; n < quantList.size() ; n++)
	{
		
	}
//<BaseNameOutput>_<SetNumber>_210_<Array>_<Section>_Bef_Aft.csv


	analysisScript.close();
}
