#ifndef SETUPANALYSIS_H
#define SETUPANALYSIS_H

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

enum class AnalysisType {Sizing, Masking, Both, None};
class ImageInfo
{
	public:
		int RCSIndex;
		std::string Name;
		//std::string Date;
		std::string Time;
		int ImageNumber;
		int RotorPosition;
		int ChipPosition;
		int SubImage;
		std::string Device;
		std::string Dye;
		//int LED;
		//int EmissionFilter;
		//int Gain;
		//int Exposure;
		int Type;
		int SetNumber;
		AnalysisType AType;
		std::string SizingBaseName;
		std::string MaskingBaseName;
		int QuantID;
		std::string QuantName;
		bool Result;

		ImageInfo() : RCSIndex(-1), Name("None"), Time("None"), ImageNumber(0),
			RotorPosition(0), ChipPosition(-1), SubImage(0), Device("None"), Dye("None"),
			Type(-1), SetNumber(-1), AType(AnalysisType::None), SizingBaseName("None"),
			QuantID(-1), QuantName("None"), Result(false) {}
		~ImageInfo() {}
};

class SizingParams
{
public:
	std::string WorkingDirectory;
	std::string TemplateDirectory;
	std::string ImageFileName;
	std::string TemplatePrefix;
	std::string FilterLabel;
	std::string SubImageLabel;
	std::string BaseName;
	bool TransposeImage;
	bool UseMedianFilter;
	unsigned int ExtraOutput;
	bool Before;
	int Index;

	SizingParams():TransposeImage(false), UseMedianFilter(true), ExtraOutput(0), Before(true) {}
	~SizingParams() {}

	std::string ToString()
	{
		std::ostringstream stream;
		stream.str(""); stream.clear();
		stream << WorkingDirectory << ","
			<< TemplateDirectory << ","
			<< ImageFileName << ","
			<< TemplatePrefix << ","
			<< FilterLabel << ","
			<< SubImageLabel << ","
			<< BaseName ;
		if (TransposeImage)
			stream << ",1";
		else
			stream << ",0";
		if (UseMedianFilter)
			stream << ",1";
		else
			stream << ",0";
		stream << "," << ExtraOutput;
		if (Before)
			stream << ",Before";
		else
			stream << ",After";

		return stream.str();
	}
	std::string Header()
	{
		return "WkDir,TmplDir,ImageName,TmplPrefix,FilterLbl,SubImageLbl,BaseName,Transpose?,MedianFilter?,ExtraOuput?,Bef/Aft";
	}
};
class MaskingParams
{
	public:
		std::string WorkingDirectory;
		std::string TemplateDirectory;
		std::string ImageFileName;
		std::string TemplatePrefix;
		std::string FilterLabel;
		std::string SubImageLabel;
		std::string InputBaseName;
		std::string BaseName;
		int ColorShiftX;
		int ColorShiftY;
		int Dilation;
		bool TransposeImage;
		bool UseMedianFilter;
		unsigned int ExtraOutput;
		bool Before;
		int Index;

		MaskingParams(): WorkingDirectory("None"), TemplateDirectory("None"), ImageFileName("None"),
			TemplatePrefix("None"), FilterLabel("None"), SubImageLabel("None"), InputBaseName("None"), 
			BaseName("None"), ColorShiftX(0), ColorShiftY(0), Dilation(1),
			TransposeImage(false), UseMedianFilter(true), ExtraOutput(0), Before(true), Index(-1){}
		~MaskingParams() {}

		std::string ToString()
		{
			std::ostringstream stream;
			stream.str(""); stream.clear();
			stream << WorkingDirectory << ","
				<< TemplateDirectory << ","
				<< ImageFileName << ","
				<< TemplatePrefix << ","
				<< FilterLabel << ","
				<< SubImageLabel << ","
				<< InputBaseName << ","
				<< BaseName << ","
				<< ColorShiftX << ","
				<< ColorShiftY << ","
				<< Dilation;
			if (TransposeImage)
				stream << ",1";
			else
				stream << ",0";
			if (UseMedianFilter)
				stream << ",1";
			else
				stream << ",0";
			stream << "," << ExtraOutput;
			if (Before)
				stream << ",Before";
			else
				stream << ",After";
			return stream.str();
		}
		std::string Header()
		{
			return 
			"WkDir,TmplDir,ImageName,TmplPrefix,FilterLbl,SubImageLbl,InputBaseName,BaseName,ColShiftX,ColShiftY,Dilation,Transpose?,MedianFilter?,ExtraOuput?,Bef/Aft";
		}
};
struct IdxSetName
{
	int Index;
	int SetNumber;
	std::string Name;
};

class QuantParams
{
	public:
		int RCSIndex;
		std::string WorkingDirectory;
		std::string TemplateDirectory;
		std::string ParameterFile;
		std::string Prefix;
		int Rotor;
		int Chip;
		std::string SubImageLabel;
		std::string Dye;
		std::string BaseNameOutput;
		std::string BaseNameInput;
		int BaseNameInputID;
		std::vector< IdxSetName > AfterBaseNames;

		QuantParams(): RCSIndex(-1), WorkingDirectory("None"), TemplateDirectory("None"), ParameterFile("None"),
			Prefix("None"), Rotor(0), Chip(-1), SubImageLabel("None"), Dye("None"), BaseNameOutput("None"), BaseNameInput("None"),
			BaseNameInputID(-1)
		{ AfterBaseNames.clear(); }
		~QuantParams() {}

		std::string ToString(bool Full)
		{
			std::ostringstream stream;
			stream.str(""); stream.clear();
			stream << WorkingDirectory << ","
				<< TemplateDirectory << ","
				<< ParameterFile << ","
				<< Prefix << ","
				<< Rotor << ","
				<< Chip << ","
				<< SubImageLabel << ","
				<< BaseNameOutput << ",";
			if (Full)
				stream << BaseNameInputID << ",";
			stream << BaseNameInput;
			for (int n = 0; n < AfterBaseNames.size(); n++)
			{
				if (Full)
				{
					stream << "," << AfterBaseNames[n].Index << ","
						<< AfterBaseNames[n].SetNumber << "," << AfterBaseNames[n].Name;
				}
				else
					stream << "," << AfterBaseNames[n].Name;
			}
			return stream.str();
		}
		std::string Header(bool Full)
		{
			if ( Full)
				return "WkDir,TmplDir,ParamFile,Prefix,Rotor,Chip,SubImageLbl,BaseName,BefBaseNameIdx,BefBaseName,AftBaseNameIdx,AfterSet,AftBaseName,[repeats if more than one After Image]";
			else
				return "WkDir,TmplDir,ParamFile,Prefix,Rotor,Chip,SubImageLbl,BaseName,BefBaseName,AftBaseName,[repeats if more than one After Image]";
		}
};
struct QuantItem
{
	std::string Name;
	std::string BeforeBaseName;
	std::vector< std::string > AfterBaseNames;
	int BeforeBaseNameID;
	std::vector< int > AfterBaseNameIDs;
};
class QuantCombineParams
{
	public:
		int ARCIndex;
		int Rotor;
		int Chip;
		int Array;
		std::string WorkingDirectory;
		std::string BaseName;
		std::vector< std::string > AfterNames;

		QuantCombineParams(): ARCIndex(-1), Rotor(0), Chip(-1), Array(0), WorkingDirectory("None"), BaseName("None") { AfterNames.clear(); }
		~QuantCombineParams() {}

		std::string ToString()
		{
			std::ostringstream stream;
			stream.str(""); stream.clear();
			stream << Rotor << "," << Chip << "," << Array << ","
				<< WorkingDirectory << ","
				<< BaseName ;
			for (int n = 0; n < AfterNames.size(); n++)
				stream << "," << AfterNames[n];

			return stream.str();
		}
		std::string Header()
		{
			return "Rotor,Chip,Array,WkDir,BaseName,AfterNames,[AfterNames]";
		}
};

class IndexThree
{
	
};
//_0_210_2_3_Bef_Aft
//<BaseNameOutput>_<SetNumber>_210_<Array>_<Section>_Bef_Aft.csv
#endif // SETUPANALYSIS_H