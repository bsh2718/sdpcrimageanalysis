/////////////////////////////////////////////////////////////////////////////////////////
//
// sdpcrMultiQuant
//
// Program takes the ouput from sdpcrQuant for multiple images taken of different
// subsections of an array.  It combines the results to produce an overall concentration
// based on the entire array.  (sdpcrQuant treats each subsection like a separate sample.)
// 
// The arguments on the command line are (string arguments cannot contain spaces)
//
// <Err > is a reference to std::string which contains any error message when the function returns.
//
// <Working Directory> Directory where input files are located and where output files will be 
//     placed.
//
// <baseName> - All of the output files (except for std::clog) start with baseName
//
// <afterNames> - std::vector of std::string, each element is the BeforeAfter output file
//     of one subsection of an array.  The program will combine them into one output file
//     This program isn't necessary for chips which have only one subsection.
//		There are four files with information about the Before Image

//
//! [includes]
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "physicalconstants.h"
#include "statistics.hpp"

#include "droplet.hpp"
#include "calcConcentration.hpp"
#include "trimDirectoryString.hpp"
#include "types.hpp"

//! [includes]

//! [namespace]
// using namespace std;
//using namespace cv;
//! [namespace]
	

bool ReadAfterBeforeFile(std::string AfterFileName, std::vector<Droplet>& DropletList,
	int& Array, std::string& SectionList, int& NumbOccupied, Statistics1& FillingFracStats, Statistics1& ShrinkageStats,
	std::string& ErrorMsg);

void ShowUsage(const char* msg, const char* exe);

int main(int Argc, char* Argv[])
{
	std::string Err = "";
	bool track = true;
	//! [load]
	//  defaults
	int numbRequiredArgs = 5;
	if (Argc < numbRequiredArgs)
	{
		ShowUsage("Too few arguments", Argv[0]);
		exit(0);
	}

	std::string workingDirectory;
	std::string baseName;
	std::vector< std::string > afterNames;
	afterNames.clear();

	int ia = 0;
	int ie = 0;
	std::istringstream token;
	{
		// double temp;
		//int itemp;
		std::string stemp;
		workingDirectory.assign(Argv[++ia]);
		baseName.assign(Argv[++ia]);
		
		while (ia < Argc - 1)
		{
			afterNames.push_back(Argv[++ia]);
		}
	}

	workingDirectory = trimDirectoryString(workingDirectory);
	std::string outputBaseName = workingDirectory + "\\" + baseName;

	std::vector< std::string > afterFileNames;
	if (afterNames.size() < 1)
	{
		Err = "AfterNames is empty. There must be at least two names in AfterNames";
		return -1;
	}
	if (afterNames.size() == 1)
	{
		Err = "AfterNames has only one entry. There must be at least two names in AfterNames";
		return -1;
	}
	afterFileNames.resize(afterNames.size());
	for (int n = 0; n < afterNames.size(); n++)
	{
		afterFileNames[n] = workingDirectory + "\\" + afterNames[n];
	}

	cv::Vec3b cValue;
	std::ostringstream oString;
	std::ofstream logFile;
	std::ofstream shiftFile;
	std::ofstream coutFile;
	std::ofstream timeFile;

	std::string outName = outputBaseName;
	outName.append("_270.txt");
	logFile.open(outName.c_str());

	if (!logFile.is_open())
	{
		oString.str("");
		oString.clear();
		oString << "Problems opening log file: " << outName;
		Err = oString.str();
		return -1;
	}

	// std::clog << "After input" << std::endl;
	logFile << "Combining quantitations results for different sections of same array\n";
	logFile << "Output Base Name: " << outputBaseName << "\n\n";

	logFile << "After Base Name\n";
	for (int n = 0; n < afterFileNames.size(); n++)
	{
		logFile << afterFileNames[n] << "\n";
	}
	logFile << std::endl;

	std::vector< Droplet > dropletList;
	dropletList.clear();
	int numbOccupied = 0;
	Statistics1 fillingFracStats;
	Statistics1 shrinkageStats;
	fillingFracStats.Clear();
	shrinkageStats.Clear();
	int array = 0;
	std::string sectionList = "";
	bool inputCheck = true;
	std::string errMsg;
	for (int n = 0; n < afterFileNames.size(); n++)
	{
		inputCheck = ReadAfterBeforeFile(afterFileNames[n], dropletList, array, sectionList, numbOccupied, fillingFracStats, shrinkageStats, errMsg);
		if (!inputCheck)
		{
			logFile << "Error reading file " << n << ", " << afterFileNames[n] << "\n   error: " << errMsg << std::endl;
			break;
		}
	}
	if (inputCheck)
	{
		fillingFracStats.Analyze();
		shrinkageStats.Analyze();

		double volumeError = 0.04;
		double mu;
		double sigma;
		bool lowerBound;
		bool upperBound;
		bool debug = false;
		logFile << "compareDroplets: dropletList.size() = " << dropletList.size() << std::endl;
		if (dropletList.size() > 10)
		{
			bool res = CalcConcentration(logFile, dropletList, volumeError,
				mu, sigma, lowerBound, upperBound, debug);
		}
		else
		{
			logFile << "Too few droplets";
			mu = 0.0;
			sigma = 1.0;
			lowerBound = true;
			upperBound = true;
		}
		oString.str("");
		oString.clear();
		oString << outputBaseName << "_cout.csv";
		coutFile.open(oString.str());
		if (array == 1)
		{
			coutFile << "Output Base Name,Array,Section,Numb ,Numb,Concentration,,Filling Frac,,Shrinkage,\n";
			coutFile << ",,List,Droplets,Occupied,Mu,Sigma,Ave,Std,Ave,Std\n";
		}
		std::cout << "# " << outputBaseName << " - " << array << " - " << sectionList << " - "
			<< dropletList.size() << " - " << numbOccupied << " - "
			<< mu << " - " << sigma << " - "
			<< fillingFracStats.Ave() << " - " << fillingFracStats.Std() << " - "
			<< shrinkageStats.Ave() << " - " << shrinkageStats.Std() << "\n";
		coutFile << outputBaseName << "," << array << "," << sectionList << ","
			<< dropletList.size() << "," << numbOccupied << ","
			<< mu << "," << sigma << ","
			<< fillingFracStats.Ave() << "," << fillingFracStats.Std() << ","
			<< shrinkageStats.Ave() << "," << shrinkageStats.Std() << "\n";

		std::cout << "%%%%%" << std::endl;
		coutFile.close();
	}
	logFile.close();

	return(0);
}

std::vector< std::string > TokenizeString(std::string InputString, std::string Delimiters)
{
	int offset = 0;
	bool cont = true;
	std::vector< std::string > results;
	results.clear();
	while (cont)
	{
		size_t pos = InputString.find_first_of(Delimiters, offset);
		if (pos == std::string::npos)
			break;
		results.push_back(InputString.substr(offset, pos - offset));
		offset = pos + 1;
		if (offset >= InputString.size())
			break;
	}
	if (results.size() == 0)
		results.push_back(InputString);

	return results;
}

bool ReadAfterBeforeFile(std::string AfterFileName, std::vector<Droplet>& DropletList,
	int& Array, std::string& SectionList, int& NumbOccupied, Statistics1& FillingFracStats, Statistics1& ShrinkageStats,
	std::string& ErrorMsg)
{
	ErrorMsg.clear();
	Droplet newDroplet;
	std::ifstream afterFile(AfterFileName.c_str());
	std::ostringstream oString;
	std::string line;
	if (!afterFile)
	{
		oString.str("");
		oString.clear();
		oString << "Error opening " << AfterFileName;
		ErrorMsg = oString.str();
		return false;
	}
	oString.str("");
	oString.clear();
	for (int n = 0; n < 13; n++)
	{
		getline(afterFile, line);
		oString << line << "\n";
		if (afterFile.bad())
		{
			oString << "Problem reading header of " << AfterFileName;
			ErrorMsg = oString.str();
			return false;
		}
	}

	bool cont = true;
	int cnt = 0;
	std::istringstream token1;
	std::istringstream token2;
	int ia;
	int is;
	int w1;
	double w2;
	oString.str("");
	oString.clear();
	std::string delimiters(",");
	int occupied = 0;
	while (cont)
	{
		getline(afterFile, line);
		if (afterFile.bad())
		{
			oString << "afterFile.bad()\n   " << line;
			break;
		}

		std::vector< std::string> results = TokenizeString(line, delimiters);
		if (results.size() < 38)
		{
			oString << "results.size() = " << results.size() << "\n   " << line;
			break;
		}
		token1.clear();
		token2.clear();
		token1.str(results[0]);
		token2.str(results[1]);
		token1 >> ia;
		token2 >> is;
		if (cnt == 0)
		{
			if (Array == 0)
			{
				Array = ia;
				SectionList = results[1];
				oString << "First line\n";
				oString << line;
				oString << "\nArray/SectionList for cnt = 0: " << Array << "/" << SectionList;
			}
			else
			{
				if (Array != ia)
				{
					ErrorMsg = "Inconsistent array numbers in list of files";
					return false;
				}
				SectionList.append("|").append(results[1]);
			}
		}

		token1.clear();
		token1.str(results[54]);
		token1 >> w1;
		if (w1 > 0)
		{
			token1.clear();
			token1.str(results[55]);
			token1 >> w1;
			newDroplet.Clear();
			newDroplet.Occupied = (w1 > 0);
			token2.clear();
			token2.str(results[23]);
			token2 >> w2;
			newDroplet.Volume = w2;
			FillingFracStats.Accumulate(w2);
			token2.clear();
			token2.str(results[59]);
			token2 >> w2;
			ShrinkageStats.Accumulate(w2);
			DropletList.push_back(newDroplet);
			if (newDroplet.Occupied)
			{
				NumbOccupied++;
				occupied++;
			}
		}
		cnt++;
	}
	return true;
}
void ShowUsage(const char* msg, const char* exe)
{
	std::clog << ">>>> " << msg << std::endl;
	std::clog << "Usage: " << exe << " <Working Directory> <Output BaseName> <Aft_Bef File 1> <Aft_Bef File 2> [<Aft_Bef File 3> ...]\n";

}