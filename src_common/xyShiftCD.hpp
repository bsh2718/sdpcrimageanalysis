#ifndef XYSHIFTCD_HPP
#define XYSHIFTCD_HPP
//! [includes]

#include <cstdlib>
#include <vector>
#include <algorithm>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "blob.hpp"
#include "blob_CD.hpp"
#include "readinfofile.hpp"
// #include "searchArea.hpp"

void XYShiftCD( std::ofstream &LogFile, double ScaleIn, int Step,
	std::vector< Blob_CD > &BlobList, int SectionOfInterest,
	cv::Mat &SectionImage, std::vector< ParameterLimit > PLimits, 
	double ThetaResolution, std::vector< cv::Point > SubSectionBands,
	std::vector< double > &Results, bool TrackAlignment, 
	std::string BaseName, std::string &ErrMsg );

#endif // XYSHIFTCD_HPP


	
