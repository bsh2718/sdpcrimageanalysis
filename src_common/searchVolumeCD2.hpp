#pragma once
//! [includes]

#include <cstdlib>
#include <vector>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "blob.hpp"

struct ParameterBounds
{
	double UpperBound;
	double LowerBound;
	double StepSize;
	int NumbSteps;
	ParameterBounds()
	{
		UpperBound = 2.0;
		LowerBound = -2.0;
		StepSize = 0.5;
		NumbSteps = 9;
	}
	ParameterBounds(double Lower, double Upper, int N)
	{
		UpperBound = Upper;
		LowerBound = Lower;
		NumbSteps = N;
		StepSize = (Upper - Lower) / ((double)N - 1.0);
	}
};
struct LocalScore
{
	int Score;
	int ThetaIdx;
	int ScaleIdx;
	double Theta;
	double Scale;
	cv::Point GridLocation;
	cv::Point Offset;

	LocalScore()
	{
		Score = 0;
		ThetaIdx = 0;
		ScaleIdx = 0;
		Theta = 0.0;
		Scale = 1.0;
		GridLocation = cv::Point(0, 0);
		Offset = cv::Point(0, 0);
	}
	LocalScore(int Scor, int TIdx, int SIdx, double Th, double Sc, cv::Point Loc, cv::Point Off = cv::Point(0,0))
	{
		Score = Scor;
		ThetaIdx = TIdx;
		ScaleIdx = SIdx;
		Theta = Th;
		Scale = Sc;
		GridLocation = Loc;
		Offset = Off;
	}
	std::string ToString(std::string Sep = ", ")
	{
		std::ostringstream oString;
		oString.str("");
		oString.clear();
		oString << Score << Sep << Theta << Sep << Scale << Sep << GridLocation.y << Sep << GridLocation.x << Sep 
			<< Offset.y << Sep << Offset.x << Sep << ThetaIdx << Sep << ScaleIdx;
		return oString.str();
	}
	std::string Header(std::string Sep = ", ")
	{
		return "Score" + Sep + "Theta" + Sep + "Scale" + Sep + "GridLocation.y" + Sep + "GridLocation.x" 
			+ Sep + "Offset.y" + Sep + "Offset.x" + Sep + "ThetaIdx" + Sep + "ScaleIdx";
	}
};

struct SearchVolumeResult
{
	int Score;
	cv::Point GlobalPosition;
	cv::Point GridLocation;
	double Theta;
	double Scale;
	int NumbAligned;
	int NumbNotAligned;
	int NumbInGuard;
	SearchVolumeResult()
	{
		Score = -1000000;
		GlobalPosition = cv::Point(0, 0);
		GridLocation = cv::Point(0, 0);
		Theta = 0.0;
		Scale = 1.0;
		NumbAligned = 0;
		NumbNotAligned = 0;
		NumbInGuard = 0;
	}
	SearchVolumeResult(int S, cv::Point GlobalP, cv::Point GridP, double Th, double Sc, int NA, int NN, int NG)
	{
		Score = S;
		GlobalPosition = GlobalP;
		GridLocation = GridP;
		Theta = Th;
		Scale = Sc;
		NumbAligned = NA;
		NumbNotAligned = NN;
		NumbInGuard = NG;
	}
	std::string ToString(std::string Sep = ", ")
	{
		std::ostringstream oString;
		oString.str("");
		oString.clear();
		oString << Score << Sep << GlobalPosition.y << Sep << GlobalPosition.x
			<< Sep << GridLocation.y << Sep << GridLocation.x << Sep << Theta << Sep << Scale << Sep
			<< NumbAligned << Sep << NumbNotAligned << Sep << NumbInGuard;
		return oString.str();
	}
	std::string Header(std::string Sep = ", ")
	{
		return "Score" + Sep + "GlobalPosition.y" + Sep + "GlobalPosition.x"
			+ Sep + "GridLocation.y" + Sep + "GridLocation.x"
			+ Sep + "Theta" + Sep + "Scale" + Sep
			+ "NumbAligned" + Sep + "NumbNotAligned" + Sep + "NumbInGuard";
	}
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// To deal with local minima in the XY search space, XYAlignment() creates rectangular search areas which are larger
// than the well spacing in the device.  XYAlignment() searches the X/Y offset pairs in a search area and assigns the
// best fit result found in the search area as the fit metric for the search area. This procedure iterates over every
// other offset in X and in Y.
// A different program (XYShift) peforms a rough alignment.  A search area is created centered on the rough alignment
// and it then searched to refine the X and Y offset and then estimate the best value of the scale factor.
//
class SearchVolume 
{
	public:
		static cv::Point XYStepSize;
		static cv::Point XYSearchLimits;
		static cv::Mat WellTemplateImage;
		static cv::Size WellTemplateSize;
		static cv::Mat SearchMatrix;
		static cv::Point2f WellTemplateCenter;
		static cv::Mat BlobImage;
		static int DebugCounter;

		bool Loaded;
		bool Searched;
		cv::Point GlobalXYLocation;
		cv::Point GridLocation;
		//double Theta;
		//double Scale;
		ParameterBounds ThetaBounds;
		ParameterBounds ScaleBounds;

		// Relative to Search Area
		int GuardScore;
		int MaxScore;
		cv::Point XYLocalMax;		
		double ThetaLocalMax;
		double ScaleLocalMax;
		SearchVolumeResult LocalSearchResult;
		std::vector< double > LocalResults;
		
		SearchVolume() : Loaded(false), Searched(false), GlobalXYLocation(cv::Point(0,0)),
			GridLocation(cv::Point(0, 0)), //Theta(0.0), Scale(1.0), 
			ThetaBounds(ParameterBounds(-0.175, 0.175, 21)), ScaleBounds(ParameterBounds(0.98, 1.02, 11)),
			GuardScore(1000000), MaxScore(-1000000), XYLocalMax(cv::Point(0,0)), ThetaLocalMax(0.0),
			ScaleLocalMax(1.0), LocalSearchResult(SearchVolumeResult())
		{
			LocalResults.resize(4);
		}
		~SearchVolume(){}
		
		SearchVolume(const SearchVolume &Source): Loaded(Source.Loaded), Searched(Source.Searched), 
			GlobalXYLocation(Source.GlobalXYLocation),
			GridLocation(Source.GridLocation), //Theta(Source.Theta), Scale(Source.Scale), 
			ThetaBounds(Source.ThetaBounds), ScaleBounds(Source.ScaleBounds), 
			GuardScore(Source.GuardScore), MaxScore(Source.MaxScore), XYLocalMax(Source.XYLocalMax), ThetaLocalMax(Source.ThetaLocalMax),
			ScaleLocalMax(Source.ScaleLocalMax), LocalSearchResult(Source.LocalSearchResult), LocalResults(Source.LocalResults)
		{}
		
		SearchVolume& operator= ( const SearchVolume &Source )
		{
			if ( this !=&Source )
			{
				Loaded = Source.Loaded;
				Searched = Source.Searched;
				GlobalXYLocation = Source.GlobalXYLocation;
				GridLocation = Source.GridLocation;
				/*Theta = Source.Theta;
				Scale = Source.Scale;*/
				ThetaBounds = Source.ThetaBounds;;
				ScaleBounds = Source.ScaleBounds;
				GuardScore = Source.GuardScore;
				MaxScore = Source.MaxScore;
				XYLocalMax = Source.XYLocalMax;
				ThetaLocalMax = Source.ThetaLocalMax;
				ScaleLocalMax = Source.ScaleLocalMax;
				LocalSearchResult = Source.LocalSearchResult;
				LocalResults = Source.LocalResults;
			}
			return *this;
		}
		
		void Clear()
		{
			Loaded = false;
			Searched = false;
			GlobalXYLocation = cv::Point(0, 0);
			GridLocation = cv::Point(0,0);;
			/*Theta = 0.0;
			Scale = 1.0;*/
			ThetaBounds = ParameterBounds(-0.175, 0.0175, 21);
			ScaleBounds = ParameterBounds(0.98, 1.02, 11);
			GuardScore = 1000000;
			MaxScore = -1000000;
			XYLocalMax = cv::Point(0,0);
			ThetaLocalMax = 0.0;
			ScaleLocalMax = 1.0;
			LocalSearchResult = SearchVolumeResult();
			LocalResults.resize(4);
			std::fill(LocalResults.begin(), LocalResults.end(), 0);
		}
		
		void Load(cv::Point GlobalLoc, cv::Point GridLoc, double Th, double Sc, 
			ParameterBounds ThBounds, ParameterBounds ScBounds)
		{
			Loaded = true;
			Searched = false;
			GlobalXYLocation = GlobalLoc;
			GridLocation = GridLoc;
			/*Theta = Th;
			Scale = Sc;*/
			ThetaBounds = ThBounds;
			ScaleBounds = ScBounds;
			GuardScore = 1000000;
			MaxScore = 0;
			XYLocalMax = cv::Point(0,0);
			ThetaLocalMax = Th;
			ScaleLocalMax = Sc;
			LocalSearchResult = SearchVolumeResult();
			LocalResults.resize(4);
			std::fill(LocalResults.begin(), LocalResults.end(), 0);
		}

		LocalScore CalculateGuardScore(int ThIndex, double Th, int ScIndex, double Sc, cv::Mat& TransMat,
			cv::Mat& WorkImage1, cv::Mat& WorkImage2, cv::Mat& WorkImage3, cv::Mat& WorkImage4, cv::Mat& WorkImage5);

		LocalScore GuardThetaSearch(std::ofstream& LogFile, std::string BaseName, cv::Point Offset,
			ParameterBounds ThBounds, ParameterBounds ScBounds, bool TrackAlignment);

		LocalScore GuardScaleSearch(std::ofstream& LogFile, std::string BaseName, cv::Point Offset,
			LocalScore BestGuardScore, bool TrackAlignment);

		SearchVolumeResult CalculateTemplateScore(std::ofstream& LogFile, std::string BaseName, cv::Point Offset, int GuardPenalty,
			double Theta, double Scale, cv::Mat& WorkImage1, cv::Mat& WorkImage2, cv::Mat& WorkImage3, cv::Mat& WorkImage4,
			bool TrackAlignment);

		SearchVolumeResult TemplateThetaSearch(std::ofstream& LogFile, std::string BaseName,
			LocalScore GuardLocalResult, ParameterBounds ThBounds, ParameterBounds ScBounds, int GuardPenalty, cv::Point Offset,
			bool TrackAlignment);

		SearchVolumeResult TemplateScaleSearch(std::ofstream& LogFile, std::string BaseName,
			SearchVolumeResult GuardLocalScore, ParameterBounds ScBounds, int GuardPenalty, cv::Point Offset,
			bool TrackAlignment);

		int ExtraOutput(std::ofstream& LogFile, std::string BaseName, LocalScore BestScore, LocalScore& ScoreOut, bool TrackAlignment);

		std::string ToString()
		{
			std::ostringstream oString;
			oString.str("");
			oString.clear();
			oString << "Grid [" << GridLocation.y << ", " << GridLocation.x << "] / GlobalLocation [" << GlobalXYLocation.y << ", " << GlobalXYLocation.x << "]\n"
				<< std::boolalpha << "     Loaded: " << Loaded << ", Searched: " << Searched << "\n"
				<< "     Score: " << LocalSearchResult.Score << ", Numb Blobs Aligned: " << LocalSearchResult.NumbAligned << ", Not aligned: "
				<< LocalSearchResult.NumbNotAligned << "\n"
				<< "     Results: " << LocalResults[0] << " / " << LocalResults[1] << " / " << LocalResults[2] << " / " << LocalResults[3];
			return oString.str();
		}
		std::string StaticToString()
		{
			std::ostringstream oString;
			oString.str("");
			oString.clear();

			oString << "XYStepSize: " << XYStepSize << ", XYSearchLimits: " << XYSearchLimits << ", WellTemplateSize: " << WellTemplateSize
				<< ", SearchMatrixSize: " << SearchMatrix.size();
			return oString.str();
		}
};