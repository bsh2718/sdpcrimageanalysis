#ifndef EXTRACTBLOBS_HPP
#define EXTRACTBLOBS_HPP

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "physicalconstants.h"
#include "statistics.hpp"
#include "blob.hpp"
#include "analysisParameters.hpp"
#include "calccentroid.hpp"

int ExtractBlobsAdaptive(std::ofstream &LogFile, double C, int BlockSize, double MinimumPeak,
	cv::Mat &BackgroundSubImage, cv::Mat &BackgroundSubImage32F, cv::Point WellSpacing8U,
	cv::Mat &WorkImage2, cv::Mat &WorkImage3, int MinContourLength, int MaxContourLength,
	AnalysisParameters &FParams, int Set1, int SetN, 
	cv::Mat &Foreground8U, std::vector<Blob> &BlobList, std::vector<Blob> &PoorBlobList, 
	std::vector<Blob> &BadBlobList, std::vector<Blob> &BackgroundList, bool TrackOutputExtraCSV );
	
// int ExtractBlobsAdpative(std::ofstream &LogFile, double C, int BlockSize, double MinimumPeak,
	// cv::Mat &BackgroundSubImage, cv::Mat &BackgroundSubImage32F, 
	// cv::Mat &WorkImage2, cv::Mat &WorkImage3, int MinContourLength, 
	// double MinArea, int Set,
	// cv::Mat &Foreground8U, std::vector<Blob> &NewBlobList );

bool ContourToBlob(AnalysisParameters &FParams, int Set1, int SetN, std::vector< cv::Point> &Contour,
	cv::Mat &BackgroundSubImage, double MinimumPeak, cv::Mat &Mask, double Threshold, Blob &NewBlob);
	
void GetForeground(cv::Mat &BackgroundSubImage, double C, int BlockSize, double MinimumPeak, cv::Mat StructElementOpen, 
	cv::Mat &WorkImage2, cv::Mat &WorkImage3, cv::Mat &Foreground8U, int Iteration );

int CountBlobsAdaptive(std::ofstream &LogFile, double C, int BlockSize, double MinimumPeak, cv::Mat &BackgroundSubImage, 
	cv::Mat &Foreground8U, cv::Mat &WorkImage2, cv::Mat &WorkImage3, int MinContourLength, 
	AnalysisParameters &FParams, int Set );

int CountBlobsAdaptive(std::ofstream &LogFile, double C, int BlockSize, double MinimumPeak, cv::Mat &BackgroundSubImage, 
	cv::Mat &Foreground8U, cv::Mat &WorkImage2, cv::Mat &WorkImage3, int MinContourLength, 
	double MinArea, std::vector< cv::Point > &BPositions, std::vector< double > &BAreas );
	
#endif  // EXTRACTBLOBS_HPP