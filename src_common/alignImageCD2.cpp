#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "statistics.hpp"
#include "blob.hpp"
#include "readinfofile.hpp"
#include "trimRectangle.hpp"
#include "physicalconstants.h"

#include "searchVolumeCD2.hpp"
#include "searchGridCD2.hpp"
#include "alignImageCD2.hpp"
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//int AlignImageCD(std::ofstream& LogFile, std::string& BaseName,
//	std::vector< Blob >& BlobList, std::vector< Blob >& OtherBlobList, std::vector< Blob >& BadBlobList,
//	cv::Mat& SectionImage, double DistToCenter, double ThetaResolution,
//	std::vector< ParameterLimit > PLimits,
//	cv::Size ImageSize, std::vector<cv::Point2f> WellSpacing, cv::Size2f WellSize, int SectionOfInterest,
//	std::vector< DeviceOrigins >& SectionOrigins,
//	std::vector< DeviceOrigins >& SubSectionOrigins, std::vector< cv::Size > SubSectionDimens,
//	std::vector< double >& Results, bool TrackAlignment, std::string& ErrMsg)
//
// Aligns image with the radial grid for CD type device 
// 
//			Input
// LogFile - ofstream connected to log filebuf
// BaseName - project name
// BlobList - vector of Blob. Includes well-like blobs which pass criteria to be used for quantitation.
// OtherBlobList - vector of Blob. Includes well-like blobs which do not pass criteria to be used for quantitation,
//		but appear to be droplets in wells.
// BadBlobList - vector of Blob.  Includes blobs which may not actually be droplets
// SectionImage - 8 bit gray scale image.  For each Section/Subsection pair in the device, a region is outlined using the
//		outside edges of the wells in each column of wells.  The outlines are the smallest regions each of which encloses
//		all of the wells of a single column in a Section/Subsection.  The interior of each of these regions is non zero.  Pixels outside
//		of this region are set to zero.  The value of the interiors indicates which Section/Subsection the column of wells
//      is a part of.
// DistToCenter - Distance (in pixels) from the top center edge of the image to the center of the CD.  This assumes that 
//      the alignment of the camera over the CD is correct.  The program has the ability to make small adjustments if
//      this distance is slightly off.
// ThetaResolution - The rows and column coordinates (y, x) of the center of each droplet are converted into radial 
//      coordinates (R, Theta).  R is in pixels with the same microns/pixel as the original image. Theta is in radians
//      with ThetaResolution radians/pixel.  R and Theta are expressed as y and x in the coordinates used to align the
//      droplets to the radial grid.
// PLimits - 
// ImageSize - size of the 
// WellSpacing - center to center distance of wells in pixels (R, Theta).  This is a vector because each subsection of
//       the device can have a different Theta spacing.
// WellSize - size of wells in pixels (in the original image) 
// SectionOfInterest - this is the one section in the image which is to be analyzed.  Droplets in wells which
//       are part of a different section are ignored.
// SectionOrigins - Coordinates in (R, Theta) of the upper left corner of each section. Only Theta is used.
// SubSectionOrigins - Coordinates in (R, Theta) of upper left corner of each section. Only R is used.
// SubSectionDimens - Number of rows and columns in a subsection.  This is the same for a given subsection regardless
//      of which section it is a part of.
// Results  - (X-Offset_Final, Y-Offset_Final, Angle, Scale_Final)
// TrackAlignment - Additional information is output if this is true
// ErrMsg - 

bool SortGScores(LocalScore A, LocalScore B)
{
	return A.Score < B.Score;
}
bool SortVSResults(SearchVolumeResult A, SearchVolumeResult B)
{
	return A.Score > B.Score;
}
int AlignImageCD(std::ofstream& LogFile, std::string& BaseName,
	std::vector< Blob >& BlobList,
	cv::Mat& WellTemplateImage, std::vector< ParameterLimit > PLimits,
	cv::Size ImageSize, std::vector<cv::Point2f> WellSpacing, cv::Size2f WellSize, int SectionOfInterest,
	std::vector< double >& GlobalResults, SearchVolumeResult& GlobalSearchResult, bool TrackAlignment, std::string& ErrMsg)
{
	std::string sep = ", ";
	std::ostringstream oString;
	std::ofstream outFile;
	std::string outName;
	std::vector< double > init;
	init.resize(4);
	init[0] = init[1] = init[2] = 0.0;
	init[3] = 1.0;
	std::vector< double > searchResults = init;
	GlobalResults = init;
	GlobalSearchResult = SearchVolumeResult();
	ErrMsg.clear();

	SearchGrid alignImage(PLimits,
		ImageSize, WellSpacing, WellSize, SectionOfInterest, WellTemplateImage);
	std::string errMsg;
	if (!alignImage.Initialize(LogFile, BaseName, BlobList, ImageSize, TrackAlignment, errMsg))
	{
		LogFile << "Error calling alignImage.Initialize()\n";
		LogFile << errMsg << std::endl;
		return 0;
	}
	LogFile << "SearchVolume static variables\n";
	LogFile << alignImage.XYGrid[0][0].StaticToString() << std::endl;
	LogFile << "Searching XYGrid center\n";
	SearchVolumeResult globalSearchResult;
	ParameterBounds thetaBounds = ParameterBounds(-PLimits[2].Limit, PLimits[2].Limit, 21);
	ParameterBounds scaleBounds = ParameterBounds(1.0 - PLimits[3].Limit, 1.0 + PLimits[3].Limit, 11);
	if (TrackAlignment)
	{
		LogFile << "ThetaBounds: " << thetaBounds.LowerBound << ", " << thetaBounds.UpperBound << ", " << thetaBounds.NumbSteps << ", " << thetaBounds.StepSize << "\n";
		LogFile << "ScaleBounds: " << scaleBounds.LowerBound << ", " << scaleBounds.UpperBound << ", " << scaleBounds.NumbSteps << ", " << scaleBounds.StepSize << "\n";
	}
	std::vector< double > thetaResults;
	std::vector< double > scaleResults;
	LocalScore bestGuardScore;
	std::vector< LocalScore > guardScoreList;
	alignImage.GridSearchGuard(LogFile, BaseName, thetaBounds, scaleBounds,
		bestGuardScore, guardScoreList, TrackAlignment);

	if (TrackAlignment)
	{
		oString.str("");
		oString.clear();
		oString << BaseName << "_ScoreArray.csv";
		std::ofstream csvFile;
		csvFile.open(oString.str());
		csvFile << "#guardScores\nGrid.x, Grid.y, Score\n";
		for (int n = 0; n < guardScoreList.size(); n++)
		{
			csvFile << guardScoreList[n].GridLocation.x << sep << guardScoreList[n].GridLocation.y 
				<< sep << guardScoreList[n].Score << "\n";
		}
		csvFile.close();
		std::vector<LocalScore> tmpScores = guardScoreList;
		std::sort(tmpScores.begin(), tmpScores.end(), SortGScores);
		int cutoff = (tmpScores[0].Score * 3) / 2;
		if (cutoff < 75)
			cutoff = 75;
		LocalScore scoreOut;
		bool divider = false;
		LogFile << "\nSorted VolumeList\nn, Score,Theta,Scale,GridLoc,GlobalLoc\n";
		for (int n = 0; n < tmpScores.size(); n++)
		{
			int r = tmpScores[n].GridLocation.y;
			int c = tmpScores[n].GridLocation.x;
			LogFile << n << ", " << tmpScores[n].Score << ", " << tmpScores[n].Theta << ", " << tmpScores[n].Scale << ", " << tmpScores[n].GridLocation 
				<< ", " <<alignImage.XYGrid[r][c].GlobalXYLocation << ", " << tmpScores[n].Offset << "\n";
			if (tmpScores[n].Score < cutoff)
			{
				oString.str("");
				oString.clear();
				oString << BaseName << "_" << r << "_" << c;
				alignImage.XYGrid[r][c].ExtraOutput(LogFile, oString.str(), tmpScores[n], scoreOut, TrackAlignment);
			}
			else if (!divider)
			{
				LogFile << "===================================\n";
				divider = true;
			}
		}
		LogFile << "\n";
	}
	cv::Point loc = bestGuardScore.GridLocation;
	LogFile << "\nbestGuardScore: " << bestGuardScore.ToString() << " GlobalXYLocation.y/.x: "
		<< alignImage.XYGrid[loc.y][loc.x].GlobalXYLocation.y << "/" << alignImage.XYGrid[loc.y][loc.x].GlobalXYLocation.x << "\n\n";

	SearchVolumeResult bestResult;
	alignImage.GridSearch(LogFile, BaseName,
		thetaBounds, scaleBounds,
		-100, guardScoreList, bestResult, TrackAlignment);
	LogFile << "After GridSearch : " << bestResult.ToString() << "\n";

	GlobalResults.resize(4);
	GlobalSearchResult = bestResult;
	GlobalResults[0] = GlobalSearchResult.GlobalPosition.x;
	GlobalResults[1] = GlobalSearchResult.GlobalPosition.y;
	GlobalResults[2] = GlobalSearchResult.Theta;
	GlobalResults[3] = GlobalSearchResult.Scale;

	LogFile << "GlobalResults: " << GlobalResults[0] << " , " << GlobalResults[1] << " , " << GlobalResults[2] << " , " << GlobalResults[3] << "\n";
	cv::Mat workImage1 = SearchVolume::BlobImage.clone();
	cv::Mat workImage2(SearchVolume::WellTemplateSize, CV_8UC1);
	cv::Mat workImage3(SearchVolume::WellTemplateSize, CV_8UC1);
	cv::Mat workImage4(SearchVolume::WellTemplateSize, CV_8UC1);
	cv::Mat rotMat = cv::getRotationMatrix2D(SearchVolume::WellTemplateCenter, GlobalResults[2], GlobalResults[3]);
	cv::warpAffine(workImage1, workImage2, rotMat, SearchVolume::WellTemplateSize, cv::INTER_NEAREST, cv::BORDER_CONSTANT, 0);
	oString.str("");
	oString.clear();
	oString << BaseName << "_FinalRotatedBlobImage.png";
	cv::imwrite(oString.str(), workImage2);
	cv::Mat transMat(cv::Size(3, 2), CV_64FC1);
	transMat.setTo(0.0);
	transMat.at<double>(cv::Point(0, 0)) = 1.0;
	transMat.at<double>(cv::Point(1, 1)) = 1.0;
	transMat.at<double>(cv::Point(2, 0)) = (double)(GlobalResults[0]);
	transMat.at<double>(cv::Point(2, 1)) = (double)(GlobalResults[1]);
	cv::warpAffine(workImage2, workImage3, transMat, SearchVolume::WellTemplateSize, cv::INTER_NEAREST, cv::BORDER_CONSTANT, 0);
	workImage4.setTo(0);
	SearchVolume::WellTemplateImage.copyTo(workImage4, workImage3);
	oString.str("");
	oString.clear();
	oString << BaseName << "_FinalTranslatedBlobImage.png";
	cv::imwrite(oString.str(), workImage3);
	oString.str("");
	oString.clear();
	oString << BaseName << "_FinalMaskedBlobImage.png";
	cv::imwrite(oString.str(), workImage4);
	workImage1.setTo(255, workImage3);
	oString.str("");
	oString.clear();
	oString << BaseName << "_ShiftOverlay.png";
	cv::imwrite(oString.str(), workImage1);
	workImage1 = SearchVolume::WellTemplateImage.clone();
	workImage1.setTo(255, workImage3);
	oString.str("");
	oString.clear();
	oString << BaseName << "_TemplateOverlay.png";
	cv::imwrite(oString.str(), workImage1);
	LogFile << "     Final rotMat\n";
	for (int r = 0; r < rotMat.rows; r++)
	{
		for (int c = 0; c < rotMat.cols; c++)
		{
			LogFile << rotMat.at<double>(cv::Point(c, r));
			if (c >= 2)
				LogFile << "\n";
			else
				LogFile << ", ";
		}
	}
	LogFile << "     Final transMat\n";
	for (int r = 0; r < transMat.rows; r++)
	{
		for (int c = 0; c < transMat.cols; c++)
		{
			LogFile << transMat.at<double>(cv::Point(c, r));
			if (c >= 2)
				LogFile << "\n";
			else
				LogFile << ", ";
		}
	}
	return GlobalSearchResult.NumbAligned;
}
