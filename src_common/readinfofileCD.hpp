#ifndef READINFOFILE_CD_HPP
#define READINFOFILE_CD_HPP
//! [includes]
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//#include <iostream>
//#include <string>
//#include <cstdlib>
//! [includes]

#include "deviceOrigins.hpp"
#include "readinfofile.hpp"

bool ReadDeviceInfoCD(std::string InfoFileName, double &DistanceToCenter, double& ThetaResolution,
	std::vector< DeviceOrigins > &SectionOrigins,
	std::vector< DeviceOrigins > &SubSectionOrigins, std::vector< cv::Size > &SubSectionDimens, std::vector<cv::Point2f> &SubSectionSpacing,
	std::vector< GuardInfo > &TGuards, std::vector< GuardInfo > &RGuards, 
	std::string &ErrorMsg);

bool WriteDeviceInfoCD(std::string InfoFileName,
	std::vector< DeviceOrigins > &SectionOrigins,
	std::vector< DeviceOrigins > &SubSectionOrigins, std::vector< cv::Size > &SubSectionDimens, std::vector<cv::Point2f> &SubSectionSpacing,
	std::vector< GuardInfo > &TGuards, std::vector< GuardInfo > &RGuards, 
	std::string &ErrorMsg);	

bool ReadSectionData(std::string SubImageFileName, int &SectionOfInterest,
	cv::Rect &ImageAnalysisArea, cv::Rect &SectionArea,
	cv::Size2f &WellSize, double &WellArea, cv::Point2f &WellRegionSize, //double &WellVolume, 
	std::vector< ParameterLimit > &PLimits, double &BlockSizeMult, double &MinimumPeak, double &MaxFillingRatio,
	std::string &ErrorMsg );	

#endif // READINFOFILE_CD_HPP
