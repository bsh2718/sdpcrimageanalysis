////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// These procedures will trim a rectangle so that it does not extend outside of an imagesize.  They will alter the 
// x and y values of the Rectangle and the height and width so that the Rectangle will fit. If a rectangle will not fit
// then it will be trimmed, not shifted.  
// The program will return false if the area of the resulting rectangle is less than 9 and true if it is 9 or greater.
//
//
// bool TrimRectangle(cv::Rect &Rectangle, cv::Size RectSize, cv::Size ImageSize )
//      input
// Rectangle - The x and y coordinates of the rectangle have been set.  The height and width are ignored.
// RectSize - Contains the height and width of desired rectangle.
// ImageSize - Size of image.
//      output
// Rectangle - On return all of the components of Rectangle have been set so it will fit in the image.
//
//
// bool TrimRectangle(cv::Rect &Rectangle, cv::Size ImageSize )
//     input
// Rectangle - On input, all of the components of Rectangle have been set to their desired values.
// ImageSize - Size of image.
//      output
// Rectangle - On return all of the components of Rectangle have been set so it will fit in the image.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//! [includes]
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "trimRectangle.hpp"
//! [includes]

//! [namespace]
//using namespace cv;
//! [namespace]
//using namespace std;

bool TrimRectangle(cv::Rect &Rectangle, cv::Size RectSize, cv::Size ImageSize )
{
	if ( Rectangle.x > ImageSize.width - 2 || Rectangle.x + RectSize.width < 1 ||
		Rectangle.y > ImageSize.height - 2 || Rectangle.y + RectSize.height < 1 )
		return false;
	
	Rectangle.width = RectSize.width;
	Rectangle.height = RectSize.height;
	
	if ( Rectangle.x < 0 )
	{
		Rectangle.width += Rectangle.x;
		Rectangle.x = 0;
	}
	if ( Rectangle.x + Rectangle.width >= ImageSize.width )
		Rectangle.width = ImageSize.width - Rectangle.x;

	if ( Rectangle.y < 0 )
	{
		Rectangle.height += Rectangle.y;
		Rectangle.y = 0;
	}
	if ( Rectangle.y + Rectangle.height >= ImageSize.height )
		Rectangle.height = ImageSize.height - Rectangle.y;
	
	if ( Rectangle.width * Rectangle.height > 8 )
		return true;
	else
		return false;
}

bool TrimRectangle(cv::Rect &Rectangle, cv::Size ImageSize )
{
	if ( Rectangle.x > ImageSize.width - 2 || Rectangle.x + Rectangle.width < 1 ||
		Rectangle.y > ImageSize.height - 2 || Rectangle.y + Rectangle.height < 1 )
		return false;

	if ( Rectangle.x < 0 )
	{
		Rectangle.width += Rectangle.x;
		Rectangle.x = 0;
	}
	if ( Rectangle.x + Rectangle.width >= ImageSize.width )
		Rectangle.width = ImageSize.width - Rectangle.x;

	if ( Rectangle.y < 0 )
	{
		Rectangle.height += Rectangle.y;
		Rectangle.y = 0;
	}
	if ( Rectangle.y + Rectangle.height >= ImageSize.height )
		Rectangle.height = ImageSize.height - Rectangle.y;
	
	if ( Rectangle.width * Rectangle.height > 8 )
		return true;
	else
		return false;
}
