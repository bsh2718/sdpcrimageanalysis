#ifndef WW_HPP
#define WW_HPP

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "physicalconstants.h"
#include "beforeAfterMatrix.hpp"
#include "compareDroplets.hpp"
#include "blob.hpp"


class Run
{
	public:
		std::vector< cv::Point > SectionPosition;
		bool Included;
		bool Occupied;
		
		Run(): Included(true), Occupied(false)
		{
			SectionPosition.clear();
		}
		~Run(){}
		
		
		void Clear()
		{
			Included = true;
			SectionPosition.clear();
		}
};


class Well
{
	public:
		int Index;
		int Col;
		int Row;
		bool Occupied;
		
		Well(){}
		~Well(){}

};

bool SortWell( Well A, Well B  );

bool WWStatistics( std::vector< Run > &RunList, double SigmaMult, int &NumbRuns, double &Mu, double &Sigma );

bool WaldWolfowitz( std::vector< std::vector< BeforeAfterMatrix > > &BeforeAfter, 
	std::vector< BlobSummary > &BeforeSizingList, std::vector< BlobSummary > &AfterSizingList, 
	cv::Size SectionDimens, double SigmaMult, RunInformation &RunInfo );

#endif  // WW_HPP