//////////////////////////////////////////////////////////////////////////////////
// void XYShift( std::ofstream &LogFile, double ScaleIn, int Step,
//		std::vector< Blob > &BlobList, 
//		cv::Mat &SectionImage, std::vector< ParameterLimit > PLimits, 
//		std::vector< double > &Results, bool TrackAlignment, std::string &ErrMsg )
//
// Takes the geometric centers of the blobs in BlobList and shifts then in X and Y
// to produce the best alignment of their coordinates with non zero regions in
// SectionImage.  The procedure assumes that the image has been corrected for angular
// offset and uses RotatedCenter2f of each blob as its center.
// SectionImage contains rectangles, each of which encloses all of the wells of a 
// particular Array/Section.  Best alignment means the X and Y offsets which 
// maximize the number of blobs coordinates in one of the rectangles. 
// The procedure iterates the values of the X and Y offsets over the range specified
// in PLimits. The step size for this iteration is given by Step.
//
//     input
// LogFile - reference to log file
// ScaleIn - Multiplicative scale factor to be applied to coordinates of blob.  This
//		procedure does not vary the scale factor.
// Step - Step size for iterating the X and Y offsets ( += Step ) when finding the
//		best value of the offsets.
// BlobList - vector of <Blob>
// SectionImage - Eight bit image which is non zero in regions which enclose the wells
//		of a particular array/section.  The boundaries of the rectangle extend to the
//		outside edges of the wells on the edges of a particular array/section.
// PLimits - vector of limit information for the offsets. (Also anlge and scale, though
//		those are not used in this procedure.)  The offsets are varied over the ranges
//
// X: PLimits[0].BaseValue-PLimits[0].Limit) to (PLimits[0].BaseValue+PLimits[0].Limit)
// Y: PLimits[1].BaseValue-PLimits[1].Limit) to (PLimits[1].BaseValue+PLimits[1].Limit)
//
//		in steps of Step.
//
//     output
// Results - vector<double> (X-Offset, Y-Offset, not used, Scale ).  The and X and Y 
//		Offset are determined here.  Scale is set to ScaleIn.  the third item, which
//		corresponds to the angle is not touched here.
// TrackAlignment - If true, extra output is printed to LogFile.
// ErrMsg - Error message
//
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "statistics.hpp"
#include "blob.hpp"
#include "blob_CD.hpp"
#include "readinfofile.hpp"
#include "xyShiftCD.hpp"

//bool EvaluateGValue(int GValue, int SectionOfInterest, int SubSectionOfInterest, int SectionImageDivider, cv::Point SubSectionBand,
//	cv::Point DeviceCenter, bool& Aligned, bool& OffPattern)
//{
//	Aligned = false;
//	OffPattern = false;
//	if (GValue > 0)
//	{
//		if ((int)floor(GValue / 10) == SectionOfInterest && (int)GValue % 10 == SubSectionOfInterest)
//		{
//			Aligned = true;
//		}
//	}
//	else if (DeviceCenter.y >= SubSectionBand.x && DeviceCenter.y <= SubSectionBand.y)
//	{
//		if (SectionOfInterest == 1)
//		{
//			if (DeviceCenter.x < SectionImageDivider)
//				OffPattern = true;
//		}
//		else
//		{
//			if (DeviceCenter.x > SectionImageDivider)
//				OffPattern = true;
//		}
//	}
//	return Aligned;
//}
bool EvaluateGValue(int GValue, int SectionOfInterest, int SubSectionOfInterest, int SectionImageDivider, cv::Point SubSectionBand,
	cv::Point DeviceCenter, bool& Aligned, bool& OffPattern, bool &OtherSection)
{
	Aligned = false;
	OffPattern = false;
	OtherSection = false;
	if (GValue > 0)
	{
		if ((int)GValue % 10 == SubSectionOfInterest)
		{
			if ((int)floor(GValue / 10) == SectionOfInterest)
				Aligned = true;
			else
				OtherSection = true;
		}
	}
	else if (DeviceCenter.y >= SubSectionBand.x && DeviceCenter.y <= SubSectionBand.y)
	{
		if (SectionOfInterest == 1)
		{
			if (DeviceCenter.x < SectionImageDivider)
				OffPattern = true;
		}
		else
		{
			if (DeviceCenter.x > SectionImageDivider)
				OffPattern = true;
		}
	}
	return Aligned;
}
int ColumnMove(std::vector<Blob_CD>& Blob_CDList, cv::Point Offset, double Scale, int SectionOfInterest,
	int SubSectionOfInterest, cv::Mat& SectionImage, int SectionImageDivider, cv::Point SubSectionBand,
	int& MaxAligned, int& MaxOffPattern, int &MaxOtherSection, int& MaxCol, int& LastCol, 
	int& NumbAligned, int& NumbOffPattern, int &NumbOtherSection)
{
	bool aligned;
	bool offPattern;
	bool otherSection;
	NumbAligned = 0;
	NumbOffPattern = 0;
	NumbOtherSection = 0;
	int numbBlobs = (int)Blob_CDList.size();
	for (int b = 0; b < numbBlobs; b++)
	{
		if (Blob_CDList[b].ShiftThetaR(Offset, SectionImage.size()))
		{
			int gValue = SectionImage.at<uchar>(Blob_CDList[b].DeviceCenter);
			if (EvaluateGValue(gValue, SectionOfInterest, SubSectionOfInterest, SectionImageDivider, SubSectionBand,
				Blob_CDList[b].DeviceCenter, aligned, offPattern, otherSection))
			{
				NumbAligned++;
			}
			else if (otherSection)
				NumbOtherSection++;
			else if (offPattern)
				NumbOffPattern++;
		}
	}
	int maxScore = 2 * MaxAligned - 2 * MaxOffPattern + MaxOtherSection;
	int newScore = 2 * NumbAligned - 2 * NumbOffPattern + NumbOtherSection;
	if (newScore > maxScore)
	{
		MaxAligned = NumbAligned;
		MaxOffPattern = NumbOffPattern;
		MaxOtherSection = NumbOtherSection;
		MaxCol = Offset.x;
		LastCol = Offset.x;
	}
	else if (newScore == maxScore)
	{
		LastCol = Offset.x;
	}
	return MaxCol;
}
void XYShiftCD( std::ofstream &LogFile, double ScaleIn, int Step,
	std::vector< Blob_CD > &Blob_CDList, int SectionOfInterest,
	cv::Mat &SectionImage, std::vector< ParameterLimit > PLimits, 
	double ThetaResolution, std::vector< cv::Point > SubSectionBands,
	std::vector< double > &Results, bool TrackAlignment, 
	std::string BaseName, std::string &ErrMsg )
{
	int initRowGuess = 20;
	std::ostringstream oString;
	cv::Size imageSize = SectionImage.size();
	cv::Mat checkRT;
	cv::Rect checkRect;
	bool checkRTcreated = false;
	int colLimit = imageSize.width - 1;
	int rowLimit = imageSize.height - 1;
	int row = initRowGuess;
	int maxCol = (int)round(PLimits[0].BaseValue);  // maxCol refers to column offset which maximizes the number of aligned blobs
	int maxRow = 0;										// maxRow refers to row offset which maximizes the number of aligned blobs
	int maxRowBelow = 1 ;
	int maxRowAbove = rowLimit;
	int maxAligned = 0;
	int maxOffPattern = 0;
	int maxOtherSection = 0;
	cv::Point sectionImageMiddle;
	sectionImageMiddle.x = SectionImage.cols / 2;
	sectionImageMiddle.y = SectionImage.rows / 2;
	
	int numbBlobs = (int)Blob_CDList.size();
	/*for ( int b = 0 ; b < numbBlobs ; b++ )
	{
		int rCheck = row + (int)round(Blob_CDList[b].Theta_R2f.y * ScaleIn);
		if ( rCheck < 1 && rCheck < maxRowBelow )
			maxRowBelow = rCheck;
		else if ( rCheck > rowLimit && rCheck > maxRowAbove )
			maxRowAbove = rCheck;
	}
	if ( maxRowBelow < 1 )
		row += ( 1 - maxRowBelow );
	else if ( maxRowAbove > rowLimit )
		row += ( rowLimit - maxRowAbove );*/
	row = maxRow;
	if ( TrackAlignment )
		LogFile << "Approx. alignment Col\n ";

	cv::Point tmpPt;
	cv::Point offset = cv::Point((int)PLimits[0].BaseValue, row );
	maxRow = row;
	if ( TrackAlignment )
	{
		LogFile << "   offset = " << offset << " - ScaleIn = " << ScaleIn << " - Step = " << Step << "\n";
		LogFile << "Col\tAlig\tOffPat\tMaxCol\tMaxAlig\tMaxOffPat\tScale\tOffset\n";

		checkRT.create(imageSize, CV_8U);
		checkRT.setTo(0);
		checkRect.width = 5;
		checkRect.height = 5;
	}
	cv::Point shiftedTenthPt = cv::Point(0,0);
	int sectionImageDivider;
	if (SectionOfInterest == 1)
		sectionImageDivider = sectionImageMiddle.x - Step;
	else
		sectionImageDivider = sectionImageMiddle.x + Step;
	for ( int b = 0 ; b < numbBlobs ; b++ )
	{
		if (Blob_CDList[b].OnImage)
		{
			bool aligned;
			bool offPattern;
			bool otherSection;
			int gValue = SectionImage.at<uchar>(Blob_CDList[b].DeviceCenter);
			if ( EvaluateGValue(gValue, SectionOfInterest, 1, sectionImageDivider, SubSectionBands[1],
				Blob_CDList[b].DeviceCenter, aligned, offPattern, otherSection))
			{
				maxAligned++;
			}
			else if (offPattern)
				maxOffPattern++;
			if (TrackAlignment)
			{
				checkRect.x = Blob_CDList[b].DeviceCenter.x - 2;
				checkRect.y = Blob_CDList[b].DeviceCenter.y - 2;
				checkRT(checkRect).setTo(100);
			}
		}
	}
	if (TrackAlignment)
	{
		checkRTcreated = true;
		oString.str("");
		oString.clear();
		oString << BaseName << "_155_XY_RT.png";
		cv::imwrite(oString.str(), checkRT);
	}
	int lastMax = maxCol;
	int thetaLimit = (int)round(PLimits[0].Limit);
	if (thetaLimit < 5)
		thetaLimit = 5;
	for ( int n = -thetaLimit; n < thetaLimit ; n += Step )
	{
		int c = (int)PLimits[0].BaseValue + n;
		offset = cv::Point(c, row);
		int numbAligned = 0;
		int numbOffPattern = 0;
		int numbOtherSection = 0;
		ColumnMove(Blob_CDList, offset, ScaleIn, SectionOfInterest,
			1, SectionImage, sectionImageDivider, SubSectionBands[1],
			maxAligned, maxOffPattern, maxOtherSection, maxCol, lastMax, numbAligned, numbOffPattern, numbOtherSection);
		if ( TrackAlignment )
		{
			LogFile << c << "\t" << numbAligned << "\t" << numbOffPattern << "\t" << numbOtherSection << "\t" << maxCol 
				<< "\t" << maxAligned << "\t" << maxOffPattern << "\t" << maxOtherSection
				<< "\t" << ScaleIn << "\t" << offset << "\n";
		}
	}
	//for (int n = 1; n < thetaLimit; n += Step)
	//{
	//	int c = (int)PLimits[0].BaseValue + n;
	//	offset = cv::Point(c, row);
	//	int numbAligned = 0;
	//	int numbOffPattern = 0;
	//	ColumnMove(Blob_CDList, offset, ScaleIn, SectionOfInterest,
	//		1, SectionImage, sectionImageDivider, SubSectionBands[1],
	//		maxAligned, maxOffPattern, maxCol, lastMax, numbAligned, numbOffPattern);
	//	if (TrackAlignment)
	//	{
	//		LogFile << c << "\t" << numbAligned << "\t" << numbOffPattern << "\t" << maxCol
	//			<< "\t" << maxAligned << "\t" << maxOffPattern
	//			<< "\t" << ScaleIn << "\t" << offset << "\n";
	//	}

	//	c = (int)PLimits[0].BaseValue - n;
	//	offset = cv::Point(c, row);
	//	ColumnMove(Blob_CDList, offset, ScaleIn, SectionOfInterest,
	//		1, SectionImage, sectionImageDivider, SubSectionBands[1],
	//		maxAligned, maxOffPattern, maxCol, lastMax, numbAligned, numbOffPattern);
	//	if (TrackAlignment)
	//	{
	//		LogFile << c << "\t" << numbAligned << "\t" << numbOffPattern << "\t" << maxCol
	//			<< "\t" << maxAligned << "\t" << maxOffPattern
	//			<< "\t" << ScaleIn << "\t" << offset << "\n";
	//	}
	//}
	if ( lastMax > maxCol )
		maxCol = ( 2 * maxCol + lastMax ) / 3;
	else
		maxCol = ( maxCol + 2 * lastMax ) / 3;
	LogFile << "RTShift: maxCol = " << maxCol << ", maxAngle = " << (double)maxCol * ThetaResolution << "\n";
	LogFile << "Approx. alignment Row\n";
	lastMax = row;
	if ( TrackAlignment )
	{
		LogFile << "   offset = " << offset << " - ScaleIn = " << ScaleIn << " - Step = " << Step << "\n";
		LogFile << "Row\tAlig\tMaxRow\tMaxAlig\tScale\tOffset\n";
	}
	int rLimit = (int)round(PLimits[1].Limit);
	for ( int n = -rLimit ; n < rLimit ; n += Step )
	{
		int r = initRowGuess + n;
		offset = cv::Point(maxCol, r);
		int numbAligned = 0;
		
		for ( int b = 0 ; b < numbBlobs ; b++ )
		{
			if (Blob_CDList[b].ShiftThetaR(offset, SectionImage.size()))
			{
				int gValue = SectionImage.at<uchar>(Blob_CDList[b].DeviceCenter);
				if ( gValue > 0 )
				{
					if ((int)floor(gValue / 10) == SectionOfInterest)
						numbAligned++;
				}
			}
		}
		if ( numbAligned > maxAligned )
		{
			maxAligned = numbAligned;
			maxRow = r;
			lastMax = r;
		}
		else if ( numbAligned == maxAligned )
		{
			lastMax = r;
		}
		if ( TrackAlignment )
		{
			LogFile << r << "\t" << numbAligned << "\t" << maxRow << "\t" << maxAligned 
				<< "\t" << ScaleIn << "\t" << offset << "\n";
		}
	}
	//for (int n = 2; n < PLimits[1].Limit; n += Step)
	//{
	//	int r = initRowGuess + n;
	//	offset = cv::Point(maxCol, r);
	//	int numbAligned = 0;

	//	for (int b = 0; b < numbBlobs; b++)
	//	{
	//		if (Blob_CDList[b].ShiftThetaR(offset, SectionImage.size()))
	//		{
	//			int gValue = SectionImage.at<uchar>(Blob_CDList[b].DeviceCenter);
	//			if (gValue > 0)
	//			{
	//				if ((int)floor(gValue / 10) == SectionOfInterest)
	//					numbAligned++;
	//			}
	//		}
	//	}
	//	if (numbAligned > maxAligned)
	//	{
	//		maxAligned = numbAligned;
	//		maxRow = r;
	//		lastMax = r;
	//	}
	//	else if (numbAligned == maxAligned)
	//	{
	//		lastMax = r;
	//	}
	//	if (TrackAlignment)
	//	{
	//		LogFile << r << "\t" << numbAligned << "\t" << maxRow << "\t" << maxAligned
	//			<< "\t" << ScaleIn << "\t" << offset << "\n";
	//	}
	//	r = initRowGuess - n;
	//	offset = cv::Point(maxCol, r);
	//	numbAligned = 0;
	//	for (int b = 0; b < numbBlobs; b++)
	//	{
	//		if (Blob_CDList[b].ShiftThetaR(offset, SectionImage.size()))
	//		{
	//			int gValue = SectionImage.at<uchar>(Blob_CDList[b].DeviceCenter);
	//			if (gValue > 0)
	//			{
	//				if ((int)floor(gValue / 10) == SectionOfInterest)
	//					numbAligned++;
	//			}
	//		}
	//	}
	//	if (numbAligned > maxAligned)
	//	{
	//		maxAligned = numbAligned;
	//		maxRow = r;
	//		lastMax = r;
	//	}
	//	else if (numbAligned == maxAligned)
	//	{
	//		lastMax = r;
	//	}
	//	if (TrackAlignment)
	//	{
	//		LogFile << r << "\t" << numbAligned << "\t" << maxRow << "\t" << maxAligned
	//			<< "\t" << ScaleIn << "\t" << offset << "\n";
	//	}
	//}
	LogFile << " lastMax/maxRow = " << lastMax << "/" << maxRow << "\n";
	if ( lastMax > maxRow )
		maxRow = ( 2 * maxRow + lastMax )/3;
	else if ( lastMax < maxRow )
		maxRow = ( maxRow + 2 * lastMax )/3;



	
	int max2Aligned = 0;
	int max2OffPattern = 0;
	int max2OtherSection = 0;
	int theta2Limit = 2 * Step;
	offset.x = maxCol - theta2Limit;
	offset.y = maxRow;
	for (int b = 0; b < numbBlobs; b++)
	{
		if (Blob_CDList[b].ShiftThetaR(offset, SectionImage.size()))
		{
			bool aligned;
			bool offPattern;
			bool otherSection;
			int gValue = SectionImage.at<uchar>(Blob_CDList[b].DeviceCenter);
			if (EvaluateGValue(gValue, SectionOfInterest, 2, sectionImageDivider, SubSectionBands[2],
				Blob_CDList[b].DeviceCenter, aligned, offPattern, otherSection))
			{
				max2Aligned++;
			}
			else if (otherSection)
				max2OtherSection++;
			else if (offPattern)
				max2OffPattern++;
		}
	}
	if (TrackAlignment)
	{
		LogFile << "Subsection 2 column alignment\n";
		LogFile << "   offset = " << offset << " - ScaleIn = " << ScaleIn << " - Step = 1\n";
		LogFile << "Col\tAlig\tOffPat\tMaxCol\tMaxAlig\tMaxOffPat\tScale\tOffset\n";
	}
	lastMax = maxCol;
	for (int n = -thetaLimit + 1; n <= thetaLimit; n++)
	{
		int c = maxCol + n;
		offset = cv::Point(c, maxRow);
		int numbAligned = 0;
		int numbOffPattern = 0;
		int numbOtherSection;
		ColumnMove(Blob_CDList, offset, ScaleIn, SectionOfInterest,
			2, SectionImage, sectionImageDivider, SubSectionBands[2],
			max2Aligned, max2OffPattern, max2OtherSection, maxCol, lastMax, numbAligned, numbOffPattern, numbOtherSection);
		if (TrackAlignment)
		{
			LogFile << c << "\t" << numbAligned << "\t" << numbOffPattern << "\t" << numbOtherSection << "\t" << maxCol
				<< "\t" << max2Aligned << "\t" << max2OffPattern << "\t" << max2OtherSection
				<< "\t" << ScaleIn << "\t" << offset << "\n";
		}
	}
	if (lastMax > maxCol)
		maxCol = (2 * maxCol + lastMax) / 3;
	else
		maxCol = (maxCol + 2 * lastMax) / 3;
	LogFile << "RTShift(SubSection2): maxCol = " << maxCol << ", maxAngle = " 
		<< (double)maxCol * ThetaResolution << "\n";

	checkRT.setTo(0);
	int chkAligned = 0;
	std::vector< int > numbInSection;
	std::vector< int > numbInSubSection;
	numbInSection.resize(10);
	numbInSubSection.resize(10);
	std::fill(numbInSection.begin(), numbInSection.end(), 0);
	std::fill(numbInSubSection.begin(), numbInSubSection.end(), 0);
	offset.x = maxCol;
	offset.y = maxRow;
	for (int b = 0; b < numbBlobs; b++)
	{
		if ( Blob_CDList[b].ShiftThetaR(offset, SectionImage.size()))
		{
			int gValue = SectionImage.at<uchar>(Blob_CDList[b].DeviceCenter);
			if (gValue > 0)
			{
				int sec = (int)floor(gValue / 10);
				int sub = gValue % 10;
				Blob_CDList[b].Section = sec;
				Blob_CDList[b].SubSection = sub;
				numbInSection[sec]++;
				numbInSubSection[sub]++;
				chkAligned++;
			}
			else
			{
				Blob_CDList[b].Section = 0;
				Blob_CDList[b].SubSection = 0;
			}
			if (TrackAlignment)
			{
				checkRect.x = Blob_CDList[b].DeviceCenter.x - 2;
				checkRect.y = Blob_CDList[b].DeviceCenter.y - 2;
				checkRT(checkRect).setTo(100);
			}
		}
	}
	if (TrackAlignment)
	{
		oString.str("");
		oString.clear();
		oString << BaseName << "_156_XY_RT.png";
		cv::imwrite(oString.str(), checkRT);
	}
	LogFile << "RTShift: maxRow = " << maxRow << "\n";
	LogFile << "Numb blobs = " << Blob_CDList.size() << ", maxAligned = " << maxAligned << ", chkAligned = " << chkAligned << std::endl;
	LogFile << "maxCol/Row = " << maxCol << " / " << maxRow << std::endl;
	LogFile << "n , InSec / InSub\n";
	for (int n = 0; n < 10; n++)
	{
		LogFile << n << " , " << numbInSection[n] << " / " << numbInSubSection[n] << "\n";
	}
	Results[0] = maxCol;
	Results[1] = maxRow;
	Results[3] = ScaleIn;
}
