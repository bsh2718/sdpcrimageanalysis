///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//
// bool ShiftImage(const std::vector< double > &Results, cv::Mat &ImageIn, cv::Mat &ShiftedImageOut, std::string &ErrMsg )
//
// bool ShiftImage32F(const std::vector< double > &Results, cv::Mat &ImageIn, cv::Mat &ShiftedImageOut, std::string &ErrMsg )
//
// bool CreateRegImage(const std::vector< double > &Results, cv::Rect SubImageArea, cv::Mat &ImageIn, 
//		cv::Mat &ImageOut, cv::Mat &ShiftedImageOut, std::string &ErrMsg )
//
//
//
//! [includes]
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <fstream>
#include <cstdlib>
#include <algorithm>


#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "calccentroid.hpp"
#include "blob.hpp"
#include "statistics.hpp"
#include "images.hpp"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Shifts image by Results and creates 8-bit image
//
//			Input
// Results - (X-Offset, Y-Offset, Angle, Scale) X-Offset, Y-Offset and Rotation angle applied to Image.
//		Scale in is a change to pixel size to correct for magnification errors and is not used here.
// ImageIn - InputImage (if image is not CV_8UC1, it is converted to it)
//			Output
// ShiftedImageOut - shfited image (CV_8UC1)
// IntensityScale - Scaling factor applied to image intensities
// IntensityMinimum - Minimum value within image.  For 16 bit images, this is subtracted before scaling to fit in 8 bits. 
//		Set to zero if ImageIn is 8-bit
// ErrMsg - If there is a problem, then the 
//
// Return Value - false if there is a problem, description is placed in ErrMsg.  true if OK
//
//
bool ShiftImage(const std::vector< double > &Results, cv::Mat &ImageIn, cv::Mat &ShiftedImageOut, 
	double &IntensityScale, double &IntensityMinimum, std::string &ErrMsg )
{
	if( ImageIn.empty() )					  // Check for invalid input
	{
		ErrMsg = "Could not open or find the image\n" ;
		return false;
	}
	ErrMsg.clear();
	cv::Mat rot_mat( 2, 3, CV_32FC1 );
	cv::Mat image;	
	cv::Mat srcROI;
	cv::Mat dstROI;
	cv::Size imageSize = ImageIn.size();
	cv::Mat image2(imageSize, CV_8UC1);
	ShiftedImageOut.create(imageSize, CV_8UC1);
	image2.setTo(0);
	ShiftedImageOut.setTo(0);
	cv::Mat workImage;
	uchar depth = ImageIn.depth() & CV_MAT_DEPTH_MASK;
	if ( depth == CV_8UC1 )
	{
		image.create(imageSize, CV_8U);
		workImage.create(imageSize, CV_8U);
	}
	else
	{
		image.create(imageSize, CV_32F);
		workImage.create(imageSize, CV_32F);
	}
	// std::clog << "ImageIn.depth = " << depth << std::endl;
	
	std::vector<double> c(3);
	c[0] = Results[0];
	c[1] = Results[1];
	c[2] = -Results[2];		// Reverse the sign of the angle because that is how warpAffine works

	//! [imrotate]
	cv::Point center = cv::Point( 0, 0 );
	double scale = 1.0;
	// std::clog << "Shift: Before getRotationMatrix " << std::endl;
	rot_mat = cv::getRotationMatrix2D( center, c[2], scale );
	// std::clog << "Shift: Before WarpAffine " << std::endl;
	warpAffine(ImageIn, image, rot_mat, cv::Size(ImageIn.cols, ImageIn.rows) );
	cv::Rect srcRect;
	cv::Rect dstRect;
	if ( c[0] >= 0 )
	{
		srcRect.x = 0;
		dstRect.x = (int)round(c[0]);
	}
	else
	{
		srcRect.x = (int)round(abs(c[0]));
		dstRect.x = 0;
	}
	dstRect.width = srcRect.width = imageSize.width - (int)ceil(abs(c[0]));
	int itmp1 = srcRect.x + srcRect.width;
	int itmp2 = dstRect.x + dstRect.width;
	while ( itmp1 > imageSize.width || itmp2 > imageSize.width )
	{
		srcRect.width -= 1;
		dstRect.width -= 1;
		itmp1 = srcRect.x + srcRect.width;
		itmp2 = dstRect.x + dstRect.width;
	}
	
	if ( c[1] >= 0 )
	{
		srcRect.y = 0;
		dstRect.y = (int)round(c[1]);
	}
	else
	{
		srcRect.y = (int)round(abs(c[1]));
		dstRect.y = 0;
	}
	dstRect.height = srcRect.height = imageSize.height - (int)round(abs(c[1]));
	itmp1 = srcRect.y + srcRect.height;
	itmp2 = dstRect.y + dstRect.height;
	while ( itmp1 > imageSize.height || itmp2 > imageSize.height )
	{
		srcRect.height -= 1;
		dstRect.height -= 1;
		itmp1 = srcRect.y + srcRect.height;
		itmp2 = dstRect.y + dstRect.height;
	}
	srcROI = image(srcRect);
	dstROI = workImage(dstRect);
	dstROI.setTo(0.0);
	srcROI.copyTo(dstROI);
	
	if ( depth == CV_8UC1 )
	{
		workImage.copyTo(ShiftedImageOut);
		IntensityScale = 1.0;
		IntensityMinimum = 0.0;
	}
	else
	{
		cv::Mat workImage2(imageSize, CV_8U);
		double minV;
		double maxV;
		cv::minMaxLoc(dstROI, &minV, &maxV, NULL, NULL );
		// std::clog << "minV/maxV " << minV << "/" << maxV << std::endl;
		if ( minV < 0.0 )
		{
			minV = 0.0;
			workImage = cv::max(workImage, 0);
		}
		if ( maxV - minV < 255.0 )
		{	
			workImage -= minV;
			workImage.convertTo(workImage2, CV_8U );
			IntensityMinimum = minV;
			IntensityScale = 1.0;
		}
		else
		{
			std::ostringstream hString;
			hString.str("");
			hString.clear();
			hString << "Image Maximum - Minimum > 255, Negative Values clipped to zero\n";
			hString << "     Min/Max Values in Image " << minV << " / " << maxV;
			IntensityScale = 255.0 / (maxV - minV);
			for ( int n = 0 ; n < ImageIn.total() ; n++ )
				workImage2.at<uchar>(n) = (uchar)round(IntensityScale * ( workImage.at<float>(n) - minV ) );
			IntensityMinimum = minV;
			hString << "\nImage Rescaled with Intensity-scale factor of " << IntensityScale;
			hString << "\n     Min/Max Values in Image before scaling " << minV << " / " << maxV;
			ErrMsg = hString.str();
		}
		workImage2.copyTo(ShiftedImageOut);
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Uses results of alignment procedures and values of ImageSection and ImageAreaOffset to create shifted image and 
// also to crop the shifted image to create image which includes only the array/section pairs of interest.
//
//			Input
// Results - (X-Offset, Y-Offset, Angle, Scale)
// SubImageArea - portion of image to be included in registation image.
// ImageIn - input image
// Convert - if true, converts 32F image to 8UC1 (not used if input image is already 8UC1)
//			Output
// ImageOut - registration image
// ShiftedImageOut - image after alignment, but before cropping to create registration image
// IntensityScale - 
// IntensityMinimum - 
// ErrMsg - 
//
// Return Value - false if there is a problem, description is placed in ErrMsg.  true if OK
bool CreateRegImage(const std::vector< double > &Results, cv::Rect SubImageArea, cv::Mat &ImageIn, bool Convert,
	cv::Mat &ImageOut, cv::Mat &ShiftedImageOut, double &IntensityScale, double &IntensityMinimum, std::string &ErrMsg )
{
	// std::clog << "Inside CreateRegImage" << std::endl;
	cv::Mat image;
	cv::Mat srcROI;
	if( ImageIn.empty() )					  // Check for invalid input
	{
		ErrMsg =  "Could not open or find the image\n";
		return false;
	}
	ErrMsg.clear();
	std::vector<double> c(4);
	c[0] = Results[0] - SubImageArea.x/Results[3];	// adjust shift so ImageSection corresponds to upper corner of image.
	c[1] = Results[1] - SubImageArea.y/Results[3];	//
	c[2] = Results[2];		// Leave angle in degrees because that is what warpAffine expectes.  However,
							// the program ShiftImage() will reverse the sign becuase that is how warpAffine works
	c[3] = Results[3];		// 
	// std::clog << "Inside CreateRegImage" << std::endl;
	// std::clog << "c = " << c[0] << ", " << c[1] << "\t" << c[2] << "\t" << c[3] << std::endl;
	// uchar depth = ImageIn.depth() & CV_MAT_DEPTH_MASK;
	// std::clog << "ImageIn.depth = " << depth << std::endl;
	ImageOut.create(cv::Size(SubImageArea.width, SubImageArea.height), CV_8U);
	ImageOut.setTo(0);
	IntensityScale = 7.77;
	if ( !ShiftImage(c, ImageIn, ShiftedImageOut, IntensityScale, IntensityMinimum, ErrMsg ) )
	{
		ErrMsg = "ShiftImage failed";
		return false;
	}
	
	// std::clog << "Inside CreateRegImage, IntensityScale/Minimum: " << IntensityScale << " / " << IntensityMinimum << std::endl;
	cv::Size shiftSize = ShiftedImageOut.size();
	cv::Rect subROI;
	subROI.x = 0;
	subROI.y = 0;
	
	if ( SubImageArea.width/c[3] > shiftSize.width )
		subROI.width = shiftSize.width;
	else
		subROI.width = (int)round(SubImageArea.width/c[3]);

	if ( SubImageArea.height/c[3] > shiftSize.height )
		subROI.height = shiftSize.height;
	else
		subROI.height = (int)round(SubImageArea.height/c[3]);
	
	// std::clog << "SubImageArea: " << SubImageArea.x << ", " << SubImageArea.y << ", " << SubImageArea.width << ", " << SubImageArea.height << std::endl;
	// std::clog << "   shiftedImageSize: " << shiftSize.width << ", " << shiftSize.height << std::endl;
	srcROI = ShiftedImageOut(subROI);
	srcROI.copyTo(ImageOut);
	// uchar depth2 = srcROI.depth() & CV_MAT_DEPTH_MASK;
	// uchar depth3 = ImageOut.depth() & CV_MAT_DEPTH_MASK;
	// std::clog << "\nCreateRegImage types: " << depth << "/" << depth2 << "/" << depth3 << " - Convert: " << std::boolalpha << Convert << std::endl;
		
	return true;
}
