// For each active Array/Section in device, this program will calculate the average and standard deviation of the
// filling ratios for the accepted droplets (in BlobList) and for droplets in BlobList and OtherBlobList combined.
// The procedure then outputs the results both to std::cout and also to a file named <BaseName>_cout.txt
//
// void OutputSizingResults(std::vector<ArraySection> ArrSecList, std::vector< Blob > &BlobList, std::vector< Blob > &OtherBlobList, std::string BaseName)
//
//     input
// ArrSecList - vector of Array/Section pairs which are being analyzed in this imagesize
// BlobList - vector of Blob, which includes the blobs which were suitable for quantitation
// OtherBlobList - vector of Blob, which contains which are well-like, but don't meet all the criteria to be used for quantitation
// BaseName - output will be sent to std::cout and to a file named <BaseName>_cout.txt
//
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "physicalconstants.h"
#include "statistics.hpp"
#include "blob.hpp"
#include "readinfofile.hpp"
#include "outputSizingResults.hpp"

void OutputSizingResults(std::vector<ArraySection> ArrSecList, std::vector< Blob > &BlobList, std::vector< Blob > &OtherBlobList, std::string BaseName)
{
	std::vector< SizingResults > sResults;
	sResults.clear();
	SizingResults newResult;
	std::ofstream outFile;
	for (int n = 0; n < ArrSecList.size(); n++)
	{
		newResult.Clear();
		int a = ArrSecList[n].Array;
		int s = ArrSecList[n].Section;
		newResult.Array = a;
		newResult.Section = s;
		for (int m = 0; m < BlobList.size(); m++)
		{
			if (BlobList[m].Array == a && BlobList[m].Section == s)
			{
				newResult.FillingFrac.Accumulate(BlobList[m].Shape.FillRatio);
				if (BlobList[m].Shape.FilterValue == 1)
					newResult.GoodFillingFrac.Accumulate(BlobList[m].Shape.FillRatio);
			}
		}
		for (int m = 0; m < OtherBlobList.size(); m++)
		{
			if (OtherBlobList[m].Array == a && OtherBlobList[m].Section == s )
			{
				newResult.FillingFrac.Accumulate(OtherBlobList[m].Shape.FillRatio);
			}
		}
		newResult.FillingFrac.Analyze();
		newResult.GoodFillingFrac.Analyze();
		sResults.push_back(newResult);
	}

	std::string outName = BaseName;
	outName.append("_cout.csv");
	outFile.open(outName.c_str());
	
	std::cout << "Array\tSection\tFV(1)\t\t\tFV(all)\t\t\n";
	std::cout << "\t\tNumbDrop\tFillFrac.Ave\tFillFrac.Std\tNumbDrop\tFilFrac.Ave\tFillFrac.Std\n";
	outFile << "Array,Section,FV1 Numb,FillingFraction,,FV(all) Numb,FillingFraction,\n";
	outFile << ",,Droplets,Ave,Std,Droplets,Ave,Std\n";
	for (int n = 0; n < sResults.size(); n++)
	{
		std::cout << sResults[n].Array << "\t" << sResults[n].Section << "\t"
			<< sResults[n].GoodFillingFrac.Count() << "\t" << sResults[n].GoodFillingFrac.Ave() << "\t" << sResults[n].GoodFillingFrac.Std()
			<< "\t" << sResults[n].FillingFrac.Count() << "\t" << sResults[n].FillingFrac.Ave() << "\t" << sResults[n].FillingFrac.Std()
			<< "\n";
		outFile << sResults[n].Array << "," << sResults[n].Section << ","
			<< sResults[n].GoodFillingFrac.Count() << "," << sResults[n].GoodFillingFrac.Ave() << "," << sResults[n].GoodFillingFrac.Std()
			<< "," << sResults[n].FillingFrac.Count() << "," << sResults[n].FillingFrac.Ave() << "," << sResults[n].FillingFrac.Std()
			<< "\n";
	}
	std::cout << "%%%%%" << std::endl;
	outFile << "%%%%%" << std::endl;
	outFile.close();
}