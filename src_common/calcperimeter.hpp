#ifndef CALCPERIMETER_HPP
#define CALCPERIMETER_HPP

#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

double CalcPerimeter(std::vector< cv::Point > &Contour);

#endif  // CALCPERIMETER_HPP