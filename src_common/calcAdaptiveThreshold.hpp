#ifndef CALCADAPTIVETHRESHOLD_H
#define CALCADAPTIVETHRESHOLD_H

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "physicalconstants.h"
#include "analysisParameters.hpp"

double CalcAdaptiveThreshold( std::ofstream &LogFile,
	cv::Mat &Image, AnalysisParameters &FParams, int Set, std::string &BaseName, int &BlockSize, double MinimumPeak,
	double &C, cv::Mat &Foreground8U, bool Track );
	
#endif // CALCADAPTIVETHRESHOLD_H
