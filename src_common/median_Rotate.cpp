// Procedure to apply median filter to image and rotate the image 90 degrees clockwise.  
//
// void MedianFilter_RotateImage(cv::Mat &WorkImage1, cv::Mat &Image, bool UseMedianFilter, bool RotateImage, bool Track, bool EightBit, std::string BaseName )
//
//     input
// WorkImage1 - work space
// Image - Image to be filtered and/or rotated.
// UseMedianFilter - filter image if true.
// RotateImage - rotate image 90 degrees clockwise if true
// Track - output rotated image if Track and RotateImage are both true.
// EightBit - true for 8 bit image, false for 16 bit image.
// Basename - base name used if rotated image is output
//
//     output
// Image - the transformed image is output here.
//
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "physicalconstants.h"
#include "median_Rotate.hpp"

void MedianFilter_RotateImage(cv::Mat &WorkImage1, cv::Mat &Image, bool UseMedianFilter, bool RotateImage, bool Track, bool EightBit, std::string BaseName )
{
	if ( UseMedianFilter )
	{
		cv::medianBlur(Image, WorkImage1, 3 );
		WorkImage1.copyTo(Image);
	}
	if ( RotateImage )
	{
		WorkImage1 = Image.t();
		int nrows = WorkImage1.rows;
		int mcols = WorkImage1.cols;
		int mcolsLimit = mcols/2;
		for ( int n = 0 ; n < WorkImage1.rows ; n++ )
		{
			for ( int m = 0; m < mcolsLimit ; m++ )
			{
				int mm = mcols - m -1;
				if ( EightBit )
				{
					int tr = WorkImage1.at<uchar>(cv::Point(m,n));
					WorkImage1.at<uchar>(cv::Point(m,n)) = WorkImage1.at<uchar>(cv::Point(mm,n));
					WorkImage1.at<uchar>(cv::Point(mm,n)) = tr;
				}
				else
				{
					int tr = WorkImage1.at<short unsigned int>(cv::Point(m,n));
					WorkImage1.at<short unsigned int>(cv::Point(m,n)) = WorkImage1.at<short unsigned int>(cv::Point(mm,n));
					WorkImage1.at<short unsigned int>(cv::Point(mm,n)) = tr;
				}
			}
		}
		WorkImage1.copyTo(Image);
		WorkImage1.release();
		if ( Track )
		{
			std::string outName = BaseName;
			outName.append("_001Rotated.png"); 
			cv::imwrite( outName, Image );
		}
	}
}


