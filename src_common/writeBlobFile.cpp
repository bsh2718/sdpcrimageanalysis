///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Writes information about blobs.  
// BlobFileName is the file to be written to
// BlobList is a vector of Blob to be written.  
// HalfWellHeight is the wellSize.height/2 in pixels and is used in sorting the blobs (blobs with y coordinates within
//		HalfWellHeight of each other are considered to be in the same row and are sorted instead by x coordinate)
// The procedures return false if a problem is encountered and in that case ErrorMsg is a description of the error. 
//
// 	The analysis program assigns a series of coordinates to the geometric center of each blob.  The location of the
// 	blob in the image changes as the the image is analyzed and the different coordinates track these changes. 
//	The coordinates which end in 2f are cv::Point2f, otherwise they are cv::Point
//		InputCenter2f - center of blob in background subtracted imagesize
//		RotatedCenter2f - center of blob in image corrected for angle
//		AdjustedCenter2f - RotatedCenter2f after adjustment for image distortion
//		DeviceCenter - the program will next adjust each individual block of blobs to see if that improves the alignment
//			of the blobs.  If it does, then this is the center of the blob after this block adjustment.  If it doesn't
//			improve the alignment, then this equals PrelimCenter2f.  Like PrelimCenter2f, this position should only be
//			used with CoordImage.  This step is not done in the DC version of the program and so the WriteBlobListDevice()
//			is not used with it.
//
//	The different procedures sort the blobs differently and output different information. There are different sort() 
//	procedures for each write procedure.  The first entry for each blob is just its position the list after sorting (N).
//	Coordinate pairs are always written in the order (row, col) and rectangles are always written in the order
//	(X0, Y0, Width, Height)
//
//
// bool WriteBlobListInput(std::string BlobFileName, std::vector<Blob> &BlobList, double HalfWellHeight,
//	std::string &ErrorMsg ) // Uses SortBlobsByInputCenter()
//		Sorts blobs by InputCenter2f.y then InputCenter2f.x. InputCenter2f.y is used only if the differences between
//		InputCenter2f.y for the two blobs exceeds HalfWellHeight.  The program outputs
//			N, nputCenter2f, BoundingRect, Contour Length, Perimeter, Area, Moment00, Circularity, FilterValue
//
//
// bool WriteBlobListRotated(std::string BlobFileName, std::vector<Blob> &BlobList, double HalfWellHeight,
//	std::string &ErrorMsg )	// Uses SortBlobsByRotatedCenter()
//		Sorts blobs by AdjustedCenter2f.y then AdjustedCenter2f.x. AdjustedCenter2f.y is used only if the differences between
//		AdjustedCenter2f.y for the two blobs exceeds HalfWellHeight. The program outputs
//			N, AdjustedCenter2f, InputCenter2f, BoundingRect, Contour Length, Perimeter, Area, Moment00, Circularity, FilterValue
//
//
// bool WriteBlobListDevice(std::string BlobFileName, std::vector<Blob> &BlobList, double HalfWellHeight,
//	std::vector< WellBlock > &BlockList, std::string &ErrorMsg )	// Uses SortBlobsByPosition()
//		Similar to WriteBlobListPrelim().  This procedure outputs
//			N, BlockListIndex, ZeroZeroCenter2f, Array, Section, SectionPosition, DeviceCenter2f, PrelimCenter2f,
//				ModelCenter2f, RawPosition, AdjustedCenter2f, InputCenter2f, BoundingRect, Contour Length, Perimeter,	
//				Area, Moment00, Circularity, FilterValue
// 		If the blob is not assigned to a block, then ZeroZeroCenter2f, Array, Section, SectionPosition, DeviceCenter2f, 
//		PrelimCenter2f, ModelCenter2f, and RawPosition are all left blank
//
//		
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//! [includes]
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <string>
#include <sstream>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>

#include "blob.hpp"
#include "readinfofile.hpp"
//#include "columnSlope.hpp"
#include "writeBlobFile.hpp"

double sortDelta = 7.0;
bool SortBlobs(Blob A, Blob B) 
{
	if ( A.Array != 0 && B.Array != 0 )
	{
		if ( A.Array != B.Array )	
		return A.Array < B.Array;
		else if ( A.Section != B.Section )
			return A.Section < B.Section;
		else if ( A.SectionPosition.y != B.SectionPosition.y )
			return A.SectionPosition.y < B.SectionPosition.y;
		else
			return A.SectionPosition.x < B.SectionPosition.x;
	}
	else if ( A.Array == 0 && B.Array != 0 )
		return false;
	else if ( A.Array != 0 && B.Array == 0 )
		return true;
	else
	{
		if ( fabs( A.InputCenter2f.y - B.InputCenter2f.y ) > sortDelta )
			return A.InputCenter2f.y < B.InputCenter2f.y;
		else
			return A.InputCenter2f.x < B.InputCenter2f.x;
	}
}
bool SortBlobsCols(Blob A, Blob B)
{
	if (A.Array != 0 && B.Array != 0)
	{
		if (A.Array != B.Array)
			return A.Array < B.Array;
		else if (A.Section != B.Section)
			return A.Section < B.Section;
		else if (A.SectionPosition.x != B.SectionPosition.x)
			return A.SectionPosition.x < B.SectionPosition.x;	
		else
			return A.SectionPosition.y < B.SectionPosition.y;
	}
	else if (A.Array == 0 && B.Array != 0)
		return false;
	else if (A.Array != 0 && B.Array == 0)
		return true;
	else
	{
		if (fabs(A.InputCenter2f.x - B.InputCenter2f.x) > sortDelta)
			return A.InputCenter2f.x < B.InputCenter2f.x;
		else
			return A.InputCenter2f.y < B.InputCenter2f.y;
	}
}

bool SortBlobsByInputCenter(Blob A, Blob B) 
{	
	if ( fabs( A.InputCenter2f.y - B.InputCenter2f.y ) > sortDelta )
		return A.InputCenter2f.y < B.InputCenter2f.y;
	else
		return A.InputCenter2f.x < B.InputCenter2f.x;
}

bool SortBlobsByRotatedCenter(Blob A, Blob B) 
{	
	if ( fabs( A.AdjustedCenter2f.y - B.AdjustedCenter2f.y ) > sortDelta )
		return A.AdjustedCenter2f.y < B.AdjustedCenter2f.y;
	else
		return A.AdjustedCenter2f.x < B.AdjustedCenter2f.x;
}

bool WriteBlobListInput(std::string BlobFileName, std::vector<Blob> &BlobList, double HalfWellHeight, std::string &ErrorMsg )
{
	ErrorMsg.clear();
	std::ofstream blobOut;	
	std::ostringstream oString;
	std::ostringstream hString1;
	std::ostringstream hString2;
	blobOut.open(BlobFileName.c_str());
	if ( !blobOut )
	{
		oString.str("");
		oString.clear();
		oString << "Error opening " << BlobFileName;
		ErrorMsg = oString.str();
		return false;
	}
	if ( HalfWellHeight < 1.5 )
		return false;

	blobOut << BlobList.size() << std::endl;
	
	std::vector< Blob > sortedBlobList = BlobList;
	sortDelta = HalfWellHeight;
	sort(sortedBlobList.begin(), sortedBlobList.end(), SortBlobsByInputCenter);
	
	hString1.str("");
	hString2.str("");
	hString1.clear();
	hString2.clear();
	hString1 << "Index";
	hString2 << "";
	hString1 << ",Input,-";
	hString2 << ",Row,Col";
	hString1 << ",Bounding Rect,,,";
	hString2 << ",X,Y,Width,Height";
	hString1 << ",Contour,Perimeter";
	hString2 << ",Length,";
	hString1 << ",Area,Mom_00";
	hString2 << ",,";
	hString1 << ",Circularity,Filter";
	hString2 << ",,Value";
	hString1 << ",Self";
	hString2 << ",Index";
	
	blobOut << hString1.str() << "\n";
	blobOut << hString2.str() << "\n";
	
	for ( int n = 0 ; n < (int)sortedBlobList.size() ; n++ )
	{		
		blobOut << sortedBlobList[n].SelfIndex;
		blobOut << "," << sortedBlobList[n].InputCenter2f.y << "," << sortedBlobList[n].InputCenter2f.x;
		blobOut << "," << sortedBlobList[n].Shape.BoundingRect.x << "," << sortedBlobList[n].Shape.BoundingRect.y;
		blobOut << "," << sortedBlobList[n].Shape.BoundingRect.width << "," << sortedBlobList[n].Shape.BoundingRect.height;
		blobOut << "," << sortedBlobList[n].Shape.Contour.size() << "," << sortedBlobList[n].Shape.Perimeter;
		blobOut << "," << sortedBlobList[n].Shape.PixelList.size() << "," << sortedBlobList[n].Shape.Mom.m00;
		blobOut << "," << sortedBlobList[n].Shape.Circularity << "," << sortedBlobList[n].Shape.FilterValue;
		blobOut << "," << sortedBlobList[n].SelfIndex;
		blobOut << "\n";
	}
	
	blobOut.close();
	return true;
}

bool WriteBlobListRotated(std::string BlobFileName, std::vector<Blob> &BlobList, double HalfWellWidth, std::string &ErrorMsg )
{
	ErrorMsg.clear();
	std::ofstream blobOut;	
	std::ostringstream oString;
	std::ostringstream hString1;
	std::ostringstream hString2;
	blobOut.open(BlobFileName.c_str());
	if ( !blobOut )
	{
		oString.str("");
		oString.clear();
		oString << "Error opening " << BlobFileName;
		ErrorMsg = oString.str();
		return false;
	}
	if ( HalfWellWidth < 1.5 )
		return false;

	blobOut << BlobList.size() << std::endl;
	
	std::vector< Blob > sortedBlobList = BlobList;
	sortDelta = HalfWellWidth;
	sort(sortedBlobList.begin(), sortedBlobList.end(), SortBlobsByRotatedCenter);
	
	hString1.str("");
	hString2.str("");
	hString1.clear();
	hString2.clear();
	hString1 << "Index";
	hString2 << "";
	hString1 << ",Rotated,-";
	hString2 << ",Row,Col";
	hString1 << ",Device,-";
	hString2 << ",Row,Col";
	hString1 << ",Input,-";
	hString2 << ",Row,Col";
	hString1 << ",Array,Section";
	hString2 << ",,";
	hString1 << ",Section Position,";
	hString2 << ",Row,Col";
	hString1 << ",Bounding Rect,,,";
	hString2 << ",X,Y,Width,Height";
	hString1 << ",Contour,Perimeter";
	hString2 << ",Length,";
	hString1 << ",Area,Mom_00";
	hString2 << ",,";
	hString1 << ",Circularity,Filter";
	hString2 << ",,Value";
	hString1 << ",Self";
	hString2 << ",Index";
	
	blobOut << hString1.str() << "\n";
	blobOut << hString2.str() << "\n";
	
	for ( int n = 0 ; n < (int)sortedBlobList.size() ; n++ )
	{		
		blobOut << sortedBlobList[n].SelfIndex;
		blobOut << "," << sortedBlobList[n].AdjustedCenter2f.y << "," << sortedBlobList[n].AdjustedCenter2f.x;
		blobOut << "," << sortedBlobList[n].DeviceCenter.y << "," << sortedBlobList[n].DeviceCenter.x;
		blobOut << "," << sortedBlobList[n].InputCenter2f.y << "," << sortedBlobList[n].InputCenter2f.x;
		blobOut << "," << sortedBlobList[n].Array << "," << sortedBlobList[n].Section;
		blobOut << "," << sortedBlobList[n].SectionPosition.y << "," << sortedBlobList[n].SectionPosition.x;
		blobOut << "," << sortedBlobList[n].Shape.BoundingRect.x << "," << sortedBlobList[n].Shape.BoundingRect.y;
		blobOut << "," << sortedBlobList[n].Shape.BoundingRect.width << "," << sortedBlobList[n].Shape.BoundingRect.height;
		blobOut << "," << sortedBlobList[n].Shape.Contour.size() << "," << sortedBlobList[n].Shape.Perimeter;
		blobOut << "," << sortedBlobList[n].Shape.PixelList.size() << "," << sortedBlobList[n].Shape.Mom.m00;
		blobOut << "," << sortedBlobList[n].Shape.Circularity << "," << sortedBlobList[n].Shape.FilterValue;
		blobOut << "," << sortedBlobList[n].SelfIndex;
		// blobOut << "," << sortedBlobList[n].ModelCenter2f.y << "," << sortedBlobList[n].ModelCenter2f.x;
		blobOut << "\n";
	}
	
	blobOut.close();
	return true;
}

