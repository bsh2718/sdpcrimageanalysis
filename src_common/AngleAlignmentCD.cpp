///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// bool AngleAlignment(std::ofstream &LogFile, std::vector< Blob > &BlobList, 
//	cv::Size SectionImageSize, cv::Size OptimalFFTSize, ParameterLimit ALimit, bool UseAssigned,
//	double &ShiftMiddle, int &ShiftMiddleResult,
//	std::string &StatusMsg, std::string BaseName, bool TrackAngle )
//
// Procedure to adjust the angle (yaw) of the image as part of the alignment procedure. 
//	When the grid of points is transformed it turns into a grid of boxes.  If the image of centroids
//	is rotated, then the grid of boxes will be rotated.  The rotation angle necessary to align the image is
//	derermined iterately by estimating the rotation angle of the grid of boxes. A new image of centroids is
//	created incorporating the rotation angle. This continues until the rotation angle converges.
//
// Input
//	LogFile - ofstream connected to log file.
//	BlobList - vector of the structure Blob.  Needed for the centroids of the well-like blobs in the image.
//	SectionImageSize - Size of input image.
//	OptimalFFTSize - Must be greater or equal to SectionImageSize. It was chosen to be a size which the FFT program can
//		efficiently transform.  The image of centroid positions is plsced in an image of this size.
//	ALimit - Structure containing the initial guess for the angle (usually 0) and the limit on the amount it
//		is allowed to change (+/-).
//  UseAssigned - not used.
//	BaseName - Base name for output file names.  Not used if TrackAngle is false.
//	TrackAngle - If true, additional information about alignment is placed in log file and some additional
//		imagea are output.  BaseName is used as part of the output file name.
//
// Output
//	ShiftMiddle - Estimated Rotation angle (degrees).
//	ShiftMiddleResult - Equals zero if the rotation estimation process is successful and is an error code otherwise.
//  StatusMsg - 
//	Return value is true if ShiftMiddleResult == 0, and false otherwise
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
//! [includes]
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "statistics.hpp"
#include "blob.hpp"
#include "blob_CD.hpp"
#include "guardErrors.hpp"
#include "readinfofile.hpp"

#include "AngleAlignmentCD.hpp"
//! [includes]

//! [namespace]
// using namespace std;
//using namespace cv;
//! [namespace]

void FFTImage(cv::Mat &CImage, cv::Size OptimalFFTSize, cv::Point FFTImageCenter, cv::Mat &MagI)
{
	cv::Mat padded;
	// std::cout << "CImage.size() " << CImage.size() << std::endl;
	cv::copyMakeBorder(CImage, padded, 0, OptimalFFTSize.height-CImage.rows, 0, OptimalFFTSize.width - CImage.cols, cv::BORDER_CONSTANT, cv::Scalar::all(0));
	// std::cout << "padded.size() " << padded.size() << std::endl;
	cv::Mat planes[] = {cv::Mat_<float>(padded), cv::Mat::zeros(padded.size(), CV_32F)};
	padded.release();
	cv::Mat complexI;
	cv::merge(planes, 2, complexI);         // Add to the expanded another plane with zeros

	cv::dft(complexI, complexI);            // this way the result may fit in the source matrix

	cv::split(complexI, planes);                   // planes[0] = Re(DFT(I), planes[1] = Im(DFT(I))
	complexI.release();
	cv::magnitude(planes[0], planes[1], planes[0]);// planes[0] = magnitude
	// std::cout << "planes[0].size() " << planes[0].size() << ", MagI.size() " << MagI.size() << std::endl;	
	planes[0].copyTo(MagI);

	// crop the spectrum, if it has an odd number of rows or columns
	// MagI = MagI(Rect(0, 0, MagI.cols & -2, MagI.rows & -2));
	if ( MagI.cols % 2 == 1 || MagI.rows % 2 == 1 )
	{
		int newCols;
		int newRows;
		if ( MagI.cols % 2 == 1 )
			newCols = MagI.cols - 1;
		else
			newCols = MagI.cols;
		
		if ( MagI.rows % 2 == 1 )
			newRows = MagI.rows - 1;
		else
			newRows = MagI.rows;
		
		MagI = MagI(cv::Rect(0, 0, newCols, newRows ));
	}
	cv::Mat q0(MagI, cv::Rect(0, 0, FFTImageCenter.x, FFTImageCenter.y));   // Top-Left - Create a ROI per quadrant
	cv::Mat q1(MagI, cv::Rect(FFTImageCenter.x, 0, FFTImageCenter.x, FFTImageCenter.y));  // Top-Right
	cv::Mat q2(MagI, cv::Rect(0, FFTImageCenter.y, FFTImageCenter.x, FFTImageCenter.y));  // Bottom-Left
	cv::Mat q3(MagI, cv::Rect(FFTImageCenter.x, FFTImageCenter.y, FFTImageCenter.x, FFTImageCenter.y)); // Bottom-Right

	cv::Mat tmp;			// swap quadrants (Top-Left with Bottom-Right)
	// std::cout << "q0, q3, tmp.size() " << q0.size() << " / " << q3.size() << " / " << tmp.size() << std::endl;
	q0.copyTo(tmp);
	q3.copyTo(q0);
	tmp.copyTo(q3);

	// std::cout << "q1, q2, tmp.size() " << q1.size() << " / " << q2.size() << " / " << tmp.size() << std::endl;
	q1.copyTo(tmp);			// swap quadrant (Top-Right with Bottom-Left)
	q2.copyTo(q1);
	tmp.copyTo(q2);
	
	// std::cout << "At end of FFTImage" << std::endl;
	planes[0].release();
	planes[1].release();
}

bool AngleAlignmentCD(std::ofstream& LogFile, std::vector< Blob_CD >& Blob_CDList,
	cv::Size ImageSize, double DistanceToCenter, double ThetaResolution, std::vector< cv::Point > SubSectionBands,
	cv::Size SectionImageSize, cv::Size OptimalFFTSize, std::vector< ParameterLimit > PLimits,
	double& ShiftMiddle, int& ShiftMiddleResult,
	std::string& StatusMsg, std::string BaseName, bool TrackAngle)
{
	std::ostringstream oString;
	std::ostringstream oString2;
	StatusMsg.clear();
	int bandUpper = SubSectionBands[1].x;
	int bandLower = SubSectionBands[2].y;
	int bandCount;
	int maxBandCount = 0;
	int maxRow = -(int)round(PLimits[1].Limit);
	for (int row = -(int)round(PLimits[1].Limit); row <= +PLimits[1].Limit; row += 3)
	{
		bandCount = 0;
		for (int b = 0; b < Blob_CDList.size(); b++)
		{
			if (Blob_CDList[b].InputTheta_R2f.y + row > bandUpper && Blob_CDList[b].InputTheta_R2f.y + row < bandLower)
				bandCount++;
		}
		if (bandCount > maxBandCount)
		{
			maxBandCount = bandCount;
			maxRow = row;
		}
		if (TrackAngle)
		{
			LogFile << row << ", " << bandCount << ", " << maxRow << ", " << maxBandCount << "\n";
		}
	}
	double radialShift = maxRow;

	double imageMiddle = (double)(ImageSize.width) * 0.5;
	cv::Mat cImage(SectionImageSize, CV_8U);
	cImage.setTo(0);
	cv::Rect checkRect;
	checkRect.width = 5;
	checkRect.height = 5;
	cv::Mat workImage(cImage.size(), CV_8U);
	workImage.setTo(0);
	for (int n = 0; n < Blob_CDList.size(); n++)
	{
		if (Blob_CDList[n].RecalculateAdjustedThetaR(DistanceToCenter, imageMiddle, ThetaResolution, 0.0, radialShift, SectionImageSize))
		{
			cImage.at<uchar>(Blob_CDList[n].DeviceCenter) = 128;
			if (TrackAngle)
			{
				checkRect.x = Blob_CDList[n].DeviceCenter.x - 2;
				checkRect.y = Blob_CDList[n].DeviceCenter.y - 2;
				workImage(checkRect).setTo(128);
			}
		}

	}
	if (TrackAngle)
	{
		std::string outName;
		oString.str("");
		oString.clear();
		oString << BaseName << "_153_RT_Before.png";
		outName = oString.str();
		cv::imwrite(outName, workImage);
		workImage.release();
	}
	//std::clog << "Inside angleScaleAlignment 1" << std::endl;
	cv::Point2f fftCenter;
	fftCenter.x = (float)(OptimalFFTSize.width * 0.5);
	fftCenter.y = (float)(OptimalFFTSize.height * 0.5);
	cv::Point fftImageCenter;
	fftImageCenter.x = (int)floor(fftCenter.x);
	fftImageCenter.y = (int)floor(fftCenter.y);

	// double angleLimit = Alimit.Limit / 57.29578;
	double angleLimit = 0.035; // 2 degrees
	int minY = 5; // 3;
	double sinAngleLimit = sin(angleLimit);
	int maxY = (int)(sin(angleLimit) * (double)OptimalFFTSize.width * 0.5 + minY);
	cv::Mat horzMask;
	cv::Mat vertMask;
	
	//std::clog << "Inside angleScaleAlignment 2" << std::endl;
	double epsilon = 0.005;
	double hAngle = 1.0;
	double vAngle = -1.0;
	std::vector<double> slope(2);
	std::vector<int> count(2);
	std::vector<double> hAngleList;
	std::vector<double> vAngleList;
	std::vector<double> angleList;
	std::vector<double> cumAngleList;
	std::vector<double> middleShiftList;
	hAngleList.clear();
	vAngleList.clear();
	angleList.clear();	
	cumAngleList.clear();
	middleShiftList.clear();
	//
	std::vector<bool> success;
	success.clear();
	int maxIter = 9;
	cv::Mat magI;

	//oString2.str("");
	//oString2.clear();
	//oString2 << "Tracking angle alignment of image" << "\n"; //, Angle Limit = " << ALimit.Limit << "\n";
	//oString2 << "Iteration";
	//oString2 << "\tHorizontal Fit\t\t\t";
	//oString2 << "\tVertical Fit\t\t\t";
	//oString2 << "\tAverage\tCum\t\n";
	//oString2 << "\tCount\tSlope\tAng\tAng(deg)";
	//oString2 << "\tCount\tSlope\tAng\tAng(deg)";
	//oString2 << "\tAng\tAng(deg)\tAng\tAng(deg)";

	bool contin = true;
	double aThreshold;
	cv::Point iPeak;
	double peak = 0.0;
	//std::cout << "Before create workImage1/2" << std::endl;
	cv::Mat workImage1 = cv::Mat::zeros(magI.size(), CV_8U);
	cv::Mat workImage2 = cv::Mat::zeros(magI.size(), CV_8U);
	while (contin && angleList.size() < maxIter)
	{
		FFTImage(cImage, OptimalFFTSize, fftImageCenter, magI);
		if (angleList.size() == 0)
		{
			//std::cout << "     magI.size() = " << magI.size() << std::endl;
			//std::cout << " fftCenter.x/y = " << fftCenter.x << " / " << fftCenter.y << std::endl;
			horzMask.create(magI.size(), CV_8U);
			horzMask.setTo(0);
			for (int n = (int)(0.5 * fftCenter.x); n < fftCenter.x; n++)
			{
				maxY = (int)abs(sinAngleLimit * (fftCenter.x - (double)n)) + minY;
				for (int m = (int)fftCenter.y - maxY; m <= fftCenter.y + maxY; m++)
				{
					horzMask.at<uchar>(cv::Point(n, m)) = 1;
					if (n < fftCenter.x - 1)
						horzMask.at<uchar>(cv::Point(magI.cols - n - 1, m)) = 1;
				}
			}
			vertMask.create(magI.size(), CV_8U);
			vertMask.setTo(0);
			for (int n = (int)(0.5 * fftCenter.y); n < fftCenter.y; n++)
			{
				maxY = (int)abs(sinAngleLimit * (fftCenter.y - (double)n)) + minY;
				for (int m = (int)fftCenter.x - maxY; m <= fftCenter.x + maxY; m++)
				{
					vertMask.at<uchar>(cv::Point(m, n)) = 1;
					if (n < fftCenter.y - 1)
						vertMask.at<uchar>(cv::Point(m, magI.rows - n - 1)) = 1;
				}
			}

			for (int n = -1; n <= 1; n++)
			{
				for (int m = -1; m <= 1; m++)
				{
					if (magI.at<float>(cv::Point(n + fftImageCenter.x, m + fftImageCenter.y)) > peak)
					{
						peak = magI.at<float>(cv::Point(n + fftImageCenter.x, m + fftImageCenter.y));
						iPeak = cv::Point(cv::Point(n + fftImageCenter.x, m + fftImageCenter.y));
					}
				}
			}
			aThreshold = peak * 0.10;
			if ( TrackAngle)
				LogFile << "AngleAlignment: aThreshold = " << aThreshold << std::endl;
		}

		workImage1.setTo(0);
		workImage2.setTo(0);
		cv::Mat magIU8;
		if (TrackAngle)
		{
			LogFile << "Trial alignment, peak = " << peak << ", iPeak = " << iPeak << ", fftImageCenter = " << fftImageCenter << ", amplitude threshold = " << aThreshold << std::endl;
			if (angleList.size() < 2)
			{
				double minVal;
				double maxVal;
				cv::Mat tmpI = magI.clone();
				cv::minMaxLoc(magI, &minVal, &maxVal, NULL, NULL);
				double sca = 255.0 / (maxVal - minVal);
				for (int n = 0; n < magI.total(); n++)
					tmpI.at<float>(n) = (float)(sca * (magI.at<float>(n) - minVal));

				tmpI.convertTo(magIU8, CV_8U);
				std::string outName;
				oString.str("");
				oString.clear();
				oString << BaseName << "_026fft" << (int)angleList.size() << "_.png";
				outName = oString.str();
				cv::imwrite(outName, magIU8);

				LogFile << "     scaled threshold " << sca * (aThreshold - minVal) << std::endl;
			}
		}
		cv::threshold(magI, workImage1, aThreshold, 255, cv::THRESH_BINARY);
		workImage2.setTo(0);
		workImage1.copyTo(workImage2, horzMask);
		std::vector< cv::Point > locations;
		locations.clear();
		cv::findNonZero(workImage2, locations);
		if (locations.size() > 3)
		{
			double sumXY = 0.0;
			double sumX2 = 0.0;
			for (int n = 0; n < locations.size(); n++)
			{
				double mag = magI.at<float>(locations[n]);
				sumXY += (locations[n].x - (double)fftImageCenter.x) * (locations[n].y - (double)fftImageCenter.y) * mag;
				sumX2 += pow(locations[n].x - fftImageCenter.x, 2) * mag;
			}
			double altSlope = sumXY / sumX2;
			slope[0] = altSlope;
			count[0] = (int)locations.size();
			hAngle = -atan(slope[0]);
			if (TrackAngle)
			{
				LogFile << "Trial SlopeH = " << altSlope << ", from " << locations.size() << " points" << std::endl;
			}
		}
		else
		{
			LogFile << "only " << locations.size() << " points above amplitude threshold of " << aThreshold << std::endl;
		}
		workImage2.setTo(0);
		workImage1.copyTo(workImage2, vertMask);
		locations.clear();
		cv::findNonZero(workImage2, locations);
		if (locations.size() > 30)
		{
			double sumXY = 0.0;
			double sumX2 = 0.0;
			for (int n = 0; n < locations.size(); n++)
			{
				double mag = magI.at<float>(locations[n]);
				sumXY += (locations[n].y - (double)fftImageCenter.y) * (locations[n].x - (double)fftImageCenter.x) * mag;
				sumX2 += pow(locations[n].y - (double)fftImageCenter.y, 2) * mag;
			}
			double altSlope = sumXY / sumX2;
			slope[1] = altSlope;
			count[1] = (int)locations.size();
			vAngle = atan(slope[1]);
			if (TrackAngle)
			{
				LogFile << "Trial SlopeV = " << altSlope << ", from " << locations.size() << " points" << std::endl;
			}
		}
		else
		{
			LogFile << "only " << locations.size() << " points above amplitude threshold of " << aThreshold << std::endl;
		}
		double ang;
		bool suc = true;
		if (count[0] > 3 && count[1] > 3)
			ang = 0.5 * (hAngle + vAngle);
		else if (count[0] > 3)
		{
			ang = 0.5 * hAngle;
			vAngle = 0.0;
		}
		else if (count[1] > 3)
		{
			ang = 0.5 * vAngle;
			hAngle = 0.0;
		}
		else
		{
			ang = 0.0;
			hAngle = 0.0;
			vAngle = 0.0;
			suc = false;
		}
		LogFile << "suc = " << std::boolalpha << suc << ", ang = " << ang << std::endl;
		LogFile << "     count[0]/[1] = " << count[0] << " / " << count[1] << std::endl;
		angleList.push_back(ang);
		hAngleList.push_back(hAngle);
		vAngleList.push_back(vAngle);
		double next;
		int currIdx = (int)cumAngleList.size() - 1;
		if (cumAngleList.size() == 0)
		{
			next = ang;
		}
		else if (currIdx < 2)
		{
			next = ang + cumAngleList[currIdx];
		}
		else
		{
			next = ang * 0.67 + cumAngleList[currIdx];
		}
		cumAngleList.push_back(next);
		success.push_back(suc);
		oString2.str("");
		oString2.clear();
		oString2 << angleList.size();
		if (count[0] > 3)
		{
			oString2 << "\t" << count[0] << "\t " << slope[0] << "\t" << hAngle << "\t" << hAngle * 57.29578;
		}
		else
		{
			oString2 << "\t" << count[0] << "\t\t\t";
		}

		if (count[1] > 3)
		{
			oString2 << "\t" << count[1] << "\t " << slope[1] << "\t" << vAngle << "\t" << vAngle * 57.29578;
		}
		else
		{
			oString2 << "\t" << count[1] << "\t\t\t";
		}
		oString2 << "\t" << ang << "\t" << ang * 57.29578 << "\t" << next << "\t" << next * 57.29578;
		LogFile << oString.str() << std::endl;
		double middleShift = -tan(2.0 * next) * DistanceToCenter;
		middleShiftList.push_back(middleShift);
		if (suc)
		{
			int idx = (int)cumAngleList.size() - 1;
			double cosAngle = cos(cumAngleList[idx]);
			double sinAngle = sin(cumAngleList[idx]);
			if (idx > 0)
			{
				oString.str(""); oString.clear(); oString << "     prev/curr Angle = " << angleList[(size_t)idx - 1] << "/" << angleList[idx]
					<< "     prev/curr CumAngle = " << cumAngleList[(size_t)idx - 1] << "/" << cumAngleList[idx]
					<< "     h/vAngle = " << hAngle << "/" << vAngle << std::endl;
			}
			else
			{
				oString.str(""); oString.clear(); oString << "     curr Angle = " << angleList[idx] << "     curr cumAngle = " << cumAngleList[idx]
					<< "     h/vAngle = " << hAngle << "/" << vAngle << std::endl;
			}
			LogFile << oString.str() << std::endl;
			cImage.setTo(0);
			LogFile << "pass " << angleList.size() << ", middleShift = " << middleShift << std::endl;
			
			int numbInImage2 = 0;
			for (int b = 0; b < Blob_CDList.size(); b++)
			{
				if (Blob_CDList[b].CalculateAdjustedThetaR(DistanceToCenter, imageMiddle, ThetaResolution, middleShift, SectionImageSize))
				{
					numbInImage2++;
					cImage.at<uchar>(Blob_CDList[b].DeviceCenter) = 128;
				}
			}
			if (idx > 1)
			{
				if (fabs(cumAngleList[idx] - cumAngleList[(size_t)idx - 1]) < epsilon)
					contin = false;
			}
		}
		else
		{
			LogFile << "\n\nAngleAlignment() could not find any peaks!!\n" << std::endl;
			contin = false;
		}
	}
	int last = (int)cumAngleList.size() - 1;
	oString.str("");
	oString.clear();

	
	if ( last > 0 && success[last] && cumAngleList.size() < maxIter )
	{
		ShiftMiddle = -tan(2.0 * cumAngleList[last]) * DistanceToCenter;
		ShiftMiddleResult = 0;
		oString << "Successive ffts converged after " << angleList.size() << " iterations\n";
		oString << "Final Middle Shift = " << ShiftMiddle << ", In final iteration hAngle = " << hAngle << ", vAngle = " << vAngle << "\n";
	}
	else if ( last == 0 )
	{
		// angle = 0.0;
		ShiftMiddle = 0.0;
		oString << "No peaks found!!  ShiftMiddle set to 0.0\n";
		ShiftMiddleResult = -1;
	}
	else if ( success[last] )
	{
		ShiftMiddle = -tan(2.0 * cumAngleList[last]) * DistanceToCenter;
		ShiftMiddleResult = 1;
		oString << "Successive ffts did not converged after " << angleList.size() << " iterations\n";
		oString << "Final Middle Shift = " << ShiftMiddle << ", In final iteration hAngle = " << hAngle << ", vAngle = " << vAngle << "\n";
	}
	else
	{
		ShiftMiddleResult = 2;
		int lastSuccess = last;
		while (lastSuccess > 0 )
		{
			if ( success[lastSuccess] )
				break;
			
			lastSuccess--;
		}
		if ( lastSuccess < 0 )
		{
			ShiftMiddle = 0.0;
			oString << "ffts failed at all steps\n";
			ShiftMiddleResult = -1;
		}
		else
		{
			ShiftMiddle = -tan(2.0 * cumAngleList[lastSuccess]) * DistanceToCenter;
			ShiftMiddleResult = 2;
			oString << "Sucessive ffts failed on last iteration: " << angleList.size() << "\n";
			oString << "Last Successful iteration: " << lastSuccess << "\n";
			oString << "Final angle = " << ShiftMiddle << "\n";
		}
	}
	LogFile << oString.str() << std::endl;
	if (TrackAngle)
	{
		LogFile << "pass, angle, hAngle, vAngle, cumAngle, middleShift, success" << std::endl;
		for (int n = 0; n < angleList.size(); n++)
		{
			LogFile << n << ", " << angleList[n] << ", " << hAngleList[n] << ", " << vAngleList[n]
				<< ", " << cumAngleList[n] << ", " << middleShiftList[n] << ", " << success[n] << std::endl;
		}
	}
	for (int b = 0; b < Blob_CDList.size(); b++)
	{
		Blob_CDList[b].RecalculateAdjustedThetaR(DistanceToCenter, imageMiddle, ThetaResolution, ShiftMiddle, radialShift, SectionImageSize);
	}

	if ( ShiftMiddleResult == 0 )
		return true;
	else
		return false;
}

