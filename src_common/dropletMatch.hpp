#ifndef DROPLETMATCH_H
#define DROPLETMATCH_H
//! [includes]

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "physicalconstants.h"
#include "statistics.hpp"
#include "blob.hpp"
#include "readinfofile.hpp"
// #include "segmentVector.hpp"
#include "beforeAfterMatrix.hpp"
// #include "compareDroplets.hpp"
#include "wellExclusionRules.hpp"

//! [includes]

//! [namespace]
// using namespace std;
//using namespace cv;
//! [namespace]

bool ReadDropletCheckFile( std::string ParameterFileName,
	double &ShrinkageLimit, double &BeforeExclusionFactor,
	double &PosNegRatio, double &RatioDefaultDivider, double &StreakExpectancyThreshold,
	std::vector< WellExclusionRules > &ExclusionRules, std::string &ErrorMsg);

void CreateAlignmentImages(std::ofstream &LogFile, std::vector< ArraySection > &ArrSecList, int NumbRows, int NumbCols,
	std::vector< std::vector< std::vector< BlobSummary > > > &BeforeSizingList, 
	std::vector< std::vector< std::vector< BlobSummary > > > &AfterSizingList, 
	std::vector< cv::Mat > &BeforeOccupied, std::vector< cv::Mat > &AfterOccupied );
	
bool AlignDroplets(std::ofstream &LogFile, std::string &BaseName, std::vector< ArraySection > &ArrSecList, int NumbRows, int NumbCols,
	std::vector< std::vector< BlobSummary > > &BeforeSizingList, 
	std::vector< std::vector< BlobSummary > > &AfterSizingList, 
	cv::Point &ShiftResult, std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter, int &MaxNumbMatches );

void EvaluateAlignment(std::ofstream &LogFile, std::vector< std::vector< int > > &ArrSecMatrix, int NumbRows, int NumbCols,
	std::vector< BlobSummary > &BeforeSizingList, std::vector< BlobSummary > &AfterSizingList,
	std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter, 
	std::vector< int > &BeforeColumns, std::vector< int > &BeforeRows,
	std::vector< int > &AfterColumns, std::vector< int > &AfterRows,
	std::vector< int > &BeforeColumnsMissing, std::vector< int > &BeforeRowsMissing,
	std::vector< int > &AfterColumnsMissing, std::vector< int > &AfterRowsMissing,
	int &BeforeFoundInAfter, int &AfterFoundInBefore, int &BeforeNotFoundInAfter, int &AfterNotFoundInBefore,
	StatisticsXY &DeltaXY, int &NumbMatches);
	

	
int ShiftSizingList(std::ofstream &LogFile, int Array, int Section, cv::Size SectionDimens,
	std::vector< BlobSummary > &BeforeSizingList, std::vector< BlobSummary > &AfterSizingList, cv::Point ArraySectionShift );
	
int ShiftSizingList(std::ofstream &LogFile, int Array, int Section, cv::Size SectionDimens,
	std::vector< BlobSummary > &AfterSizingList, cv::Point ArraySectionShift );
	
	
#endif // DROPLETMATCH_H

