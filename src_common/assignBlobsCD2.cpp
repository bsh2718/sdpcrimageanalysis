#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "statistics.hpp"
#include "blob.hpp"
#include "assignBlobsCD2.hpp"
#include "trimRectangle.hpp"
#include "alignImageCD2.hpp"
#include "searchVolumeCD2.hpp"

bool AssignBlobsCD2(std::ofstream& LogFile, std::string BaseName,
	std::vector< double > Results, cv::Size ImageSize, cv::Mat& CoordsImage, 
	std::vector< Blob >& BlobList, std::vector< Blob >& OtherBlobList, std::vector< Blob >& BadBlobList,
	bool TrackAssignment, std::string& ErrMsg)
{
	std::ostringstream oString;
	cv::Mat rotMat = cv::getRotationMatrix2D(SearchVolume::WellTemplateCenter, Results[2], Results[3]);
	cv::Mat transMat = rotMat.clone();
	transMat.setTo(0.0);
	transMat.at<double>(cv::Point(0, 0)) = 1.0;
	transMat.at<double>(cv::Point(1, 1)) = 1.0;
	transMat.at<double>(cv::Point(2, 0)) = Results[0];
	transMat.at<double>(cv::Point(2, 1)) = Results[1];

	cv::Point imageCenter;
	imageCenter.x = ImageSize.width / 2;
	imageCenter.y = ImageSize.height / 2;

	cv::Mat blobMat(SearchVolume::WellTemplateSize, CV_16UC1);
	blobMat.setTo(0);
	for (int b = 0; b < BlobList.size(); b++)
	{
		cv::Point pt;
		pt.x = (int)round(BlobList[b].DeviceCenter.x + SearchVolume::WellTemplateCenter.x) - imageCenter.x;
		pt.y = (int)round(BlobList[b].DeviceCenter.y + SearchVolume::WellTemplateCenter.y) - imageCenter.y;
		if (pt.x >= 0 && pt.x < SearchVolume::WellTemplateSize.width
			&& pt.y >= 0 && pt.y < SearchVolume::WellTemplateSize.height)
		{
			blobMat.at<short unsigned int>(pt) = b + 1;
		}
	}
	cv::Mat workImage1(SearchVolume::WellTemplateSize, CV_16UC1);
	cv::Mat workImage2(SearchVolume::WellTemplateSize, CV_16UC1);
	cv::warpAffine(blobMat, workImage1, rotMat, SearchVolume::WellTemplateSize, cv::INTER_NEAREST, cv::BORDER_CONSTANT, 0);
	cv::warpAffine(workImage1, workImage2, transMat, SearchVolume::WellTemplateSize, cv::INTER_NEAREST, cv::BORDER_CONSTANT, 0);
	std::vector< cv::Point > idxList;
	cv::findNonZero(workImage2, idxList);
	for (int n = 0; n < idxList.size(); n++)
	{
		cv::Vec3b cValue = CoordsImage.at<cv::Vec3b>(idxList[n]);
		int b = workImage2.at<short unsigned int>(idxList[n]) - 1;
		if (b >= 0 && b < BlobList.size())
		{
			if (cValue[0] > 0)
			{
				BlobList[b].Section = cValue[0];
				BlobList[b].SectionPosition.y = cValue[1];
				BlobList[b].SectionPosition.x = cValue[2];
				BlobList[b].Status = 1;
			}
			else
			{
				BlobList[b].Section = 0;
				BlobList[b].SectionPosition.y = 0;
				BlobList[b].SectionPosition.x = 0;
				BlobList[b].Status = 0;
			}
		}
	}

	if (TrackAssignment)
	{
		oString.str("");
		oString.clear();
		oString << BaseName << "_Assign_BlobMat.png";
		cv::imwrite(oString.str(), blobMat);

		oString.str("");
		oString.clear();
		oString << BaseName << "_Assign_workImage2.png";
		cv::imwrite(oString.str(), blobMat);
		

		cv::Mat workImage10 = SearchVolume::WellTemplateImage.clone();
		LogFile << "workImage10.size() = " << workImage10.size() << std::endl;
		LogFile << "WellTemplateSize = " << SearchVolume::WellTemplateSize << std::endl;
		cv::Rect rectMask;
		rectMask.width = 3;
		rectMask.height = 3;
		for (int n = 0; n < idxList.size(); n++)
		{
			//LogFile << "idxList[" << n << "] = " << idxList[n] << std::endl;
			if (idxList[n].x > 0 && idxList[n].x < SearchVolume::WellTemplateSize.width - 1
				&& idxList[n].y > 0 && idxList[n].y < SearchVolume::WellTemplateSize.height - 1)
			{
				rectMask.x = idxList[n].x - 1;
				rectMask.y = idxList[n].y - 1;
				workImage10(rectMask).setTo(250);
			}
			else
			{
				LogFile << "Misplaced blob at (r, c) " << idxList[n].y << ", " << idxList[n].x << std::endl;
			}

		}
		oString.str("");
		oString.clear();
		oString << BaseName << "_Assign_Overlay.png";
		cv::imwrite(oString.str(), workImage10);
	}

	blobMat.setTo(0);
	for (int b = 0; b < OtherBlobList.size(); b++)
	{
		cv::Point pt;
		pt.x = (int)round(OtherBlobList[b].DeviceCenter.x + SearchVolume::WellTemplateCenter.x) - imageCenter.x;
		pt.y = (int)round(OtherBlobList[b].DeviceCenter.y + SearchVolume::WellTemplateCenter.y) - imageCenter.y;
		if (pt.x >= 0 && pt.x < SearchVolume::WellTemplateSize.width
			&& pt.y >= 0 && pt.y < SearchVolume::WellTemplateSize.height)
		{
			blobMat.at<short unsigned int>(pt) = b + 1;
		}
	}
	cv::warpAffine(blobMat, workImage1, rotMat, SearchVolume::WellTemplateSize, cv::INTER_NEAREST, cv::BORDER_CONSTANT, 0);
	cv::warpAffine(workImage1, workImage2, transMat, SearchVolume::WellTemplateSize, cv::INTER_NEAREST, cv::BORDER_CONSTANT, 0);
	cv::findNonZero(workImage2, idxList);
	for (int n = 0; n < idxList.size(); n++)
	{
		cv::Vec3b cValue = CoordsImage.at<cv::Vec3b>(idxList[n]);
		int b = workImage2.at<short unsigned int>(idxList[n]) - 1;
		if (b >= 0 && b < OtherBlobList.size())
		{
			if (cValue[0] > 0)
			{
				OtherBlobList[b].Section = cValue[0];
				OtherBlobList[b].SectionPosition.y = cValue[1];
				OtherBlobList[b].SectionPosition.x = cValue[2];
				OtherBlobList[b].Status = 0;
			}
			else
			{
				OtherBlobList[b].Section = 0;
				OtherBlobList[b].SectionPosition.y = 0;
				OtherBlobList[b].SectionPosition.x = 0;
				OtherBlobList[b].Status = 0;
			}
		}
	}

	blobMat.setTo(0);
	for (int b = 0; b < BadBlobList.size(); b++)
	{
		cv::Point pt;
		pt.x = (int)round(BadBlobList[b].DeviceCenter.x + SearchVolume::WellTemplateCenter.x) - imageCenter.x;
		pt.y = (int)round(BadBlobList[b].DeviceCenter.y + SearchVolume::WellTemplateCenter.y) - imageCenter.y;
		if (pt.x >= 0 && pt.x < SearchVolume::WellTemplateSize.width
			&& pt.y >= 0 && pt.y < SearchVolume::WellTemplateSize.height)
		{
			blobMat.at<short unsigned int>(pt) = b + 1;
		}
	}
	cv::warpAffine(blobMat, workImage1, rotMat, SearchVolume::WellTemplateSize, cv::INTER_NEAREST, cv::BORDER_CONSTANT, 0);
	cv::warpAffine(workImage1, workImage2, transMat, SearchVolume::WellTemplateSize, cv::INTER_NEAREST, cv::BORDER_CONSTANT, 0);
	cv::findNonZero(workImage2, idxList);
	for (int n = 0; n < idxList.size(); n++)
	{
		cv::Vec3b cValue = CoordsImage.at<cv::Vec3b>(idxList[n]);
		int b = workImage2.at<short unsigned int>(idxList[n]) - 1;
		if (b >= 0 && b < BadBlobList.size())
		{
			if (cValue[0] > 0)
			{
				BadBlobList[b].Section = cValue[0];
				BadBlobList[b].SectionPosition.y = cValue[1];
				BadBlobList[b].SectionPosition.x = cValue[2];
				BadBlobList[b].Status = 0;
			}
			else
			{
				BadBlobList[b].Section = 0;
				BadBlobList[b].SectionPosition.y = 0;
				BadBlobList[b].SectionPosition.x = 0;
				BadBlobList[b].Status = 0;
			}
		}
	}
	return true;
}