#ifndef ANALYSISPARAMETERS_H
#define ANALYSISPARAMETERS_H
//! [includes]
#include <vector>
#include <string>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//#include <iostream>
//#include <string>
//#include <cstdlib>
//! [includes]

#include "blob.hpp"

#define NUMBFILTERS 6

// class with two memeber variables.  It is used to hole the minimum and maximum values
// associated with the range over which a parameter is allowed to vary.  These parameters
// are the ones used to align an image with the grid which defines the location and
// identiry of the wells in the device.  Some of the objects of this class contain the
// amount by which a parameter is allowed to range in the negative (Min) and positive (Max)
// direction from the initial guess for that parameter.  Other objects contain the 
// minimum and maximum limits for the parameter.
class MinMaxLimits
{
	public:
		double Min;
		double Max;
		
		MinMaxLimits(): Min(0.0), Max(0.0) {}
		~MinMaxLimits(){}
		
		MinMaxLimits(const MinMaxLimits &Source): Min(Source.Min), Max(Source.Max)
		{}

		MinMaxLimits& operator= ( const MinMaxLimits &Source )
		{
			if ( this !=&Source )
			{
				Min = Source.Min;
				Max = Source.Max;
			}
			return *this;
		}
		
		void Clear()
		{
			Min = 0.0;
			Max = 0.0;
		}
		
		void Set(double MinIn, double MaxIn )
		{
			Min = MinIn;
			Max = MaxIn;
		}
};
//
// 
class MinMaxGroup
{
	public:
		MinMaxLimits WellHeightDelta;
		MinMaxLimits WellWidthDelta;
		MinMaxLimits BlobWidth;
		MinMaxLimits BlobHeight;
		
		MinMaxLimits WellFraction;
		MinMaxLimits BlobArea;
		
		MinMaxLimits Circularity;
		MinMaxLimits AspectRatio;
		
		MinMaxGroup(){}
		~MinMaxGroup(){}
		
		MinMaxGroup(const MinMaxGroup &Source): 
			WellHeightDelta(Source.WellHeightDelta),
			WellWidthDelta(Source.WellWidthDelta),
			BlobWidth(Source.BlobWidth),
			BlobHeight(Source.BlobHeight),
			WellFraction(Source.WellFraction),
			BlobArea(Source.BlobArea),
			Circularity(Source.Circularity),
			AspectRatio(Source.AspectRatio)
			{}

		MinMaxGroup& operator= ( const MinMaxGroup &Source )
		{
			if ( this !=&Source )
			{
				WellHeightDelta = Source.WellHeightDelta;
				WellWidthDelta = Source.WellWidthDelta;
				BlobWidth = Source.BlobWidth;
				BlobHeight = Source.BlobHeight;
				WellFraction = Source.WellFraction;
				BlobArea = Source.BlobArea;
				Circularity = Source.Circularity;
				AspectRatio = Source.AspectRatio;
			}
			return *this;
		}
		
		void Clear()
		{
			WellHeightDelta.Clear();
			WellWidthDelta.Clear();
			BlobWidth.Clear();
			BlobHeight.Clear();
			WellFraction.Clear();
			BlobArea.Clear();
			Circularity.Clear();
			AspectRatio.Clear();
		}
		
		void SetHeightDelta(MinMaxLimits HeightDelta, double WellHeight )
		{
			WellHeightDelta = HeightDelta;
			BlobHeight.Min = WellHeight - HeightDelta.Min;
			if ( BlobHeight.Min < 2.0 )
				BlobHeight.Min = 2.0;
			BlobHeight.Max = WellHeight + HeightDelta.Max;
			if ( BlobHeight.Max < BlobHeight.Min + 2.0 )
				BlobHeight.Max = BlobHeight.Min + 2.0;
			
		}
		
		void SetWidthDelta(MinMaxLimits WidthDelta, double WellWidth )
		{
			WellWidthDelta = WidthDelta;
			BlobWidth.Min = WellWidth - WidthDelta.Min;
			if ( BlobWidth.Min < 3.0 )
				BlobWidth.Min = 3.0;
			BlobWidth.Max = WellWidth + WidthDelta.Max;
			if ( BlobWidth.Max < BlobWidth.Min + 2.0 )
				BlobWidth.Max = BlobWidth.Min + 2.0;
		}
		
		void SetWellFraction( MinMaxLimits FractionLimits, double WellArea )
		{
			WellFraction = FractionLimits;
			BlobArea.Min = WellArea * FractionLimits.Min;
			if ( BlobArea.Min < 6.0 )
				BlobArea.Min = 6.0;
			BlobArea.Max = WellArea * FractionLimits.Max;
			if ( BlobArea.Max < BlobArea.Min + 6.0 )
				BlobArea.Max = BlobArea.Min + 6.0;
		}
		
		void Set( cv::Size2f WellSize, double WellArea, MinMaxLimits HeightDelta, 
			MinMaxLimits WidthDelta, MinMaxLimits FractionLimits,
			MinMaxLimits Circ, MinMaxLimits AspectR)
		{
			SetHeightDelta(HeightDelta, WellSize.height);
			SetWidthDelta(WidthDelta, WellSize.width);
			SetWellFraction(FractionLimits, WellArea);
			Circularity = Circ;
			AspectRatio = AspectR;
		}
		
		void Default()
		{
			MinMaxLimits def;
			def.Set(3.0, 3.0);
			SetHeightDelta(def, 10.0);
			SetWidthDelta(def, 16.0);
			
			def.Set(0.50, 1.5);
			SetWellFraction(def, 160.0);
			
			Circularity.Set(0.60, 1.0);
			AspectRatio.Set(0.7, 3.0);
		}
};

class AnalysisParameters
{
	public:
		int RollingBallRadius;	// Rolling ball radius used as part of background subtration (a)
		double BackgroundFrac;	// Fraction of image expected to be background, used as part of background subtration (a)	
		double TopFraction;		// Fraction of blob assumed to be in contact with the upper and lower surfaces of the well (a)
		cv::Size2f WellSize;	// The size of well is used to change minimum and maximum deltas on width and hegith 
								//		to upper and lower limits for blob width and height
		double WellArea;		// The area of the well is used to change minimum and maximum deltas on blob area into upper
								//		and lower limits on blob area
		// (a) these are not filters, but are included in the filter file for lack of any other good place for them.
		
		std::vector< MinMaxGroup > ParameterSet;	
		// Each parameter set corresponds to a filter which determines how "well like" a blob is.  The index of each set is used
		// as its Filter Value (FV). At present, the possible FV are
		//		FV=0 is the most lenient of filters. It is used when adjusting the threshold.  The threshold is adjusted to maximize
		//			the number of blobs with FV=0 or better
		//		FV=1 is the most strict set of filters.  Blobs which pass this filter are used in quantitation if they can be assigned
		//			to a well.
		//		FV=2 is somewhat less strict than FV=1.  Blobs assigned a FV of 1 are not used in quantitation.  However, they are
		//			retained and used along with the FV=1 blobs in aligning the blobs to the grid of wells.
		//		FV=3 is typically the same as FV=0. It is simply a category for bad blobs which are not other wise used.  The list is
		//			retained for diagnostic uses only.
		
		AnalysisParameters(): 
			RollingBallRadius(150), BackgroundFrac(0.45),
			TopFraction(0.50), WellSize(cv::Size2f(10.0, 10.0)), WellArea(100.0)
		{
			ParameterSet.resize(3);
			for ( int n = 0 ; n < 3 ; n++ )
				ParameterSet[n].Default();

		}
		~AnalysisParameters(){}
		
		AnalysisParameters(const AnalysisParameters &Source): 
			RollingBallRadius(Source.RollingBallRadius), BackgroundFrac(Source.BackgroundFrac),
			TopFraction(Source.TopFraction), WellSize(Source.WellSize),
				WellArea(Source.WellArea)
		{	
			ParameterSet.resize(Source.ParameterSet.size() );
			for ( int n = 0 ; n < ParameterSet.size() ; n++ )
				ParameterSet[n] = Source.ParameterSet[n];
		}
		
		AnalysisParameters& operator= ( const AnalysisParameters &Source )
		{
			if ( this !=&Source )
			{
				RollingBallRadius = Source.RollingBallRadius;
				BackgroundFrac = Source.BackgroundFrac;
				TopFraction = Source.TopFraction;
				WellSize = Source.WellSize;
				WellArea = Source.WellArea;
				ParameterSet.resize(Source.ParameterSet.size() );
				for ( int n = 0 ; n < ParameterSet.size() ; n++ )
					ParameterSet[n] = Source.ParameterSet[n];
			}
			return *this;
		}

		// Reads parameter file. It then uses WellAreaIn and WellSize2f to set the upper and lower limits
		// for each of the parameters used to characterize the shape and size of a blob and creates the
		// ParameterSets.  See .cpp file for more details.
		bool ReadFile(std::string FileName, double WellAreaIn, 
			cv::Size2f WellSize2f, std::string &ErrorMsg);

		// int TestBlob(int Set1, int SetN, BlobShape &Shape );
		
		int TestBlob(int Set1, int SetN, BlobShape &Shape, unsigned int &ResultCode, std::string &ResultString );
		
		int TestBlob(int Set, BlobShape &Shape, int Area);

		// int TestBlob(int Set, BlobShape &Shape, std::vector< bool > &Result);
		
		// int TestBlob(int Set, BlobShape &Shape, unsigned int &Result );

		void Output(std::ofstream &LogFile) const;

};
#endif // ANALYSISPARAMETERS_H

