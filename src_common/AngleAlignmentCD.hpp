#ifndef ANGLESCALEALIGNMENT_H
#define ANGLESCALEALIGNMENT_H
//! [includes]

#include <cstdlib>
#include <vector>
#include <algorithm>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "blob.hpp"
#include "blob_CD.hpp"
#include "guardErrors.hpp"
#include "readinfofile.hpp"
//! [includes]

//! [defines]

// class which combines Pixel location with the intensity of that pixel

class PixelIntensity
{
	public:
		cv::Point Pixel;
		double Intensity;
			
		PixelIntensity(): Pixel(cv::Point(0,0)), Intensity(0.0)
		{}
		
		~PixelIntensity(){}
		
		PixelIntensity(const PixelIntensity &Source): Pixel(Source.Pixel), Intensity(Source.Intensity)
		{}
			 
		PixelIntensity& operator=(const PixelIntensity &Source)
		{
			if ( this != &Source )
			{
				Pixel = Source.Pixel;
				Intensity = Source.Intensity;
			}
			return *this;
		}
		
		void Clear()
		{
			Pixel = cv::Point(0, 0);
			Intensity = 0.0;
		}
};

bool AngleAlignmentCD(std::ofstream& LogFile, std::vector< Blob_CD >& BlobList,
	cv::Size ImageSize, double DistanceToCenter, double ThetaResolution, std::vector< cv::Point > SubSectionBands,
	cv::Size SectionImageSize, cv::Size OptimalFFTSize, std::vector< ParameterLimit > PLimits,
	double& ShiftMiddle, int& ShiftMiddleResult,
	std::string& StatusMsg, std::string BaseName, bool Track);
#endif // ANGLESCALEALIGNMENT_H