// For each active Array/Section in device, this program will calculate the average and standard deviation of the
// filling ratios for the accepted droplets (in BlobList) and for droplets in BlobList and OtherBlobList combined.
// The procedure then outputs the results both to std::cout and also to a file named <BaseName>_cout.txt
//
// void OutputMaskingResults(std::vector<ArraySection> ArrSecList, std::vector< BlobSummary > &BlobList, 
//		std::vector< BlobSummary > &OtherBlobList, std::string BaseName)
//
//     input
// ArrSecList - vector of Array/Section pairs which are being analyzed in this imagesize
// BlobList - vector of BlobSummary, which includes the blobs which were suitable for quantitation
// OtherBlobList - vector of BlobSummary, which contains which are well-like, but don't meet all the 
//		criteria to be used for quantitation
// BaseName - output will be sent to std::cout and to a file named <BaseName>_cout.txt
//
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "physicalconstants.h"
#include "statistics.hpp"
#include "blob.hpp"
#include "readinfofile.hpp"
#include "outputMaskingResults.hpp"

void OutputMaskingResults(std::vector<ArraySection> ArrSecList, std::vector< BlobSummary > &InputBlobList, 
	std::vector< BlobSummary > &BlobList, std::string BaseName)
{
	std::vector< MaskingResults > mResults;
	mResults.clear();
	MaskingResults newResult;
	std::ofstream outFile;
	for (int n = 0; n < ArrSecList.size(); n++)
	{
		newResult.Clear();
		int a = ArrSecList[n].Array;
		int s = ArrSecList[n].Section;
		newResult.Array = a;
		newResult.Section = s;
		for (int m = 0; m < InputBlobList.size(); m++)
		{
			if ( InputBlobList[m].Array == a && InputBlobList[m].Section == s)
			{
				if (InputBlobList[m].ShapeSummary.FilterValue == 1)
				{
					newResult.MaskFillingFrac.Accumulate(InputBlobList[m].ShapeSummary.FillRatio);
					newResult.MaskBlobIntensity.Accumulate(InputBlobList[m].ShapeSummary.BlobIntensities.Sum );
				}
			}
		}
		for (int m = 0; m < BlobList.size(); m++)
		{
			if ( BlobList[m].Array == a && BlobList[m].Section == s )
			{
				if (BlobList[m].ShapeSummary.FilterValue == 1)
					newResult.BlobIntensity.Accumulate(BlobList[m].ShapeSummary.MaskedBlobIntensities.Sum );
			}
		}
		newResult.MaskFillingFrac.Analyze();
		newResult.MaskBlobIntensity.Analyze();
		newResult.BlobIntensity.Analyze();
		mResults.push_back(newResult);
	}

	std::string outName = BaseName;
	outName.append("_cout.csv");
	outFile.open(outName.c_str());
	
	std::cout << "Array\tSection\tNumb\tBlob\tBlob\tMsk Numb\tMask Blob\tMask Blob\tMask Fill\tMask Fill\n";
	std::cout << "\t\tDroplets\tInt.Ave\tInt.Std\tDroplets\tInt.Ave\tInt.Std\tFrac.Ave\tFrac.Std\n";
	outFile << "Array,Section,Numb,Blob,Blob,Mask,Mask Blob,Mask Blob,Mask Fill,Mask Fill\n";
	outFile << ",,Droplets,Int.Ave,Int.Std,NumbDrop,Int.Ave,Int.Std,Frac.Ave,Frac.Std\n";
	for (int n = 0; n < mResults.size(); n++)
	{
		std::cout << mResults[n].Array << "\t" << mResults[n].Section << "\t"
			<< mResults[n].BlobIntensity.Count() << "\t" << mResults[n].BlobIntensity.Ave() << "\t" << mResults[n].BlobIntensity.Std()
			<< "\t" << mResults[n].MaskBlobIntensity.Count() << "\t" << mResults[n].MaskBlobIntensity.Ave() << "\t" << mResults[n].MaskBlobIntensity.Std()
			<< "\t" << mResults[n].MaskFillingFrac.Ave() << "\t" << mResults[n].MaskFillingFrac.Std()
			<< "\n";
		outFile << mResults[n].Array << "," << mResults[n].Section << ","
			<< mResults[n].BlobIntensity.Count() << "," << mResults[n].BlobIntensity.Ave() << "," << mResults[n].BlobIntensity.Std()
			<< "," << mResults[n].MaskBlobIntensity.Count() << "," << mResults[n].MaskBlobIntensity.Ave() << "," << mResults[n].MaskBlobIntensity.Std()
			<< "," << mResults[n].MaskFillingFrac.Ave() << "," << mResults[n].MaskFillingFrac.Std()
			<< "\n";
	}
	std::cout << "%%%%%" << std::endl;
	outFile << "%%%%%" << std::endl;
	outFile.close();
}