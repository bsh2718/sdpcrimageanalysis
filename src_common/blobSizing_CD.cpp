///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//
// int CalculateFilling(std::ofstream &LogFile, const cv::Mat &BackgroundSubImage32F, std::vector< Blob > &BlobList, 
//		int TopNumb, std::string &OutputName, double ScaledWellArea, double Threshold, 
//		std::vector< std::vector< cv::Point > > &TemplatePixels, std::vector< double > &TemplatePixelFraction )
//
// bool SortBlobsSizing(Blob A, Blob B)
//
// bool WriteSizingFile(std::vector< Blob > &BlobList, std::vector< double > &Results,
// 		double Threshold, cv::Mat &BackgroundSubImage32F, cv::Mat &ThresholdedImage,
// 		std::string &OutName, cv::Mat &GImage, cv::Mat &Mask)
//
// bool ReadSizingFile(std::string &FileName, std::vector< BlobSummary > &BlobList, int &NumbBlobs, std::vector< double > &Results,
//		double &MinRingAve, double &MaxFillingRatio, MinMaxGroup &FilterParameters, std::string &ErrorMsg )	
//
//! [includes]

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <fstream>
#include <cstdlib>
#include <algorithm>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "blob.hpp"
#include "statistics.hpp"
#include "images.hpp"
#include "inputErrMessage.hpp"
#include "analysisParameters.hpp"
#include "trimRectangle.hpp"
// #include "outputTime.hpp"
#include "blobSizing_CD.hpp"
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//
// Calculates Filling Fraction for blobs in BlobList.
//
//			Input
//	LogFile - output stream for messages to logfile
//	BackgroundSubImage32F - background subtracted image of type <float>
//	BlobList - vector<Blob>, Information about blobs
//	TopNumb - number of pixels in top reqion required before filling fraction calculation is considered reliable
//	OutputName - if OutputName.size() > 5, the additional information is output to a file of that name.
//	ScaledWellArea - area of well corrected for scaling.
//	MinRingAve - Procedure will accumulate rings around the center of the blobs until the average intensity of the
//		pixels in the ring falls below this value. At which point it stops and analyzes the accumulated rings to
//		determine the filling fraction.  MinRingAve is typically 0.25 * minimumPeak. minimumPeak is the smallest
//		difference between the peak value in a blob and the average of its boundary that is allowed in order that
//		a region marked by the adaptive thresholding to be considered a blob. 
//	MaxFillingRatio - If the peak intensity region of a blob is significantly shifted from the geometric center of the
//		blob, then the calculated filling fraction can be greater than one.  MaxFillingRatio is the largest acceptable
//		filling fraction.  Blobs with filling fraction which are larger are rejected as having anomalous filling.
//	WellSpacing2f - Center to center distance between wells.
//	TemplatePixels - each template is a list of points which form an elliptical ring.  Together the rings form an ellipse
//		with the same aspect ratio as the well and start from a small ellipse and progresses to an ellipse with an area
//		larger than TopNumb
//	TemplatePixelFraction - vector of ratio of area of ellipse to well area.  TemplatePixelFraction[n] is the number of
//		pixels in all rings 0..n divided by well area (not currently used, should be removed)
//
//			Output
//	Results of filling fraction calculation are stored in BlobList[].Shape
//
unsigned char AdjustColorIntensity(unsigned char InitColor)
{
	if (InitColor > 128)
		return 255;
	unsigned char cV = InitColor / 2;
	return cV * 3 + 63;
}

int CalculateFilling(std::ofstream &LogFile, const cv::Mat &BackgroundSubImage32F, std::vector< Blob > &BlobList, 
	int TopNumb, std::string &OutputName, double ScaledWellArea, double MinRingAve, double MaxFillingRatio,
	cv::Point2f WellSpacing2f,
	std::vector< std::vector< cv::Point > > &TemplatePixels, std::vector< double > &TemplatePixelFraction )
{
	FillInfo newFill;
	cv::Rect boundingRect;
	cv::Size imageSize = BackgroundSubImage32F.size();
	cv::Mat maskImage(imageSize, CV_8U);
	cv::Mat workImage(imageSize, CV_8U);
	cv::Size boundingSize;
	boundingSize.width = (int)floor(WellSpacing2f.x);
	boundingSize.height = (int)floor(WellSpacing2f.y);
	if ( boundingSize.width % 2 == 0 )
		boundingSize.width -= 3;
	else
		boundingSize.width -= 2;
	
	if ( boundingSize.height % 2 == 0 )
		boundingSize.height -= 3;
	else
		boundingSize.height -= 2;
	cv::Point offset;
	offset.x = (boundingSize.width - 1 )/2;
	offset.y = (boundingSize.height - 1 )/2;
	int skip;
	bool outputSome;
	std::vector< cv::Point > pixelList;
	std::vector< double > pixelIntensities;
	std::ofstream blobOut;	
	std::ostringstream oString;
	int numbCalc = 0;
	int tSize = (int)TemplatePixels.size();
	// std::cout << "\nInside CalculateFilling(), BlobList.size() = " << BlobList.size() << std::endl;
	if ( OutputName.size() > 5 )
	{
		blobOut.open(OutputName.c_str());
		if ( !blobOut )
		{
			skip = 1;
			outputSome = false;
			LogFile << "\nCalculate Filling is unable to open " << OutputName << " for output\n";
		}
		else
		{
			skip = 10;
			outputSome = true;
			std::ostringstream hString1;
			std::ostringstream hString2;
			hString1.str("");
			hString2.str("");
			hString1.clear();
			hString2.clear();
			hString1 << "Index,Array,Section";
			hString2 << ",,";
			hString1 << ",Section,";
			hString2 << ",Row,Col";
			hString1 << ",Shifted,Image";
			hString2 << ",Row,Col";
			hString1 << ",Numb,Row,Col,Intensities";
			hString2 << ",Pixels,,,";
			hString1 << ",Filling Rings,,,,,";
			hString2 << ",Ring,Ave,Std,Numb,Max,Min";
			hString1 << ",Cumulative Filling Rings,,";
			hString2 << ",Ave,Std,Numb";
			blobOut << hString1.str() << "\n";
			blobOut << hString2.str() << std::endl;
		}
	}
	else
	{
		skip = 1;
		outputSome = false;
	}
	
	// std::cout << "Inside CalculateFilling() 2, TemplatePixels.size() = " << TemplatePixels.size() << std::endl;
	// std::vector< cv::Point > lastTemplate = TemplatePixels[TemplatePixels.size() - 1];
	std::vector< cv::Point > lastTemplate = TemplatePixels[tSize - 1];
	cv::Size bckImageLimit = BackgroundSubImage32F.size();
	bckImageLimit.width--;
	bckImageLimit.height--;
	int maxPListX = -10000000;
	int minPListX = 10000000;
	int maxPListY = -10000000;
	int minPListY = 10000000;
	// std::vector< double > deltaX(TemplatePixels.size() );
	// std::vector< double > deltaY(TemplatePixels.size() );
	std::vector< double > deltaX( tSize );
	std::vector< double > deltaY( tSize );
	// for ( int m = 0 ; m < TemplatePixels.size() ; m++ )
	for ( int m = 0 ; m < tSize ; m++ )
	{
		std::vector< cv::Point > pList = TemplatePixels[m];
		for ( int p = 0 ; p < (int)pList.size() ; p++ )
		{
			if ( pList[p].x > maxPListX )
				maxPListX = pList[p].x;
			if ( pList[p].x < minPListX )
				minPListX = pList[p].x;
			if ( pList[p].y > maxPListY )
				maxPListY = pList[p].y;
			if ( pList[p].y < minPListY )
				minPListY = pList[p].y;
		}
		deltaX[m] = ( maxPListX - minPListX ) * 0.24;
		deltaY[m] = ( maxPListY - minPListY ) * 0.24;
	}
	int bSize = (int)BlobList.size();
	// for ( int n = 0 ; n < (int)BlobList.size() ; n++ )
	workImage.setTo(0);
	for ( int n = 0 ; n < bSize ; n++ )
	{
		if (BlobList[n].Shape.Contour.size() > 4 )
		{
			for ( int p = 0 ; p < BlobList[n].Shape.PixelList.size() ; p++ )
				workImage.at<uchar>(BlobList[n].Shape.PixelList[p]) = 255;
		}
	}
	cv::Mat structElement = cv::getStructuringElement( cv::MORPH_RECT, cv::Size(3, 3), cv::Point(-1, -1) );
	cv::dilate(workImage, maskImage, structElement);
	for ( int n = 0 ; n < bSize ; n++ )
	{
		if (BlobList[n].Shape.Contour.size() > 4 )
		{
			for ( int p = 0 ; p < BlobList[n].Shape.PixelList.size() ; p++ )
				maskImage.at<uchar>(BlobList[n].Shape.PixelList[p]) = 0;
		}
	}
	for ( int n = 0 ; n < bSize ; n++ )
	{
		cv::Point bCenter = cv::Point((int)round(BlobList[n].Shape.InputCenter2f.x),
			(int)round(BlobList[n].Shape.InputCenter2f.y) );
		bool calcFilling = true;
		if ( (int)BlobList[n].Shape.Contour.size() < 5 )
			calcFilling = false;
		else
		{
			for ( int m = 0 ; m < (int)lastTemplate.size() ; m++ )	
			{
				if ( bCenter.x + lastTemplate[m].x < 0 || bCenter.x + lastTemplate[m].x > bckImageLimit.width 
					|| bCenter.y + lastTemplate[m].y < 0 || bCenter.y + lastTemplate[m].y > bckImageLimit.height )
				{
					calcFilling = false;
					break;
				}
			}
		}
		
		if ( calcFilling )
		{
			numbCalc++;
			bool output = false;
			if ( outputSome && (n % skip == 0 ) )
			{
				output = true;
				pixelList.clear();
				pixelIntensities.clear();
			}
			BlobList[n].Shape.FillingInfo.clear();
			// for ( int m = 0 ; m < TemplatePixels.size() ; m++ )
			for ( int m = 0 ; m < tSize ; m++ )
			{
				double sumX = 0.0;
				double sumY = 0.0;
				double sumXI = 0.0;
				double sumYI = 0.0;
				double sumI = 0.0;
				newFill.Intensities.Clear();
				newFill.MaxInten = 0.0;
				newFill.MinInten = 1.0E+37;
				std::vector< cv::Point > pList = TemplatePixels[m];
				if ( m > 0 )
					newFill.CumulativeIntensities = BlobList[n].Shape.FillingInfo[m-1].CumulativeIntensities;
				else
					newFill.CumulativeIntensities.Clear();
				
				for ( int p = 0 ; p < (int)pList.size() ; p++ )
				{
					double tmp = BackgroundSubImage32F.at<float>(bCenter.y + pList[p].y, bCenter.x + pList[p].x);
					newFill.Intensities.Accumulate(tmp);
					newFill.CumulativeIntensities.Accumulate(tmp);
					if ( tmp > newFill.MaxInten )
						newFill.MaxInten = tmp;
					if ( tmp < newFill.MinInten )
						newFill.MinInten = tmp;
					if ( output )
					{
						pixelList.push_back(cv::Point(bCenter.x + pList[p].x, bCenter.y + pList[p].y) );
						pixelIntensities.push_back(tmp);
					}
					sumX += (bCenter.x + pList[p].x);
					sumY += (bCenter.y + pList[p].y);
					sumXI += (bCenter.x + pList[p].x) * tmp;
					sumYI += (bCenter.y + pList[p].y) * tmp;
					sumI += tmp;
				}
				newFill.Intensities.Analyze();
				double stdOfMeanIntensity = newFill.Intensities.Std() / sqrt((double)newFill.Intensities.Count() );
				newFill.CumulativeIntensities.Analyze();
				// newFill.StdOfMeanCumulativeIntensity = newFill.CumulativeIntensities.Std() / sqrt((double)newFill.CumulativeIntensities.Count() );
				newFill.Top = false;
				newFill.CumulativeFraction = (double)newFill.CumulativeIntensities.Count()/ScaledWellArea;
				double cnt = (int)pList.size();
				newFill.Center2f = cv::Point2f( (float)(sumX / cnt), (float)(sumY / cnt) );
				newFill.Centroid2f = cv::Point2f( (float)(sumXI / sumI), (float)(sumYI / sumI) );
				if ( m == 0 )
				{
					newFill.AnomalousIntensity = false;
					newFill.AnomalousShape = false;
				}
				else
				{
					if ( newFill.Intensities.Ave() > BlobList[n].Shape.FillingInfo[m-1].CumulativeIntensities.Ave() + 2.5 * BlobList[n].Shape.FillingInfo[m-1].CumulativeIntensities.Std() )
						newFill.AnomalousIntensity = true;
					else
						newFill.AnomalousIntensity = false;
					
					double diffX = fabs(newFill.Center2f.x - newFill.Centroid2f.x);
					double diffY = fabs(newFill.Center2f.y - newFill.Centroid2f.y);
					if ( diffX > deltaX[m] || diffY > deltaY[m] )
						newFill.AnomalousShape = true;
					else
						newFill.AnomalousShape = false;
				}

				if ( newFill.Intensities.Ave() > 2.5 * stdOfMeanIntensity && newFill.Intensities.Ave() > MinRingAve )
					BlobList[n].Shape.FillingInfo.push_back(newFill);
				else
					break;
			}
			// std::cout << "          After m = 0; m < TemplatePixels.size()" << std::endl;
			// std::cout << "          BlobList[" << n << "].Shape.FillingInfo.size() = " << BlobList[n].Shape.FillingInfo.size() << std::endl;
			int mTop = 0;
			if ( BlobList[n].Shape.FillingInfo.size() > 1 )
			{
				BlobList[n].Shape.FillingInfo[0].Top = true;
				int fSize = (int)BlobList[n].Shape.FillingInfo.size();
				// for ( int m = 1 ; m < BlobList[n].Shape.FillingInfo.size() ; m++ )
				for ( int m = 1 ; m < fSize ; m++ )
				{
					// std::cout << "          m = " << m << ", FillingInfo.size() = " <<  BlobList[n].Shape.FillingInfo.size() << std::endl;
					if ( BlobList[n].Shape.FillingInfo[m-1].Top )
					{
						double aCurrent = BlobList[n].Shape.FillingInfo[m].Intensities.Ave();
						double stdOfMeanCumulativeIntensity = BlobList[n].Shape.FillingInfo[m-1].CumulativeIntensities.Std() 
							/ sqrt((double)BlobList[n].Shape.FillingInfo[m-1].CumulativeIntensities.Count() );
						double maxDiff = 2.5 * stdOfMeanCumulativeIntensity;
						if ( maxDiff < 3.0 )
							maxDiff = 3.0;
						
						double aPrevious = BlobList[n].Shape.FillingInfo[m-1].Intensities.Ave();
						double aTop = BlobList[n].Shape.FillingInfo[mTop].CumulativeIntensities.Ave();

						double topDiff = aTop - aCurrent;
						double prevDiff = aPrevious - aCurrent;
						if ( BlobList[n].Shape.FillingInfo[m-1].CumulativeIntensities.Count() <= TopNumb/2 )
						{
							if ( topDiff < 1.5 * maxDiff && prevDiff < 2.0 * maxDiff  )
							{
								BlobList[n].Shape.FillingInfo[m].Top = true;
								mTop = m;
							}
						}
						else if ( BlobList[n].Shape.FillingInfo[m-1].CumulativeIntensities.Count() <= TopNumb )
						{
							if ( topDiff < maxDiff && prevDiff < 1.5 * maxDiff  )
							{
								BlobList[n].Shape.FillingInfo[m].Top = true;
								mTop = m;
							}
						}
					}
				}
				
				BlobList[n].Shape.TopIntensities = BlobList[n].Shape.FillingInfo[mTop].CumulativeIntensities;

				BlobList[n].Shape.FillIntensity = BlobList[n].Shape.TopIntensities.Ave() * ScaledWellArea;
				BlobList[n].Shape.FillRatio = BlobList[n].Shape.BlobIntensities.Sum() / BlobList[n].Shape.FillIntensity;
				
				BlobList[n].Shape.TopIntensityFraction = (double)BlobList[n].Shape.FillingInfo[mTop].CumulativeIntensities.Count() / ScaledWellArea;
				BlobList[n].Shape.TopFillingNumber = mTop;
				if ( BlobList[n].Shape.FillRatio > MaxFillingRatio )
				{
					BlobList[n].Shape.AnomalousFilling = true;
					BlobList[n].Shape.FilterValue += 200;
				}
				else
					BlobList[n].Shape.AnomalousFilling = false;
				
				if ( output )
				{
					// int idx = 1000000 * BlobList[n].Array + 100000 * BlobList[n].Section 
						// + 1000 * BlobList[n].SectionPosition.x  + BlobList[n].SectionPosition.y;
					blobOut << n << "," << BlobList[n].Array << "," << BlobList[n].Section
						<< "," << BlobList[n].SectionPosition.y << "," << BlobList[n].SectionPosition.x
						<< "," << BlobList[n].Shape.InputCenter2f.y 
						<< "," << BlobList[n].Shape.InputCenter2f.x
						<< "," << BlobList[n].Shape.PixelIntensities.size() << "\n";
					int pSize = (int)pixelIntensities.size();
					// for ( int m = 0 ; m < pixelIntensities.size() ; m++ )
					for ( int m = 0 ; m < pSize ; m++ )
					{
						if ( m < (int)BlobList[n].Shape.FillingInfo.size() )
						{
							blobOut << ",,,,,,,," << pixelList[m].y << "," << pixelList[m].x 
								<< "," << pixelIntensities[m] 
								<< "," << m 
								<< "," << BlobList[n].Shape.FillingInfo[m].Intensities.Ave()
								<< "," << BlobList[n].Shape.FillingInfo[m].Intensities.Std()
								<< "," << BlobList[n].Shape.FillingInfo[m].Intensities.Count()
								<< "," << BlobList[n].Shape.FillingInfo[m].MaxInten
								<< "," << BlobList[n].Shape.FillingInfo[m].MinInten
								<< "," << BlobList[n].Shape.FillingInfo[m].CumulativeIntensities.Ave()
								<< "," << BlobList[n].Shape.FillingInfo[m].CumulativeIntensities.Std()
								<< "," << BlobList[n].Shape.FillingInfo[m].CumulativeIntensities.Count();
						}
						else
						{
							blobOut << ",,,,,,,," << pixelList[m].y << "," << pixelList[m].x 
								<< "," << pixelIntensities[m];
						}
						blobOut << std::endl;
					}
				}
			}
			else if ( BlobList[n].Shape.FillingInfo.size() == 1 )
			{
				mTop = 0;
				BlobList[n].Shape.FillingInfo[0].Top = true;
				BlobList[n].Shape.TopIntensities = BlobList[n].Shape.FillingInfo[mTop].CumulativeIntensities;

				BlobList[n].Shape.FillIntensity = BlobList[n].Shape.TopIntensities.Ave() * ScaledWellArea;
				BlobList[n].Shape.FillRatio = BlobList[n].Shape.BlobIntensities.Sum() / BlobList[n].Shape.FillIntensity;
				
				BlobList[n].Shape.TopIntensityFraction = (double)BlobList[n].Shape.FillingInfo[mTop].CumulativeIntensities.Count() / ScaledWellArea;
				BlobList[n].Shape.TopFillingNumber = mTop;
				
			}
			else
			{
				BlobList[n].Shape.FillIntensity = 0.0;
				BlobList[n].Shape.FillRatio = 0.0;
				BlobList[n].Shape.TopIntensityFraction = 0.0;
				BlobList[n].Shape.TopFillingNumber = 0;
				newFill.Intensities.Clear();
				newFill.CumulativeIntensities.Clear();
				newFill.MaxInten = 0.0;
				newFill.MinInten = 0.0;
				newFill.CumulativeFraction = 0.0;
				newFill.Top = false;
				BlobList[n].Shape.FillingInfo.clear();
				BlobList[n].Shape.FillingInfo.push_back(newFill);
			}
			// std::cout << "          After m = 1 ; m <= BlobList[n].FillingInfo.size()" << std::endl;
		}
		else
		{
			BlobList[n].Shape.FillIntensity = 0.0;
			BlobList[n].Shape.FillRatio = 0.0;
			BlobList[n].Shape.TopIntensityFraction = 0.0;
			BlobList[n].Shape.TopFillingNumber = 0;
			newFill.Intensities.Clear();
			newFill.CumulativeIntensities.Clear();
			newFill.MaxInten = 0.0;
			newFill.MinInten = 0.0;
			newFill.CumulativeFraction = 0.0;
			newFill.Top = false;
			BlobList[n].Shape.FillingInfo.clear();
			BlobList[n].Shape.FillingInfo.push_back(newFill);
		}
		if ( BlobList[n].Shape.FillingInfo.size() > 0 )
		{
			boundingRect.x = bCenter.x - offset.x;
			boundingRect.y = bCenter.y - offset.y;
			if ( TrimRectangle(boundingRect, boundingSize, imageSize ) )
			{
				cv::Mat roiMask = maskImage(boundingRect);
				cv::Mat roiImage = BackgroundSubImage32F(boundingRect);
				std::vector< cv::Point > aList;
				std::vector< double > addedList;
				cv::findNonZero(roiMask, aList);
				BlobList[n].Shape.AddedPixelList = aList;
				if ( aList.size() > 0 )
				{
					addedList.resize(aList.size());
					BlobList[n].Shape.AddedBlobIntensities.Clear();
					BlobList[n].Shape.TotalBlobIntensities = BlobList[n].Shape.BlobIntensities;
					for ( int p = 0 ; p < aList.size() ; p++ )
						addedList[p] = roiImage.at<float>(aList[p]);
					
					BlobList[n].Shape.AddedPixelIntensities = addedList;
					BlobList[n].Shape.AddedBlobIntensities.Accumulate(addedList);
					BlobList[n].Shape.TotalBlobIntensities.Accumulate (addedList);
					BlobList[n].Shape.AddedBlobIntensities.Analyze();
					BlobList[n].Shape.TotalBlobIntensities.Analyze();
					BlobList[n].Shape.AltFillRatio = BlobList[n].Shape.TotalBlobIntensities.Sum() / BlobList[n].Shape.FillIntensity;
				}
				else
				{
					BlobList[n].Shape.AddedPixelList.clear();
					BlobList[n].Shape.AddedPixelIntensities.clear();
					BlobList[n].Shape.AddedBlobIntensities.Clear();
					BlobList[n].Shape.TotalBlobIntensities = BlobList[n].Shape.BlobIntensities;
					BlobList[n].Shape.AltFillRatio = BlobList[n].Shape.AltFillRatio;
				}
			}
			else
			{
				BlobList[n].Shape.AddedPixelList.clear();
				BlobList[n].Shape.AddedPixelIntensities.clear();
				BlobList[n].Shape.AddedBlobIntensities.Clear();
				BlobList[n].Shape.TotalBlobIntensities = BlobList[n].Shape.BlobIntensities;
				BlobList[n].Shape.AltFillRatio = BlobList[n].Shape.AltFillRatio;
			}
		}
	}
	
	if ( outputSome )
	{
		// blobOut << "Finiahed\n" << std::endl;
		blobOut.close();
	}
	return numbCalc;
}

// bool SortBlobsSizing(Blob A, Blob B) 
// {
	// if ( A.Array == 0 && B.Array != 0 )
		// return false;
	// else if ( A.Array != 0 && B.Array == 0 )
		// return true;
	// else if ( A.Array == 0 && B.Array == 0 )
		// return A.InputCenter2f.y < B.InputCenter2f.y;
	// else 
	// {
		// if ( ( A.Shape.FilterValue == 1 && B.Shape.FilterValue == 1 ) 
			// || ( A.Shape.FilterValue != 1 && B.Shape.FilterValue != 1 ) )
		// {
			// if ( A.Array != B.Array )	
				// return A.Array < B.Array;
			// else if ( A.Section != B.Section )
				// return A.Section < B.Section;
			// else if ( A.SectionPosition.y != B.SectionPosition.y )
				// return A.SectionPosition.y < B.SectionPosition.y;
			// else
				// return A.SectionPosition.x < B.SectionPosition.x;
		// }
		// else if ( A.Shape.FilterValue == 1 )
			// return true;
		// else
			// return false;
	// }
// }

bool SortBlobsSizingS(BlobSummary A, BlobSummary B) 
{
	if (A.Status > 0 && B.Status <= 0)
		return true;
	else if (B.Status > 0 && A.Status <= 0)
		return false;
	else if (A.Array == 0 && B.Array != 0)
		return false;
	else if (A.Array != 0 && B.Array == 0)
		return true;
	else if (A.Array == 0 && B.Array == 0)
		return A.InputCenter2f.y < B.InputCenter2f.y;
	else if (A.SectionPosition.x > 0 && B.SectionPosition.x < 1)
		return true;
	else if (A.SectionPosition.y > 0 && B.SectionPosition.y < 1)
		return true;
	else if (A.SectionPosition.x < 1 && B.SectionPosition.x > 0)
		return false;
	else if (A.SectionPosition.y < 1 && B.SectionPosition.y > 0)
		return false;
	else 
	{
		if ( ( A.ShapeSummary.FilterValue == 1 && B.ShapeSummary.FilterValue == 1 ) 
			|| ( A.ShapeSummary.FilterValue != 1 && B.ShapeSummary.FilterValue != 1 ) )
		{
			if ( A.Array != B.Array )	
				return A.Array < B.Array;
			else if ( A.Section != B.Section )
				return A.Section < B.Section;
			else if ( A.SectionPosition.y != B.SectionPosition.y )
				return A.SectionPosition.y < B.SectionPosition.y;
			else
				return A.SectionPosition.x < B.SectionPosition.x;
		}
		else if ( A.ShapeSummary.FilterValue == 1 )
			return true;
		else
			return false;
	}
}

int SOI = 1;
bool SortBlobsSizingSOI(BlobSummary A, BlobSummary B)
{
	if (A.Status > 0 && B.Status <= 0)
		return true;
	else if (B.Status > 0 && A.Status <= 0)
		return false;
	else if (A.Array == 0 && B.Array != 0)
		return false;
	else if (A.Array != 0 && B.Array == 0)
		return true;
	else if (A.Array == 0 && B.Array == 0)
		return A.InputCenter2f.y < B.InputCenter2f.y;
	else if (A.SectionPosition.x > 0 && B.SectionPosition.x < 1)
		return true;
	else if (A.SectionPosition.y > 0 && B.SectionPosition.y < 1)
		return true;
	else if (A.SectionPosition.x < 1 && B.SectionPosition.x > 0)
		return false;
	else if (A.SectionPosition.y < 1 && B.SectionPosition.y > 0)
		return false;
	else if (A.Section == SOI && B.Section != SOI)
		return true;
	else if (A.Section != SOI && B.Section == SOI)
		return false;
	else
	{
		if ((A.ShapeSummary.FilterValue == 1 && B.ShapeSummary.FilterValue == 1)
			|| (A.ShapeSummary.FilterValue != 1 && B.ShapeSummary.FilterValue != 1))
		{
			if (A.Array != B.Array)
				return A.Array < B.Array;
			else if (A.Section != B.Section)
				return A.Section < B.Section;
			else if (A.SectionPosition.y != B.SectionPosition.y)
				return A.SectionPosition.y < B.SectionPosition.y;
			else
				return A.SectionPosition.x < B.SectionPosition.x;
		}
		else if (A.ShapeSummary.FilterValue == 1)
			return true;
		else
			return false;
	}
}
// bool SortBlobsBySize(Blob A, Blob B )
// {
	// return A.Shape.PixelList.size() < B.Shape.PixelList.size() ;
// }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//
// Writing Sizing information about blobs in BlobList.
//
//			Input
// BlobList - vector<Blob> vector of blobs to be written to output file.  SortBlobsSizing is used to sort blobs 
//		before output.
// Results - vector of results (X-Offset, Y-Offset, Angle, Scale)
// Threshold - threshold used to identify blobs
// BackgroundSubImage32F - background subtracted image
// ThresholdedImage - mask obtained by applying Threshold to BackgroundSubImage32F
// OutName - name of output file
// GImage - Color image created from BackgroundSubImage32F. Pixels within blobs are turned blue or green depending oneBlob
//		the blob's Filter Value.  If blob meets all the criteria the pixels are turned blue.  If the blob meets all the 
//		criteria, excpet that it is a little too large or small, the pixels are turned green.  The intensity of the blue
//		or green is proportional to the intensity in BackgroundSubImage32F.
// Mask - A mask created from BackgroundSubImage32F using Threshold.  If the blob is blue or green in GImage, then the
//		blob is white in Mask, otherwise it is gray.
//

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This is the reverse of WriteSizingFile
//
// 		Input
// FileName - Name of sizing file
// 		Output
// BlobList - vector of BlobSummary read from file
// NumbBlobs - number of blobs in BlobList
// Results - (X Offset, Y Offset, Rotation Angle, Scale Factor) for best alignment results
// MinRingAve - 
// MaxFillingRatio - 
// FilterParameters - A list of the minimum and maximum limits allowed for 
// ErrorMsg - If there is an error reading the file a description is put here and false is returned.  If reading the 
// 	file is successful, then ErrorMsg is cleared and true is returned.
//
bool ReadSizingFile(std::string &FileName, std::vector< BlobSummary > &BlobList, int &NumbBlobs, std::vector< double > &Results,
	double &MinRingAve, double &MaxFillingRatio, MinMaxGroup &FilterParameters, std::string &ErrorMsg )	
{
	std::istringstream sToken;
	std::istringstream sLine;
	std::string line;
	std::string token;
	BlobSummary oneBlob;
	BlobShapeSummary oneShape;
	std::ostringstream oString;
	std::ifstream sizingFile(FileName.c_str());
	if( ! sizingFile ) 
	{
		oString.str("");
		oString.clear();
		oString << "Error opening " << FileName;
		ErrorMsg = oString.str();
		return false;
	}
	Stat1Results s1Results;
	Results.resize(4);
	getline( sizingFile, line);
	getline( sizingFile, line);
	
	sLine.clear(); sLine.str(line);
	int m = 0;
	std::getline(sLine, token, ',');
	sToken.clear(); sToken.str(token); sToken >> NumbBlobs;
	if ( sToken.fail() ) { InputErrMessage(FileName, m, "NumbBlobs", ErrorMsg); return false; }
	
	std::getline(sLine, token, ',');
	sToken.clear(); sToken.str(token); sToken >> Results[0];
	if ( sToken.fail() ) { InputErrMessage(FileName, m, "Results[0]", ErrorMsg); return false; }
	
	std::getline(sLine, token, ',');
	sToken.clear(); sToken.str(token); sToken >> Results[1];
	if ( sToken.fail() ) { InputErrMessage(FileName, m, "Results[1]", ErrorMsg); return false; }

	std::getline(sLine, token, ',');
	sToken.clear(); sToken.str(token); sToken >> Results[2];
	if ( sToken.fail() ) { InputErrMessage(FileName, m, "Results[2]", ErrorMsg); return false; }
	
	std::getline(sLine, token, ',');
	sToken.clear(); sToken.str(token); sToken >> Results[3];
	if ( sToken.fail() ) { InputErrMessage(FileName, m, "Results[3]", ErrorMsg); return false; }
	
	std::getline(sLine, token, ',');
	sToken.clear(); sToken.str(token); sToken >> MinRingAve;
	if ( sToken.fail() ) { InputErrMessage(FileName, m, "MinRingAve", ErrorMsg); return false; }
	
	std::getline(sLine, token, ',');
	sToken.clear(); sToken.str(token); sToken >> MaxFillingRatio;
	if ( sToken.fail() ) { InputErrMessage(FileName, m, "MaxFillingRatio", ErrorMsg); return false; }
	
	std::getline(sLine, token, ',');
	
	std::getline(sLine, token, ',');
	sToken.clear(); sToken.str(token); sToken >> FilterParameters.BlobHeight.Min;
	if ( sToken.fail() ) { InputErrMessage(FileName, m, "BlobHeight.Min", ErrorMsg); return false; }
	
	std::getline(sLine, token, ',');
	sToken.clear(); sToken.str(token); sToken >> FilterParameters.BlobHeight.Max;
	if ( sToken.fail() ) { InputErrMessage(FileName, m, "BlobHeight.Max", ErrorMsg); return false; }
	
	std::getline(sLine, token, ',');
	sToken.clear(); sToken.str(token); sToken >> FilterParameters.BlobWidth.Min;
	if ( sToken.fail() ) { InputErrMessage(FileName, m, "BlobWidth.Min", ErrorMsg); return false; }
	
	std::getline(sLine, token, ',');
	sToken.clear(); sToken.str(token); sToken >> FilterParameters.BlobWidth.Max;
	if ( sToken.fail() ) { InputErrMessage(FileName, m, "BlobWidth.Max", ErrorMsg); return false; }
	
	std::getline(sLine, token, ',');
	sToken.clear(); sToken.str(token); sToken >> FilterParameters.BlobArea.Min;
	if ( sToken.fail() ) { InputErrMessage(FileName, m, "BlobArea.Min", ErrorMsg); return false; }
	
	std::getline(sLine, token, ',');
	sToken.clear(); sToken.str(token); sToken >> FilterParameters.BlobArea.Max;
	if ( sToken.fail() ) { InputErrMessage(FileName, m, "BlobArea.Max", ErrorMsg); return false; }
	
	std::getline(sLine, token, ',');
	sToken.clear(); sToken.str(token); sToken >> FilterParameters.Circularity.Min;
	if ( sToken.fail() ) { InputErrMessage(FileName, m, "Circularity.Min", ErrorMsg); return false; }
	
	std::getline(sLine, token, ',');
	sToken.clear(); sToken.str(token); sToken >> FilterParameters.Circularity.Max;
	if ( sToken.fail() ) { InputErrMessage(FileName, m, "Circularity.Max", ErrorMsg); return false; }
	
	std::getline(sLine, token, ',');
	sToken.clear(); sToken.str(token); sToken >> FilterParameters.AspectRatio.Min;
	if ( sToken.fail() ) { InputErrMessage(FileName, m, "AspectRatio.Min", ErrorMsg); return false; }
	
	std::getline(sLine, token, ',');
	sToken.clear(); sToken.str(token); sToken >> FilterParameters.AspectRatio.Max;
	if ( sToken.fail() ) { InputErrMessage(FileName, m, "AspectRatio.Max", ErrorMsg); return false; }
	
	getline( sizingFile, line);
	getline( sizingFile, line);
	BlobList.clear();
	oneShape.Clear();
	int index;
	// int status;
	// int nBlanks = 0;
	for ( int n = 0 ; n < NumbBlobs ; n++ )
	{
		if ( std::getline( sizingFile, line) )
		{
			sLine.clear(); sLine.str(line);
		
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> index;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Index", ErrorMsg); return false; }
			
			oneBlob.SelfIndex = n;

			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> oneBlob.Status;
			if (sToken.fail()) { InputErrMessage(FileName, n, "Status", ErrorMsg); return false; }

			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> oneBlob.Array;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Array", ErrorMsg); return false; }
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> oneBlob.Section;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Section", ErrorMsg); return false; }
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> oneBlob.SectionPosition.y;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Section Position Y (Row)", ErrorMsg); return false; }
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> oneBlob.SectionPosition.x;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Section Position X (Col)", ErrorMsg); return false; }
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> oneBlob.InputCenter2f.y;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Input Center Y", ErrorMsg); return false; }
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> oneBlob.InputCenter2f.x;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Input Center X", ErrorMsg); return false; }
			oneShape.InputCenter2f = oneBlob.InputCenter2f;
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> oneBlob.DeviceCenter.y;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Device Center Y", ErrorMsg); return false; }
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> oneBlob.DeviceCenter.x;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Device Center X", ErrorMsg); return false; }
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> oneShape.FilterValue;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Filter Value", ErrorMsg); return false; }
			
			if ( oneShape.FilterValue < 200)
				oneShape.AnomalousFilling = false;
			else
			{
				oneShape.AnomalousFilling = true;
				oneShape.FilterValue -= 200;
			}
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> oneShape.FilterString;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Filter String", ErrorMsg); return false; }
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> oneShape.ContourLength;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Contour Length", ErrorMsg); return false; }
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> oneShape.Perimeter;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Perimeter", ErrorMsg); return false; }
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> oneShape.Area;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Area", ErrorMsg); return false; }
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> oneShape.Mom.m00;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "M_00", ErrorMsg); return false; }
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> oneShape.BoundingRect.height;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Bounding Rectangle Height", ErrorMsg); return false; }
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> oneShape.BoundingRect.width;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Bounding Rectangle Width", ErrorMsg); return false; }

			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> oneShape.AspectRatio;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Aspect Ratio", ErrorMsg); return false; }
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> oneShape.Circularity;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Circularity", ErrorMsg); return false; }

			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> oneShape.FillIntensity;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Fill Intensity", ErrorMsg); return false; }
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> oneShape.FillRatio;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Fill Ratio", ErrorMsg); return false; }
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> oneShape.TopIntensityFraction;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Top Intensity Fraction", ErrorMsg); return false; }
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> oneShape.TopFillingNumber;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Top Filling Number", ErrorMsg); return false; }
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> s1Results.Ave;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Top Intensites Ave", ErrorMsg); return false; }
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> s1Results.Std;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Top Intensites Std", ErrorMsg); return false; }
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> s1Results.Count;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Top Intensites Count", ErrorMsg); return false; }
			
			s1Results.Sum = 0.0;
			s1Results.Sum2 = 0.0;
			oneShape.TopIntensities = s1Results;
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> s1Results.Ave;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Blob Intensites Ave", ErrorMsg); return false; }
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> s1Results.Sum;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Blob Intensites Sum", ErrorMsg); return false; }
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> s1Results.Count;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Blob Intensites Count", ErrorMsg); return false; }
			
			s1Results.Std = 0.0;
			s1Results.Sum2 = 0.0;
			oneShape.BlobIntensities = s1Results;
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> oneShape.TopFillingNumber;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Last Ring Added Number", ErrorMsg); return false; }
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> s1Results.Ave;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Last Ring Intensities Ave", ErrorMsg); return false; }
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> s1Results.Std;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Last Ring Intensities Std", ErrorMsg); return false; }
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> s1Results.Count;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Last Ring Intensities Count", ErrorMsg); return false; }
			
			s1Results.Sum = 0.0;
			s1Results.Sum2 = 0.0;
			oneShape.LastRingIntensities = s1Results;
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> oneShape.LastRingMaxIntensity;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Last Ring Max Intensity", ErrorMsg); return false; }
			
			std::getline(sLine, token, ',');
			sToken.clear(); sToken.str(token); sToken >> oneShape.LastRingMinIntensity;
			if ( sToken.fail() ) { InputErrMessage(FileName, n, "Last Ring Min Intensity", ErrorMsg); return false; }
			
			oneBlob.ShapeSummary = oneShape;
			BlobList.push_back(oneBlob);
		}
		else
		{
			InputErrMessage(FileName, n, "Inputing line", ErrorMsg); 
			return false;
		}
	}
	return true;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//
// Writing Sizing information about blobs in BlobList.
//
//			Input
// BlobList - vector<Blob> vector of blobs to be written to output file. These blobs are acceptable and used for 
//		quantitation. SortBlobsSizing is used to sort blobs before output.
// OtherBlobList - vector<Blob> vector of blobs to be written to output file. These blobs are well-like, but do not
//		meet the criteria to be included in BlobList.
// BadBlobList -  vector<Blob> vector of blobs to be written to output file. These blobs are not well-like.
// Results - vector of results (X-Offset, Y-Offset, Angle, Scale)
// MinRingAve - 
// MaxFillingRatio - 
// FParams - 
// Set - 
// BackgroundSubImage32F - background subtracted image
// ThresholdedImage - mask obtained by applying Threshold to BackgroundSubImage32F
// OutName - name of output file
// ImageScale - 
// ImageMinimum - 
// GImage - Color image created from BackgroundSubImage32F. Pixels within blobs are turned blue or green depending oneBlob
//		the blob's Filter Value.  If blob meets all the criteria the pixels are turned blue.  If the blob meets all the 
//		criteria, excpet that it is a little too large or small, the pixels are turned green.  The intensity of the blue
//		or green is proportional to the intensity in BackgroundSubImage32F.
// Mask - A mask created from BackgroundSubImage32F using Threshold.  If the blob is blue or green in GImage, then the
//		blob is white in Mask, otherwise it is gray.
// TrackFilling -
//
	bool WriteSizingFile(std::ofstream& LogFile, 
		std::vector< Blob > &BlobList, std::vector< Blob > &OtherBlobList, std::vector< Blob > &BadBlobList,
		std::vector< double > &Results, int SectionOfInterest,
		double MinRingAve, double MaxFillingRatio, AnalysisParameters &FParams, int Set, cv::Mat &BackgroundSubImage32F, cv::Mat &ThresholdedImage,
		std::string &OutName, double ImageScale, double ImageMinimum, 
		cv::Mat &GImage, cv::Mat &Mask, cv::Mat &ContMask, bool TrackFilling )
{
	 //std::cout << "Inside WriteSizingFile" << std::endl;
	 //std::cout << "     List Sizes = " << BlobList.size() << std::endl;
	 //std::cout << "                - " << OtherBlobList.size() << std::endl;
	 //std::cout << "                - " << BadBlobList.size() << std::endl;
	cv::Size imageSize = BackgroundSubImage32F.size();
	int imageSizeTotal = (int)BackgroundSubImage32F.total();
	cv::Vec3b cValue;
	cv::Vec3b mValue;
	std::ofstream blobOut;
	std::ostringstream hString1;
	std::ostringstream hString2;
	Stat1Results s1Results;
	int bSize = (int)(BlobList.size() + OtherBlobList.size() + BadBlobList.size());
	SOI = SectionOfInterest;
	std::vector< BlobSummary > sortedBlobList;
	sortedBlobList.resize(bSize);	
	Blob tmpBlob;
	int bCnt = 0;
	std::vector< BlobSummary > unassignedBlobList;
	unassignedBlobList.clear();
	std::vector< std::vector< cv::Point > > trialContours;
	int otherOffset = (int)((ceil((double)BlobList.size() * 0.001)) * 1000.0);
	while (otherOffset - BlobList.size() < 100)
		otherOffset += 100;
	if ( TrackFilling )
	{
		cv::Mat workImage(imageSize, CV_8UC1);
		workImage.setTo(0);
		int cCnt = 0;
		trialContours.resize(BlobList.size());
		for ( int n = 0 ; n < BlobList.size() ; n++ )
		{
			if ( BlobList[n].Shape.Status == 0 )
			{
				sortedBlobList[bCnt] = BlobList[n];
				trialContours[cCnt] = BlobList[n].Shape.Contour;
				bCnt++;
				cCnt++;
			}
		}
		int lastOrigBlob = bCnt;
		//std::clog << "After sorting BlobList, bCnt/cCnt = " << bCnt << " / " << cCnt << std::endl;
		trialContours.resize(cCnt);
		cv::drawContours(workImage, trialContours, -1, 255, cv::FILLED);
		cCnt = 0;
		trialContours.resize(OtherBlobList.size());
		//std::clog << "Before sorting OtherBlobList, OtherBlobList.size() = " << OtherBlobList.size() 
		//	<< ", sortedBlobList.size() = " << sortedBlobList.size() << std::endl;
		int numbOverlaps = 0;
		for ( int n = 0 ; n < OtherBlobList.size() ; n++ )
		{
			cv::Point pTmp = OtherBlobList[n].InputCenter2f;
			if ( workImage.at<uchar>(pTmp) == 0 )
			{
				//std::clog << "     " << n << "/" << bCnt << "/" << cCnt << std::endl;
				sortedBlobList[bCnt] = OtherBlobList[n];
				sortedBlobList[bCnt].SelfIndex += otherOffset;
				trialContours[cCnt] = OtherBlobList[n].Shape.Contour;
				bCnt++;
				cCnt++;
			}
			else
			{
				numbOverlaps++;
				LogFile << "Overlap when sorting OtherBlobList at " << pTmp << std::endl;
			}
		}
		//std::clog << "After sorting OtherBlobList, bCnt/cCnt = " << bCnt << " / " << cCnt << ", Numb Overlaps = " << numbOverlaps << std::endl;
		trialContours.resize(cCnt);
		cv::drawContours(workImage, trialContours, -1, 255, cv::FILLED);
		// timeMsg = ">>> After Draw Contours";
		// OutputTime(timeMsg);
		numbOverlaps = 0;
		otherOffset += (int)OtherBlobList.size();
		for ( int n = 0 ; n < BadBlobList.size() ; n++ )
		{
			cv::Point pTmp = BadBlobList[n].InputCenter2f;
			if ( workImage.at<uchar>(pTmp) == 0 )
			{
				sortedBlobList[bCnt] = BadBlobList[n];
				sortedBlobList[bCnt].SelfIndex += otherOffset;
				bCnt++;
			}
			else
			{
				numbOverlaps++;
				LogFile << "Overlap when sorting BadBlobList at " << pTmp << std::endl;
			}
		}
		//std::cout << "After sorting BlobList, bCnt = " << bCnt << ". Numb Overlaps = " << numbOverlaps << std::endl;
	}
	else
	{
		for ( int n = 0 ; n < BlobList.size() ; n++ )
		{
			sortedBlobList[bCnt] = BlobList[n];
			bCnt++;
		}
		for ( int n = 0 ; n < OtherBlobList.size() ; n++ )
		{
			sortedBlobList[bCnt] = OtherBlobList[n];
			sortedBlobList[bCnt].SelfIndex += otherOffset;
			bCnt++;
		}
		otherOffset += (int)OtherBlobList.size();
		for ( int n = 0 ; n < BadBlobList.size() ; n++ )
		{
			sortedBlobList[bCnt] = BadBlobList[n];
			sortedBlobList[bCnt].SelfIndex += otherOffset;
			bCnt++;
		}
	}
	
	sortedBlobList.resize(bCnt);
	std::sort(sortedBlobList.begin(), sortedBlobList.end(), SortBlobsSizingSOI );
//	std::cout << "sortedBlobList.size() = " << sortedBlobList.size() << "\n";
	// timeMsg = ">>> After sort BlobList";
	// OutputTime(timeMsg);
	int sSize = (int)sortedBlobList.size();
	GImage.create(imageSize, CV_8UC3);
	Mask.create(imageSize, CV_16U);
	ContMask.create(imageSize, CV_16U);
	cValue[0] = cValue[1] = cValue[2] = 0;
	GImage.setTo(cValue);
	Mask.setTo(0);
	ContMask.setTo(0);
	cv::Mat workImage1;
	cv::Mat workImage2;
		
	BackgroundSubImage32F.copyTo(workImage1);
	BackgroundSubImage32F.copyTo(workImage2);
	workImage1 -= ImageMinimum;
	workImage2 = workImage1 * ImageScale;
	// timeMsg = ">>> Before setup GImage";
	// OutputTime(timeMsg);
	for ( int n = 0 ; n < imageSizeTotal ; n++ )
	{
		int itmp = (int)floor(workImage2.at<float>(n));
		if ( itmp > 0)
			cValue[0] = cValue[1] = cValue[2] = itmp;
		else
			cValue[0] = cValue[1] = cValue[2] = 0;
		GImage.at<cv::Vec3b>(n) = cValue;
	}
	blobOut.open( OutName.c_str() );
	if ( !blobOut.is_open() )
		return false;
	// std::cout << "After blobOut.open(), OutName = " << OutName << std::endl;
	blobOut << "Count,Results[0],Results[1],Results[2],Results[3],,"
		<< "MinRingAve,MaxFillingRatio,Hght Min,Hght Max,Wdth Min,Wdth Max,Area Min,Area Max,Circ Min,Circ Max,AspR Min,AspR Max\n";
	blobOut << bCnt << "," << Results[0] << "," << Results[1] << "," << Results[2] << "," << Results[3] 
		<< "," << MinRingAve << "," << MaxFillingRatio << ","
		<< "," << FParams.ParameterSet[Set].BlobHeight.Min << "," << FParams.ParameterSet[Set].BlobHeight.Max
		<< "," << FParams.ParameterSet[Set].BlobWidth.Min << "," << FParams.ParameterSet[Set].BlobWidth.Max
		<< "," << FParams.ParameterSet[Set].BlobArea.Min << "," << FParams.ParameterSet[Set].BlobArea.Max
		<< "," << FParams.ParameterSet[Set].Circularity.Min << "," << FParams.ParameterSet[Set].Circularity.Max
		<< "," << FParams.ParameterSet[Set].AspectRatio.Min << "," << FParams.ParameterSet[Set].AspectRatio.Max
		<< "\n";
	hString1.str("");
	hString1.clear();
	hString2.str("");
	hString2.clear();
	hString1 << "Index,Status";
	hString2 << ",";
	hString1 << ",Array,Section,SectionPosition,";
	hString2 << ",,,Row,Col";
	hString1 << ",Input Center,";
	hString2 << ",Row,Col";
	hString1 << ",Device Center,";
	hString2 << ",Row,Col";
	hString1 << ",Filter,Filter,Contour,Perimeter";
	hString2 << ",,String,Length,";
	hString1 << ",Area,M00,Blob Bounding Rect,";
	hString2 << ",,,Height,Width";
	hString1 << ",Aspect,Circularity";
	hString2 << ",Ratio,";
	hString1 << ",Fill,Fill,Top,Top";
	hString2 << ",Intensity,Ratio,Fraction,Number";
	hString1 << ",Top Inten,Top Inten,Top Inten";
	hString2 << ",Ave,Std,Numb";
	hString1 << ",Blob Inten,Blob Inten,Blob Inten";
	hString2 << ",Ave,Total,Numb";
	hString1 << ",Last Filling Ring,,,,,";
	hString2 << ",Ring Numb,Ave,Std,Numb,Max,Min";
	hString1 << ",Ring[0],Ring[0]/,Ring[1],Ring[1]/";
	hString2 << ",Inten,Ave Int,Inten,Ave Int";
	hString1 << ",Rotated Center,";
	hString2 << ",Row,Col";
	hString1 << ",Adjusted R/Theta,";
	hString2 << ",R,Theta";
	blobOut << hString1.str() << "\n";
	blobOut << hString2.str() << std::endl;
	mValue[2] = 0;
	// timeMsg = ">>> Before loop over sSize";
	// OutputTime(timeMsg);
	for (int n = 0; n < sSize; n++)
	{
		int topRing = sortedBlobList[n].ShapeSummary.TopFillingNumber;
		// int idx = 1000000 * sortedBlobList[n].Array + 100000 * sortedBlobList[n].Section 
			// + 1000 * sortedBlobList[n].SectionPosition.x  + sortedBlobList[n].SectionPosition.y;
		blobOut << sortedBlobList[n].SelfIndex << "," << sortedBlobList[n].Status << "," << sortedBlobList[n].Array << "," << sortedBlobList[n].Section
			<< "," << sortedBlobList[n].SectionPosition.y << "," << sortedBlobList[n].SectionPosition.x
			<< "," << sortedBlobList[n].InputCenter2f.y << "," << sortedBlobList[n].InputCenter2f.x
			<< "," << sortedBlobList[n].DeviceCenter.y << "," << sortedBlobList[n].DeviceCenter.x;

		blobOut << "," << sortedBlobList[n].ShapeSummary.FilterValue
			<< "," << sortedBlobList[n].ShapeSummary.FilterString
			<< "," << sortedBlobList[n].ShapeSummary.ContourLength
			<< "," << sortedBlobList[n].ShapeSummary.Perimeter
			<< "," << sortedBlobList[n].ShapeSummary.BlobIntensities.Count
			<< "," << sortedBlobList[n].ShapeSummary.Mom.m00
			<< "," << sortedBlobList[n].ShapeSummary.BoundingRect.height << "," << sortedBlobList[n].ShapeSummary.BoundingRect.width
			<< "," << sortedBlobList[n].ShapeSummary.AspectRatio
			<< "," << sortedBlobList[n].ShapeSummary.Circularity
			<< "," << sortedBlobList[n].ShapeSummary.FillIntensity
			<< "," << sortedBlobList[n].ShapeSummary.FillRatio
			<< "," << sortedBlobList[n].ShapeSummary.TopIntensityFraction
			<< "," << sortedBlobList[n].ShapeSummary.TopFillingNumber
			<< "," << sortedBlobList[n].ShapeSummary.TopIntensities.Ave
			<< "," << sortedBlobList[n].ShapeSummary.TopIntensities.Std
			<< "," << sortedBlobList[n].ShapeSummary.TopIntensities.Count
			<< "," << sortedBlobList[n].ShapeSummary.BlobIntensities.Ave
			<< "," << sortedBlobList[n].ShapeSummary.BlobIntensities.Sum
			<< "," << sortedBlobList[n].ShapeSummary.BlobIntensities.Count
			<< "," << topRing;

		if (sortedBlobList[n].ShapeSummary.FillingSummary.size() > topRing)
		{
			blobOut << "," << sortedBlobList[n].ShapeSummary.FillingSummary[topRing].Intensities.Ave()
				<< "," << sortedBlobList[n].ShapeSummary.FillingSummary[topRing].Intensities.Std()
				<< "," << sortedBlobList[n].ShapeSummary.FillingSummary[topRing].Intensities.Count()
				<< "," << sortedBlobList[n].ShapeSummary.FillingSummary[topRing].MaxInten
				<< "," << sortedBlobList[n].ShapeSummary.FillingSummary[topRing].MinInten;
		}
		else
		{
			blobOut << ",0,0,0,0,0";
		}
		if (sortedBlobList[n].ShapeSummary.FillingSummary.size() > 0)
		{
			blobOut << ","
				<< sortedBlobList[n].ShapeSummary.FillingSummary[0].Intensities.Ave() << ","
				<< sortedBlobList[n].ShapeSummary.FillingSummary[0].Intensities.Ave() / sortedBlobList[n].ShapeSummary.FillingSummary[topRing].Intensities.Ave();
		}
		else
		{
			blobOut << ",0,0";
		}

		if (sortedBlobList[n].ShapeSummary.FillingSummary.size() > 1)
		{
			blobOut << "," << sortedBlobList[n].ShapeSummary.FillingSummary[1].Intensities.Ave() << ","
				<< sortedBlobList[n].ShapeSummary.FillingSummary[1].Intensities.Ave() / sortedBlobList[n].ShapeSummary.FillingSummary[topRing].Intensities.Ave();
		}
		else
		{
			blobOut << ",0,0";
		}
		blobOut << "," << sortedBlobList[n].RotatedCenter2f.y << "," << sortedBlobList[n].RotatedCenter2f.x;
		blobOut << "," << sortedBlobList[n].AdjustedCenter2f.y << "," << sortedBlobList[n].AdjustedCenter2f.x;
		//		blobOut << "," << sortedBlobList[n].ShapeSummary.AddedBlobIntensities.Sum << "," << sortedBlobList[n].ShapeSummary.AddedBlobIntensities.Count
					//<< "," << sortedBlobList[n].ShapeSummary.TotalBlobIntensities.Sum << "," << sortedBlobList[n].ShapeSummary.AltFillRatio;

		blobOut << std::endl;

		//	if ( TrackFilling && sortedBlobList[n].ShapeSummary.FilterValue == 1 && (sortedBlobList[n].Array == 0 || sortedBlobList[n].Section == 0 ) )
		//		unassignedBlobList.push_back(sortedBlobList[n]);
	}
	//if ( TrackFilling && unassignedBlobList.size() > 0 )
	//{
	//	blobOut << "\n==========\n";
	//	for ( int n = 0 ; n < unassignedBlobList.size() ; n++ )
	//	{
	//		int topRing = unassignedBlobList[n].ShapeSummary.TopFillingNumber;
	//		blobOut << unassignedBlobList[n].SelfIndex << "," << unassignedBlobList[n].Status 
	//			<< "," << unassignedBlobList[n].Array <<  "," << unassignedBlobList[n].Section
	//			<< "," << unassignedBlobList[n].SectionPosition.y <<  "," << unassignedBlobList[n].SectionPosition.x 
	//			<< "," << unassignedBlobList[n].InputCenter2f.y << "," << unassignedBlobList[n].InputCenter2f.x
	//			<< "," << unassignedBlobList[n].DeviceCenter.y << "," << unassignedBlobList[n].DeviceCenter.x;

	//		blobOut << "," << unassignedBlobList[n].ShapeSummary.FilterValue 
	//			<< "," << unassignedBlobList[n].ShapeSummary.FilterString 
	//			<< "," << unassignedBlobList[n].ShapeSummary.ContourLength 
	//			<< "," << unassignedBlobList[n].ShapeSummary.Perimeter
	//			<< "," << unassignedBlobList[n].ShapeSummary.BlobIntensities.Count
	//			<< "," << unassignedBlobList[n].ShapeSummary.Mom.m00 
	//			<< "," << unassignedBlobList[n].ShapeSummary.BoundingRect.height << "," << unassignedBlobList[n].ShapeSummary.BoundingRect.width
	//			<< "," << unassignedBlobList[n].ShapeSummary.AspectRatio 
	//			<< "," << unassignedBlobList[n].ShapeSummary.Circularity
	//			<< "," << unassignedBlobList[n].ShapeSummary.FillIntensity 
	//			<< "," << unassignedBlobList[n].ShapeSummary.FillRatio
	//			<< "," << unassignedBlobList[n].ShapeSummary.TopIntensityFraction 
	//			<< "," << unassignedBlobList[n].ShapeSummary.TopFillingNumber
	//			<< "," << unassignedBlobList[n].ShapeSummary.TopIntensities.Ave 
	//			<< "," << unassignedBlobList[n].ShapeSummary.TopIntensities.Std 
	//			<< "," << unassignedBlobList[n].ShapeSummary.TopIntensities.Count
	//			<< "," << unassignedBlobList[n].ShapeSummary.BlobIntensities.Ave 
	//			<< "," << unassignedBlobList[n].ShapeSummary.BlobIntensities.Sum 
	//			<< "," << unassignedBlobList[n].ShapeSummary.BlobIntensities.Count
	//			<< "," << topRing ;

	//		if ( unassignedBlobList[n].ShapeSummary.FillingSummary.size() > topRing )
	//		{
	//			blobOut << "," << unassignedBlobList[n].ShapeSummary.FillingSummary[topRing].Intensities.Ave() 
	//				<< "," << unassignedBlobList[n].ShapeSummary.FillingSummary[topRing].Intensities.Std()
	//				<< "," << unassignedBlobList[n].ShapeSummary.FillingSummary[topRing].Intensities.Count()
	//				<< "," << unassignedBlobList[n].ShapeSummary.FillingSummary[topRing].MaxInten 
	//				<< "," << unassignedBlobList[n].ShapeSummary.FillingSummary[topRing].MinInten;
	//		}
	//		else
	//		{
	//			blobOut << ",0,0,0,0,0";
	//		}
	//		if ( unassignedBlobList[n].ShapeSummary.FillingSummary.size() > 0 )
	//		{
	//			blobOut << ","
	//			<< unassignedBlobList[n].ShapeSummary.FillingSummary[0].Intensities.Ave() << ","
	//			<< unassignedBlobList[n].ShapeSummary.FillingSummary[0].Intensities.Ave() / unassignedBlobList[n].ShapeSummary.FillingSummary[topRing].Intensities.Ave() ;
	//		}
	//		if ( unassignedBlobList[n].ShapeSummary.FillingSummary.size() > 1 )
	//		{
	//			blobOut << "," << unassignedBlobList[n].ShapeSummary.FillingSummary[1].Intensities.Ave() << ","
	//				<< unassignedBlobList[n].ShapeSummary.FillingSummary[1].Intensities.Ave() / unassignedBlobList[n].ShapeSummary.FillingSummary[topRing].Intensities.Ave() ;
	//		}
	//		blobOut << "," << unassignedBlobList[n].ShapeSummary.AddedBlobIntensities.Sum << "," << unassignedBlobList[n].ShapeSummary.AddedBlobIntensities.Count
	//			<< "," << unassignedBlobList[n].ShapeSummary.TotalBlobIntensities.Sum << "," << unassignedBlobList[n].ShapeSummary.AltFillRatio;
	//			
	//		blobOut << std::endl;
	//	}
	//}
	for ( int n = 0 ; n < BlobList.size() ; n++ )
	{
		if ( BlobList[n].Shape.FilterValue > 0 && BlobList[n].Array > 0 )
		{
			int pSize = (int)BlobList[n].Shape.PixelList.size();
			for ( int m = 0 ; m < pSize ; m++ )
			{
				cValue = GImage.at<cv::Vec3b>(BlobList[n].Shape.PixelList[m]);
				if ( BlobList[n].Shape.FilterValue == 1 && !BlobList[n].Shape.AnomalousFilling )
				{
					cValue[2] = 0;
					if (BlobList[n].Status > 0)
					{
						cValue[0] = 0;
						cValue[1] = AdjustColorIntensity(cValue[1]);
					}
					else
					{
						cValue[1] /= 2;
						if (cValue[1] < 63)
							cValue[1] = 63;
						cValue[0] = cValue[1] + 20;
					}
					GImage.at<cv::Vec3b>(BlobList[n].Shape.PixelList[m]) = cValue;
				}
				else if ( BlobList[n].Shape.FilterValue == 2 && !BlobList[n].Shape.AnomalousFilling )
				{
					cValue[1] = cValue[2] = 0;
					cValue[0] = AdjustColorIntensity(cValue[0]);
					GImage.at<cv::Vec3b>(BlobList[n].Shape.PixelList[m]) = cValue;
				}
				else if ( BlobList[n].Shape.FilterValue > 2 && !BlobList[n].Shape.AnomalousFilling )
				{
					cValue[0] = 0;
					if ( cValue[1] < 63 )
					{
						cValue[2] = cValue[1] = AdjustColorIntensity(cValue[1]) / 2;
					}
					else
					{
						cValue[2] = cValue[1] = 129;
					}
					GImage.at<cv::Vec3b>(BlobList[n].Shape.PixelList[m]) = cValue;
				}
				else
				{
					cValue[0] = cValue[1] = 0;
					cValue[2] = AdjustColorIntensity(cValue[2]);
					GImage.at<cv::Vec3b>(BlobList[n].Shape.PixelList[m]) = cValue;
				}
			}
		}
	}
	for ( int n = 0 ; n < OtherBlobList.size() ; n++ )
	{
		if ( OtherBlobList[n].Shape.FilterValue > 0 && OtherBlobList[n].Array > 0 )
		{
			int pSize = (int)OtherBlobList[n].Shape.PixelList.size();
			for ( int m = 0 ; m < pSize ; m++ )
			{
				cValue = GImage.at<cv::Vec3b>(OtherBlobList[n].Shape.PixelList[m]);
				if ( OtherBlobList[n].Shape.FilterValue == 1 && !OtherBlobList[n].Shape.AnomalousFilling )
				{
					cValue[0] = cValue[2] = 0;
					cValue[1] = AdjustColorIntensity(cValue[1]);
					GImage.at<cv::Vec3b>(OtherBlobList[n].Shape.PixelList[m]) = cValue;

				}
				else if ( OtherBlobList[n].Shape.FilterValue == 2 && !OtherBlobList[n].Shape.AnomalousFilling )
				{
					cValue[1] = cValue[2] = 0;
					cValue[0] = AdjustColorIntensity(cValue[0]);
					GImage.at<cv::Vec3b>(OtherBlobList[n].Shape.PixelList[m]) = cValue;

				}
				else if ( OtherBlobList[n].Shape.FilterValue > 2 && !OtherBlobList[n].Shape.AnomalousFilling )
				{
					cValue[0] = 0;
					if ( cValue[1] < 63 )
					{
						cValue[2] = cValue[1] = AdjustColorIntensity(cValue[1]) / 2;
					}
					else
					{
						cValue[2] = cValue[1] = 129;
					}
					GImage.at<cv::Vec3b>(OtherBlobList[n].Shape.PixelList[m]) = cValue;

				}
				else
				{
					cValue[0] = cValue[1] = 0;
					cValue[2] = AdjustColorIntensity(cValue[2]);
					GImage.at<cv::Vec3b>(OtherBlobList[n].Shape.PixelList[m]) = cValue;

				}
			}
		}
	}
	for ( int n = 0 ; n < BadBlobList.size() ; n++ )
	{
		int pSize = (int)BadBlobList[n].Shape.PixelList.size();
		for (int m = 0; m < pSize; m++)
		{
			cValue = GImage.at<cv::Vec3b>(BadBlobList[n].Shape.PixelList[m]);
			cValue[0] = cValue[1] = 0;
			cValue[2] = AdjustColorIntensity(cValue[2]);
			GImage.at<cv::Vec3b>(BadBlobList[n].Shape.PixelList[m]) = cValue;
		}
	}
	// timeMsg = ">>> After loop over sSize";
	// OutputTime(timeMsg);
	bCnt = 0;
	trialContours.clear();
	for ( int n = 0 ; n < BlobList.size() ; n++ )
	{
		trialContours.push_back(BlobList[n].Shape.Contour);
		cv::drawContours(Mask, trialContours, bCnt, bCnt+1, -1);
		cv::drawContours(ContMask, trialContours, bCnt, bCnt+1, 1);
		bCnt++;
	}
	for ( int n = 0 ; n < OtherBlobList.size() ; n++ )
	{
		if ( Mask.at<short unsigned int>(cv::Point((int)OtherBlobList[n].InputCenter2f.x, (int)OtherBlobList[n].InputCenter2f.y) ) == 0 )
		{
			trialContours.push_back(OtherBlobList[n].Shape.Contour);
			cv::drawContours(Mask, trialContours, bCnt, bCnt+1, -1);
			cv::drawContours(ContMask, trialContours, bCnt, bCnt+1, 1);
			bCnt++;
		}
	}

	for ( int n = 0 ; n < BadBlobList.size() ; n++ )
	{
		if ( bCnt > 65534 )
			break;
		if ( Mask.at<short unsigned int>(cv::Point((int)BadBlobList[n].InputCenter2f.x, (int)BadBlobList[n].InputCenter2f.y) ) == 0
			&& BadBlobList[n].Shape.FilterValue != 10 && BadBlobList[n].Shape.FilterValue != 0 )
		{
			trialContours.push_back(BadBlobList[n].Shape.Contour);
			cv::drawContours(Mask, trialContours, bCnt, bCnt+1, -1);
			cv::drawContours(ContMask, trialContours, bCnt, bCnt+1, 1);
			bCnt++;
		}
	}
	// timeMsg = ">>> After draw contours";
	// OutputTime(timeMsg);
	blobOut.close();
	// std::cout << "At end of WriteSizingFile" << std::endl;
	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//
// Writing Sizing information about blobs in BlobList.
//
//			Input
// BlobList - vector<BlobSummary> vector of blobs to be written to output file. These blobs are acceptable and used for 
//		quantitation. SortBlobsSizing is used to sort blobs before output.
// Results - vector of results (X-Offset, Y-Offset, Angle, Scale)
// MinRingAve - 
// MaxFillingRatio - 
// FParams - 
// Set - 
// BackgroundSubImage32F - background subtracted image
// OutName - name of output file
// GImage - Color image created from BackgroundSubImage32F. Pixels within blobs are turned blue or green depending oneBlob
//		the blob's Filter Value.  If blob meets all the criteria the pixels are turned blue.  If the blob meets all the 
//		criteria, excpet that it is a little too large or small, the pixels are turned green.  The intensity of the blue
//		or green is proportional to the intensity in BackgroundSubImage32F.
//
bool WriteSizingFile(std::vector< BlobSummary > &BlobList, std::vector< double > &Results,
	double MinRingAve, double MaxFillingRatio, AnalysisParameters &FParams, int Set, cv::Mat &BackgroundSubImage32F,
	std::string &OutName, cv::Mat &GImage )
{
	// std::cout << "Inside WriteSizingFile" << std::endl;
	// std::cout << "     List Sizes = " << BlobList.size() << std::endl;
	
	cv::Size imageSize = BackgroundSubImage32F.size();
	// int imageSizeTotal = BackgroundSubImage32F.total();
	cv::Vec3b cValue;
	cv::Vec3b mValue;
	std::ofstream blobOut;
	std::ostringstream hString1;
	std::ostringstream hString2;
	Stat1Results s1Results;
	int bSize = (int)BlobList.size();
	std::vector< BlobSummary > sortedBlobList;
	sortedBlobList.resize(bSize);
	cv::Mat workImage(imageSize, CV_8UC1);
	workImage.setTo(0);
	Blob tmpBlob;
	int bCnt = 0;
	for ( int n = 0 ; n < BlobList.size() ; n++ )
	{
		if ( BlobList[n].ShapeSummary.Status == 0 && BlobList[n].ShapeSummary.FilterValue >= 0 )
		{
			sortedBlobList[bCnt] = BlobList[n];
			bCnt++;
		}
	}
	// int lastOrigBlob = bCnt;
	// std::cout << "After sorting BlobList, bCnt = " << bCnt << std::endl;
	
	sortedBlobList.resize(bCnt);
	sort(sortedBlobList.begin(), sortedBlobList.end(), SortBlobsSizingSOI );
	int sSize = (int)sortedBlobList.size();
	GImage.create(imageSize, CV_8UC3);
	cv::Mat workImage1;
	cv::Mat workImage2;
	
	workImage1.create(BackgroundSubImage32F.size(), CV_32F);
	workImage2.create(BackgroundSubImage32F.size(), CV_8U);
	double minV;
	double maxV;
	cv::minMaxLoc(BackgroundSubImage32F, &minV, &maxV, NULL, NULL );
	// std::clog << "minV/maxV " << minV << "/" << maxV << std::endl;
	workImage1 = cv::max(BackgroundSubImage32F, 0.0);
	if ( minV < 0.0 )
		minV = 0.0;

	if ( maxV - minV < 255.0 )
	{	
		workImage1 -= minV;
		workImage1.convertTo(workImage2, CV_8U );
	}
	else
	{
		double iScale = 255.0 / (maxV - minV);
		for ( int n = 0 ; n < workImage1.total() ; n++ )
			workImage2.at<uchar>(n) = (uchar)round(iScale * ( workImage1.at<float>(n) - minV ) );
	}
	
	cv::cvtColor(workImage2, GImage, cv::COLOR_GRAY2RGB);
	
	blobOut.open( OutName.c_str() );
	if ( !blobOut.is_open() )
		return false;
	// std::cout << "After blobOut.open(), OutName = " << OutName << std::endl;
	blobOut << "Count,Results[0],Results[1],Results[2],Results[3],,"
		<< "MinRingAve,MaxFillingRatio,Hght Min,Hght Max,Wdth Min,Wdth Max,Area Min,Area Max,Circ Min,Circ Max,AspR Min,AspR Max\n";
	blobOut << bCnt << "," << Results[0] << "," << Results[1] << "," << Results[2] << "," << Results[3] 
		<< "," << MinRingAve << "," << MaxFillingRatio << ","
		<< "," << FParams.ParameterSet[Set].BlobHeight.Min << "," << FParams.ParameterSet[Set].BlobHeight.Max
		<< "," << FParams.ParameterSet[Set].BlobWidth.Min << "," << FParams.ParameterSet[Set].BlobWidth.Max
		<< "," << FParams.ParameterSet[Set].BlobArea.Min << "," << FParams.ParameterSet[Set].BlobArea.Max
		<< "," << FParams.ParameterSet[Set].Circularity.Min << "," << FParams.ParameterSet[Set].Circularity.Max
		<< "," << FParams.ParameterSet[Set].AspectRatio.Min << "," << FParams.ParameterSet[Set].AspectRatio.Max
		<< "\n";
	hString1.str("");
	hString1.clear();
	hString2.str("");
	hString2.clear();
	hString1 << "Index,Status";
	hString2 << ",";
	hString1 << ",Array,Section,SectionPosition,";
	hString2 << ",,,Row,Col";
	hString1 << ",Input Center,";
	hString2 << ",Row,Col";
	hString1 << ",Device Center,";
	hString2 << ",Row,Col";
	hString1 << ",Filter,Filter,Contour,Perimeter";
	hString2 << ",,String,Length,";
	hString1 << ",Area,M00,Blob Bounding Rect,";
	hString2 << ",,,Height,Width";
	hString1 << ",Aspect,Circularity";
	hString2 << ",Ratio,";
	hString1 << ",Fill,Fill,Top,Top";
	hString2 << ",Intensity,Ratio,Fraction,Number";
	hString1 << ",Top Inten,Top Inten,Top Inten";
	hString2 << ",Ave,Std,Numb";
	hString1 << ",Mask Int,Mask Int,Mask Int";
	hString2 << ",Ave,Total,Numb";
	hString1 << ",Last Filling Ring,,,,,";
	hString2 << ",Ring Numb,Ave,Std,Numb,Max,Min";
	blobOut << hString1.str() << "\n";
	blobOut << hString2.str() << std::endl;
	// std::cout << "After write header strings" << std::endl;
	// for ( int n = 0 ; n < sortedBlobList.size() ; n++ )
	mValue[2] = 0;
	for ( int n = 0 ; n < sSize ; n++ )
	{	
		int fv = sortedBlobList[n].ShapeSummary.FilterValue;
		if ( fv >= 0 )
		{
			int topRing = sortedBlobList[n].ShapeSummary.TopFillingNumber;
			// int idx = 1000000 * sortedBlobList[n].Array + 100000 * sortedBlobList[n].Section 
				// + 1000 * sortedBlobList[n].SectionPosition.x  + sortedBlobList[n].SectionPosition.y;
			int status;
			if (fv == 1)
				status = 1;
			else
				status = 0;
			blobOut << sortedBlobList[n].SelfIndex << "," << status << "," << sortedBlobList[n].Array <<  "," << sortedBlobList[n].Section 
				<< "," << sortedBlobList[n].SectionPosition.y <<  "," << sortedBlobList[n].SectionPosition.x 
				<< "," << sortedBlobList[n].InputCenter2f.y << "," << sortedBlobList[n].InputCenter2f.x
				<< "," << sortedBlobList[n].DeviceCenter.y << "," << sortedBlobList[n].DeviceCenter.x;
			// if (n >= lastOrigBlob - 1 )
			// {
				// std::cout << "Inside WriteSizingFile, n = " << n << ", sIdx = " << sIdx << ", (r, c) = (" 
					// << sortedBlobList[n].InputCenter2f.y << ", " << sortedBlobList[n].InputCenter2f.x << ")" << std::endl;
			// }
			int fv = sortedBlobList[n].ShapeSummary.FilterValue;
			if ( sortedBlobList[n].ShapeSummary.AnomalousFilling )
				fv += 200;
			blobOut << "," << fv
				<< "," << sortedBlobList[n].ShapeSummary.FilterString 
				<< "," << sortedBlobList[n].ShapeSummary.ContourLength 
				<< "," << sortedBlobList[n].ShapeSummary.Perimeter
				<< "," << sortedBlobList[n].ShapeSummary.Area
				<< "," << sortedBlobList[n].ShapeSummary.Mom.m00 
				<< "," << sortedBlobList[n].ShapeSummary.BoundingRect.height << "," << sortedBlobList[n].ShapeSummary.BoundingRect.width
				<< "," << sortedBlobList[n].ShapeSummary.AspectRatio 
				<< "," << sortedBlobList[n].ShapeSummary.Circularity
				<< "," << sortedBlobList[n].ShapeSummary.FillIntensity 
				<< "," << sortedBlobList[n].ShapeSummary.FillRatio
				<< "," << sortedBlobList[n].ShapeSummary.TopIntensityFraction 
				<< "," << sortedBlobList[n].ShapeSummary.TopFillingNumber
				
				<< "," << sortedBlobList[n].ShapeSummary.TopIntensities.Ave 
				<< "," << sortedBlobList[n].ShapeSummary.TopIntensities.Std 
				<< "," << sortedBlobList[n].ShapeSummary.TopIntensities.Count
				
				<< "," << sortedBlobList[n].ShapeSummary.MaskedBlobIntensities.Ave
				<< "," << sortedBlobList[n].ShapeSummary.MaskedBlobIntensities.Sum
				<< "," << sortedBlobList[n].ShapeSummary.MaskedBlobIntensities.Count
				<< "," << topRing ;
			if ( sortedBlobList[n].ShapeSummary.LastRingIntensities.Count > 0 )
			{
				blobOut << "," << sortedBlobList[n].ShapeSummary.LastRingIntensities.Ave 
					<< "," << sortedBlobList[n].ShapeSummary.LastRingIntensities.Std
					<< "," << sortedBlobList[n].ShapeSummary.LastRingIntensities.Count
					<< "," << sortedBlobList[n].ShapeSummary.LastRingMaxIntensity 
					<< "," << sortedBlobList[n].ShapeSummary.LastRingMinIntensity;
			}
			else
			{
				blobOut << ",0,0,0,0,0";
			}
			blobOut << std::endl;
			
			if ( sortedBlobList[n].ShapeSummary.FilterValue > 0 && sortedBlobList[n].Array > 0 )
			{
				int pSize = (int)sortedBlobList[n].ShapeSummary.PixelList.size();
				for ( int m = 0 ; m < pSize ; m++ )
				{
					cValue = GImage.at<cv::Vec3b>(sortedBlobList[n].ShapeSummary.PixelList[m]);
					if ( sortedBlobList[n].ShapeSummary.FilterValue == 1 && !sortedBlobList[n].ShapeSummary.AnomalousFilling )
					{
						cValue[0] = cValue[2] = 0;
						if ( cValue[1] < 127 )
							cValue[1] *= 2;
						else
							cValue[1] = 255;
						GImage.at<cv::Vec3b>(sortedBlobList[n].ShapeSummary.PixelList[m]) = cValue;
					}
					else if ( sortedBlobList[n].ShapeSummary.FilterValue == 2 && !sortedBlobList[n].ShapeSummary.AnomalousFilling )
					{
						cValue[1] = cValue[2] = 0;
						if ( cValue[0] < 127 )
							cValue[0] *= 2;
						else
							cValue[0] = 255;
						GImage.at<cv::Vec3b>(sortedBlobList[n].ShapeSummary.PixelList[m]) = cValue;
					}
					else if ( sortedBlobList[n].ShapeSummary.FilterValue > 2 && !sortedBlobList[n].ShapeSummary.AnomalousFilling )
					{
						cValue[0] = 0;
						if ( cValue[1] < 63 )
						{
							cValue[2] = ( cValue[1] *= 2 );
						}
						else
						{
							cValue[2] = cValue[1] = 127;
						}
						GImage.at<cv::Vec3b>(sortedBlobList[n].ShapeSummary.PixelList[m]) = cValue;
					}
					else
					{
						cValue[0] = cValue[1] = 0;
						if ( cValue[2] < 127 )
							cValue[2] *= 2;
						else
							cValue[2] = 255;
						GImage.at<cv::Vec3b>(sortedBlobList[n].ShapeSummary.PixelList[m]) = cValue;
					}
				}
			}
		}
	}
	blobOut.close();
	// std::cout << "At end of WriteSizingFile (Blob Summary)" << std::endl;
	return true;
}
