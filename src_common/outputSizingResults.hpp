#ifndef OUTPUTSIZINGRESULTS_HPP
#define OUTPUTSIZINGRESULTS_HPP

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "physicalconstants.h"
#include "statistics.hpp"
#include "blob.hpp"
#include "readinfofile.hpp"

class SizingResults
{
	public:
		int Array;
		int Section;
		Statistics1 FillingFrac;
		Statistics1 GoodFillingFrac;
		
		SizingResults(): Array(0), Section(0)
		{
			FillingFrac.Clear();
			GoodFillingFrac.Clear();
		}
		~SizingResults(){}
		
		SizingResults(const SizingResults &Source): Array(Source.Array), Section(Source.Section)
		{
			FillingFrac = Source.FillingFrac;
			GoodFillingFrac = Source.GoodFillingFrac;
		}
		
		SizingResults& operator=(const SizingResults &Source)
		{
			if ( this !=&Source )
			{
				Array = Source.Array;
				Section = Source.Section;
				FillingFrac = Source.FillingFrac;
				GoodFillingFrac = Source.GoodFillingFrac;
			}
			return *this;
		}
		
		void Clear()
		{
			Array = 0;
			Section = 0;
			FillingFrac.Clear();
			GoodFillingFrac.Clear();
		}
};

void OutputSizingResults(std::vector<ArraySection> ArrSecList, std::vector< Blob > &BlobList, std::vector< Blob > &OtherBlobList, std::string BaseName);

#endif  // 	OUTPUTSIZINGRESULTS_HPP