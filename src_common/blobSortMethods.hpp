#ifndef BLOBSORTMETHODS_H
#define BLOBSORTMETHODS_H
//! [includes]

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//#include <iostream>
//#include <string>
//#include <cstdlib>
//! [includes]

#include <vector>
#include "blob.hpp"

bool SortBlobsByColumnSmall(Blob A, Blob B);

bool SortBySelfIndex( Blob A, Blob B );

namespace SortBlobMethods
{
	static double ByColsSortDelta;
	bool ByCols(Blob A, Blob B);

	static double ByArraySectionDelta;
	bool ByArraySection(Blob A, Blob B);
}


#endif // BLOBSORTMETHODS_H

