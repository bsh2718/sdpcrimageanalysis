#ifndef CHECKSECTIONONE_H
#define CHECKSECTIONONE_H
//! [includes]
#include <vector>
#include <algorithm>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//#include <iostream>
//#include <string>
//#include <cstdlib>
//! [includes]

#include "blob.hpp"

bool CheckSectionOne(std::ofstream& LogFile, cv::Size ImageSize, cv::Point2f WellSpacing, int FirstRowSum, cv::Mat& WorkImage,
	std::vector<Blob>& BlobList, std::vector<Blob>& OtherBlobList, std::vector<Blob>& BadBlobList, bool Track = false);

#endif // CHECKSECTIONONE_H

