#ifndef DEVICEORIGINS_HPP
#define DEVICEORIGINS_HPP
//! [includes]
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//#include <iostream>
//#include <string>
//#include <cstdlib>
//! [includes]

//#include "chamber.h"

//! [namespace]
//using namespace std;
//using namespace cv;
//! [namespace]

class DeviceOrigins
{
	public:
		int Index;				// Array or Section index
		cv::Point2f Origin;		// Location of upper left well in array or section.  
								// If object is being used for an array, Origin.y is ignored
								// If object is being used for a section, Origin.x is ignored
		DeviceOrigins(): Index(0), Origin(cv::Point2f(0.0, 0.0))
		{}
		~DeviceOrigins(){}
		
		DeviceOrigins& operator= ( const DeviceOrigins &Source )
		{
			if ( this !=&Source )
			{
				Index = Source.Index;
				Origin = Source.Origin;
			}
			return *this;
		}
		
		DeviceOrigins( const DeviceOrigins &Source )
		{
			Index = Source.Index;
			Origin = Source.Origin;
		}
};

#endif // DEVICEORIGINS_HPP