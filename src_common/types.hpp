#ifndef TYPES_HPP
#define TYPES_HPP

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

std::string type2str(int type);


bool IsEightBit(int type);

#endif // TYPES_HPP
