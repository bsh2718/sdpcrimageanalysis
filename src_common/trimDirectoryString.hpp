#ifndef TRIMDIRECTORYSTRING_H
#define TRIMDIRECTORYSTRING_h
//! [includes]
#include <string>

std::string trimDirectoryString(const std::string& s);

#endif // TRIMDIRECTORYSTRING_H

