#pragma once
//! [includes]
#include <vector>
#include <iostream>
#include <sstream>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//#include <iostream>
//#include <string>
//#include <cstdlib>
//! [includes]

#include "blob.hpp"
#include "statistics.hpp"
#include "physicalconstants.h"

//! [namespace]
//using namespace std;
//using namespace cv;
//! [namespace]
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Blob_CD2 is used as a class for information about blobs in the image from a CD (radial columns)
//
class Blob_CD2
{
public:
	int MainListIndex;					// Self Index for this blob in primary BlobList
	int SelfIndex;
	int Status;
	int FilterValue;
	cv::Point2f InputCenter2f;	// Input center after adjustment for image distortions (from primary BlobList)
	cv::Point2f InputTheta_R2f;
	cv::Point2f AdjustedCenter2f;
	cv::Point2f Theta_R2f;
	cv::Point DeviceCenter;
	bool OnImage;

	int Section;
	int SubSection;
	cv::Point SubSectionPosition;

	Blob_CD2() : MainListIndex(-1), SelfIndex(-1), Status(0), FilterValue(0), InputCenter2f(cv::Point2f(0.0, 0.0)),
		InputTheta_R2f(cv::Point2f(0.0, 0.0)), AdjustedCenter2f(cv::Point2f(0.0, 0.0)),
		Theta_R2f(cv::Point2f(0.0, 0.0)), DeviceCenter(cv::Point(0, 0)), OnImage(false), 
		Section(0), SubSection(0), SubSectionPosition(cv::Point(0,0))
	{}

	~Blob_CD2() {}

	Blob_CD2& operator= (const Blob_CD2& Source)
	{
		if (this != &Source)
		{
			MainListIndex = Source.MainListIndex;
			SelfIndex = Source.SelfIndex;
			Status = Source.Status;
			FilterValue = Source.FilterValue;
			InputCenter2f = Source.InputCenter2f;
			InputTheta_R2f = Source.InputTheta_R2f;
			AdjustedCenter2f = Source.AdjustedCenter2f;
			Theta_R2f = Source.Theta_R2f;
			DeviceCenter = Source.DeviceCenter;
			OnImage = Source.OnImage;
			Section = Source.Section;
			SubSection = Source.SubSection;
			SubSectionPosition = Source.SubSectionPosition;
		}
		return *this;
	}

	Blob_CD2(const Blob_CD2& Source) : MainListIndex(Source.MainListIndex), SelfIndex(Source.SelfIndex),
		Status(Source.Status), FilterValue(Source.FilterValue), InputCenter2f(Source.InputCenter2f), InputTheta_R2f(Source.InputTheta_R2f),
		AdjustedCenter2f(Source.AdjustedCenter2f), Theta_R2f(Source.Theta_R2f), DeviceCenter(Source.DeviceCenter),
		OnImage(Source.OnImage), 
		Section(Source.Section), SubSection(Source.SubSection), SubSectionPosition(Source.SubSectionPosition)
	{}

	Blob_CD2(const Blob& Source, int Self) : MainListIndex(Source.SelfIndex), SelfIndex(Self),
		Status(Source.Status), FilterValue(Source.Shape.FilterValue), InputCenter2f(Source.InputCenter2f), InputTheta_R2f(cv::Point2f(0.0, 0.0)),
		AdjustedCenter2f(cv::Point2f(0.0, 0.0)), Theta_R2f(cv::Point2f(0.0, 0.0)), DeviceCenter(cv::Point(0, 0)),
		OnImage(false), 
		Section(0), SubSection(0), SubSectionPosition(cv::Point(0, 0))
	{}

	void Clear()
	{
		MainListIndex = -1;
		SelfIndex = -1;
		Status = 0;
		FilterValue = 0;
		InputCenter2f = cv::Point2f(0.0, 0.0);
		InputTheta_R2f = cv::Point2f(0.0, 0.0);
		AdjustedCenter2f = cv::Point2f(0.0, 0.0);
		Theta_R2f = cv::Point2f(0.0, 0.0);
		DeviceCenter = cv::Point(0, 0);
		OnImage = false;
		Section = 0;
		SubSection = 0;
		SubSectionPosition = cv::Point(0,0);
	}

	void LoadBlob(Blob Source, int SelfIndex, cv::Point2f ImageCenter)
	{
		MainListIndex = Source.SelfIndex;
		Status = Source.Status;
		InputCenter2f = Source.InputCenter2f - ImageCenter;
		InputTheta_R2f.y = sqrt(pow(InputCenter2f.x, 2) + pow(InputCenter2f.y, 2));
		if (InputTheta_R2f.y < 1.0E-6)
			InputTheta_R2f.x = 0.0;
		else
		{
			InputTheta_R2f.x = acos(InputCenter2f.x / InputTheta_R2f.y);
			if (InputCenter2f.y < 0.0)
				InputTheta_R2f.x += PI;
		}
		AdjustedCenter2f = InputCenter2f;
		Theta_R2f = InputTheta_R2f;
		DeviceCenter.x = (int)round(InputCenter2f.x);
		DeviceCenter.y = (int)round(InputCenter2f.y);
		OnImage = true;
		Section = 0;
		SubSection = 0;
		SubSectionPosition = cv::Point(0, 0);
	}

	bool ApplyShift(cv::Point2f Shift, double Rotation, cv::Size SectionImageSize)
	{
		double newTheta = InputTheta_R2f.x + Rotation;
		AdjustedCenter2f.x = InputTheta_R2f.y * cos(newTheta) + Shift.x;
		AdjustedCenter2f.y = InputTheta_R2f.y * sin(newTheta) + Shift.y;
		DeviceCenter.x = (int)round(AdjustedCenter2f.x);
		DeviceCenter.y = (int)round(AdjustedCenter2f.y);
		if (DeviceCenter.x >= 0 && DeviceCenter.x < SectionImageSize.width
			&& DeviceCenter.y >= 0 && DeviceCenter.y < SectionImageSize.height)
			OnImage = true;
		else
			OnImage = false;

		return OnImage;
	}
	bool ApplyShiftScale(cv::Point2f Shift, double Rotation, double Scale, cv::Size SectionImageSize)
	{
		double newTheta = InputTheta_R2f.x + Rotation;
		AdjustedCenter2f.x = Scale * InputTheta_R2f.y * cos(newTheta) + Shift.x;
		AdjustedCenter2f.y = Scale * InputTheta_R2f.y * sin(newTheta) + Shift.y;
		DeviceCenter.x = (int)round(AdjustedCenter2f.x);
		DeviceCenter.y = (int)round(AdjustedCenter2f.y);
		if (DeviceCenter.x >= 0 && DeviceCenter.x < SectionImageSize.width
			&& DeviceCenter.y >= 0 && DeviceCenter.y < SectionImageSize.height)
			OnImage = true;
		else
			OnImage = false;

		return OnImage;
	}
	

	std::string ToString()
	{
		std::ostringstream oString;
		oString.str("");
		oString.clear();
		oString << MainListIndex << "," << SelfIndex << "," << Status << "," << FilterValue << ","
			<< InputCenter2f.y << "," << InputCenter2f.x << ","
			<< InputTheta_R2f.y << "," << InputTheta_R2f.x << ","
			<< AdjustedCenter2f.y << "," << AdjustedCenter2f.x << ","
			<< Theta_R2f.y << "," << Theta_R2f.x << ","
			<< DeviceCenter.y << "," << DeviceCenter.x << ","
			<< OnImage << "," 
			<< Section << "," << SubSection << ","
			<< SubSectionPosition.x << "," << SubSectionPosition.y;
		return oString.str();
	}

	std::string Header()
	{
		return "MainIndex,SelfIndex,Status,FilterValue,InpCtr2f.y,InpCtr2f.x,InpT_R2f.y,InpT_R2f.x,AdjT_R2f.x,AdjT_R2f.y,T_R2f.y,T_R2f.x,DevCtr.y,DevCtr.x,OnImg,TRCalc,Sec,SubSec,SubSe.x,SubSec.y";
	}
};