#ifndef GUARDERRORS_HPP
#define GUARDERRORS_HPP

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "statistics.hpp"
#include "readinfofile.hpp"
#include "calccentroid.hpp"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// class to store guard errors in image alignment.  There is one these for each guard in X and each guard in Y.
//
// If a guard is the first one (far left for XGuard or top for YGuard), then PrevIsEdge is true.
// If a guard is the last one (far right for YGuard or bottom for YGuard), then NextIsEdge is true
// LowerBound and UpperBound mark the beginning and end of each guard.
// For each well-like blob found in a guard region, the class will store
//		1. Location - the location of the blob centroid in the aligned image (which should be in the guard image)
//		2. OrigLocation - the location of the blob in the input image
//		3. BlobIndex - the index of the blob in BlobList
//		4. Error - the value of the error associated with the blob centroid in its aligned location (not really used anymore)
//
#define OFF_TOP 1
#define OFF_BOTTOM 2
#define OFF_LEFT 3
#define OFF_RIGHT 4
#define OFFIMAGELISTSIZE 5
class GuardErrors
{
	public:
		bool PrevIsEdge;
		bool NextIsEdge;
		double LowerBound;
		double UpperBound;
		std::vector< cv::Point > Location;
		std::vector< cv::Point > OrigLocation;
		std::vector< int > BlobIndex;
		std::vector< double > Error;
		
		GuardErrors(): PrevIsEdge(false), NextIsEdge(false), LowerBound(0.0), UpperBound(10000.0)
		{ Location.clear(); OrigLocation.clear(); BlobIndex.clear(); Error.clear(); }
		~GuardErrors(){}
		
		GuardErrors& operator= ( const GuardErrors &Source )
		{
			if ( this !=&Source )
			{
				PrevIsEdge = Source.PrevIsEdge;
				NextIsEdge = Source.NextIsEdge;
				LowerBound = Source.LowerBound;
				UpperBound = Source.UpperBound;
				Location = Source.Location;	
				OrigLocation = Source.Location;
				BlobIndex = Source.BlobIndex;
				Error = Source.Error;
			}
			return *this;
		}
		
		GuardErrors(const GuardErrors &Source)
		{
				PrevIsEdge = Source.PrevIsEdge;
				NextIsEdge = Source.NextIsEdge;
				LowerBound = Source.LowerBound;
				UpperBound = Source.UpperBound;
				OrigLocation = Source.Location;
				BlobIndex = Source.BlobIndex;
				Location = Source.Location;
		}
		
		void Clear()
		{
			Location.clear();
			OrigLocation.clear();
			BlobIndex.clear();
			Error.clear();
		}
		
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
// Declaration of procedure which returns the total number of errors in vector of < GuardErrors >.  It is defined in
// registration.cpp but is not part of RegistrationFit class.
//
int NumbErrorsInList(std::vector< GuardErrors > &List );

void SetupGuardErrorLists( std::vector< GuardInfo > &XGuards, std::vector< GuardInfo > &YGuards, cv::Size ImageSize,
	std::vector< GuardErrors > &XGuardErrorList, std::vector< GuardErrors > &YGuardErrorList, std::vector< GuardErrors > &OffImageList );
	
	
#endif // GUARDERRORS_HPP
