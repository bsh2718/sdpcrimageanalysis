// GetListIndex (cv::Point Shift) returns an index to which direction Shift is referring to.
// This is used to count the number of directions in which a node has connections.
// 
// SortConnections is used to sort the connections to find which connection is the closest.
//! [includes]
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//#include <iostream>
//#include <string>
//#include <cstdlib>
//! [includes]

#include "statistics.hpp"
#include "adjacent.hpp"

int GetListIndex(cv::Point Shift)
{
	double tmpX = 2.41 * fabs((double)Shift.x);
	double tmpY = 2.41 * fabs((double)Shift.y);
	if (Shift.y >= 0)
	{
		if (Shift.y > tmpX)
			return 1;
		else if (abs(Shift.x) > tmpY)
		{
			if (Shift.x > 0)
				return 4;
			else
				return 3;
		}
		else if (Shift.x > 0)
			return 2;
		else
			return 0;
	}
	else
	{
		if (abs(Shift.y) > tmpX)
			return 6;
		else if (abs(Shift.x) > tmpY)
		{
			if (Shift.x > 0)
				return 4;
			else
				return 3;
		}
		else if (Shift.x > 0)
			return 7;
		else
			return 5;
	}	
}

//bool GetShiftDirection(int Indice, cv::Point& ShiftDirection)
//{
//	if (Indice < 0 || Indice > 8)
//		return false;
//	switch (Indice)
//	{
//	case 0:
//		ShiftDirection = cv::Point(-1, 1);
//		break;
//	case 1:
//		ShiftDirection = cv::Point(0, 1);
//		break;
//	case 2:
//		ShiftDirection = cv::Point(1, 1);
//		break;
//	case 3:
//		ShiftDirection = cv::Point(-1, 0);
//		break;
//	case 4:
//		ShiftDirection = cv::Point(1, 0);
//		break;
//	case 5:
//		ShiftDirection = cv::Point(-1, -1);
//		break;
//	case 6:
//		ShiftDirection = cv::Point(0, -1);
//		break;
//	case 7:
//		ShiftDirection = cv::Point(1, -1);
//		break;
//	}
//	return true;
//}

bool SortConnections(const NodeConnection& A, const NodeConnection& B)
{
	double a2 = pow(A.Shift.x, 2) + pow(A.Shift.y, 2);
	double b2 = pow(B.Shift.x, 2) + pow(B.Shift.y, 2);
	if (a2 != b2)
		return a2 < b2;
	else if (abs(A.Shift.y) == abs(B.Shift.y) )
		return abs(A.Shift.x) < abs(B.Shift.x);
	else
		return abs(A.Shift.y) < abs(B.Shift.y);
}
