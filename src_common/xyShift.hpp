#ifndef XYSHIFT_HPP
#define XYSHIFT_HPP
//! [includes]

#include <cstdlib>
#include <vector>
#include <algorithm>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "blob.hpp"
#include "readinfofile.hpp"
// #include "searchArea.hpp"

void XYShift( std::ofstream &LogFile, double ScaleIn, int Step,
		std::vector< Blob > &BlobList, std::vector< std::vector< int > > &ArrSecMatrix,
		cv::Mat &ArrSecImage, std::vector< ParameterLimit > PLimits, 
		std::vector< double > &Results, bool TrackAlignment, std::string &ErrMsg );

#endif // XYSHIFT_HPP


	
