#ifndef STARTASSIGNMENT_HPP
#define STARTASSIGNMENT_HPP
//! [includes]

#include <cstdlib>
#include <vector>
#include <algorithm>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "blob.hpp"
#include "adjacent.hpp"
#include "trimRectangle.hpp"

class AssignBlobs
{
public:
	int NumbArrays;
	int NumbSections;
	int NumbRows;
	int NumbCols;
	int SectionOfInterest;
	float SectionGap;
	std::vector< float > ArrayYScale;
	std::vector< std::pair< cv::Point2f, cv::Point2f> > AdjustedSectionOrigins;
	cv::Point Tolerance;
	cv::Point2f WellSpacing2f;
	cv::Point WellSpacingI;
	cv::Point2f WellSize2f;
	double Dist2Target;
	cv::Size SearchImageSize;
	cv::Mat SearchImage;
	std::vector< Blob > SortedBlobList;
	std::vector < bool > ArrayUsed;
	int NumbOffsets;
	std::vector < std::pair< int, int > > HorzOffsetLimits;
	std::vector < std::pair< int, int > > VertOffsetLimits;
	
	std::vector< std::vector< BlobNode > > BlobNodeList;
	std::vector< BlobNode > UnalignedBlobNodeList;
	std::vector< cv::Point > CornerList;
	std::vector< std::pair< cv::Point, cv::Point> > MinMaxList;
	std::vector< cv::Point > ArraySectionShift;
	std::vector< int > NumbInArraySection;
	std::vector< int > InitNodes;

	bool Track;

	AssignBlobs(): NumbArrays(0), NumbSections(0), NumbRows(0), NumbCols(0), SectionOfInterest(1), SectionGap(0.0),
		Tolerance(cv::Point(0,0)), WellSpacing2f(cv::Point2f(0.0,0.0)), WellSpacingI(cv::Point(0,0)), 
		WellSize2f(cv::Point2f(1.0,1.0)), Dist2Target(2.0), SearchImageSize(cv::Size(10,10)),
		NumbOffsets(1), Track(false)
	{
		ArrayYScale.clear();
		AdjustedSectionOrigins.clear();
		SortedBlobList.clear();
		ArrayUsed.clear();
		HorzOffsetLimits.clear();
		VertOffsetLimits.clear();
		BlobNodeList.clear();
		UnalignedBlobNodeList.clear();
		CornerList.clear();
		MinMaxList.clear();
		ArraySectionShift.clear();
		NumbInArraySection.clear();
		InitNodes.clear();
	}
	~AssignBlobs() {}

	AssignBlobs& operator= (const AssignBlobs& Source)
	{
		if (this != &Source)
		{
			NumbArrays = Source.NumbArrays;
			NumbSections = Source.NumbSections;
			NumbRows = Source.NumbRows;
			NumbCols = Source.NumbCols;
			SectionOfInterest = Source.SectionOfInterest;
			SectionGap = Source.SectionGap;
			ArrayYScale = Source.ArrayYScale;
			AdjustedSectionOrigins = Source.AdjustedSectionOrigins;
			Tolerance = Source.Tolerance;
			WellSpacing2f = Source.WellSpacing2f;
			WellSpacingI = Source.WellSpacingI;
			WellSize2f = Source.WellSize2f;
			Dist2Target = Source.Dist2Target;
			SearchImageSize = Source.SearchImageSize;
			Source.SearchImage.copyTo(SearchImage);
			SortedBlobList = Source.SortedBlobList;
			ArrayUsed = Source.ArrayUsed;
			NumbOffsets = Source.NumbOffsets;
			HorzOffsetLimits = Source.HorzOffsetLimits;
			VertOffsetLimits = Source.VertOffsetLimits;
			BlobNodeList = Source.BlobNodeList;
			CornerList = Source.CornerList;
			MinMaxList = Source.MinMaxList;
			UnalignedBlobNodeList = Source.UnalignedBlobNodeList;
			ArraySectionShift = Source.ArraySectionShift;
			NumbInArraySection = Source.NumbInArraySection;
			InitNodes = Source.InitNodes;
			Track = Source.Track;
		}
		return *this;
	}

	AssignBlobs(const AssignBlobs& Source) : NumbArrays(Source.NumbArrays), NumbSections(Source.NumbSections),
		NumbRows(Source.NumbRows), NumbCols(Source.NumbCols), SectionOfInterest(Source.SectionOfInterest), SectionGap(Source.SectionGap),
		Tolerance(Source.Tolerance), WellSpacing2f(Source.WellSpacing2f), WellSpacingI(Source.WellSpacingI), 
		WellSize2f(Source.WellSize2f), Dist2Target(Source.Dist2Target), 
		SearchImageSize(Source.SearchImageSize), NumbOffsets(Source.NumbOffsets),
		BlobNodeList(Source.BlobNodeList), UnalignedBlobNodeList(Source.UnalignedBlobNodeList), 
		CornerList(Source.CornerList), MinMaxList(Source.MinMaxList), ArraySectionShift(Source.ArraySectionShift)
	{
		Source.SearchImage.copyTo(SearchImage);
		ArrayYScale = Source.ArrayYScale;
		AdjustedSectionOrigins = Source.AdjustedSectionOrigins;
		SortedBlobList = Source.SortedBlobList;
		ArrayUsed = Source.ArrayUsed;
		HorzOffsetLimits = Source.HorzOffsetLimits;
		VertOffsetLimits = Source.VertOffsetLimits;
		NumbInArraySection = Source.NumbInArraySection;
		InitNodes = Source.InitNodes;
	}

	void Clear()
	{
		ArrayYScale.clear();
		AdjustedSectionOrigins.clear();
		SortedBlobList.clear();
		ArrayUsed.clear();
		HorzOffsetLimits.clear();
		VertOffsetLimits.clear();
		BlobNodeList.clear();
		UnalignedBlobNodeList.clear();
		CornerList.clear();
		MinMaxList.clear();
		ArraySectionShift.clear();
		NumbInArraySection.clear();
		InitNodes.clear();
		Track = false;
	}

	int ArraySectionNumber(int Array, int Section);

	bool ArraySectionFromNumber(int ArraySectionNumber, int& Array, int& Section);

	bool Initialize(std::ofstream& LogFile, std::string BaseName, std::vector< double > Results,
		std::vector< Blob >& BlobList, std::vector< std::vector< int > > ArrSecMatrix,
		cv::Size ImageSize, cv::Point2f WellSpacing, cv::Size2f WellSize,
		std::vector< DeviceOrigins >& ArrayOrigins,
		std::vector< DeviceOrigins >& SectionOrigins,
		int NumbArrays, int NumbSections, int NumbCols, int NumbRows, double Scale, double ScaleLimit,
		bool Track, std::string& ErrMsg);

	//bool ConnectNodesSOI(std::ofstream& LogFile, std::string BaseName, int Array);

	bool ConnectNodes(std::ofstream& LogFile, std::string BaseName, int Array, int Section, bool Track = false);

	bool EvaluateNodesSOI(std::ofstream& LogFile, std::string BaseName, int Array);

	bool DetermineArraySectionShiftSOI(std::ofstream& LogFile, std::string BaseName, int Array);

	bool EvaluateNodes(std::ofstream& LogFile, std::string BaseName, int Array, int Section);

	bool DetermineArraySectionShift(std::ofstream& LogFile, std::string BaseName, int Array, int Section);

	std::string ArraySectionShiftsToString();

	//bool AssignRowColumn(std::ofstream& LogFile, std::string BaseName, int Array, int Section);

	void UpdateBlobList(std::vector< Blob >& BlobList);

	void AssignOtherBlobs(std::ofstream& LogFile, std::vector< Blob >& OtherBlobList, std::vector< double > Results, double Scale);

	bool EstimateScale(std::ofstream& LogFile, int SectionOfInterest, double& AveScale, double& XScale, double& YScale);

	int FindNextColumn(std::vector< std::vector< int > >& ArraySectionMatrix, int Row, int CurrColumn);

	int FindNextRow(std::vector< std::vector< int > >& ArraySectionMatrix, int CurrentRow, int Column);
	
	int GetSectionOfInterest();

	bool SetSectionOfInterest(int NewSOI);

	bool GetCorner11(int Array, int Section, cv::Point& Corner11);

	int GetNumberOfNodes(int Array, int Section);

};

struct AlignData
{
	int Index;
	int Length;
	int Count;
	Statistics1 Stats;
	double Spacing;
	bool SpacingOK;
};

//namespace AssignConst
//{
//
//	static int Pass0Count = 48;
//	static std::vector<cv::Point> Pass0
//	{
//		cv::Point(0,-1), cv::Point(-1, 0), cv::Point( 1, 0), cv::Point( 0, 1),
//		cv::Point(1, 1), cv::Point(-1, 1), cv::Point( 1,-1), cv::Point(-1,-1),
//		cv::Point(2, 0), cv::Point( 0,-2), cv::Point(-2, 0), cv::Point( 0, 2),
//		cv::Point(1,-2), cv::Point(-2,-1), cv::Point(-1, 2), cv::Point( 1, 2),
//		cv::Point(2, 1), cv::Point( 2,-1), cv::Point(-1,-2), cv::Point(-2, 1),
//		cv::Point(2, 2), cv::Point( 2,-2), cv::Point(-2,-2), cv::Point(-2, 2),
//		cv::Point(0, 3), cv::Point( 0,-3), cv::Point( 3, 0), cv::Point(-3, 0),
//		cv::Point(1, 3), cv::Point( 1,-3), cv::Point(-1, 3), cv::Point(-1,-3),
//		cv::Point(3, 1), cv::Point( 3,-1), cv::Point(-3,-1), cv::Point(-3, 1),
//		cv::Point(2, 3), cv::Point( 2,-3), cv::Point(-2, 3), cv::Point(-2,-3),
//		cv::Point(3, 2), cv::Point( 3,-2), cv::Point(-3, 2), cv::Point(-3,-2),
//		cv::Point(0, 4), cv::Point( 0,-4), cv::Point( 4, 0), cv::Point(-4, 0)
//	};
//
//	static int Pass1Count = 28;
//	static std::vector<cv::Point> Pass1
//	{ 
//		cv::Point(2, 2), cv::Point( 2,-2), cv::Point(-2,-2), cv::Point(-2, 2),
//		cv::Point(2, 1), cv::Point( 2,-1), cv::Point(-1,-2), cv::Point(-2, 1),
//		cv::Point(1,-2), cv::Point(-2,-1), cv::Point(-1, 2), cv::Point( 1, 2),
//		cv::Point(2, 0), cv::Point( 0,-2), cv::Point(-2, 0), cv::Point( 0, 2),
//		cv::Point(1, 1), cv::Point(-1, 1), cv::Point( 1,-1), cv::Point(-1,-1),
//		cv::Point(0,-1), cv::Point(-1, 0), cv::Point( 1, 0), cv::Point( 0, 1),
//		cv::Point(0, 3), cv::Point( 0,-3), cv::Point( 3, 0), cv::Point(-3, 0)
//  
//	};
//
//	static int Pass2Count = 32;
//	static std::vector<cv::Point> Pass2
//	{
//		cv::Point( 1, 3), cv::Point( 1,-3), cv::Point(-1, 3), cv::Point(-1,-3),
//		cv::Point( 3, 1), cv::Point( 3,-1), cv::Point(-3,-1), cv::Point(-3, 1),
//		cv::Point( 2, 3), cv::Point( 2,-3), cv::Point(-2, 3), cv::Point(-2,-3),
//		cv::Point( 3, 2), cv::Point( 3,-2), cv::Point(-3, 2), cv::Point(-3,-2),
//		cv::Point( 0, 4), cv::Point( 0,-4), cv::Point( 4, 0), cv::Point(-4, 0),
//		cv::Point( 3, 3), cv::Point( 3,-3), cv::Point(-3,-3), cv::Point(-3, 3),
//		cv::Point( 1, 4), cv::Point( 1,-4), cv::Point(-1, 4), cv::Point(-1,-4),
//		cv::Point( 4, 1), cv::Point( 4,-1), cv::Point(-4,-1), cv::Point(-4, 1)
//	};
//
//	static int Pass3Count = 24;
//	static std::vector<cv::Point> Pass3
//	{
//		cv::Point(0,-1), cv::Point(-1, 0), cv::Point( 1, 0), cv::Point( 0, 1),
//		cv::Point(1, 1), cv::Point(-1, 1), cv::Point( 1,-1), cv::Point(-1,-1),
//		cv::Point(2, 0), cv::Point( 0,-2), cv::Point(-2, 0), cv::Point( 0, 2),
//		cv::Point(1,-2), cv::Point(-2,-1), cv::Point(-1, 2), cv::Point( 1, 2),
//		cv::Point(2, 1), cv::Point( 2,-1), cv::Point(-1,-2), cv::Point(-2, 1),
//		cv::Point(2, 2), cv::Point( 2,-2), cv::Point(-2,-2), cv::Point(-2, 2)
//	};
//}
#endif // STARTASSIGNMENT_HPP


	
