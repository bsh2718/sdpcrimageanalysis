#ifndef CALCCONCENTRATION_HPP
#define CALCCONCENTRATION_HPP

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>

bool CalcConcentration( std::ofstream &LogFile, std::vector<Droplet> &DropletList, double VolumeError, 
	double &Mu, double &Sigma, bool &LowerBound, bool &UpperBound,
	bool Debug );
	
#endif  // CALCCONCENTRATION_HPP
