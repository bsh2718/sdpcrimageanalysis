#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "blob.hpp"
#include "blob_CD.hpp"
#include "createBlobCDLists.hpp"

int createBlobCDLists(
	std::vector< Blob >& BlobList, std::vector< Blob >& OtherBlobList, std::vector< Blob >& BadBlobList,
	cv::Size ImageSize, cv::Size SectionImageSize, double DistToCenter, double ThetaResolution,
	std::vector< Blob_CD >& Blob_CDList, std::vector< Blob_CD >& OtherBlob_CDList, std::vector< Blob_CD >& BadBlob_CDList)
{

	double imageMiddle = (double)(ImageSize.width) * 0.5;
	Blob_CDList.resize(BlobList.size());
	OtherBlob_CDList.resize(OtherBlobList.size());
	BadBlob_CDList.resize(BadBlobList.size());
	int numbInSubImage = 0;
	for (int n = 0; n < BlobList.size(); n++)
	{
		Blob_CDList[n] = Blob_CD(BlobList[n], n);
		if (Blob_CDList[n].CalculateInputThetaR(DistToCenter, imageMiddle, ThetaResolution, SectionImageSize))
			numbInSubImage++;
	}
	for (int n = 0; n < OtherBlobList.size(); n++)
	{
		OtherBlob_CDList[n] = Blob_CD(OtherBlobList[n], n);
		if (OtherBlob_CDList[n].CalculateInputThetaR(DistToCenter, imageMiddle, ThetaResolution, SectionImageSize))
			numbInSubImage++;
	}
	for (int n = 0; n < BadBlobList.size(); n++)
	{
		BadBlob_CDList[n] = Blob_CD(BadBlobList[n], n);
		if (BadBlob_CDList[n].CalculateInputThetaR(DistToCenter, imageMiddle, ThetaResolution, SectionImageSize))
			numbInSubImage++;
	}
	return numbInSubImage;
}