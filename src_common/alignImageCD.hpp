#ifndef ALIGNIMAGE_HPP
#define ALIGNIMAGE_HPP
//! [includes]

#include <cstdlib>
#include <vector>
#include <algorithm>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "blob.hpp"
#include "blob_CD.hpp"
#include "deviceOrigins.hpp"
#include "readinfofile.hpp"
//#include "searchArea.hpp"

int AlignImageCD(std::ofstream &LogFile, std::string &BaseName, 
	std::vector< Blob > &BlobList, std::vector< Blob > &OtherBlobList, std::vector< Blob >& BadBlobList,
	std::vector< Blob_CD >& Blob_CDList, std::vector< Blob_CD >& OtherBlob_CDList, std::vector< Blob_CD > BadBlob_CDList,
	cv::Mat &SectionImage, double DistToCenter, double ThetaResolution, std::vector< cv::Point > SubSectionBands,
	std::vector< ParameterLimit > PLimits,
	cv::Size ImageSize, std::vector<cv::Point2f> WellSpacing, cv::Size2f WellSize, int SectionOfInterest,
	std::vector< DeviceOrigins > &SectionOrigins,
	std::vector< DeviceOrigins > &SubSectionOrigins, std::vector< cv::Size > SubSectionDimens,
	std::vector< double > &Results, bool TrackAlignment, std::string &ErrMsg );

namespace AlignConst
{
	static int Pass1Count = 28;
	static std::vector<cv::Point> Pass1
	{ 
		cv::Point(2, 2), cv::Point( 2,-2), cv::Point(-2,-2), cv::Point(-2, 2),
		cv::Point(2, 1), cv::Point( 2,-1), cv::Point(-1,-2), cv::Point(-2, 1),
		cv::Point(1,-2), cv::Point(-2,-1), cv::Point(-1, 2), cv::Point( 1, 2),
		cv::Point(2, 0), cv::Point( 0,-2), cv::Point(-2, 0), cv::Point( 0, 2),
		cv::Point(1, 1), cv::Point(-1, 1), cv::Point( 1,-1), cv::Point(-1,-1),
		cv::Point(0,-1), cv::Point(-1, 0), cv::Point( 1, 0), cv::Point( 0, 1),
		cv::Point(0, 3), cv::Point( 0,-3), cv::Point( 3, 0), cv::Point(-3, 0)
  
	};

	static int Pass2Count = 32;
	static std::vector<cv::Point> Pass2
	{
		cv::Point(1, 3), cv::Point(1,-3), cv::Point(-1, 3), cv::Point(-1,-3),
		cv::Point(3, 1), cv::Point(3,-1), cv::Point(-3,-1), cv::Point(-3, 1),
		cv::Point(2, 3), cv::Point(2,-3), cv::Point(-2, 3), cv::Point(-2,-3),
		cv::Point(3, 2), cv::Point(3,-2), cv::Point(-3, 2), cv::Point(-3,-2),
		cv::Point(0, 4), cv::Point(0,-4), cv::Point( 4, 0), cv::Point(-4, 0),
		cv::Point(3, 3), cv::Point(3,-3), cv::Point(-3,-3), cv::Point(-3, 3),
		cv::Point(1, 4), cv::Point(1,-4), cv::Point(-1, 4), cv::Point(-1,-4),
		cv::Point(4, 1), cv::Point(4,-1), cv::Point(-4,-1), cv::Point(-4, 1)
	};
}
#endif // ALIGNIMAGE_HPP


	
