

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//#include <iostream>
//#include <string>
//#include <cstdlib>
//! [includes]

#include <vector>
#include "blob.hpp"
#include "blobSortMethods.hpp"
// Sort Blobs small to large
// If the blobs have both been assigned
//   Sort by assigned column.
//     If blobs have the same assigned column, then
//       If the blobs are in the same array and section, sort by row
//       If the blobs are in different arrays sort by array
//       If the blobs are in the same arrays, but different sections, sort by section
// If only one blob is assigned
//    Sort assigned blob ahead of unassigned blob
// If both blobs are unassigned, then sort by AdjustedCenter2f.x
//  
bool SortBlobsByColumnSmall(Blob A, Blob B)
{
	if ( A.SectionPosition.x > 0 && B.SectionPosition.x > 0 )
	{
		if ( A.SectionPosition.x == B.SectionPosition.x )
		{
			if ( A.Section == B.Section && A.Array == B.Array )
				return A.SectionPosition.y < B.SectionPosition.y ;
			else if ( A.Section == B.Section )
				return A.Array < B.Array;
			else
				return A.Section < B.Section;
		}
		else
			return A.SectionPosition.x < B.SectionPosition.x;
	}
	else if ( A.SectionPosition.x > 0 )
		return true;
	else if ( B.SectionPosition.x > 0 )
		return false;
	else
		return A.AdjustedCenter2f.x < B.AdjustedCenter2f.x;
}

// Sort by SelfIndex small to large
bool SortBySelfIndex( Blob A, Blob B )
{
	return A.SelfIndex < B.SelfIndex;
}

namespace SortBlobMethods
{
	bool ByCols(Blob A, Blob B)
	{
		if (A.Array != 0 && B.Array != 0)
		{
			if (A.Array != B.Array)
				return A.Array < B.Array;
			else if (A.Section != B.Section)
				return A.Section < B.Section;
			else if (A.SectionPosition.x != B.SectionPosition.x)
				return A.SectionPosition.x < B.SectionPosition.x;
			else
				return A.SectionPosition.y < B.SectionPosition.y;
		}
		else if (A.Array == 0 && B.Array != 0)
			return false;
		else if (A.Array != 0 && B.Array == 0)
			return true;
		else
		{
			if (fabs(A.AdjustedCenter2f.x - B.AdjustedCenter2f.x) > ByColsSortDelta)
				return A.AdjustedCenter2f.x < B.AdjustedCenter2f.x;
			else
				return A.AdjustedCenter2f.y < B.AdjustedCenter2f.y;
		}
	}

	bool ByArraySection(Blob A, Blob B)
	{
		if (A.Array != 0 && B.Array != 0)
		{
			if (A.Array != B.Array)
				return A.Array < B.Array;
			else if (A.Section != B.Section)
				return A.Section < B.Section;
			else if (fabs(A.AdjustedCenter2f.y - B.AdjustedCenter2f.y) > ByArraySectionDelta)
				return A.AdjustedCenter2f.y < B.AdjustedCenter2f.y;
			else
				return A.AdjustedCenter2f.x < B.AdjustedCenter2f.x;
		}
		else if (A.Array == 0 && B.Array != 0)
			return false;
		else if (A.Array != 0 && B.Array == 0)
			return true;
		else
		{
			if (fabs(A.AdjustedCenter2f.y - B.AdjustedCenter2f.y) > ByArraySectionDelta)
				return A.AdjustedCenter2f.y < B.AdjustedCenter2f.y;
			else
				return A.AdjustedCenter2f.x < B.AdjustedCenter2f.x;
		}
	}
}