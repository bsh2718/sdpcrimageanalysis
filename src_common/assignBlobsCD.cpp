#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "statistics.hpp"
#include "blob.hpp"
#include "blob_CD.hpp"
//#include "blobSortMethods.hpp"
#include "readinfofile.hpp"
#include "writeBlobFile.hpp"
#include "xyShiftCD.hpp"
#include "assignBlobsCD.hpp"
#include "xyShiftCD.hpp"
#include "trimRectangle.hpp"
#include "alignImageCD.hpp"
#include "adjacentCD.hpp"

int AssignBlobsCD::SectionSubSectionNumber(int Section, int SubSection)
{
	return (Section - 1) * NumbSubSections + (SubSection - 1);
}

bool AssignBlobsCD::SectionSubSectionFromNumber(int SecSubNumber, int &Section, int &SubSection)
{
	int sub = SecSubNumber % NumbSubSections;
	int sec = (SecSubNumber - sub) /NumbSubSections;
	sub++;
	sec++;
	if (sec > 0 && sec <= NumbSections && sub > 0 && sub <= NumbSubSections)
	{
		Section = sec;
		SubSection = sub;
		return true;
	}
	else
		return false;
}

double SortNodeDelta = 10.0;
cv::Point2f SortCorner = cv::Point2f(0.0, 0.0);

//bool SortBlobNodePositions(const Blob_CDNode& A, const Blob_CDNode& B)
//{
//	double tmpA = sqrt(pow(A.InputTheta_R2f.x - SortCorner.x, 2) + pow(A.InputTheta_R2f.y - SortCorner.y, 2));
//	double tmpB = sqrt(pow(B.InputTheta_R2f.x - SortCorner.x, 2) + pow(B.InputTheta_R2f.y - SortCorner.y, 2));
//	if (fabs(tmpB - tmpA) < SortNodeDelta)
//		return A.InputTheta_R2f.x < B.InputTheta_R2f.x;
//	else
//		return tmpA < tmpB;
//}
bool SortBlobNodePositions(const Blob_CDNode& A, const Blob_CDNode& B)
{
	double tmpA = sqrt(pow(A.Theta_R2f.x - SortCorner.x, 2) + pow(A.Theta_R2f.y - SortCorner.y, 2));
	double tmpB = sqrt(pow(B.Theta_R2f.x - SortCorner.x, 2) + pow(B.Theta_R2f.y - SortCorner.y, 2));
	if (fabs(tmpB - tmpA) < SortNodeDelta)
		return A.Theta_R2f.x < B.Theta_R2f.x;
	else
		return tmpA < tmpB;
}
bool SortBlobIndex(const Blob_CDNode& A, const Blob_CDNode& B)
{
	return A.SelfIndex < B.SelfIndex;
}

int TripleDelta = 5;
bool SortTriples(const std::vector< int >& A, const std::vector< int >& B)
{
	if (A[1] != B[1])
		return A[1] < B[1];
	else if (abs(A[0] - B[0]) < TripleDelta)
		return A[0] < B[0];
	else
		return A[2] > B[2];
}

bool AssignBlobsCD::Initialize(std::ofstream& LogFile, std::string BaseName, std::vector< double > Results,
		std::vector< Blob_CD >& InputBlob_CDList, int SecOfInterest, std::vector< cv::Point > SubSecRowsCols,
		cv::Mat &SectionImageIn, std::vector<cv::Point2f> WellSpacing, cv::Size2f WellSize,
		double DistToCenter, double ThetaResolu,
		std::vector< DeviceOrigins >& SectionOrigins,
		std::vector< DeviceOrigins >& SubSectionOrigins,
		double Scale, double ScaleLimit,
		bool TrackAssignment, std::string& ErrMsg)
{
	std::ostringstream oString;
	if (SubSecRowsCols.size() - 1 != SubSectionOrigins.size())
	{
		oString.str("");
		oString.clear();
		oString << "SubSectionRowsCols.size() - 1  != SubSectionOrigins.size(), SubSectionRowsCols.size() = "
			<< SubSecRowsCols.size() << ", SubSectionOrigins.size() = " << SubSectionOrigins.size();
		ErrMsg = oString.str();
		LogFile << ErrMsg << "\n";
		return false;
	}
	SectionImage = SectionImageIn;
	SectionImageSize = SectionImage.size();
	SearchImage.create(SectionImageSize, CV_16UC1);
	SearchImageSize = SearchImage.size();
	DistanceToCenter = DistToCenter;
	ThetaResolution = ThetaResolu;
	SectionOfInterest = SecOfInterest;
	SubSectionRowsCols = SubSecRowsCols;
	NumbSections = (int)SectionOrigins.size();
	NumbSubSections = (int)SubSectionOrigins.size();
	std::string outName;
	Blob_CDList = InputBlob_CDList;
	WellTR_Spacing2f.resize(SubSectionOrigins.size() + 1);
	WellTR_SpacingI.resize(SubSectionOrigins.size() + 1);
	WellTR_Size2f.resize(SubSectionOrigins.size() + 1);
	Track = TrackAssignment;
	for (int n = 0; n < SubSectionOrigins.size(); n++)
	{
		int idx = SubSectionOrigins[n].Index;
		double rStart = DistanceToCenter + SubSectionOrigins[n].Origin.y;
		WellTR_Spacing2f[idx].x = WellSpacing[n].x;
		WellTR_Spacing2f[idx].y = WellSpacing[n].y;
		WellTR_SpacingI[idx].x = (int)round(WellSpacing[n].x);
		WellTR_SpacingI[idx].y = (int)round(WellSpacing[n].y);
		WellTR_Size2f[idx].width = (float)(atan((WellSize.width * 0.5) / rStart) * 2.0 / ThetaResolution);
		WellTR_Size2f[idx].height = WellSize.height;
	}
	
	LogFile << "SectionOfInterest: " << SectionOfInterest << std::endl;
	for (int n = 0; n < SubSectionOrigins.size(); n++)
	{
		int idx = SubSectionOrigins[n].Index;
		LogFile << "WellTR_SpacingI[" << idx << "].x/y: " << WellTR_SpacingI[idx].x << "/" << WellTR_SpacingI[idx].y << "\n";
		LogFile << "WellTR_Size2f[" << idx << "].x/y: " << WellTR_Size2f[idx].width << "/" << WellTR_Size2f[idx].height << "\n";
	}
	
	SubSectionScale.resize((size_t)NumbSubSections + 1);
	std::fill(SubSectionScale.begin(), SubSectionScale.end(), 0.0f);

	SubSectionShift.resize((size_t)SectionSubSectionNumber(NumbSections, NumbSubSections) + 1);
	std::fill(SubSectionShift.begin(), SubSectionShift.end(), cv::Point(0, 0));
	NumbInSubSection.resize((size_t)SectionSubSectionNumber(NumbSections, NumbSubSections) + 1);
	std::fill(NumbInSubSection.begin(), NumbInSubSection.end(), 0);
	CornerList.resize((size_t)SectionSubSectionNumber(NumbSections, NumbSubSections) + 1);
	InitNodes.resize((size_t)SectionSubSectionNumber(NumbSections, NumbSubSections) + 1);
	SearchBands.resize((size_t)SectionSubSectionNumber(NumbSections, NumbSubSections) + 1);
	LogFile << "NumbSections/NumbSubSections: " << NumbSections << "/" << NumbSubSections << "\n";
	for (int a = 0; a < NumbSections; a++)
	{
		int sec = a + 1;
		for (int s = 0; s < NumbSubSections; s++)
		{
			int sub = s + 1;
			int as = SectionSubSectionNumber(sec, sub);
			CornerList[as].x = (int)round(SectionOrigins[a].Origin.x);
			CornerList[as].y = (int)round(SubSectionOrigins[s].Origin.y);
			LogFile << "Section/SubSection: " << sec << "/" << sub << ", as/Origin: " << as << "/" << CornerList[as] << "\n";
		}
	}
	{
		int midX = SectionImageSize.width / 2;
		int sec = 1;
		int sub = 1;
		int as = SectionSubSectionNumber(sec, sub);
		SearchBands[as].LowerRight.x = midX - (int)round(3.0 * WellSpacing[sub].x);
		SearchBands[as].UpperLeft.x = SearchBands[as].LowerRight.x - (int)round(WellSpacing[sub].x * SubSectionRowsCols[sub].x);
		SearchBands[as].LowerRight.y = (int)round(SubSectionOrigins[sub].Origin.y + WellSpacing[sub].y * (SubSectionRowsCols[sub].y - 2));
		SearchBands[as].UpperLeft.y = SearchBands[as].LowerRight.y - (int)round(WellSpacing[sub].y * SubSectionRowsCols[sub].y);

		sec = 1;
		sub = 2;
		as = SectionSubSectionNumber(sec, sub);
		SearchBands[as].LowerRight.x = midX - (int)round(3.0 * WellSpacing[sub].x);
		SearchBands[as].UpperLeft.x = SearchBands[as].LowerRight.x - (int)round(WellSpacing[sub].x * SubSectionRowsCols[sub].x);
		SearchBands[as].UpperLeft.y = (int)round(SubSectionOrigins[sub].Origin.y + WellSpacing[sub].y * 2);
		SearchBands[as].LowerRight.y = SearchBands[as].UpperLeft.y + (int)round(WellSpacing[sub].y * SubSectionRowsCols[sub].y);

		sec = 2;
		sub = 1;
		as = SectionSubSectionNumber(sec, sub);
		SearchBands[as].UpperLeft.x = midX + (int)round(3.0 * WellSpacing[sub].x);
		SearchBands[as].LowerRight.x = SearchBands[as].UpperLeft.x + (int)round(WellSpacing[sub].x * SubSectionRowsCols[sub].x);
		SearchBands[as].LowerRight.y = (int)round(SubSectionOrigins[sub].Origin.y + WellSpacing[sub].y * (SubSectionRowsCols[sub].y - 2));
		SearchBands[as].UpperLeft.y = SearchBands[as].LowerRight.y - (int)round(WellSpacing[sub].y * SubSectionRowsCols[sub].y);

		sec = 2;
		sub = 2;
		as = SectionSubSectionNumber(sec, sub);
		SearchBands[as].UpperLeft.x = midX + (int)round(3.0 * WellSpacing[sub].x);
		SearchBands[as].LowerRight.x = SearchBands[as].UpperLeft.x + (int)round(WellSpacing[sub].x * SubSectionRowsCols[sub].x);
		SearchBands[as].UpperLeft.y = (int)round(SubSectionOrigins[sub].Origin.y + WellSpacing[sub].y * 2);
		SearchBands[as].LowerRight.y = SearchBands[as].UpperLeft.y + (int)round(WellSpacing[sub].y * SubSectionRowsCols[sub].y);

	}
	BlobNodeList.resize((size_t)SectionSubSectionNumber(NumbSections, NumbSubSections) + 1);
	for (int n = 0; n < BlobNodeList.size(); n++)
		BlobNodeList[n].clear();
	UnalignedBlobNodeList.clear();
	std::vector< Blob_CDNode > tempBlobNodeList;
	tempBlobNodeList.clear();
	Blob_CDNode newBlobNode;
	newBlobNode.Clear();
	SortNodeDelta = WellSpacing[0].y * 4.0;
	
	std::ofstream outFile;
	std::string header;
	LogFile << "Results[0]/[1]/Scale: " << Results[0] << " / " << Results[1] << " / " << Scale << std::endl;
	//int subSec;
	int numbAssigned = 0;
	int numbUnassigned = 0;
	for (int n = 0; n < Blob_CDList.size(); n++)
	{
		bool assigned = false;
		int sub = -1;
		int as = -1;
		if (Blob_CDList[n].Section == SectionOfInterest && Blob_CDList[n].SubSection > 0)
		{
			assigned = true;
			sub = Blob_CDList[n].SubSection;
			as = SectionSubSectionNumber(SectionOfInterest, sub);
		}
		else
		{
			for (int s = 1; s <= NumbSubSections; s++)
			{
				int m = SectionSubSectionNumber(SectionOfInterest, s);
				if (Blob_CDList[n].DeviceCenter.x > SearchBands[m].UpperLeft.x
					&& Blob_CDList[n].DeviceCenter.x < SearchBands[m].LowerRight.x
					&& Blob_CDList[n].DeviceCenter.y > SearchBands[m].UpperLeft.y
					&& Blob_CDList[n].DeviceCenter.y < SearchBands[m].LowerRight.y)
				{
					as = m;
					sub = s;
					assigned = true;
					break;
				}
			}
		}
		if (assigned)
			numbAssigned++;
		else
			numbUnassigned++;
		//LogFile << n << " / " << std::boolalpha << assigned << ",  " << as << " , " << SectionOfInterest << " / " << sub << "\n";
		newBlobNode = Blob_CDList[n];
		newBlobNode.MainListIndex = n;
		newBlobNode.Status = 0;
		if (assigned)
		{
			Blob_CDList[n].SubSection = sub;
			Blob_CDList[n].Section = SectionOfInterest;
			newBlobNode.SelfIndex = (int)BlobNodeList[as].size();
			newBlobNode.SubSection = sub;
			newBlobNode.Section = SectionOfInterest;
			BlobNodeList[as].push_back(newBlobNode);
		}
		else
		{
			Blob_CDList[n].Section = 0;
			Blob_CDList[n].SubSection = 0;
			newBlobNode.SelfIndex = (int)UnalignedBlobNodeList.size();
			newBlobNode.SubSection = 0;
			newBlobNode.Section = 0;
			UnalignedBlobNodeList.push_back(newBlobNode);
		}
	}
	LogFile << ">>>>>\nNumbAssigned = " << numbAssigned << " / NumbUnassigned = " << numbUnassigned << "\n<<<<<" << std::endl;
	for (int sub = 1; sub <= NumbSubSections ; sub++)
	{
		int as = SectionSubSectionNumber(SectionOfInterest, sub);
		SortCorner = CornerList[as];
		if (BlobNodeList[as].size() > 0)
		{
			std::sort(BlobNodeList[as].begin(), BlobNodeList[as].end(), SortBlobNodePositions);
			for (int b = 0; b < BlobNodeList[as].size(); b++)
				BlobNodeList[as][b].SelfIndex = b;
			if (Track)
			{
				oString.str("");
				oString.clear();
				oString << BaseName << "_" << SectionOfInterest << "_" << sub <<"_RawNodeList.csv";
				outName = oString.str();
				outFile.open(outName);
				std::string outString = BlobNodeList[as][0].ToString(header);
				outFile << "SearchBands.y/x,," << SearchBands[as].UpperLeft.y << "," << SearchBands[as].UpperLeft.x << ",,"
					<< SearchBands[as].LowerRight.y << "," << SearchBands[as].LowerRight.x << ",(Adjusted Centers)\n";
				outFile << "corner.y/x,," << (SearchBands[as].UpperLeft.y - Results[1]) / Scale << ","
					<< (SearchBands[as].UpperLeft.x - Results[0]) / Scale << ",,"
					<< (SearchBands[as].LowerRight.y - Results[1]) / Scale << "," << (SearchBands[as].LowerRight.x - Results[0]) / Scale
					<< ",(Input Centers)\n";
				outFile << header << outString;
				for (int s = 1; s < BlobNodeList[as].size(); s++)
					outFile << BlobNodeList[as][s].ToString(header);
				outFile.close();
				LogFile << "BlobNodeList - Section: " << SectionOfInterest << ", SubSection: " << sub << " .size() = "
					<< BlobNodeList[as].size() << "\n";
				if (SectionOfInterest != NumbSections)
				{
					for (int s = 1; s <= NumbSections; s++)
					{
						if (s != SectionOfInterest)
						{
							int as2 = SectionSubSectionNumber(s, sub);
							LogFile << "     BlobNodeList - Section: " << s << ", SubSection: " << sub << " .size() = "
								<< BlobNodeList[as2].size() << "\n";
						}
					}
				}
			}
		}
	}

	SortCorner = CornerList[0];
	
	//Tolerance.resize((size_t)NumbSubSections + 1); 
	HorzOffsetLimits.resize((size_t)NumbSubSections + 1);
	VertOffsetLimits.resize((size_t)NumbSubSections + 1);
	NumbOffsets.resize((size_t)NumbSubSections + 1);
	for (int sub = 1; sub <= NumbSubSections; sub++)
	{
		int numbCols2 = SubSecRowsCols[sub].x + 2;
		int numbRows2 = SubSecRowsCols[sub].y + 2;
		HorzOffsetLimits[sub].clear();
		VertOffsetLimits[sub].clear();
		for (int n = 0; n < numbCols2; n++)
		{
			int upperH;
			int lowerH;
			int upperV;
			int lowerV;
			double tmp = (double)n * ScaleLimit;
			if (tmp > 0.299)
				break;
			double del;
			if (n == 0)
				del = 0.35;
			else if (n == 1)
				del = 0.35;
			else if (n == 2)
				del = 0.25;
			else
				del = 0.15;

			upperH = (int)round((n + del) * WellTR_Spacing2f[sub].x);
			lowerH = (int)round((n - del) * WellTR_Spacing2f[sub].x);

			upperV = (int)round((n + del) * WellTR_Spacing2f[sub].y);
			lowerV = (int)round((n - del) * WellTR_Spacing2f[sub].y);

			HorzOffsetLimits[sub].push_back(std::pair<int, int>(upperH, lowerH));
			VertOffsetLimits[sub].push_back(std::pair<int, int>(upperV, lowerV));
			//Tolerance[sub] = cv::Point((int)round(WellTR_Spacing2f[sub].x * 0.4), (int)round(WellTR_Spacing2f[sub].y * 0.2));
		}
		NumbOffsets[sub] = (int)HorzOffsetLimits[sub].size();

		LogFile << "Connect Nodes, SubSection (" << sub << "): NumbOffsets: " << NumbOffsets[sub] << ", Basespacing: " << WellTR_Spacing2f[sub].x << "\n";
		LogFile << "     Horz Offsets\n";
		for (int h = 0; h < NumbOffsets[sub]; h++)
			LogFile << h << " - " << HorzOffsetLimits[sub][h].first << "/" << HorzOffsetLimits[sub][h].second << "\n";

		LogFile << "     Vert Offsets, Basespacing: " << WellTR_Spacing2f[sub].y << "\n";
		for (int v = 0; v < NumbOffsets[sub]; v++)
			LogFile << v << " - " << VertOffsetLimits[sub][v].first << "/" << VertOffsetLimits[sub][v].second << "\n";
		LogFile << "     ScaleLimit: " << ScaleLimit << "\n";
	}
	return true;
}

int AssignBlobsCD::GetSectionOfInterest()
{
	return SectionOfInterest;
}

bool AssignBlobsCD::SetSectionOfInterest(int NewSOI)
{
	if (NewSOI > NumbSections || NewSOI < 1)
		return false;
	SectionOfInterest = NewSOI;
	return true;
}

bool AssignBlobsCD::GetCorner11(int Section, int SubSection, cv::Point& Corner11)
{
	if (Section < 1 || Section > NumbSections || SubSection < 1 || SubSection > NumbSubSections)
		return false;

	int as = SectionSubSectionNumber(Section, SubSection);
	Corner11 = CornerList[as];
	return true;
}

bool AssignBlobsCD::ConnectNodes(std::ofstream& LogFile, std::string BaseName, int Section, int SubSection, bool Track)
{
	if (Section < 1 || Section > NumbSections || SubSection < 1 || SubSection > NumbSubSections)
		return false;

	int as = SectionSubSectionNumber(Section, SubSection);
	LogFile << "ConnectNodes: BlobNodeList[" << as << "].size() = " << BlobNodeList[as].size() << std::endl;
	if (BlobNodeList[as].size() < 10)
		return false;

	cv::Point2f target; // = CornerList[as];
	//if (Section >= SectionOfInterest)
	//	target += 3.0 * WellSpacing2f;
	//else
	//{
	//	int as0 = SectionSubSectionNumber(Array, SectionOfInterest);
	//	target.x += (float)3.0 * WellSpacing2f.x;
	//	target.y = CornerList[as0].y - (float)10.0 * WellSpacing2f.y;
	//}
	
	std::vector<std::vector< int > > rowCountList;
	std::vector< int > rowSums;
	int numbRowCount = (int)(ceil((double)SearchImageSize.height / (2.5 * WellTR_Spacing2f[1].y)) + 2);
	rowCountList.resize(numbRowCount);
	rowSums.resize(numbRowCount);
	double deltaRow = (2.5 * WellTR_Spacing2f[SubSection].y);
	for (int rc = 0; rc < numbRowCount; rc++)
		rowCountList[rc].clear();
	for (int b = 0; b < BlobNodeList[as].size(); b++)
	{
		int r = (int)floor(BlobNodeList[as][b].Theta_R2f.y / deltaRow);
		if (r >= 0 && r < numbRowCount)
			rowCountList[r].push_back(b);
	}
	int maxRowSum = 0;
	int maxRowSumIdx = -1;
	int firstRow;
	int lastRow;
	int minRow = -1;
	int maxRow = -1;
	if (SubSection == 1)
	{
		firstRow = 2;
		lastRow = (numbRowCount *5)/6;
	}
	else
	{
		firstRow = numbRowCount /4;
		lastRow = numbRowCount - 3;
	}
	for (int rc = firstRow; rc <= lastRow; rc++)
	{
		rowSums[rc] = (int)(rowCountList[(size_t)rc - 2].size() + rowCountList[(size_t)rc - 1].size() 
			+ rowCountList[rc].size() + rowCountList[(size_t)rc + 1].size() + rowCountList[(size_t)rc + 2].size());
		if (rowSums[rc] > maxRowSum)
		{
			maxRowSum = rowSums[rc];
			maxRowSumIdx = rc;
		}
		if (rowSums[rc] > 2 && minRow < 0)
			minRow = rc;
		else if (minRow >= 0 && rowSums[rc] > 2)
			maxRow = rc;
	}
	std::vector< std::vector< int > > colCountList;
	std::vector< int > colSums;
	int numbColCount = (int)(ceil((double)SearchImageSize.width / (2.5 * WellTR_Spacing2f[SubSection].x) + 2 ));
	colCountList.resize(numbColCount);
	colSums.resize(numbColCount);
	
	for (int cc = 0; cc < numbColCount; cc++)
		colCountList[cc].clear();

	double deltaCol = (2.5 * WellTR_Spacing2f[SubSection].x);
	for (int b = 0; b < BlobNodeList[as].size(); b++)
	{
		int c = (int)floor(BlobNodeList[as][b].Theta_R2f.x / deltaCol);
		if (c >= 0 && c < numbColCount)
			colCountList[c].push_back(b);
	}
	int firstCol;
	int lastCol;
	int minCol = -1;
	int maxCol = -1;
	if (Section == 1)
	{
		firstCol = 2;
		lastCol = (numbColCount * 6) / 10;
	}
	else
	{
		firstCol =( numbColCount * 4)/ 10;
		lastCol = numbColCount - 3;
	}
	int maxColSum = 0;
	int maxColSumIdx = -1;
	for (int cc = firstCol; cc < lastCol; cc++)
	{
		colSums[cc] = (int)(colCountList[(size_t)cc - 2].size() + colCountList[(size_t)cc - 1].size() + colCountList[cc].size()
			+ colCountList[(size_t)cc + 1].size() + colCountList[(size_t)cc + 2].size());
		if (colSums[cc] > maxColSum)
		{
			maxColSum = colSums[cc];
			maxColSumIdx = cc;
		}
	}
	target.x = (float)round(maxColSumIdx * deltaCol);
	target.y = (float)round(maxRowSumIdx * deltaRow);
	LogFile << "Section/SubSection = " << Section << " / " << SubSection << std::endl;
	LogFile << "BlobNodeList[" << as << "].size() = " << BlobNodeList[as].size() << std::endl;
	LogFile << "     numbColCount/numbRowCount = " << numbColCount << " / " << numbRowCount << std::endl;
	LogFile << "     target = " << target << std::endl;
	LogFile << "     first/lastRow = " << firstRow << " / " << lastRow << std::endl;
	LogFile << "     minRow/maxRow = " << minRow << " / " << maxRow << std::endl;
	LogFile << "     first/lastCol = " << firstCol << " / " << lastCol << std::endl;
	LogFile << "     maxRowSum/maxRowSumIdx/deltaRow = " << maxRowSum << " / " << maxRowSumIdx << " / " << deltaRow << std::endl;
	LogFile << "     maxColSum/maxColSumIdx/deltaCol = " << maxColSum << " / " << maxColSumIdx << " / " << deltaCol << std::endl;
	if (Track)
	{
		LogFile << "\nmaxRow data" << std::endl;
		for (int n = 0; n < rowCountList.size(); n++)
		{
			if (rowCountList[n].size() > 0)
				LogFile << n << " , " << n * deltaRow << " , " << rowCountList[n].size() << "\n";
		}
		LogFile << "\nmaxCol data" << std::endl;
		for (int n = 0; n < colCountList.size(); n++)
		{
			if (colCountList[n].size() > 0)
				LogFile << n << " , " << n * deltaCol << " , "<< colCountList[n].size() << "\n";
		}
	}
	int initNode = -1;
	for (int ri = 0; ri < rowCountList[maxRowSumIdx].size(); ri++)
	{
		for (int ci = 0; ci < colCountList[maxColSumIdx].size(); ci++)
		{
			if (rowCountList[maxRowSumIdx][ri] == colCountList[maxColSumIdx][ci])
			{
				initNode = rowCountList[maxRowSumIdx][ri];
				break;
			}
		}
	}
	int findInitNodePass = 1;
	if (initNode < 0)
	{
		std::vector< int > combRowList = rowCountList[maxRowSumIdx];
		combRowList.insert(combRowList.end(), rowCountList[(size_t)maxRowSumIdx - 1].begin(), rowCountList[(size_t)maxRowSumIdx - 1].end());
		combRowList.insert(combRowList.end(), rowCountList[(size_t)maxRowSumIdx + 1].begin(), rowCountList[(size_t)maxRowSumIdx + 1].end());
		combRowList.insert(combRowList.end(), rowCountList[(size_t)maxRowSumIdx - 2].begin(), rowCountList[(size_t)maxRowSumIdx - 2].end());
		combRowList.insert(combRowList.end(), rowCountList[(size_t)maxRowSumIdx + 2].begin(), rowCountList[(size_t)maxRowSumIdx + 2].end());
		std::vector< int > combColList = colCountList[maxColSumIdx];
		combColList.insert(combColList.end(), colCountList[(size_t)maxColSumIdx - 1].begin(), colCountList[(size_t)maxColSumIdx - 1].end());
		combColList.insert(combColList.end(), colCountList[(size_t)maxColSumIdx + 1].begin(), colCountList[(size_t)maxColSumIdx + 1].end());
		combColList.insert(combColList.end(), colCountList[(size_t)maxColSumIdx - 2].begin(), colCountList[(size_t)maxColSumIdx - 2].end());
		combColList.insert(combColList.end(), colCountList[(size_t)maxColSumIdx + 2].begin(), colCountList[(size_t)maxColSumIdx + 2].end());
		for (int ri = 0; ri < combRowList.size(); ri++)
		{
			for (int ci = 0; ci < combColList.size(); ci++)
			{
				if (combRowList[ri] == combColList[ci])
				{
					initNode = combRowList[ri];
					break;
				}
			}
		}
		findInitNodePass++;
		if (initNode < 0)
		{
			combColList.clear();
			int diff = 3;
			while (maxColSumIdx - diff >= 0 && (size_t)maxColSumIdx + diff < colCountList.size())
			{
				if (maxColSumIdx - diff >= 0)
					combColList.insert(combColList.end(), colCountList[(size_t)maxColSumIdx - diff].begin(), colCountList[(size_t)maxColSumIdx - diff].end());
				if ((size_t)maxColSumIdx + diff < colCountList.size())
					combColList.insert(combColList.end(), colCountList[(size_t)maxColSumIdx + diff].begin(), colCountList[(size_t)maxColSumIdx + diff].end());
				diff++;
			}
			for (int ri = 0; ri < combRowList.size(); ri++)
			{
				for (int ci = 0; ci < combColList.size(); ci++)
				{
					if (combRowList[ri] == combColList[ci])
					{
						initNode = combRowList[ri];
						break;
					}
				}
			}
			findInitNodePass++;
		}
	}
	int altInitNode = 0;
	double bestR2 = pow((double)BlobNodeList[as][0].Theta_R2f.x - target.x, 2)
		+ pow(BlobNodeList[as][0].Theta_R2f.y - target.y, 2);
	double deltaV = (double)BlobNodeList[as][0].Theta_R2f.y - target.y;
	double epsilon2 = pow(WellTR_Spacing2f[SubSection].y, 2);
	SearchImage.setTo(0);
	for (int b = 0; b < BlobNodeList[as].size(); b++)
	{
		if (BlobNodeList[as][b].DeviceCenter.x > 0
			&& BlobNodeList[as][b].DeviceCenter.x < SearchImageSize.width
			&& BlobNodeList[as][b].DeviceCenter.y > 0
			&& BlobNodeList[as][b].DeviceCenter.y < SearchImageSize.height)
		{
			SearchImage.at<short unsigned int>(BlobNodeList[as][b].DeviceCenter) = b + 1;
			double r2 = pow((double)BlobNodeList[as][b].Theta_R2f.x - target.x, 2)
				+ pow(BlobNodeList[as][b].Theta_R2f.y - target.y, 2);
			if (r2 < bestR2 - epsilon2)
			{
				altInitNode = b;
				bestR2 = r2;
				deltaV = (double)BlobNodeList[as][b].Theta_R2f.y - target.y;
			}
			else if (r2 < bestR2 + epsilon2)
			{
				double del = (double)BlobNodeList[as][b].Theta_R2f.y - target.y;
				if (del > deltaV)
				{
					altInitNode = b;
					bestR2 = r2;
					deltaV = del;
				}
			}
		}
	}
	LogFile << "Section/SubSection: " << Section << "/" << SubSection << ", BlobNodeList.size(): " << BlobNodeList[as].size()
		<< std::endl;
	LogFile << "findInitNodePass = " << findInitNodePass << std::endl;
	LogFile << "initNode = " << initNode;
	if (initNode >= 0)
		LogFile << ", R/Theta: " << BlobNodeList[as][initNode].Theta_R2f.y << " / " << BlobNodeList[as][initNode].Theta_R2f.x;
	LogFile << "\naltInitNode = " << altInitNode << ", row/col: " << BlobNodeList[as][altInitNode].Theta_R2f.y
		<< " / " << BlobNodeList[as][altInitNode].Theta_R2f.x << std::endl;
	if (initNode < 0)
	{
		LogFile << "Blob assignment failure" << std::endl;
		return false;
	}

	BlobNodeList[as][initNode].Status = 1;
	int deltaX = (int)round((BlobNodeList[as][initNode].Theta_R2f.x - CornerList[as].x) / WellTR_Spacing2f[SubSection].x);
	int deltaY = (int)round((BlobNodeList[as][initNode].Theta_R2f.y - CornerList[as].y) / WellTR_Spacing2f[SubSection].y);
	BlobNodeList[as][initNode].SubSection = SubSection;
	BlobNodeList[as][initNode].Section = SectionOfInterest;
	BlobNodeList[as][initNode].SubSectionPosition.x = deltaX + 1;
	BlobNodeList[as][initNode].SubSectionPosition.y = deltaY + 1;
	std::string header;
	std::string iNode = BlobNodeList[as][initNode].ToString(header);
	LogFile << header << "\n";
	LogFile << "InitNode: " << iNode << "\n";
	InitNodes[as] = initNode;
	
	cv::Rect searchRect;
	cv::Point searchOffset;
	cv::Size searchRectSize;
	if (NumbOffsets[SubSection] > 1)
	{
		searchRectSize.width = (int)round((NumbOffsets[SubSection] - 0.5) * WellTR_Spacing2f[SubSection].x * 2.0);
		searchRectSize.height = (int)round((NumbOffsets[SubSection] - 0.5) * WellTR_Spacing2f[SubSection].y * 2.0);
	}
	else
	{
		searchRectSize.width = (int)round(((double)WellTR_Spacing2f[(size_t)SubSection].x + HorzOffsetLimits[(size_t)SubSection][0].first)*2.0);
		searchRectSize.height = (int)round(((double)WellTR_Spacing2f[(size_t)SubSection].y + VertOffsetLimits[(size_t)SubSection][0].first)* 2.0);
	}
	if (searchRectSize.width % 2 == 0)
		searchRectSize.width++;
	if (searchRectSize.height % 2 == 0)
		searchRectSize.height++;
	searchOffset.x = -(searchRectSize.width  - 1) / 2;
	searchOffset.y = -(searchRectSize.height - 1) / 2;
	std::vector< cv::Point > locations;
	int pass = 0;
	int totalConnections = 0;
	int addedConnections = 0;
	LogFile << "searchRect.width/height: " << searchRectSize.width << " / " << searchRectSize.height << std::endl;
	LogFile << "searchOffset.x/y: " << searchOffset.x << " / " << searchOffset.y << std::endl;
	for (int n = 0; n < BlobNodeList[as].size() - 1; n++)
	{
		searchRect.x = (int)round(BlobNodeList[as][n].Theta_R2f.x + searchOffset.x);
		searchRect.y = (int)round(BlobNodeList[as][n].Theta_R2f.y + searchOffset.y);
		//if (Track)
		//{
		//	LogFile << "Node/Index/Adj.x/Adj.y/Inp.x/Inp.y: " << n << " / " << BlobNodeList[as][n].Index << " / "
		//		<< BlobNodeList[as][n].InputCenter2f.x << " / " << BlobNodeList[as][n].InputCenter2f.y << " / "
		//		<< BlobNodeList[as][n].InputTheta_R2f.x << " / " << BlobNodeList[as][n].InputTheta_R2f.y << "/"
		//		<< BlobNodeList[as][n].InputTheta_R2f.x << " / " << BlobNodeList[as][n].InputTheta_R2f.y << "\n";

		//	addedConnections = 0;
		//}
		addedConnections = 0;
		if (TrimRectangle(searchRect, searchRectSize, SearchImageSize))
		{
			cv::Point2f target = BlobNodeList[as][n].Theta_R2f;
			cv::findNonZero(SearchImage(searchRect), locations);
			for (int c = 0; c < locations.size(); c++)
			{
				int node = SearchImage(searchRect).at<short unsigned int>(locations[c]) - 1;
				if (node != n)
				{
					cv::Point2f delta = BlobNodeList[as][node].Theta_R2f - target;
					int deltaXI = (int)round(delta.x / WellTR_Spacing2f[SubSection].x);
					int deltaYI = (int)round(delta.y / WellTR_Spacing2f[SubSection].y);
					if ((abs(deltaXI) < NumbOffsets[SubSection] && abs(deltaYI) < NumbOffsets[SubSection]) &&
						(abs(deltaXI) == 0 || abs(deltaYI) == 0 || abs(deltaXI) + abs(deltaYI) < NumbOffsets[SubSection] - 1))
					{
						/*if (pass < 15 && c % 7 == 0)
						{
							pass++;
							LogFile << "n/node: " << n << "/" << node << ", deltaY/X: " << delta.y << "/" << delta.x
								<< ", deltaX/YI: " << deltaXI << "/" << deltaYI << "\n";
						}*/
						double errX = fabs(delta.x);
						double errY = fabs(delta.y);
						if (errX < HorzOffsetLimits[SubSection][abs(deltaXI)].first
							&& errX > HorzOffsetLimits[SubSection][abs(deltaXI)].second
							&& errY < VertOffsetLimits[SubSection][abs(deltaYI)].first
							&& errY > VertOffsetLimits[SubSection][abs(deltaYI)].second)
						{
							int idx = BlobNodeList[as][node].SelfIndex;
							BlobNodeList[as][n].AddConnection(idx, cv::Point(deltaXI, deltaYI));
							addedConnections++;
						}
					}
				}
			}
			//if (Track)
			//{
			//	LogFile << "     added Connections = " << addedConnections << "\n";
			//}
		}
		std::sort(BlobNodeList[as][n].Connections.begin(), BlobNodeList[as][n].Connections.end(), SortConnections);
		if (BlobNodeList[as][n].Connections.size() > 24)
			BlobNodeList[as][n].Connections.resize(24);
	}

	if (BlobNodeList[as].size() > 0 && Track)
	{
		std::stringstream oString;
		oString.str("");
		oString.clear();
		oString << BaseName << "_"  << SectionOfInterest << "_" << SubSection << "_CnctNodeList.csv";
		std::string outName = oString.str();
		std::ofstream outFile;
		outFile.open(outName);
		std::string header;
		std::string outString = BlobNodeList[as][0].ToString(header);
		outFile << "SearchBands.y/x,," << SearchBands[as].UpperLeft.y << "," << SearchBands[as].UpperLeft.x << ",,"
			<< SearchBands[as].LowerRight.y << "," << SearchBands[as].LowerRight.x << ",(Adjusted Centers)\n";
		outFile << header;
		for (int s = 0; s < BlobNodeList[as].size(); s++)
			outFile << BlobNodeList[as][s].ToString(header);
		outFile.close();
	}
	return true;
}

bool AssignBlobsCD::EvaluateNodes(std::ofstream& LogFile, int Section, int SubSection, std::string BaseName)
{
	int as = SectionSubSectionNumber(Section, SubSection);
	std::vector< std::pair<bool, bool> > nodesUsed(BlobNodeList[as].size());
	std::fill(nodesUsed.begin(), nodesUsed.end(), std::pair<bool, bool>(false, false));
	
	int initNode = InitNodes[as];
	nodesUsed[initNode].first = false;
	nodesUsed[initNode].second = true;
	int posX = (int)round((BlobNodeList[as][initNode].DeviceCenter.x - CornerList[as].x) / WellTR_Spacing2f[SubSection].x) + 1;
	int posY = (int)round((BlobNodeList[as][initNode].DeviceCenter.y - CornerList[as].y) / WellTR_Spacing2f[SubSection].y) + 1;
	BlobNodeList[as][initNode].SubSectionPosition = cv::Point(posX, posY);
	BlobNodeList[as][initNode].Status = 1;
	BlobNodeList[as][initNode].ReferenceNode.NodeIndex = -1;
	LogFile << ">>>>>>>>>> Assigning Section/SubSection: " << Section << "/" << SubSection << "<<<<<<<<<<\n";
	LogFile << "Initial Node: BlobNodeList[" << as << "][" << initNode << "].SectionPosition.y/x = "
		<< BlobNodeList[as][initNode].SubSectionPosition.y << "/" << BlobNodeList[as][initNode].SubSectionPosition.x
		<< ", SelfIndex = " << BlobNodeList[as][initNode].SelfIndex
		<< "\n   Input.R/Theta = " << BlobNodeList[as][initNode].InputTheta_R2f.y << "/" << BlobNodeList[as][initNode].InputTheta_R2f.x
		<< ", R/Theta = " << BlobNodeList[as][initNode].Theta_R2f.y << "/" << BlobNodeList[as][initNode].Theta_R2f.x
		<< "\n   Corner.R/Theta: " << CornerList[as].y << "/" << CornerList[as].x << "\n";
	LogFile << "      Numb Connections (initNode) = " << BlobNodeList[as][initNode].Connections.size() << std::endl;
	if (BlobNodeList[as][initNode].Connections.size() < 1)
		return false;
	for (int c = 0; c < BlobNodeList[as][initNode].Connections.size(); c++)
	{
		int idx1 = BlobNodeList[as][initNode].Connections[c].NodeIndex;
		BlobNodeList[as][idx1].SubSectionPosition = BlobNodeList[as][initNode].SubSectionPosition
			+ BlobNodeList[as][initNode].Connections[c].Shift;
		BlobNodeList[as][idx1].Status = 1;
		nodesUsed[idx1].second = true;
	}
	nodesUsed[initNode].first = true;

	bool flag = true;
	int firstNode = 0;
	int numbUnused = (int)nodesUsed.size() - 1;
	while (flag)
	{
		int unused = 0;
		for (int n = firstNode; n < nodesUsed.size(); n++)
		{
			if (!nodesUsed[n].first)
			{
				if (nodesUsed[n].second)
				{
					for (int c = 0; c < BlobNodeList[as][n].Connections.size(); c++)
					{
						int idx1 = BlobNodeList[as][n].Connections[c].NodeIndex;
						BlobNodeList[as][idx1].SubSectionPosition = BlobNodeList[as][n].SubSectionPosition 
							+ BlobNodeList[as][n].Connections[c].Shift;
						BlobNodeList[as][idx1].Status = 1;
						nodesUsed[idx1].second = true;
					}
					nodesUsed[n].first = true;
				}
			}
			if (!nodesUsed[n].first)
			{
				unused++;
				if (unused == 1)
					firstNode = n;
			}
		}
		if (unused == 0 || unused == numbUnused)
			flag = false;
		else
			numbUnused = unused;
	}
	std::ostringstream oString;
	std::string outName;
	std::ofstream outFile;
	std::string header;
	//int as0 = -1;
	//int as1 = -1;
	//if (SubSection > 1)
	//	as0 = SectionSubSectionNumber(Section, SubSection - 1);
	//if (SubSection < NumbSubSections)
	//	as1 = SectionSubSectionNumber(Section, SubSection + 1);

	std::sort(BlobNodeList[as].begin(), BlobNodeList[as].end(), SortBlobNodePositions);
	for (int b = 0; b < BlobNodeList[as].size(); b++)
		BlobNodeList[as][b].SelfIndex = b;
	if (Track)
	{
		oString.str("");
		oString.clear();
		oString << BaseName << "_" << Section << "_" << SubSection << "_InitNodeList.csv";
		outName = oString.str();
		outFile.open(outName);
		std::string outString = BlobNodeList[as][0].ToString(header);
		outFile << "SearchBands.y/x,," << SearchBands[as].UpperLeft.y << "," << SearchBands[as].UpperLeft.x << ",,"
			<< SearchBands[as].LowerRight.y << "," << SearchBands[as].LowerRight.x << ",(Adjusted Centers)\n";
		outFile << header << outString;
		for (int s = 1; s < BlobNodeList[as].size(); s++)
			outFile << BlobNodeList[as][s].ToString(header);
		outFile.close();
		LogFile << "BlobNodeList - Section: " << Section << ", SubSection: " << SubSection << " .size() = "
			<< BlobNodeList[as].size() << "\n";
	}
	
	DetermineSubSectionShift(LogFile, BaseName, Section, SubSection);
	LogFile << "ArraySectionShift for Section: " << Section << ", SubSection: " << SubSection
		<< " = " << SubSectionShift[as].y << "/" << SubSectionShift[as].x << "\n";

	for (int n = 0; n < BlobNodeList[as].size(); n++)
	{
		BlobNodeList[as][n].SubSectionPosition += SubSectionShift[as];
	}
	if (Track)
	{
		oString.str("");
		oString.clear();
		oString << BaseName << "_" << Section << "_" << SubSection << "_ShiftedNodeList.csv";
		outName = oString.str();
		outFile.open(outName);
		std::string outString = BlobNodeList[as][0].ToString(header);
		outFile << "SearchBands.y/x,," << SearchBands[as].UpperLeft.y << "," << SearchBands[as].UpperLeft.x << ",,"
			<< SearchBands[as].LowerRight.y << "," << SearchBands[as].LowerRight.x << ",(Adjusted Centers)\n";
		outFile << header << outString;
		for (int s = 1; s < BlobNodeList[as].size(); s++)
			outFile << BlobNodeList[as][s].ToString(header);
		outFile.close();
		LogFile << "BlobNodeList - Section: " << Section << ", SubSection: " << SubSection << " .size() = "
			<< BlobNodeList[as].size() << "\n";
	}

	//as0 = -1;
	//as1 = -1;
	//if (SubSection > 1)
	//	as0 = SectionSubSectionNumber(Section, SubSection - 1);
	//if (SubSection < NumbSubSections)
	//	as1 = SectionSubSectionNumber(Section, SubSection + 1);
	std::vector<int> eraseList;
	eraseList.clear();
	for (int n = 0; n < BlobNodeList[as].size(); n++)
	{
		if (BlobNodeList[as][n].SubSectionPosition.x < 1 || BlobNodeList[as][n].SubSectionPosition.x > SubSectionRowsCols[SubSection].x
			|| BlobNodeList[as][n].SubSectionPosition.y < 1 || BlobNodeList[as][n].SubSectionPosition.y > SubSectionRowsCols[SubSection].y)
		{
			//BlobNodeList[as][n].Status = 0;
			BlobNodeList[as][n].Connections.clear();
			BlobNodeList[as][n].NumbDirections = 0;
			BlobNodeList[as][n].Status = -10;
			UnalignedBlobNodeList.push_back(BlobNodeList[as][n]);
			eraseList.push_back(n);
		}
		else
		{
			int idx = BlobNodeList[as][n].Index;
			Blob_CDList[idx].SubSection = SubSection;
			Blob_CDList[idx].Section = Section;
			Blob_CDList[idx].SubSectionPosition = BlobNodeList[as][n].SubSectionPosition;
			Blob_CDList[idx].DeviceCenter = BlobNodeList[as][n].DeviceCenter;
			Blob_CDList[idx].Status = 1;
		}
	}
	if (eraseList.size() > 0)
	{
		std::sort(eraseList.begin(), eraseList.end());
		for (int n = (int)eraseList.size() - 1; n >= 0; n--)
			BlobNodeList[as].erase(BlobNodeList[as].begin() + eraseList[n]);
	}
	if (UnalignedBlobNodeList.size() > 0)
	{
		SortCorner = CornerList[0];
		std::sort(UnalignedBlobNodeList.begin(), UnalignedBlobNodeList.end(), SortBlobNodePositions);
		for (int b = 0; b < UnalignedBlobNodeList.size(); b++)
			UnalignedBlobNodeList[b].SelfIndex = b;
		if (Track)
		{
			oString.str("");
			oString.clear();
			oString << BaseName << "_0_0_NodeList.csv";
			outName = oString.str();
			outFile.open(outName);
			std::string outString = UnalignedBlobNodeList[0].ToString(header);
			outFile << header << outString;
			for (int s = 1; s < UnalignedBlobNodeList.size(); s++)
				outFile << UnalignedBlobNodeList[s].ToString(header);
			outFile.close();
		}
	}

	LogFile << "UnalignedBlobNodeList.size() = " << UnalignedBlobNodeList.size() << "\n";
	if (Track)
	{
		oString.str("");
		oString.clear();
		oString << BaseName << "_" << Section << "_" << SubSection << "_Shifted2NodeList.csv";
		outName = oString.str();
		outFile.open(outName);
		std::string outString = BlobNodeList[as][0].ToString(header);
		outFile << "SearchBands.y/x,," << SearchBands[as].UpperLeft.y << "," << SearchBands[as].UpperLeft.x << ",,"
			<< SearchBands[as].LowerRight.y << "," << SearchBands[as].LowerRight.x << ",(Adjusted Centers)\n";
		outFile << header << outString;
		for (int s = 1; s < BlobNodeList[as].size(); s++)
			outFile << BlobNodeList[as][s].ToString(header);
		outFile.close();
		LogFile << "BlobNodeList - Section: " << Section << ", SubSection: " << SubSection << " .size() = "
			<< BlobNodeList[as].size() << "\n";
	}
	return true;
}

int AssignBlobsCD::RecheckBlobs(std::ofstream& LogFile, std::vector< double > Results, double Scale)
{
	cv::Point innerUpperLeft = cv::Point(100000, 100000);
	cv::Point innerLowerRight = cv::Point(-1, -1);
	cv::Point outerUpperLeft = cv::Point(100000, 100000);
	cv::Point outerLowerRight = cv::Point(-1, -1);
	for (int n = 1; n <= NumbSubSections; n++)
	{
		int as = SectionSubSectionNumber(SectionOfInterest, n);
		for (int b = 0; b < BlobNodeList[as].size(); b++)
		{
			if (BlobNodeList[as][b].Theta_R2f.x < innerUpperLeft.x)
				innerUpperLeft.x = (int)round(BlobNodeList[as][b].Theta_R2f.x);
			if (BlobNodeList[as][b].Theta_R2f.y < innerUpperLeft.y)
				innerUpperLeft.y = (int)round(BlobNodeList[as][b].Theta_R2f.y);
			if (BlobNodeList[as][b].Theta_R2f.x > innerLowerRight.x)
				innerLowerRight.x = (int)round(BlobNodeList[as][b].Theta_R2f.x);
			if (BlobNodeList[as][b].Theta_R2f.y > innerLowerRight.y)
				innerLowerRight.y = (int)round(BlobNodeList[as][b].Theta_R2f.y);
		}
	}
	if (SectionOfInterest == 1)
	{
		outerUpperLeft.x = innerUpperLeft.x - (int)round(WellTR_Spacing2f[1].x * 3.0);
		outerUpperLeft.y = innerUpperLeft.y - (int)round(WellTR_Spacing2f[1].y * 2.0);

		outerLowerRight.x = innerLowerRight.x + (int)round(WellTR_Spacing2f[NumbSubSections].x * 0.4);
		outerLowerRight.y = innerLowerRight.y + (int)round(WellTR_Spacing2f[NumbSubSections].y * 2.0);
	}
	else if ( SectionOfInterest == NumbSections)
	{
		outerUpperLeft.x = innerUpperLeft.x - (int)round(WellTR_Spacing2f[1].x * 0.4);
		outerUpperLeft.y = innerUpperLeft.y - (int)round(WellTR_Spacing2f[1].y * 2.0);

		outerLowerRight.x = innerLowerRight.x + (int)round(WellTR_Spacing2f[NumbSubSections].x * 3.0);
		outerLowerRight.y = innerLowerRight.y + (int)round(WellTR_Spacing2f[NumbSubSections].y * 2.0);
	}
	else
	{
		outerUpperLeft.x = innerUpperLeft.x - (int)round(WellTR_Spacing2f[1].x * 0.4);
		outerUpperLeft.y = innerUpperLeft.y - (int)round(WellTR_Spacing2f[1].y * 2.0);

		outerLowerRight.x = innerLowerRight.x + (int)round(WellTR_Spacing2f[NumbSubSections].x * 0.4);
		outerLowerRight.y = innerLowerRight.y + (int)round(WellTR_Spacing2f[NumbSubSections].y * 2.0);
	}

	int numbInside = 0;
	LogFile << "<<<<<<<<<<<<<<<<<<\n";
	LogFile << "innerUpperLeft.x/y: " << innerUpperLeft.x << "/" << innerUpperLeft.y << "\n";
	LogFile << "innerLowerRight.x/y: " << innerLowerRight.x << "/" << innerLowerRight.y << "\n";
	LogFile << "outerUpperLeft.x/y: " << outerUpperLeft.x << "/" << outerUpperLeft.y << "\n";
	LogFile << "outerLowerRight.x/y: " << outerLowerRight.x << "/" << outerLowerRight.y << "\n";
	LogFile << "\n";
	int nAssigned = 0;

	std::vector<int > inside;
	inside.clear();
	for (int b = 0; b < UnalignedBlobNodeList.size(); b++)
	{
		if (UnalignedBlobNodeList[b].Theta_R2f.x >= outerUpperLeft.x
			&& UnalignedBlobNodeList[b].Theta_R2f.y >= outerUpperLeft.y
			&& UnalignedBlobNodeList[b].Theta_R2f.x <= outerLowerRight.x
			&& UnalignedBlobNodeList[b].Theta_R2f.y <= outerLowerRight.y
			)
		{
			numbInside++;
			inside.push_back(b);
			LogFile << "UnalignedBlobNodeList[" << b << "].Theta_R2f.x/y: "
				<< UnalignedBlobNodeList[b].Theta_R2f.x << "/" << UnalignedBlobNodeList[b].Theta_R2f.y
				<< "  -  InputCenter.x/y: " << UnalignedBlobNodeList[b].InputCenter2f.x << "/" << UnalignedBlobNodeList[b].InputCenter2f.y
				<< "\n";
		}
	}
	LogFile << "UnalignedBlobNodeList.size(): " << UnalignedBlobNodeList.size() << "\n";
	LogFile << "numbInSide: " << numbInside << "\n";
	LogFile << "<<<<<<<<<<<<<<<<<<\n";
	std::vector< std::vector<int> > addList;
	addList.resize((size_t)SectionSubSectionNumber(NumbSections, NumbSubSections) + 1);
	for (int n = 0; n < addList.size(); n++)
		addList[n].clear();

	if (numbInside > 0)
	{
		std::vector< std::vector< NodeConnection > > connections;
		NodeConnection newConnection;
		std::vector< int > closestConnection;
		closestConnection.resize((size_t)SectionSubSectionNumber(NumbSections, NumbSubSections) + 1);
		std::vector< cv::Point2f > maxDeltas;
		std::vector< int > numbOffs;
		maxDeltas.resize((size_t)NumbSubSections + 1);
		numbOffs.resize((size_t)NumbSubSections + 1);
		connections.resize((size_t)SectionSubSectionNumber(NumbSections, NumbSubSections) + 1);
		LogFile << "numbOffs, maxDeltas.x/y, WellTRSpacing.x/y\n";
		for (int sub = 1; sub <= NumbSubSections; sub++)
		{
			numbOffs[sub] = NumbOffsets[sub];
			if (numbOffs[sub] > 5)
				numbOffs[sub] = 5;

			maxDeltas[sub] = cv::Point2f((float)((numbOffs[sub] + 0.5) * WellTR_Spacing2f[sub].x),
				(float)((numbOffs[sub] + 0.5) * WellTR_Spacing2f[sub].y));
			 
			LogFile << sub << ", " << numbOffs[sub] << ", " << maxDeltas[sub].x << "/" << maxDeltas[sub].y 
				<< ", " << WellTR_Spacing2f[sub].x << "/" << WellTR_Spacing2f[sub].y << "\n";
		}

		for (int n = 0; n < inside.size() ; n++)
		{
			int u = inside[n];
			if (UnalignedBlobNodeList[u].DeviceCenter.x > 0 && UnalignedBlobNodeList[u].DeviceCenter.x < SectionImage.cols
				&& UnalignedBlobNodeList[u].DeviceCenter.y > 0 && UnalignedBlobNodeList[u].DeviceCenter.y < SectionImage.rows)
			{
				cv::Point2f target = UnalignedBlobNodeList[u].DeviceCenter;
				for (int sub = 1; sub <= NumbSubSections; sub++)
				{
					int as = SectionSubSectionNumber(SectionOfInterest, sub);
					connections[as].clear();
					for (int b = 0; b < BlobNodeList[as].size(); b++)
					{
						cv::Point2f delta = target - BlobNodeList[as][b].Theta_R2f;
						if (fabs(delta.x) < maxDeltas[sub].x && fabs(delta.y) < maxDeltas[sub].y)
						{
							int deltaXI = (int)round(delta.x / WellTR_Spacing2f[sub].x);
							int deltaYI = (int)round(delta.y / WellTR_Spacing2f[sub].y);
							//if (moreOutput)
							//	LogFile << "     deltaXI/YI: " << deltaXI << "/" << deltaYI << "\n";
							if (abs(deltaXI) + abs(deltaYI) < numbOffs[sub] - 1)
							{
								double errX = fabs(delta.x);
								double errY = fabs(delta.y);
								if (errX < HorzOffsetLimits[sub][abs(deltaXI)].first
									&& errX > HorzOffsetLimits[sub][abs(deltaXI)].second
									&& errY < VertOffsetLimits[sub][abs(deltaYI)].first
									&& errY > VertOffsetLimits[sub][abs(deltaYI)].second)
								{
									int newX = BlobNodeList[as][b].SubSectionPosition.x + deltaXI;
									int newY = BlobNodeList[as][b].SubSectionPosition.y + deltaYI;
									newConnection.Status = 0;
									newConnection.NodeIndex = b;
									newConnection.Shift = cv::Point(deltaXI, deltaYI);
									if (newX > 0 && newX <= SubSectionRowsCols[sub].x && newY > 0 && newY <= SubSectionRowsCols[sub].y)
									{
										if (abs(deltaXI) <= 1 && abs(deltaYI) <= 1)
										{
											connections[as].clear();
											connections[as].push_back(newConnection);
											break;
										}
										else
										{
											connections[as].push_back(newConnection);
										}
									}
								}
							}
						}
					}
				}
				std::fill(closestConnection.begin(), closestConnection.end(), -1);
				int numbC = 0;
				for (int sub = 1; sub <= NumbSubSections; sub++)
				{
					int as = SectionSubSectionNumber(SectionOfInterest, sub);
					if (connections[as].size() > 0)
					{
						if ( connections[as].size() > 1)
							std::sort(connections[as].begin(), connections[as].end(), SortConnections);
						closestConnection[as] = connections[as][0].NodeIndex;
						numbC++;
					}
				}
				if (numbC > 0)
				{
					int bestAS = -1;
					double bestDist2 = 1.0E+30;
					for (int sub = 1; sub <= NumbSubSections; sub++)
					{
						int as = SectionSubSectionNumber(SectionOfInterest, sub);
						if (closestConnection[as] >= 0)
						{
							int con = closestConnection[as];
							double dist2 = pow(connections[as][0].Shift.x, 2) + pow(connections[as][0].Shift.y, 2);
							if (dist2 < bestDist2)
							{
								bestDist2 = dist2;
								bestAS = as;
							}
						}
					}
					if (bestAS >= 0)
					{
						int sec;
						int sub;
						SectionSubSectionFromNumber(bestAS, sec, sub);
						if (sec == SectionOfInterest)
						{
							UnalignedBlobNodeList[u].Section = sec;
							UnalignedBlobNodeList[u].SubSection = sub;
							int con = closestConnection[bestAS];
							UnalignedBlobNodeList[u].SubSectionPosition = BlobNodeList[bestAS][con].SubSectionPosition + connections[bestAS][0].Shift;
							UnalignedBlobNodeList[u].DeviceCenter = BlobNodeList[bestAS][con].DeviceCenter;
							UnalignedBlobNodeList[u].Status = 1;
							addList[bestAS].push_back(u);
						}
						
					}
				}
				else
				{
					UnalignedBlobNodeList[u].Section = 0;
					UnalignedBlobNodeList[u].SubSection = 0;
					UnalignedBlobNodeList[u].SubSectionPosition = cv::Point(0, 0);
					UnalignedBlobNodeList[u].Status = 0;
					addList[SectionSubSectionNumber(SectionOfInterest,1)].push_back(u);
				}
			}
		}
		for (int sub = 1; sub <= NumbSubSections; sub++)
		{
			int as = SectionSubSectionNumber(SectionOfInterest, sub);
			LogFile << "addList[" << as << "].size() = " << addList[as].size() << "\n";
			if (addList[as].size() > 0)
			{
				for (int a = 0; a < addList[as].size(); a++)
				{
					int b = addList[as][a];
					UnalignedBlobNodeList[b].SelfIndex = (int)BlobNodeList[as].size();
					BlobNodeList[as].push_back(UnalignedBlobNodeList[b]);
					if (Track)
					{
						LogFile << "Added UnalignedBlobNodeList[" << b << "].Theta_R2f.x/y: "
							<< UnalignedBlobNodeList[b].Theta_R2f.x << "/" << UnalignedBlobNodeList[b].Theta_R2f.y
							<< "  -  InputCenter.x/y: " << UnalignedBlobNodeList[b].InputCenter2f.x << "/" << UnalignedBlobNodeList[b].InputCenter2f.y
							<< "  -  SectionPostion.x/y: " << UnalignedBlobNodeList[b].SubSectionPosition.x << "/" << UnalignedBlobNodeList[b].SubSectionPosition.y
							<< "  -  DeviceCenter.x/y: " << UnalignedBlobNodeList[b].DeviceCenter.x << "/" << UnalignedBlobNodeList[b].DeviceCenter.y
							<< "  - UnalignedBlobNodeList[b].Main/Idx: " << UnalignedBlobNodeList[b].MainListIndex << "/" << UnalignedBlobNodeList[b].Index
							<< "  - Status/Section/Sub: " << UnalignedBlobNodeList[b].Status << "/" << UnalignedBlobNodeList[b].Section << "/"
							<< UnalignedBlobNodeList[b].SubSection
							<< "\n";
					}
					int idx = UnalignedBlobNodeList[b].MainListIndex;
					Blob_CDList[idx].SubSection = UnalignedBlobNodeList[b].SubSection;
					Blob_CDList[idx].Section = UnalignedBlobNodeList[b].Section;
					Blob_CDList[idx].SubSectionPosition = UnalignedBlobNodeList[b].SubSectionPosition;
					Blob_CDList[idx].DeviceCenter = UnalignedBlobNodeList[b].DeviceCenter;
					Blob_CDList[idx].FilterValue = UnalignedBlobNodeList[b].Status;

					UnalignedBlobNodeList[addList[as][a]].Status = -20;
				}
			}
		}
	}
	return nAssigned;
}

bool AssignBlobsCD::DetermineSubSectionShift(std::ofstream& LogFile, std::string BaseName, int Section, int SubSection)
{
	if (SubSection < 1 || SubSection > NumbSubSections )
		return false;

	int as = SectionSubSectionNumber(Section, SubSection);
	int rowOffset = 0;
	int colOffset = 0;
	int numbCols = SubSectionRowsCols[SubSection].x;
	int minX = 2 * numbCols;
	int maxX = -numbCols;
	int numbRows = SubSectionRowsCols[SubSection].y;
	int minY = 2 * numbRows;
	int maxY = -numbRows;
	LogFile << "DetermineArraySectionShift: BlobNodeList[" << as << "].size() = " << BlobNodeList[as].size() << "\n";
	for (int n = 0; n < BlobNodeList[as].size(); n++)
	{
		if (BlobNodeList[as][n].Status > 0)
		{
			int idx = BlobNodeList[as][n].SubSectionPosition.x;
			int idy = BlobNodeList[as][n].SubSectionPosition.y;
			if (idx > -numbCols && idx < 2 * numbCols && idy > -numbRows && idy < 2 * numbRows)
			{
				if (idx < minX)
					minX = idx;
				if (idx > maxX)
					maxX = idx;
				
				if (idy < minY)
					minY = idy;
				if (idy > maxY)
					maxY = idy;
			}
		}
	}
	if (maxX <= minX || maxY <= minY)
		return false;
	colOffset = -minX;
	rowOffset = -minY;
	std::vector< int > colCount((size_t)((size_t)maxX - minX + 2));
	std::fill(colCount.begin(), colCount.end(), 0);
	std::vector< AlignData > rowAlign((size_t)((size_t)maxY - minY + 2));
	AlignData blankAlignData;
	blankAlignData.Index = 0;
	blankAlignData.Length = 0;
	blankAlignData.Count = 0;
	blankAlignData.Stats.Clear();
	blankAlignData.Spacing = 0.0;
	blankAlignData.SpacingOK = true;
	std::fill(rowAlign.begin(), rowAlign.end(), blankAlignData);
	std::vector< double > rowSpacing((size_t)((size_t)maxY - minY + 2));
	std::fill(rowSpacing.begin(), rowSpacing.end(), 0.0);

	for (int n = 0; n < BlobNodeList[as].size(); n++)
	{
		if (BlobNodeList[as][n].Status > 0)
		{
			int idx = BlobNodeList[as][n].SubSectionPosition.x;
			int idy = BlobNodeList[as][n].SubSectionPosition.y;
			if (idx > -numbCols && idx < 2 * numbCols && idy > -numbRows && idy < 2 * numbRows)
			{
				colCount[(size_t)idx + colOffset]++;
				rowAlign[idy + (size_t)rowOffset].Stats.Accumulate(BlobNodeList[as][n].Theta_R2f.y);
			}
		}
	}

	LogFile << "Section/SubSection " << SectionOfInterest << " / " << SubSection << "\n";
	if (Track)
	{
		LogFile << "     Col Count (min/maxX = " << minX << " / " << maxX << ")\n";
		for (int c = minX; c <= maxX; c++)
		{
			LogFile << c << " - " << colCount[(size_t)c + colOffset] << std::endl;
		}
		LogFile << "     Row Stats (min/maxY = " << minY << " / " << maxY << ")\n";
		for (int r = minY; r <= maxY; r++)
		{
			LogFile << r << " - " << rowAlign[r + (size_t)rowOffset].Stats.Count() << std::endl;
		}
	}
	int bestX = -1;
	int bestXValue = -1;
	if (maxX - minX + 1 <= numbCols)
	{
		//LogFile << "maxX - minX + 1 / NumbCols = " << maxX - minX + 1 << " / " << NumbCols << std::endl;
		if (Section == 1)
		{
			SubSectionShift[as].x = 1 - minX;
		}
		else if (Section == NumbSections)
		{
			SubSectionShift[as].x = numbCols - maxX;
		}
		else
		{
			if (maxX <= numbCols && minX >= 1)
				SubSectionShift[as].x = 0;
			else if (maxX > numbCols)
				SubSectionShift[as].x = numbCols - maxX;
			else if (minX < 1)
				SubSectionShift[as].x = 1 - minX;
		}
		if ( Track)
			LogFile << "ArraySectionShift[as].x = " << SubSectionShift[as].x << std::endl;
	}
	else
	{
		//LogFile << "minX / maxX / maxX - NumbCols = " << minX << " / " << maxX << " / " << maxX - NumbCols << std::endl;

		for (int x = minX; x <= maxX - numbCols + 1; x++)
		{
			int xcnt = x;
			int cnt = 0;
			while ((size_t)xcnt + colOffset < colCount.size() && xcnt - x < numbCols)
			{
				cnt += colCount[(size_t)xcnt + colOffset];
				xcnt++;
			}
			if (cnt > bestXValue)
			{
				bestXValue = cnt;
				bestX = x;
			}

		}
		SubSectionShift[as].x = 1 - bestX;
		if ( Track)
			LogFile << "ArraySectionShift[as].x = " << SubSectionShift[as].x << std::endl;
	}
	std::vector<int> emptyRows;
	emptyRows.clear();
	double dPointedAt;
	for (int n = 0; n < rowAlign.size(); n++)
	{
		rowAlign[n].Index = n - rowOffset;
		rowAlign[n].Stats.Analyze();
		if (n > 1 && rowAlign[n].Stats.Count() > 0)
		{
			int nm1 = n - 1;
			while (nm1 > 0)
			{
				if (rowAlign[nm1].Stats.Count() > 0)
				{
					rowAlign[nm1].Spacing = (rowAlign[n].Stats.Ave() - rowAlign[nm1].Stats.Ave())/WellTR_Spacing2f[SubSection].y;
					double md = std::modf(rowAlign[nm1].Spacing, &dPointedAt);
					if (rowAlign[nm1].Spacing < 2.0)
					{
						if (md > 0.25 && md < 0.75)
							rowAlign[nm1].SpacingOK = false;
						else
							rowAlign[nm1].SpacingOK = true;
					}
					else if (rowAlign[nm1].Spacing < 4.0)
					{
						if (md > 0.23 && md < 0.77)
							rowAlign[nm1].SpacingOK = false;
						else
							rowAlign[nm1].SpacingOK = true;
					}
					else if (rowAlign[nm1].Spacing < 6.0)
					{
						if (md > 0.21 && md < 0.79)
							rowAlign[nm1].SpacingOK = false;
						else
							rowAlign[nm1].SpacingOK = true;
					}
					else
					{
						if (md > 0.20 && md < 0.80)
							rowAlign[nm1].SpacingOK = false;
						else
							rowAlign[nm1].SpacingOK = true;
					}
						break;
				}
				nm1--;
			}
		}
		if (rowAlign[n].Stats.Count() == 0)
		{
			rowAlign[n].SpacingOK = true;
			rowAlign[n].Count = 0;
			emptyRows.push_back(n);
		}
	}
	int maxLengthOfRows = 0;
	int firstN = 0;
	if (Section > 1)
	{
		if (emptyRows.size() > 0)
		{
			if (emptyRows[0] < numbRows / 3)
				firstN = emptyRows[0];
		}
	}
	for (int n = firstN; n < rowAlign.size() - 1; n++)
	{
		rowAlign[n].Length = 1;
		rowAlign[n].Count = (int)rowAlign[n].Stats.Count();
		if (rowAlign[n].SpacingOK && rowAlign[n].Count > 0)
		{
			for (int nn = n + 1; nn < rowAlign.size(); nn++)
			{
				rowAlign[n].Length++;
				rowAlign[n].Count += (int)rowAlign[nn].Stats.Count();
				if (!rowAlign[nn].SpacingOK || rowAlign[n].Length >= numbRows || nn >= rowAlign.size() - 1)
				{
					if (rowAlign[nn].Stats.Count() < 1)
					{
						int mm = nn;
						while (rowAlign[mm].Stats.Count() < 1 && mm > n)
							mm--;
						rowAlign[n].Length -= (nn - mm);
					}
					if (rowAlign[n].Length > maxLengthOfRows)
					{
						maxLengthOfRows = rowAlign[n].Length;
						if (Track)
						{
							LogFile << "rowAlign[n].Length >= maxLengthOfRows: " << n << " / " << rowAlign[n].Length << " / " << maxLengthOfRows
								<< " / Index = " << rowAlign[n].Index << " / rowOffset = " << rowOffset << "\n";
						}
						
					}
					break;
				}
			}
		}
	}
	if (rowAlign.size() > 0)
	{
		rowAlign[rowAlign.size() - 1].Length = 1;
		rowAlign[rowAlign.size() - 1].Count = rowAlign[rowAlign.size() - 1].Stats.Count();
	}
	//LogFile << "SOI Final maxLengthOfRows = " << maxLengthOfRows << std::endl;
	std::vector< int > idxWithMaxLength;
	idxWithMaxLength.clear();
	int lastN = (int)rowAlign.size() - maxLengthOfRows;
	if (lastN < 1)
		lastN = 1;
	for (int n = 0; n < lastN; n++)
	{
		if (rowAlign[n].Length >= maxLengthOfRows)
			idxWithMaxLength.push_back(n);
	}
	int bestY;
	if (idxWithMaxLength.size() == 1)
	{
		int idx = idxWithMaxLength[0];
		bestY = rowAlign[idx].Index;
	}
	else
	{
		int idx = idxWithMaxLength[0];
		int maxIdx = idx;
		int maxIdxCount = rowAlign[idx].Count;
		for (int n = 1; n < idxWithMaxLength.size(); n++)
		{
			idx = idxWithMaxLength[n];
			if (rowAlign[idx].Count > maxIdxCount)
			{
				maxIdxCount = rowAlign[idx].Count;
				maxIdx = n;
			}
		}
		bestY = rowAlign[maxIdx].Index;
		//ogFile << "          bestY = " << bestY << std::endl;
	}
	SubSectionShift[as].y = 1 - bestY;
	if (Track)
	{
		LogFile << "row stats: Section/SubSection: " << Section << "/" << SubSection << std::endl;
		LogFile << "maxLengthOfRows: " << maxLengthOfRows << ", bestY = " << bestY << std::endl;
		for (int n = 0; n < idxWithMaxLength.size(); n++)
			LogFile << "     maxLength Index: " << idxWithMaxLength[n] << std::endl;
		LogFile << "\nIndex / Ave / Std / SCnt / LCount / Length / Spacing / OK\n";
		for (int n = 0; n < rowAlign.size(); n++)
		{
			LogFile << rowAlign[n].Index << " / " << rowAlign[n].Stats.Ave() << " / " << rowAlign[n].Stats.Std() << " / " << rowAlign[n].Stats.Count() 
				<< " /" << rowAlign[n].Count 
				<< " / " << rowAlign[n].Length << " / " << rowAlign[n].Spacing << " / " << std::boolalpha << rowAlign[n].SpacingOK << std::endl;
		}
	}

	LogFile << "ArraySectionShift[" << as << "].y/x " << SubSectionShift[as].y << "/" << SubSectionShift[as].x << std::endl;
	return true;
}

int AssignBlobsCD::GetNumberOfNodes(int Section, int SubSection)
{
	if (Section < 1 || Section > NumbSections || SubSection < 1 || SubSection > NumbSubSections)
		return 0;
	
		int as = SectionSubSectionNumber(Section, SubSection);
		return (int)BlobNodeList[as].size();
}

std::string AssignBlobsCD::SectionShiftsToString()
{
	std::ostringstream oString;
	oString.str("");
	oString.clear();
	oString << "Array,Section,SecPos.x,SecPos.y,Numb of Blobs\n";
	for (int sec = 1; sec <= NumbSections; sec++)
	{
		for (int sub = 1; sub <= NumbSubSections; sub++)
		{
			int as = SectionSubSectionNumber(sec, sub);
			oString << sec << "," << sub << "," << SubSectionShift[as].x << "," << SubSectionShift[as].y << "," << NumbInSubSection[as] << "\n";
		}
	}
	return oString.str();
}

void AssignBlobsCD::GetBlob_CDList(std::vector< Blob_CD>& Blob_CDListOut)
{
	Blob_CDListOut = Blob_CDList;
}

void AssignBlobsCD::UpdateBlobList(std::vector< Blob >& BlobList)
{
	for (int n = 0; n < Blob_CDList.size(); n++)
	{
		int tmp = Blob_CDList[n].MainListIndex;
		if (tmp >= 0 && tmp < BlobList.size())
		{
			int sec = Blob_CDList[n].Section;
			int sub = Blob_CDList[n].SubSection;
			int s = 1;
			int numbRows = 0;
			while (s < sub)
			{
				numbRows += SubSectionRowsCols[s].y;
				s++;
			}
			
			BlobList[tmp].Section = sec;
			BlobList[tmp].SectionPosition.x = Blob_CDList[n].SubSectionPosition.x;
			BlobList[tmp].SectionPosition.y = Blob_CDList[n].SubSectionPosition.y + numbRows;
			BlobList[tmp].Status = Blob_CDList[n].Status;
			BlobList[tmp].DeviceCenter = Blob_CDList[n].DeviceCenter;
			BlobList[tmp].AdjustedCenter2f = Blob_CDList[n].Theta_R2f;
		}
	}
}
void AssignBlobsCD::UpdateOtherBlobList(std::vector< Blob >& OtherBlobList, std::vector<Blob_CD> &OtherBlob_CDList)
{
	for (int n = 0; n < OtherBlob_CDList.size(); n++)
	{
		int tmp = OtherBlob_CDList[n].MainListIndex;
		if (tmp >= 0 && tmp < OtherBlobList.size())
		{
			int sec = Blob_CDList[n].Section;
			int sub = Blob_CDList[n].SubSection;
			int s = 1;
			int numbRows = 0;
			while (s < sub)
			{
				numbRows += SubSectionRowsCols[s].y;
				s++;
			}
			OtherBlobList[tmp].Section = sec;
			OtherBlobList[tmp].SectionPosition.x = Blob_CDList[n].SubSectionPosition.x;
			OtherBlobList[tmp].SectionPosition.y = Blob_CDList[n].SubSectionPosition.y + numbRows;
			OtherBlobList[tmp].Status = Blob_CDList[n].Status;
		}
	}
}

void AssignBlobsCD::AssignOtherBlobs(std::ofstream& LogFile, std::vector< Blob >& OtherBlobList, std::vector< Blob_CD > &OtherBlob_CDList,
	std::vector< double > Results, double Scale)
{
	std::vector< NodeConnection > connections;
	std::vector< NodeConnection > otherConnections;
	NodeConnection newConnection;
	Blob_CD newOtherBlob;
	double imageMiddle = (double)( - 1) / 2.0;
	LogFile << "OtherBlob_CDList.size() = " << OtherBlob_CDList.size() << std::endl;
	if ( OtherBlob_CDList.size() > 0)
	{
		for ( int n = 0 ; n < OtherBlob_CDList.size(); n++)
		{
			if (OtherBlob_CDList[n].OnImage && OtherBlob_CDList[n].FilterValue < 10 && OtherBlob_CDList[n].FilterValue > 0)
			{
				
				cv::Point2f target = OtherBlob_CDList[n].DeviceCenter;
				int gValue = SectionImage.at<uchar>(OtherBlob_CDList[n].DeviceCenter);
				int sec = (int)floor(gValue / 10);
				int sub = gValue % 10;
				//std::cout << "otherBlob_CDList[" << n << "], sec/sub: " << sec << "/" << sub << std::endl;
				if (sub > 0 && sub <= NumbSubSections)
				{
					cv::Point2f maxDelta = cv::Point2f((float)((NumbOffsets[sub] + 0.5) * WellTR_Spacing2f[sub].x),
						(float)((NumbOffsets[sub] + 0.5) * WellTR_Spacing2f[sub].y));
					OtherBlob_CDList[n].Section = sec;
					OtherBlob_CDList[n].SubSection = sub;
					int numbCols = SubSectionRowsCols[sub].x;
					int numbRows = SubSectionRowsCols[sub].y;
					connections.clear();
					otherConnections.clear();
					for (int b = 0; b < Blob_CDList.size(); b++)
					{
						if (Blob_CDList[b].Status > 0)
						{
							cv::Point2f delta = target - Blob_CDList[b].Theta_R2f;
							if (fabs(delta.x) < maxDelta.x && fabs(delta.y) < maxDelta.y)
							{
								int deltaXI = (int)round(delta.x / WellTR_Spacing2f[sub].x);
								int deltaYI = (int)round(delta.y / WellTR_Spacing2f[sub].y);
								if ((abs(deltaXI) < NumbOffsets[sub] && abs(deltaYI) < NumbOffsets[sub]) &&
									(abs(deltaXI) == 0 || abs(deltaYI) == 0 || abs(deltaXI) + abs(deltaYI) < NumbOffsets[sub] - 1))
								{
									double errX = fabs(delta.x);
									double errY = fabs(delta.y);
									//double errR2 = pow(errX, 2) + pow(errY, 2);
									if (errX < HorzOffsetLimits[sub][abs(deltaXI)].first
										&& errX > HorzOffsetLimits[sub][abs(deltaXI)].second
										&& errY < VertOffsetLimits[sub][abs(deltaYI)].first
										&& errY > VertOffsetLimits[sub][abs(deltaYI)].second)
									{
										int newX = Blob_CDList[b].SubSectionPosition.x + deltaXI;
										int newY = Blob_CDList[b].SubSectionPosition.y + deltaYI;
										newConnection.Status = 0;
										newConnection.NodeIndex = b;
										newConnection.Shift = cv::Point(deltaXI, deltaYI);
										if (newX > 0 && newX <= numbCols && newY > 0 && newY <= numbRows)
										{
											if (abs(deltaXI) <= 1 && abs(deltaYI) <= 1)
											{
												connections.clear();
												connections.push_back(newConnection);
												break;
											}
											else
											{
												connections.push_back(newConnection);
											}
										}
										otherConnections.push_back(newConnection);
									}
								}
							}
						}
					}
					//LogFile << "connection sizes: " << connections.size() << "/" << otherConnections.size() << std::endl;
					//LogFile << "Assign Other: " << OtherBlobList[n].SelfIndex;
					if (connections.size() > 0)
					{
						if (connections.size() > 1)
							std::sort(connections.begin(), connections.end(), SortConnections);
						//LogFile << " - Numb Connections = " << connections.size() << ", connection[0]: " << connections[0].ToString() << std::endl;
						OtherBlob_CDList[n].Section = Blob_CDList[connections[0].NodeIndex].Section;
						OtherBlob_CDList[n].SubSection = Blob_CDList[connections[0].NodeIndex].SubSection;
						OtherBlob_CDList[n].SubSectionPosition = Blob_CDList[connections[0].NodeIndex].SubSectionPosition + connections[0].Shift;
						OtherBlob_CDList[n].Status = 1;
					}
					else
					{
						std::vector< std::vector< int > > triples;
						std::vector< int > newTriple(3);
						triples.clear();
						LogFile << " - No connections\n";
						for (int b = 0; b < Blob_CDList.size(); b++)
						{
							cv::Point2f delta = target - Blob_CDList[b].Theta_R2f;
							newTriple[0] = n;
							if (fabs(delta.x) < 5.0 * WellTR_Spacing2f[sub].x && fabs(delta.y) < 5.0 * WellTR_Spacing2f[sub].y)
							{
								newTriple[1] = (int)round(pow(delta.x, 2) + pow(delta.y, 2));
								newTriple[2] = b;
								triples.push_back(newTriple);
							}
						}
						if (triples.size() > 1)
							std::sort(triples.begin(), triples.end(), SortTriples);

						int last = 6;
						if (triples.size() < 6)
							last = (int)triples.size();
						for (int t = 0; t < last; t++)
						{
							int b = triples[t][2];
							cv::Point2f delta = target - Blob_CDList[b].Theta_R2f;
							LogFile << n
								<< ", " << OtherBlobList[n].InputCenter2f.y << "/" << OtherBlobList[n].InputCenter2f.x
								<< ", " << target.y << "/" << target.x
								<< " : " << b << ", " << Blob_CDList[b].AdjustedTheta_R2f.y << "/" << Blob_CDList[b].AdjustedTheta_R2f.x
								<< ", " << Blob_CDList[b].Theta_R2f.y << "/" << Blob_CDList[b].Theta_R2f.x
								<< " : " << delta.y << "/" << delta.x
								<< " : " << delta.y / WellTR_Spacing2f[sub].y << "/" << delta.x / WellTR_Spacing2f[sub].x
								<< "\n";
						}
					}
					LogFile << "=====\n";
				}
			}
			else
			{
				OtherBlob_CDList[n].Section = 0;
				OtherBlob_CDList[n].SubSection = 0;
				OtherBlob_CDList[n].SubSectionPosition = cv::Point(0,0);
				OtherBlob_CDList[n].Status = 0;
			}
		}
		UpdateOtherBlobList(OtherBlobList, OtherBlob_CDList);
	}
}

bool AssignBlobsCD::EstimateScale(std::ofstream& LogFile, int SubSection, double& AveScale, double& XScale, double& YScale)
{
	Statistics1 scaleXStats;
	Statistics1 scaleYStats;
	scaleXStats.Clear();
	scaleYStats.Clear();
	//double delta;
	std::vector< std::vector< int > > subSectionMatrix;
	int numbCols = SubSectionRowsCols[SubSection].x;
	int numbRows = SubSectionRowsCols[SubSection].y;
	subSectionMatrix.resize((size_t)numbRows + 1);
	for (int r = 1; r <= numbRows; r++)
	{
		subSectionMatrix[r].resize((size_t)numbCols + 1);
		std::fill(subSectionMatrix[r].begin(), subSectionMatrix[r].end(), -1);
	}

	int as = SectionSubSectionNumber(SectionOfInterest, SubSection);
	for (int b = 0; b < BlobNodeList[as].size(); b++)
	{
		if (BlobNodeList[as][b].Status > 0)
		{
			int idx = BlobNodeList[as][b].Index;
			if (Blob_CDList[idx].FilterValue > 0)
			{
				if (!Blob_CDList[idx].OnImage)
				{
					LogFile << "For idx = " << idx << ", SectionPosition.x/.y = " << Blob_CDList[idx].SubSectionPosition.x
						<< "/" << Blob_CDList[idx].SubSectionPosition.y << std::endl;
				}
				else
					subSectionMatrix[Blob_CDList[idx].SubSectionPosition.y][Blob_CDList[idx].SubSectionPosition.x] = idx;
			}
		}
	}
	double delta;
	for (int r = 1; r <= numbRows; r++)
	{
		int cCurr = 1;
		for (cCurr = 1; cCurr <= numbCols; cCurr++)
		{
			if (subSectionMatrix[r][cCurr] >= 0)
				break;
		}
		int cNext = FindNextColumn(subSectionMatrix, r, cCurr);
		while (cNext < numbCols + 1)
		{
			if (cNext <= numbCols)
			{
				int idx0 = subSectionMatrix[r][cCurr];
				int idx1 = subSectionMatrix[r][cNext];
				delta = ((double)Blob_CDList[idx1].Theta_R2f.x - Blob_CDList[idx0].Theta_R2f.x) / ((double)cNext - cCurr);
				scaleXStats.Accumulate(delta);
				cCurr = cNext;
				cNext = FindNextColumn(subSectionMatrix, r, cCurr);
			}
			else
				break;
		}
	}
	for (int c = 1; c <= numbCols; c++)
	{
		int rCurr = 1;
		for (rCurr = 1; rCurr <= numbRows; rCurr++)
		{
			if (subSectionMatrix[rCurr][c] >= 0)
				break;
		}
		int rNext = FindNextRow(subSectionMatrix, rCurr, c);
		while (rNext < numbRows + 1)
		{
			if (rNext <= numbRows)
			{
				int idx0 = subSectionMatrix[rCurr][c];
				int idx1 = subSectionMatrix[rNext][c];
				delta = ((double)Blob_CDList[idx1].Theta_R2f.y - Blob_CDList[idx0].Theta_R2f.y) / ((double)rNext - rCurr);
				scaleYStats.Accumulate(delta);
				rCurr = rNext;
				rNext = FindNextRow(subSectionMatrix, rCurr, c);
			}
		}
	}
	
	scaleXStats.Analyze();
	scaleYStats.Analyze();
	LogFile << "scaleXStats.Ave/Count: " << scaleXStats.Ave() << "/" << scaleXStats.Count() << ", WellSpacing2f.x: " << WellTR_Spacing2f[SubSection].x << std::endl;
	LogFile << "scaleYStats.Ave/Count: " << scaleYStats.Ave() << "/" << scaleYStats.Count() << ", WellSpacing2f.y: " << WellTR_Spacing2f[SubSection].y << std::endl;
	if (scaleXStats.Ave() > 0.5 && scaleYStats.Ave() > 0.5)
	{
		XScale = WellTR_Spacing2f[SubSection].x / scaleXStats.Ave();
		YScale = WellTR_Spacing2f[SubSection].y / scaleYStats.Ave();
		if (scaleXStats.Count() > 100)
		{
			if (scaleYStats.Count() > 100)
				AveScale = 0.5 * (XScale + YScale);
			else if (scaleYStats.Count() > 20)
				AveScale = 0.666 * XScale + 0.334 * YScale;
			else
				AveScale = XScale;
		}
		else if (scaleXStats.Count() > 20)
		{
			if (scaleYStats.Count() > 100)
				AveScale = 0.334 * XScale + 0.666 * YScale;
			else if (scaleYStats.Count() > 20)
				AveScale = 0.50 * XScale + 0.50 * YScale;
			else
				AveScale = 1.0;
		}
		else
		{
			if (scaleYStats.Count() > 20)
				AveScale = YScale;
			else
				AveScale = 1.0;
		}
	}
	else
	{
		XScale = 1.0;
		YScale = 1.0;
		AveScale = 1.0;
	}
	return AveScale;
}

int AssignBlobsCD::FindNextColumn(std::vector< std::vector< int > > &SubSectionMatrix, int Row, int CurrColumn)
{
	int numbCols = (int)SubSectionMatrix[Row].size();
	if (CurrColumn < numbCols)
	{
		for (int c = CurrColumn + 1; c < numbCols; c++)
		{
			if (SubSectionMatrix[Row][c] >= 0)
				return c;
		}
		return numbCols * 3;
	}
	else
		return numbCols * 3;
}

int AssignBlobsCD::FindNextRow(std::vector< std::vector< int > >& SubSectionMatrix, int CurrentRow, int Column)
{
	int numbRows = (int)SubSectionMatrix.size();
	if (CurrentRow < numbRows)
	{
		for (int r = CurrentRow + 1; r < numbRows; r++)
		{
			if (SubSectionMatrix[r][Column] >= 0)
				return r;
		}
		return numbRows * 3;
	}
	else
		return numbRows * 3;
}

