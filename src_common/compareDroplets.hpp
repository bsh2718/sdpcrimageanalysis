#ifndef COMPAREdROPLETS_H
#define COMPAREdROPLETS_H
//! [includes]

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "physicalconstants.h"
#include "statistics.hpp"
#include "blob.hpp"
// #include "readinfofile.hpp"
// #include "segmentVector.hpp"
#include "wellExclusionRules.hpp"
#include "analysisParameters.hpp"
#include "beforeAfterMatrix.hpp"
// #include "dropletMatch.hpp"
#include "OtsuThreshold.hpp"
//! [includes]

//! [namespace]
// using namespace std;
//using namespace cv;
//! [namespace]

class RunInformation
{
	public:
		int Array;
		int Section;
		std::vector< int > NumbRuns;
		std::vector< double > Mu;
		std::vector< double > Sigma;
		int NumbRunsExcluded;
		int NumbWellsExcluded;
		
		
		RunInformation(): Array(0), Section(0), NumbRunsExcluded(0), NumbWellsExcluded(0)
		{
			NumbRuns.clear();
			Mu.clear();
			Sigma.clear();
		}
		~RunInformation(){}
		
		RunInformation(const RunInformation &Source): Array(Source.Array), Section(Source.Section),
			NumbRuns(Source.NumbRuns), Mu(Source.Mu), 
			Sigma(Source.Sigma), NumbRunsExcluded(Source.NumbRunsExcluded),
			NumbWellsExcluded(Source.NumbWellsExcluded) {}
		
		RunInformation& operator=(const RunInformation &Source)
		{
			if ( this != &Source )
			{
				Array = Source.Array;
				Section = Source.Section;
				NumbRuns = Source.NumbRuns;
				Mu = Source.Mu;
				Sigma = Source.Sigma;
				NumbRunsExcluded = Source.NumbRunsExcluded;
				NumbWellsExcluded = Source.NumbWellsExcluded;
			}
			return *this;
		}
		
		void Clear()
		{
			Array = 0;
			Section = 0;
			NumbRuns.clear();
			Mu.clear();
			Sigma.clear();
			NumbRunsExcluded = 0;
			NumbWellsExcluded = 0;
		}
};

class AfterBeforeRatioInfo
{
	public:
		bool Status;
		int Array;
		int Section;
		cv::Point SectionPosition;
		std::vector< double > Ratio;
		
		
		AfterBeforeRatioInfo(): Status(false), Array(0), Section(0), SectionPosition(cv::Point(0,0))
		{
			Ratio.resize(1);
			Ratio[0] = 1.0;
		}
		~AfterBeforeRatioInfo(){}
		
		AfterBeforeRatioInfo(const AfterBeforeRatioInfo &Source): Status(Source.Status), Array(Source.Array), Section(Source.Section),
			SectionPosition(Source.SectionPosition),
			Ratio(Source.Ratio) {}
		
		AfterBeforeRatioInfo& operator=(const AfterBeforeRatioInfo &Source)
		{
			if ( this != &Source )
			{
				Status = Source.Status;
				Array = Source.Array;
				Section = Source.Section;
				SectionPosition = Source.SectionPosition;
				Ratio = Source.Ratio;
			}
			return *this;
		}
		
		void Clear()
		{
			Status = false;
			Array = 0;
			Section = 0;
			SectionPosition = cv::Point(0,0);
			Ratio.resize(1);
			Ratio[0] = 1.0;
		}
};


class QuantitationResult
{
	public:
		int Array;
		int Section;
		int NumbDroplets;
		int NumbOccupied;
		Statistics1 FillingFraction;
		Statistics1 Shrinkage;
		double Concentration;
		double ConcSigma;
		
		
		QuantitationResult(): Array(0), Section(0), NumbDroplets(0),
			NumbOccupied(0), Concentration(0.0), ConcSigma(0.0)
		{
			FillingFraction.Clear();
			Shrinkage.Clear();
		}
		~QuantitationResult(){}
		
		QuantitationResult(const QuantitationResult &Source): Array(Source.Array), Section(Source.Section),
			NumbDroplets(Source.NumbDroplets), NumbOccupied(Source.NumbOccupied), 
			Concentration(Source.Concentration), ConcSigma(Source.ConcSigma)
		{
			FillingFraction = Source.FillingFraction;
			Shrinkage = Source.Shrinkage;
		}
		
		QuantitationResult& operator=(const QuantitationResult &Source)
		{
			if ( this != &Source )
			{
				Array = Source.Array;
				Section = Source.Section;
				NumbDroplets = Source.NumbDroplets;
				NumbOccupied = Source.NumbOccupied;
				FillingFraction = Source.FillingFraction;
				Shrinkage = Source.Shrinkage;
				Concentration = Source.Concentration;
				ConcSigma = Source.ConcSigma;
			}
			return *this;
		}
		
		void Clear()
		{
			Array = 0;
			Section = 0;
			NumbDroplets = 0;
			NumbOccupied = 0;
			FillingFraction.Clear();
			Shrinkage.Clear();
			Concentration = 0.0;
			ConcSigma = 0.0;
		}
};
void AssignStatus(std::ofstream &LogFile, int Array, int Section, cv::Size SectionDimens,
	std::vector< WellExclusionRules > ExclusionRules, 
	std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter);

void CalcAfterBeforeComparisons(std::ofstream &LogFile, int Array, int Section, cv::Size SectionDimens,
	double ShrinkageLimit, std::vector< BlobSummary > &BeforeSizingList, std::vector< BlobSummary > &AfterSizingList, 
	std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter);

int CalcBeforeAveIntenDistribution(std::ofstream &LogFile, int Array, int Section, cv::Size SectionDimens,
	std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter,
	std::vector< BlobSummary > &BeforeSizingList, double BeforeExclusionFactor, 
	std::vector< std::pair< double, int > > &AveIntenHistogram, Statistics1 &AveIntenStats, 
	double &AveIntenThreshold, int &NumbAcceptable );

bool OutputDropletAveIntensityData(std::ofstream &LogFile, std::string &BaseName, 
	int Array, int Section, cv::Size SectionDimens,  
	std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter,
	std::vector< BlobSummary > &BeforeSizingList, std::vector< double> BeforeResults, double BeforeThreshold, 
	MinMaxGroup &BeforeParameters, double BeforeExclusionFactor, 
	std::vector< std::pair< double, int > > &AveIntenHistogram, Statistics1 &AveIntenStats, 
	double AveIntenThreshold, int NumbAcceptableBeforeDroplets);
	
int CalcAfterBeforeRatioDistribution(std::ofstream &LogFile, int Array, int Section, cv::Size SectionDimens,	
	std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter,
	double PosNegRatio, double RatioDefaultDivider,
	std::vector< std::pair< double, int > > &RatioHistogram, Statistics1 &RatioStats, 
	Statistics1 &BelowStats, Statistics1 &AboveStats, AfterBeforeResults &RatioResults );

bool OutputDropletComparison(std::ofstream &LogFile, std::string BaseName,
	int Array, int Section, int ImageNumber, double Time, cv::Size SectionDimens, double WellArea, cv::Size2f WellSize2f,
	std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter,
	std::vector< BlobSummary > &BeforeSizingList, std::vector< double> BeforeResults, double BeforeThreshold, 
	MinMaxGroup &BeforeParameters, double ShrinkageLimit, double BeforeExclusionFactor, 
	std::vector< BlobSummary > &AfterSizingList, std::vector< double> AfterResults, double AfterThreshold, 
	MinMaxGroup &AfterParameters, double PosNegRatio, double RatioDefaultDivider, double RunSigmaMultiplier, 
	AfterBeforeResults &RatioResults, RunInformation &RunInfo,
	cv::Point ArraySectionShift, QuantitationResult &QResult, std::string &ErrorMsg );
	
bool OutputDropletComparisonImages(std::ofstream &LogFile, std::string BaseName,
	int Array, int Section, int ImageNumber, double Time, cv::Size SectionDimens, 
	std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter,
	std::vector< BlobSummary > &BeforeSizingList, 
	std::vector< BlobSummary > &AfterSizingList, 
	cv::Mat &BeforeImage, cv::Mat &BeforeMaskImage, cv::Mat &BeforeContourImage, cv::Mat &BeforeMap, cv::Mat &BeforeArrayCheck,
	cv::Mat &AfterImage, cv::Mat &AfterMaskImage, cv::Mat &AfterContourImage, cv::Mat &AfterMap, cv::Mat &AfterAfterCheck,
	cv::Point WellSpacing2f, bool CreateMap, std::vector< std::string > &MapText,
	bool OutputImages, std::string &ErrorMsg);
	
bool OutputDropletRatioData(std::ofstream &LogFile, std::string &BaseName, 
	int Array, int Section, int ImageNumber, double Time, cv::Size SectionDimens, double PosNegRatio, double RatioDefaultDivider,
	std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter,
	std::vector< BlobSummary > &BeforeSizingList, 
	std::vector< std::pair< double, int > > &RatioHistogram, Statistics1 &RatioStats, 
	Statistics1 &BelowStats, Statistics1 &AboveStats, AfterBeforeResults &RatioResults );


// void AssignBeforeAfter(std::ofstream &LogFile, int Array, int Section, cv::Size SectionDimens,
	// std::vector< BlobSummary > &BeforeSizingList, std::vector< BlobSummary > &AfterSizingList, 
	// std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter);

// void CheckForMismatches( std::ofstream &LogFile, int Array, int Section, cv::Size SectionDimens, cv::Size2f WellSize,
	// std::vector< BlobSummary > &BeforeSizingList, std::vector< BlobSummary > &AfterSizingList, 
	// std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter);


#endif // COMPAREdROPLETS_H