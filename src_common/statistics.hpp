////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// All of these classes calculate average and standard deviations for for a set of numbers.  There are 1, 2 and 3 
// dimensional forms of these classes.
//
// For all of these classes
// int Accumulate(---) adds a set of data to the object.  The argument list consists of 1, 2 or 3 double variables depending
// 	on the dimension of the class. It returns the number of sets accumulated.
// int Analyze() performs the analysis on the accumulated data. It returns the number of data sets analyzed.
// bool Analyzed() returns false if Analyze() has been called and false otherwise.  If after Analyze has been performed, 
// 	Accumulate() is called to add more data, then the internal variable which indicates whether Analyze() has been
// 	called is set to false.
// int Count() returns the number of sets of data accumulated.
// void Clear() - clears (erases) all the data in the class.
//
// class Statistics1 - 1 dimension
// When Accumulate(X) is called, the object updates
//	sum of X
//	sum of X^2
// When Analyze is called the object calculates the Average and Standard deviation of X.
// double Ave() returns average X
// double Std() returns standard deviation of X
// double Sum() returns (sum of X)
// double SumOfSquared() returns (sum of X^2)
// bool Results(Stat1Struct& S) If accumulated data has been analyzed, it loads the results into S and returns true.
// 	Otherwise it returns false.
//
//
// class Statistics1D - 1 dimension 
// This is similar to Statistics1, except that it also accumulates
// 	sum of X^3
// and caluclates the skew of the distribution when Analyze() is called.  In additional to the member functions of
// Statisics1, this class also has
// double Skew() - returns skew of distribution
// double SumOfCubes() - returns (sum of X^3)
// bool Results(Stat1DStruct& S) loads the results into S.
//
//
// class StatisticsXY - 2 dimensions
// int Accumulate(X, Y) loads a set of two numbers and calculates R = sqrt(X^2 + Y^2). The object will accumulate
// 	sum of X
// 	sum of X^2
// 	sum of Y
// 	sum of Y^2
// 	sum of R
// 	sum of R^2
// int Analyze() will calculate the Average and Standard deviations of X, Y and R. In the following list replace Q by X, Y or R.
// double AveQ() returns average of Q
// double StdQ() returns standard deviation of Q
// double SumQ() returns sum of Q
// double SumOfQSquared() returns sum of Q^2
//
//
// class LinearRegression - 2 dimensions
// int Accumulate(X, Y) will accumulates
// 	sum of X
// 	sum of X^2
// 	sum of Y
// 	sum of Y^2
// 	sum of X * Y
// int Analyze() will calculate the slope, Y-intercept and R value of a straight line fit to the data. In the following list
// replace Q by X or Y
// double Slope() returns the slope
// double YIntercept() returns the y-intercept
// double RValue() returns the R value for the fit
// double AveQ() returns average of Q
// double StdQ() returns standard deviation of Q
// double SumQ() returns sum of Q
// double SumOfQSquared() returns sum of Q^2
// double SumXY() returns sum of X * Y
// int Remove(X, Y) removes the pair X and Y from the sums accumulated. It returns the number of sets of data remaining in the object. 
// 	This only makes sense if the pair has previously been Accumulated.   The object has no way of checking this.  If the 
// 	arguments to Remove are not a pair of numbers previously Accumulated, then the data in the object will not be meaningful.
//
//
// class StatisticsCR - 2 dimenstions.
// This is a clone of Statistics2, except that X and Y are replaced by Col and Row (e.g. AveX() becomes AveCol(), etc.). 
// R is still present and equals sqrt(Col^2 + Row^2).
//
//
// class StatisticsXYZ - 3 dimensions.
// And extension of Statistics2 into 3 dimensions.  
// int Accumulate(X, Y, Z) will accumulate
// 	sum of X
// 	sum of X^2
// 	sum of Y
// 	sum of Y^2
// 	sum of Z 
// 	sum of Z^2
// 	sum of R
// 	sum of R^2
// where R = sqrt(X^2 + Y^2 + Z^2).
// int Analyze() will calculate the average and standard deviation of X, Y, Z and R. Replace Q by X, Y, Z or R in the following list
// double RValue() returns the R value for the fit
// double AveQ() returns average of Q
// double StdQ() returns standard deviation of Q
// double SumQ() returns sum of Q
// double SumOfQSquared() returns sum of Q^2
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef STATISTICS_H
#define STATISTICS_H 
#include <cstdlib>
#include <cmath>

// struct Stat1Struct 
// {
	// public:
		// int Count;
		// double Ave;
		// double Std;
		// double Sum;
		// double Sum2;
// };

// struct Stat1DStruct 
// {
	// public:
		// int Count;
		// double Ave;
		// double Std;
		// double Skew;
		// double Sum;
		// double Sum2;
		// double Sum3;
// };

// void CopyStat1Struct(Stat1Struct Source, Stat1Struct &Dest);
// class used to store some results from an object of class Statistics1 


class Statistics1_Vector
{
	private:
	int Numb;
	double Ave_V;
	double Std_V;
	double Sum_V;
	double Sum2_V;
	bool Analy;
	std::vector< double >::iterator it;
	
	public:
	Statistics1_Vector() :
		Numb(0), Ave_V(0.0), Std_V(0.0), Sum_V(0.0), Sum2_V(0.0), Analy(false) {}
	
	Statistics1_Vector(const Statistics1_Vector &Other): Numb(Other.Numb), Ave_V(Other.Ave_V), Std_V(Other.Std_V), 
		Sum_V(Other.Sum_V), Sum2_V(Other.Sum2_V), Analy(Other.Analy) {}

	Statistics1_Vector& operator=(const Statistics1_Vector &Other)
	{
		if ( this != &Other )
		{
			Numb = Other.Numb;
			Ave_V = Other.Ave_V;
			Std_V = Other.Std_V;
			Sum_V = Other.Sum_V;
			Sum2_V = Other.Sum2_V;
			Analy = Other.Analy;
		}
		return *this;
	}

	void Clear()
	{
		Numb = 0;
		Ave_V = 0.0;
		Std_V = 0.0;
		Sum_V = 0.0;
		Sum2_V = 0.0;
		Analy = false;
	}

	int Analyze()
	{
		if(Numb > 1 )
		{
			Ave_V = Sum_V / (double)Numb;
			Std_V = sqrt(Sum2_V * (double)Numb
					- Sum_V * Sum_V) / (double)Numb;
			Analy = true;
		}
		else if( Numb == 1 )
		{
			Ave_V = Sum_V;
			Std_V = 0.0;
			Analy = true;
		}
		else
		{
			Clear();
			Analy = false;
		}
		return Numb;
	}
	
	int Accumulate(std::vector< double > &ValV )
	{
		for( it = ValV.begin() ; it != ValV.end() ; ++it)		
		{
			Numb++;
			Sum_V += *it;
			Sum2_V += pow(*it, 2);
		}
		Analy = false;
		return Numb;
	}
	bool Analyzed() const
	{
		return Analy;
	}

	int Count() const
	{
		return Numb;
	}
	double Ave()
	{
		if ( ! Analy ) Analyze();
		return Ave_V;
	}
	double Std()
	{
		if ( ! Analy ) Analyze();
		return Std_V;
	}
	double Sum()
	{
		if ( ! Analy ) Analyze();
		return Sum_V;
	}
	double SumOfSquared()
	{
		if ( ! Analy ) Analyze();
		return Sum2_V;
	}
	
};

class Statistics1_VectorMinMax
{
	private:
	int Numb;
	double Ave_V;
	double Std_V;
	double Sum_V;
	double Sum2_V;
	double Min_V;
	double Max_V;
	bool Analy;
	std::vector< double >::iterator it;
	
	public:
	Statistics1_VectorMinMax() :
		Numb(0), Ave_V(0.0), Std_V(0.0), Sum_V(0.0), Sum2_V(0.0), Min_V(1.0E+37), Max_V(-1.0E+37), Analy(false) {}
	
	Statistics1_VectorMinMax(const Statistics1_VectorMinMax &Other): Numb(Other.Numb), Ave_V(Other.Ave_V), Std_V(Other.Std_V), 
		Sum_V(Other.Sum_V), Sum2_V(Other.Sum2_V), Min_V(Other.Min_V), Max_V(Other.Max_V), Analy(Other.Analy) {}

	Statistics1_VectorMinMax& operator=(const Statistics1_VectorMinMax &Other)
	{
		if ( this != &Other )
		{
			Numb = Other.Numb;
			Ave_V = Other.Ave_V;
			Std_V = Other.Std_V;
			Sum_V = Other.Sum_V;
			Sum2_V = Other.Sum2_V;
			Min_V = Other.Min_V;
			Max_V = Other.Max_V;
			Analy = Other.Analy;
		}
		return *this;
	}

	void Clear()
	{
		Numb = 0;
		Ave_V = 0.0;
		Std_V = 0.0;
		Sum_V = 0.0;
		Sum2_V = 0.0;
		Min_V = 1.0E+37;
		Max_V = -1.0E+37;
		Analy = false;
	}

	int Analyze()
	{
		if(Numb > 1 )
		{
			Ave_V = Sum_V / (double)Numb;
			Std_V = sqrt(Sum2_V * (double)Numb
					- Sum_V * Sum_V) / (double)Numb;
			Analy = true;
		}
		else if( Numb == 1 )
		{
			Ave_V = Sum_V;
			Std_V = 0.0;
			Analy = true;
		}
		else
		{
			Clear();
			Analy = false;
		}
		return Numb;
	}
	
	int Accumulate(std::vector< double > &ValV )
	{
		for( it = ValV.begin() ; it != ValV.end() ; ++it)		
		{
			Numb++;
			Sum_V += *it;
			Sum2_V += pow(*it, 2);
			if ( *it > Max_V )
				Max_V = *it;
			if ( *it < Min_V )
				Min_V = *it;
		}
		Analy = false;
		return Numb;
	}
	bool Analyzed() const
	{
		return Analy;
	}

	int Count() const
	{
		return Numb;
	}
	double Ave()
	{
		if ( ! Analy ) Analyze();
		return Ave_V;
	}
	double Std()
	{
		if ( ! Analy ) Analyze();
		return Std_V;
	}
	double Sum()
	{
		if ( ! Analy ) Analyze();
		return Sum_V;
	}
	double SumOfSquared()
	{
		if ( ! Analy ) Analyze();
		return Sum2_V;
	}
	
	double Min()
	{
		return Min_V;
	}
	
	double Max()
	{
		return Max_V;
	}
};

class Statistics1
{
	private:
	int Numb;
	double Ave_V;
	double Std_V;
	double Sum_V;
	double Sum2_V;
	bool Analy;
	std::vector< double >::iterator it;
	
	public:
	Statistics1() :
		Numb(0), Ave_V(0.0), Std_V(0.0), Sum_V(0.0), Sum2_V(0.0), Analy(false) {}
	
	Statistics1(const Statistics1 &Other): Numb(Other.Numb), Ave_V(Other.Ave_V), Std_V(Other.Std_V), 
		Sum_V(Other.Sum_V), Sum2_V(Other.Sum2_V), Analy(Other.Analy) {}

	Statistics1& operator=(const Statistics1 &Other)
	{
		if ( this != &Other )
		{
			Numb = Other.Numb;
			Ave_V = Other.Ave_V;
			Std_V = Other.Std_V;
			Sum_V = Other.Sum_V;
			Sum2_V = Other.Sum2_V;
			Analy = Other.Analy;
		}
		return *this;
	}

	void Clear()
	{
		Numb = 0;
		Ave_V = 0.0;
		Std_V = 0.0;
		Sum_V = 0.0;
		Sum2_V = 0.0;
		Analy = false;
	}

	int Analyze()
	{
		if(Numb > 1 )
		{
			Ave_V = Sum_V / (double)Numb;
			double tmp = Sum2_V * (double)Numb - Sum_V * Sum_V;
			if ( tmp > 0.0 )
				Std_V = sqrt(tmp) / (double)Numb;
			else
				Std_V = 0.0;
			Analy = true;
		}
		else if( Numb == 1 )
		{
			Ave_V = Sum_V;
			Std_V = 0.0;
			Analy = true;
		}
		else
		{
			Clear();
			Analy = false;
		}
		return Numb;
	}
	int Accumulate(double Val)
	{
		Numb++;
		Sum_V += Val;
		Sum2_V += Val * Val;
		Analy = false;
		return Numb;
	}
	int Accumulate(std::vector< double > &ValV )
	{
		for( it = ValV.begin() ; it != ValV.end() ; ++it)		
		{
			Numb++;
			Sum_V += *it;
			Sum2_V += pow(*it, 2);
		}
		Analy = false;
		return Numb;
	}
	bool Analyzed() const
	{
		return Analy;
	}

	int Count() const
	{
		return Numb;
	}
	double Ave()
	{
		if ( ! Analy ) Analyze();
		return Ave_V;
	}
	double Std()
	{
		if ( ! Analy ) Analyze();
		return Std_V;
	}
	double Sum()
	{
		if ( ! Analy ) Analyze();
		return Sum_V;
	}
	double SumOfSquared()
	{
		if ( ! Analy ) Analyze();
		return Sum2_V;
	}
	
	Statistics1& operator=(Statistics1_Vector &Other)
	{
		Numb = Other.Count();
		Ave_V = Other.Ave();
		Std_V = Other.Std();
		Sum_V = Other.Sum();
		Sum2_V = Other.SumOfSquared();
		Analy = Other.Analyzed();
		return *this;
	}
	
	Statistics1& operator=(Statistics1_VectorMinMax &Other)
	{
		Numb = Other.Count();
		Ave_V = Other.Ave();
		Std_V = Other.Std();
		Sum_V = Other.Sum();
		Sum2_V = Other.SumOfSquared();
		Analy = Other.Analyzed();
		return *this;
	}
};

class Stat1Results
{
	public:
		int Count;
		double Ave;
		double Std;
		double Sum;
		double Sum2;

	Stat1Results(): Count(0), Ave(0.0), Std(0.0), Sum(0.0), Sum2(0.0) {}
	~Stat1Results(){}

	Stat1Results& operator= ( const Stat1Results &Source )
	{
		if ( this !=&Source )
		{
			Count = Source.Count;
			Ave = Source.Ave;
			Std = Source.Std;
			Sum = Source.Sum;
			Sum2 = Source.Sum2;
		}
		return *this;
	}
	
	Stat1Results( const Stat1Results &Source ): Count(Source.Count),
		Ave(Source.Ave), Std(Source.Std), Sum(Source.Sum),
		Sum2(Source.Sum2)
	{}

	Stat1Results& operator= ( Statistics1 &Source )
	{
		Count = Source.Count();
		Ave = Source.Ave();
		Std = Source.Std();
		Sum = Source.Sum();
		Sum2 = Source.SumOfSquared();
		return *this;
	}
	
	Stat1Results& operator= ( Statistics1_Vector &Source )
	{
		Count = Source.Count();
		Ave = Source.Ave();
		Std = Source.Std();
		Sum = Source.Sum();
		Sum2 = Source.SumOfSquared();
		return *this;
	}
	
	Stat1Results& operator= ( Statistics1_VectorMinMax &Source )
	{
		Count = Source.Count();
		Ave = Source.Ave();
		Std = Source.Std();
		Sum = Source.Sum();
		Sum2 = Source.SumOfSquared();
		return *this;
	}
	
	void Clear()
	{
		Count = 0;
		Ave = 0.0;
		Std = 0.0;
		Sum = 0.0;
		Sum2 = 0.0;
	}
};

class Statistics1D
{
	private:
	int Numb;
	double Ave_V;
	double Std_V;
	double Skew_V;
	double Sum_V;
	double Sum2_V;
	double Sum3_V;
	bool Analy;

	public:
	Statistics1D() :
		Numb(0), Ave_V(0.0), Std_V(0.0), Skew_V(0.0), Sum_V(0.0), Sum2_V(0.0), Sum3_V(0.0), Analy(false) {}
	
	Statistics1D(Statistics1D &Other): Numb(Other.Numb), Ave_V(Other.Ave_V), Std_V(Other.Std_V), Skew_V(Other.Skew_V),
		Sum_V(Other.Sum_V), Sum2_V(Other.Sum2_V), Sum3_V(Other.Sum3_V), Analy(Other.Analy) {}

	Statistics1D& operator=(const Statistics1D &Other)
	{
		if ( this != &Other )
		{
			Numb = Other.Numb;
			Ave_V = Other.Ave_V;
			Std_V = Other.Std_V;
			Skew_V = Other.Skew_V;
			Sum_V = Other.Sum_V;
			Sum2_V = Other.Sum2_V;
			Sum3_V = Other.Sum3_V;
			Analy = Other.Analy;
		}
		return *this;
	}

	void Clear()
	{
		Numb = 0;
		Ave_V = 0.0;
		Std_V = 0.0;
		Skew_V = 0.0;
		Sum_V = 0.0;
		Sum2_V = 0.0;
		Sum3_V = 0.0;
		Analy = false;
	}

	int Analyze()
	{
		if(Numb > 1 )
		{
			Ave_V = Sum_V / (double)Numb;
			Std_V = sqrt( (Sum2_V * (double)Numb
					- Sum_V * Sum_V) / ( (double)Numb * (Numb - 1.0) ) );
			if ( Numb == 2 )
				Skew_V = 0.0;
			else
			{
				Skew_V = ( Sum3_V - 3.0 * Ave_V * Sum2_V + 3.0 * pow(Ave_V, 2) * Sum3_V - Numb * pow(Ave_V,3) )
					/ (Numb * pow(Std_V, 3));
			}
			Analy = true;
		}
		else if( Numb == 1 )
		{
			Ave_V = Sum_V;
			Std_V = 0.0;
			Skew_V = 0.0;
			Analy = true;
		}
		else
		{
			Clear();
			Analy = false;
		}
		return Numb;
	}
	int Accumulate(double Val)
	{
		Numb++;
		Sum_V += Val;
		Sum2_V += Val * Val;
		Sum3_V += Val * Val * Val;
		Analy = false;
		return Numb;
	}
	bool Analyzed() const
	{
		return Analy;
	}

	int Count() const
	{
		return Numb;
	}
	double Ave()
	{
		if ( ! Analy ) Analyze();
		return Ave_V;
	}
	double Std()
	{
		if ( ! Analy ) Analyze();
		return Std_V;
	}
	double Skew()
	{
		if ( ! Analy ) Analyze();
		return Skew_V;
	}
	double Sum()
	{
		if ( ! Analy ) Analyze();
		return Sum_V;
	}
	double SumOfSquared()
	{
		if ( ! Analy ) Analyze();
		return Sum2_V;
	}
	double SumOfCubes()
	{
		if ( ! Analy ) Analyze();
		return Sum3_V;
	}
	
	// bool Results( Stat1DStruct &Output)
	// {
		// if ( Analy )
		// {
			// Output.Count = Numb;
			// Output.Ave = Ave_V;
			// Output.Std = Std_V;
			// Output.Skew = Skew_V;
			// Output.Sum = Sum_V;
			// Output.Sum2 = Sum2_V;
			// Output.Sum3 = Sum3_V;
		// }
		// else
		// {
			// Output.Count = 0;
		// }
		// return Analy;
	// }
};

class StatisticsXY
{
	private:
	int Numb;
	double Ave_X;
	double Std_X;
	double Sum_X;
	double Sum2_X;
	double Ave_Y;
	double Std_Y;
	double Sum_Y;
	double Sum2_Y;
	double Ave_R;
	double Std_R;
	double Sum_R;
	double Sum2_R;
	bool Analy;

	public:
	StatisticsXY() :
		Numb(0),
		Ave_X(0.0), Std_X(0.0), Sum_X(0.0), Sum2_X(0.0),
		Ave_Y(0.0), Std_Y(0.0), Sum_Y(0.0), Sum2_Y(0.0),
		Ave_R(0.0), Std_R(0.0), Sum_R(0.0), Sum2_R(0.0),
		Analy(false)
	{
	}

	void Clear()
	{
		Numb = 0;
		Ave_X = 0.0;
		Std_X = 0.0;
		Ave_Y = 0.0;
		Std_Y = 0.0;
		Ave_R = 0.0;
		Std_R = 0.0;
		Sum_X = 0.0;
		Sum2_X = 0.0;
		Sum_Y = 0.0;
		Sum2_Y = 0.0;
		Sum_R = 0.0;
		Sum2_R = 0.0;
		Analy = false;
	}

	int Analyze()
	{
		if(Numb > 1 )
		{
			Ave_X = Sum_X / (double)Numb;
			Std_X = sqrt(Sum2_X * (double)Numb
					- Sum_X * Sum_X) / (double)Numb;
			Ave_Y = Sum_Y / (double)Numb;
			Std_Y = sqrt(Sum2_Y * (double)Numb
					- Sum_Y * Sum_Y) / (double)Numb;
			Ave_R = Sum_R / (double)Numb;
			Std_R = sqrt(Sum2_R * (double)Numb
					- Sum_R * Sum_R) / (double)Numb;
			Analy = true;
		}
		else if( Numb == 1 )
		{
			Ave_X = Sum_X;
			Std_X = 0.0;
			Ave_Y = Sum_Y;
			Std_Y = 0.0;
			Ave_R = Sum_R;
			Std_R = 0.0;
			Analy = true;
		}
		else
		{
			Clear();
			Analy = false;
		}
		return Numb;
	}
	int Accumulate(double X, double Y)
	{
		Numb++;
		Sum_X += X;
		Sum2_X += X * X;
		Sum_Y += Y;
		Sum2_Y += Y * Y;
		double tmp = X * X + Y * Y;
		Sum2_R += tmp;
		Sum_R += sqrt(tmp);
		Analy = false;
		return Numb;
	}
	bool Analyzed() const
	{
		return Analy;
	}

	int Count() const
	{
		return Numb;
	}
	double AveX()
	{
		if ( ! Analy ) Analyze();
		return Ave_X;
	}
	double StdX()
	{
		if ( ! Analy ) Analyze();
		return Std_X;
	}
	double AveY()
	{
		if ( ! Analy ) Analyze();
		return Ave_Y;
	}
	double StdY()
	{
		if ( ! Analy ) Analyze();
		return Std_Y;
	}
	double AveR()
	{
		if ( ! Analy ) Analyze();
		return Ave_R;
	}
	double StdR()
	{
		if ( ! Analy ) Analyze();
		return Std_R;
	}
	double SumX()
	{
		if ( ! Analy ) Analyze();
		return Sum_X;
	}
	double SumOfXSquared()
	{
		if ( ! Analy ) Analyze();
		return Sum2_X;
	}
	double SumY()
	{
		if ( ! Analy ) Analyze();
		return Sum_Y;
	}
	double SumOfYSquared()
	{
		if ( ! Analy ) Analyze();
		return Sum2_Y;
	}
	double SumR()
	{
		if ( ! Analy ) Analyze();
		return Sum_R;
	}
	double SumOfRSquared()
	{
		if ( ! Analy ) Analyze();
		return Sum2_R;
	}
};

class LinearRegression
{
	private:
	int Numb;
	double Ave_X;
	double Std_X;
	double Sum_X;
	double Sum2_X;
	double Sum_XY;
	double Ave_Y;
	double Std_Y;
	double Sum_Y;
	double Sum2_Y;
	double Slope_L;
	double Y_Intercept;
	double R_Value;
	bool Analy;

	public:
	LinearRegression() :
		Numb(0),
		Ave_X(0.0), Std_X(0.0), Sum_X(0.0), Sum2_X(0.0), Sum_XY(0.0),
		Ave_Y(0.0), Std_Y(0.0), Sum_Y(0.0), Sum2_Y(0.0),
		Slope_L(0.0), Y_Intercept(0.0), R_Value(0.0),
		Analy(false)
	{
	}

	void Clear()
	{
		Numb = 0;
		Ave_X = 0.0;
		Std_X = 0.0;
		Ave_Y = 0.0;
		Std_Y = 0.0;
		Slope_L = 0.0;
		Y_Intercept = 0.0;
		R_Value = 0.0;
		Sum_X = 0.0;
		Sum2_X = 0.0;
		Sum_Y = 0.0;
		Sum2_Y = 0.0;
		Sum_XY = 0.0;
		Analy = false;
	}

	int Analyze()
	{
		if(Numb > 1 )
		{
			Ave_X = Sum_X / (double)Numb;
			Std_X = sqrt(Sum2_X * (double)Numb
					- Sum_X * Sum_X) / (double)Numb;
			Ave_Y = Sum_Y / (double)Numb;
			Std_Y = sqrt(Sum2_Y * (double)Numb
					- Sum_Y * Sum_Y) / (double)Numb;
			double tmp = (Numb * Sum_XY - Sum_X * Sum_Y);
			double tmp2 = (Numb * Sum2_X - pow(Sum_X,2));
			Slope_L = tmp/tmp2;
			Y_Intercept = Ave_Y - Slope_L * Ave_X;
			if ( fabs(Slope_L) < 1.0E-12)
				R_Value = 0.0;
			else
				R_Value = tmp / sqrt(tmp2 * (Numb * Sum2_Y - pow(Sum_Y,2)) );
			Analy = true;
		}
		else if( Numb == 1 )
		{
			Ave_X = Sum_X;
			Std_X = 0.0;
			Ave_Y = Sum_Y;
			Std_Y = 0.0;
			Slope_L = 0.0;
			Y_Intercept = 0.0;
			R_Value = 0.0;
			Analy = true;
		}
		else
		{
			Clear();
			Analy = false;
		}
		return Numb;
	}
	int Accumulate(double X, double Y)
	{
		Numb++;
		Sum_X += X;
		Sum2_X += X * X;
		Sum_Y += Y;
		Sum2_Y += Y * Y;
		Sum_XY += X * Y;
		Analy = false;
		return Numb;
	}
	
	bool Solve(double X, double &Y)
	{
		if ( Numb > 2 )
		{
			if (!Analy)
				Analyze();
			Y = Slope_L * X + Y_Intercept;
			return true;
		}
		else
		{
			Y = 0.0;
			return false;
		}
	}
	
	int Remove(double X, double Y)
	{
		Numb--;
		Sum_X -= X;
		Sum2_X -= X * X;
		Sum_Y -= Y;
		Sum2_Y -= Y * Y;
		Sum_XY -= X * Y;
		Analy = false;
		return Numb;
	}
	
	bool Analyzed() const
	{
		return Analy;
	}

	int Count() const
	{
		return Numb;
	}
	double AveX()
	{
		if ( ! Analy ) Analyze();
		return Ave_X;
	}
	double StdX()
	{
		if ( ! Analy ) Analyze();
		return Std_X;
	}
	double AveY()
	{
		if ( ! Analy ) Analyze();
		return Ave_Y;
	}
	double StdY()
	{
		if ( ! Analy ) Analyze();
		return Std_Y;
	}
	double Slope()
	{
		if ( ! Analy ) Analyze();
		return Slope_L;
	}
	double YIntercept()
	{
		if ( ! Analy ) Analyze();
		return Y_Intercept;
	}
	double RValue()
	{
		if ( ! Analy ) Analyze();
		return R_Value;
	}
	double SumX()
	{
		if ( ! Analy ) Analyze();
		return Sum_X;
	}
	double SumOfXSquared()
	{
		if ( ! Analy ) Analyze();
		return Sum2_X;
	}
	double SumY()
	{
		if ( ! Analy ) Analyze();
		return Sum_Y;
	}
	double SumOfYSquared()
	{
		if ( ! Analy ) Analyze();
		return Sum2_Y;
	}
	double SumXY()
	{
		if ( ! Analy ) Analyze();
		return Sum_XY;
	}
};

class StatisticsCR
{
	private:
	int Numb;
	double Ave_Col;
	double Std_Col;
	double Sum_Col;
	double Sum2_Col;
	double Ave_Row;
	double Std_Row;
	double Sum_Row;
	double Sum2_Row;
	double Ave_R;
	double Std_R;
	double Sum_R;
	double Sum2_R;
	bool Analy;

	public:
	StatisticsCR() :
		Numb(0),
		Ave_Col(0.0), Std_Col(0.0), Sum_Col(0.0), Sum2_Col(0.0),
		Ave_Row(0.0), Std_Row(0.0), Sum_Row(0.0), Sum2_Row(0.0),
		Ave_R(0.0), Std_R(0.0), Sum_R(0.0), Sum2_R(0.0),
		Analy(false)
	{
	}

	void Clear()
	{
		Numb = 0;
		Ave_Col = 0.0;
		Std_Col = 0.0;
		Ave_Row = 0.0;
		Std_Row = 0.0;
		Ave_R = 0.0;
		Std_R = 0.0;
		Sum_Col = 0.0;
		Sum2_Col = 0.0;
		Sum_Row = 0.0;
		Sum2_Row = 0.0;
		Sum_R = 0.0;
		Sum2_R = 0.0;
		Analy = false;
	}

	int Analyze()
	{
		if(Numb > 1 )
		{
			Ave_Col = Sum_Col / (double)Numb;
			Std_Col = sqrt(Sum2_Col * (double)Numb
					- Sum_Col * Sum_Col) / (double)Numb;
			Ave_Row = Sum_Row / (double)Numb;
			Std_Row = sqrt(Sum2_Row * (double)Numb
					- Sum_Row * Sum_Row) / (double)Numb;
			Ave_R = Sum_R / (double)Numb;
			Std_R = sqrt(Sum2_R * (double)Numb
					- Sum_R * Sum_R) / (double)Numb;
			Analy = true;
		}
		else if( Numb == 1 )
		{
			Ave_Col = Sum_Col;
			Std_Col = 0.0;
			Ave_Row = Sum_Row;
			Std_Row = 0.0;
			Ave_R = Sum_R;
			Std_R = 0.0;
			Analy = true;
		}
		else
		{
			Clear();
			Analy = false;
		}
		return Numb;
	}
	int Accumulate(double Col, double Row)
	{
		Numb++;
		Sum_Col += Col;
		Sum2_Col += Col * Col;
		Sum_Row += Row;
		Sum2_Row += Row * Row;
		double tmp = Col * Col + Row * Row;
		Sum2_R += tmp;
		Sum_R += sqrt(tmp);
		Analy = false;
		return Numb;
	}
	bool Analyzed() const
	{
		return Analy;
	}

	int Count() const
	{
		return Numb;
	}
	double AveCol()
	{
		if ( ! Analy ) Analyze();
		return Ave_Col;
	}
	double StdCol()
	{
		if ( ! Analy ) Analyze();
		return Std_Col;
	}
	double AveRow()
	{
		if ( ! Analy ) Analyze();
		return Ave_Row;
	}
	double StdRow()
	{
		if ( ! Analy ) Analyze();
		return Std_Row;
	}
	double AveR()
	{
		if ( ! Analy ) Analyze();
		return Ave_R;
	}
	double StdR()
	{
		if ( ! Analy ) Analyze();
		return Std_R;
	}
	double SumCol()
	{
		if ( ! Analy ) Analyze();
		return Sum_Col;
	}
	double SumOfColSquared()
	{
		if ( ! Analy ) Analyze();
		return Sum2_Col;
	}
	double SumRow()
	{
		if ( ! Analy ) Analyze();
		return Sum_Row;
	}
	double SumOfRowSquared()
	{
		if ( ! Analy ) Analyze();
		return Sum2_Row;
	}
	double SumR()
	{
		if ( ! Analy ) Analyze();
		return Sum_R;
	}
	double SumOfRSquared()
	{
		if ( ! Analy ) Analyze();
		return Sum2_R;
	}
};


class StatisticsXYZ
{
	private:
	int Numb;
	double Ave_X;
	double Std_X;
	double Sum_X;
	double Sum2_X;
	double Ave_Y;
	double Std_Y;
	double Sum_Y;
	double Sum2_Y;
	double Ave_Z;
	double Std_Z;
	double Sum_Z;
	double Sum2_Z;
	double Ave_R;
	double Std_R;
	double Sum_R;
	double Sum2_R;
	bool Analy;

	public:
	StatisticsXYZ() :
		Numb(0),
		Ave_X(0.0), Std_X(0.0), Sum_X(0.0), Sum2_X(0.0),
		Ave_Y(0.0), Std_Y(0.0), Sum_Y(0.0), Sum2_Y(0.0),
		Ave_Z(0.0), Std_Z(0.0), Sum_Z(0.0), Sum2_Z(0.0),
		Ave_R(0.0), Std_R(0.0), Sum_R(0.0), Sum2_R(0.0),
		Analy(false)
	{
	}

	void Clear()
	{
		Numb = 0;
		Ave_X = 0.0;
		Std_X = 0.0;
		Ave_Y = 0.0;
		Std_Y = 0.0;
		Ave_Z = 0.0;
		Std_Z = 0.0;
		Ave_R = 0.0;
		Std_R = 0.0;
		Sum_X = 0.0;
		Sum2_X = 0.0;
		Sum_Y = 0.0;
		Sum2_Y = 0.0;
		Sum_Z = 0.0;
		Sum2_Z = 0.0;
		Sum_R = 0.0;
		Sum2_R = 0.0;
		Analy = false;
	}

	int Analyze()
	{
		if(Numb > 1 )
		{
			Ave_X = Sum_X / (double)Numb;
			Std_X = sqrt(Sum2_X * (double)Numb
					- Sum_X * Sum_X) / (double)Numb;
			Ave_Y = Sum_Y / (double)Numb;
			Std_Y = sqrt(Sum2_Y * (double)Numb
					- Sum_Y * Sum_Y) / (double)Numb;
			Ave_Z = Sum_Z / (double)Numb;
			Std_Z = sqrt(Sum2_Z * (double)Numb
					- Sum_Z * Sum_Z) / (double)Numb;
			Ave_R = Sum_R / (double)Numb;
			Std_R = sqrt(Sum2_R * (double)Numb
					- Sum_R * Sum_R) / (double)Numb;
			Analy = true;
		}
		else if( Numb == 1 )
		{
			Ave_X = Sum_X;
			Std_X = 0.0;
			Ave_Y = Sum_Y;
			Std_Y = 0.0;
			Ave_Z = Sum_Z;
			Std_Z = 0.0;
			Ave_R = Sum_R;
			Std_R = 0.0;
			Analy = true;
		}
		else
		{
			Clear();
			Analy = false;
		}
		return Numb;
	}
	int Accumulate(double X, double Y, double Z)
	{
		Numb++;
		Sum_X += X;
		Sum2_X += X * X;
		Sum_Y += Y;
		Sum2_Y += Y * Y;
		Sum_Z += Z;
		Sum2_Z += Z * Z;
		double tmp = X * X + Y * Y + Z * Z;
		Sum2_R += tmp;
		Sum_R += sqrt(tmp);
		Analy = false;
		return Numb;
	}
	bool Analyzed() const
	{
		return Analy;
	}

	int Count() const
	{
		return Numb;
	}
	double AveX()
	{
		if ( ! Analy ) Analyze();
		return Ave_X;
	}
	double StdX()
	{
		if ( ! Analy ) Analyze();
		return Std_X;
	}
	double AveY()
	{
		if ( ! Analy ) Analyze();
		return Ave_Y;
	}
	double StdY()
	{
		if ( ! Analy ) Analyze();
		return Std_Y;
	}
	double AveZ()
	{
		if ( ! Analy ) Analyze();
		return Ave_Z;
	}
	double StdZ()
	{
		if ( ! Analy ) Analyze();
		return Std_Z;
	}
	double AveR()
	{
		if ( ! Analy ) Analyze();
		return Ave_R;
	}
	double StdR()
	{
		if ( ! Analy ) Analyze();
		return Std_R;
	}
	double SumX()
	{
		if ( ! Analy ) Analyze();
		return Sum_X;
	}
	double SumOfXSquared()
	{
		if ( ! Analy ) Analyze();
		return Sum2_X;
	}
	double SumY()
	{
		if ( ! Analy ) Analyze();
		return Sum_Y;
	}
	double SumOfYSquared()
	{
		if ( ! Analy ) Analyze();
		return Sum2_Y;
	}
	double SumZ()
	{
		if ( ! Analy ) Analyze();
		return Sum_Z;
	}
	double SumOfZSquared()
	{
		if ( ! Analy ) Analyze();
		return Sum2_Z;
	}
	double SumR()
	{
		if ( ! Analy ) Analyze();
		return Sum_R;
	}
	double SumOfRSquared()
	{
		if ( ! Analy ) Analyze();
		return Sum2_R;
	}
};


#endif  // STATISTICS_H 
