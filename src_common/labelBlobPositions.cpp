/////////////////////////////////////////////////////////////////////////////////////////////////
// void AssignBlobs(std::ofstream &LogFile, std::vector< double > &Results, cv::Mat &CoordImage, 
//	std::vector< Blob > &BlobList, std::vector< Blob > &OtherBlobList, 
//	std::vector< Blob > &BadBlobList,std::vector< Blob > &AllBlobs )
//
// For each blob in BlobList, OtherBlobList and AllBlobs, the procedure uses 
// 		bool DecodeBlobPosition(cv::Mat &CoordImage, cv::Size ImageLimits, Blob &Trial )
// to determine which blobs can be assigned to a well.  If a blob can be assigned to a well,
// it sets the blob's member variables Array, Section, and SectionPosition to the values of the
// well.  Otherwise, it sets them to zero.
//
//     input
// LogFile - 
// Results - vector<double> (X-Offset, Y-Offset, Angle, Scale). X-Offset, Y-Offset and Scale are
//		used to scale and shift the RotatedCenter2f of each blob to obtain a DeviceCenter.  The 
//		result is used to determine if the blob can be assigned to a well.
// CoordImage - RGB image which contains information about the area of each well and which regions
//		should not contain blobs.  The value in CoordImage at DeviceCenter indicates if the blob
//		is within a well.
// BlobList - List of blobs acceptable for quantitation.  Blobs which are also assigned to a well
//		will have the values of Array, Section and SectionPosition set accordingly.
// OtherBlobList - List of blobs acceptable for quantitation.  Blobs which are also assigned to a
//		well will have the values of Array, Section and SectionPosition set accordingly.
// BadBlobList - not used
// AllBlobs - List of all the blobs in BlobList, OtherBlobList, and BadBlobList. Blobs which are 
//		also assigned to a well will have the values of Array, Section and SectionPosition set 
//		accordingly.
//
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "statistics.hpp"
#include "blob.hpp"
//#include "searchArea.hpp"
#include "readinfofile.hpp"
#include "writeBlobFile.hpp"

#include "labelBlobPositions.hpp"

void LabelBlobPositions(std::ofstream &LogFile, std::vector< double > &Results, cv::Mat &CoordImage, 
	std::vector< Blob > &BlobList, std::string Label )
{
	double shiftX = Results[0];
	double shiftY = Results[1];
	double scale = Results[3];
	int onImage = 0;
	int offImage = 0;
	int assigned = 0;
	cv::Size imageLimits;
	imageLimits.width = CoordImage.cols - 2;
	imageLimits.height = CoordImage.rows - 2;
	// std::cout << "ListSizes: " << BlobList.size() << " / " << OtherBlobList.size() <<  " / " << BadBlobList.size() << std::endl;
	// std::cout << "Results[0/1/2/3] " << shiftX << " / " << shiftY << " / " << Results[2] << " / " << scale << std::endl;
	// std::cout << "Image Size " << imageLimits.width << " / " << imageLimits.height << std::endl;
	
	for ( int n = 0 ; n < BlobList.size() ; n++ )
	{
		if ( BlobList[n].ApplyShiftScale(shiftX, shiftY, scale, imageLimits ) )
		{
			onImage++;
			if ( DecodeBlobPosition(CoordImage, imageLimits, BlobList[n] ) )
				assigned++;
		}
		else
			offImage++;
	}
	LogFile << "\nAssignment of " << Label << ", Total Number of Blobs: " << BlobList.size() << ", Number assigned: " << assigned << "\n";
	LogFile << "                        Number On Image: " << onImage << ", Number Off Image: " << offImage << std::endl;
}

// void AssignBlobs2(std::ofstream &LogFile, std::vector< double > &Results, cv::Mat &CoordImage, 
	// std::vector< Blob > &BlobList, std::string Label )
// {
	// double shiftX = Results[0];
	// double shiftY = Results[1];
	// double scale = Results[3];
	// int onImage = 0;
	// int offImage = 0;
	// int assigned = 0;
	// cv::Size imageLimits;
	// imageLimits.width = CoordImage.cols - 2;
	// imageLimits.height = CoordImage.rows - 2;	
	// for ( int n = 0 ; n < BlobList.size() ; n++ )
	// {
		// if ( BlobList[n].ApplyShiftScale(shiftX, shiftY, scale, imageLimits ) )
		// {
			// onImage++;
			// if ( DecodeBlobPosition2(CoordImage, imageLimits, BlobList[n] ) )
				// assigned++;
		// }
		// else
			// offImage++;
	// }
	// LogFile << "\nAssignment of " << Label << ", Total Number of Blobs: " << BlobList.size() << ", Number assigned: " << assigned << "\n";
	// LogFile << "                        Number On Image: " << onImage << ", Number Off Image: " << offImage << std::endl;
// }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Applies alignment to centroid and determines if the resulting position is in a well, a guard region, or off the image.
// If shifted centroid is not in a well, then Array, Section and SectionPosition are all set to zero.
//
//			Input
// CoordImage - 24 bit color image of the device. If a pixel is in a well, then its color
//		indicates the Array, Section, Column and Row of that well.  If a pixel is in a guard region, it
//		indicates which guard region(s) it is in.
// ImageLimits - size of CoordImage
// Trial - Object of Blob whose well coordinates are to be decoded.
// ShiftX - 
// ShiftY - 
// Scale - 
// 
//			Output
// Array - 
// Section -
// SectionPosition - 
// XGuardError - true if shifted centroid is in an X Guard region, false otherwise
// YGuardError - true if shifted centroid is in an Y Guard region, false otherwise
// Return Value - is 0 or 9 if the shifted centroid is still in the image and 1 - 8 if it is off image.
//		Return value codes 
//			0 - aligned with well
//			9 - in image, but not aligned with well. might be in a guard error region.
//			1-8 - Off image.
//
//			+-----+-----+-----+
//			|     |     |     |
//			|  1  |  2  |  3  |
//			|     |     |     |
//			+-----*-----*-----+
//			|     |     |     |
//			|  8  | 0,9 |  4  |
//			|     |     |     |
//			+-----*-----*-----+
//			|     |     |     |
//			|  7  |  6  |  5  |
//			|     |     |     |
//			+-----+-----+-----+
//		(x,y) = (0,0) is upper left corner in this diagram
//		* mark the corners of the CoordImage, regions 1-8 are off the Coord image.
//

int DecodeBlobPosition(cv::Mat &CoordImage, cv::Size ImageLimits, Blob &Trial, double ShiftX, double ShiftY, double Scale, int &Array, int &Section, 
		cv::Point &SectionPosition, bool &XGuardError, bool &YGuardError )
{
	cv::Point result;
	bool res = Trial.CalcShiftScale(ShiftX, ShiftY, Scale, ImageLimits, result);
	// std::cout << "After Trial.CalcShiftScale " << std::endl;
	if ( res )
	{
		// int iTmp = 0;
		cv::Vec3b cValue = CoordImage.at<cv::Vec3b>(result);
		// std::cout << "Inside DecodeCentroid - 2" << std::endl;
		XGuardError = false;
		YGuardError = false;
		if ( cValue[0] > 0 )
		{
			if ( cValue[2] > 0 && cValue[2] < 128 )
			{
				Section = (int)floor(cValue[0]/10);
				Array = cValue[0] % 10;
				SectionPosition.y = cValue[1];
				SectionPosition.x = (int)cValue[2];
				return 0;
			}
			else
			{
				Section = 0;
				Array = 0;
				SectionPosition.x = 0;
				SectionPosition.y = 0;
				return 9;
			}
		}
		else
		{
			Section = 0;
			Array = 0;
			SectionPosition.x = 0;
			SectionPosition.y = 0;
			if ( cValue[1] > 0 )
				XGuardError = true;
			if ( cValue[2] > 0 )
				YGuardError = true;
			return 9;
		}
	}
	else
	{
		// std::cout << "Inside DecodeCentroid - 3" << std::endl;
		Section = 0;
		Array = 0;
		SectionPosition.x = 0;
		SectionPosition.y = 0;
		XGuardError = false;
		YGuardError = false;
		if ( result.y < 1 )
		{
			if ( result.x < 1 )
				return 1;
			else if ( result.x > ImageLimits.width )
				return 3;
			else
				return 2;
		}
		else if ( result.y > ImageLimits.height )
		{
			if ( result.x < 1 )
				return 7;
			else if ( result.x > ImageLimits.width )
				return 5;
			else
				return 6;
		}
		else if ( result.x < 1 )
			return 8;
		else if ( result.x > ImageLimits.width )
			return 4;
		else
			return 9;
	}
}

// int DecodeBlobPosition2(cv::Mat &CoordImage, cv::Size ImageLimits, Blob &Trial, double ShiftX, double ShiftY, double Scale, int &Array, int &Section, 
		// cv::Point &SectionPosition, bool &XGuardError, bool &YGuardError )
// {
	// cv::Point result;
	// bool res = Trial.CalcShiftScale(ShiftX, ShiftY, Scale, ImageLimits, result);

	// if ( res )
	// {

		// cv::Vec3b cValue = CoordImage.at<cv::Vec3b>(result);

		// XGuardError = false;
		// YGuardError = false;
		// if ( cValue[0] > 0 )
		// {
			// Section = (int)floor(cValue[0]/10);
			// Array = cValue[0] % 10;
			// SectionPosition.y = cValue[1];
			// SectionPosition.x = (int)cValue[2] % 128;
			// return 0;
		// }
		// else
		// {
			// Section = 0;
			// Array = 0;
			// SectionPosition.x = 0;
			// SectionPosition.y = 0;
			// if ( cValue[1] > 0 )
				// XGuardError = true;
			// if ( cValue[2] > 0 )
				// YGuardError = true;
			// return 9;
		// }
	// }
// }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Applies alignment to centroid and determines if the resulting position is in a well, a guard region, or off the image.
// If shifted centroid is not in a well, then Array, Section and SectionPosition are all set to zero.
//
//			Input
// CoordImage - 24 bit color image of the device. If a pixel is in a well, then its color
//		indicates the Array, Section, Column and Row of that well.  If a pixel is in a guard region, it
//		indicates which guard region(s) it is in.
// ImageLimits - size of CoordImage
// Trial - Object of Blob whose well coordinates are to be decoded.
//
//			Output
// Trial - If blob can be assigned to a well, then the values of Array, Section, and SectionPosition are set accordingly.
//		If the blob cannot be set to zero, then Array, Section and SectionPosition are all set to zero.
// Return value - true if the blob can be assigned to a well, false otherwise.
// 
bool DecodeBlobPosition(cv::Mat &CoordImage, cv::Size ImageLimits, Blob &Trial )
{
	if ( Trial.OnImage )
	{
		// int iTmp = 0;
		if ( Trial.DeviceCenter.x < 1 || Trial.DeviceCenter.x > ImageLimits.width 
			|| Trial.DeviceCenter.y < 1 || Trial.DeviceCenter.y > ImageLimits.height )
		{
			std::cout << "Trial.OnImage == true, but blob is at " << Trial.DeviceCenter << std::endl;
			Trial.OnImage = false;
			Trial.Section = 0;
			Trial.Array = 0;
			Trial.SectionPosition.x = 0;
			Trial.SectionPosition.y = 0;
			return false;
		}
			
		cv::Vec3b cValue = CoordImage.at<cv::Vec3b>(Trial.DeviceCenter);
		// std::cout << "Inside DecodeCentroid - 2" << std::endl;
		if ( cValue[0] > 0 )
		{
			Trial.Section = (int)floor(cValue[0]/10);
			Trial.Array = cValue[0] % 10;
			Trial.SectionPosition.y = cValue[1];
			Trial.SectionPosition.x = (int)cValue[2] % 128;
			return true;
		}
		else
		{
			Trial.Section = 0;
			Trial.Array = 0;
			Trial.SectionPosition.x = 0;
			Trial.SectionPosition.y = 0;
			return false;
		}
	}
	else
	{
		return false;
	}
}
