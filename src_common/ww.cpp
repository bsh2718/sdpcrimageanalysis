#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "physicalconstants.h"
#include "beforeAfterMatrix.hpp"
#include "compareDroplets.hpp"
#include "blob.hpp"
#include "ww.hpp"
////////////////////////////////////////////////////////////////////////////////////////////////////
//bool WaldWolfowitz( std::vector< std::vector< BeforeAfterMatrix > > &BeforeAfter, 
//	std::vector< BlobSummary > &BeforeSizingList, std::vector< BlobSummary > &AfterSizingList, 
//	cv::Size SectionDimens, double SigmaMult, RunInformation &RunInfo )
//
// Implements Wald-Wolfowitz test to determine the likelyhood that the observed number of runs of
// positive and negative wells.  For a given number of wells and a given number of positive wells,
// it calculates the expected number of runs and the standard deviation of the distribution of the
// number of runs.  This is compared to the observed number of runs and if there are too few, it 
// means that some of the positive runs are bridged wells.  The program will marke wells in the 
// longest run as bridged and repeats the test until the number of runs is acceptable.
//
//     input
// BeforeAfter - 3D vector of BeforeAfterMatrix.  The first two dimensions are for Array and Section.
//		The third dimension is a list over the droplets in that Array and Section.  
// BeforeSizingList - 
// AfterSizingList - 
// SectionDimens - 
// SigmaMult - 
//     output
// BeforeAfter - If any droplets are excluded due to bridging, the change in status is indicated here
// RunInfo - Object with information about the results of checking for bridging
// Return Value - false if there are no droplets in the device.

bool SortWell( Well A, Well B  )
{
	if ( A.Col == B.Col )
		return A.Row < B.Row;
	else
		return A.Col < B.Col;
}

bool WWStatistics( std::vector< Run > &RunList, double SigmaMult, int &NumbRuns, double &Mu, double &Sigma )
{
	NumbRuns = 1;
	bool current = RunList[0].Occupied;
	int numbOcc = 0;
	int numbUnocc = 0;
	if ( current )
	{
		if ( RunList[0].Included )
			numbOcc = (int)RunList[0].SectionPosition.size();
		else
			numbOcc = 1;
	}
	else
	{
		if ( RunList[0].Included )
			numbUnocc = (int)RunList[0].SectionPosition.size();
		else
			numbUnocc = 1;
	}
	for ( int n = 1 ; n < RunList.size() ; n++ )
	{
		if (  RunList[n].Occupied )
		{
			if ( RunList[n].Included )
				numbOcc += (int)RunList[n].SectionPosition.size();
			else
				numbOcc += 1;
		}
		else
		{
			if ( RunList[n].Included )
				numbUnocc += (int)RunList[n].SectionPosition.size();
			else
				numbUnocc += 1;
		}
		if ( RunList[n].Occupied != current )
		{
			current = RunList[n].Occupied;
			NumbRuns++;
		}
	}
	// std::cout << "\nRunList.size()/NumbRuns: " << RunList.size() << " / " << NumbRuns << ", numbOcc/numbUnocc: " << numbOcc << " / " << numbUnocc << "\n";
	// std::cout << "SigmaMult: " << SigmaMult << "\n";
	if ( numbOcc < 2 || numbUnocc < 2 )
		return true;
	double nTotal = numbOcc + numbUnocc;
	double nn = numbOcc * numbUnocc;
	Mu = ( 2.0 * nn )/ nTotal + 1.0;
	Sigma = sqrt( ( ( Mu - 1.0 ) * ( Mu - 2.0 ) ) / (nTotal - 1.0) );
	// std::cout << nTotal << "\t" << nn << "\t" << Mu << "\t" << Sigma << "\n";	
	if ( Sigma > 0.5 )
	{
		if ( Mu - Sigma * SigmaMult > NumbRuns )
			return false;
		else
			return true;
	}
	else
	{
		if ( Mu - 0.5 * SigmaMult > NumbRuns )
			return false;
		else
			return true;
	}
}


bool WaldWolfowitz( std::vector< std::vector< BeforeAfterMatrix > > &BeforeAfter, 
	std::vector< BlobSummary > &BeforeSizingList, std::vector< BlobSummary > &AfterSizingList, 
	cv::Size SectionDimens, double SigmaMult, RunInformation &RunInfo )
{
	RunInfo.Clear();
	std::vector< Run > runList;
	runList.clear();
	std::vector< Well > wellList;
	wellList.clear();
	Well newWell;
	double adjustedSigmaMult;
	double numberOcc = 0.0;
	double totalNumber = 0.0;
	for ( int r = 1 ; r <= SectionDimens.height ; r++ )
	{
		for ( int c = 1 ; c <= SectionDimens.width ; c++ )
		{
			BeforeAfterMatrix current;
			current = BeforeAfter[r][c];
			int befIndex = current.BeforeIndex;
			int aftIndex = current.AfterIndex;
			if ( befIndex >= 0 && aftIndex >= 0 )
			{
				if ( !current.EdgeExcluded && !current.BeforeAnomalousFilling && !current.AfterAnomalousFilling
					&& current.RelativeAveIntenStatus > 0 
					&& current.ShrinkageStatus > 0 && BeforeSizingList[befIndex].ShapeSummary.FilterValue == 1 
					&& AfterSizingList[aftIndex].ShapeSummary.FilterValue == 1 
					&& !current.Mismatched )
				{
					newWell.Index = BeforeAfter[r][c].AfterIndex;
					newWell.Col = c;
					newWell.Row = r;
					if ( BeforeAfter[r][c].Occupied == 0 )
						newWell.Occupied = false;
					else
					{
						newWell.Occupied = true;
						numberOcc++;
					}
					totalNumber++;
					wellList.push_back(newWell);
				}
			}
		}
	}
	if (wellList.size() < 1)
	{
		RunInfo.NumbRuns.push_back(0);
		RunInfo.Mu.push_back(1.0);
		RunInfo.Sigma.push_back(1.0);
		RunInfo.NumbRunsExcluded = 0;
		RunInfo.NumbWellsExcluded = 0;
		return false;
	}
	
	adjustedSigmaMult = numberOcc / totalNumber;
	if ( adjustedSigmaMult >= 0.5 )
		adjustedSigmaMult *= 8.0;
	else
		adjustedSigmaMult = (1.0 - adjustedSigmaMult) * 6.0;
	if ( adjustedSigmaMult < SigmaMult )
		adjustedSigmaMult = SigmaMult;
	else if ( adjustedSigmaMult > SigmaMult + 1.5 )
		adjustedSigmaMult = SigmaMult + 1.5;
	
	std::sort( wellList.begin(), wellList.end(), SortWell );
	// std::cout << "SigmaMult = " << SigmaMult << "\n";
	// std::cout << "adjustedSigmaMult = " << adjustedSigmaMult << "\n";
	// for ( int n = 0 ; n < wellList.size() ; n++ )
		// std::cout << n << "\t" << wellList[n].Col << "\t" << wellList[n].Row << "\t" << wellList[n].Occupied << "\n";
	Run newRun;
	newRun.SectionPosition.clear();
	newRun.SectionPosition.push_back(cv::Point(wellList[0].Col, wellList[0].Row ) );
	newRun.Included = true;
	newRun.Occupied = wellList[0].Occupied;
	int last = 0;
	int w = 1;
	while( w < wellList.size() )
	{
		if ( wellList[w].Occupied == newRun.Occupied && wellList[w].Row == newRun.SectionPosition[last].y + 1 )
		{
			newRun.SectionPosition.push_back(cv::Point(wellList[w].Col, wellList[w].Row ) );
			last = (int)newRun.SectionPosition.size() - 1;
		}
		else
		{
			runList.push_back(newRun);
			newRun.SectionPosition.clear();
			if ( w < wellList.size() -1 )
			{
				newRun.SectionPosition.push_back(cv::Point(wellList[w].Col, wellList[w].Row ) );
				newRun.Included = true;
				newRun.Occupied = wellList[w].Occupied;
				last = 0;
			}
			else if ( w == wellList.size() - 1)
				break;
		}
		w++;
		if ( w == wellList.size() ) 
			runList.push_back(newRun);
	}
	// std::cout << "=====\n";
	// for ( int n = 0 ; n < runList.size() ; n++ )
		// std::cout << n << "\t" << runList[n].SectionPosition.size() << "\t" << runList[n].Occupied << "\t" << runList[n].Included << "\n";
	// std::cout << "==========\n";
	bool res = false;
	while ( !res )
	{
		double sigma;
		double mu;
		int numbRuns;
		res = WWStatistics( runList, adjustedSigmaMult, numbRuns, mu, sigma );
		RunInfo.NumbRuns.push_back(numbRuns);
		RunInfo.Mu.push_back(mu);
		RunInfo.Sigma.push_back(sigma);
		//std::cout << "     " << numbRuns << "\t" << mu << "\t" << sigma << "\t" << res << "\n";
		if ( !res )
		{
			// std::cout << "===== Looking for runs to exclude\n";
			int maxRunLength = -1;
			int maxRunIndex = -1;
			for ( int n = 0 ; n < runList.size() ; n++ )
			{
				if ( runList[n].Occupied && runList[n].Included && runList[n].SectionPosition.size() > 1 )
				{
					
					if ( (int)runList[n].SectionPosition.size() > maxRunLength )
					{
						// std::cout << "\t\t" << n << "\t" << runList[n].SectionPosition.size() << "\t" << maxRunLength;
						maxRunLength = (int)runList[n].SectionPosition.size();
						maxRunIndex = n;
						// std::cout << "\t\t" << maxRunLength << "\n";
					}
				}
			}

			if ( maxRunIndex >= 0 )
			{
				runList[maxRunIndex].Included = false;
				RunInfo.NumbRunsExcluded++;
				RunInfo.NumbWellsExcluded += (int)runList[maxRunIndex].SectionPosition.size();
				// for ( int m = 0 ; m < runList[maxRunIndex].SectionPosition.size() ; m++ )
					// std::cout << "\t\t" << runList[maxRunIndex].SectionPosition[m].x << "\t" << runList[maxRunIndex].SectionPosition[m].y << "\n";
			}
			else
				res = true;
		}
	}
	// std::cout << "===============" << std::endl;
	
	for ( int n = 0 ; n < runList.size() ; n++ )
	{
		if ( !runList[n].Included )
		{
			for ( int m = 0 ; m < runList[n].SectionPosition.size() ; m++ )
			{
				int r = runList[n].SectionPosition[m].y;
				int c = runList[n].SectionPosition[m].x;
				BeforeAfter[r][c].RunExcluded = true;
			}
		}
	}
	return true;
}
