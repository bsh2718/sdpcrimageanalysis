#ifndef BLOBANALYSIS_H
#define BLOBANALYSIS_H
//! [includes]
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//#include <iostream>
//#include <string>
//#include <cstdlib>
//! [includes]

#include "statistics.hpp"

#define RAWPOSITIONUNUSED -10000
#define POSITIONUNUSED2F -10000.0

//! [namespace]
//using namespace std;
//using namespace cv;
//! [namespace]
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Classes defined
//		FillInfo
//		FillInfoSummary
//		BlobShape
//		BlobShapeSummary
//		Blob
//		BlobSummary
//
//	Blob contains a vector of BlobShape, which contains a vector of FillInfo
//	BlobSummary contains a vector of BlobShapeSummary, which contains a vector of FillInfoSummary
//
//	Blob is used by sdpcrSizingAnalysis in analyzing images.  Some, but not all of the information in Blob
//	is saved in .csv files.  BlobSummary is used when the .csv files are read by the quantitation program.
//	Currently the assignment operator has been overloaded so the following work
//		FillInfoSummary = FillInfo
//		BlobShapeSummary = BlobShape
//		BlobSummary = Blob
//	(There is no information in the summary classes which is not present in one form or other in the full class)
//	All of these classes have a member function named Clear() which resets the member variables to the values
//	they have when the default constructor is used.
//
//============ Blob ============
//		Member Variables
//	Status - (int) status indicator. equals 0 if blob has not been assigned to a well
//	SelfIndex - (int) when the vector of blobs (blobList) is first generated, this is set to the blob's position
//		in blobList. Later, blobList is copied to s separate list and sorted for analysis.  The SelfIndex in the
//		sorted list enables the program to write the results of the analysis into the correct element of blobList.
//	OnImage - (bool) indicates whether the location of blob is still on the image after alignment.
//
// 			The analysis program assigns a series of coordinates to the geometric center of each blob.  The 
// 			location of the blob in the image changes as the the image is analyzed and the different coordinates
//			track these changes. The coordinates which end in 2f are cv::Point2f, otherwise they are cv::Point
//	InputCenter2f - center of blob in background subtracted imagesize
//	RotatedCenter2f - center of blob in image corrected for angle
//	RotatedCenter - cv::Point copy of RotatedCenter2f
//	RawPosition - Each blob is assigned to a block and this is the offset in columns and rows of wells from the 
//		(0, 0) blob of that block.
//	ModelCenter2f - center of blob in an idealized image. The RotatedCenter2f of the (0, 0) blob is adjusted for
//		scaling and the result is used as a base position and the location of all other blobs are obtained from 
//		RawPosition and WellSpacing.  This position assumes that the distances and orientation of all blobs,
//		relative to to the (0, 0) blob is exactly what is expected and "corrects" for any distortion in the device,
//		errors in magnification and any uncorrected angle misalignment.  These positions are in aligning the blobs
//		to the device.
//	PrelimCenter2f - center of blob after initial alignment of blob positions to device.  A single X and Y offset is
//		applied to all blobs to create this coordinate which is then compared to CoordImage, which is a 3 color
//		image indexed to the actual device.  CoordImage is generally much larger than the input image, so these
//		coordinates should only be used with CoordImage and not be compared to objects in the original image.
//	DeviceCenter2f - the program will next adjust each individual block of blobs to see if that improves the alignment
//		of the blobs.  If it does, then this is the center of the blob after this block adjustment.  If it doesn't
//		improve the alignment, then this equals PrelimCenter2f.  Like PrelimCenter2f, this position should only be
//		used with CoordImage.
//	BlockListIndex indicates which block a blob has been assigned to.  It is < 0 if the blob is not assigned to a block.
//
//			For blobs which are assigned to a well
//	Array - array index (left most array is 1)
//	Section - section index (topmost section of each array is 1)
//	SectionPosition - (cv::Point) column and row position of well in section/array.  The top/left well in section
//		is cv::Point(1,1)
//			For blobs which are not assigned to a well, Array, Section, and SectionPosition are all set to 0.
//			For blobs in a guard region (areas in device where there are no wells).
//	Guard - (cv::Point) X and Y guard index of guard region where blob is located. The left most X guard (area 
//		to the left of left most array) is 1.  The top most Y guard (area above top most section) is one. 
//		Guard = cv::Point(X-Guard Index, Y-Guard Index) and equals cv::Point(0,0) if blob is not in a guard region.
//	Shape - vector<BlobShape> vector of shape information about blob.  The different elements correspond to
//		information obtained with different thresholds.  The data in Shape[0] is what is used in aligning the
//		image. InputCenter2f equals Shape[0].InputCenter2f
//
//	Blob has a second member function 
//		bool ApplyRotation(double CosAngle, double SinAngle, cv::Size ImageSize )
//	ApplyRotation uses the values of CosAngle and SinAngle to convert InputCenter2f to RotatedCenter2f.  It
//		rounds the values of RotatedCenter2f to obtain RotatedCenter. It compares RotatedCenter to ImageSize
//		to set OnImage.  It returns OnImage.
//
//============ BlobSummary ============
//	Each instance of BlobSummary is obtained from a corresponding instance of Blob. This can happen by
//	reading the data for each blob which was output earlier and loading it into BlobSummary or by used of
//	the assignment operator.
//
// BlobSummary does not have Shape, but instead has ShapeSummary which is vector<BlobShapeSummary>.  Each
//		element of ShapeSummary is obtained from the corresponding element of Shape.
// BlobSummary does not have the ApplyRotation member function.
//
//
//============ BlobShape ============
//		Member Variables
//	Threshold - (double) Threshold applied to background subtracted image to obtain blob information
//	BoundingRect - (cv::Rect) Bounding rectangle for blob
//	Contour - (vector<cv::Point>) pixel locations of contour which defines blob (eight connected)
//	Perimeter - (double) length of line connecting the centers of pixels in Contour.  This is smaller
//		than Contour.size() and not necessarily an integer.
//	InputCenter2f - geometric center of blob.  For Shape[0], this equals the value in Blob.
//	PixelList - (vector<cv::Point>) list of pixels in blob
//	PixelIntensities - (vector<double>) list of intensities of pixels in blob
//	TopIntensities - (object of class Statistics1) contains information about the pixel intensities of
//		pixels within the "top" region of the blob (this is separately below)
//	BlobIntensities - (object of class Statistics1) contains information about the pixel intensities of
//		all the pixels in the blob.
//  Mom - (cv::Moments) moments of blob
//	AspectRatio - (double) BoundingRect.height / BoundingRect.width
//	Circularity - (double) Circularity calculated from Perimenter and Mom.m00
//			Criteria are input to determine if a blob is acceptable (analysisParameters.h).  All of the
//			criteria are defined to have both minimum and maximum values. The criteria include
//			limits on blob width and height, two different limits on the blob area, and limits on the
//			blob circularity and blob aspect ratio.
//	FilterValue - (int) equals	1 if blob meets all criteria, including the more restrictive of the
//									area limits
//								2 if the blob fails the more strict of the two area limits, but passes
//									the less strict area limit and all the other criteria
//								0 otherwise
//	FillingInfo - (vector<FillInfo>) the blob pixels are divided into concentric elliptical rings starting
//			with one centered on the center of the blob and proceeding outward.  Each ring has an entry
//			in FillingInfo
//	FillIntensity - (double) TopIntensities.Ave() times the scaled well area (the area of the well in
//			pixels adjusted for any scaling correction).  This is the intensity which would be observed
//			if the well was completely filled and the fluorescent intensity emitted from all pixels 
//			equaled TopIntensities.Ave()
//	FillRatio - (double) BlobIntensities.Sum() / FillIntensity. The fractional filling of the well.
//	TopIntensityFraction - (double) total number of pixels in top region / scaled well area
//	TopFillingNumber - (int) index to element of FillInfo which has the last ring which is in the
//			top region.
//
//============ BlobShapeSummary ============
//		Member Variables
//	Threshold - (double) Same as corresponding variable in BlobShape
//	BoundingRect - (cv::Rect) Same as corresponding variable in BlobShape
//	ContourLength - (int) equals Contour.size() of BlobShape
//	Perimeter - (double) Same as corresponding variable in BlobShape
//	InputCenter2f - Same as corresponding variable in BlobShape
//	Area - (int) equals PixelList.size() of BlobShape
//	TopIntensities - (object of class Stat1Results) contains summary information from TopIntensities
//		object in BlobShape
//	BlobIntensities - (object of class Stat1Results) contains summary information from BlobIntensities
//		object in BlobShape.
//  Mom - (cv::Moments) moments of blob
//	AspectRatio - (double) Same as corresponding variable in BlobShape
//	Circularity - (double) Same as corresponding variable in BlobShape
//	FilterValue - (int) Same as corresponding variable in BlobShape
//	FillingSummary - (vector<FillInfoSummary>) vector of summary of information in FillInfo.  However,
//		at present the detailed information in FillInfo is not saved in a .csv file and so this variable
//		might be removed at some future time. Some information from Shape.FillInfo is saved in the last
//		five variables of this class.
//	FillIntensity - (double) ame as corresponding variable in BlobShape
//	FillRatio - (double) ame as corresponding variable in BlobShape
//	TopIntensityFraction - (double) ame as corresponding variable in BlobShape
//	TopFillingNumber - (int) ame as corresponding variable in BlobShape
//	LastRingIntensities - (object of class Stat1Results) summary information for last ring in top region
//			The next two items are information about the last ring in top region
//	LastRingMaxIntensity - (double) max intensity in last ring of top region
//	LastRingMinIntensity - (double) min intensity in last ring of top region
//			The next
//	NumbAnomalousIntensityRings - (int) number of rings in BlobShape.FillingInfo which have anomalous
//		intensities.
//	NumbAnomalousShapeRings  - (int) number of rings in BlobShape.FillingInfo which have an anomalous
//		shape.
//
//============ FillInfo ============
//	Each instance of FillInfo refers to a specific ring in FillingInfo
//		Member Variables
//	Intensities - (object of class Statistics1) contains information about the pixel intensities of
//		pixels in this ring
//	CumulativeIntensities - (object of class Statistics1) contains information about the pixel 
//		intensities of all pixels in this and all previous (smaller) rings
//	PixelList - (vector<cv::Point>) list of pixels in ring
//	PixelIntensities - (vector<double>) list of intensities of pixels in ring
//	MaxInten - (double) Maximum intensity in this ring
//	MinInten - (double) Minimum intensity in this ring
//	CumulativeFraction - (double) Fraction of well area occupied by this and all previous rings.
//	Top - (bool) true if ring is part of top region
//	Center2f - (cv::Point2f) Geometric center of ring
//	Centroid2f - (cv::Point2f) Intensity weighted center of ring
//	AnomalousIntensity - (bool) true if average intensity of ring is too much larger than the
//		cumulative average intensity of all previous rings.  NumbAnomalousIntensityRings is the summary
//		of all rings in Shape.FillInfo for which this is true.
//	AnomalousShape - (bool) true if the difference between Center2f and Centroid2f is too larger
//		in either dimension. NumbAnomalousShapeRings is the sum all rings in Shape.FillInfo for which
//		this is true.
//
//============ FillInfoSummary ============
//	This does not include PixelList or PixelIntensities, but is otherwise identical to FillInfo.
//
//============ Top Region ============
//
//	The main program creates a series of Templates each of which is an elliptical ring with the approximately
//	the same aspect ratio as the well.  They are ordered from smallest (which is usally a rectangle or square)
//	to largest. Each ring is centered on the blob and those pixels which are masked by the ring are selected.
//	Intensity information is collected for the pixels in the ring and for pixels in the current and all
//	previous rings.  A ring is considered part of the top region if its average intensity is comparable to the
//	cumulative average associated with the previous ring.  The last ring in the top region is generally the
//	last ring for which this is true.  The search for rings in the top region will also terminate once the
//	number of pixels enclosed the current ring (inclusize) is greater than the required number of pixels
//	in the top region.  This is usually input to the program as a fraction (<1) to be applied to the scaled
//	well area.
//
//	The assumption is that the average intensity of the top reqion represents the intensity from a portion of
//	the well which is completely filled in the z-direction.  So if the well was completely filled (all the way
//	into the corners) the total intensity from the blob in the well should equal the average intensity of the
//	top region times the area of the well (FillIntensity).  The area is scaled to account for errors in 
//	magnification (a different microns/pixel in the actual image than expected).  The actual total intensity 
//	from the blob (BlobIntensities.Sum() ) divided by FillIntensity should be the fraction of the well which
//	contains reaction mixture.  This then gets multiplied by the volume of the well (a separate input
//	parameter) to get the volume of the blob.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// The top region of a fluorescent blob is analyzed using a set of rings centered on the centroid of the
// blob.  FillInfo contains intensity information about each of the rings. 
class FillInfo
{
	public:
		Statistics1 Intensities;			// Statistics for pixels in this ring. 
		Statistics1 CumulativeIntensities;	// Statistics for pixels in this ring and all pixels enclosed by this ring
		std::vector<cv::Point> PixelList;
		std::vector<double> PixelIntensities;

		double MaxInten;					// Max Intensity in this ring
		double MinInten;					// Min Intensity in this ring
		double CumulativeFraction;			// Fraction of well area in CumulativeIntensities
		bool Top;							// equals true if ring is part of top region
		cv::Point2f Center2f;					// geometric center of ring;
		cv::Point2f Centroid2f;				// Intensity weighted centroid of ring
		bool AnomalousIntensity;			// true indicates a ring which is significantly more intense than innner rings
		bool AnomalousShape;				// true indiccates a ring where the intensity weighted centroid is severely off center
											
		FillInfo(): MaxInten(0.0), MinInten(0.0),
			CumulativeFraction(0.0), Top(false), Center2f(cv::Point2f(0.0, 0.0)), Centroid2f(cv::Point2f(0.0, 0.0)),
			AnomalousIntensity(false), AnomalousShape(false)
		{
			Intensities.Clear();
			CumulativeIntensities.Clear();
			PixelList.clear();
			PixelIntensities.clear();
		}
		~FillInfo(){}
		
		FillInfo(const FillInfo &Source):
			MaxInten(Source.MaxInten), MinInten(Source.MinInten),
			CumulativeFraction(Source.CumulativeFraction), Top(Source.Top),
			Center2f(Source.Center2f), Centroid2f(Source.Centroid2f),
			AnomalousIntensity(Source.AnomalousIntensity), AnomalousShape(Source.AnomalousShape)
		{
			Intensities = Source.Intensities;
			CumulativeIntensities = Source.CumulativeIntensities;
			PixelList = Source.PixelList;
			PixelIntensities = Source.PixelIntensities;
		}

		FillInfo& operator=(const FillInfo &Source)
		{
			if ( this != &Source )
			{
				MaxInten = Source.MaxInten;
				MinInten = Source.MinInten;
				Intensities = Source.Intensities;
				CumulativeIntensities = Source.CumulativeIntensities;
				PixelList = Source.PixelList;
				PixelIntensities = Source.PixelIntensities;
				CumulativeFraction = Source.CumulativeFraction;
				Top = Source.Top;
				Center2f = Source.Center2f;
				Centroid2f = Source.Centroid2f;
				AnomalousIntensity = Source.AnomalousIntensity;
				AnomalousShape = Source.AnomalousShape;
			}
			return *this;
		}
		
		void Clear()
		{
			MaxInten = 0.0;
			MinInten = 0.0;
			Intensities.Clear();
			CumulativeIntensities.Clear();
			PixelList.clear();
			PixelIntensities.clear();
			CumulativeFraction = 0.0;
			Top = false;
			Center2f = cv::Point2f(0.0, 0.0);
			Centroid2f = cv::Point2f(0.0, 0.0);
			AnomalousIntensity = false;
			AnomalousShape = false;
		}
};

class FillInfoSummary
{
	public:
		Statistics1 Intensities;			// Statistics for pixels in this ring. 
		Statistics1 CumulativeIntensities;	// Statistics for pixels in this ring and all pixels enclosed by this ring

		double MaxInten;					// Max Intensity in this ring
		double MinInten;					// Min Intensity in this ring
		double CumulativeFraction;			// Fraction of well area in CumulativeIntensities
		bool Top;							// equals true if ring is part of top region
		cv::Point2f Center2f;					// geometric center of ring;
		cv::Point2f Centroid2f;				// Intensity weighted centroid of ring
		bool AnomalousIntensity;			// true indicates a ring which is significantly more intense than innner rings
		bool AnomalousShape;				// true indiccates a ring where the intensity weighted centroid is severely off center 
											
		FillInfoSummary(): MaxInten(0.0), MinInten(0.0), 
			CumulativeFraction(0.0), Top(false), Center2f(cv::Point2f(0.0, 0.0)), Centroid2f(cv::Point2f(0.0, 0.0)),
			AnomalousIntensity(false), AnomalousShape(false)
		{
			Intensities.Clear();
			CumulativeIntensities.Clear();
		}
		~FillInfoSummary(){}
		
		FillInfoSummary(const FillInfoSummary &Source):
			MaxInten(Source.MaxInten), MinInten(Source.MinInten),
			CumulativeFraction(Source.CumulativeFraction), Top(Source.Top),
			Center2f(Source.Center2f), Centroid2f(Source.Centroid2f),
			AnomalousIntensity(Source.AnomalousIntensity), AnomalousShape(Source.AnomalousShape)
		{
			Intensities = Source.Intensities;
			CumulativeIntensities = Source.CumulativeIntensities;
		}

		FillInfoSummary& operator=(const FillInfoSummary &Source)
		{
			if ( this != &Source )
			{
				MaxInten = Source.MaxInten;
				MinInten = Source.MinInten;
				Intensities = Source.Intensities;
				CumulativeIntensities = Source.CumulativeIntensities;
				CumulativeFraction = Source.CumulativeFraction;
				Top = Source.Top;
				Center2f = Source.Center2f;
				Centroid2f = Source.Centroid2f;
				AnomalousIntensity = Source.AnomalousIntensity;
				AnomalousShape = Source.AnomalousShape;
			}
			return *this;
		}
		
		FillInfoSummary& operator= ( const FillInfo &Source )
		{
			MaxInten = Source.MaxInten;
			MinInten = Source.MinInten;
			Intensities = Source.Intensities;
			CumulativeIntensities = Source.CumulativeIntensities;
			CumulativeFraction = Source.CumulativeFraction;
			Top = Source.Top;
			Center2f = Source.Center2f;
			Centroid2f = Source.Centroid2f;
			AnomalousIntensity = Source.AnomalousIntensity;
			AnomalousShape = Source.AnomalousShape;

			return *this;
		}
		
		void Clear()
		{
			MaxInten = 0.0;
			MinInten = 0.0;
			Intensities.Clear();
			CumulativeIntensities.Clear();
			CumulativeFraction = 0.0;
			Top = false;
			Center2f = cv::Point2f(0.0, 0.0);
			Centroid2f = cv::Point2f(0.0, 0.0);
			AnomalousIntensity = false;
			AnomalousShape = false;
		}
};

class BlobShape
{
	public:
		int Status;
		double Threshold;
		cv::Rect BoundingRect;
		std::vector< cv::Point > Contour;	
		double Perimeter;
		cv::Point2f InputCenter2f;
		
		std::vector< cv::Point > PixelList;
		std::vector< double > PixelIntensities;
		
		std::vector< cv::Point > AddedPixelList;
		std::vector< double > AddedPixelIntensities;

		Statistics1 TopIntensities;
		Statistics1 BlobIntensities;
		Statistics1 AddedBlobIntensities;
		Statistics1 TotalBlobIntensities;
		//double TotalToThresholdedIntensityRatio;
		
		cv::Moments Mom;
		double AspectRatio;
		double Circularity;
		int FilterValue;
		unsigned int FilterCode;
		std::string FilterString;
		std::vector< FillInfo > FillingInfo;
		
		double FillIntensity;
		double FillRatio;
		double TopIntensityFraction;
		int TopFillingNumber;
		double AltFillRatio;
		bool AnomalousFilling;
		
		BlobShape(): Status(0), Threshold(0.0), BoundingRect(cv::Rect(0,0,1,1)), Perimeter(0.0),
			InputCenter2f(cv::Point2f(0.0, 0.0)), // TotalToThresholdedIntensityRatio(1.0), 
			AspectRatio(1.0), Circularity(0.0), FilterValue(0), FilterCode(0), FilterString("_____"),
			FillIntensity(0.0), FillRatio(0.0), TopIntensityFraction(0.0), TopFillingNumber(0),
			AltFillRatio(0.0), AnomalousFilling(false)
		{
			Contour.clear();
			PixelList.clear();
			PixelIntensities.clear();
			AddedPixelList.clear();
			AddedPixelIntensities.clear();
			TopIntensities.Clear();
			BlobIntensities.Clear();
			AddedBlobIntensities.Clear();
			TotalBlobIntensities.Clear();
			Mom.m00 = 0.0;
			FillingInfo.clear();
		}
		~BlobShape(){}
		
		BlobShape(const BlobShape &Source): Status(Source.Status), Threshold(Source.Threshold), 
			BoundingRect(Source.BoundingRect), Perimeter(Source.Perimeter),
			InputCenter2f(Source.InputCenter2f), //TotalToThresholdedIntensityRatio(Source.TotalToThresholdedIntensityRatio), 
			Mom(Source.Mom), AspectRatio(Source.AspectRatio),
			Circularity(Source.Circularity), FilterValue(Source.FilterValue), FilterCode(Source.FilterCode), FilterString(Source.FilterString),
			FillIntensity(Source.FillIntensity), FillRatio(Source.FillRatio), 
			TopIntensityFraction(Source.TopIntensityFraction), TopFillingNumber(Source.TopFillingNumber),
			AltFillRatio(Source.AltFillRatio), 
			AnomalousFilling(Source.AnomalousFilling)
		{
			Contour = Source.Contour;
			PixelList = Source.PixelList;
			PixelIntensities = Source.PixelIntensities;
			AddedPixelList = Source.AddedPixelList;
			AddedPixelIntensities = Source.AddedPixelIntensities;
			TopIntensities = Source.TopIntensities;
			BlobIntensities = Source.BlobIntensities;
			AddedBlobIntensities = Source.AddedBlobIntensities;
			TotalBlobIntensities = Source.TotalBlobIntensities;
			FillingInfo = Source.FillingInfo;
		}
		
		BlobShape& operator=(const BlobShape &Source)
		{
			if ( this != &Source )
			{
				Status = Source.Status;
				Threshold = Source.Threshold;
				BoundingRect = Source.BoundingRect;
				Contour = Source.Contour;
				Perimeter = Source.Perimeter;
				InputCenter2f = Source.InputCenter2f;
				PixelList = Source.PixelList;
				PixelIntensities = Source.PixelIntensities;
				AddedPixelList = Source.AddedPixelList;
				AddedPixelIntensities = Source.AddedPixelIntensities;
				TopIntensities = Source.TopIntensities;
				BlobIntensities = Source.BlobIntensities;
				AddedBlobIntensities = Source.AddedBlobIntensities;
				TotalBlobIntensities = Source.TotalBlobIntensities;
				// TotalToThresholdedIntensityRatio = Source.TotalToThresholdedIntensityRatio;
				Mom = Source.Mom;
				AspectRatio = Source.AspectRatio;
				Circularity = Source.Circularity;
				FilterValue = Source.FilterValue;
				FilterCode = Source.FilterCode;
				FilterString = Source.FilterString;
				FillingInfo = Source.FillingInfo;
				FillIntensity = Source.FillIntensity;
				FillRatio = Source.FillRatio;
				TopIntensityFraction = Source.TopIntensityFraction;
				TopFillingNumber = Source.TopFillingNumber;
				AltFillRatio = Source.AltFillRatio;
				AnomalousFilling = Source.AnomalousFilling;
			}
			return *this;
		}
		
		void Clear()
		{
			Status = 0;
			Threshold = 0.0;
			BoundingRect = cv::Rect(0,0,1,1);
			Contour.clear();
			Perimeter = 0.0;
			InputCenter2f = cv::Point2f(0.0, 0.0);
			PixelList.clear();
			PixelIntensities.clear();
			AddedPixelList.clear();
			AddedPixelIntensities.clear();
			TopIntensities.Clear();
			BlobIntensities.Clear();
			AddedBlobIntensities.Clear();
			TotalBlobIntensities.Clear();
			// TotalToThresholdedIntensityRatio = 1.0;
			Mom.m00 = 0.0;
			AspectRatio = 1.0;
			Circularity = 0.0;
			FilterValue = 0;
			FilterCode = 0;
			FilterString = "_____";
			FillingInfo.clear();
			FillIntensity = 0.0;
			FillRatio = 0.0;
			TopIntensityFraction = 0.0;
			TopFillingNumber = 0;
			AltFillRatio = 0.0;
			AnomalousFilling = false;
		}
};

class BlobShapeSummary
{
	public:
		int Status;
		double Threshold;
		cv::Rect BoundingRect;
		int ContourLength;		
		double Perimeter;
		cv::Point2f InputCenter2f;
		std::vector< cv::Point > PixelList;
		std::vector< double > PixelIntensities;
		
		int Area;
		Stat1Results TopIntensities;
		Stat1Results BlobIntensities;
		Stat1Results AddedBlobIntensities;
		Stat1Results MaskedBlobIntensities;
		Stat1Results TotalBlobIntensities;
		// double TotalToThresholdedIntensityRatio;
		
		cv::Moments Mom;
		double AspectRatio;
		double Circularity;
		int FilterValue;
		unsigned int FilterCode;
		std::string FilterString;
		std::vector< FillInfoSummary > FillingSummary;
		
		double FillIntensity;
		double FillRatio;
		double TopIntensityFraction;
		int TopFillingNumber;
		
		Stat1Results LastRingIntensities;
		double LastRingMaxIntensity;
		double LastRingMinIntensity;
		
		double AltFillRatio;
		bool AnomalousFilling;
		
		double AfterBeforeRatio;
		double BeforeFillRatio;
		double Shrinkage;
		
		BlobShapeSummary(): Status(0), Threshold(0.0),
			BoundingRect(cv::Rect(0,0,1,1)), ContourLength(0), Perimeter(0.0),
			InputCenter2f(cv::Point2f(0.0, 0.0)), Area(0), 
			AspectRatio(1.0), Circularity(0.0), FilterValue(0), FilterCode(0), FilterString("_____"),
			FillIntensity(0.0), FillRatio(0.0),
			TopIntensityFraction(0.0), TopFillingNumber(0), 
			LastRingMaxIntensity(0.0), LastRingMinIntensity(0.0),
			AltFillRatio(0.0),
			AnomalousFilling(false),
			AfterBeforeRatio(-1.0), BeforeFillRatio(1.0), Shrinkage(-1.0)
		{
			PixelList.clear();
			PixelIntensities.clear();
			TopIntensities.Clear();
			AddedBlobIntensities.Clear();
			BlobIntensities.Clear();
			MaskedBlobIntensities.Clear();
			Mom.m00 = Mom.m10 = Mom.m01 = Mom.m20 = Mom.m11 = Mom.m02 = 0.0;
			FillingSummary.clear();
			LastRingIntensities.Clear();
		}
		~BlobShapeSummary(){}
		
		BlobShapeSummary(const BlobShapeSummary &Source): Status(Source.Status), Threshold(Source.Threshold),
			BoundingRect(Source.BoundingRect), ContourLength(Source.ContourLength), Perimeter(Source.Perimeter),
			InputCenter2f(Source.InputCenter2f), Area(Source.Area), 
			Mom(Source.Mom), AspectRatio(Source.AspectRatio),
			Circularity(Source.Circularity), FilterValue(Source.FilterValue), FilterCode(Source.FilterCode), FilterString(Source.FilterString),
			FillIntensity(Source.FillIntensity), FillRatio(Source.FillRatio),
			TopIntensityFraction(Source.TopIntensityFraction), TopFillingNumber(Source.TopFillingNumber),
			LastRingMaxIntensity(Source.LastRingMaxIntensity), LastRingMinIntensity(Source.LastRingMinIntensity),
			AltFillRatio(Source.AltFillRatio),
			AnomalousFilling(Source.AnomalousFilling),
			AfterBeforeRatio(Source.AfterBeforeRatio), BeforeFillRatio(Source.BeforeFillRatio), Shrinkage(Source.Shrinkage)
		{
			PixelList = Source.PixelList;
			PixelIntensities = Source.PixelIntensities;
			TopIntensities = Source.TopIntensities;
			AddedBlobIntensities = Source.AddedBlobIntensities;
			BlobIntensities = Source.BlobIntensities;
			MaskedBlobIntensities = Source.MaskedBlobIntensities;
			TotalBlobIntensities = Source.TotalBlobIntensities;
			FillingSummary = Source.FillingSummary;
			LastRingIntensities = Source.LastRingIntensities;
		}
		
		BlobShapeSummary& operator=(const BlobShapeSummary &Source)
		{
			if ( this != &Source )
			{
				Status = Source.Status;
				Threshold = Source.Threshold;
				BoundingRect = Source.BoundingRect;
				ContourLength = Source.ContourLength;
				Perimeter = Source.Perimeter;
				InputCenter2f = Source.InputCenter2f;
				PixelList = Source.PixelList;
				PixelIntensities = Source.PixelIntensities;
				Area = Source.Area;
				TopIntensities = Source.TopIntensities;
				AddedBlobIntensities = Source.AddedBlobIntensities;
				BlobIntensities = Source.BlobIntensities;
				MaskedBlobIntensities = Source.MaskedBlobIntensities;
				TotalBlobIntensities = Source.TotalBlobIntensities;
				Mom = Source.Mom;
				AspectRatio = Source.AspectRatio;
				Circularity = Source.Circularity;
				FilterValue = Source.FilterValue;
				FilterCode = Source.FilterCode;
				FilterString = Source.FilterString;
				FillingSummary = Source.FillingSummary;
				FillIntensity = Source.FillIntensity;
				FillRatio = Source.FillRatio;
				TopIntensityFraction = Source.TopIntensityFraction;
				TopFillingNumber = Source.TopFillingNumber;
				LastRingIntensities = Source.LastRingIntensities;
				LastRingMaxIntensity = Source.LastRingMaxIntensity;
				LastRingMinIntensity = Source.LastRingMinIntensity;
				AltFillRatio = Source.AltFillRatio;
				AnomalousFilling = Source.AnomalousFilling;
				AfterBeforeRatio = Source.AfterBeforeRatio;
				BeforeFillRatio = Source.BeforeFillRatio;
				Shrinkage = Source.Shrinkage;
			}
			return *this;
		}
		
		BlobShapeSummary& operator= ( BlobShape &Source )
		{
			Status = Source.Status;
			Threshold = Source.Threshold;
			BoundingRect = Source.BoundingRect;
			ContourLength = (int)Source.Contour.size();
			Perimeter = Source.Perimeter;
			InputCenter2f = Source.InputCenter2f;
			PixelList = Source.PixelList;
			PixelIntensities = Source.PixelIntensities;
			Area = (int)Source.PixelIntensities.size();
			TopIntensities = Source.TopIntensities;
			BlobIntensities = Source.BlobIntensities;
			AddedBlobIntensities = Source.AddedBlobIntensities;
			MaskedBlobIntensities.Clear();
			TotalBlobIntensities = Source.TotalBlobIntensities;
			Mom = Source.Mom;
			AspectRatio = Source.AspectRatio;
			Circularity = Source.Circularity;
			FilterValue = Source.FilterValue;
			FilterCode = Source.FilterCode;
			FilterString = Source.FilterString;
			AnomalousFilling = false;
			int fSize = (int)Source.FillingInfo.size();
			if ( fSize > 0 )
			{
				FillingSummary.resize(fSize);
				for ( int n = 0 ; n < fSize ; n++ )
				{
					FillingSummary[n] = Source.FillingInfo[n];
				}
			}
			else
				FillingSummary.clear();

			FillIntensity = Source.FillIntensity;
			FillRatio = Source.FillRatio;
			
			TopIntensityFraction = Source.TopIntensityFraction;
			TopFillingNumber = Source.TopFillingNumber;
			if ( TopFillingNumber > 0 && TopFillingNumber < fSize )
			{
				LastRingIntensities = Source.FillingInfo[TopFillingNumber].Intensities;
				LastRingMaxIntensity = Source.FillingInfo[TopFillingNumber].MaxInten;
				LastRingMinIntensity = Source.FillingInfo[TopFillingNumber].MinInten;
			}
			else
			{
				LastRingIntensities.Clear();
				LastRingMaxIntensity = 0;
				LastRingMinIntensity = 0;
			}
			AltFillRatio = Source.AltFillRatio;
			AnomalousFilling = Source.AnomalousFilling;
			AfterBeforeRatio = -1.0;
			BeforeFillRatio = 1.0;
			Shrinkage = -1.0;
				
			return *this;
		}
		
		void Clear()
		{
			Status = 0;
			Threshold = 0.0;
			BoundingRect= cv::Rect(0,0,1,1); 
			ContourLength = 0;
			Perimeter = 0.0;
			InputCenter2f = cv::Point2f(0.0, 0.0);
			PixelList.clear();
			PixelIntensities.clear();
			Area = 0;
			AspectRatio = 1.0;
			Circularity = 0.0;
			FilterValue = 0;
			FilterCode = 0;
			FilterString = "_____";
			TopIntensities.Clear();
			BlobIntensities.Clear();
			AddedBlobIntensities.Clear();
			MaskedBlobIntensities.Clear();
			TotalBlobIntensities.Clear();
			Mom.m00 = Mom.m10 = Mom.m01 = Mom.m20 = Mom.m11 = Mom.m02 = 0.0;
			FillingSummary.clear();
			FillIntensity = 0.0;
			FillRatio = 0.0;
			TopIntensityFraction = 0.0;
			TopFillingNumber = 0;
			LastRingIntensities.Clear();
			LastRingMaxIntensity = 0.0;
			LastRingMinIntensity = 0.0;
			AltFillRatio = 0.0;
			AnomalousFilling = false;
			AfterBeforeRatio = -1.0;
			BeforeFillRatio = 1.0;
			Shrinkage = -1.0;
		}
};

// Blob is used as a class for information about blobs in the image at two different stages of the analysis
//
class Blob
{
	public:
		int Status;
		int SelfIndex;
		bool OnImage;
		cv::Point2f InputCenter2f;		// Equal weighted center of blob in original image (calculated from PixelList)
										//     Threshold used to define blob is the threshold in Shape[0]
		cv::Point2f RotatedCenter2f;	// Location of InputCenter after rotational correction
		cv::Point2f AdjustedCenter2f;	// RotatedCenter2f after adjustments for image distortion, 
										//		if no adjustment made, then it equals RotatedCenter2f
		cv::Point AdjustedCenter;		// (int)round(AdjustedCenter2f)
		
		cv::Point2f DeviceCenter;		// location of blob in device. This is RotatedCenter2f scaled to account for
										// magnification errors and _then_ shifted in X and Y to align the blobs with coordImage
		
		int Array;
		int Section;
		cv::Point SectionPosition;
		cv::Point Guard;
		bool OnNexusList;				// On a Nexus List
		bool UsedAsBase;				// Used as base for a different nexus
		int NexusListIdx;				// which nexus list
		int NexusIdx;					// index into nexus list

		BlobShape Shape;				// Shape information, This includes a
										//     list of pixel locations, intensities, the blob contour and filling calculation details.
		
		Blob(): Status(0), SelfIndex(-1), OnImage(false), InputCenter2f(cv::Point2f(0.0,0.0)), 
			RotatedCenter2f(cv::Point2f(0.0,0.0)), AdjustedCenter2f(cv::Point2f(0.0,0.0)), AdjustedCenter(cv::Point(0,0)),
			DeviceCenter(cv::Point(0,0)),
			Array(0), Section(0), SectionPosition(cv::Point(0,0)), Guard(cv::Point(0,0)), 
			OnNexusList(false), UsedAsBase(false), NexusListIdx(-1), NexusIdx(-1)
		{
			Shape.Clear();
		}
		~Blob(){}
		
		Blob& operator= ( const Blob &Source )
		{
			if ( this !=&Source )
			{
				Status = Source.Status;
				SelfIndex = Source.SelfIndex;
				OnImage = Source.OnImage;
				InputCenter2f = Source.InputCenter2f;
				RotatedCenter2f = Source.RotatedCenter2f;
				AdjustedCenter2f = Source.AdjustedCenter2f;
				AdjustedCenter = Source.AdjustedCenter;
				DeviceCenter = Source.DeviceCenter;
				Array = Source.Array;
				Section = Source.Section;
				SectionPosition = Source.SectionPosition;
				Guard = Source.Guard;
				OnNexusList = Source.OnNexusList;
				UsedAsBase = Source.UsedAsBase;
				NexusListIdx = Source.NexusListIdx;
				NexusIdx = Source.NexusIdx;
				Shape = Source.Shape;
			}
			return *this;	
		}
		
		Blob( const Blob &Source ): Status(Source.Status), SelfIndex(Source.SelfIndex), OnImage(Source.OnImage),
			InputCenter2f(Source.InputCenter2f), RotatedCenter2f(Source.RotatedCenter2f), AdjustedCenter2f(Source.AdjustedCenter2f),
			AdjustedCenter(cv::Point(0, 0)), DeviceCenter(Source.DeviceCenter),
			Array(Source.Array),
			Section(Source.Section), SectionPosition(Source.SectionPosition), Guard(Source.Guard), 
			OnNexusList(Source.OnNexusList), UsedAsBase(Source.UsedAsBase), NexusListIdx(Source.NexusListIdx), NexusIdx(Source.NexusIdx)
		{
			Shape = Source.Shape;
		}
		
		void Clear()
		{
			Status = 0;
			SelfIndex = -1;
			OnImage = false;
			InputCenter2f = cv::Point2f(0.0,0.0);
			RotatedCenter2f = cv::Point2f(0.0,0.0);
			AdjustedCenter2f = cv::Point2f(0.0,0.0);
			AdjustedCenter = cv::Point(0,0);
			DeviceCenter = cv::Point(0,0);
			Array = 0;
			Section = 0;
			SectionPosition = cv::Point(0,0);
			Guard = cv::Point(0,0);
			OnNexusList = false;
			UsedAsBase = false;
			NexusListIdx = -1;
			NexusIdx = -1;
			Shape.Clear();
		}
		
		bool ApplyRotation(double CosAngle, double SinAngle, cv::Size ImageSize )
		{
			double newX = (InputCenter2f.x * CosAngle - InputCenter2f.y * SinAngle );
			double newY = (InputCenter2f.x * SinAngle + InputCenter2f.y * CosAngle );
			RotatedCenter2f = cv::Point2f((float)newX, (float)newY);
			AdjustedCenter2f = RotatedCenter2f;
			AdjustedCenter.x = (int)round(AdjustedCenter2f.x);
			AdjustedCenter.y = (int)round(AdjustedCenter2f.y);
			cv::Point pTmp = cv::Point((int)round(newX), (int)round(newY));
			
			if ( pTmp.x < 1 || pTmp.x > ImageSize.width - 2
				|| pTmp.y < 1 ||  pTmp.y > ImageSize.height - 2 )
			{
				OnImage = false;
			}
			else
			{
				OnImage = true;
			}
			
			return OnImage;
		}
		
		bool ApplyShiftScale(double ShiftX, double ShiftY, double Scale, cv::Size ImageSize )
		{
			DeviceCenter = cv::Point2f ( (float)round( (AdjustedCenter2f.x * Scale ) + ShiftX ), (float)round( (AdjustedCenter2f.y * Scale ) + ShiftY ) );
			if ( DeviceCenter.x < 1 || DeviceCenter.x > ImageSize.width - 2
				|| DeviceCenter.y < 1 || DeviceCenter.y > ImageSize.height - 2 )
			{
				OnImage = false;
			}
			else
			{
				OnImage = true;
			}
			
			return OnImage;
		}
		
		bool CalcShiftScale(double ShiftX, double ShiftY, double Scale, cv::Size ImageLimits, cv::Point &Result )
		{
			Result.x = (int)round( ( AdjustedCenter2f.x * Scale ) + ShiftX );
			Result.y = (int)round( ( AdjustedCenter2f.y * Scale ) + ShiftY );
			
			if ( Result.x < 1 || Result.x > ImageLimits.width - 2 || Result.y < 1 || Result.y > ImageLimits.height - 2 )
				return false;
			else
				return true;
		}
		
		bool CalcScale(double Scale, cv::Size ImageLimits, cv::Point &Result )
		{
			Result.x = (int)round( AdjustedCenter2f.x * Scale ) ;
			Result.y = (int)round( AdjustedCenter2f.y * Scale ) ;
			
			if ( Result.x < 1 || Result.x > ImageLimits.width - 2 || Result.y < 1 || Result.y > ImageLimits.height - 2 )
				return false;
			else
				return true;
		}
};

class BlobSummary	// With the exception of ShapeSummary, all member variables have the same meaning as in Blob
{
	public:
		int Status;
		bool Occupied;
		int SelfIndex;
		bool OnImage;
		cv::Point2f InputCenter2f;
		cv::Point2f RotatedCenter2f;
		cv::Point2f AdjustedCenter2f;
		cv::Point2f DeviceCenter;
		
		int Array;
		int Section;
		cv::Point SectionPosition;
		cv::Point Guard;
		
		BlobShapeSummary ShapeSummary;	// Summary of Shape information.  This does not include
														//     pixel locations, intensities, the blob contour or filling calculation details.
		
		BlobSummary(): Status(0), Occupied(false), SelfIndex(-1), OnImage(false), InputCenter2f(cv::Point2f(0.0,0.0)), 
			RotatedCenter2f(cv::Point2f(0.0,0.0)), AdjustedCenter2f(cv::Point2f(0.0,0.0)),
			DeviceCenter(cv::Point(0,0)),
			Array(0), Section(0), SectionPosition(cv::Point(0,0)), Guard(cv::Point(0,0))
		{
			ShapeSummary.Clear();
		}
		~BlobSummary(){}
		
		BlobSummary& operator= ( const BlobSummary &Source )
		{
			if ( this !=&Source )
			{
				Status = Source.Status;
				Occupied = Source.Occupied;
				SelfIndex = Source.SelfIndex;
				OnImage = Source.OnImage;
				InputCenter2f = Source.InputCenter2f;
				RotatedCenter2f = Source.RotatedCenter2f;
				AdjustedCenter2f = Source.AdjustedCenter2f;
				DeviceCenter = Source.DeviceCenter;
				Array = Source.Array;
				Section = Source.Section;
				SectionPosition = Source.SectionPosition;
				Guard = Source.Guard;
				ShapeSummary = Source.ShapeSummary;
			}
			return *this;	
		}
		
		BlobSummary& operator= ( Blob &Source )
		{
			Status = Source.Status;
			Occupied = false;
			SelfIndex = Source.SelfIndex;
			OnImage = Source.OnImage;
			InputCenter2f = Source.InputCenter2f;
			RotatedCenter2f = Source.RotatedCenter2f;
			AdjustedCenter2f = Source.AdjustedCenter2f;
			DeviceCenter = Source.DeviceCenter;
			Array = Source.Array;
			Section = Source.Section;
			SectionPosition = Source.SectionPosition;
			Guard = Source.Guard;
			ShapeSummary = Source.Shape;

			return *this;	
		}
		
		BlobSummary( const BlobSummary &Source ): Status(Source.Status), 
			Occupied(Source.Occupied),
			SelfIndex(Source.SelfIndex),
			OnImage(Source.OnImage),
			InputCenter2f(Source.InputCenter2f), 
			RotatedCenter2f(Source.RotatedCenter2f),
			AdjustedCenter2f(Source.AdjustedCenter2f),
			DeviceCenter(Source.DeviceCenter),
			Array(Source.Array),
			Section(Source.Section), SectionPosition(Source.SectionPosition),
			Guard(Source.Guard)
		{
			ShapeSummary = Source.ShapeSummary;
		}
		
		void Clear()
		{
			Status = 0;
			SelfIndex = -1;
			OnImage = false;
			InputCenter2f = cv::Point2f(0.0,0.0);
			RotatedCenter2f = cv::Point2f(0.0,0.0);
			AdjustedCenter2f = cv::Point2f(0.0, 0.0);
			DeviceCenter = cv::Point(0, 0);
			Array = 0;
			Section = 0;
			SectionPosition = cv::Point(0,0);
			Guard = cv::Point(0,0);
			ShapeSummary.Clear();
		}
};

#endif // BLOBANALYSIS_H

