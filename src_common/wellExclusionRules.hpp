#ifndef WELLEXCLUSIONRULES_H
#define WELLEXCLUSIONRULES_H
//! [includes]

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//! [includes]

//! [namespace]
// using namespace std;
//using namespace cv;
//! [namespace]

class WellExclusionRules
{
	public:
		int Array;
		int Section;
		int LeftCol;
		int RightCol;
		int TopRow;
		int BotRow;
		int Corner;
		int ExcludeSecondIfFirstMissing;
			
		WellExclusionRules(): Array(-1), Section(-1), LeftCol(0), RightCol(0), TopRow(0), BotRow(0), Corner(0), ExcludeSecondIfFirstMissing(0)
		{
		}
		~WellExclusionRules(){}
		
		WellExclusionRules(const WellExclusionRules &Source): Array(Source.Array), Section(Source.Section), 
			LeftCol(Source.LeftCol), RightCol(Source.RightCol), TopRow(Source.TopRow), BotRow(Source.BotRow),
			Corner(Source.Corner), ExcludeSecondIfFirstMissing(Source.ExcludeSecondIfFirstMissing)
		{
		}

		WellExclusionRules& operator=(const WellExclusionRules &Source)
		{
			if ( this != &Source )
			{
				Array = Source.Array;
				Section = Source.Section;
				LeftCol = Source.LeftCol;
				RightCol = Source.RightCol;
				TopRow = Source.TopRow;
				BotRow = Source.BotRow;
				Corner = Source.Corner;
				ExcludeSecondIfFirstMissing = Source.ExcludeSecondIfFirstMissing;
			}
			return *this;
		}
		
		void Clear()
		{
			Array = -1;
			Section = -1;
			LeftCol = 0;
			RightCol = 0;
			TopRow = 0;
			BotRow = 0;
			Corner = 0;
			ExcludeSecondIfFirstMissing = 0;
		}
};
	
#endif // WELLEXCLUSIONRULES_H

