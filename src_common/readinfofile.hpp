#ifndef READINFOFILE_HPP
#define READINFOFILE_HPP
//! [includes]
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//#include <iostream>
//#include <string>
//#include <cstdlib>
//! [includes]

#include "deviceOrigins.hpp"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	Used to hold Array/Section pairs which are expected to be present in the image. 
class ArraySection
{
	public:
		int Array;
		int Section;
		
		ArraySection(): Array(0), Section(0) 
		{}
		~ArraySection(){}
		
		ArraySection(const ArraySection &Source):
		Array(Source.Array), Section(Source.Section)
		{}
		
		ArraySection& operator= ( const ArraySection &Source )
		{
			if ( this !=&Source )
			{
				Array = Source.Array;
				Section = Source.Section;
			}
			return *this;
		}
};

class GuardInfo
{
	public:
		int Index;			// Guard index
		double Width;		// Width of Guard
		double Location;	// Location of center of guard (this is column coordinate for XGuards and row coordinate for YGuards)
		int PrevArea;		// Index of previous guard, -1 if this is first guard
		int NextArea;		// Index of next guard, -1 if this is the last guard
		
		GuardInfo(): Index(-1), Width(10.0), Location(10.0), PrevArea(0), NextArea(0)
		{}
		~GuardInfo(){}
		
		GuardInfo& operator= ( const GuardInfo &Source )
		{
			if ( this !=&Source )
			{
				Index = Source.Index;
				Width = Source.Width;
				Location = Source.Location;
				PrevArea = Source.PrevArea;
				NextArea = Source.NextArea;
			}
			return *this;
		}
		
		GuardInfo( const GuardInfo &Source )
		{
			Index = Source.Index;
			Width = Source.Width;
			Location = Source.Location;
			PrevArea = Source.PrevArea;
			NextArea = Source.NextArea;
		}
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Limits on (X-Offset, Y-Offset, Angle, Scale).  There is one of these for each parameter.  This is currently used to
// record the initial values of each parameters and the maximum allowed variation in X-Offset and Y-Offset.  The limits
// are not currently used for Angle or Scale and are present only for comparison with the angle and scale which the
// program obtains.  The BaseValue of Scale is not used.
//
class ParameterLimit
{
	public:
		bool Use;			// Variable not used
		double BaseValue;	// Initial value of parameter, 
		double Limit;		//
		
		ParameterLimit(): Use(true), BaseValue(-1000.0), Limit(10.0) {}
		~ParameterLimit(){}
		
		ParameterLimit& operator= ( const ParameterLimit &Source )
		{
			if ( this !=&Source )
			{
				Use = Source.Use;
				BaseValue = Source.BaseValue;
				Limit = Source.Limit;
			}
			return *this;
		}
		
		ParameterLimit(const ParameterLimit &Source)
		{
			Use = Source.Use;
			BaseValue = Source.BaseValue;
			Limit = Source.Limit;
		}
};
void InputError(std::string& FileName, const std::string& Message, std::string& ErrorMsg);

void InputError(std::string& FileName, const std::string& Message, int N, std::string& ErrorMsg);

bool ReadDeviceInfo(std::string InfoFileName,
	std::vector< DeviceOrigins > &ArrayOrigins,
	std::vector< DeviceOrigins > &SectionOrigins, std::vector< cv::Size > &SectionDimens,
	std::vector< GuardInfo > &XGuards, std::vector< GuardInfo > &YGuards, 
	std::string &ErrorMsg);

bool WriteDeviceInfo(std::string InfoFileName,
	std::vector< DeviceOrigins > &ArrayOrigins,
	std::vector< DeviceOrigins > &SectionOrigins, std::vector< cv::Size > &SectionDimens,
	std::vector< GuardInfo > &XGuards, std::vector< GuardInfo > &YGuards, 
	std::string &ErrorMsg);	

bool ReadSubImageData(std::string SubImageFileName, int NumbArrays, std::vector<ArraySection> &ArrSecList,
	int &NumbArraysUsed, cv::Rect &ImageAnalysisArea, cv::Rect &SubImageArea,
	cv::Size2f &WellSize, double &WellArea, cv::Point2f &WellSpacing, //double &WellVolume, 
	std::vector< ParameterLimit > &PLimits, double &BlockSizeMult, double &MinimumPeak, double &MaxFillingRatio,
	std::string &ErrorMsg );	

#endif // READINFOFILE_HPP

