/////////////////////////////////////////////////////////////////////////////////////////
//
// sdpcrDropletCheck
//
// Program inputs an image of device and identifies blobs in the image which resemble the
// wells which are expected to be present.  It then aligns the image with a template 
// created from the entire device and assigns those blobs which are aligned with a well.
// Each ID for each well includes its array, its section and the row and column position
// within the section. The columns are parallel to the direction of flow in the device.
// Each image is typically of only a portion of the device, and is referred to as a 
// subimage.
// 
// The arguments on the command line are (string arguments cannot contain spaces)
//
// <Parameter File Name> 
// <Before Sizing File Name> - The full name of .csv file containing information about 
//			before (sizing) image
// <After Sizing File Name> - The full name of .csv file containing information about 
//			after (sizing) image
// <Prefix> - The program reads files with information about the device.  The names of all of
//           hese files start with <Prefix>
// <SubImage Label> - The program also reads files with information about the portion of the
//          device being imaged. There can be different <SubImage Label>s for the before and
//			after images.  It doesn't matter which one is used.
// <BaseName> - All of the output files (except for std::clog) start with BaseName
// <Exclude Edge Wells> - (bool) If true, then wells on the left and right columns and top and bottom
//			rows of each section are not included in analysis
// <Isolated Wells> - (int) For wells which are not on an edge (or all wells if <Exclude Edge Wells>
//			if false), this is the minimum number of neighbor wells which a well must have before
//			it gets included in the analysis.  This cannot be less than zero and obviousy should
//			not exceed 8.
// <Anonmalous Intensity> - (double) If the top intensity of a blob in the sizing image is larger
//			than its closest neighbors by a multiplicative factor of <Anonmalous Intensity>, 
//			exclude that well from analysis.
// [<Seed>] - (long int) random number generator seed.  random number generator is used to 
//			generate simulated errors to estimate the errors in the best fit result.
//
//! [includes]
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "physicalconstants.h"
#include "statistics.hpp"
#include "blob.hpp"
#include "readinfofile.hpp"
#include "wellExclusionRules.hpp"
#include "dropletMatch.hpp"
// #include "segmentVector.hpp"
//! [includes]

//! [namespace]
// using namespace std;
//using namespace cv;
//! [namespace]

void CreateErrMessage(std::string &FileName, const std::string &Message, std::string &ErrorMsg)
{
	std::ostringstream oString;
	oString.str("");
	oString.clear();
	oString << Message << " from " << FileName;
	ErrorMsg = oString.str();
}
		
bool ReadDropletCheckFile( std::string ParameterFileName,
	double &ShrinkageLimit, double &BeforeExclusionFactor,
	double &PosNegRatio, double &RatioDefaultDivider, double &StreakExpectancyThreshold,
	std::vector< WellExclusionRules > &ExclusionRules, std::string &ErrorMsg)
{
	std::istringstream token;
	std::string line;
	std::ostringstream oString;
	WellExclusionRules newRule;
	ExclusionRules.clear();

	std::ifstream parameterFile(ParameterFileName.c_str());
	if( ! parameterFile ) 
	{
		oString.str("");
		oString.clear();
		oString << "Error opening " << ParameterFileName;
		ErrorMsg = oString.str();
		return false;
	}

	//int itemp;
	getline( parameterFile, line);
	if ( !parameterFile.good() )
	{
		parameterFile.close();
		CreateErrMessage(ParameterFileName, "Error reading parameter file", ErrorMsg);
		return false;
	}	
	getline( parameterFile, line);
	token.clear(); token.str(line); token >> ShrinkageLimit;
	if ( token.fail() )
	{
		parameterFile.close();
		CreateErrMessage(ParameterFileName, "Error reading Shrinkage Limit", ErrorMsg);
		return false;
	}
	
	getline( parameterFile, line);
	if ( !parameterFile.good() )
	{
		parameterFile.close();
		CreateErrMessage(ParameterFileName, "Error reading parameter file", ErrorMsg);
		return false;
	}	
	getline( parameterFile, line);
	token.clear(); token.str(line); token >> BeforeExclusionFactor;
	if ( token.fail() )
	{
		parameterFile.close();
		CreateErrMessage(ParameterFileName, "Error reading Before Exclusion Factor", ErrorMsg);
		return false;
	}
	
	getline( parameterFile, line);
	if ( !parameterFile.good() )
	{
		parameterFile.close();
		CreateErrMessage(ParameterFileName, "Error reading parameter file", ErrorMsg);
		return false;
	}	
	getline( parameterFile, line);
	token.clear(); token.str(line); token >> PosNegRatio;
	if ( token.fail() )
	{
		parameterFile.close();
		CreateErrMessage(ParameterFileName, "Error reading Positive/Negative Ratio", ErrorMsg);
		return false;
	}
	
	getline( parameterFile, line);
	if ( !parameterFile.good() )
	{
		parameterFile.close();
		CreateErrMessage(ParameterFileName, "Error reading parameter file", ErrorMsg);
		return false;
	}	
	getline( parameterFile, line);
	token.clear(); token.str(line); token >> RatioDefaultDivider;
	if ( token.fail() )
	{
		parameterFile.close();
		CreateErrMessage(ParameterFileName, "Error reading Ratio Default Divider", ErrorMsg);
		return false;
	}
	// std::clog << RatioDefaultDivider << std::endl;
	getline( parameterFile, line);
	if ( !parameterFile.good() )
	{
		parameterFile.close();
		CreateErrMessage(ParameterFileName, "Error reading parameter file", ErrorMsg);
		return false;
	}	
	getline( parameterFile, line);
	token.clear(); token.str(line); token >> StreakExpectancyThreshold;
	if ( token.fail() )
	{
		parameterFile.close();
		CreateErrMessage(ParameterFileName, "Error reading Streak Expectancy Threshold", ErrorMsg);
		return false;
	}
	// std::clog << StreakExpectancyThreshold << std::endl;
	getline( parameterFile, line);
	if ( !parameterFile.good() )
	{
		parameterFile.close();
		CreateErrMessage(ParameterFileName, "Error reading parameter file", ErrorMsg);
		return false;
	}
	getline( parameterFile, line);
	if ( !parameterFile.good() )
	{
		parameterFile.close();
		CreateErrMessage(ParameterFileName, "Error reading parameter file", ErrorMsg);
		return false;
	}
	// std::clog << line << std::endl;
	while ( getline(parameterFile, line ) )
	{
		// std::clog << line << "\n";
		if ( line.find('#') != std::string::npos )
		{
			break;		
		}
		else
		{
			token.clear(); token.str(line); token >> newRule.Array >> newRule.Section >> newRule.LeftCol
				>> newRule.RightCol >> newRule.TopRow >> newRule.BotRow >> newRule.Corner >> newRule.ExcludeSecondIfFirstMissing;
			if ( token.fail() )
			{
				parameterFile.close();
				CreateErrMessage(ParameterFileName, "Error reading rule from parameter file", ErrorMsg);
				return false;
			}
			else
			{
				ExclusionRules.push_back(newRule);
				// getline(parameterFile, line );
			}
		}
	}
	parameterFile.close();
	if ( ExclusionRules.size() < 1 )
	{
		CreateErrMessage(ParameterFileName, "No rules in parameter file", ErrorMsg);
		return false;
	}
	
	return true;
}

void CreateAlignmentImages(std::ofstream &LogFile, std::vector< ArraySection > &ArrSecList, int NumbRows, int NumbCols,
	std::vector< std::vector< BlobSummary > > &BeforeSizingList, 
	std::vector< std::vector< BlobSummary > > &AfterSizingList, 
	std::vector< cv::Mat > &BeforeOccupied, std::vector< cv::Mat > &AfterOccupied )
{
	int numbRows1 = 2 * NumbRows + 1;
	int numbCols1 = 2 * NumbCols + 1;
	cv::Size alignSize = cv::Size(numbCols1, numbRows1);
	BeforeOccupied.resize(ArrSecList.size()); //(sectionDimensPlus1, CV_32S);
	AfterOccupied.resize(ArrSecList.size());  //sectionDimensPlus1, CV_32S);
	for ( int k = 0 ; k < ArrSecList.size() ; k++ )
	{
		BeforeOccupied[k].create(alignSize, CV_8U);
		AfterOccupied[k].create(alignSize, CV_8U);
		BeforeOccupied[k].setTo(0);
		AfterOccupied[k].setTo(0);
	}
	
	for ( int k = 0 ; k < ArrSecList.size() ; k++ )
	{
		for ( int m = 0 ; m < BeforeSizingList[k].size() ; m++ )
		{
			// if ( BeforeSizingList[k][m].Array == ArrSecList[k].Array && BeforeSizingList[k][m].Section == ArrSecList[k].Section )
				BeforeOccupied[k].at<uchar>(BeforeSizingList[k][m].SectionPosition) = 255;
		}
		
		for ( int m = 0 ; m < AfterSizingList[k].size() ; m++ )
		{
			// if ( AfterSizingList[k][m].Array == ArrSecList[k].Array && AfterSizingList[k][m].Section == ArrSecList[k].Section )
				AfterOccupied[k].at<uchar>(AfterSizingList[k][m].SectionPosition) = 255;
		}
	}
}


bool AlignDroplets(std::ofstream &LogFile, std::string &BaseName, std::vector< ArraySection > &ArrSecList, int NumbRows, int NumbCols,
	std::vector< std::vector< BlobSummary > > &BeforeSizingList, 
	std::vector< std::vector< BlobSummary > > &AfterSizingList, 
	cv::Point &ShiftResult, std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter, int &MaxNumbMatches )
{	
	MaxNumbMatches = 0;
	if ( BeforeSizingList.size() == 0 || AfterSizingList.size() == 0 )
		return false;
	
	std::vector< cv::Mat > beforeOccupied;
	std::vector< cv::Mat > afterOccupied;
	CreateAlignmentImages(LogFile, ArrSecList, NumbRows, NumbCols, 
		BeforeSizingList, AfterSizingList,
		beforeOccupied, afterOccupied);
	
	std::ostringstream oString;
	// std::string outName;
	LogFile << "Inside AlignImage, BaseName = " << BaseName << "\n";
	// std::cout << "ArrSecList.size() = " << ArrSecList.size() << "\n";
	// for ( int k = 0 ; k < ArrSecList.size() ; k++ )
	// {
		// oString.str("");
		// oString.clear();
		// oString << BaseName << "_" << k << "_beforeOccupied.png";
		// cv::imwrite(oString.str(), beforeOccupied[k]);
		
		// oString.str("");
		// oString.clear();
		// oString << BaseName << "_" << k << "_afterOccupied.png";
		// cv::imwrite(oString.str(), afterOccupied[k]);
	// }
	// for ( int k = 0 ;  k < BeforeSizingList.size() ; k++ )
	// {
		// std::cout << BeforeSizingList[k].size() << "\n";
		// int ll = 100;
		// if ( ll > BeforeSizingList[k].size() )
			// ll = BeforeSizingList[k].size();
		
		// for ( int n = 0 ; n < ll ; n++ )
			// std::cout << k << "/" << n << " : " 
				// << BeforeSizingList[k][n].Array << "/" << BeforeSizingList[k][n].Section << "/" 
				// << BeforeSizingList[k][n].SectionPosition.y << "/" << BeforeSizingList[k][n].SectionPosition.x << " -- "
				// << BeforeSizingList[k][n].SelfIndex << "\n";
	// }
	cv::Point beforeShift = cv::Point(0,0);
	cv::Point afterShift = cv::Point(0,0);
	cv::Point baseOffset = cv::Point(1,1);
	cv::Rect beforeRect;
	cv::Rect afterRect;
	cv::Mat beforeMat;
	cv::Mat afterMat;
	cv::Mat resultMat;
	afterRect.width = beforeRect.width = NumbCols;
	afterRect.height = beforeRect.height = NumbRows;
	
	MaxNumbMatches = -1;
	beforeRect.x = 1;
	beforeRect.y = 1;
	bool useAfterShift = true;
	for ( int n = 0 ; n < NumbCols ; n++ )
	{
		for ( int m = 0 ; m < NumbRows ; m++ )
		{
			afterRect.x = baseOffset.x + n;
			afterRect.y = baseOffset.y + m;
			int numbMatches = 0;
			for ( int k = 0 ; k < ArrSecList.size() ; k++ )
			{
				beforeMat = beforeOccupied[k](beforeRect);
				afterMat = afterOccupied[k](afterRect);
				cv::bitwise_and(beforeMat, afterMat, resultMat);
				numbMatches += cv::countNonZero(resultMat);
			}
			if ( numbMatches > MaxNumbMatches )
			{
				MaxNumbMatches = numbMatches;
				afterShift = cv::Point(-n, -m);
			}
			// LogFile << "aftershift: " << n << " / " << m << " = " << numbMatches << ", afterRect: " << afterRect << std::endl;
		}
	}
	afterRect.x = 1;
	afterRect.y = 1;
	for ( int n = 0 ; n < NumbCols ; n++ )
	{
		for ( int m = 0 ; m < NumbRows ; m++ )
		{
			beforeRect.x = baseOffset.x + n;
			beforeRect.y = baseOffset.y + m;
			int numbMatches = 0;
			for ( int k = 0 ; k < ArrSecList.size() ; k++ )
			{
				beforeMat = beforeOccupied[k](beforeRect);
				afterMat = afterOccupied[k](afterRect);
				cv::bitwise_and(beforeMat, afterMat, resultMat);
				numbMatches += cv::countNonZero(resultMat);
			}
			if ( numbMatches > MaxNumbMatches )
			{
				MaxNumbMatches = numbMatches;
				beforeShift = cv::Point(-n, -m);
				useAfterShift = false;
			}
			// LogFile << "beforeshift: " << n << " / " << m << " = " << numbMatches << ", beforeRect: " << beforeRect << std::endl;
		}
	}
	
	cv::Size alignSize = cv::Size(NumbCols+1, NumbRows+1);
	// for ( int k = 0 ; k < ArrSecList.size() ; k++ )
	// {
		// beforeOccupied[k].create(alignSize, CV_32S);
		// afterOccupied[k].create(alignSize, CV_32S);
		// beforeOccupied[k].setTo(-1);
		// afterOccupied[k].setTo(-1);
	// }
	
	if ( useAfterShift )
	{
		ShiftResult = afterShift;
		// for ( int k = 0 ; k < ArrSecList.size() ; k++ )
		// {
			// for ( int m = 0 ; m < BeforeSizingList.size() ; m++ )
			// {
				// if ( BeforeSizingList[k][m].Array == ArrSecList[k].Array && BeforeSizingList[k][m].Section == ArrSecList[k].Section )
					// beforeOccupied[k].at<int>(BeforeSizingList[k][m].SectionPosition) = BeforeSizingList[k][m].SelfIndex;
			// }
			
			// for ( int m = 0 ; m < AfterSizingList.size() ; m++ )
			// {
				// if ( AfterSizingList[k][m].Array == ArrSecList[k].Array && AfterSizingList[k][m].Section == ArrSecList[k].Section )
				// {
					// cv::Point newPoint = AfterSizingList[k][m].SectionPosition + afterShift;
					// if ( newPoint.x > 0 && newPoint.y > 0 && newPoint.x <= NumbCols && newPoint.y <= NumbRows )
						// afterOccupied[k].at<int>(newPoint) = AfterSizingList[k][m].SelfIndex;
				// }
			// }
		// }
	}
	else
	{
		ShiftResult.x = -beforeShift.x;
		ShiftResult.y = -beforeShift.y;
		// for ( int k = 0 ; k < ArrSecList.size() ; k++ )
		// {
			// for ( int m = 0 ; m < BeforeSizingList.size() ; m++ )
			// {
				// if ( BeforeSizingList[k][m].Array == ArrSecList[k].Array && BeforeSizingList[k][m].Section == ArrSecList[k].Section )
				// {
					// cv::Point newPoint = BeforeSizingList[k][m].SectionPosition + beforeShift;
					// if ( newPoint.x > 0 && newPoint.y > 0 && newPoint.x <= NumbCols && newPoint.y <= NumbRows )
						// beforeOccupied[k].at<int>(BeforeSizingList[k][m].SectionPosition) = BeforeSizingList[k][m].SelfIndex;
				// }
			// }
			
			// for ( int m = 0 ; m < AfterSizingList.size() ; m++ )
			// {
				// if ( AfterSizingList[k][m].Array == ArrSecList[k].Array && AfterSizingList[k][m].Section == ArrSecList[k].Section )
				// {
					// afterOccupied[k].at<int>(AfterSizingList[k][m].SectionPosition) = AfterSizingList[k][m].SelfIndex;
				// }
			// }
		// }
	}
	// int numbArrays = 0;
	// int numbSections = 0;
	// for ( int n = 0 ; n < ArrSecList.size() ; n++ )
	// {
		// if ( ArrSecList[n].Array > numbArrays )
			// numbArrays = ArrSecList[n].Array;
		// if ( ArrSecList[n].Section > numbSections )
			// numbSections = ArrSecList[n].Section;
	// }
	// std::vector< std::vector< int > > arrSecMatrix;
	// arrSecMatrix.resize(numbArrays + 1);
	// for ( int n = 0 ; n < (int)arrSecMatrix.size() ; n++ )
	// {
		// arrSecMatrix[n].resize(numbSections+1);
		// std::fill(arrSecMatrix[n].begin(), arrSecMatrix[n].end(), 0);
	// }
	// for ( int n = 0 ; n < (int)ArrSecList.size() ; n++ )
		// arrSecMatrix[ArrSecList[n].Array][ArrSecList[n].Section] = 1;
	
	for ( int k = 0 ; k < ArrSecList.size() ; k++ )
	{	
		int a = ArrSecList[k].Array;
		int s = ArrSecList[k].Section;
		int r;
		int c;
		for ( int n = 0 ; n < BeforeSizingList[k].size() ; n++ )
		{
			if ( BeforeSizingList[k][n].Array == a && BeforeSizingList[k][n].Section == s )
			{
				c = BeforeSizingList[k][n].SectionPosition.x;
				r = BeforeSizingList[k][n].SectionPosition.y;
				// std::cout << k << "/" << n << " : " << a << "/" << s << "/" << r << "/" << c << " - ";
				if ( c > 0 && c <= NumbCols && r > 0 && r <= NumbRows )
				{
					BeforeAfter[a][s][r][c].BeforeIndex = BeforeSizingList[k][n].SelfIndex;
					 // std::cout << BeforeSizingList[k][n].SelfIndex << "\n";
				}
				else if (c > 0 && c < NumbCols+2 && r > 0 && r < NumbRows+2)
				{
					BeforeAfter[a][s][r][c].BeforeIndex = -1;
					// std::cout << "\n";
				}
			}
		}
		for ( int n = 0 ; n < AfterSizingList[k].size() ; n++ )
		{
			if ( AfterSizingList[k][n].Array == a && AfterSizingList[k][n].Section == s )
			{
				c = AfterSizingList[k][n].SectionPosition.x + ShiftResult.x;
				r = AfterSizingList[k][n].SectionPosition.y + ShiftResult.y;
				// if ( AfterSizingList[k][n].SelfIndex == 12 )
				// {
					// LogFile << "AfterSizingList[" << k << "][" << n << "].SelfIndex == 12\n";
				// }
				// else if (AfterSizingList[k][n].SelfIndex == 66)
				// {
					// LogFile << "AfterSizingList[" << k << "][" << n << "].SelfIndex == 66\n";
				// }
				if ( c > 0 && c <= NumbCols && r > 0 && r <= NumbRows )
					BeforeAfter[a][s][r][c].AfterIndex = AfterSizingList[k][n].SelfIndex;
				else if(c > 0 && c < NumbCols+2 && r > 0 && r < NumbRows+2)
					BeforeAfter[a][s][r][c].AfterIndex = -1;
			}			
		}
	}
	
	
	// for ( int n = 1 ; n <= numbArrays ; n++  )
	// {
		// for ( int m = 1 ; m <= numbSections ; m++ )
		// {
			// if ( arrSecMatrix[n][m] > 0 )
			// {
				// for ( int j = 1 ; j < BeforeAfter[n][m].size() ; j++ )
				// {
					// for ( int k = 2 ; k < BeforeAfter[n][m][j].size() ; k++ )
					// {
						// if ( BeforeAfter[n][m][j][k].BeforeIndex == BeforeAfter[n][m][j][k-1].BeforeIndex && BeforeAfter[n][m][j][k].BeforeIndex >= 0 )
						// {
							// LogFile << "BeforeAfter[" << n << "][" << m << "][" << j << "][" << k << "].BeforeIndex = " << BeforeAfter[n][m][j][k].BeforeIndex
								// << " is identical to index for column " << k-1 << "\n";
						// }
						// if ( BeforeAfter[n][m][j][k].AfterIndex == BeforeAfter[n][m][j][k-1].AfterIndex && BeforeAfter[n][m][j][k].AfterIndex >= 0 )
						// {
							// LogFile << "BeforeAfter[" << n << "][" << m << "][" << j << "][" << k << "].AfterIndex = " << BeforeAfter[n][m][j][k].AfterIndex
								// << " is identical to index for column " << k-1 << "\n";
						// }
					// }
				// }
			// }
		// }
	// }
	
	
	
	if ( ShiftResult.x == 0 && ShiftResult.y == 0 )
	{
		LogFile << "\n========== No shift needed to align before/after images " << "\n";
		return true;
	}
	else
	{
		LogFile << "\n========== Droplets in After Image were shifted!!\n";
		LogFile << "     Best ArraySectionShift is at (col, row) (" << ShiftResult.x << ", " << ShiftResult.y << "), numberMatched = " << MaxNumbMatches << "\n";
		LogFile << "==========\n";
		return false;
	}

	// for ( int k = 0 ; k < ArrSecList.size() ; k++ )
	// {
		// int a = ArrSecList[k].Array;
		// int s = ArrSecList[k].Section;
		// for ( int n = 1 ; n <= NumbCols ; n++ )
		// {
			// for ( int m = 1 ; m <= NumbRows ; m++ )
			// {
				// BeforeAfter[a][s][m][n].BeforeIndex = beforeOccupied[k].at<int>(cv::Point(n, m));
				// BeforeAfter[a][s][m][n].AfterIndex = afterOccupied[k].at<int>(cv::Point(n, m));
			// }
		// }
	// }
}
	
void EvaluateAlignment(std::ofstream &LogFile, std::vector< std::vector< int > > &ArrSecMatrix, int NumbRows, int NumbCols,
	std::vector< BlobSummary > &BeforeSizingList, std::vector< BlobSummary > &AfterSizingList,
	std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter, 
	std::vector< int > &BeforeColumns, std::vector< int > &BeforeRows,
	std::vector< int > &AfterColumns, std::vector< int > &AfterRows,
	std::vector< int > &BeforeColumnsMissing, std::vector< int > &BeforeRowsMissing,
	std::vector< int > &AfterColumnsMissing, std::vector< int > &AfterRowsMissing,
	int &BeforeFoundInAfter, int &AfterFoundInBefore, int &BeforeNotFoundInAfter, int &AfterNotFoundInBefore,
	StatisticsXY &DeltaXY, int &NumbMatches)
{
	DeltaXY.Clear();
	BeforeFoundInAfter = 0;
	AfterFoundInBefore = 0;
	BeforeNotFoundInAfter = 0;
	AfterNotFoundInBefore = 0;
	BeforeColumns.resize(NumbCols+1);
	BeforeRows.resize(NumbRows+1);
	AfterColumns.resize(NumbCols+1);
	AfterRows.resize(NumbRows+1);
	BeforeColumnsMissing.resize(NumbCols+1);
	BeforeRowsMissing.resize(NumbRows+1);
	AfterColumnsMissing.resize(NumbCols+1);
	AfterRowsMissing.resize(NumbRows+1);
	std::fill(BeforeColumns.begin(), BeforeColumns.end(), 0 );
	std::fill(BeforeRows.begin(), BeforeRows.end(), 0 );
	std::fill(AfterColumns.begin(), AfterColumns.end(), 0 );
	std::fill(AfterRows.begin(), AfterRows.end(), 0 );
	std::fill(BeforeColumnsMissing.begin(), BeforeColumnsMissing.end(), 0 );
	std::fill(BeforeRowsMissing.begin(), BeforeRowsMissing.end(), 0 );
	std::fill(AfterColumnsMissing.begin(), AfterColumnsMissing.end(), 0 );
	std::fill(AfterRowsMissing.begin(), AfterRowsMissing.end(), 0 );
	NumbMatches = 0;
	for ( int a = 1 ; a < BeforeAfter.size() ; a++ )
	{
		for ( int s = 1 ; s < BeforeAfter[a].size() ; s++ )
		{
			if ( ArrSecMatrix[a][s] > 0 )
			{
				for ( int r = 1 ; r <= NumbRows ; r++ )
				{
					for ( int c = 1 ; c <= NumbCols ; c++ )
					{
						int iBefore = BeforeAfter[a][s][r][c].BeforeIndex;
						int iAfter = BeforeAfter[a][s][r][c].AfterIndex;
						// std::cout << a << "/" << s << "/" << r << "/" << c << " - " << iBefore << "/" << iAfter << "\n";
						if ( iBefore >= 0 && iAfter >= 0 )
						{
							BeforeFoundInAfter++;
							AfterFoundInBefore++;
							BeforeColumns[c]++;
							BeforeRows[r]++;
							AfterColumns[c]++;
							AfterRows[r]++;
							double dx = BeforeSizingList[iBefore].InputCenter2f.x - AfterSizingList[iAfter].InputCenter2f.x;
							double dy = BeforeSizingList[iBefore].InputCenter2f.y - AfterSizingList[iAfter].InputCenter2f.y;
							DeltaXY.Accumulate(dx, dy);
							NumbMatches++;
						}
						else if ( iBefore >= 0 )
						{
							BeforeNotFoundInAfter++;
							BeforeColumns[c]++;
							BeforeRows[r]++;
							BeforeColumnsMissing[c]++;
							BeforeRowsMissing[r]++;
						}
						else if ( iAfter >= 0 )
						{
							AfterNotFoundInBefore++;
							AfterColumns[c]++;
							AfterRows[r]++;
							AfterColumnsMissing[c]++;
							AfterRowsMissing[r]++;
						}
					}
				}
			}
		}
	}
	DeltaXY.Analyze();
}

int ShiftSizingList(std::ofstream &LogFile, int Array, int Section, cv::Size SectionDimens,
	std::vector< BlobSummary > &AfterSizingList, cv::Point ArraySectionShift )
{
	int numbShiftedOff = 0;
	for ( int n = 0 ; n < AfterSizingList.size() ; n++ )
	{
		AfterSizingList[n].SectionPosition += ArraySectionShift;
		if ( AfterSizingList[n].SectionPosition.x < 1 || AfterSizingList[n].SectionPosition.x > SectionDimens.width 
			|| AfterSizingList[n].SectionPosition.y < 1 || AfterSizingList[n].SectionPosition.y > SectionDimens.height )
		{
			if ( AfterSizingList[n].Status < 0 )
				AfterSizingList[n].Status -= 4;
			else
				AfterSizingList[n].Status = -4;
			
			numbShiftedOff++;
		}
	}
	
	return numbShiftedOff;
}

int ShiftSizingList(std::ofstream &LogFile, int Array, int Section, cv::Size SectionDimens,
	std::vector< BlobSummary > &BeforeSizingList, std::vector< BlobSummary > &AfterSizingList, cv::Point ArraySectionShift )
{
	int firstBefore;
	int lastBefore;
	int firstAfter;
	int lastAfter;
	int numbShiftedOff = 0;
	if ( ArraySectionShift.y != 0 )
	{
		firstBefore = 99999999;
		lastBefore = -99999999;
		firstAfter = 99999999;
		lastAfter = -99999999;
		for ( int n = 0 ; n < BeforeSizingList.size() ; n++ )
		{
			if ( BeforeSizingList[n].SectionPosition.y < firstBefore )
				firstBefore = BeforeSizingList[n].SectionPosition.y;
			
			if ( BeforeSizingList[n].SectionPosition.y > lastBefore )
				lastBefore = BeforeSizingList[n].SectionPosition.y; 
		}
	
		for ( int n = 0 ; n < AfterSizingList.size() ; n++ )
		{
			if ( AfterSizingList[n].SectionPosition.y < firstAfter )
				firstAfter = AfterSizingList[n].SectionPosition.y;
			
			if ( AfterSizingList[n].SectionPosition.y > lastAfter )
				lastAfter = AfterSizingList[n].SectionPosition.y; 
		}
		
		if ( firstAfter + ArraySectionShift.y > 0 && lastAfter + ArraySectionShift.y < SectionDimens.height )
		{
			for ( int n = 0 ; n < AfterSizingList.size() ; n++ )
				AfterSizingList[n].SectionPosition.y += ArraySectionShift.y;
				
		}
		else if ( firstBefore - ArraySectionShift.y > 0 && lastBefore - ArraySectionShift.y < SectionDimens.height)
		{
			for ( int n = 0 ; n < BeforeSizingList.size() ; n++ )
				BeforeSizingList[n].SectionPosition.y += ArraySectionShift.y;

		}
		else if ( (ArraySectionShift.y < 0 && abs(firstAfter + ArraySectionShift.y) == 1 ) ||
			( ArraySectionShift.y > 0 && abs(lastAfter + ArraySectionShift.y - SectionDimens.height + 1) == 1) )
		{
			for ( int n = 0 ; n < AfterSizingList.size() ; n++ )
			{
				AfterSizingList[n].SectionPosition.y += ArraySectionShift.y;
				if ( AfterSizingList[n].SectionPosition.y < 1 || AfterSizingList[n].SectionPosition.y > SectionDimens.height )
				{
					if ( AfterSizingList[n].Status < 0 )
						AfterSizingList[n].Status -= 4;
					else
						AfterSizingList[n].Status = -4;
					
					numbShiftedOff++;
				}
			}
		}
		else if ( ( ArraySectionShift.y < 0 && abs(firstBefore + ArraySectionShift.y) == 1 ) ||
			( ArraySectionShift.y > 0 && abs(lastBefore + ArraySectionShift.y - SectionDimens.height + 1) == 1) )
		{
			for ( int n = 0 ; n < BeforeSizingList.size() ; n++ )
			{
				BeforeSizingList[n].SectionPosition.y += ArraySectionShift.y;
				if ( BeforeSizingList[n].SectionPosition.y < 1 || BeforeSizingList[n].SectionPosition.y > SectionDimens.height )
				{
					if ( BeforeSizingList[n].Status < 0 )
						BeforeSizingList[n].Status -= 4;
					else
						BeforeSizingList[n].Status = -4;
					
					numbShiftedOff++;
				}
			}
		}
		else
		{
			for ( int n = 0 ; n < AfterSizingList.size() ; n++ )
			{
				AfterSizingList[n].SectionPosition += ArraySectionShift;
				if ( AfterSizingList[n].SectionPosition.x < 1 || AfterSizingList[n].SectionPosition.x > SectionDimens.width 
					|| AfterSizingList[n].SectionPosition.y < 1 || AfterSizingList[n].SectionPosition.y > SectionDimens.height )
				{
					if ( AfterSizingList[n].Status < 0 )
						AfterSizingList[n].Status -= 4;
					else
						AfterSizingList[n].Status = -4;
					
					numbShiftedOff++;
				}
			}
		}
	}
	
	if ( ArraySectionShift.x != 0 )
	{
		firstBefore = 99999999;
		lastBefore = -99999999;
		firstAfter = 99999999;
		lastAfter = -99999999;
		for ( int n = 0 ; n < BeforeSizingList.size() ; n++ )
		{
			if ( BeforeSizingList[n].SectionPosition.x < firstBefore )
				firstBefore = BeforeSizingList[n].SectionPosition.x;
			
			if ( BeforeSizingList[n].SectionPosition.x > lastBefore )
				lastBefore = BeforeSizingList[n].SectionPosition.x; 
		}
	
		for ( int n = 0 ; n < AfterSizingList.size() ; n++ )
		{
			if ( AfterSizingList[n].SectionPosition.x < firstAfter )
				firstAfter = AfterSizingList[n].SectionPosition.x;
			
			if ( AfterSizingList[n].SectionPosition.x > lastAfter )
				lastAfter = AfterSizingList[n].SectionPosition.x; 
		}
		
		if ( firstAfter + ArraySectionShift.x > 0 && lastAfter + ArraySectionShift.x < SectionDimens.width )
		{
			for ( int n = 0 ; n < AfterSizingList.size() ; n++ )
				AfterSizingList[n].SectionPosition.x += ArraySectionShift.x;
				
		}
		else if ( firstBefore - ArraySectionShift.x > 0 && lastBefore - ArraySectionShift.x < SectionDimens.width)
		{
			for ( int n = 0 ; n < BeforeSizingList.size() ; n++ )
				BeforeSizingList[n].SectionPosition.x += ArraySectionShift.x;

		}
		else if ( (ArraySectionShift.x < 0 && abs(firstAfter + ArraySectionShift.x) == 1 ) ||
			( ArraySectionShift.x > 0 && abs(lastAfter + ArraySectionShift.x - SectionDimens.width + 1) == 1) )
		{
			for ( int n = 0 ; n < AfterSizingList.size() ; n++ )
			{
				AfterSizingList[n].SectionPosition.x += ArraySectionShift.x;
				if ( AfterSizingList[n].SectionPosition.x < 1 || AfterSizingList[n].SectionPosition.x > SectionDimens.width )
				{
					if ( AfterSizingList[n].Status < 0 )
						AfterSizingList[n].Status -= 4;
					else
						AfterSizingList[n].Status = -4;
					
					numbShiftedOff++;
				}
			}
		}
		else if ( ( ArraySectionShift.x < 0 && abs(firstBefore + ArraySectionShift.x) == 1 ) ||
			( ArraySectionShift.x > 0 && abs(lastBefore + ArraySectionShift.x - SectionDimens.width + 1) == 1) )
		{
			for ( int n = 0 ; n < BeforeSizingList.size() ; n++ )
			{
				BeforeSizingList[n].SectionPosition.x += ArraySectionShift.x;
				if ( BeforeSizingList[n].SectionPosition.x < 1 || BeforeSizingList[n].SectionPosition.x > SectionDimens.width )
				{
					if ( BeforeSizingList[n].Status < 0 )
						BeforeSizingList[n].Status -= 4;
					else
						BeforeSizingList[n].Status = -4;
					
					numbShiftedOff++;
				}
			}
		}
		else
		{
			for ( int n = 0 ; n < AfterSizingList.size() ; n++ )
			{
				AfterSizingList[n].SectionPosition += ArraySectionShift;
				if ( AfterSizingList[n].SectionPosition.x < 1 || AfterSizingList[n].SectionPosition.x > SectionDimens.width 
					|| AfterSizingList[n].SectionPosition.y < 1 || AfterSizingList[n].SectionPosition.y > SectionDimens.width )
				{
					if ( AfterSizingList[n].Status < 0 )
						AfterSizingList[n].Status -= 4;
					else
						AfterSizingList[n].Status = -4;
					
					numbShiftedOff++;
				}
			}
		}
	}
	
	return numbShiftedOff;
}

// bool AssignStatus( std::ofstream &LogFile, std::vector< BlobSummary > &SizingList,
	// int Array, int Section, std::vector< WellExclusionRules > ExclusionRules,
	// cv::Size SectionDimens, cv::Mat &BlobIndexMat )
// {
	// cv::Size sectionDimensPlus2;
	// sectionDimensPlus2.height = SectionDimens.height + 2;
	// sectionDimensPlus2.width = SectionDimens.width + 2;
	// BlobIndexMat.create(sectionDimensPlus2, CV_32S);
	// BlobIndexMat.setTo(-1);
	
	// if ( SizingList.size() == 0 )
		// return false;
	
	// cv::Mat occupiedMat;
	// cv::Mat edgeStatusMat;
	// occupiedMat.create(sectionDimensPlus2, CV_8U);
	// edgeStatusMat.create(sectionDimensPlus2, CV_8U);
	// occupiedMat.setTo(0);
	// edgeStatusMat.setTo(0);
	
	// for ( int n = 0 ; n < SizingList.size() ; n++ )
	// {
		// if ( SizingList[n].Array == Array && SizingList[n].Section == Section )
		// {
			// if ( SizingList[n].SectionPosition.x < 1 || SizingList[n].SectionPosition.x > SectionDimens.width 
				// || SizingList[n].SectionPosition.y < 1 || SizingList[n].SectionPosition.y > SectionDimens.height )
			// {
				// LogFile << "In AssignStatus - SizingList[" << n << "].SectionPosition = " << SizingList[n].SectionPosition << std::endl;
			// }
			// else
			// {
				// if ( SizingList[n].ShapeSummary[0].FilterValue == 1 )
					// SizingList[n].Status = 0;
				// else
					// SizingList[n].Status = -1;
				
				// occupiedMat.at<uchar>(SizingList[n].SectionPosition) = 1;
				// BlobIndexMat.at<int>(SizingList[n].SectionPosition) = n;
			// }
		// }
	// }
	
	// WellExclusionRules currentRules = ExclusionRules[0];
	// if ( ExclusionRules.size() > 1 )
	// {
		// for ( int n = 1 ; n < ExclusionRules.size() ; n++ )
		// {
			// if ( Array == ExclusionRules[n].Array )
			// {
				// if ( Section == ExclusionRules[n].Section || ExclusionRules[n].Section == 0 )
				// {
					// currentRules = ExclusionRules[n];
					// break;
				// }
			// }
			// else if ( ExclusionRules[n].Array == 0 && ExclusionRules[n].Section == Section )
			// {
				// currentRules = ExclusionRules[n];
				// break;
			// }
		// }
	// }
	
	// edgeStatusMat.col(1) = occupiedMat.col(1) * currentRules.LeftCol;
	// edgeStatusMat.col(SectionDimens.width) = occupiedMat.col(SectionDimens.width) * currentRules.RightCol;
	// edgeStatusMat.row(1) = occupiedMat.row(1) * currentRules.TopRow;
	// edgeStatusMat.row(SectionDimens.height) = occupiedMat.row(SectionDimens.height) * currentRules.BotRow;

	// edgeStatusMat.at<uchar>(cv::Point(currentRules.LeftCol+1, currentRules.TopRow+1)) = currentRules.Corner;
	// edgeStatusMat.at<uchar>(cv::Point(currentRules.LeftCol+1, SectionDimens.height - currentRules.BotRow)) = currentRules.Corner;
	// edgeStatusMat.at<uchar>(cv::Point(SectionDimens.width - currentRules.LeftCol, currentRules.TopRow+1)) = currentRules.Corner;
	// edgeStatusMat.at<uchar>(cv::Point(SectionDimens.width - currentRules.LeftCol, SectionDimens.height - currentRules.BotRow)) 
		// = currentRules.Corner;
		
	// std::vector< int > firstCol(SectionDimens.height+1);
	// std::vector< int > lastCol(SectionDimens.height+1);
	// std::vector< int > firstRow(SectionDimens.width+1);
	// std::vector< int > lastRow(SectionDimens.width+1);
	// std::fill( firstCol.begin(), firstCol.end(), 1);
	// std::fill( lastCol.begin(), lastCol.end(), SectionDimens.width);
	// std::fill( firstRow.begin(), firstRow.end(), 1);
	// std::fill( lastRow.begin(), lastRow.end(), SectionDimens.height);
	// for ( int n = 1 ; n <= SectionDimens.height ; n++ )
	// {
		// while ( occupiedMat.at<uchar>(cv::Point(firstCol[n],n)) == 0 && firstCol[n] < lastCol[n] )
			// firstCol[n]++;
		
		// while ( occupiedMat.at<uchar>(cv::Point(lastCol[n],n)) == 0 && lastCol[n] > firstCol[n] )
			// lastCol[n]--;
	// }
	
	// for ( int m = 1 ; m <= SectionDimens.width ; m++ )
	// {
		// while ( occupiedMat.at<uchar>(cv::Point(m, firstRow[m])) == 0 && firstRow[m] < lastRow[m] )
			// firstRow[m]++;

		// while ( occupiedMat.at<uchar>(cv::Point(lastRow[m], m)) == 0 && lastRow[m] > firstRow[m] )
			// lastRow[m]--;
	// }
	
	// cv::Mat workMat1;
	// int start;
	// int stripLength;
	// for ( int n = 1 ; n <= SectionDimens.height ; n++ )
	// {
		// if ( n == 1 )
		// {
			// start = 1;
			// stripLength = 2;
		// }
		// else if ( n == SectionDimens.height )
		// {
			// start = n - 1;
			// stripLength = 2;
		// }
		// else
		// {
			// start = n - 1;
			// stripLength = 3;
		// }
		
		// int first = 1;
		// int last = SectionDimens.width;
		// if ( currentRules.LeftCol > 0 )
		// {
			// while ( occupiedMat.at<uchar>(cv::Point(first,n)) == 0 && first < last )
				// first++;
			// if ( first < last && first > currentRules.LeftCol )
			// {
				// if ( cv::countNonZero(occupiedMat(cv::Rect(first-1,start,1,stripLength))) == 0 )
					// edgeStatusMat.at<uchar>(cv::Point(first, n)) = currentRules.LeftCol;
			// }
		// }
		
		// if ( currentRules.RightCol > 0 )
		// {
			// while ( occupiedMat.at<uchar>(cv::Point(last,n)) == 0 && last > first )
				// last--;
			// if ( first < last && last < SectionDimens.width - currentRules.RightCol + 1 )
			// {
				// if ( cv::countNonZero(occupiedMat(cv::Rect(last+1,start,1,stripLength))) == 0 )
					// edgeStatusMat.at<uchar>(cv::Point(last, n)) = currentRules.RightCol;
			// }
		// }
	// }
	
	// for ( int m = 1 ; m <= SectionDimens.width ; m++ )
	// {
		// if ( m == 1 )
		// {
			// start = 1;
			// stripLength = 2;
		// }
		// else if ( m == SectionDimens.width )
		// {
			// start = m - 1;
			// stripLength = 2;
		// }
		// else
		// {
			// start = m - 1;
			// stripLength = 3;
		// }
		
		// if ( m + 2 > SectionDimens.width )
			// stripLength = SectionDimens.width - start +1;
		// else
			// stripLength = m + 2 - start + 1;
		
		// int first = 1;
		// int last = SectionDimens.height;
		// if ( currentRules.TopRow > 0 )
		// {
			// while ( occupiedMat.at<uchar>(cv::Point(m,first)) == 0 && first < last )
				// first++;
			// if ( first < last && first > currentRules.TopRow )
			// {
					// if ( cv::countNonZero(occupiedMat(cv::Rect(start,first-1,stripLength,1))) == 0 )
						// edgeStatusMat.at<uchar>(cv::Point(m, first)) = currentRules.TopRow;
			// }
		// }
		// if ( currentRules.BotRow > 0 )
		// {
			// while ( occupiedMat.at<uchar>(cv::Point(m,last)) == 0 && last > first )
				// last--;
			// if ( first < last && last < SectionDimens.height - currentRules.BotRow + 1 )
			// {
				// if ( cv::countNonZero(occupiedMat(cv::Rect(start,last+1,stripLength,1))) == 0 )
					// edgeStatusMat.at<uchar>(cv::Point(m, last)) = currentRules.BotRow;
			// }
		// }
	// }

	// int maxStatus = currentRules.Corner;
	// if ( currentRules.LeftCol > maxStatus )
		// maxStatus = currentRules.LeftCol;
	// if ( currentRules.LeftCol > maxStatus )
		// maxStatus = currentRules.LeftCol;
	// if ( currentRules.RightCol > maxStatus )
		// maxStatus = currentRules.RightCol;
	// if ( currentRules.TopRow > maxStatus )
		// maxStatus = currentRules.TopRow;
	// if ( currentRules.BotRow > maxStatus )
		// maxStatus = currentRules.BotRow;

	// for ( int s = maxStatus ; s > 1 ; s-- )
	// {
		// int sMinus1 = s - 1;
		// for ( int n = 1 ; n <= SectionDimens.height ; n++ )
		// {
			// for ( int m = 1 ; m <= SectionDimens.width ; m++ )
			// {
				// if (edgeStatusMat.at<uchar>(cv::Point(m,n)) == s )
				// {
					// workMat1 = edgeStatusMat(cv::Rect(m-1,n-1,3,3));
					// if ( workMat1.at<uchar>(0,1) < sMinus1 )
						// workMat1.at<uchar>(0,1) = sMinus1 ;
					// if ( workMat1.at<uchar>(1,0) < sMinus1 )
						// workMat1.at<uchar>(1,0) = sMinus1 ;
					// if ( workMat1.at<uchar>(1,2) < sMinus1 )
						// workMat1.at<uchar>(1,2) = sMinus1 ;
					// if ( workMat1.at<uchar>(2,1) < sMinus1 )
						// workMat1.at<uchar>(2,1) = sMinus1 ;
				// }
			// }
		// }
	// }
	// for ( int n = 1 ; n <= SectionDimens.height ; n++ )
	// {
		// for ( int m = 1 ; m <= SectionDimens.width ; m++ )
		// {
			// if ( occupiedMat.at<uchar>(cv::Point(m,n)) > 0 )
			// {
				// if ( SizingList[BlobIndexMat.at<int>(cv::Point(m,n))].Status >= 0 )
					// SizingList[BlobIndexMat.at<int>(cv::Point(m,n))].Status = edgeStatusMat.at<uchar>(cv::Point(m,n));
			// }
		// }
	// }
	
	// return true;
// }

// void CheckShrinkageLimit(std::ofstream &LogFile, double &ShrinkageLimit,
	// cv::Mat &BeforeBlobIndexMat, std::vector< BlobSummary > &BeforeSizingList,
	// cv::Mat &AfterBlobIndexMat, std::vector< BlobSummary > &AfterSizingList )
// {
	// for ( int n = 0 ; n < BeforeBlobIndexMat.total() ; n++ )
	// {
		// int iBefore = BeforeBlobIndexMat.at<int>(n);
		// int iAfter = AfterBlobIndexMat.at<int>(n);
		// if ( iBefore >= 0 )
		// {
			// if ( iAfter >= 0 )
			// {
				// double ratio = (double)AfterSizingList[iAfter].ShapeSummary[0].Area /(double)BeforeSizingList[iBefore].ShapeSummary[0].Area;
				// AfterSizingList[iAfter].ShapeSummary[0].Shrinkage = ratio;
				// if ( ratio < ShrinkageLimit )
					// AfterSizingList[iAfter].Status -= 2;
					
				// BeforeSizingList[iBefore].Status = AfterSizingList[iAfter].Status;
			// }
			// else
				// BeforeSizingList[iBefore].Status = -2;
		// }	
	// }
// }

// void CalcAfterBeforeRatio(std::ofstream &LogFile, 
	// cv::Mat &BeforeBlobIndexMat, std::vector< BlobSummary > &BeforeSizingList,
	// cv::Mat &AfterBlobIndexMat, std::vector< BlobSummary > &AfterSizingList )
// {
	// for ( int n = 0 ; n < BeforeBlobIndexMat.total() ; n++ )
	// {
		// int iBefore = BeforeBlobIndexMat.at<int>(n);
		// int iAfter = AfterBlobIndexMat.at<int>(n);
		// if ( iBefore >= 0 && iAfter >= 0 )
		// {
			// if ( BeforeSizingList[iBefore].ShapeSummary[0].BlobIntensities.Sum > 1.0 )
				// AfterSizingList[iAfter].ShapeSummary[0].AfterBeforeRatio 
					// = AfterSizingList[iAfter].ShapeSummary[0].BlobIntensities.Sum / BeforeSizingList[iBefore].ShapeSummary[0].BlobIntensities.Sum;
			// else 
				// AfterSizingList[iAfter].ShapeSummary[0].AfterBeforeRatio = AfterSizingList[iAfter].ShapeSummary[0].BlobIntensities.Sum;
			
			// AfterSizingList[iAfter].ShapeSummary[0].BeforeFillRatio = BeforeSizingList[iBefore].ShapeSummary[0].FillRatio;
		// }
	// }
// }

double sortSummaryDelta = 7.0;
double sortSummaryDeltaCol = 5.0;
bool SortBlobSummary(BlobSummary A, BlobSummary B) 
{	
	if ( A.Array != 0 && B.Array != 0 )
	{
		if ( A.Array != B.Array )	
		return A.Array < B.Array;
		else if ( A.Section != B.Section )
			return A.Section < B.Section;
		else if ( A.SectionPosition.y != B.SectionPosition.y )
			return A.SectionPosition.y < B.SectionPosition.y;
		else
			return A.SectionPosition.x < B.SectionPosition.x;
	}
	else if ( A.Array == 0 && B.Array != 0 )
		return false;
	else if ( A.Array != 0 && B.Array == 0 )
		return true;
	else
	{
		if ( fabs( A.InputCenter2f.y - B.InputCenter2f.y ) > sortSummaryDelta )
			return A.InputCenter2f.y < B.InputCenter2f.y;
		else
			return A.InputCenter2f.x < B.InputCenter2f.x;
	}
}

bool SortBlobSummaryCol(BlobSummary A, BlobSummary B) 
{	
	if ( A.Array != 0 && B.Array != 0 )
	{
		if ( A.Array != B.Array )	
		return A.Array < B.Array;
		else if ( A.Section != B.Section )
			return A.Section < B.Section;
		else if ( A.SectionPosition.x != B.SectionPosition.x )
			return A.SectionPosition.x < B.SectionPosition.x;
		else
			return A.SectionPosition.y < B.SectionPosition.y;
	}
	else if ( A.Array == 0 && B.Array != 0 )
		return false;
	else if ( A.Array != 0 && B.Array == 0 )
		return true;
	else
	{
		if ( fabs( A.InputCenter2f.x - B.InputCenter2f.x ) > sortSummaryDelta )
			return A.InputCenter2f.x < B.InputCenter2f.x;
		else
			return A.InputCenter2f.y < B.InputCenter2f.y;
	}
}

bool TrimBox(cv::Rect &Rectangle, cv::Size RectSize, cv::Size ImageSize )
{
	if ( Rectangle.x > ImageSize.width - 2 || Rectangle.x + RectSize.width < 1 ||
		Rectangle.y > ImageSize.height - 2 || Rectangle.y + RectSize.height < 1 )
		return false;
	
	Rectangle.width = RectSize.width;
	Rectangle.height = RectSize.height;
	
	if ( Rectangle.x < 0 )
	{
		Rectangle.width += Rectangle.x;
		Rectangle.x = 0;
	}
	if ( Rectangle.x + Rectangle.width >= ImageSize.width )
		Rectangle.width = ImageSize.width - Rectangle.x;

	if ( Rectangle.y < 0 )
	{
		Rectangle.height += Rectangle.y;
		Rectangle.y = 0;
	}
	if ( Rectangle.y + Rectangle.height >= ImageSize.height )
		Rectangle.height = ImageSize.height - Rectangle.y;
	
	if ( Rectangle.width * Rectangle.height > 8 )
		return true;
	else
		return false;
}
