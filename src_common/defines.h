#ifndef DEFINES_H
#define DEFINES_H

// The first estimate of the background level is obtained by analyzing the backgroundFrac of pixels
//		with the smallest intensity to obtain an average and std dev of intensities for those pixels.
//		backgroundFrac is a user input fraction between 0 and 1 (non inclusive)  The first guess for
//		the background is 
//			<average> + STDMULTFORFIRSTBACKGNDCALC * <std. dev.>
//
#define STDMULTFORFIRSTBACKGNDCALC 2.5

// Number of variable parameters used in alignmenent of image (X-Offset, Y-Offset, Angle, Scale)
// Scale is not actually used as a variable parameter, however an approximate scale is calculated
//		which can be compared to the expected limits on the amount of scale
#define NUMBPARAMETERS 4


#endif  // DEFINES_H