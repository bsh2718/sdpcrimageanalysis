#ifndef ASSIGNBLOBSCD_HPP
#define ASSIGNBLOBSCD_HPP
//! [includes]

#include <cstdlib>
#include <vector>
#include <algorithm>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "blob_CD.hpp"
#include "adjacentCD.hpp"
#include "trimRectangle.hpp"
#include "deviceOrigins.hpp"

struct Search_Band
{
	cv::Point UpperLeft;
	cv::Point LowerRight;
};

class AssignBlobsCD
{
public:
	int NumbSections;
	int NumbSubSections;
	int SectionOfInterest;
	std::vector< cv::Point > SubSectionRowsCols;
	std::vector< cv::Point > SubSectionBands;
	std::vector< Search_Band > SearchBands;
	std::vector< float > SubSectionScale;
	std::vector< std::pair< cv::Point2f, cv::Point2f> > AdjustedSubSectionOrigins;
	std::vector< cv::Point > Tolerance;
	std::vector< cv::Point2f > WellTR_Spacing2f;
	std::vector< cv::Point > WellTR_SpacingI;
	std::vector< cv::Size2f > WellTR_Size2f;
	double DistanceToCenter;
	double ThetaResolution;
	cv::Size SearchImageSize;
	cv::Mat SearchImage;
	cv::Size SectionImageSize;
	cv::Mat SectionImage;
	std::vector< Blob_CD > Blob_CDList;
	std::vector< int > NumbOffsets;
	std::vector< std::vector < std::pair< int, int > > > HorzOffsetLimits;
	std::vector< std::vector < std::pair< int, int > > > VertOffsetLimits;
	
	std::vector< std::vector< Blob_CDNode > > BlobNodeList;
	std::vector< Blob_CDNode > UnalignedBlobNodeList;
	std::vector< cv::Point > CornerList;
	std::vector< cv::Point > SubSectionShift;
	std::vector< int > NumbInSubSection;
	std::vector< int > InitNodes;
	bool Track;

	AssignBlobsCD(): NumbSections(0), NumbSubSections(0), SectionOfInterest(1),
		DistanceToCenter(2000.0), ThetaResolution(0.0002), SearchImageSize(cv::Size(10,10)),
		SectionImageSize(cv::Size(10,10)), NumbOffsets(1), Track(false)
	{
		SubSectionRowsCols.clear();
		SubSectionBands.clear();
		SearchBands.clear();
		SubSectionScale.clear();
		AdjustedSubSectionOrigins.clear();
		Tolerance.clear();
		WellTR_Spacing2f.clear();
		WellTR_SpacingI.clear();
		WellTR_Size2f.clear();
		Blob_CDList.clear();
		HorzOffsetLimits.clear();
		VertOffsetLimits.clear();
		BlobNodeList.clear();
		UnalignedBlobNodeList.clear();
		CornerList.clear();
		SubSectionShift.clear();
		NumbInSubSection.clear();
		InitNodes.clear();
	}
	~AssignBlobsCD() {}

	AssignBlobsCD& operator= (const AssignBlobsCD& Source)
	{
		if (this != &Source)
		{
			NumbSections = Source.NumbSections;
			NumbSubSections = Source.NumbSubSections;
			SectionOfInterest = Source.SectionOfInterest;
			SubSectionRowsCols = Source.SubSectionRowsCols;
			SubSectionBands = Source.SubSectionBands;
			SearchBands = Source.SearchBands;
			SubSectionScale = Source.SubSectionScale;
			AdjustedSubSectionOrigins = Source.AdjustedSubSectionOrigins;
			Tolerance = Source.Tolerance;
			WellTR_Spacing2f = Source.WellTR_Spacing2f;
			WellTR_SpacingI = Source.WellTR_SpacingI;
			WellTR_Size2f = Source.WellTR_Size2f;
			DistanceToCenter = Source.DistanceToCenter;
			ThetaResolution = Source.ThetaResolution;
			SearchImageSize = Source.SearchImageSize;
			SearchImage = Source.SearchImage;
			Source.SearchImage.copyTo(SearchImage);
			SectionImageSize = Source.SectionImageSize;
			Source.SearchImage.copyTo(SearchImage);
			Blob_CDList = Source.Blob_CDList;
			NumbOffsets = Source.NumbOffsets;
			HorzOffsetLimits = Source.HorzOffsetLimits;
			VertOffsetLimits = Source.VertOffsetLimits;
			BlobNodeList = Source.BlobNodeList;
			CornerList = Source.CornerList;
			UnalignedBlobNodeList = Source.UnalignedBlobNodeList;
			SubSectionShift = Source.SubSectionShift;
			NumbInSubSection = Source.NumbInSubSection;
			InitNodes = Source.InitNodes;
			Track = Source.Track;
		}
		return *this;
	}

	AssignBlobsCD(const AssignBlobsCD& Source) : NumbSections(Source.NumbSections), NumbSubSections(Source.NumbSubSections),
		SectionOfInterest(Source.SectionOfInterest),
		Tolerance(Source.Tolerance), WellTR_Spacing2f(Source.WellTR_Spacing2f), WellTR_SpacingI(Source.WellTR_SpacingI), 
		WellTR_Size2f(Source.WellTR_Size2f), DistanceToCenter(Source.DistanceToCenter), ThetaResolution(Source.ThetaResolution),
		SearchImageSize(Source.SearchImageSize), NumbOffsets(Source.NumbOffsets),
		BlobNodeList(Source.BlobNodeList), UnalignedBlobNodeList(Source.UnalignedBlobNodeList), 
		CornerList(Source.CornerList), SubSectionShift(Source.SubSectionShift)
	{
		Source.SearchImage.copyTo(SearchImage);
		SectionImage = Source.SectionImage;
		SubSectionRowsCols = Source.SubSectionRowsCols;
		SubSectionBands = Source.SubSectionBands;
		SearchBands = Source.SearchBands;
		SubSectionScale = Source.SubSectionScale;
		AdjustedSubSectionOrigins = Source.AdjustedSubSectionOrigins;
		Blob_CDList = Source.Blob_CDList;
		HorzOffsetLimits = Source.HorzOffsetLimits;
		VertOffsetLimits = Source.VertOffsetLimits;
		NumbInSubSection = Source.NumbInSubSection;
		InitNodes = Source.InitNodes;
	}

	void Clear()
	{
		SubSectionRowsCols.clear();
		SubSectionBands.clear();
		SearchBands.clear();
		SubSectionScale.clear();
		AdjustedSubSectionOrigins.clear();
		Tolerance.clear();
		WellTR_Spacing2f.clear();
		WellTR_SpacingI.clear();
		WellTR_Size2f.clear();
		SearchImage.release();
		Blob_CDList.clear();
		HorzOffsetLimits.clear();
		VertOffsetLimits.clear();
		BlobNodeList.clear();
		UnalignedBlobNodeList.clear();
		CornerList.clear();
		SubSectionShift.clear();
		NumbInSubSection.clear();
		InitNodes.clear();
		Track = false;
	}

	int SectionSubSectionNumber(int Section, int SubSection);

	bool SectionSubSectionFromNumber(int SectionSubSectionNumber, int& Section, int& SubSection);

	bool Initialize(std::ofstream& LogFile, std::string BaseName, std::vector< double > Results,
		std::vector< Blob_CD >& BlobList, int SecOfInterest, std::vector< cv::Point > SubSecRowsCols, 
		cv::Mat &SectionImageIn, std::vector<cv::Point2f> WellSpacing, cv::Size2f WellSize,
		double DistToCenter, double ThetaResolu,
		std::vector< DeviceOrigins >& SectionOrigins,
		std::vector< DeviceOrigins >& SubSectionOrigins,
		double Scale, double ScaleLimit,
		bool Track, std::string& ErrMsg);

	//bool ConnectNodesSOI(std::ofstream& LogFile, std::string BaseName, int Array);

	bool ConnectNodes(std::ofstream& LogFile, std::string BaseName, int Array, int Section, bool Track = false);

	bool EvaluateNodes(std::ofstream& LogFile, int Section, int SubSection, std::string BaseName);

	bool DetermineSubSectionShift(std::ofstream& LogFile, std::string BaseName, int Section, int SubSection);

	//bool EvaluateNodes(std::ofstream& LogFile, std::string BaseName, int Array, int Section);

	//bool DetermineArraySectionShift(std::ofstream& LogFile, std::string BaseName, int Array, int Section);

	std::string SectionShiftsToString();

	int RecheckBlobs(std::ofstream& LogFile, std::vector< double > Results, double Scale);

	void GetBlob_CDList(std::vector< Blob_CD>& Blob_CDListOut);

	void UpdateBlobList(std::vector< Blob >& BlobList);

	void UpdateOtherBlobList(std::vector< Blob >& OtherBlobList, std::vector<Blob_CD>& OtherBlob_CDList);

	void AssignOtherBlobs(std::ofstream& LogFile, std::vector< Blob >& OtherBlobList, std::vector< Blob_CD >& OtherBlob_CDList,
		std::vector< double > Results, double Scale);

	bool EstimateScale(std::ofstream& LogFile, int SectionOfInterest, double& AveScale, double& XScale, double& YScale);

	int FindNextColumn(std::vector< std::vector< int > >& ArraySectionMatrix, int Row, int CurrColumn);

	int FindNextRow(std::vector< std::vector< int > >& ArraySectionMatrix, int CurrentRow, int Column);
	
	int GetSectionOfInterest();

	bool SetSectionOfInterest(int NewSOI);

	bool GetCorner11(int Section, int SubSection, cv::Point& Corner11);

	int GetNumberOfNodes(int Section, int SubSection);

};

struct AlignData
{
	int Index;
	int Length;
	int Count;
	Statistics1 Stats;
	double Spacing;
	bool SpacingOK;
};


#endif // ASSIGNBLOBSCD_HPP


	
