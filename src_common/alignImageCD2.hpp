#pragma once
//! [includes]

#include <cstdlib>
#include <vector>
#include <algorithm>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "blob.hpp"
#include "deviceOrigins.hpp"
#include "readinfofile.hpp"
#include "searchVolumeCD2.hpp"

int AlignImageCD(std::ofstream& LogFile, std::string& BaseName,
	std::vector< Blob >& BlobList,
	cv::Mat& WellTemplateImage, std::vector< ParameterLimit > PLimits,
	cv::Size ImageSize, std::vector<cv::Point2f> WellSpacing, cv::Size2f WellSize, int SectionOfInterest,
	std::vector< double >& GlobalResults, SearchVolumeResult& GlobalSearchResult, bool TrackAlignment, std::string& ErrMsg);