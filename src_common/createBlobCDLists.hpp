#pragma once
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "blob.hpp"
#include "blob_CD.hpp"

int createBlobCDLists(
	std::vector< Blob >& BlobList, std::vector< Blob >& OtherBlobList, std::vector< Blob >& BadBlobList,
	cv::Size ImageSize, cv::Size SectionImageSize, double DistToCenter, double ThetaResolution,
	std::vector< Blob_CD >& Blob_CDList, std::vector< Blob_CD >& OtherBlob_CDList, std::vector< Blob_CD >& BadBlob_CDList);