#ifndef TRACKOUTPUTDC_HPP
#define TRACKOUTPUTDC_HPP
//
// <Tracking Value> is read by main() and used here to load an object of type TrackAnalysis.  The member variables
// are obtained by setting <Tracking Value> to the bit-wise or of any of the desired items below.  This is used to 
// determine if any extra output is generated for diagnostic purposes.  The Output procedures are called to generate
// some of that output.
//
//
//			<Tracking Value> == 1 (track.OutputExtraImages)
//	
//	<BaseName>_001Rotated.png is the input image after being rotated (only output if the image is rotated)
//	<BaseName>_003RB-Bck.png is the background removed by rolling ball method.
//	<BaseName>_004RB-SubIm.png is the image after the the rolling ball method.
//	<BaseName>_005BckSubIm.png is image after rolling ball and flat background subtracted.  Unlike <BaseName>_006BckIm8U.png
//      which is always 8 bit, <BaseName>_005BckSubIm.png will have the same bit depth as the input image.
//  <BaseName>_008FG-Msk.png is a mask of the regions identified as part of the foreground by ExtractBlobsAdaptive()
//	<BaseName>_014BL-Inp.png is a color mask of the fluoresecnt blobs in the image which are color coded depending on the filter
//		value of that blob (green: FV = 1, yellow: FV = 2, red: FV = 0 or 3 ). Areas in <BaseName>_008FG-Msk.png which are
//      either too large or small to be droplets are omitted from this image.
//	<BaseName>_015AllBlobs.png image of all the blobs with FV = 1 or 2 outlined
//	<BaseName>_020B-Accptd.png image of blobs with FV = 1 outlined
//	<BaseName>_021B-Rjctd.png image of blobs with FV = 2 outlined
//	<BaseName>_091Device.png is the final aligned image
//
//			<Tracking Value> == 2 (track.OutputExtraCSV)
//
//  <BaseName>_010Blobs.csv spreadsheet of info about blobs with FV = 1
//  <BaseName>_011PoorBl.csv spreadsheet of info about blobs with FV = 2
//  <BaseName>_012BadBl.csv spreadsheet of info about blobs with FV = 0 or > 2
//  <BaseName>_017Backgnd.csv spreadsheet of info about blobs which ExtractBlobsAdaptive()
//      assigned to being part of the background.
//	<BaseName>_050BL-All.csv is a spread sheet of all blobs ordered by position in the rotated image
//
//			<Tracking Value> == 4 (track.TrackAngle)
//	
//	<BaseName>_025Angle.png image after correcting for image being tilted at an angle
//	<BaseName>_026fft.png image after correcting for image being tilted at an angle
//
//			<Tracking Value> == 8 (track.TrackAssignment)
//	
//  This option is no longer used.
//
//			<Tracking Value> == 16 (track.TrackAlignment)
//	
//  This option writes information about the alignment process into the logfile.
//
//			<Tracking Value> == 32 (track.TrackFilling)
//	
//	<BaseName>_102Fill.csv spread sheet of pixel data used to calculate filling ratio.
//  
//  This option also checks to see if area associated with different blobs overlap and if
//  so, it writes this information into the Logfile and std::out
//
//
//! [includes]
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "physicalconstants.h"
#include "blob.hpp"

void OutputImages003_004(cv::Mat &WorkImage1, cv::Mat &WorkImage2, cv::Mat &BackgroundSubImage32F, cv::Mat &Background32F, bool EightBit, std::string BaseName );

void OutputImages008_014(cv::Mat &WorkImage3, std::vector<std::vector<cv::Point> > &TrialContours, cv::Size ImageSize,
	std::vector<Blob> &BlobList, std::vector<Blob> &OtherBlobList, std::vector<Blob> &BadBlobList, 
	cv::Mat &Foreground8U, std::string BaseName );

void OutputImages_020_021_015(std::ofstream &LogFile, cv::Mat &WorkImage2, cv::Mat &WorkImage3, cv::Mat &WorkImage4, int ContourLevel,
	std::vector<Blob> &BlobList, std::vector<Blob> &OtherBlobList, std::vector<std::vector<cv::Point> > &TrialContours, std::string BaseName );

void OutputCSV_017_022(std::vector<Blob> &BackgroundList, std::vector<Blob> &BadBlobList, cv::Point WellSpacing, std::string BaseName);

class TrackAnalysis
{
	public:
		bool OutputExtraImages;
		bool OutputExtraCSV;
		bool TrackAngle;
		bool TrackAssignment;
		bool TrackAlignment;
		bool TrackFilling;
		bool ShiftFour;
		
		TrackAnalysis(): OutputExtraImages(false), OutputExtraCSV(false),
			TrackAngle(false), TrackAssignment(false),
			TrackAlignment(false), TrackFilling(false),
			ShiftFour(false)
			{}
		~TrackAnalysis(){}
		
		void Load(unsigned int ExtraOutput, std::string &InfoMsg )
		{
			unsigned int outputMask = 1;
			bool extra = false;
			std::ostringstream oString;
			oString.str("");
			oString.clear();
			
			if ( ( ExtraOutput & outputMask ) > 0 )
			{
				OutputExtraImages = true;
				oString << "All Images output\n";
				extra = true;
			}
			outputMask = 2;
			if ( ( ExtraOutput & outputMask ) > 0 )
			{
				OutputExtraCSV = true;
				oString << "All CSV files output\n";
				extra = true;
			}
			outputMask = 4;
			if ( ( ExtraOutput & outputMask ) > 0 )
			{
				TrackAngle = true;
				oString << "Track Angle correction\n";
				extra = true;
			}
			outputMask = 8;
			if ( ( ExtraOutput & outputMask ) > 0 )
			{
				TrackAssignment = true;
				oString << "Track Assignment of blobs and scaling\n";
				extra = true;
			}
			outputMask = 16;
			if ( ( ExtraOutput & outputMask ) > 0 )
			{
				TrackAlignment = true;
				oString << "Track image alignment\n";
				extra = true;
			}
			outputMask = 32;
			if ( ( ExtraOutput & outputMask ) > 0 )
			{
				TrackFilling = true;
				oString << "Track Thresholding of blobs and size determination\n";
				extra = true;
			}
			outputMask = 512;
			if ( ( ExtraOutput & outputMask ) > 0 )
			{
				ShiftFour = true;
				oString << "If input image is 16 bit, then pixel values will be right shifted 4 bits.\n";
				extra = true;
			}
			
			if ( !extra )
				oString << "No extra output generated\n";
			
			InfoMsg = oString.str();
		}
		
		void Clear()
		{
			OutputExtraImages = false;
			OutputExtraCSV = false;
			TrackAngle = false;
			TrackAssignment = false;
			TrackAlignment = false;
			TrackFilling = false;
			ShiftFour = false;
		}
};

#endif  // TRACKOUTPUTDC_HPP
