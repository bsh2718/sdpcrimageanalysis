#ifndef LABELBLOBPOSITION_HPP
#define LABELBLOBPOSITION_HPP
//! [includes]

#include <cstdlib>
#include <vector>
#include <algorithm>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "blob.hpp"

void LabelBlobPositions(std::ofstream &LogFile, std::vector< double > &Results, cv::Mat &CoordImage, 
	std::vector< Blob > &BlobList, std::string Label );

// void AssignBlobs2(std::ofstream &LogFile, std::vector< double > &Results, cv::Mat &CoordImage, 
	// std::vector< Blob > &BlobList, std::string Label );
	
int DecodeBlobPosition(cv::Mat &CoordImage, cv::Size CoordImageLimits, Blob &Trial, double ShiftX, double ShiftY, double Scale, int &Array, int &Section, 
	cv::Point &SectionPosition, bool &XGuardError, bool &YGuardError );

// int DecodeBlobPosition2(cv::Mat &CoordImage, cv::Size CoordImageLimits, Blob &Trial, double ShiftX, double ShiftY, double Scale, int &Array, int &Section, 
	// cv::Point &SectionPosition, bool &XGuardError, bool &YGuardError );
	
bool DecodeBlobPosition(cv::Mat &CoordImage, cv::Size CoordImageLimits, Blob &Trial );

#endif // LABELBLOBPOSITION_HPP

