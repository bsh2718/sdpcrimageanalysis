#ifndef OTSUTHRESHOLD_H
#define OTSUTHRESHOLD_H

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

class AfterBeforeResults
{
	public:
		int ThresholdMethod;
		int BestOtsuIdx;
		int BestMidPointIdx;
		double OtsuThreshold;
		double Threshold;
		int ThresholdIdx;
		int NumbBelow;
		std::vector< double > Data;
		std::vector< double > Sum;
		std::vector< double > OtsuResults;
		std::vector< double > MidPointResults;
		std::vector< double > Middle;
		std::vector< double > BelowAve;
		std::vector< double > AboveAve;
		
		AfterBeforeResults(){}
		~AfterBeforeResults(){}
		
		AfterBeforeResults(int Size): ThresholdMethod(0), BestOtsuIdx(-1), BestMidPointIdx(-1),
			OtsuThreshold(0.0), Threshold(0.0), ThresholdIdx(0), NumbBelow(0)
		{
			Data.resize(Size);
			Sum.resize(Size);
			OtsuResults.resize(Size);
			MidPointResults.resize(Size);
			Middle.resize(Size);
			BelowAve.resize(Size);
			AboveAve.resize(Size);
		}

		AfterBeforeResults(const AfterBeforeResults &Source ): ThresholdMethod(Source.ThresholdMethod),
			BestOtsuIdx(Source.BestOtsuIdx), BestMidPointIdx(Source.BestMidPointIdx), OtsuThreshold(Source.OtsuThreshold), 
			Threshold(Source.Threshold), ThresholdIdx(Source.ThresholdIdx), NumbBelow(Source.NumbBelow)
		{
			Data = Source.Data;
			Sum = Source.Sum;
			OtsuResults = Source.OtsuResults;
			MidPointResults = Source.MidPointResults;
			Middle = Source.Middle;
			BelowAve = Source.BelowAve;
			AboveAve = Source.AboveAve;
		}
		
		AfterBeforeResults& operator=(const AfterBeforeResults &Source)
		{
			if ( this != &Source )
			{
				ThresholdMethod = Source.ThresholdMethod;
				BestOtsuIdx = Source.BestOtsuIdx;
				BestMidPointIdx = Source.BestMidPointIdx;
				OtsuThreshold = Source.OtsuThreshold;
				Threshold = Source.Threshold;
				ThresholdIdx = Source.ThresholdIdx;
				NumbBelow = Source.NumbBelow;
				Data = Source.Data;
				Sum = Source.Sum;
				OtsuResults = Source.OtsuResults;
				MidPointResults = Source.MidPointResults;
				Middle = Source.Middle;
				BelowAve = Source.BelowAve;
				AboveAve = Source.AboveAve;
			}
			return *this;
		}
		
		void Resize(size_t Size)
		{
			if ( Size >= 0 )
			{
				Data.resize(Size);
				Sum.resize(Size);
				OtsuResults.resize(Size);
				MidPointResults.resize(Size);
				Middle.resize(Size);
				BelowAve.resize(Size);
				AboveAve.resize(Size);
			}
		}
		
		void Clear()
		{
			ThresholdMethod = 0;
			BestOtsuIdx = -1;
			BestMidPointIdx = -1;
			OtsuThreshold = 0.0;
			Threshold = 0.0;
			ThresholdIdx = 0;
			NumbBelow = 0;
			Data.clear();
			Sum.clear();
			OtsuResults.clear();
			MidPointResults.clear();
			Middle.clear();
			BelowAve.clear();
			AboveAve.clear();
		}
};

bool OtsuThreshold(std::ofstream &LogFile, std::vector<double> &Data, 
	AfterBeforeResults &Results );

#endif // OTSUTHRESHOLD_H