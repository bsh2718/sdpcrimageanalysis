#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "statistics.hpp"
#include "blob.hpp"
//#include "blobSortMethods.hpp"
#include "readinfofile.hpp"
#include "writeBlobFile.hpp"
#include "xyShift.hpp"
#include "assignBlobs.hpp"
#include "xyShift.hpp"
#include "trimRectangle.hpp"
#include "alignImage.hpp"
#include "assignBlobs.hpp"


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// AssignBlobs is a class which defines a set of functions and variables used to assign blobs to wells.  The position
// of the blobs is assumed to have been 
//      adjusted for angle (AngleAlignment)
//      an approximate alignment in X and Y has determined which Array/Section each blob is probably a part of.
// It uses this and other information about the grid dimensions to assign the blobs to wells.  This means that they
// are accepted as being a droplet in a well instead of just a fluorescent blob.
// 
// After being constructed, the object is initialized.
// 
// bool AssignBlobs::Initialize(std::ofstream& LogFile, std::string BaseName, std::vector< double > Results,
//     std::vector< Blob >& BlobList, std::vector< std::vector< int > > ArrSecMatrix,
//     cv::Size ArrSecImageSize, cv::Point2f WellSpacing, cv::Size2f WellSize,
//     std::vector< DeviceOrigins >& ArrayOrigins,
//     std::vector< DeviceOrigins >& SectionOrigins,
//     int NumbArraysIn, int NumbSectionsIn, int NumbColsIn, int NumbRowsIn, double Scale, double ScaleLimit,
//     bool TrackAssignment, std::string& ErrMsg)
// 
// LogFile - ofstream connected to log filebuf
// BaseName - project name
// Results	- (X-Offset, Y-Offset, Angle, Scale) Angle is the value obtained from the Angle alignment.
//		The two offsets are obtained from XYShift.  Scale isn't used.
// BlobList - vector of Blob. Includes well-like blobs which pass criteria to be used for quantitation.
// ArrSecImage - 8 bit gray scale image.  For each Array/Section pair in the device, a region is outlined using the
//		outside edges of the wells on the edge of the Array/Section.  The outline is the smallest region which encloses
//		all of the wells of that Array/Section.  The interior of each of these regions is non zero.  Pixels outside
//		of this region are set to zero.
// ArrSecImageSize - size of ArrSecImage
// WellSpacing - center to center distance of wells in pixels
// WellSize - size of wells in pixels
// ArrayOrigins - Vector of DeviceOrigins (deviceOrigins.hpp).  Index ranges from 1 to the number of arrays on device.
//      The origin variable is the location of the upper left well of the array.  Only the x coordinate is used.
// SectionOrigins -  Vector of DeviceOrigins (deviceOrigins.hpp).  Index ranges from 1 to the number of sections on device.
//      The origin variable is the location of the upper left well of the section.  Only the y coordinate is used.
// NumbArraysIn - number of arrays in device
// NumbSectionsIn - number of sections in device
// NumbColsIn - number of columns in section (assumed to be the same for all sections)
// NumbRowsIn - number of rows in section (assumed to be the same for all sections)
// Scale - initial guess for Scale (usually 1)
// ScaleLimit - maximum deviation from unity for Scale.  Used to estimate estimate how close to a multiple WellSpacing a 
//      distance must be to be considered an integral number of steps away.
// TrackAssignment - Additional information is output if this is true
// ErrMsg - 
//
// 
//
//

int AssignBlobs::ArraySectionNumber(int Array, int Section)
{
	return (Array - 1) * NumbSections + (Section - 1);
}

bool AssignBlobs::ArraySectionFromNumber(int ArraySectionNumber, int &Array, int &Section)
{
	int s = ArraySectionNumber % NumbSections;
	int a = (ArraySectionNumber - s) /NumbSections;
	s++;
	a++;
	if (a > 0 && a <= NumbArrays && s > 0 && s <= NumbSections)
	{
		Array = a;
		Section = s;
		return true;
	}
	else
		return false;
}

double SortNodeDelta = 10.0;
cv::Point2f SortCorner = cv::Point2f(0.0, 0.0);

bool SortBlobNodePositions(const BlobNode& A, const BlobNode& B)
{
	//if (fabs(A.AdjustedCenter2f.y - B.AdjustedCenter2f.y) < SortNodeDelta)
	//	return A.AdjustedCenter2f.x < B.AdjustedCenter2f.x;
	//else
	//	return A.AdjustedCenter2f.y < B.AdjustedCenter2f.y;
	double tmpA = sqrt(pow(A.AdjustedCenter2f.x - SortCorner.x, 2) + pow(A.AdjustedCenter2f.y - SortCorner.y, 2));
	double tmpB = sqrt(pow(B.AdjustedCenter2f.x - SortCorner.x, 2) + pow(B.AdjustedCenter2f.y - SortCorner.y, 2));
	if (fabs(tmpB - tmpA) < SortNodeDelta)
		return A.AdjustedCenter2f.x < B.AdjustedCenter2f.x;
	else
		return tmpA < tmpB;
}

bool SortBlobIndex(const BlobNode& A, const BlobNode& B)
{
	return A.SelfIndex < B.SelfIndex;
}

int TripleDelta = 5;
bool SortTriples(const std::vector< int >& A, const std::vector< int >& B)
{
	if (A[1] != B[1])
		return A[1] < B[1];
	else if (abs(A[0] - B[0]) < TripleDelta)
		return A[0] < B[0];
	else
		return A[2] > B[2];
}

bool AssignBlobs::Initialize(std::ofstream& LogFile, std::string BaseName, std::vector< double > Results,
	std::vector< Blob >& BlobList, std::vector< std::vector< int > > ArrSecMatrix, 
	cv::Size ArrSecImageSize, cv::Point2f WellSpacing, cv::Size2f WellSize,
	std::vector< DeviceOrigins >& ArrayOrigins,
	std::vector< DeviceOrigins >& SectionOrigins,
	int NumbArraysIn, int NumbSectionsIn, int NumbColsIn, int NumbRowsIn, double Scale, double ScaleLimit,
	bool TrackAssignment, std::string& ErrMsg)
{
	NumbArrays = NumbArraysIn;
	NumbSections = NumbSectionsIn;
	NumbCols = NumbColsIn;
	NumbRows = NumbRowsIn;
	std::string outName;
	SortedBlobList = BlobList;
	WellSpacing2f = WellSpacing;
	WellSpacingI = cv::Point((int)round(WellSpacing.x), (int)round(WellSpacing.y));
	WellSize2f = WellSize;
	Track = TrackAssignment;

	SearchImage.create(ArrSecImageSize, CV_16UC1);
	SearchImageSize = SearchImage.size();
	for (int n = 0; n < SortedBlobList.size(); n++)
	{
		
		SortedBlobList[n].AdjustedCenter2f.x = (float)(Scale * SortedBlobList[n].RotatedCenter2f.x + Results[0]);
		SortedBlobList[n].AdjustedCenter2f.y = (float)(Scale * SortedBlobList[n].RotatedCenter2f.y + Results[1]);
		SortedBlobList[n].AdjustedCenter.x = (int)round(SortedBlobList[n].AdjustedCenter2f.x);
		SortedBlobList[n].AdjustedCenter.y = (int)round(SortedBlobList[n].AdjustedCenter2f.y);
	}
	std::vector<int> sectionCount((size_t)NumbSections + 1);

	std::fill(sectionCount.begin(), sectionCount.end(), 0);
	for (int a = 1; a < ArrSecMatrix.size(); a++)
	{
		for (int s = 1; s < ArrSecMatrix[a].size(); s++)
		{
			if (ArrSecMatrix[a][s] > 0)
				sectionCount[s]++;
		}
	}
	LogFile << "ArrSecMatrix\n";
	for (int as = 0; as < ArrSecMatrix.size(); as++)
	{
		LogFile << as;
		for (int ss = 0; ss < ArrSecMatrix[as].size(); ss++)
		{
			LogFile << " : " << ArrSecMatrix[as][ss];
		}
		LogFile << "\n";
	}
	LogFile << "\n";
	int maxCount = sectionCount[1];
	SectionOfInterest = 1;
	for (int s = 2; s < sectionCount.size(); s++)
	{
		if (sectionCount[s] > maxCount)
		{
			maxCount = sectionCount[s];
			SectionOfInterest = s;
		}
	}
	LogFile << "SectionOfInterest: " << SectionOfInterest << std::endl;

	if (NumbSections > 1)
		SectionGap = (float)SectionOrigins[1].Origin.y - ((float)SectionOrigins[0].Origin.y + ((float)NumbRows - 1.0f) * WellSpacing2f.y);
	else
		SectionGap = 0.0;

	ArrayYScale.resize((size_t)NumbArrays + 1);
	std::fill(ArrayYScale.begin(), ArrayYScale.end(), 0.0f);
	AdjustedSectionOrigins.resize((size_t)NumbArrays + 1);
	std::fill(AdjustedSectionOrigins.begin(), AdjustedSectionOrigins.end(), 
		std::pair<cv::Point2f, cv::Point2f>(cv::Point2f(0.0,0.0), cv::Point2f(0.0,0.0)));
	ArrayUsed.resize((size_t)NumbArrays + 1);
	ArrayUsed[0] = false;
	for (int a = 1; a < ArrSecMatrix.size(); a++)
	{
		if (ArrSecMatrix[a][SectionOfInterest] > 0)
			ArrayUsed[a] = true;
	}

	ArraySectionShift.resize((size_t)ArraySectionNumber(NumbArrays, NumbSections) + 1);
	std::fill(ArraySectionShift.begin(), ArraySectionShift.end(), cv::Point(0, 0));
	NumbInArraySection.resize((size_t)ArraySectionNumber(NumbArrays, NumbSections) + 1);
	std::fill(NumbInArraySection.begin(), NumbInArraySection.end(), 0);
	CornerList.resize((size_t)ArraySectionNumber(NumbArrays, NumbSections) + 1);
	MinMaxList.resize((size_t)ArraySectionNumber(NumbArrays, NumbSections) + 1);
	InitNodes.resize((size_t)ArraySectionNumber(NumbArrays, NumbSections) + 1);
	for (int a = 0; a < NumbArrays; a++)
	{
		for (int s = 0; s < NumbSections; s++)
		{
			int as = ArraySectionNumber(a + 1, s + 1);
			CornerList[as] = cv::Point2f(ArrayOrigins[a].Origin.x , SectionOrigins[s].Origin.y);
		}
	}
	BlobNodeList.resize((size_t)ArraySectionNumber(NumbArrays, NumbSections) + 1);
	for (int n = 0; n < BlobNodeList.size(); n++)
		BlobNodeList[n].clear();
	UnalignedBlobNodeList.clear();
	std::vector< BlobNode > tempBlobNodeList;
	tempBlobNodeList.clear();
	BlobNode newBlobNode;
	newBlobNode.Clear();
	SortNodeDelta = WellSpacing.y * 4.0;
	
	std::ostringstream oString;
	std::ofstream outFile;
	std::string header;
	LogFile << "Results[0]/[1]/Scale: " << Results[0] << " / " << Results[1] << " / " << Scale << std::endl;
	for (int m = 0; m < MinMaxList.size(); m++)
	{
		int a;
		int s;
		ArraySectionFromNumber(m, a, s);
		MinMaxList[m].first.x = (int)round(CornerList[m].x - 2.0 * WellSpacing2f.x);
		MinMaxList[m].first.y = (int)round(CornerList[m].y);
		MinMaxList[m].second.x = (int)round(CornerList[m].x + (NumbCols + 1) * WellSpacing2f.x);
		MinMaxList[m].second.y = (int)round(CornerList[m].y + (NumbRows - 1) * WellSpacing2f.y);
		LogFile << m << " - Array/Section: " << a << " / " << s << "\n";
		LogFile << "  - minX/Y: " << MinMaxList[m].first.x << " / " << MinMaxList[m].first.y
			<< ", maxX/Y : " << MinMaxList[m].second.x << " / " << MinMaxList[m].second.y << " (Adjusted Centers)\n";
		LogFile << "   - minX/Y: " << (MinMaxList[m].first.x -Results[0])/Scale << " / " << (MinMaxList[m].first.y - Results[1])/Scale
			<< ", maxX/Y : " << (MinMaxList[m].second.x -Results[0])/Scale << " / " << (MinMaxList[m].second.y - Results[1])/Scale 
			<< " (Input Centers)\n";
	}
	int arr;
	for (int n = 0; n < SortedBlobList.size(); n++)
	{
		bool assigned = false;
		int as = -1;
		for (int a = 1 ; a <= NumbArrays ; a++ )
		{
			int m = ArraySectionNumber(a, SectionOfInterest);
			if (SortedBlobList[n].AdjustedCenter.x > MinMaxList[m].first.x
				&& SortedBlobList[n].AdjustedCenter.x < MinMaxList[m].second.x)
			{
				as = m;
				arr = a;
				assigned = true;
				break;
			}
		}
		newBlobNode.Index = n;
		newBlobNode.Status = 0;
		newBlobNode.AdjustedCenter2f = SortedBlobList[n].AdjustedCenter2f;
		newBlobNode.AdjustedCenter = SortedBlobList[n].AdjustedCenter;
		newBlobNode.InputCenter2f = SortedBlobList[n].InputCenter2f;
		if (assigned)
		{
			SortedBlobList[n].Array = arr;
			SortedBlobList[n].Section = SectionOfInterest;
			newBlobNode.Array = arr;
			newBlobNode.Section = SectionOfInterest;
			BlobNodeList[as].push_back(newBlobNode);
		}
		else
		{
			SortedBlobList[n].Array = 0;
			SortedBlobList[n].Section = 0;
			newBlobNode.Array = 0;
			newBlobNode.Section = 0;
			UnalignedBlobNodeList.push_back(newBlobNode);
		}
	}

	for (int a = 1; a <= NumbArrays ; a++)
	{
		int as = ArraySectionNumber(a, SectionOfInterest);
		if (BlobNodeList[as].size() > 0)
		{
			std::sort(BlobNodeList[as].begin(), BlobNodeList[as].end(), SortBlobNodePositions);
			for (int b = 0; b < BlobNodeList[as].size(); b++)
				BlobNodeList[as][b].SelfIndex = b;
			if (Track)
			{
				oString.str("");
				oString.clear();
				oString << BaseName << "_" << a << "_" << SectionOfInterest << "_InitNodeList.csv";
				outName = oString.str();
				outFile.open(outName);
				std::string outString = BlobNodeList[as][0].ToString(header);
				outFile << "corner.y/x,," << MinMaxList[as].first.y << "," << MinMaxList[as].first.x << ",,"
					<< MinMaxList[as].second.y << "," << MinMaxList[as].second.x << ",(Adjusted Centers)\n";
				outFile << "corner.y/x,," << (MinMaxList[as].first.y - Results[1]) / Scale << ","
					<< (MinMaxList[as].first.x - Results[0]) / Scale << ",,"
					<< (MinMaxList[as].second.y - Results[1]) / Scale << "," << (MinMaxList[as].second.x - Results[0]) / Scale
					<< ",(Input Centers)\n";
				outFile << header << outString;
				for (int s = 1; s < BlobNodeList[as].size(); s++)
					outFile << BlobNodeList[as][s].ToString(header);
				outFile.close();
				LogFile << "BlobNodeList - Array: " << a << ", Section: " << SectionOfInterest << " .size() = "
					<< BlobNodeList[as].size() << "\n";
				if (SectionOfInterest != NumbSections)
				{
					for (int s = 1; s <= NumbSections; s++)
					{
						if (s != SectionOfInterest)
						{
							int as2 = ArraySectionNumber(a, s);
							LogFile << "     BlobNodeList - Array: " << a << ", Section: " << s << " .size() = "
								<< BlobNodeList[as2].size() << "\n";
						}
					}
				}
			}
		}
	}

	if ( UnalignedBlobNodeList.size() > 0 )
	{
		std::sort(UnalignedBlobNodeList.begin(), UnalignedBlobNodeList.end(), SortBlobNodePositions);
		for (int b = 0; b < UnalignedBlobNodeList.size(); b++)
			UnalignedBlobNodeList[b].SelfIndex = b;
		if (Track)
		{
			oString.str("");
			oString.clear();
			oString << BaseName << "_0_0_NodeList.csv";
			outName = oString.str();
			outFile.open(outName);
			std::string outString = UnalignedBlobNodeList[0].ToString(header);
			outFile << header << outString;
			for (int s = 1; s < UnalignedBlobNodeList.size(); s++)
				outFile << UnalignedBlobNodeList[s].ToString(header);
			outFile.close();
		}
	}
	
	LogFile << "UnalignedBlobNodeList.size() = " << UnalignedBlobNodeList.size() << "\n";
	SortCorner = CornerList[0];
		
	cv::Point2f tolerance0 = cv::Point2f((float)(WellSpacing.x * 2.0 * ScaleLimit), (float)(WellSpacing.y * ScaleLimit));
	cv::Point2f tolerance1 = cv::Point2f((float)(WellSpacing.x * 4.0 * ScaleLimit), (float)(WellSpacing.y * 3.0 * ScaleLimit));
	cv::Point2f tolerance2 = cv::Point2f((float)(WellSpacing.x * (float)0.4), (float)(WellSpacing.y * (float)0.3));
	Tolerance = cv::Point((int)round(WellSpacing.x * 0.4), (int)round(WellSpacing.y * 0.2));
	int numbCols2 = NumbCols + 2;
	int numbRows2 = NumbRows + 2;

	HorzOffsetLimits.clear();
	VertOffsetLimits.clear();
	for(int n = 0; n < numbCols2; n++)
	{
		int upperH;
		int lowerH;
		int upperV;
		int lowerV;
		double tmp = (double)n  * ScaleLimit;
		if (tmp > 0.299)
			break;

		upperH = (int)round((n + 0.3) * WellSpacing2f.x);
		lowerH = (int)round((n - 0.3) * WellSpacing2f.x);

		upperV = (int)round((n + 0.3) * WellSpacing2f.y);
		lowerV = (int)round((n - 0.3) * WellSpacing2f.y);

		HorzOffsetLimits.push_back(std::pair<int, int>(upperH, lowerH));
		VertOffsetLimits.push_back(std::pair<int, int>(upperV, lowerV));
	}
	NumbOffsets = (int)HorzOffsetLimits.size();

	LogFile << "Connect Nodes: NumbOffsets: " << NumbOffsets << "\n";
	LogFile << "     Horz Offsets\n";
	for (int h = 0; h < NumbOffsets; h++)
		LogFile << h << " - " << HorzOffsetLimits[h].first << "/" << HorzOffsetLimits[h].second << "\n";

	LogFile << "     Vert Offsets\n";
	for (int v = 0; v < NumbOffsets; v++)
		LogFile << v << " - " << VertOffsetLimits[v].first << "/" << VertOffsetLimits[v].second << "\n";

	return true;
}

int AssignBlobs::GetSectionOfInterest()
{
	return SectionOfInterest;
}

bool AssignBlobs::SetSectionOfInterest(int NewSOI)
{
	if (NewSOI > NumbSections || NewSOI < 1)
		return false;
	SectionOfInterest = NewSOI;
	return true;
}

bool AssignBlobs::GetCorner11(int Array, int Section, cv::Point& Corner11)
{
	if (Array < 1 || Array > NumbArrays || Section < 1 || Section > NumbSections)
		return false;
	if (!ArrayUsed[Array])
		return false;

	int as = ArraySectionNumber(Array, Section);
	Corner11 = CornerList[as];
	return true;
}

bool AssignBlobs::ConnectNodes(std::ofstream& LogFile, std::string BaseName, int Array, int Section, bool Track)
{
	if (Array < 1 || Array > NumbArrays || Section < 1 || Section > NumbSections)
		return false;
	if (!ArrayUsed[Array])
	{
		LogFile << "Array Not used\n";
		return false;
	}

	int as = ArraySectionNumber(Array, Section);
	LogFile << "ConnectNodes: BlobNodeList[" << as << "].size() = " << BlobNodeList[as].size() << std::endl;
	if (BlobNodeList[as].size() < 10)
		return false;

	cv::Point2f target; 
	std::vector<std::vector< int > > rowCountList;
	std::vector< int > rowSums;
	int numbRowCount = (int)(ceil((double)SearchImageSize.height / (2.5 * WellSpacing2f.y)) + 2);
	rowCountList.resize(numbRowCount);
	rowSums.resize(numbRowCount);
	double deltaRow = (2.5 * WellSpacing2f.y);
	for (int rc = 0; rc < numbRowCount; rc++)
		rowCountList[rc].clear();
	for (int b = 0; b < BlobNodeList[as].size(); b++)
	{
		int r = (int)floor(BlobNodeList[as][b].AdjustedCenter2f.y / deltaRow);
		if (r >= 0 && r < numbRowCount)
			rowCountList[r].push_back(b);
	}
	int maxRowSum = 0;
	int maxRowSumIdx = -1;
	int firstRow;
	int lastRow;
	int minRow = -1;
	int maxRow = -1;
	if (Section == 1)
	{
		firstRow = 2;
		lastRow = numbRowCount - 12;
	}
	else
	{
		firstRow = 7;
		lastRow = numbRowCount - 7;
	}
	for (int rc = firstRow; rc <= lastRow; rc++)
	{
		rowSums[rc] = (int)(rowCountList[rc - 2].size() + rowCountList[rc - 1].size() + rowCountList[rc].size() + rowCountList[rc + 1].size() + rowCountList[rc + 2].size());
		if (rowSums[rc] > maxRowSum)
		{
			maxRowSum = rowSums[rc];
			maxRowSumIdx = rc;
		}
		if (rowSums[rc] > 2 && minRow < 0)
			minRow = rc;
		else if (minRow >= 0 && rowSums[rc] > 2)
			maxRow = rc;
	}
	std::vector< std::vector< int > > colCountList;
	std::vector< int > colSums;
	int numbColCount = (int)(ceil((double)SearchImageSize.width / (2.5 * WellSpacing2f.x) + 2 ));
	colCountList.resize(numbColCount);
	colSums.resize(numbColCount);
	double deltaCol = (2.5 * WellSpacing2f.x);
	for (int cc = 0; cc < numbColCount; cc++)
		colCountList[cc].clear();
	for (int b = 0; b < BlobNodeList[as].size(); b++)
	{
		int c = (int)floor(BlobNodeList[as][b].AdjustedCenter2f.x / deltaCol);
		if (c >= 0 && c < numbColCount)
			colCountList[c].push_back(b);
	}
	int maxColSum = 0;
	int maxColSumIdx = -1;
	for (int cc = 2; cc < numbColCount - 2; cc++)
	{
		colSums[cc] = (int)(colCountList[cc - 2].size() + colCountList[cc - 1].size() + colCountList[cc].size() + colCountList[cc + 1].size() + colCountList[cc + 2].size());
		if (colSums[cc] > maxColSum)
		{
			maxColSum = colSums[cc];
			maxColSumIdx = cc;
		}
	}
	target.x = (float)round(maxColSumIdx * deltaCol);
	target.y = (float)round(maxRowSumIdx * deltaRow);
	LogFile << "Array/Section = " << Array << " / " << Section << std::endl;
	LogFile << "BlobNodeList[" << as << "].size() = " << BlobNodeList[as].size() << ", Array/Section = " << Array
		<< "/" << Section << std::endl;
	LogFile << "     numbColCount/numbRowCount = " << numbColCount << " / " << numbRowCount << std::endl;
	LogFile << "     target = " << target << std::endl;
	LogFile << "     first/lastRow = " << firstRow << " / " << lastRow << std::endl;
	LogFile << "     minRow/maxRow = " << minRow << " / " << maxRow << std::endl;
	LogFile << "     maxRowSum/maxRowSumIdx/deltaRow = " << maxRowSum << " / " << maxRowSumIdx << " / " << deltaRow << std::endl;
	LogFile << "     maxColSum/maxColSumIdx/deltaCol = " << maxColSum << " / " << maxColSumIdx << " / " << deltaCol << std::endl;
	if (Track)
	{
		LogFile << "\nmaxRow data" << std::endl;
		for (int n = 0; n < rowCountList.size(); n++)
		{
			if (rowCountList[n].size() > 0)
				LogFile << n << " , " << rowCountList[n].size() << "\n";
		}
		LogFile << "\nmaxCol data" << std::endl;
		for (int n = 0; n < colCountList.size(); n++)
		{
			if (colCountList[n].size() > 0)
				LogFile << n << " , " << colCountList[n].size() << "\n";
		}
	}
	int initNode = -1;
	for (int ri = 0; ri < rowCountList[maxRowSumIdx].size(); ri++)
	{
		for (int ci = 0; ci < colCountList[maxColSumIdx].size(); ci++)
		{
			if (rowCountList[maxRowSumIdx][ri] == colCountList[maxColSumIdx][ci])
			{
				initNode = rowCountList[maxRowSumIdx][ri];
				break;
			}
		}
	}
	int findInitNodePass = 1;
	if (initNode < 0)
	{
		std::vector< int > combRowList = rowCountList[maxRowSumIdx];
		combRowList.insert(combRowList.end(), rowCountList[maxRowSumIdx - 1].begin(), rowCountList[maxRowSumIdx - 1].end());
		combRowList.insert(combRowList.end(), rowCountList[maxRowSumIdx + 1].begin(), rowCountList[maxRowSumIdx + 1].end());
		combRowList.insert(combRowList.end(), rowCountList[maxRowSumIdx - 2].begin(), rowCountList[maxRowSumIdx - 2].end());
		combRowList.insert(combRowList.end(), rowCountList[maxRowSumIdx + 2].begin(), rowCountList[maxRowSumIdx + 2].end());
		std::vector< int > combColList = colCountList[maxColSumIdx];
		combColList.insert(combColList.end(), colCountList[maxColSumIdx - 1].begin(), colCountList[maxColSumIdx - 1].end());
		combColList.insert(combColList.end(), colCountList[maxColSumIdx + 1].begin(), colCountList[maxColSumIdx + 1].end());
		combColList.insert(combColList.end(), colCountList[maxColSumIdx - 2].begin(), colCountList[maxColSumIdx - 2].end());
		combColList.insert(combColList.end(), colCountList[maxColSumIdx + 2].begin(), colCountList[maxColSumIdx + 2].end());
		for (int ri = 0; ri < combRowList.size(); ri++)
		{
			for (int ci = 0; ci < combColList.size(); ci++)
			{
				if (combRowList[ri] == combColList[ci])
				{
					initNode = combRowList[ri];
					break;
				}
			}
		}
		findInitNodePass++;
		if (initNode < 0)
		{
			combColList.clear();
			int diff = 3;
			while (maxColSumIdx - diff >= 0 && maxColSumIdx + diff < colCountList.size())
			{
				if (maxColSumIdx - diff >= 0)
					combColList.insert(combColList.end(), colCountList[maxColSumIdx - diff].begin(), colCountList[maxColSumIdx - diff].end());
				if (maxColSumIdx + diff < colCountList.size())
					combColList.insert(combColList.end(), colCountList[maxColSumIdx + diff].begin(), colCountList[maxColSumIdx + diff].end());
				diff++;
			}
			for (int ri = 0; ri < combRowList.size(); ri++)
			{
				for (int ci = 0; ci < combColList.size(); ci++)
				{
					if (combRowList[ri] == combColList[ci])
					{
						initNode = combRowList[ri];
						break;
					}
				}
			}
			findInitNodePass++;
		}
	}
	int altInitNode = 0;
	double bestR2 = pow((double)BlobNodeList[as][0].AdjustedCenter2f.x - target.x, 2)
		+ pow(BlobNodeList[as][0].AdjustedCenter2f.y - target.y, 2);
	double deltaV = (double)BlobNodeList[as][0].AdjustedCenter2f.y - target.y;
	double epsilon2 = pow(WellSpacing2f.y, 2);
	SearchImage.setTo(0);
	for (int b = 0; b < BlobNodeList[as].size(); b++)
	{
		if (BlobNodeList[as][b].AdjustedCenter.x > 0
			&& BlobNodeList[as][b].AdjustedCenter.x < SearchImageSize.width
			&& BlobNodeList[as][b].AdjustedCenter.y > 0
			&& BlobNodeList[as][b].AdjustedCenter.y < SearchImageSize.height)
		{
			SearchImage.at<short unsigned int>(BlobNodeList[as][b].AdjustedCenter) = b + 1;
			double r2 = pow((double)BlobNodeList[as][b].AdjustedCenter2f.x - target.x, 2)
				+ pow(BlobNodeList[as][b].AdjustedCenter2f.y - target.y, 2);
			if (r2 < bestR2 - epsilon2)
			{
				altInitNode = b;
				bestR2 = r2;
				deltaV = (double)BlobNodeList[as][b].AdjustedCenter2f.y - target.y;
			}
			else if (r2 < bestR2 + epsilon2)
			{
				double del = (double)BlobNodeList[as][b].AdjustedCenter2f.y - target.y;
				if (del > deltaV)
				{
					altInitNode = b;
					bestR2 = r2;
					deltaV = del;
				}
			}
		}
	}
	LogFile << "Array/Section: " << Array << "/" << Section << ", BlobNodeList.size(): " << BlobNodeList[as].size()
		<< std::endl;
	LogFile << "findInitNodePass = " << findInitNodePass << std::endl;
	LogFile << "initNode = " << initNode;
	if (initNode >= 0)
		LogFile << ", row/col: " << BlobNodeList[as][initNode].InputCenter2f.y << " / " << BlobNodeList[as][initNode].InputCenter2f.x;
	LogFile << "\naltInitNode = " << altInitNode << ", row/col: " << BlobNodeList[as][altInitNode].InputCenter2f.y
		<< " / " << BlobNodeList[as][altInitNode].InputCenter2f.x << std::endl;
	if (initNode < 0)
	{
		LogFile << "Blob assignment failure" << std::endl;
		return false;
	}

	BlobNodeList[as][initNode].Status = 1;
	int deltaX = (int)round((BlobNodeList[as][initNode].AdjustedCenter2f.x - CornerList[as].x) / WellSpacing2f.x);
	int deltaY = (int)round((BlobNodeList[as][initNode].AdjustedCenter2f.y - CornerList[as].y) / WellSpacing2f.y);
	BlobNodeList[as][initNode].Array = Array;
	BlobNodeList[as][initNode].Section = SectionOfInterest;
	BlobNodeList[as][initNode].SectionPosition.x = deltaX + 1;
	BlobNodeList[as][initNode].SectionPosition.y = deltaY + 1;
	std::string header;
	std::string iNode = BlobNodeList[as][initNode].ToString(header);
	LogFile << header << "\n";
	LogFile << "InitNode: " << iNode << "\n";
	InitNodes[as] = initNode;
	
	cv::Rect searchRect;
	cv::Point searchOffset;
	cv::Size searchRectSize;
	if (NumbOffsets > 1)
	{
		searchRectSize.width = (int)round((NumbOffsets - 0.5) * WellSpacing2f.x * 2.0);
		searchRectSize.height = (int)round((NumbOffsets - 0.5) * WellSpacing2f.y * 2.0);
	}
	else
	{
		searchRectSize.width = (int)round((WellSpacing2f.x + HorzOffsetLimits[0].first)*2.0);
		searchRectSize.height = (int)round((WellSpacing2f.y + VertOffsetLimits[0].first)* 2.0);
	}
	if (searchRectSize.width % 2 == 0)
		searchRectSize.width++;
	if (searchRectSize.height % 2 == 0)
		searchRectSize.height++;
	searchOffset.x = -(searchRectSize.width - 1) / 2;
	searchOffset.y = -(searchRectSize.height - 1) / 2;
	std::vector< cv::Point > locations;
	int pass = 0;
	int totalConnections = 0;
	int addedConnections = 0;
	LogFile << "searchRect.width/height: " << searchRectSize.width << " / " << searchRectSize.height << std::endl;
	LogFile << "searchOffset.x/y: " << searchOffset.x << " / " << searchOffset.y << std::endl;
	for (int n = 0; n < BlobNodeList[as].size() - 1; n++)
	{
		searchRect.x = BlobNodeList[as][n].AdjustedCenter.x + searchOffset.x;
		searchRect.y = BlobNodeList[as][n].AdjustedCenter.y + searchOffset.y;
		//if (Track)
		//{
		//	LogFile << "Node/Index/Adj.x/Adj.y/Inp.x/Inp.y: " << n << " / " << BlobNodeList[as][n].Index << " / "
		//		<< BlobNodeList[as][n].AdjustedCenter.x << " / " << BlobNodeList[as][n].AdjustedCenter.y << " / "
		//		<< BlobNodeList[as][n].InputCenter2f.x << " / " << BlobNodeList[as][n].InputCenter2f.y << "\n";
		//	addedConnections = 0;
		//}
		if (TrimRectangle(searchRect, searchRectSize, SearchImageSize))
		{
			cv::Point2f target = BlobNodeList[as][n].AdjustedCenter2f;
			cv::findNonZero(SearchImage(searchRect), locations);
			for (int c = 0; c < locations.size(); c++)
			{
				int node = SearchImage(searchRect).at<short unsigned int>(locations[c]) - 1;
				if (node != n)
				{
					cv::Point2f delta = BlobNodeList[as][node].AdjustedCenter2f - target;
					int deltaXI = (int)round(delta.x / WellSpacing2f.x);
					int deltaYI = (int)round(delta.y / WellSpacing2f.y);
					if ((abs(deltaXI) < NumbOffsets && abs(deltaYI) < NumbOffsets) &&
						(abs(deltaXI) == 0 || abs(deltaYI) == 0 || abs(deltaXI) + abs(deltaYI) < NumbOffsets - 1))
					{
						/*if (pass < 15 && c % 7 == 0)
						{
							pass++;
							LogFile << "n/node: " << n << "/" << node << ", deltaY/X: " << delta.y << "/" << delta.x
								<< ", deltaX/YI: " << deltaXI << "/" << deltaYI << "\n";
						}*/
						double errX = fabs(delta.x);
						double errY = fabs(delta.y);
						if (errX < HorzOffsetLimits[abs(deltaXI)].first
							&& errX > HorzOffsetLimits[abs(deltaXI)].second
							&& errY < VertOffsetLimits[abs(deltaYI)].first
							&& errY > VertOffsetLimits[abs(deltaYI)].second)
						{
							int idx = BlobNodeList[as][node].SelfIndex;
							BlobNodeList[as][n].AddConnection(idx, cv::Point(deltaXI, deltaYI));
							addedConnections++;
						}
					}
				}
			}
			//if (Track)
			//{
			//	LogFile << "     added Connections = " << addedConnections << "\n";
			//}
		}
		std::sort(BlobNodeList[as][n].Connections.begin(), BlobNodeList[as][n].Connections.end(), SortConnections);
		if (BlobNodeList[as][n].Connections.size() > 24)
			BlobNodeList[as][n].Connections.resize(24);
	}

	if (BlobNodeList[as].size() > 0 && Track)
	{
		std::stringstream oString;
		oString.str("");
		oString.clear();
		oString << BaseName << "_" << Array << "_" << SectionOfInterest << "_CnctNodeList.csv";
		std::string outName = oString.str();
		std::ofstream outFile;
		outFile.open(outName);
		std::string header;
		std::string outString = BlobNodeList[as][0].ToString(header);
		outFile << "corner.y/x,," << MinMaxList[as].first.y << "," << MinMaxList[as].first.x << ",,"
			<< MinMaxList[as].second.y << "," << MinMaxList[as].second.x << ",(Adjusted Centers)\n";
		outFile << header;
		for (int s = 0; s < BlobNodeList[as].size(); s++)
			outFile << BlobNodeList[as][s].ToString();
		outFile.close();
	}
	return true;
}

bool AssignBlobs::EvaluateNodesSOI(std::ofstream& LogFile, std::string BaseName, int Array)
{
	if (Array < 1 || Array > NumbArrays)
		return false;
	if (!ArrayUsed[Array])
		return false;

	int as = ArraySectionNumber(Array, SectionOfInterest);
	std::vector< std::pair<bool, bool> > nodesUsed(BlobNodeList[as].size());
	std::fill(nodesUsed.begin(), nodesUsed.end(), std::pair<bool, bool>(false, false));
	
	int initNode = InitNodes[as];
	nodesUsed[initNode].first = false;
	nodesUsed[initNode].second = true;
	int posX = (int)round((BlobNodeList[as][initNode].AdjustedCenter2f.x - CornerList[as].x) / WellSpacing2f.x) + 1;
	int posY = (int)round((BlobNodeList[as][initNode].AdjustedCenter2f.y - CornerList[as].y) / WellSpacing2f.y) + 1;
	BlobNodeList[as][initNode].SectionPosition = cv::Point(posX, posY);
	BlobNodeList[as][initNode].Status = 1;
	BlobNodeList[as][initNode].ReferenceNode.NodeIndex = -1;
	LogFile << ">>>>>>>>>> Assigning Array/Section: " << Array << "/" << SectionOfInterest << "<<<<<<<<<<\n";
	LogFile << "Initial Node: BlobNodeList[" << as << "][" << initNode << "].SectionPosition.y/x = "
		<< BlobNodeList[as][initNode].SectionPosition.y << "/" << BlobNodeList[as][initNode].SectionPosition.x
		<< ", SelfIndex = " << BlobNodeList[as][initNode].SelfIndex
		<< "\n   Input.y/x = " << BlobNodeList[as][initNode].InputCenter2f.y << "/" << BlobNodeList[as][initNode].InputCenter2f.x
		<< ", Adjusted.y/x = " << BlobNodeList[as][initNode].AdjustedCenter2f.y << "/" << BlobNodeList[as][initNode].AdjustedCenter2f.x
		<< "\n   Corner.y/x: " << CornerList[as].y << "/" << CornerList[as].x << "\n";
	LogFile << "      Numb Connections (initNode) = " << BlobNodeList[as][initNode].Connections.size() << std::endl;
	if (BlobNodeList[as][initNode].Connections.size() < 1)
		return false;
	for (int c = 0; c < BlobNodeList[as][initNode].Connections.size(); c++)
	{
		int idx1 = BlobNodeList[as][initNode].Connections[c].NodeIndex;
		BlobNodeList[as][idx1].SectionPosition = BlobNodeList[as][initNode].SectionPosition
			+ BlobNodeList[as][initNode].Connections[c].Shift;
		BlobNodeList[as][idx1].Status = 1;
		nodesUsed[idx1].second = true;
	}
	nodesUsed[initNode].first = true;

	bool flag = true;
	int firstNode = 0;
	int numbUnused = (int)nodesUsed.size() - 1;
	while (flag)
	{
		int unused = 0;
		for (int n = firstNode; n < nodesUsed.size(); n++)
		{
			if (!nodesUsed[n].first)
			{
				if (nodesUsed[n].second)
				{
					for (int c = 0; c < BlobNodeList[as][n].Connections.size(); c++)
					{
						int idx1 = BlobNodeList[as][n].Connections[c].NodeIndex;
						BlobNodeList[as][idx1].SectionPosition = BlobNodeList[as][n].SectionPosition 
							+ BlobNodeList[as][n].Connections[c].Shift;
						BlobNodeList[as][idx1].Status = 1;
						nodesUsed[idx1].second = true;
					}
					nodesUsed[n].first = true;
				}
			}
			if (!nodesUsed[n].first)
			{
				unused++;
				if (unused == 1)
					firstNode = n;
			}
		}
		if (unused == 0 || unused == numbUnused)
			flag = false;
		else
			numbUnused = unused;
	}
	
	DetermineArraySectionShiftSOI(LogFile, BaseName, Array);
	LogFile << "ArraySectionShift for Array: " << Array << ", SectionOfInterest: " << SectionOfInterest
		<< " = " << ArraySectionShift[as].y << "/" << ArraySectionShift[as].x << "\n";
	int as0 = -1;
	int as1 = -1;
	if (SectionOfInterest > 1)
		as0 = ArraySectionNumber(Array, SectionOfInterest - 1);
	if (SectionOfInterest < NumbSections)
		as1 = ArraySectionNumber(Array, SectionOfInterest + 1);
	for (int n = 0; n < BlobNodeList[as].size(); n++)
	{
		BlobNodeList[as][n].SectionPosition += ArraySectionShift[as];
		if (BlobNodeList[as][n].SectionPosition.x < 1 || BlobNodeList[as][n].SectionPosition.x > NumbCols
			|| BlobNodeList[as][n].SectionPosition.y < 1 || BlobNodeList[as][n].SectionPosition.y > NumbRows)
		{
			BlobNodeList[as][n].Status = 0;
			BlobNodeList[as][n].Connections.clear();
			BlobNodeList[as][n].NumbDirections = 0;
			if (BlobNodeList[as][n].SectionPosition.y < 1 && as0 >= 0)
			{
				BlobNodeList[as][n].Section = SectionOfInterest - 1;
				BlobNodeList[as0].push_back(BlobNodeList[as][n]);
			}
			else if (BlobNodeList[as][n].SectionPosition.y > NumbRows && as1 >= 0)
			{
				BlobNodeList[as][n].Section = SectionOfInterest + 1;
				BlobNodeList[as1].push_back(BlobNodeList[as][n]);
			}
			else
				UnalignedBlobNodeList.push_back(BlobNodeList[as][n]);

			BlobNodeList[as][n].Status = -10;
		}
		else
		{
			int idx = BlobNodeList[as][n].Index;
			SortedBlobList[idx].Array = Array;
			SortedBlobList[idx].Section = SectionOfInterest;
			SortedBlobList[idx].SectionPosition = BlobNodeList[as][n].SectionPosition;
			SortedBlobList[idx].Status = 1;
		}
	}
	std::ostringstream oString;
	std::string outName;
	std::ofstream outFile;
	std::string header;
	if (as0 >= 0 && BlobNodeList[as0].size() > 0 )
	{
		std::sort(BlobNodeList[as0].begin(), BlobNodeList[as0].end(), SortBlobNodePositions);
		for (int b = 0; b < BlobNodeList[as0].size(); b++)
			BlobNodeList[as0][b].SelfIndex = b;
		if (Track)
		{
			oString.str("");
			oString.clear();
			oString << BaseName << "_" << Array << "_" << SectionOfInterest - 1 << "_InitNodeList.csv";
			outName = oString.str();
			outFile.open(outName);
			std::string outString = BlobNodeList[as0][0].ToString(header);
			outFile << "corner.y/x,," << MinMaxList[as0].first.y << "," << MinMaxList[as0].first.x << ",,"
				<< MinMaxList[as0].second.y << "," << MinMaxList[as0].second.x << ",(Adjusted Centers)\n";
			outFile << header << outString;
			for (int s = 1; s < BlobNodeList[as0].size(); s++)
				outFile << BlobNodeList[as0][s].ToString(header);
			outFile.close();
			LogFile << "BlobNodeList - Array: " << Array << ", Section: " << SectionOfInterest - 1 << " .size() = "
				<< BlobNodeList[as0].size() << "\n";
		}
	}
	if (as1 >= 0 && BlobNodeList[as1].size() > 0 )
	{
		std::sort(BlobNodeList[as1].begin(), BlobNodeList[as1].end(), SortBlobNodePositions);
		for (int b = 0; b < BlobNodeList[as1].size(); b++)
			BlobNodeList[as1][b].SelfIndex = b;
		if (Track)
		{
			oString.str("");
			oString.clear();
			oString << BaseName << "_" << Array << "_" << SectionOfInterest + 1 << "_InitNodeList.csv";
			outName = oString.str();
			outFile.open(outName);
			std::string outString = BlobNodeList[as1][0].ToString(header);
			outFile << "corner.y/x,," << MinMaxList[as1].first.y << "," << MinMaxList[as1].first.x << ",,"
				<< MinMaxList[as1].second.y << "," << MinMaxList[as1].second.x << ",(Adjusted Centers)\n";
			outFile << header << outString;
			for (int s = 1; s < BlobNodeList[as1].size(); s++)
				outFile << BlobNodeList[as1][s].ToString(header);
			outFile.close();
			LogFile << "BlobNodeList - Array: " << Array << ", Section: " << SectionOfInterest + 1 << " .size() = "
				<< BlobNodeList[as1].size() << "\n";
		}
	}
	return true;
}

bool AssignBlobs::EvaluateNodes(std::ofstream& LogFile, std::string BaseName, int Array, int Section)
{
	if (Array < 1 || Array > NumbArrays || Section < 1 || Section > NumbSections || Section == SectionOfInterest)
		return false;
	if (!ArrayUsed[Array])
		return false;

	int as = ArraySectionNumber(Array, Section);
	std::vector< std::pair<bool, bool> > nodesUsed(BlobNodeList[as].size());
	std::fill(nodesUsed.begin(), nodesUsed.end(), std::pair<bool, bool>(false, false));
	int initNode = InitNodes[as];
	nodesUsed[initNode].first = false;
	nodesUsed[initNode].second = true;
	int posX = (int)round((BlobNodeList[as][initNode].AdjustedCenter2f.x - AdjustedSectionOrigins[Array].first.x) / WellSpacing2f.x) + 1;
	int posY;
	float adjustedSectionOrigin_y;
	if (Section < SectionOfInterest)
	{
		posY = (int)round((BlobNodeList[as][initNode].AdjustedCenter2f.y - AdjustedSectionOrigins[Array].first.y)
			/ WellSpacing2f.y) + NumbRows;
		adjustedSectionOrigin_y = AdjustedSectionOrigins[Array].first.y;
	}
	else
	{
		posY = (int)round((BlobNodeList[as][initNode].AdjustedCenter2f.y - AdjustedSectionOrigins[Array].second.y)
			/ WellSpacing2f.y) + 1;
		adjustedSectionOrigin_y = AdjustedSectionOrigins[Array].second.y;
	}

	BlobNodeList[as][initNode].SectionPosition = cv::Point(posX, posY);
	BlobNodeList[as][initNode].Status = 1;
	BlobNodeList[as][initNode].ReferenceNode.NodeIndex = -1;
	LogFile << ">>>>>>>>>> Assigning Array/Section: " << Array << "/" << Section << "<<<<<<<<<<\n";
	LogFile << "Initial Node: BlobNodeList[" << as << "][" << initNode << "].SectionPosition.y/x = "
		<< BlobNodeList[as][initNode].SectionPosition.y << "/" << BlobNodeList[as][initNode].SectionPosition.x
		<< ", SelfIndex = " << BlobNodeList[as][initNode].SelfIndex
		<< ", Input.y/x = " << BlobNodeList[as][initNode].InputCenter2f.y << "/" << BlobNodeList[as][initNode].InputCenter2f.x
		<< ", Adjusted.y/x = " << BlobNodeList[as][initNode].AdjustedCenter2f.y << "/" << BlobNodeList[as][initNode].AdjustedCenter2f.x
		<< ", AdjustedOrigin.y/x: " << adjustedSectionOrigin_y << "/" << AdjustedSectionOrigins[Array].first.x << "\n";
	LogFile << "      Numb Connections = " << BlobNodeList[as][initNode].Connections.size() << std::endl;
	for (int c = 0; c < BlobNodeList[as][initNode].Connections.size(); c++)
	{
		int idx1 = BlobNodeList[as][initNode].Connections[c].NodeIndex;
		if (Track)
		{
			LogFile << c << ": " << BlobNodeList[as][initNode].Connections[c].ToString() << ", [initNode].y/x: "
				<< BlobNodeList[as][initNode].AdjustedCenter2f.y << "/" << BlobNodeList[as][initNode].AdjustedCenter2f.x
				<< ", [idx1].y/x: " << BlobNodeList[as][idx1].AdjustedCenter2f.y << "/" << BlobNodeList[as][idx1].AdjustedCenter2f.x << std::endl;
		}
		BlobNodeList[as][idx1].SectionPosition = BlobNodeList[as][initNode].SectionPosition
			+ BlobNodeList[as][initNode].Connections[c].Shift;
		BlobNodeList[as][idx1].Status = 1;
		nodesUsed[idx1].second = true;
	}
	nodesUsed[initNode].first = true;

	bool flag = true;
	int firstNode = 0;
	int numbUnused = (int)nodesUsed.size() - 1;
	while (flag)
	{
		int unused = 0;
		for (int n = firstNode; n < nodesUsed.size(); n++)
		{
			if (!nodesUsed[n].first)
			{
				if (nodesUsed[n].second)
				{
					for (int c = 0; c < BlobNodeList[as][n].Connections.size(); c++)
					{
						int idx1 = BlobNodeList[as][n].Connections[c].NodeIndex;
						BlobNodeList[as][idx1].SectionPosition = BlobNodeList[as][n].SectionPosition 
							+ BlobNodeList[as][n].Connections[c].Shift;
						BlobNodeList[as][idx1].Status = 1;
						nodesUsed[idx1].second = true;
					}
					nodesUsed[n].first = true;
				}
			}
			if (!nodesUsed[n].first)
			{
				unused++;
				if (unused == 1)
					firstNode = n;
			}
		}
		if (unused == 0 || unused == numbUnused)
			flag = false;
		else
			numbUnused = unused;
	}

	DetermineArraySectionShift(LogFile, BaseName, Array, Section);
	LogFile << "ArraySectionShift for Array: " << Array << ", Section: " << Section
		<< " = " << ArraySectionShift[as].y << "/" << ArraySectionShift[as].x << "\n";
	for (int n = 0; n < BlobNodeList[as].size(); n++)
	{
		BlobNodeList[as][n].SectionPosition += ArraySectionShift[as];
		if (BlobNodeList[as][n].SectionPosition.x < 1 || BlobNodeList[as][n].SectionPosition.x > NumbCols
			|| BlobNodeList[as][n].SectionPosition.y < 1 || BlobNodeList[as][n].SectionPosition.y > NumbRows)
		{
			UnalignedBlobNodeList.push_back(BlobNodeList[as][n]);
			BlobNodeList[as][n].Status = -10;
		}
		else
		{
			int idx = BlobNodeList[as][n].Index;
			SortedBlobList[idx].Array = Array;
			SortedBlobList[idx].Section = Section;
			SortedBlobList[idx].SectionPosition = BlobNodeList[as][n].SectionPosition;
			SortedBlobList[idx].Status = 1;
		}

	}
	return true;
}

bool AssignBlobs::DetermineArraySectionShiftSOI(std::ofstream& LogFile, std::string BaseName, int Array)
{
	if (Array < 1 || Array > NumbArrays )
		return false;
	if (!ArrayUsed[Array])
		return false;

	int as = ArraySectionNumber(Array, SectionOfInterest);
	int rowOffset = 0;
	int colOffset = 0;
	int minX = 2 * NumbCols;
	int maxX = -NumbCols;
	int minY = 2 * NumbRows;
	int maxY = -NumbRows;
	LogFile << "DetermineArraySectionShift: BlobNodeList[" << as << "].size() = " << BlobNodeList[as].size() << "\n";
	for (int n = 0; n < BlobNodeList[as].size(); n++)
	{
		if (BlobNodeList[as][n].Status > 0)
		{
			int idx = BlobNodeList[as][n].SectionPosition.x;
			int idy = BlobNodeList[as][n].SectionPosition.y;
			if (idx > -NumbCols && idx < 2 * NumbCols && idy > -NumbRows && idy < 2 * NumbRows)
			{
				if (idx < minX)
					minX = idx;
				if (idx > maxX)
					maxX = idx;
				
				if (idy < minY)
					minY = idy;
				if (idy > maxY)
					maxY = idy;
			}
		}
	}
	if (maxX <= minX || maxY <= minY)
		return false;
	colOffset = -minX;
	rowOffset = -minY;
	std::vector< int > colCount((size_t)(maxX - minX + 2));
	std::fill(colCount.begin(), colCount.end(), 0);
	std::vector< AlignData > rowAlign((size_t)(maxY - minY + 2));
	AlignData blankAlignData;
	blankAlignData.Index = 0;
	blankAlignData.Length = 0;
	blankAlignData.Count = 0;
	blankAlignData.Stats.Clear();
	blankAlignData.Spacing = 0.0;
	blankAlignData.SpacingOK = true;
	std::fill(rowAlign.begin(), rowAlign.end(), blankAlignData);
	std::vector< double > rowSpacing((size_t)(maxY - minY + 2));
	std::fill(rowSpacing.begin(), rowSpacing.end(), 0.0);

	for (int n = 0; n < BlobNodeList[as].size(); n++)
	{
		if (BlobNodeList[as][n].Status > 0)
		{
			int idx = BlobNodeList[as][n].SectionPosition.x;
			int idy = BlobNodeList[as][n].SectionPosition.y;
			if (idx > -NumbCols && idx < 2 * NumbCols && idy > -NumbRows && idy < 2 * NumbRows)
			{
				colCount[(int)idx + colOffset]++;
				rowAlign[idy + (int)rowOffset].Stats.Accumulate(BlobNodeList[as][n].AdjustedCenter2f.y);
			}
		}
	}

	LogFile << "Array/Section " << Array << "/" << SectionOfInterest << "\n";
	if (Track)
	{
		LogFile << "     Col Count (min/maxX = " << minX << " / " << maxX << ")\n";
		for (int c = minX; c <= maxX; c++)
		{
			LogFile << c << " - " << colCount[(int)c + colOffset] << std::endl;
		}
		LogFile << "     Row Stats (min/maxY = " << minY << " / " << maxY << ")\n";
		for (int r = minY; r <= maxY; r++)
		{
			LogFile << r << " - " << rowAlign[r + (int)rowOffset].Stats.Count() << std::endl;
		}
	}
	int bestX = -1;
	int bestXValue = -1;
	if (maxX - minX + 1 <= NumbCols)
	{
		//LogFile << "maxX - minX + 1 / NumbCols = " << maxX - minX + 1 << " / " << NumbCols << std::endl;
		if (Array == 1)
		{
			ArraySectionShift[as].x = NumbCols - maxX;
		}
		else if (Array == NumbArrays)
		{
			ArraySectionShift[as].x = 1 - minX;
		}
		else
		{
			if (maxX <= NumbCols && minX >= 1)
				ArraySectionShift[as].x = 0;
			else if (maxX > NumbCols)
				ArraySectionShift[as].x = NumbCols - maxX;
			else if (minX < 1)
				ArraySectionShift[as].x = 1 - minX;
		}
		if ( Track)
			LogFile << "ArraySectionShift[as].x = " << ArraySectionShift[as].x << std::endl;
	}
	else
	{
		//LogFile << "minX / maxX / maxX - NumbCols = " << minX << " / " << maxX << " / " << maxX - NumbCols << std::endl;

		for (int x = minX; x <= maxX - NumbCols + 1; x++)
		{
			int xcnt = x;
			int cnt = 0;
			while ((int)xcnt + colOffset < colCount.size() && xcnt - x < NumbCols)
			{
				cnt += colCount[(int)xcnt + colOffset];
				xcnt++;
			}
			if (cnt > bestXValue)
			{
				bestXValue = cnt;
				bestX = x;
			}

		}
		ArraySectionShift[as].x = 1 - bestX;
		if ( Track)
			LogFile << "ArraySectionShift[as].x = " << ArraySectionShift[as].x << std::endl;
	}
	std::vector<int> emptyRows;
	emptyRows.clear();
	double dPointedAt;
	for (int n = 0; n < rowAlign.size(); n++)
	{
		rowAlign[n].Index = n - rowOffset;
		rowAlign[n].Stats.Analyze();
		if (n > 1 && rowAlign[n].Stats.Count() > 0)
		{
			int nm1 = n - 1;
			while (nm1 > 0)
			{
				if (rowAlign[nm1].Stats.Count() > 0)
				{
					rowAlign[nm1].Spacing = (rowAlign[n].Stats.Ave() - rowAlign[nm1].Stats.Ave())/WellSpacing2f.y;
					double md = std::modf(rowAlign[nm1].Spacing, &dPointedAt);
					if (rowAlign[nm1].Spacing < 2.0)
					{
						if (md > 0.25 && md < 0.75)
							rowAlign[nm1].SpacingOK = false;
						else
							rowAlign[nm1].SpacingOK = true;
					}
					else if (rowAlign[nm1].Spacing < 4.0)
					{
						if (md > 0.23 && md < 0.77)
							rowAlign[nm1].SpacingOK = false;
						else
							rowAlign[nm1].SpacingOK = true;
					}
					else if (rowAlign[nm1].Spacing < 6.0)
					{
						if (md > 0.21 && md < 0.79)
							rowAlign[nm1].SpacingOK = false;
						else
							rowAlign[nm1].SpacingOK = true;
					}
					else
					{
						if (md > 0.20 && md < 0.80)
							rowAlign[nm1].SpacingOK = false;
						else
							rowAlign[nm1].SpacingOK = true;
					}
						break;
				}
				nm1--;
			}
		}
		if (rowAlign[n].Stats.Count() == 0)
		{
			rowAlign[n].SpacingOK = true;
			rowAlign[n].Count = 0;
			emptyRows.push_back(n);
		}
	}
	int maxLengthOfRows = 0;
	int firstN = 0;
	if (SectionOfInterest > 1)
	{
		if (emptyRows.size() > 0)
		{
			if (emptyRows[0] < NumbRows / 3)
				firstN = emptyRows[0];
		}
	}
	for (int n = firstN; n < rowAlign.size() - 1; n++)
	{
		rowAlign[n].Length = 1;
		rowAlign[n].Count = (int)rowAlign[n].Stats.Count();
		if (rowAlign[n].SpacingOK && rowAlign[n].Count > 0)
		{
			for (int nn = n + 1; nn < rowAlign.size(); nn++)
			{
				rowAlign[n].Length++;
				rowAlign[n].Count += (int)rowAlign[nn].Stats.Count();
				if (!rowAlign[nn].SpacingOK || rowAlign[n].Length >= NumbRows || nn >= rowAlign.size() - 1)
				{
					if (rowAlign[nn].Stats.Count() < 1)
					{
						int mm = nn;
						while (rowAlign[mm].Stats.Count() < 1 && mm > n)
							mm--;
						rowAlign[n].Length -= (nn - mm);
					}
					if (rowAlign[n].Length > maxLengthOfRows)
					{
						maxLengthOfRows = rowAlign[n].Length;
						if (Track)
						{
							LogFile << "rowAlign[n].Length >= maxLengthOfRows: " << n << " / " << rowAlign[n].Length << " / " << maxLengthOfRows
								<< " / Index = " << rowAlign[n].Index << " / rowOffset = " << rowOffset << "\n";
						}
						
					}
					break;
				}
			}
		}
	}
	if (rowAlign.size() > 0)
	{
		rowAlign[rowAlign.size() - 1].Length = 1;
		rowAlign[rowAlign.size() - 1].Count = rowAlign[rowAlign.size() - 1].Stats.Count();
	}
	//LogFile << "SOI Final maxLengthOfRows = " << maxLengthOfRows << std::endl;
	std::vector< int > idxWithMaxLength;
	idxWithMaxLength.clear();
	int lastN = rowAlign.size() - maxLengthOfRows;
	if (lastN < 1)
		lastN = 1;
	for (int n = 0; n < lastN; n++)
	{
		if (rowAlign[n].Length >= maxLengthOfRows)
			idxWithMaxLength.push_back(n);
	}
	int bestY;
	if (idxWithMaxLength.size() == 1)
	{
		int idx = idxWithMaxLength[0];
		bestY = rowAlign[idx].Index;
	}
	else
	{
		int idx = idxWithMaxLength[0];
		int maxIdx = idx;
		int maxIdxCount = rowAlign[idx].Count;
		for (int n = 1; n < idxWithMaxLength.size(); n++)
		{
			idx = idxWithMaxLength[n];
			if (rowAlign[idx].Count > maxIdxCount)
			{
				maxIdxCount = rowAlign[idx].Count;
				maxIdx = n;
			}
		}
		bestY = rowAlign[maxIdx].Index;
		//ogFile << "          bestY = " << bestY << std::endl;
	}
	ArraySectionShift[as].y = 1 - bestY;
	if (Track)
	{
		LogFile << "row stats: Array/Section: " << Array << "/" << SectionOfInterest << std::endl;
		LogFile << "maxLengthOfRows: " << maxLengthOfRows << ", bestY = " << bestY << std::endl;
		for (int n = 0; n < idxWithMaxLength.size(); n++)
			LogFile << "     maxLength Index: " << idxWithMaxLength[n] << std::endl;
		LogFile << "\nIndex / Ave / Std / SCnt / LCount / Length / Spacing / OK\n";
		for (int n = 0; n < rowAlign.size(); n++)
		{
			LogFile << rowAlign[n].Index << " / " << rowAlign[n].Stats.Ave() << " / " << rowAlign[n].Stats.Std() << " / " << rowAlign[n].Stats.Count() 
				<< " /" << rowAlign[n].Count 
				<< " / " << rowAlign[n].Length << " / " << rowAlign[n].Spacing << " / " << std::boolalpha << rowAlign[n].SpacingOK << std::endl;
		}
	}

	if (NumbSections > 1)
	{
		Statistics1 minYStats;
		Statistics1 maxYStats;
		minYStats.Clear();
		maxYStats.Clear();
		int lastY = bestY;
		int lastY3 = bestY;
		for (int y = bestY; y < bestY + NumbRows - 1; y++)
		{
			if ((y + rowOffset) < rowAlign.size() && rowAlign[(int)y + rowOffset].Stats.Count() > 0)
				lastY = y;
			if ((y + rowOffset) < rowAlign.size() && rowAlign[y + (int)rowOffset].Stats.Count() >= NumbCols / 3)
				lastY3 = y;
		}
		if (lastY3 > 10)
			lastY = lastY3;
		else if (lastY - lastY3 > 4)
			lastY = (lastY + lastY3) / 2;
		//LogFile << "best/lastY = " << bestY << "/" << lastY << std::endl;
		for (int n = 0; n < BlobNodeList[as].size(); n++)
		{
			if (BlobNodeList[as][n].SectionPosition.y == bestY)
				minYStats.Accumulate(BlobNodeList[as][n].AdjustedCenter2f.y);
			else if (BlobNodeList[as][n].SectionPosition.y == lastY)
				maxYStats.Accumulate(BlobNodeList[as][n].AdjustedCenter2f.y);
		}
		minYStats.Analyze();
		maxYStats.Analyze();
		ArrayYScale[Array] = (float)(maxYStats.Ave() - minYStats.Ave()) / (WellSize2f.y * (float)((size_t)lastY - bestY));
		AdjustedSectionOrigins[Array].first.y = (float)minYStats.Ave() - SectionGap * ArrayYScale[Array];
		AdjustedSectionOrigins[Array].second.y = (float)minYStats.Ave() 
			+ (SectionGap + WellSize2f.y * ((size_t)NumbRows - 1.0f)) * ArrayYScale[Array];

		int firstX = minX;
		int lastX = maxX;
		if (minX + ArraySectionShift[as].x < 1)
			firstX = minX + (1 - ArraySectionShift[as].x - minX);
		if (maxX + ArraySectionShift[as].x > NumbCols)
			lastX = maxX + (NumbCols - ArraySectionShift[as].x - maxX);

		int maxC = 0;
		int maxColumnSum = 0;
		for (int c = 0; c < colCount.size() ; c++)
		{
			if (colCount[c] > maxColumnSum)
			{
				maxColumnSum = colCount[c];
				maxC = c;
			}
		}
		if (maxC > 0)
		{
			for (int c = 0; c < maxC; c++)
			{
				if (colCount[c] > maxColumnSum * 0.8)
				{
					maxC = c;
					break;
				}
			}
		}
		int maxColumn = maxC - colOffset;

		Statistics1 xStats;
		xStats.Clear();
		
		for (int n = 0; n < BlobNodeList[as].size(); n++)
		{
			if (BlobNodeList[as][n].SectionPosition.x == maxColumn)
				xStats.Accumulate(BlobNodeList[as][n].AdjustedCenter2f.x);
		}
		xStats.Analyze();
		maxColumn += ArraySectionShift[as].x;
		AdjustedSectionOrigins[Array].first.x = (float)xStats.Ave() - ((size_t)maxColumn - 1.0f) * WellSpacing2f.x;
		AdjustedSectionOrigins[Array].second.x = AdjustedSectionOrigins[Array].first.x;
	}

	LogFile << "ArraySectionShift[" << as << "].y/x " << ArraySectionShift[as].y << "/" << ArraySectionShift[as].x << std::endl;
	LogFile << "AdjustedSectionOrigins[" << Array << "].first.y/x = " 
		<< AdjustedSectionOrigins[Array].first.y << "/" << AdjustedSectionOrigins[Array].first.x << "\n";
	LogFile << "AdjustedSectionOrigins[" << Array << "].second.y/x = "
		<< AdjustedSectionOrigins[Array].second.y << "/" << AdjustedSectionOrigins[Array].second.x << "\n";
	return true;
}

bool AssignBlobs::DetermineArraySectionShift(std::ofstream& LogFile, std::string BaseName, int Array, int Section)
{
	if (Array < 1 || Array > NumbArrays || Section < 1 || Section > NumbSections || Section == SectionOfInterest)
		return false;
	if (!ArrayUsed[Array])
		return false;

	int as = ArraySectionNumber(Array, Section);

	int rowOffset = 0;
	int colOffset = 0;
	int minX = 2 * NumbCols;
	int maxX = -NumbCols;
	int minY = 2 * NumbRows;
	int maxY = -NumbRows;
	//LogFile << "DetermineArraySectionShift: BlobNodeList[" << as << "].size() = " << BlobNodeList[as].size() << "\n";
	for (int n = 0; n < BlobNodeList[as].size(); n++)
	{
		if (BlobNodeList[as][n].Status > 0)
		{
			int idx = BlobNodeList[as][n].SectionPosition.x;
			int idy = BlobNodeList[as][n].SectionPosition.y;
			if (idx > -NumbCols && idx < 2 * NumbCols && idy > -NumbRows && idy < 2 * NumbRows)
			{
				if (idx < minX)
					minX = idx;
				if (idx > maxX)
					maxX = idx;

				if (idy < minY)
					minY = idy;
				if (idy > maxY)
					maxY = idy;
			}
		}
	}

	if (maxX <= minX || maxY <= minY)
		return false;
	colOffset = -minX;
	rowOffset = -minY;
	std::vector< int > colCount(maxX - minX + 2);
	std::fill(colCount.begin(), colCount.end(), 0);
	std::vector< AlignData > rowAlign(maxY - minY + 2);
	AlignData blankAlignData;
	blankAlignData.Index = 0;
	blankAlignData.Length = 0;
	blankAlignData.Count = 0;
	blankAlignData.Stats.Clear();
	blankAlignData.Spacing = 0.0;
	blankAlignData.SpacingOK = true;
	std::fill(rowAlign.begin(), rowAlign.end(), blankAlignData);
	std::vector< double > rowSpacing(maxY - minY + 2);
	std::fill(rowSpacing.begin(), rowSpacing.end(), 0.0);

	for (int n = 0; n < BlobNodeList[as].size(); n++)
	{
		if (BlobNodeList[as][n].Status > 0)
		{
			int idx = BlobNodeList[as][n].SectionPosition.x;
			int idy = BlobNodeList[as][n].SectionPosition.y;
			if (idx > -NumbCols && idx < 2 * NumbCols && idy > -NumbRows && idy < 2 * NumbRows)
			{
				colCount[idx + colOffset]++;
				rowAlign[idy + rowOffset].Stats.Accumulate(BlobNodeList[as][n].AdjustedCenter2f.y);
			}
		}
	}

	LogFile << "\n\nArray/Section " << Array << "/" << Section << "\n";
	if (Track)
	{
		LogFile << "     Col Count (min/maxX = " << minX << " / " << maxX << ")\n";
		for (int c = minX; c <= maxX; c++)
		{
			LogFile << c << " - " << colCount[c + colOffset] << std::endl;
		}
		LogFile << "     Row Count (min/maxY = " << minY << " / " << maxY << ")\n";
		for (int r = minY; r <= maxY; r++)
		{
			LogFile << r << " - " << rowAlign[r + rowOffset].Stats.Count() << std::endl;
		}
	}
	int bestX = -1;
	int bestXValue = -1;
	if (maxX - minX + 1 <= NumbCols)
	{
		//LogFile << "maxX - minX + 1 / NumbCols = " << maxX - minX + 1 << " / " << NumbCols << std::endl;
		if (Array == 1)
		{
			ArraySectionShift[as].x = NumbCols - maxX;
		}
		else if (Array == NumbArrays)
		{
			ArraySectionShift[as].x = 1 - minX;
		}
		else
		{
			if (maxX <= NumbCols && minX >= 1)
				ArraySectionShift[as].x = 0;
			else if (maxX > NumbCols)
				ArraySectionShift[as].x = NumbCols - maxX;
			else if (minX < 1)
				ArraySectionShift[as].x = 1 - minX;
		}
		if ( Track)
			LogFile << "ArraySectionShift[as].x = " << ArraySectionShift[as].x << std::endl;
	}
	else
	{
		//LogFile << "minX / maxX / maxX - NumbCols = " << minX << " / " << maxX << " / " << maxX - NumbCols << std::endl;
		for (int x = minX; x <= maxX - NumbCols + 1; x++)
		{
			int xcnt = x;
			int cnt = 0;
			while (xcnt + colOffset < colCount.size() && xcnt - x < NumbCols)
			{
				cnt += colCount[xcnt + colOffset];
				xcnt++;
			}
			if (cnt > bestXValue)
			{
				bestXValue = cnt;
				bestX = x;
			}

		}
		ArraySectionShift[as].x = 1 - bestX;
		if ( Track)
			LogFile << "ArraySectionShift[as].x = " << ArraySectionShift[as].x << std::endl;
	}
	std::vector<int> emptyRows;
	emptyRows.clear();
	double dPointedAt;
	for (int n = 0; n < rowAlign.size(); n++)
	{
		rowAlign[n].Index = n - rowOffset;
		rowAlign[n].Stats.Analyze();
		if (n > 1 && rowAlign[n].Stats.Count() > 0)
		{
			int nm1 = n - 1;
			while (nm1 > 0)
			{
				if (rowAlign[nm1].Stats.Count() > 0)
				{
					rowAlign[nm1].Spacing = (rowAlign[n].Stats.Ave() - rowAlign[nm1].Stats.Ave())/WellSpacing2f.y;
					double md = std::modf(rowAlign[nm1].Spacing, &dPointedAt);
					if (md > 0.25 && md < 0.75)
						rowAlign[nm1].SpacingOK = false;
					else
						rowAlign[nm1].SpacingOK = true;
					break;
				}
				nm1--;
			}
		}
		if (rowAlign[n].Stats.Count() == 0)
		{
			emptyRows.push_back(n);
			rowAlign[n].Count = 0;
			rowAlign[n].SpacingOK = true;
		}
	}
	int maxLengthOfRows = 0;
	//LogFile << "rowAlign.size() = " << rowAlign.size() << std::endl;
	for (int n = 0; n < rowAlign.size() - 1; n++)
	{
		rowAlign[n].Length = 1;
		rowAlign[n].Count = (int)rowAlign[n].Stats.Count();
		if (rowAlign[n].SpacingOK && rowAlign[n].Count > 0)
		{
			for (int nn = n + 1; nn < rowAlign.size(); nn++)
			{
				rowAlign[n].Length++;
				rowAlign[n].Count += (int)rowAlign[nn].Stats.Count();
				if (!rowAlign[nn].SpacingOK || rowAlign[n].Length >= NumbRows || nn >= rowAlign.size() - 1)
				{
					if (rowAlign[nn].Stats.Count() < 1)
					{
						int mm = nn;
						while (rowAlign[mm].Stats.Count() < 1 && mm > n)
							mm--;
						rowAlign[n].Length -= (nn - mm);
					}
					if (rowAlign[n].Length > maxLengthOfRows)
					{
						maxLengthOfRows = rowAlign[n].Length;
						//LogFile << "New maxLengthOfRows = " << maxLengthOfRows << std::endl;
					}
					break;
				}
			}
		}
	}
	if (rowAlign.size() > 0)
	{
		rowAlign[rowAlign.size() - 1].Length = 1;
		rowAlign[rowAlign.size() - 1].Count = rowAlign[rowAlign.size() - 1].Stats.Count();
	}
	//LogFile << "Final maxLengthOfRows = " << maxLengthOfRows << std::endl;
	std::vector< int > idxWithMaxLength;
	idxWithMaxLength.clear();
	int lastN = rowAlign.size() - maxLengthOfRows;
	if (lastN < 1)
		lastN = 1;
	//LogFile << "lastN = " << lastN << std::endl;
	for (int n = 0; n < lastN; n++)
	{
		if (rowAlign[n].Length >= maxLengthOfRows)
		{
			if (Track)
			{
				LogFile << "rowAlign[n].Length >= maxLengthOfRows: " << n << " / " << rowAlign[n].Length << " / " << maxLengthOfRows
					<< " / Index = " << rowAlign[n].Index << " / rowOffset = " << rowOffset << "\n";
			}
			idxWithMaxLength.push_back(n);
		}
	}
	int bestYIdx;
	int lastYIdx;
	double bestY;
	double lastY;
	if (idxWithMaxLength.size() == 1)
	{
		int idx = idxWithMaxLength[0];
		bestYIdx = rowAlign[idx].Index;
		int jdx = idx + rowAlign[idx].Length - 1;
		if (jdx >= rowAlign.size())
			jdx = rowAlign.size() - 1;
		lastYIdx = rowAlign[jdx].Index;
		bestY = rowAlign[idx].Stats.Ave();
		lastY = rowAlign[jdx].Stats.Ave();
	}
	else
	{
		int idx = idxWithMaxLength[0];
		int maxIdx = idx;
		int maxIdxCount = rowAlign[idx].Count;
		for (int n = 1; n < idxWithMaxLength.size(); n++)
		{
			idx = idxWithMaxLength[n];
			if (rowAlign[idx].Count > maxIdxCount)
			{
				maxIdxCount = rowAlign[idx].Count;
				maxIdx = n;
			}
		}
		bestYIdx = rowAlign[maxIdx].Index;
		int maxJdx = maxIdx + rowAlign[maxIdx].Length - 1;
		if (maxJdx >= rowAlign.size())
			maxJdx = rowAlign.size() - 1;
		lastYIdx = rowAlign[maxJdx].Index;
		bestY = rowAlign[maxIdx].Stats.Ave();
		lastY = rowAlign[maxJdx].Stats.Ave();
	}
	//LogFile << "          bestY = " << bestYIdx << " : " << bestY << std::endl;
	//LogFile << "          lastY = " << lastYIdx << " : " << lastY << std::endl;
	if (Section > SectionOfInterest)
	{
		ArraySectionShift[as].y = 1 - bestYIdx;
	}
	else
	{
		ArraySectionShift[as].y = NumbRows - lastYIdx;
	}
	if (Track)
	{
		LogFile << "row stats: Array/Section: " << Array << "/" << Section << std::endl;
		LogFile << "maxLengthOfRows/Idx: " << maxLengthOfRows << ", bestY = " << bestY << std::endl;
		for (int n = 0; n < idxWithMaxLength.size(); n++)
			LogFile << "     maxLength index: " << idxWithMaxLength[n] << std::endl;
		LogFile << "\nIndex / Ave / Std / SCnt / LCount / Length / Spacing / OK\n";
		for (int n = 0; n < rowAlign.size(); n++)
		{
			LogFile << rowAlign[n].Index << " / " << rowAlign[n].Stats.Ave() << " / " << rowAlign[n].Stats.Std() << " / " << rowAlign[n].Stats.Count()
				<< " /" << rowAlign[n].Count
				<< " / " << rowAlign[n].Length << " / " << rowAlign[n].Spacing << " / " << std::boolalpha << rowAlign[n].SpacingOK << std::endl;
		}
	}
	LogFile << "ArraySectionShift[" << as << "].y/x " << ArraySectionShift[as].y << "/" << ArraySectionShift[as].x << std::endl;
	return true;
}

int AssignBlobs::GetNumberOfNodes(int Array, int Section)
{
	if (Array < 1 || Array > NumbArrays || Section < 1 || Section > NumbSections)
		return 0;
	if (!ArrayUsed[Array])
		return 0;

	int as = ArraySectionNumber(Array, Section);
	return (int)BlobNodeList[as].size();
}

std::string AssignBlobs::ArraySectionShiftsToString()
{
	std::ostringstream oString;
	oString.str("");
	oString.clear();
	oString << "Array,Section,SecPos.x,SecPos.y,Numb of Blobs\n";
	for (int a = 1; a <= NumbArrays; a++)
	{
		for (int s = 1; s <= NumbSections; s++)
		{
			int as = ArraySectionNumber(a, s);
			oString << a << "," << s << "," << ArraySectionShift[as].x << "," << ArraySectionShift[as].y << "," << NumbInArraySection[as] << "\n";
		}
	}
	return oString.str();
}

void AssignBlobs::UpdateBlobList(std::vector< Blob >& BlobList)
{
	for (int n = 0; n < SortedBlobList.size(); n++)
	{
		int tmp = SortedBlobList[n].SelfIndex;
		if (tmp >= 0 && tmp < BlobList.size())
		{
			BlobList[tmp] = SortedBlobList[n];
		}
	}
}

void AssignBlobs::AssignOtherBlobs(std::ofstream& LogFile, std::vector< Blob >& OtherBlobList, 
	std::vector< double > Results, double Scale)
{
	std::vector< NodeConnection > connections;
	std::vector< NodeConnection > otherConnections;
	NodeConnection newConnection;
	cv::Point2f maxDelta = cv::Point2f((float)((NumbOffsets + 0.5) * WellSpacing2f.x), (float)((NumbOffsets + 0.5) * WellSpacing2f.y));
	LogFile << "maxDelta = " << maxDelta << " - NumbOffsets: " << NumbOffsets 
		<< ", size(): " << OtherBlobList.size() << "/" << SortedBlobList.size() << "\n";
	for (int n = 0; n < OtherBlobList.size(); n++)
	{
		if (OtherBlobList[n].Shape.AspectRatio > 0.75 && OtherBlobList[n].Shape.FilterValue < 10)
		{
			OtherBlobList[n].AdjustedCenter2f.x = (float)(Scale * OtherBlobList[n].RotatedCenter2f.x + Results[0]);
			OtherBlobList[n].AdjustedCenter2f.y = (float)(Scale * OtherBlobList[n].RotatedCenter2f.y + Results[1]);
			OtherBlobList[n].AdjustedCenter.x = (int)round(OtherBlobList[n].AdjustedCenter2f.x);
			OtherBlobList[n].AdjustedCenter.y = (int)round(OtherBlobList[n].AdjustedCenter2f.y);

			cv::Point2f target = OtherBlobList[n].AdjustedCenter2f;
			connections.clear();
			otherConnections.clear();
			for (int b = 0; b < SortedBlobList.size(); b++)
			{
				if (SortedBlobList[b].Status > 0)
				{
					cv::Point2f delta = target - SortedBlobList[b].AdjustedCenter2f;
					if (fabs(delta.x) < maxDelta.x && fabs(delta.y) < maxDelta.y)
					{
						int deltaXI = (int)round(delta.x / WellSpacing2f.x);
						int deltaYI = (int)round(delta.y / WellSpacing2f.y);
						if ((abs(deltaXI) < NumbOffsets && abs(deltaYI) < NumbOffsets) &&
							(abs(deltaXI) == 0 || abs(deltaYI) == 0 || abs(deltaXI) + abs(deltaYI) < NumbOffsets - 1))
						{
							double errX = fabs(delta.x);
							double errY = fabs(delta.y);
							//double errR2 = pow(errX, 2) + pow(errY, 2);
							if (errX < HorzOffsetLimits[abs(deltaXI)].first
								&& errX > HorzOffsetLimits[abs(deltaXI)].second
								&& errY < VertOffsetLimits[abs(deltaYI)].first
								&& errY > VertOffsetLimits[abs(deltaYI)].second)
							{
								int newX = SortedBlobList[b].SectionPosition.x + deltaXI;
								int newY = SortedBlobList[b].SectionPosition.y + deltaYI;
								newConnection.Status = 0;
								newConnection.NodeIndex = b;
								newConnection.Shift = cv::Point(deltaXI, deltaYI);
								if (newX > 0 && newX <= NumbCols && newY > 0 && newY <= NumbRows)
								{
									if (abs(deltaXI) <= 1 && abs(deltaYI) <= 1)
									{
										connections.clear();
										connections.push_back(newConnection);
										break;
									}
									else
									{
										connections.push_back(newConnection);
									}
								}
								otherConnections.push_back(newConnection);
							}
						}
					}
				}
			}
			//LogFile << "connection sizes: " << connections.size() << "/" << otherConnections.size() << std::endl;
			//LogFile << "Assign Other: " << OtherBlobList[n].SelfIndex;
			if (connections.size() > 0)
			{
				if (connections.size() > 1)
					std::sort(connections.begin(), connections.end(), SortConnections);
				//LogFile << " - Numb Connections = " << connections.size() << ", connection[0]: " << connections[0].ToString() << std::endl;
				OtherBlobList[n].Array = SortedBlobList[connections[0].NodeIndex].Array;
				OtherBlobList[n].Section = SortedBlobList[connections[0].NodeIndex].Section;
				OtherBlobList[n].SectionPosition = SortedBlobList[connections[0].NodeIndex].SectionPosition + connections[0].Shift;
				OtherBlobList[n].Status = 1;
			}
			else
			{
				std::vector< std::vector< int > > triples;
				std::vector< int > newTriple(3);
				triples.clear();
				LogFile << " - No connections\n";
				for (int b = 0; b < SortedBlobList.size(); b++)
				{
					cv::Point2f delta = target - SortedBlobList[b].AdjustedCenter2f;
					newTriple[0] = n;
					if (fabs(delta.x) < 5.0 * WellSpacing2f.x && fabs(delta.y) < 5.0 * WellSpacing2f.y)
					{
						newTriple[1] = (int)round(pow(delta.x, 2) + pow(delta.y, 2));
						newTriple[2] = b;
						triples.push_back(newTriple);
					}
				}
				if (triples.size() > 1)
					std::sort(triples.begin(), triples.end(), SortTriples);

				int last = 6;
				if (triples.size() < 6)
					last = (int)triples.size();
				for ( int t = 0; t < last ; t++)
				{
					int b = triples[t][2];
					cv::Point2f delta = target - SortedBlobList[b].AdjustedCenter2f;
					LogFile << n
						<< ", " << OtherBlobList[n].InputCenter2f.y << "/" << OtherBlobList[n].InputCenter2f.x
						<< ", " << target.y << "/" << target.x
						<< " : " << b << ", " << SortedBlobList[b].InputCenter2f.y << "/" << SortedBlobList[b].InputCenter2f.x
						<< ", " << SortedBlobList[b].AdjustedCenter2f.y << "/" << SortedBlobList[b].AdjustedCenter2f.x
						<< " : " << delta.y << "/" << delta.x
						<< " : " << delta.y / WellSpacing2f.y << "/" << delta.x / WellSpacing2f.x
						<< "\n";
				}
				LogFile << "=====\n";
			}
		}
	}
}

bool AssignBlobs::EstimateScale(std::ofstream& LogFile, int SectionOfInterest, double& AveScale, double& XScale, double& YScale)
{
	Statistics1 scaleXStats;
	Statistics1 scaleYStats;
	scaleXStats.Clear();
	scaleYStats.Clear();
	//double delta;
	std::vector< std::vector< int > > arraySectionMatrix;
	arraySectionMatrix.resize((size_t)NumbRows + 1);
	for (int r = 1; r <= NumbRows; r++)
		arraySectionMatrix[r].resize((size_t)NumbCols + 1);

	for (int a = 1; a <= NumbArrays; a++)
	{
		for (int r = 1; r <= NumbRows; r++)
			std::fill(arraySectionMatrix[r].begin(), arraySectionMatrix[r].end(), -1);
		int as = ArraySectionNumber(a, SectionOfInterest);
		for (int b = 0; b < BlobNodeList[as].size(); b++)
		{
			if (BlobNodeList[as][b].Status > 0 )
			{
				int idx = BlobNodeList[as][b].Index;
				if (SortedBlobList[idx].Status > 0 )
				{
					if (SortedBlobList[idx].SectionPosition.y < 1 || SortedBlobList[idx].SectionPosition.y > NumbRows
						|| SortedBlobList[idx].SectionPosition.x < 1 || SortedBlobList[idx].SectionPosition.x > NumbCols)
					{
						LogFile << "For idx = " << idx << ", SectionPosition.x/.y = " << SortedBlobList[idx].SectionPosition.x
							<< "/" << SortedBlobList[idx].SectionPosition.y << std::endl;
					}
					else
						arraySectionMatrix[SortedBlobList[idx].SectionPosition.y][SortedBlobList[idx].SectionPosition.x] = idx;
				}
			}
		}
		double delta;
		for (int r = 1; r <= NumbRows; r++)
		{
			int cCurr = 1;
			for (cCurr = 1; cCurr <= NumbCols; cCurr++)
			{
				if (arraySectionMatrix[r][cCurr] >= 0)
					break;
			}
			int cNext = FindNextColumn(arraySectionMatrix, r, cCurr);
			while (cNext < NumbCols + 1)
			{
				if (cNext <= NumbCols)
				{
					int idx0 = arraySectionMatrix[r][cCurr];
					int idx1 = arraySectionMatrix[r][cNext];
					delta = ((double)SortedBlobList[idx1].AdjustedCenter2f.x - SortedBlobList[idx0].AdjustedCenter2f.x) / ((double)cNext - cCurr);
					scaleXStats.Accumulate(delta);
					cCurr = cNext;
					cNext = FindNextColumn(arraySectionMatrix, r, cCurr);
				}
				else
					break;
			}
		}
		for (int c = 1; c <= NumbCols; c++)
		{
			int rCurr = 1;
			for (rCurr = 1; rCurr <= NumbRows; rCurr++)
			{
				if (arraySectionMatrix[rCurr][c] >= 0)
					break;
			}
			int rNext = FindNextRow(arraySectionMatrix, rCurr, c);
			while (rNext < NumbRows + 1)
			{
				if (rNext <= NumbRows)
				{
					int idx0 = arraySectionMatrix[rCurr][c];
					int idx1 = arraySectionMatrix[rNext][c];
					delta = ((double)SortedBlobList[idx1].AdjustedCenter2f.y - SortedBlobList[idx0].AdjustedCenter2f.y) / ((double)rNext - rCurr);
					scaleYStats.Accumulate(delta);
					rCurr = rNext;
					rNext = FindNextRow(arraySectionMatrix, rCurr, c);
				}
			}
		}
	}
	
	scaleXStats.Analyze();
	scaleYStats.Analyze();
	LogFile << "scaleXStats.Ave/Count: " << scaleXStats.Ave() << "/" << scaleXStats.Count() << ", WellSpacing2f.x: " << WellSpacing2f.x << std::endl;
	LogFile << "scaleYStats.Ave/Count: " << scaleYStats.Ave() << "/" << scaleYStats.Count() << ", WellSpacing2f.y: " << WellSpacing2f.y << std::endl;
	XScale = WellSpacing2f.x / scaleXStats.Ave();
	YScale = WellSpacing2f.y / scaleYStats.Ave();
	if (scaleXStats.Count() > 100)
	{
		if (scaleYStats.Count() > 100)
			AveScale = 0.5 * (XScale + YScale);
		else if (scaleYStats.Count() > 20)
			AveScale = 0.666 * XScale + 0.334 * YScale;
		else
			AveScale = XScale;
	}
	else if (scaleXStats.Count() > 20)
	{
		if (scaleYStats.Count() > 100)
			AveScale = 0.334 * XScale + 0.666 * YScale;
		else if (scaleYStats.Count() > 20)
			AveScale = 0.50 * XScale + 0.50 * YScale;
		else
			AveScale = 1.0;
	}
	else
	{
		if (scaleYStats.Count() > 20)
			AveScale = YScale;
		else
			AveScale = 1.0;
	}
	return AveScale;
}

int AssignBlobs::FindNextColumn(std::vector< std::vector< int > > &ArraySectionMatrix, int Row, int CurrColumn)
{
	if (CurrColumn < NumbCols)
	{
		for (int c = CurrColumn + 1; c <= NumbCols; c++)
		{
			if (ArraySectionMatrix[Row][c] >= 0)
				return c;
		}
		return NumbCols * 3;
	}
	else
		return NumbCols * 3;
}

int AssignBlobs::FindNextRow(std::vector< std::vector< int > >& ArraySectionMatrix, int CurrentRow, int Column)
{
	if (CurrentRow < NumbRows)
	{
		for (int r = CurrentRow + 1; r <= NumbRows; r++)
		{
			if (ArraySectionMatrix[r][Column] >= 0)
				return r;
		}
		return NumbRows * 3;
	}
	else
		return NumbRows * 3;
}

