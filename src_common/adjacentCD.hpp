#ifndef ADJACENT_H
#define ADJACENT_H
//! [includes]
#include <vector>
#include <algorithm>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//#include <iostream>
//#include <string>
//#include <cstdlib>
//! [includes]

#include "blob.hpp"
#include "blob_CD.hpp"
#include "statistics.hpp"

int GetListIndex(cv::Point Shift);
bool GetShiftDirection(int Indice, cv::Point &ShiftDirection);

class NodeConnection
{
public:
	int Status;
	int NodeIndex;
	cv::Point Shift;

	NodeConnection() : Status(0), NodeIndex(-10), Shift(cv::Point(0, 0))
	{}
	~NodeConnection() {}

	NodeConnection(const NodeConnection& Source) : Status(Source.Status), NodeIndex(Source.NodeIndex), 
		Shift(Source.Shift)
	{}

	NodeConnection& operator=(const NodeConnection& Source)
	{
		if (this != &Source)
		{
			Status = Source.Status;
			NodeIndex = Source.NodeIndex;
			Shift = Source.Shift;
		}
		return *this;
	}

	void Clear()
	{
		Status = 0;
		NodeIndex = -10;
		Shift = cv::Point(0,0);
	}

	std::string ToString()
	{
		std::ostringstream oString;
		oString.str("");
		oString.clear();
		oString << NodeIndex << "," << Shift.y << "," << Shift.x;
		return oString.str();
	}

	std::string ToStringHeader()
	{
		return "NodeIndex,Shift.y,Shift.x";
	}
};


bool SortConnections(const NodeConnection& A, const NodeConnection& B);

class Blob_CDNode
{
public:
	int MainListIndex;
	int Index;
	int SelfIndex;
	int Status;
	int Section;
	int SubSection;
	cv::Point SubSectionPosition;
	cv::Point2f InputCenter2f;
	cv::Point2f InputTheta_R2f;
	cv::Point2f Theta_R2f;
	cv::Point2f AdjustedTheta_R2f;
	cv::Point DeviceCenter;
	NodeConnection ReferenceNode;
	std::vector< NodeConnection > Connections;
	std::vector< int > ShiftDirections;
	int NumbDirections;
	/*bool Up;
	bool Down;
	bool Left;
	bool Right;
	bool Connected;*/

	Blob_CDNode() : MainListIndex(-1), Index(-1), SelfIndex(-1), Status(0), Section(0), SubSection(0), SubSectionPosition(cv::Point(0, 0)),
		InputCenter2f(cv::Point2f(0.0, 0.0)), InputTheta_R2f(cv::Point2f(0.0, 0.0)), Theta_R2f(cv::Point2f(0.0, 0.0)), 
		AdjustedTheta_R2f(cv::Point2f(0.0, 0.0)), DeviceCenter(cv::Point(0, 0)), NumbDirections(0)
		/*,Up(false), Down(false), Left(false), Right(false), Connected(false)*/
	{
		ReferenceNode.Clear();
		Connections.clear();
		ShiftDirections.resize(8);
		std::fill(ShiftDirections.begin(), ShiftDirections.end(), 0);
	}

	~Blob_CDNode() {}

	Blob_CDNode(const Blob_CDNode& Source) : MainListIndex(Source.MainListIndex), Index(Source.Index), SelfIndex(Source.SelfIndex), Status(Source.Status),
		Section(Source.Section), SubSection(Source.SubSection), SubSectionPosition(Source.SubSectionPosition), 
		InputCenter2f(Source.InputCenter2f), InputTheta_R2f(Source.InputTheta_R2f), Theta_R2f(Source.Theta_R2f), 
		AdjustedTheta_R2f(Source.AdjustedTheta_R2f), DeviceCenter(Source.DeviceCenter),
		ReferenceNode(Source.ReferenceNode), Connections(Source.Connections), 
		ShiftDirections(Source.ShiftDirections), NumbDirections(Source.NumbDirections)
		/*,
		Up(Source.Up), Down(Source.Down), Left(Source.Left), Right(Source.Right), Connected(Source.Connected)*/
	{}

	Blob_CDNode& operator=(const Blob_CDNode& Source)
	{
		if (this != &Source)
		{
			MainListIndex = Source.MainListIndex;
			Index = Source.Index;
			SelfIndex = Source.SelfIndex;
			Status = Source.Status;
			Section = Source.Section;
			SubSection = Source.SubSection;
			SubSectionPosition = Source.SubSectionPosition;
			InputCenter2f = Source.InputCenter2f;
			InputTheta_R2f = Source.InputTheta_R2f;
			Theta_R2f = Source.Theta_R2f;
			AdjustedTheta_R2f = Source.AdjustedTheta_R2f;
			DeviceCenter = Source.DeviceCenter;
			ReferenceNode = Source.ReferenceNode;
			Connections = Source.Connections;
			ShiftDirections = Source.ShiftDirections;
			NumbDirections = Source.NumbDirections;
			/*Up = Source.Up;
			Down = Source.Down;
			Left = Source.Left;
			Right = Source.Right;
			Connected = Source.Connected;*/
		}
		return *this;
	}

	Blob_CDNode& operator=(const Blob_CD& Source)
	{
		MainListIndex = Source.MainListIndex;
		Index = Source.SelfIndex;
		SelfIndex = -1;
		Status = 0;
		Section = Source.Section;
		SubSection = Source.SubSection;
		SubSectionPosition = Source.SubSectionPosition;
		InputCenter2f = Source.InputCenter2f;
		InputTheta_R2f = Source.InputTheta_R2f;
		Theta_R2f = Source.Theta_R2f;
		AdjustedTheta_R2f = Source.AdjustedTheta_R2f;
		DeviceCenter = Source.DeviceCenter;
		ReferenceNode.Clear();
		Connections.clear();
		ShiftDirections.resize(8);
		std::fill(ShiftDirections.begin(), ShiftDirections.end(), 0);
		NumbDirections = 0;
		return *this;
	}

	void Clear()
	{
		MainListIndex = -1;
		Index = -1;
		SelfIndex = -1;
		Status = 0;
		Section = 0;
		SubSection = 0;
		SubSectionPosition = cv::Point(0, 0);
		InputCenter2f = cv::Point2f(0.0, 0.0);
		InputTheta_R2f = cv::Point2f(0.0, 0.0);
		Theta_R2f = cv::Point2f(0.0, 0.0);
		AdjustedTheta_R2f = cv::Point2f(0.0, 0.0);
		DeviceCenter = cv::Point(0, 0);
		ReferenceNode.Clear();
		Connections.clear();
		ShiftDirections.resize(8);
		std::fill(ShiftDirections.begin(), ShiftDirections.end(), 0);
		NumbDirections = 0;
		/*Up = false;
		Down = false;
		Left = false;
		Right = false;
		Connected = false;*/
	}

	void Reset()
	{
		Status = 0;
		//Array = 0;
		//Section = 0;
		SubSectionPosition = cv::Point(0, 0);
		//InputCenter2f = cv::Point2f(0.0, 0.0);
		//InputCenter2f = cv::Point2f(0.0, 0.0);
		ReferenceNode.Clear();
		Connections.clear();
		ShiftDirections.resize(8);
		std::fill(ShiftDirections.begin(), ShiftDirections.end(), 0);
		NumbDirections = 0;
	}

	void AddConnection(int NodeIdx, cv::Point Shift)
	{
		NodeConnection newConnection;
		newConnection.Status = 0;
		newConnection.NodeIndex = NodeIdx;
		newConnection.Shift = cv::Point(Shift.x, Shift.y);
		if (ShiftDirections[GetListIndex(newConnection.Shift)] == 0)
			NumbDirections++;
		ShiftDirections[GetListIndex(newConnection.Shift)]++;
		Connections.push_back(newConnection);
	}

	/*bool SortConnections(const NodeConnection& A, const NodeConnection& B)
	{
		double a2 = pow(A.Shift.x, 2) + pow(A.Shift.y, 2);
		double b2 = pow(B.Shift.x, 2) + pow(B.Shift.y, 2);
		if (a2 != b2)
			return a2 < b2;
		else if (A.Shift.y == B.Shift.y)
			return A.Shift.x < B.Shift.x;
		else
			return A.Shift.y < B.Shift.y;
	}*/

	std::string ToString(std::string &Header)
	{
		Header = "MainIdx,SelfIndex,Status,Section,SubSec,SecPos.y,SecPos.x,Inp.y,Inp.x,Adj.R,Adj.T,R,Theta,Dev.y,Dev.x,NumbC\n";
		std::ostringstream oString;
		oString.str("");
		oString.clear();
		
		oString << MainListIndex << "," << SelfIndex << "," << Status << "," << Section << "," << SubSection 
			<< "," << SubSectionPosition.y << "," << SubSectionPosition.x
			<< "," << InputCenter2f.y << "," << InputCenter2f.x
			<< "," << AdjustedTheta_R2f.y << "," << AdjustedTheta_R2f.x 
			<< "," << Theta_R2f.y << "," << Theta_R2f.x
			<< "," << DeviceCenter.y << "," << DeviceCenter.x << "," << Connections.size()
			<< "\n";
		return oString.str();
	}

	std::string ToString()
	{
		std::ostringstream oString;
		oString.str("");
		oString.clear();
		oString << MainListIndex << "," << SelfIndex << "," << Status << "," << Section << "," << SubSection
			<< "," << SubSectionPosition.y << "," << SubSectionPosition.x
			<< "," << InputCenter2f.y << "," << InputCenter2f.x
			<< "," << Theta_R2f.y << "," << Theta_R2f.x 
			<< "," << DeviceCenter.y << "," << DeviceCenter.x << "," << Connections.size();
		if (Connections.size() > 0)
		{
			std::sort(Connections.begin(), Connections.end(), SortConnections);
			for (int n = 0; n < Connections.size() && n < 3; n++)
			{
				oString << "," << Connections[n].ToString();
			}
		}
		oString << "\n";
		return oString.str();
	}

	/*void CheckConnections()
	{
		Up = Down = Left = Right = Connected = false;
		bool nw = false;
		bool ne = false;
		bool sw = false;
		bool se = false;
		if (Connections.size() > 0)
		{
			std::sort(Connections.begin(), Connections.end(), SortConnections);
			for (int n = 0; n < Connections.size(); n++)
			{
				if (Connections[n].Shift.x == 0 && Connections[n].Shift.y == 0)
				{

				}
				else if (Connections[n].Shift.x == 0 || abs(Connections[n].Shift.x) < abs(Connections[n].Shift.y))
				{
					if (Connections[n].Shift.y > 0)
						Up = true;
					else
						Down = true;
				}
				else if (Connections[n].Shift.y == 0 || abs(Connections[n].Shift.y) < abs(Connections[n].Shift.x))
				{
					if (Connections[n].Shift.x > 0)
						Right = true;
					else
						Left = true;
				}
				else if (Connections[n].Shift.x > 0 && Connections[n].Shift.y > 0)
					ne = true;
				else if (Connections[n].Shift.x < 0 && Connections[n].Shift.y > 0)
					nw = true;
				else if (Connections[n].Shift.x > 0 && Connections[n].Shift.y < 0)
					se = true;
				else
					sw = true;
			}
			if (!Up)
			{
				if (ne && nw)
					Up = true;
			}
			if (!Right)
			{
				if (ne && se)
					Right = true;
			}
			if (!Down)
			{
				if (se && sw)
					Down = true;
			}
			if (!Left)
			{
				if (sw && nw)
					Left = true;
			}
		}
	}*/
};

#endif // ADJACENT_H

