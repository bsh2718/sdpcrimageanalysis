
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "physicalconstants.h"
#include "statistics.hpp"
#include "blob.hpp"
#include "calccentroid.hpp"
#include "calcperimeter.hpp"
#include "analysisParameters.hpp"
#include "trimRectangle.hpp"

#include "extractBlobsAdaptive.hpp"
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Applies a threshold to background subtracted image and returns a vector of Blobs which meet the criteria
// set in FParams and a second vector of blobs which do not.
//
// int ExtractBlobsAdaptive(std::ofstream &LogFile, double C, int BlockSize, double MinimumPeak,
//	cv::Mat &BackgroundSubImage8U, cv::Mat &BackgroundSubImage32F, cv::Point WellSpacing8U, 
//	cv::Mat &WorkImage2, cv::Mat &WorkImage3, int MinContourLength, int MaxContourLength,
//	AnalysisParameters &FParams, int Set1, int Set2,
//	cv::Mat &Foreground8U, std::vector<Blob> &BlobList, std::vector<Blob> &OtherBlobList, 
//	std::vector<Blob> &BadBlobList, std::vector<Blob> &BackgroundList, bool TrackOutputExtraCSV )
//
//     input
// LogFile - ofstream to logfile
// C - offset for adaptive thresholding
// BlockSize - size of neighbor hood in adaptive thresholding (must be an odd positive integer)
// MinimumPeak - If the maximum intensity within a region marked by the adaptive threshold is less than MinimumPeak
//		then that region will not be treated as a blob
// BackgroundSubImage8U - Background subtracted image, converted to 8 bit.
// BackgroundSubImage32F - Background subtracted image.
// WellSpacing8U - center to center distance between wells, rounded to int.
// WorkImage2 - work space
// WorkImage3 - work space
// MinContourLength - contour length of objects found by thresholding image must be at least this large to be
//		considered a blob
// MaxContourLength - contour length of objects found by thresholding image must be at less than this to be
//		considered a blob
// FParams - See analysisParameters for explanation of criteria
// Set1 - set of parameters in FParams used to sort blobs into BlobList.
// Set2 - set of parameters in FParams used to sort blobs into OtherBlobList
// BlobList - vector of Blob containing well-like blobs which are acceptable for quantitation (pass the critera selected by Set1)
// OtherBlobList - vector of Blob containing well-like blobs which are not acceptable for quantitation (pass the critera selected
//		by Set2, but not those selected by Set1 )
// BadBlobList - vector of Blob containing blobs which did not pass the criteria selected by either Set1 or Set2
// BackgroundList - vector of Blob containing regions which were determined to be part of the background and not droplets. 
//		This list is only created if TrackOutputExtraCSV is true.
// TrackOutputExtraCSV - if true, then BackgroundList is created.
//
//		OutputTime
// Forground8U - mask of all blobs
// BlobList - vector of blobs which meet criteria in FParams for Set1 (used for quantitation)
// OtherBlobList - vector of blobs which meet criteria in FParams for Set2, but not Set1 (not used for quantitation)
// BadBlobList - vector of blobs which do not meet the criteria in FParams for either Set1 or Set2 (not used for quantitation)
//

bool ContourToBlob(AnalysisParameters &FParams, int Set1, int SetN, std::vector< cv::Point> &Contour, 
	cv::Mat &BackgroundSubImage, double MinimumPeak, cv::Mat &WorkImage, double Threshold, Blob &NewBlob)
{
	std::vector< bool > filterResults;
	Statistics1_VectorMinMax blobIntensities;
	NewBlob.Clear();
	BlobShape newShape;
	unsigned int resultCode;
	std::string resultString;
	cv::Moments mom;
	std::string errorMsg;
	std::vector< bool > resultList;
	std::vector<cv::Point> Idx;
	newShape.Clear();
	mom = moments(Contour);
	newShape.Mom = mom;
	newShape.BoundingRect = cv::boundingRect(Contour);
	newShape.AspectRatio = (double)newShape.BoundingRect.width / (double)newShape.BoundingRect.height;
	newShape.Contour = Contour;
	// std::clog << "ContourToBlob: before CalcCentroid" << std::endl;
	CalcCentroid(BackgroundSubImage, Contour, WorkImage, newShape.BoundingRect, newShape.InputCenter2f, newShape.PixelList, newShape.PixelIntensities);
	// std::clog << "ContourToBlob: after CalcCentroid, newShape.PixelIntensities.size() " << newShape.PixelIntensities.size() << std::endl;
	blobIntensities.Clear();
	blobIntensities.Accumulate(newShape.PixelIntensities);
	blobIntensities.Analyze();
	// std::clog << "ContourToBlob: blobIntensities.Max() / Min() " << blobIntensities.Max() << " / " << blobIntensities.Min() << std::endl;
	if ( blobIntensities.Max() < MinimumPeak )
		return false;
	
	cv::Point peak = newShape.InputCenter2f;
	double sum = BackgroundSubImage.at< float >(peak);
	peak.x += 1;
	sum += BackgroundSubImage.at< float >(peak);
	peak.y += 1;
	sum += BackgroundSubImage.at< float >(peak);
	peak.x -= 1;
	sum += BackgroundSubImage.at< float >(peak);
	peak.x -= 1;
	sum += BackgroundSubImage.at< float >(peak);
	peak.y -= 1;
	sum += BackgroundSubImage.at< float >(peak);
	peak.y -= 1;
	sum += BackgroundSubImage.at< float >(peak);
	peak.x += 1;
	sum += BackgroundSubImage.at< float >(peak);
	peak.x += 1;
	sum += BackgroundSubImage.at< float >(peak);
	sum /= 9.0;
	if ( sum < MinimumPeak )
		return false;
	
	newShape.Threshold = Threshold;
	newShape.Perimeter = CalcPerimeter(newShape.Contour);
	newShape.BlobIntensities = blobIntensities;
	newShape.Circularity = ( FOURPI * mom.m00) / (double)pow( newShape.Perimeter, 2);
	
	newShape.FilterValue = FParams.TestBlob(Set1, SetN, newShape, resultCode, resultString );
	newShape.FilterCode = resultCode;
	newShape.FilterString = resultString;
	// int res = FParams.TestBlob(newShape, resultList, errorMsg );
	newShape.FillingInfo.clear();
	NewBlob.InputCenter2f = newShape.InputCenter2f;
	NewBlob.DeviceCenter.x = NewBlob.InputCenter2f.x;
	NewBlob.DeviceCenter.y = NewBlob.InputCenter2f.y;
	NewBlob.Shape = newShape;
	return true;
}

bool ContourToBlob(std::vector< cv::Point> &Contour, 
	cv::Mat &BackgroundSubImage, double MinimumPeak, cv::Mat &WorkImage, double C, Blob &NewBlob)
{
	// std::clog << "Start of ContourToBlob" << std::endl;
	std::vector< bool > filterResults;
	Statistics1_VectorMinMax blobIntensities;
	NewBlob.Clear();
	BlobShape newShape;
	cv::Moments mom;
	std::string errorMsg;
	std::vector< bool > resultList;
	std::vector<cv::Point> Idx;
	newShape.Clear();
	mom = moments(Contour);
	newShape.Mom = mom;
	newShape.BoundingRect = cv::boundingRect(Contour);
	newShape.AspectRatio = (double)newShape.BoundingRect.width / (double)newShape.BoundingRect.height;
	newShape.Contour = Contour;
	// std::clog << "ContourToBlob: before CalcCentroid" << std::endl;
	CalcCentroid(BackgroundSubImage, Contour, WorkImage, newShape.BoundingRect, newShape.InputCenter2f, newShape.PixelList, newShape.PixelIntensities);
	// std::clog << "ContourToBlob: after CalcCentroid" << std::endl;
	blobIntensities.Clear();
	blobIntensities.Accumulate(newShape.PixelIntensities);
	blobIntensities.Analyze();
	if ( blobIntensities.Max() < MinimumPeak )
		return false;
	
	cv::Point peak = newShape.InputCenter2f;
	double sum = BackgroundSubImage.at< float >(peak);
	peak.x += 1;
	sum += BackgroundSubImage.at< float >(peak);
	peak.y += 1;
	sum += BackgroundSubImage.at< float >(peak);
	peak.x -= 1;
	sum += BackgroundSubImage.at< float >(peak);
	peak.x -= 1;
	sum += BackgroundSubImage.at< float >(peak);
	peak.y -= 1;
	sum += BackgroundSubImage.at< float >(peak);
	peak.y -= 1;
	sum += BackgroundSubImage.at< float >(peak);
	peak.x += 1;
	sum += BackgroundSubImage.at< float >(peak);
	peak.x += 1;
	sum += BackgroundSubImage.at< float >(peak);
	sum /= 9.0;
	if ( sum < MinimumPeak )
		return false;
	
	// std::clog << "ContourToBlob: after blobIntensities.Analyze" << std::endl;
	std::vector< int > pixelIntensityHistogram((size_t)blobIntensities.Max() + 2);
	std::fill(pixelIntensityHistogram.begin(), pixelIntensityHistogram.end(), 0);
	int numb = (int)newShape.PixelIntensities.size();
	for ( int n = 0 ; n < numb ; n++ )
	{
		int ntmp = (int)floor(newShape.PixelIntensities[n]);
		if ( ntmp >= 0 )
			pixelIntensityHistogram[ntmp]++;
	}

	newShape.Threshold = C;
	newShape.Perimeter = CalcPerimeter(newShape.Contour);
	newShape.BlobIntensities = blobIntensities;
	newShape.Circularity = ( FOURPI * mom.m00) / (double)pow( newShape.Perimeter, 2);
	
	// int res = FParams.TestBlob(newShape, resultList, errorMsg );
	newShape.FillingInfo.clear();
	NewBlob.InputCenter2f = newShape.InputCenter2f;
	NewBlob.Shape = newShape;
	return true;
}

void GetForegroundAdaptive(cv::Mat &BackgroundSubImage, double C, int BlockSize, cv::Mat &StructElement, 
	cv::Mat &WorkImage2, cv::Mat &WorkImage3, cv::Mat &Foreground8U, int NumbOpen )
{
	WorkImage2 = BackgroundSubImage.clone();
	WorkImage3 = BackgroundSubImage.clone();
	
	cv::adaptiveThreshold(WorkImage2, WorkImage3, 255, cv::ADAPTIVE_THRESH_MEAN_C, cv::THRESH_BINARY, BlockSize, C );
	WorkImage3.convertTo(WorkImage2, CV_8U);
	int v = WorkImage2.rows - 1;
	int h = WorkImage2.cols - 1;
	for (int n = 0 ; n < WorkImage2.cols ; n++ )
	{
		WorkImage2.at<uchar>(0, n) = 0;
		WorkImage2.at<uchar>(v, n) = 0;
	}
	for (int n = 0 ; n < WorkImage2.rows ; n++ )
	{
		WorkImage2.at<uchar>(n, 0) = 0;
		WorkImage2.at<uchar>(n, h) = 0;
	}
	WorkImage2.copyTo(WorkImage3);
	
	// floodFill filss in area around wells
	cv::floodFill(WorkImage3, cv::Point(0,0), cv::Scalar(255));
		 
	// Invert floodfilled image, this leaves non zero values for holes disconnected from edge
	cv::bitwise_not(WorkImage3, WorkImage3);
	
	// Combine the two images to get the closed foreground
	Foreground8U.create(WorkImage2.size(), CV_8U); 
	// cv::bitwise_or(WorkImage2, WorkImage3, Foreground8U );
	Foreground8U = ( WorkImage2 | WorkImage3 );
	
	if ( NumbOpen > 0 )
	{
		Foreground8U.copyTo(WorkImage3);
		cv::morphologyEx(WorkImage3, Foreground8U, cv::MORPH_OPEN, StructElement, cv::Point(-1,-1), NumbOpen);
	}
}

int ExtractBlobsAdaptive(std::ofstream &LogFile, double C, int BlockSize, double MinimumPeak,
	cv::Mat &BackgroundSubImage8U, cv::Mat &BackgroundSubImage32F, cv::Point WellSpacing8U, 
	cv::Mat &WorkImage2, cv::Mat &WorkImage3, int MinContourLength, int MaxContourLength,
	AnalysisParameters &FParams, int Set1, int Set2,
	cv::Mat &Foreground8U, std::vector<Blob> &BlobList, std::vector<Blob> &OtherBlobList, 
	std::vector<Blob> &BadBlobList, std::vector<Blob> &BackgroundList, bool TrackOutputExtraCSV )
{
	if ( Set1 < 0 || Set2 < Set1 || Set2 >= FParams.ParameterSet.size()  )
		return 0;
	
	std::ostringstream oString;
	std::string timeMsg;
	// timeMsg = "Inside ExtractBlobs";
	// OutputTime(timeMsg);
	cv::Mat structElement = cv::getStructuringElement( cv::MORPH_RECT, cv::Size(3, 3), cv::Point(-1, -1) );
	std::vector<std::vector<cv::Point> > trialContours;
	trialContours.clear();
	
	GetForegroundAdaptive(BackgroundSubImage8U, C, BlockSize, structElement, WorkImage2, WorkImage3, Foreground8U, 2 );
	WorkImage2 = Foreground8U.clone();
	
	cv::findContours(WorkImage2, trialContours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE, cv::Point(0, 0) );
	int nCont = (int)trialContours.size();
	std::vector< bool > filterResults;
	Statistics1_VectorMinMax addedIntensities;
	Blob newBlob;
	BlobShape newShape;
	cv::Moments mom;
	std::string errorMsg;
	std::vector< bool > resultList;
	std::vector<cv::Point> Idx;
	// timeMsg = "     Before nCont loop";
	// std::clog << "nCont = " << nCont << std::endl;
	// OutputTime(timeMsg);
	// BlobList.resize(nCont);
	// OtherBlobList.resize(nCont);
	// BadBlobList.resize(nCont);
	std::vector< Blob > allBlobs;
	allBlobs.resize(nCont);
	if ( TrackOutputExtraCSV )
		BackgroundList.resize(nCont);
	int numbBlobList = 0;
	int numbOtherBlobList = 0;
	int numbBadBlobList = 0;
	int numbBackgroundList = 0;
	int numbAllBlobs = 0;
	//cv::Point rectOffset;
	//rectOffset.x = ( WellSpacing8U.x - 1 )/2;
	//rectOffset.y = ( WellSpacing8U.y - 1 )/2;
	cv::Rect roi;
	std::vector< cv::Point > locations;
	// cv::Size imageSize = BackgroundSubImage8U.size();
	WorkImage2 = Foreground8U;
	WorkImage3.setTo(0);
	cv::dilate(WorkImage2, WorkImage3, structElement);
	cv::bitwise_xor(WorkImage2, WorkImage3, WorkImage3 );
	for ( int n = 0 ; n < nCont ; n++ )
	{	
		// std::clog << n << " / " << trialContours[n].size() << " / "  << MinContourLength << std::endl;
		if ( trialContours[n].size() >  MinContourLength && trialContours[n].size() < MaxContourLength )
		{
			if ( ContourToBlob(FParams, Set1, Set2, trialContours[n], BackgroundSubImage32F, MinimumPeak, WorkImage3, C, newBlob) )
			{
				allBlobs[numbAllBlobs] = newBlob;
				numbAllBlobs++;
				// int res = newBlob.Shape.FilterValue;
				// if ( res == Set1 )
				// {
					// newBlob.SelfIndex = numbBlobList;
					// BlobList[numbBlobList] = newBlob;
					// numbBlobList++;
				// }
				// else if ( res > Set1 && res <= Set2 )
				// {
					// newBlob.SelfIndex = numbOtherBlobList;
					// OtherBlobList[numbOtherBlobList] = newBlob;
					// numbOtherBlobList++;
				// }
				// else
				// {
					// newBlob.SelfIndex = numbBadBlobList;
					// BadBlobList[numbBadBlobList] = newBlob;
					// numbBadBlobList++;
				// }
			}
		}
		else if ( TrackOutputExtraCSV )
		{
			if ( ContourToBlob(trialContours[n], BackgroundSubImage32F, MinimumPeak, WorkImage3, C, newBlob) )
			{
				BackgroundList[numbBackgroundList] = newBlob;
				numbBackgroundList++;
			}
			// else
			// {
				// std::cout << "Geting background failed for trialContours[" << n << "].size() = " << trialContours[n].size() << "\n";
			// }
		}
	}
	
	
	
	StatisticsXY blobStats;
	blobStats.Clear();
	for ( int n = 0 ; n < numbAllBlobs ; n++ )
	{
		if ( allBlobs[n].Shape.FilterValue == Set1 )
			blobStats.Accumulate(allBlobs[n].Shape.BlobIntensities.Ave(), allBlobs[n].Shape.BlobIntensities.Sum() );
	}
	blobStats.Analyze();
	double aveThreshold = blobStats.AveX() * 0.20;
	double totThreshold = blobStats.AveY() * 0.20;
	nCont = numbBlobList;
	BlobList.resize(numbAllBlobs);
	OtherBlobList.resize(numbAllBlobs);
	BadBlobList.resize(numbAllBlobs);
	numbBlobList = 0;
	numbOtherBlobList = 0;
	numbBadBlobList = 0;
	for ( int n = 0 ; n < numbAllBlobs ; n++ )
	{
		if ( allBlobs[n].Shape.BlobIntensities.Ave() < aveThreshold && allBlobs[n].Shape.BlobIntensities.Sum() < totThreshold )
		{
			//allBlobs[n].SelfIndex = numbBadBlobList;
			//allBlobs[n].Shape.FilterValue = 10;
			//BadBlobList[numbBadBlobList] = allBlobs[n];
			//numbBadBlobList++;
		}
		else
		{
			int res = allBlobs[n].Shape.FilterValue;
			if ( res == Set1 )
			{
				allBlobs[n].SelfIndex = numbBlobList;
				BlobList[numbBlobList] = allBlobs[n];
				numbBlobList++;
			}
			else if ( res > Set1 && res <= Set2 )
			{
				allBlobs[n].SelfIndex = numbOtherBlobList;
				OtherBlobList[numbOtherBlobList] = allBlobs[n];
				numbOtherBlobList++;
			}
			else
			{
				allBlobs[n].SelfIndex = numbBadBlobList;
				BadBlobList[numbBadBlobList] = allBlobs[n];
				numbBadBlobList++;
			}
		}
	}



	BlobList.resize(numbBlobList);
	OtherBlobList.resize(numbOtherBlobList);
	BadBlobList.resize(numbBadBlobList);
	if ( TrackOutputExtraCSV )
		BackgroundList.resize(numbBackgroundList);
	else
		BackgroundList.clear();

	// timeMsg = "     Before return";
	// OutputTime(timeMsg);
	return (int)BlobList.size();
}

//int ExtractBlobsAdaptive(std::ofstream &LogFile, double C, int BlockSize, double MinimumPeak,
//	cv::Mat &BackgroundSubImage8U, cv::Mat &BackgroundSubImage32F, 
//	cv::Mat &WorkImage2, cv::Mat &WorkImage3, int MinContourLength, 
//	double MinArea, int Set,
//	cv::Mat &Foreground8U, std::vector<Blob> &BlobList )
//{
//	if ( Set < 0 )
//		return 0;
//
//	std::ostringstream oString;
//	std::string timeMsg;
//	// timeMsg = "Inside ExtractBlobs";
//	// OutputTime(timeMsg);
//	cv::Mat structElement = cv::getStructuringElement( cv::MORPH_RECT, cv::Size(3, 3), cv::Point(-1, -1) );
//	// cv::Mat structElementOpen = cv::getStructuringElement( cv::MORPH_RECT, cv::Size(3, 3), cv::Point(1, 1) );
//	std::vector<std::vector<cv::Point> > trialContours;
//	trialContours.clear();
//	
//	GetForegroundAdaptive(BackgroundSubImage8U, C, BlockSize, structElement, WorkImage2, WorkImage3, Foreground8U, 2 );
//
//	WorkImage2 = Foreground8U.clone();
//
//	// BlobList.clear();
//	// BadBlobList.clear();
//	
//	cv::findContours(WorkImage2, trialContours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE, cv::Point(0, 0) );
//	int nCont = (int)trialContours.size();
//	std::vector< bool > filterResults;
//	Statistics1_VectorMinMax blobIntensities;
//	Blob newBlob;
//	// BlobShape newShape;
//	cv::Moments mom;
//	std::string errorMsg;
//	std::vector< bool > resultList;
//	std::vector<cv::Point> Idx;
//	// timeMsg = "     Before nCont loop";
//	// OutputTime(timeMsg);
//	WorkImage3.setTo(0);
//	cv::drawContours(WorkImage3, trialContours, -1, 255, cv::FILLED);
//	BlobList.resize(nCont);
//	int numbBlobList = 0;
//	// int numbBadBlobList = 0;
//	for ( int n = 0 ; n < nCont ; n++ )
//	{	
//		if ( trialContours[n].size() >  MinContourLength )
//		{
//			if ( ContourToBlob(trialContours[n], BackgroundSubImage32F, MinimumPeak, WorkImage3, C, newBlob) )
//			{
//				if ( newBlob.Shape.PixelList.size() >= MinArea )
//				{
//					newBlob.SelfIndex = numbBlobList;
//					newBlob.Shape.FilterValue = Set;
//					BlobList[numbBlobList] = newBlob;
//					numbBlobList++;
//				}
//			}
//		}
//	}
//
//	BlobList.resize(numbBlobList);
//
//	// timeMsg = "     Before return";
//	// OutputTime(timeMsg);
//	return (int)BlobList.size();
//}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Applies a threshold to background subtracted image and returns the number of blobs with FilterValue = 1
//
//	LogFile - ofstream to logfile
//	ThresholdBck - background level
//	BackgroundSubImage8U - Image
//	Foreground8U - scratch space
//	WorkImage3 - scratch space
//	MinContourLength - contour length of objects found by thresholding image must be at least this large to be
//		considered a blob
//	FParams - See analysisParameters for explanation of criteria
//
//		OutputTime


int CountBlobsAdaptive(std::ofstream &LogFile, double C, int BlockSize, double MinimumPeak, cv::Mat &BackgroundSubImage8U, 
	cv::Mat &Foreground8U, cv::Mat &WorkImage2, cv::Mat &WorkImage3, int MinContourLength, 
	AnalysisParameters &FParams, int Set )
{
	std::ostringstream oString;
	std::string timeMsg;
	// timeMsg = "Inside ExtractBlobs";
	// OutputTime(timeMsg);
	std::vector<Blob> blobList;
	blobList.clear();
	// cv::Mat structElementOpen = cv::getStructuringElement( cv::MORPH_RECT, cv::Size(3, 3), cv::Point(1, 1) );
	cv::Mat structElement = cv::getStructuringElement( cv::MORPH_RECT, cv::Size(3, 3), cv::Point(-1, -1) );
	std::vector<std::vector<cv::Point> > trialContours;
	trialContours.clear();
	GetForegroundAdaptive(BackgroundSubImage8U, C, BlockSize, structElement, WorkImage2, WorkImage3, Foreground8U, 2 );

	WorkImage2 = Foreground8U.clone();

	cv::findContours(Foreground8U, trialContours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE, cv::Point(0, 0) );
	int nCont = (int)trialContours.size();
	std::vector< bool > filterResults;
	Statistics1 blobIntensities;
	Blob newBlob;
	BlobShape newShape;
	cv::Moments mom;
	std::string errorMsg;
	std::vector< bool > resultList;
	std::vector<cv::Point> Idx;
	// timeMsg = "     Before nCont loop";
	// OutputTime(timeMsg);
	int numbFound = 0;
	// LogFile << " nCont = " << nCont << std::endl;
	for ( int n = 0 ; n < nCont ; n++ )
	{	
		if ( trialContours[n].size() >  MinContourLength )
		{
			double min;
			double max;
			newShape.Clear();
			mom = moments(trialContours[n]);
			newShape.BoundingRect = cv::boundingRect(trialContours[n]);
			cv::Mat rect = Foreground8U(newShape.BoundingRect);
			int area = cv::countNonZero(rect);
			cv::minMaxLoc(rect, &min, &max, NULL, NULL);
			// if ( FParams.TestBlob(Set, newShape, area) == 1 )
				// numbFound++;
			if ( max > MinimumPeak )
			{
				newShape.AspectRatio = (double)newShape.BoundingRect.width / (double)newShape.BoundingRect.height;;
				newShape.Perimeter = CalcPerimeter(trialContours[n]);
				newShape.Circularity = ( FOURPI * mom.m00) / (double)pow( newShape.Perimeter, 2); // 1?
				int fc = FParams.TestBlob(Set, newShape, area);
				if ( fc == 1 )
					numbFound ++; 
			}
		}
	}
	// timeMsg = "     Before return";
	// OutputTime(timeMsg);
	return numbFound;
}

//int CountBlobsAdaptive(std::ofstream &LogFile, double C, int BlockSize, double MinimumPeak, cv::Mat &BackgroundSubImage8U, 
//	cv::Mat &Foreground8U, cv::Mat &WorkImage2, cv::Mat &WorkImage3, int MinContourLength, 
//	double MinArea, std::vector< cv::Point > &BPositions, std::vector< double > &BAreas )
//{
//	std::ostringstream oString;
//	std::string timeMsg;
//	// timeMsg = "Inside ExtractBlobs";
//	// OutputTime(timeMsg);
//	std::vector<Blob> blobList;
//	blobList.clear();
//	cv::Mat structElement = cv::getStructuringElement( cv::MORPH_RECT, cv::Size(3, 3), cv::Point(-1, -1) );
//	std::vector<std::vector<cv::Point> > trialContours;
//	trialContours.clear();
//
//	GetForegroundAdaptive(BackgroundSubImage8U, C, BlockSize, structElement, WorkImage2, WorkImage3, Foreground8U, 2 ); // 2?
//
//	WorkImage2 = Foreground8U.clone();
//
//	cv::findContours(Foreground8U, trialContours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE, cv::Point(0, 0) );
//	int nCont = (int)trialContours.size();
//	std::vector< bool > filterResults;
//	Statistics1 blobIntensities;
//	Blob newBlob;
//	BlobShape newShape;
//	cv::Moments mom;
//	std::string errorMsg;
//	std::vector< bool > resultList;
//	std::vector<cv::Point> Idx;
//	// timeMsg = "     Before nCont loop";
//	// OutputTime(timeMsg);
//	int numbFound = 0;
//	// LogFile << " nCont = " << nCont << std::endl;
//	cv::Point newBPosition;
//	BPositions.clear();
//	BAreas.clear();
//	for ( int n = 0 ; n < nCont ; n++ )
//	{	
//		if ( trialContours[n].size() >  MinContourLength )
//		{
//			double min;
//			double max;
//			newShape.Clear();
//			// mom = moments(trialContours[n]);
//			newShape.BoundingRect = cv::boundingRect(trialContours[n]);
//			cv::Mat rect = Foreground8U(newShape.BoundingRect);
//			int area = cv::countNonZero(rect);
//			cv::minMaxLoc(rect, &min, &max, NULL, NULL );
//			// newShape.AspectRatio = (double)newShape.BoundingRect.width / (double)newShape.BoundingRect.height;;
//			// newShape.Perimeter = CalcPerimeter(trialContours[n]);
//			// newShape.Circularity = ( FOURPI * mom.m00) / (double)pow( newShape.Perimeter, 2);
//			// if ( FParams.TestBlob(Set, newShape, area) == 1 )
//			if ( area >= MinArea && max > MinimumPeak )
//			{
//				numbFound++;
//				newBPosition.x = (int)round(newShape.BoundingRect.x);
//				newBPosition.y = (int)round(newShape.BoundingRect.y);
//				BPositions.push_back(newBPosition);
//				BAreas.push_back(area);
//			}
//		}
//	}
//	// timeMsg = "     Before return";
//	// OutputTime(timeMsg);
//	return numbFound;
//}