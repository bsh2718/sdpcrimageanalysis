#ifndef INPUTERRMESSAGE_HPP
#define INPUTERRMESSAGE_HPP

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <string>
#include <sstream>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
//#include <cmath>
#include <algorithm>
#include <vector>
//#include <iomanip>

//! [includes]

//! [namespace]
//using namespace cv;
//! [namespace]
//using namespace std;

// This procedure creates an error message to be sent to the calling procedure.  It is intended
// for errors involved in inputing data from files. It is not visible to any procedure not in this file.
//
void InputErrMessage(std::string &BlobFileName, int N, const std::string &Message , std::string &ErrorMsg);

#endif  // INPUTERRMESSAGE_HPP