///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// void InputErrMessage(std::string &BlobFileName, int N, const std::string &Message , std::string &ErrorMsg)
//
// This procedure creates an error message to be sent to the calling procedure.  It is intended
// for errors involved in inputing data from files.
//
// 	Input
// BlobFileName - name of file with blob information which is being read in
// N - record number for which error occurred
// Message - Description of error
// 	Output
// ErrorMsg - Message constructed from inputs.
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//! [includes]
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <string>
#include <sstream>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
//#include <cmath>
#include <algorithm>
#include <vector>
//#include <iomanip>

#include "inputErrMessage.hpp"
//! [includes]

//! [namespace]
//using namespace cv;
//! [namespace]
//using namespace std;


void InputErrMessage(std::string &BlobFileName, int N, const std::string &Message , std::string &ErrorMsg)
{
	std::ostringstream oString;
	oString.str("");
	oString.clear();
	oString << "Error Reading Information from  " << BlobFileName << ", Record = " << N << ", Entry = " << Message;
	ErrorMsg = oString.str();
}
