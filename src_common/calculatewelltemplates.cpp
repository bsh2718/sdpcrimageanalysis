//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// bool CalculateWellTemplates( cv::Size2f WellSize, double WellArea, double TopFraction, int MaxNumber,
//	std::vector< std::vector< cv::Point > > &TemplatePixels, std::vector< double > &TemplatePixelFraction )
//
// Sizing analysis searches for top intensity area of each blob.  To do this it needs a set of templates. The
// first template is an ellipical region with an area equal to (Well Area * TopFraction * 0.10) with the same
// aspect ratio as a well. For each succesive template, the nth template is the set of pixels which must be
// added to the (n-1)th template to increase its area by at least 0.5 * circumference of the (n-1)th template.
// Since the image is composed of pixels, the actual increase in area from one template to the next is usually
// a little larger.
//
// 	Input
// WellSize - Size of well (cv::Size2f)
// WellArea - Area of well (double)
// TopFraction - If droplet fills the well in the Z-direction, then a portion of the droplet is pressed against
// 	the top and bottom (in Z) surfaces of the well and the fluorescent intensity from this region should be
// 	reasonably constant and be larger on average than any other region in the blob.  If this top intensity
// 	region is at least TopFraction of the well area, then the blob is assumed to fill the well in Z within
// 	that region and the intensity of that region can be used to determine the filling fraction of the well.
// MaxNumber - Procedure continues to calculate templates until the last template has at least MaxNumber pixels
// 	in it.
// 	
// 	Output
// TemplatePixels - Each template is stored as a vector of pixels.  TemplatePixels is a vector of those templates
// 	(std::vector< std::vector< cv::Point > >)
// TemplatePixelFraction - For each template the actual fraction of the total well area is calculated and
// 	stored in this vector (std::vector< double>)
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include <iostream>
// #include <sstream>
#include <fstream>
// #include <string>
#include <cstdlib>
#include <vector>
#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "physicalconstants.h"
#include "calculatewelltemplates.hpp"

double ECircumference(double A, double B)
{
	double h;
	double aA = fabs(A);
	double aB = fabs(B);
	if ( aA > aB )
		h = pow(aA - aB, 2)/pow(aA + aB, 2);
	else
		h = pow(aB - aA, 2)/pow(aB + aA, 2);
	
	return PI * ( aA + aB )* ( 1.0 + (3.0 * h)/(10.0 + sqrt(4.0 - 3.0*h)) );
}

bool InsideE(double A, double B, double X, double Y)
{
	if ( pow(X/A, 2) + pow(Y/B, 2) > 1.0 )
		return false;
	else
		return true;
}

double EArea( double A, double B )
{
	double area = 0;
	for ( int x = 0 ; x <= A ; x++ )
	{
		for ( int y = 0 ; y <= B ; y++ )
		{
			if ( InsideE( A, B, x, y) )
			{
				if ( x == 0 && y == 0 )
					area += 0.25;
				else if ( x == 0 || y == 0 )
					area += 0.5;
				else
					area += 1.0;
			}
		}
	}
	return 4.0 * area;
}

int EList( double A, double B, std::vector< cv::Point > &PList )
{
	PList.clear();
	// std::clog << "EList \n";

	for ( int x = -(int)ceil(A+0.1) ; x < ceil(A+0.1) ; x++ )
	{
		for ( int y = -(int)ceil(B+0.1) ; y < ceil(B+0.1) ; y++ )
		{
			if ( InsideE( A, B, x, y) )
			{
				PList.push_back(cv::Point(x, y) );
			}
		}
	}
	return (int)PList.size();
}

bool CalculateWellTemplates( cv::Size2f WellSize, double WellArea, double TopFraction, int MaxNumber,
	std::vector< std::vector< cv::Point > > &TemplatePixels, std::vector< double > &TemplatePixelFraction )
{
	// std::clog << "Inside CalculateWellTemplates, first WellArea = " << WellArea << "\n";
	// std::clog << "      WellSize.width = " << WellSize.width << ", WellSize.height = " << WellSize.height << ", TopFraction = " << TopFraction << "\n";
	if ( WellArea < 20.0 || WellSize.width < 2 || WellSize.height < 2 || MaxNumber < WellArea )
		return false;
	
	double ratio = WellSize.width/WellSize.height;
	double x0;
	double y0;
	std::vector< cv::Point > pList;
	TemplatePixels.clear();
	if ( ratio > 1.0 )
	{
		y0 = 1.01;
		x0 = ratio * 1.01;
	}
	else
	{
		x0 = 1.01;
		y0 = 1.01/ratio;
	}
	double targetArea = WellArea * TopFraction * 0.1;
	if ( targetArea > WellArea )
		targetArea = WellArea;
	double area = EArea(x0, y0);
	
	while ( area < targetArea  )
	{
		if ( ratio > 1.0 )
		{
			y0 += 0.5;
			x0 = ratio * y0;
		}
		else
		{
			x0 += 0.5;
			y0 = x0/ratio;
		}
		area = EArea(x0, y0);
	}
	targetArea = MaxNumber;
	// std::clog << "Inside CalculateWellTemplates, first area = " << area << ", size of pList = " << pList.size() << "\n";
	EList(x0, y0, pList);
	TemplatePixels.push_back(pList);
	TemplatePixelFraction.push_back(area/WellArea);
	std::vector< cv::Point > interior = pList;
	double cir = ECircumference(x0, y0);
	double intermedTarget = area + cir * 0.5;
	// std::clog << "     (x0,y0) = (" << x0 << ", " << y0 << ") cir = " << cir << ", intermedTarget = " << intermedTarget << "\n";
	std::vector< cv::Point > tmpList;
	while ( area < MaxNumber )
	{
		while ( area < intermedTarget )
		{
			if ( ratio > 1.0 )
			{
				y0 += 0.3;
				x0 = ratio * y0;
			}
			else
			{
				x0 += 0.3;
				y0 = x0/ratio;
			}
			area = EArea(x0, y0);
		}
		EList(x0, y0, tmpList);
		pList.clear();
		for ( int n = 0 ; n < tmpList.size() ; n++ )
		{
			bool flag = true;
			for ( int m = 0 ; m < interior.size() ; m++ )
			{
				if ( interior[m].x == tmpList[n].x && interior[m].y == tmpList[n].y )
				{
					flag = false;
					break;
				}
			}
			if ( flag )
				pList.push_back( tmpList[n] );
		}
		TemplatePixels.push_back(pList);
		TemplatePixelFraction.push_back(area/WellArea);
		// std::clog << "tmpList.size() " << tmpList.size() << ", pList.size() " << pList.size() << "TemplatePixels.size() " << TemplatePixels.size() << "\n";
		interior = tmpList;
		area = EArea(x0, y0);
		cir = ECircumference(x0, y0);
		intermedTarget = area + cir * 0.5;
		// std::clog << "     (x0,y0) = (" << x0 << ", " << y0 << ") cir = " << cir << ", intermedTarget = " << intermedTarget << "\n";	
	}
	return true;
}
