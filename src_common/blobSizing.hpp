#ifndef BLOBSIZING_HPP
#define BLOBSIZING_HPP

//! [includes]
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "statistics.hpp"
#include "analysisParameters.hpp"
#include "blob.hpp"


bool WriteSizingFile(std::vector< Blob > &BlobList, std::vector< Blob > &PoorBlobList, std::vector< Blob > &BadBlobList,
	std::vector< double > &Results, int SectionOfInterest,
	double MinRingAve, double MaxFillingRatio, AnalysisParameters &FParams, int Set, cv::Mat &BackgroundSubImage32F, cv::Mat &ThresholdedImage,
	std::string &OutName, double ImageScale, double ImageMinimum,
	cv::Mat &GImage, cv::Mat &Mask, cv::Mat &ContMask, bool TrackFilling );

bool WriteSizingFile(std::vector< BlobSummary > &BlobList, std::vector< double > &Results,
	double MinRingAve, double MaxFillingRatio, AnalysisParameters &FParams, int Set, cv::Mat &BackgroundSubImage32F,
	std::string &OutName, cv::Mat &GImage );
	
bool ReadSizingFile(std::string &FileName, std::vector< BlobSummary > &BlobList, int &NumbBlobs, std::vector< double > &Results,
	double &MinRingAve, double &MaxFillingRatio, MinMaxGroup &FilterParameters, std::string &ErrorMsg );
	
int CalculateFilling(std::ofstream &LogFile, const cv::Mat &BackgroundSubImage32F, std::vector< Blob > &BlobList, 
	int TopNumb, std::string &OutputName, double ScaledWellArea, double MinRingAve, double MaxFillingRatio,
	cv::Point2f WellSpacing2f,
	std::vector< std::vector< cv::Point > > &TemplatePixels, std::vector< double > &TemplatePixelFraction );
	
#endif  // BLOBSIZING_HPP