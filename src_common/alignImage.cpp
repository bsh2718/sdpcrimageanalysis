#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "statistics.hpp"
#include "blob.hpp"
//#include "blobSortMethods.hpp"
#include "readinfofile.hpp"
#include "writeBlobFile.hpp"
#include "xyShift.hpp"
#include "assignBlobs.hpp"
#include "xyShift.hpp"
#include "trimRectangle.hpp"
#include "adjacent.hpp"
#include "assignBlobs.hpp"
#include "alignImage.hpp"


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// double AlignImageDC(std::ofstream &LogFile, std::string &BaseName, std::vector< double > &Init, 
// 	std::vector< Blob > &BlobList, std::vector< Blob > &OtherBlobList, std::vector< Blob >& BadBlobList,
// 	cv::Mat &CoordImage, cv::Mat &ArrSecImage, std::vector< ParameterLimit > PLimits, 
// 	std::vector< std::vector< int > > ArrSecMatrix, std::vector< ArraySection > ArrSecList, 
// 	cv::Size ImageSize, cv::Point2f WellSpacing, cv::Size2f WellSize, 
// 	int NumbArrays, int NumbSections, int NumbCols, int NumbRows, 
//  std::vector< DeviceOrigins >& ArrayOrigins,
//  std::vector< DeviceOrigins > &SectionOrigins, int &SectionOfInterest,
// 	std::vector< double > &Results, bool TrackAlignment, std::string &ErrMsg )
//
// Shifts positions of centroids in X and Y direction and adjusts the scale to obtain the best alignment with the 
// device grid.  The centroid positions must have already been corrected for angle. 
// 
//			Input
// LogFile - ofstream connected to log filebuf
// BaseName - project name
// Init	- (X-Offset_Init, Y-Offset_Init, Angle, Scale_Init) Angle is the value obtained from the Angle alignment.
//		The two offsets and the scale are initial guesses
// BlobList - vector of Blob. Includes well-like blobs which pass criteria to be used for quantitation.
// OtherBlobList - vector of Blob. Includes well-like blobs which do not pass criteria to be used for quantitation,
//		but appear to be droplets in wells.
// BadBlobList - vector of Blob. Includes blobs which do not resemble droplets and are not going to be used for
//      quantitation.
// CoordImage - 24 bit color image of the device. If a pixel is in a well, then its color
//		indicates the Array, Section, Column and Row of that well.  If a pixel is in a guard region, it
//		indicates which guard region(s) it is in.
// ArrSecImage - 8 bit gray scale image.  For each Array/Section pair in the device, a region is outlined using the
//		outside edges of the wells on the edge of the Array/Section.  The outline is the smallest region which encloses
//		all of the wells of that Array/Section.  The interior of each of these regions is non zero.  Pixels outside
//		of this region are set to zero.
// PLimits - Vector of ParameterLimit (readinfofile.hpp). There is one for each alignment varaiable (0: XShift, 1: YShift,
//      2: Rotation Angle, 3: Scaling Factor). The BaseValue element is the initial guess at a value for the parameter and 
//      Limit the maximum that the parameter is allowed to vary from the BaseValue.
// ArrSecMatrix - 2D vector of size [NumbArrays+1][NumbSections+1].  If a particular Array and Section are to be analyzed
//		in this image. If ArrSecMatrix[a][s] is not zero then Array a and Section s are to be analyzed. 
// ArrSecList - Vector of Array/Section pairs which are to be analyzed in the image.  If images of the device are
//		taken at multiple positions, then this vector will not include all of the Array/Section pairs in the device,
//		only those expected to be in the image.
// ImageSize - size of the image.
// WellSpacing - center to center distance of wells in pixels
// WellSize - size of wells in pixels
// NumbArrays - number of arrays in device
// NumbSections - number of sections in device
// NumbCols - number of columns in a single Array/Section.  This must be the same for all Array/Sections
// NumbRows - number of rows in a single Array/Section.  This must be the same for all Array/Sections
// Results  - (X-Offset_Final, Y-Offset_Final, Angle, Scale_Final)
// ArrayOrigins - Vector of DeviceOrigins (deviceOrigins.hpp).  Index ranges from 1 to the number of arrays on device.
//      The origin variable is the location of the upper left well of the array.  Only the x coordinate is used.
// SectionOrigins -  Vector of DeviceOrigins (deviceOrigins.hpp).  Index ranges from 1 to the number of sections on device.
//      The origin variable is the location of the upper left well of the section.  Only the y coordinate is used.
// SectionOfInterest - While each image can contain multiple arrays, only one section of each array is the SectionOfInterest.
// TrackAlignment - Additional information is output if this is true
// ErrMsg - If there is a problem with this step, an error message is placed here.  Otherwise the string is empty.
//
// 1. XYShift() takes the coordinates of the blobs (RotatedCenter) and does an alignment in X and Y (starting from the
// inital guesses for them) to maximize the number of blobs which are in one of the non-zero areas of ArrSecImage.
//
// 2. If TrackAlignment is true, then the result of 1 and the initial value of the scale are applied to the blob's 
// rotated coordinates (RotatedCenter2f) and stored in DeviceCenter2f. It counts and reports number of droplets 
// whose shifted position would place them within CoordImage.
//
// 3. An AssignBlobs object (assigner) is constructed and initialized with information about the blobs and the device.
// The SectionOfInterest is determined by determining which section has the most blobs in it.  (This might be changed
// in the future by simply having the SectionOfInterest dictated by the calling program.)
// 
// 4. assigner works by determining which blobs are adjacent to each other and separated by a distance which is an
// integral multiple of the WellSpacing.  The WellSpacing has x and y components which corresponds to spacing in the
// horizontal (row) and vertical (column) directions.  For each array, an initial blob is selected in the SectionOfInterest
// and denoted as the (0,0) blob. All blobs which are separated by an integral muliple of steps in both the horizontal 
// and vertical directions from the (0,0) blob are assigned a relative (row, colum) position.  Additional blobs are assigned
// a (row, column) position by relating its position to the (row, column) position of the nearest assigned blob.  This 
// continues until as many blobs as possible have been assigned for each array and SectionOfInterest.  PLimits[3] is the
// scaling factor and the PLimits[3].Limit is the largest deviation from unity that the scale factor is expected to be.
// This is used to estimate how close to a multiple WellSpacing a distance must be to be considered an integral 
// number of steps away.
// 
// 5. The network of connected blobs is then shifted until the nextwork fits in the blobs grid of wells.  The upper
// left corner of the wells in each section is the (row=1, column=1) well.  The shifts must result in all the droplets
// being assigned positions in the grid of wells with 
//      rows = 1, ..., NumbRows       and columns = 1, ..., NumbCols
// If the network doesn't fit, that is its too wide or too tall, then it means a blob adjacent to the wells is being
// treated as a droplet and the assigner determines which one it is and excludes it from the assignment.
// 
// 6. This process is then repeated with the other sections in the device which might be visible in the image.
//
// 7. The original BlobList is updated with the Array, Section, SectionPosition.Row and SectionPosition.Column assignments
//
// 8. The position of the blobs in the OtherBlobList and the BadBlobList are compared to the assigned blobs to determine
// if any of them are droplets in wells.
// 
// 9. The scale factor for the image is determined by comparing the average vertical and horizontal distances between
// adjacent wells with the expected distances as specified in WellSpacing.
//
//

int AlignImage(std::ofstream &LogFile, std::string &BaseName, std::vector< double > &Init, 
	std::vector< Blob > &BlobList, std::vector< Blob > &OtherBlobList, std::vector< Blob >& BadBlobList,
	cv::Mat &CoordImage, cv::Mat &ArrSecImage, std::vector< ParameterLimit > PLimits, 
	std::vector< std::vector< int > > ArrSecMatrix, std::vector< ArraySection > ArrSecList, 
	cv::Size ImageSize, cv::Point2f WellSpacing, cv::Size2f WellSize, 
	int NumbArrays, int NumbSections, int NumbCols, int NumbRows, 
	std::vector< DeviceOrigins >& ArrayOrigins,
	std::vector< DeviceOrigins > &SectionOrigins, int &SectionOfInterest,
	std::vector< double > &Results, bool TrackAlignment, std::string &ErrMsg )
{
	std::ostringstream oString;
	std::string outName;
	std::vector< double > searchResults = Init;
	Results = Init;
	ErrMsg.clear();
	cv::Size imageSize = CoordImage.size();
	cv::Point tmpPt;
	int numbBlobs = (int)BlobList.size();
	double arrayWidth = ( NumbCols - 1.0 ) * WellSpacing.x + WellSize.width;
	std::vector< cv::Point > pl;
	if ( TrackAlignment )
		LogFile << "\n>>>>> First call to XYShift()\n";

	// 1.
	XYShift( LogFile, Init[3], 4,
		BlobList, ArrSecMatrix,
		ArrSecImage, PLimits, 
		Results, TrackAlignment, ErrMsg );
	int numbInSubImage = 0;
	// 2.
	if ( TrackAlignment )
	{
		for ( int n = 0 ; n < BlobList.size() ; n++ )
			if ( BlobList[n].ApplyShiftScale(Results[0], Results[1], Results[3], CoordImage.size() ) )
				numbInSubImage++;
		if (TrackAlignment)
		{
			LogFile << "After XYShift, Results = " << Results[0] << " / " << Results[1] << " / " << Results[2] << " / " << Results[3] << std::endl;
			LogFile << "     Numb in SubImage = " << numbInSubImage << std::endl;
		}

		for (int n = 0; n < OtherBlobList.size(); n++)
			if (OtherBlobList[n].ApplyShiftScale(Results[0], Results[1], Results[3], CoordImage.size()))
				numbInSubImage++;
		
		if (TrackAlignment)
		LogFile << "Including Other Blobs, Numb in SubImage = " << numbInSubImage << std::endl;

		for (int n = 0; n < BadBlobList.size(); n++)
			if (BadBlobList[n].ApplyShiftScale(Results[0], Results[1], Results[3], CoordImage.size()))
				numbInSubImage++;

		if (TrackAlignment)
		LogFile << "Including Bad Blobs, Numb in SubImage = " << numbInSubImage << std::endl;
	}
	// 3.
	AssignBlobs assigner;
	assigner.Initialize(LogFile, BaseName, Results,
		BlobList, ArrSecMatrix,
		ArrSecImage.size(), WellSpacing, WellSize,
		ArrayOrigins, SectionOrigins,
		NumbArrays, NumbSections, NumbCols, NumbRows, Results[3], PLimits[3].Limit,
		TrackAlignment, ErrMsg);

	SectionOfInterest = assigner.GetSectionOfInterest();
	// 4. 
	for (int a = 1; a <= NumbArrays; a++)
	{
		if ( TrackAlignment)
			LogFile << "Before ConnectNodes, array/SectionOfInterest: " << a << "/" << SectionOfInterest << "\n";
		if (assigner.GetNumberOfNodes(a, SectionOfInterest) > 10)
		{
			// 4.
			if (assigner.ConnectNodes(LogFile, BaseName, a, SectionOfInterest, TrackAlignment))
			{
				// 5.
				if (assigner.EvaluateNodesSOI(LogFile, BaseName, a))
				{
					cv::Point corner11;
					assigner.GetCorner11(a, SectionOfInterest, corner11);
					if ( TrackAlignment)
						LogFile << "     Position of (1,1) blob: " << corner11 << std::endl;

					// 6.
					if (NumbSections > 1)
					{
						int s0 = SectionOfInterest - 1;
						if (s0 > 0 && assigner.GetNumberOfNodes(a, s0) > 10)
						{
							if (assigner.ConnectNodes(LogFile, BaseName, a, s0, TrackAlignment))
							{
								assigner.EvaluateNodes(LogFile, BaseName, a, s0);
							}
						}
						s0 = SectionOfInterest + 1;
						if (s0 <= NumbSections && assigner.GetNumberOfNodes(a, s0) > 10)
						{
							if (assigner.ConnectNodes(LogFile, BaseName, a, s0, TrackAlignment))
							{
								assigner.EvaluateNodes(LogFile, BaseName, a, s0);
							}
						}
					}
				}
				else
					LogFile << "\n>>>>> ConnectNodes for Array " << a << ", Section " << SectionOfInterest << " failed\n" << std::endl;
			}
			else
				LogFile << "\n>>>>> ConnectNodes for Array " << a << ", Section " << SectionOfInterest << " failed\n" << std::endl;
		}
		else
			LogFile << "\n>>>>> Fewer than 10 nodes for Array " << a << ", Section " << SectionOfInterest << "\n" << std::endl;
	}
	std::string shiftString = assigner.ArraySectionShiftsToString();
	LogFile << "==========\n";
	LogFile << "     Array/Section Shifts\n";
	LogFile << shiftString;
	LogFile << "==========\n";
	double aveScale;
	double xScale;
	double yScale;
	// 7.
	assigner.UpdateBlobList(BlobList);

	std::vector< Blob > recheckBlobs;
	recheckBlobs.clear();
	for (int b = 0; b < BlobList.size(); b++)
	{
		if (BlobList[b].Status == 0 && BlobList[b].Shape.FilterValue == 1)
			recheckBlobs.push_back(BlobList[b]);
	}
	if (recheckBlobs.size() > 0)
	{
		//LogFile << "Rechecking unassigned Blobs\n";
		assigner.AssignOtherBlobs(LogFile, recheckBlobs, Results, Results[3]);
		for (int b = 0; b < recheckBlobs.size() ; b++)
		{
			if (recheckBlobs[b].Status > 0)
			{
				BlobList[recheckBlobs[b].SelfIndex] = recheckBlobs[b];
			}
		}
	}
	// 8.
	//LogFile << "Assigning Other Blobs\n";
	assigner.AssignOtherBlobs(LogFile, OtherBlobList, Results, Results[3]);
	//LogFile << "Assigning Bad Blobs\n";
	assigner.AssignOtherBlobs(LogFile, BadBlobList, Results, Results[3]);

	//9.
	assigner.EstimateScale(LogFile, SectionOfInterest, aveScale, xScale, yScale);
	Results[3] = aveScale;
	//LogFile << "Before assigner.Clear()" << std::endl;
	assigner.Clear();
	LogFile << Results[0] << " / " << Results[1] << " / "<< Results[2] << " / "<< Results[3] << std::endl;
	int numbNotInSubImage = (int)BlobList.size() - numbInSubImage;
	LogFile << "Numb Blobs in subImage = " << numbInSubImage << "\n";
	LogFile << "Numb Blobs not in subImage = " << numbNotInSubImage << std::endl;
	
	return numbInSubImage;
}	
