//////////////////////////////////////////////////////////////////////////////////////////
//
// int CalcAdaptiveThreshold( std::ofstream &LogFile,
//		cv::Mat &Image, AnalysisParameters &FParams, int Set, std::string &BaseName, int &BlockSize, double MinimumPeak,
//		double &C, cv::Mat &Foreground8U, bool Track )
//
// Implements adaptive thersholding on Image.  The block size is fixed at BlockSize.  The one adjustable parameter is C.  C is adjusted
// to maximize the number of well like blobs in the image.  What constitutes well-like in this instance is defined by one of the
// parameter filter sets in FParams.   The number Set is used to select which set in FParams is used.
//
//     input
// LogFile - output stream for messages to logfile
// Image - image that adaptive thresholding is to be applied to
// FParams - structure which contains sets of parameters used to evaluate blobs to decide if blob is acceptable for quantitation.
//		each set contains minimum and maximum values of Blob Height, Blob Width, Blob Area, Blob Circularity and Blob Aspect ratio.
//		Currently there are four sets of these values in FParams, numbered 0 - 3.
// Set - The set in FParams to be used in counting blobs.
// BaseName - base name of any output files created by this procedure.
// BlockSize - Sets the size of square neighborhood used in adaptive thresholding (must be an odd number).  A pixel at the center
//		of this neighborhood is marked as above the threshold if that pixel's intensity is greater than the mean of the pixels
//		in the neighborhood minus C.  In this procedure BlockSize is fixed while C is varied.
// MinimumPeak - In addition to statisfying the shape requirements in FParams (selected by Set), the peak value within a region
//		must equal or exceed MinimumPeak in order to be counted as a fluorescent blob which might be a droplet.
// Track - not currently used 
//     output
// C - value of C which maximizes the number of well-like blobs in the image.
// Foreground8U - mask of blobs in the image.
//
//////////////////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "physicalconstants.h"
#include "statistics.hpp"
#include "extractBlobsAdaptive.hpp"
#include "analysisParameters.hpp"

#include "calcAdaptiveThreshold.hpp"

int CalcMaxV(std::vector< int > &CurrV)
{
	int maxV = 0;
	if ( CurrV[1] > CurrV[0] && CurrV[1] > CurrV[2] )
		maxV = 1;
	else if ( CurrV[2] > CurrV[0] && CurrV[2] > CurrV[1] )
		maxV = 2;
	else if ( CurrV[0] == CurrV[1] && CurrV[1] > CurrV[2] )
		maxV = 1;
	else if ( CurrV[0] < CurrV[1] && CurrV[1] == CurrV[2] )
		maxV = 1;
	return maxV;
}

double MedianMat(cv::Mat Input)
{    
	cv::Mat reshaped = Input.reshape(0,1); // spread Input Mat to single row
	std::vector<double> vecFromMat;
	reshaped.copyTo(vecFromMat); // Copy reshaped Mat to vector vecFromMat
	// move nth element to nth position in vector (in this case the median)
	std::nth_element(vecFromMat.begin(), vecFromMat.begin() + vecFromMat.size() / 2, vecFromMat.end()); 
	return vecFromMat[vecFromMat.size() / 2];
}

bool SortTrials( std::pair<double, int> A, std::pair<double, int> B )
{
	if( A.second > B.second )
		return true;
	else if ( A.second < B.second )
		return false;
	else
		return A.first < B.first;
}

double CalcAdaptiveThreshold( std::ofstream &LogFile,
	cv::Mat &Image, AnalysisParameters &FParams, int Set, std::string &BaseName, int &BlockSize, double MinimumPeak,
	double &C, cv::Mat &Foreground8U, bool Track )
{
	cv::Mat workImage1;
	cv::Mat workImage2;
	cv::Mat workImage3;
	workImage1 = Image.clone();
	int minContourLength = (int)(1.5 * ( FParams.ParameterSet[Set].BlobWidth.Min + FParams.ParameterSet[Set].BlobHeight.Min ));
	if ( BlockSize % 2 == 0 )
		BlockSize++;

	double med = MedianMat(workImage1);
	
	std::vector< double > curr(3);
	std::vector< int > currV(3);
	std::vector< double > init(5);
	std::vector< int > initV(5);
	// std::vector< double > numbBlobs((int)floor(MaxInten));
	std::vector< std::pair<double, int> > trials;
	trials.resize(3);
	double step;
	if ( med < 20.0 )
	{
		trials[0].first = -1.0;
		trials[1].first = 0.0;
		trials[2].first = 1.0;
		step = 1.17;
	}
	else
	{
		trials[0].first = -2.5;
		trials[1].first = 0;
		trials[2].first = 2.5;
		step = 2.31;
	}
	for ( int n = 0 ; n < 3 ; n++ )
	{
		trials[n].second = CountBlobsAdaptive(LogFile, trials[n].first, BlockSize, MinimumPeak, workImage1, 
			Foreground8U, workImage2, workImage3, minContourLength, 
			FParams, Set );
	}
	LogFile << "Iterating for best threshold\n";
	LogFile << " [0] " << trials[0].first << "/" << trials[0].second << "\n";
	LogFile << " [1] " << trials[1].first << "/" << trials[1].second << "\n";
	LogFile << " [2] " << trials[2].first << "/" << trials[2].second << "\n\n";
	std::sort( trials.begin(), trials.end(), SortTrials );
	int maxV = trials[0].second;
	int iter = 0;
	int itermax = 20;
	std::pair<double, int> newTrial;
	while ( step > 0.15 && iter < itermax )
	{
		newTrial.first = trials[0].first - step;
		newTrial.second = CountBlobsAdaptive(LogFile, newTrial.first, BlockSize, MinimumPeak, workImage1, 
			Foreground8U, workImage2, workImage3, minContourLength, 
			FParams, Set );
		trials.push_back(newTrial);
		newTrial.first = trials[0].first + step;
		newTrial.second = CountBlobsAdaptive(LogFile, newTrial.first, BlockSize, MinimumPeak, workImage1, 
			Foreground8U, workImage2, workImage3, minContourLength, 
			FParams, Set );
		trials.push_back(newTrial);
		std::sort(trials.begin(), trials.end(), SortTrials );
		if ( trials[0].second == maxV )
			step /= 1.91;
		else
		{
			step *= 0.91;
			maxV = trials[0].second;
		}
		iter++;
		LogFile << "iteration: " << iter << ", step = " << step << ", " << trials[0].first << "/" << trials[0].second << "\n";
	}

	C = trials[0].first;
	LogFile << "C = " << C << "\n";	
	return C;
}