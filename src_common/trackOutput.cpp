// Output procedures used to output extra images or csv which can be used for diagnostic purposes.
// Not all diagnostic output is handled through these files.
//
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "physicalconstants.h"
#include "blob.hpp"
#include "writeBlobFile.hpp"
#include "trackOutput.hpp"

void OutputImages003_004(cv::Mat &WorkImage1, cv::Mat &WorkImage2, cv::Mat &BackgroundSubImage32F, cv::Mat &Background32F, bool EightBit, std::string BaseName )
{
	WorkImage2 = cv::max(BackgroundSubImage32F, 0);
	if ( EightBit )
	{
		WorkImage2.convertTo(WorkImage1, CV_8U);
	}
	else
	{
		WorkImage2.convertTo(WorkImage1, CV_16U);
	}
	std::string outName = BaseName;
	outName.append("_004RB-SubIm.png");
	cv::imwrite( outName, WorkImage1);		
	
	WorkImage2 = cv::max(Background32F, 0);
	if ( EightBit )
	{
		WorkImage2.convertTo(WorkImage1, CV_8U);
	}
	else
	{
		WorkImage2.convertTo(WorkImage1, CV_16U);
	}
	outName = BaseName;
	outName.append("_003RB-Bck.png");
	cv::imwrite( outName, WorkImage1 );
	
	WorkImage1.release();
	WorkImage2.release();
}

void OutputImages008_014(cv::Mat &WorkImage3, std::vector<std::vector<cv::Point> > &TrialContours, cv::Size ImageSize,
	std::vector<Blob> &BlobList, std::vector<Blob> &OtherBlobList, std::vector<Blob> &BadBlobList, 
	cv::Mat &Foreground8U, std::string BaseName )
{
	std::string outName = BaseName;
	outName.append("_008FG-Msk.png"); 
	cv::imwrite( outName, Foreground8U );
	
	cv::Vec3b cValue;
	WorkImage3.create(ImageSize, CV_8UC3);
	cValue[0] = 0;
	cValue[1] = 0;
	cValue[2] = 0;
	WorkImage3.setTo(cValue);
	
	cValue[1] = 255;
	TrialContours.resize(BlobList.size());
	for ( int n = 0 ; n < BlobList.size() ; n++ )
		TrialContours[n] = BlobList[n].Shape.Contour;
	
	cv::drawContours(WorkImage3, TrialContours, -1, cValue, -1);
	
	cValue[1] = 128;
	cValue[2] = 128;
	TrialContours.resize(OtherBlobList.size());
	for ( int n = 0 ; n < OtherBlobList.size() ; n++ )
		TrialContours[n] = OtherBlobList[n].Shape.Contour;
	
	cv::drawContours(WorkImage3, TrialContours, -1, cValue, -1);
	
	cValue[1] = 0;
	cValue[2] = 255;
	TrialContours.resize(BadBlobList.size());
	for ( int n = 0 ; n < BadBlobList.size() ; n++ )
		TrialContours[n] = BadBlobList[n].Shape.Contour;
	
	cv::drawContours(WorkImage3, TrialContours, -1, cValue, -1);
	
	outName = BaseName;
	outName.append("_014Bl_Init.png");
	cv::imwrite(outName, WorkImage3);
	
	WorkImage3.release();
}

void OutputImages_020_021_015(std::ofstream &LogFile, cv::Mat &WorkImage2, cv::Mat &WorkImage3, cv::Mat &WorkImage4, int ContourLevel,
	std::vector<Blob> &BlobList, std::vector<Blob> &OtherBlobList, std::vector<std::vector<cv::Point> > &TrialContours, std::string BaseName )
{
	TrialContours.resize(BlobList.size());
	for ( int n = 0 ; n < BlobList.size() ; n++ )
	{
		TrialContours[n] = BlobList[n].Shape.Contour;
		cv::drawContours(WorkImage2, TrialContours, n, ContourLevel, 1);
		cv::drawContours(WorkImage4, TrialContours, n, ContourLevel, 1);
	}
	
	TrialContours.resize(OtherBlobList.size());
	for ( int n = 0 ; n < OtherBlobList.size() ; n++ )
	{
		TrialContours[n] = OtherBlobList[n].Shape.Contour;
		cv::drawContours(WorkImage3, TrialContours, n, ContourLevel, 1);
		cv::drawContours(WorkImage4, TrialContours, n, ContourLevel, 1);
	}

	std::string outName = BaseName;
	outName.append("_020B-Accptd.png");
	cv::imwrite( outName, WorkImage2 );
	
	outName = BaseName;
	outName.append("_021B-Rjctd.png");
	cv::imwrite( outName, WorkImage3 );
	
	outName = BaseName;
	outName.append("_015AllBlobs.png");
	cv::imwrite( outName, WorkImage4 );	
	
	TrialContours.clear();
}

void OutputCSV_017_022(std::vector<Blob> &BackgroundList, std::vector<Blob> &BadBlobList, cv::Point WellSpacing, std::string BaseName)
{
	std::ofstream outFile;
	for ( int n = 0 ; n < BackgroundList.size() ; n++ )
	{
		double maxBackground = 0.0;
		int maxBackgroundCount = 0;
		for ( int p = 0 ; p < BackgroundList[n].Shape.PixelIntensities.size() ; p++ )
		{
			if ( BackgroundList[n].Shape.PixelIntensities[p] > maxBackground )
				maxBackground = BackgroundList[n].Shape.PixelIntensities[p];
		}
		BackgroundList[n].Shape.Mom.m00 = maxBackground;
		maxBackground *= 0.90;
		for ( int p = 0 ; p < BackgroundList[n].Shape.PixelIntensities.size() ; p++ )
		{
			if ( BackgroundList[n].Shape.PixelIntensities[p] > maxBackground )
				maxBackgroundCount++;
		}
		BackgroundList[n].Shape.Circularity = maxBackgroundCount;		
	}
	
	std::string outName = BaseName; 
	outName.append("_017Backgnd.csv");
	std::string errorMsg;
	WriteBlobListInput(outName, BackgroundList, WellSpacing.y * 0.5, errorMsg );	
	BackgroundList.clear();
	
	outName = BaseName;
	outName.append("_022BadC.csv");
	outFile.open(outName.c_str());
	for ( int n = 0 ; n < BadBlobList.size() ; n++ )
	{
		outFile << n << "\n";
		for ( int m = 0 ; m < BadBlobList[n].Shape.Contour.size() ; m++)
		{
			outFile << "," << BadBlobList[n].Shape.Contour[m].x << "," << BadBlobList[n].Shape.Contour[m].y << "\n";
		}
	}
	outFile.close();
}

