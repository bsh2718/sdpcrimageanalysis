///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// int CalcCentroid(cv::Mat &Image32, std::vector< cv::Point> &Contour, cv::Mat &WorkImage, cv::Rect &BoundingRect, cv::Point2f &Center, 
//	std::vector< cv::Point > &PixelList, std::vector<double> &Intensities)
// Calculates geometric center of one of the blobs defined in Mask, using intensities 
// in Image (float). BoundingRect is the bounding box of the blob. Mask has the same size as Image.  The procedure calls 
// FindNonZeroPixels on Mask with BoundingRect to get a list of the nonzero pixels within Mask.  It uses that list
// to calculate the average position of the pixels (Center) from Image32.
// it also returns a list of the nonzero pixels in the Mask and their corresponding intensities in Image32.  
// The return value of the  procedure is the number of nonzero pixels in Mask that are also in BoundingRect.
//
//		Input
// Image32 - float image
// Contour - std::vector< cv::Point > containing pixels comprising the contour of the blob being analyzed. The contour
//		must be closed.
// WorkImage - work space
// BoundingRect - (cv::Rect) Bounding rectangle of blob for which the calculation is to be made.
//
//		Output
// Center - (cv::Point2f) Geometric center of blob
// PixelList - (std::vector< cv::Point >) vector of pixels in blob.
// Intensities - (std::vector< double >) vector of intensities in blob (Intensities[n] = Image.at<uchar>(PixelList[n]).
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// #include <iostream>
// #include <sstream>
// #include <fstream>
// #include <string>
// #include <cstdlib>
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "calccentroid.hpp"

int CalcCentroid(cv::Mat &Image32, std::vector< cv::Point> &Contour, cv::Mat &WorkImage, cv::Rect &BoundingRect, cv::Point2f &Center, 
	std::vector< cv::Point > &PixelList, std::vector<double> &Intensities)
{
	std::vector<cv::Point> idx;
	PixelList.clear();
	Intensities.clear();
	cv::Mat roi = WorkImage(BoundingRect);
	roi.setTo(0);
	std::vector< std::vector< cv::Point > > contourList;
	contourList.resize(1);
	contourList[0] = Contour;
	cv::drawContours(WorkImage, contourList, -1, 255, cv::FILLED);
	cv::findNonZero(roi, PixelList);
	int numb = (int)PixelList.size();
	Intensities.resize(numb);
	double sumX = 0.0;
	double sumY = 0.0;
	if ( numb > 0 )
	{
		for ( int n = 0 ; n < numb ; n++ )
		{
			PixelList[n].x += BoundingRect.x;
			PixelList[n].y += BoundingRect.y;
			Intensities[n] = Image32.at<float>(PixelList[n].y, PixelList[n].x);
			sumX += PixelList[n].x;
			sumY += PixelList[n].y;
		}
		Center.x = (float)(sumX / (double)numb);
		Center.y = (float)(sumY / (double)numb);
		return numb;
	}
	else
	{
		Center.x = 0.0;
		Center.y = 0.0;
		return 0;
	}
}
