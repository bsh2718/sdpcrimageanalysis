
#include <string>

#include "TrimDirectoryString.hpp"

const std::string DIRCHARREMOVE = " \n\r\t\f\v\\";
std::string trimDirectoryString(const std::string& s)
{
	size_t end = s.find_last_not_of(DIRCHARREMOVE);
	return (end == std::string::npos) ? "" : s.substr(0, end + 1);
}