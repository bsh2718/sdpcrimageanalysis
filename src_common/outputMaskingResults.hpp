#ifndef OUTPUTSIZINGRESULTS_HPP
#define OUTPUTSIZINGRESULTS_HPP

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "physicalconstants.h"
#include "statistics.hpp"
#include "blob.hpp"
#include "readinfofile.hpp"

class MaskingResults
{
	public:
		int Array;
		int Section;
		Statistics1 MaskFillingFrac;
		Statistics1 MaskBlobIntensity;
		Statistics1 BlobIntensity;
		
		MaskingResults(): Array(0), Section(0)
		{
			MaskFillingFrac.Clear();
			MaskBlobIntensity.Clear();
			BlobIntensity.Clear();
		}
		~MaskingResults(){}
		
		MaskingResults(const MaskingResults &Source): Array(Source.Array), Section(Source.Section)
		{
			MaskFillingFrac = Source.MaskFillingFrac;
			MaskBlobIntensity = Source.MaskBlobIntensity;
			BlobIntensity = Source.BlobIntensity;
		}
		
		MaskingResults& operator=(const MaskingResults &Source)
		{
			if ( this !=&Source )
			{
				Array = Source.Array;
				Section = Source.Section;
				MaskFillingFrac = Source.MaskFillingFrac;
				MaskBlobIntensity = Source.MaskBlobIntensity;
				BlobIntensity = Source.BlobIntensity;
			}
			return *this;
		}
		
		void Clear()
		{
			Array = 0;
			Section = 0;
			MaskFillingFrac.Clear();
			MaskBlobIntensity.Clear();
			BlobIntensity.Clear();
		}
};

void OutputMaskingResults(std::vector<ArraySection> ArrSecList, std::vector< BlobSummary > &InputBlobList, 
	std::vector< BlobSummary > &BlobList, std::string BaseName);

#endif  // 	OUTPUTSIZINGRESULTS_HPP