#ifndef BLOBCD_H
#define BLOBCD_H
//! [includes]
#include <vector>
#include <iostream>
#include <sstream>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//#include <iostream>
//#include <string>
//#include <cstdlib>
//! [includes]

#include "blob.hpp"
#include "statistics.hpp"

//! [namespace]
//using namespace std;
//using namespace cv;
//! [namespace]
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Blob_CD is used as a class for information about blobs in the image from a CD (radial columns)
//
class Blob_CD
{
public:
	int MainListIndex;					// Self Index for this blob in primary BlobList
	int SelfIndex;
	int Status;
	int FilterValue;
	cv::Point2f InputCenter2f;	// Input center after adjustment for image distortions (from primary BlobList)
	cv::Point2f InputTheta_R2f;
	cv::Point2f AdjustedTheta_R2f;
	cv::Point2f Theta_R2f;
	cv::Point DeviceCenter;
	bool OnImage;
	bool ThetaRCalculated;

	int Section;
	int SubSection;
	cv::Point SubSectionPosition;

	Blob_CD() : MainListIndex(-1), SelfIndex(-1), Status(0), FilterValue(0), InputCenter2f(cv::Point2f(0.0, 0.0)),
		InputTheta_R2f(cv::Point2f(0.0, 0.0)), AdjustedTheta_R2f(cv::Point2f(0.0, 0.0)),
		Theta_R2f(cv::Point2f(0.0, 0.0)), DeviceCenter(cv::Point(0, 0)), OnImage(false), ThetaRCalculated(false),
		Section(0), SubSection(0), SubSectionPosition(cv::Point(0,0))
	{}

	~Blob_CD() {}

	Blob_CD& operator= (const Blob_CD& Source)
	{
		if (this != &Source)
		{
			MainListIndex = Source.MainListIndex;
			SelfIndex = Source.SelfIndex;
			Status = Source.Status;
			FilterValue = Source.FilterValue;
			InputCenter2f = Source.InputCenter2f;
			InputTheta_R2f = Source.InputTheta_R2f;
			AdjustedTheta_R2f = Source.AdjustedTheta_R2f;
			Theta_R2f = Source.Theta_R2f;
			DeviceCenter = Source.DeviceCenter;
			OnImage = Source.OnImage;
			ThetaRCalculated = Source.ThetaRCalculated;
			Section = Source.Section;
			SubSection = Source.SubSection;
			SubSectionPosition = Source.SubSectionPosition;
		}
		return *this;
	}

	Blob_CD(const Blob_CD& Source) : MainListIndex(Source.MainListIndex), SelfIndex(Source.SelfIndex),
		Status(Source.Status), FilterValue(Source.FilterValue), InputCenter2f(Source.InputCenter2f), InputTheta_R2f(Source.InputTheta_R2f),
		AdjustedTheta_R2f(Source.AdjustedTheta_R2f), Theta_R2f(Source.Theta_R2f), DeviceCenter(Source.DeviceCenter),
		OnImage(Source.OnImage), ThetaRCalculated(Source.ThetaRCalculated),
		Section(Source.Section), SubSection(Source.SubSection), SubSectionPosition(Source.SubSectionPosition)
	{}

	Blob_CD(const Blob& Source, int Self) : MainListIndex(Source.SelfIndex), SelfIndex(Self),
		Status(Source.Status), FilterValue(Source.Shape.FilterValue), InputCenter2f(Source.InputCenter2f), InputTheta_R2f(cv::Point2f(0.0, 0.0)),
		AdjustedTheta_R2f(cv::Point2f(0.0, 0.0)), Theta_R2f(cv::Point2f(0.0, 0.0)), DeviceCenter(cv::Point(0, 0)),
		OnImage(false), ThetaRCalculated(false),
		Section(0), SubSection(0), SubSectionPosition(cv::Point(0, 0))
	{}

	void Clear()
	{
		MainListIndex = -1;
		SelfIndex = -1;
		Status = 0;
		FilterValue = 0;
		InputCenter2f = cv::Point2f(0.0, 0.0);
		InputTheta_R2f = cv::Point2f(0.0, 0.0);
		AdjustedTheta_R2f = cv::Point2f(0.0, 0.0);
		Theta_R2f = cv::Point2f(0.0, 0.0);
		DeviceCenter = cv::Point(0, 0);
		OnImage = false;
		ThetaRCalculated = false;
		Section = 0;
		SubSection = 0;
		SubSectionPosition = cv::Point(0,0);
	}

	bool ApplyRotationToThetaR(double CosAngle, double SinAngle, cv::Size ImageSize )
	{
		double newTheta = (InputTheta_R2f.x * CosAngle - InputTheta_R2f.y * SinAngle );
		double newR = (InputTheta_R2f.x * SinAngle + InputTheta_R2f.y * CosAngle );
		AdjustedTheta_R2f = cv::Point2f((float)newTheta, (float)newR);
		Theta_R2f = AdjustedTheta_R2f;
		DeviceCenter.x = (int)round(Theta_R2f.x);
		DeviceCenter.y = (int)round(Theta_R2f.y);
		if (DeviceCenter.x < 2 || DeviceCenter.x > ImageSize.width - 3
			|| DeviceCenter.y < 2 || DeviceCenter.y > ImageSize.height - 3 )
		{
			OnImage = false;
		}
		else
		{
			OnImage = true;
		}	
		return OnImage;
	}		

	bool CalculateInputThetaR(double RadialDistance, double ImageMiddle,
		double ThetaResolution, cv::Size DeviceImageSize)
	{
		double y = RadialDistance + InputCenter2f.y;
		double x = InputCenter2f.x - ImageMiddle;
		InputTheta_R2f.x = (float)((atan(x / y) / ThetaResolution) + DeviceImageSize.width * 0.5);
		InputTheta_R2f.y = (float)(sqrt(pow(x, 2) + pow(y, 2)) - RadialDistance);
		AdjustedTheta_R2f = InputTheta_R2f;
		Theta_R2f = InputTheta_R2f;
		DeviceCenter.x = (int)round(Theta_R2f.x);
		DeviceCenter.y = (int)round(Theta_R2f.y);
		if (DeviceCenter.x < 2 || DeviceCenter.x > DeviceImageSize.width - 3
			|| DeviceCenter.y < 2 || DeviceCenter.y > DeviceImageSize.height - 3)
		{
			OnImage = false;
		}
		else
		{
			OnImage = true;
		}
		return OnImage;
	}

	bool CalculateAdjustedThetaR(double RadialDistance, double ImageMiddle,
		double ThetaResolution, double ShiftMiddle, cv::Size DeviceImageSize)
	{
		double y = RadialDistance + InputCenter2f.y;
		double x = InputCenter2f.x - ImageMiddle - ShiftMiddle;
		AdjustedTheta_R2f.x = (float)((atan(x / y) / ThetaResolution) + DeviceImageSize.width * 0.5);
		AdjustedTheta_R2f.y = (float)(sqrt(pow(x, 2) + pow(y, 2)) - RadialDistance);
		Theta_R2f = AdjustedTheta_R2f;
		DeviceCenter.x = (int)round(Theta_R2f.x);
		DeviceCenter.y = (int)round(Theta_R2f.y);
		if (DeviceCenter.x < 2 || DeviceCenter.x > DeviceImageSize.width - 3
			|| DeviceCenter.y < 2 || DeviceCenter.y > DeviceImageSize.height - 3)
		{
			OnImage = false;
		}
		else
		{
			OnImage = true;
		}
		return OnImage;
	}
	bool RecalculateAdjustedThetaR(double RadialDistance, double ImageMiddle,
		double ThetaResolution, double ShiftMiddle, double RadialShift, cv::Size DeviceImageSize)
	{
		double y = RadialDistance + InputCenter2f.y + RadialShift;
		double x = InputCenter2f.x - ImageMiddle - ShiftMiddle;
		AdjustedTheta_R2f.x = (float)((atan(x / y) / ThetaResolution) + DeviceImageSize.width * 0.5);
		AdjustedTheta_R2f.y = (float)(sqrt(pow(x, 2) + pow(y, 2)) - RadialDistance);
		Theta_R2f = AdjustedTheta_R2f;
		DeviceCenter.x = (int)round(Theta_R2f.x);
		DeviceCenter.y = (int)round(Theta_R2f.y);
		if (DeviceCenter.x < 2 || DeviceCenter.x > DeviceImageSize.width - 3
			|| DeviceCenter.y < 2 || DeviceCenter.y > DeviceImageSize.height - 3)
		{
			OnImage = false;
		}
		else
		{
			OnImage = true;
		}
		return OnImage;
	}
	bool ResetAdjustedThetaR(cv::Size DeviceImageSize)
	{
		AdjustedTheta_R2f = InputTheta_R2f;
		Theta_R2f = InputTheta_R2f;
		DeviceCenter.x = (int)round(Theta_R2f.x);
		DeviceCenter.y = (int)round(Theta_R2f.y);
		if (DeviceCenter.x < 2 || DeviceCenter.x > DeviceImageSize.width - 3
			|| DeviceCenter.y < 2 || DeviceCenter.y > DeviceImageSize.height - 3)
		{
			OnImage = false;
		}
		else
		{
			OnImage = true;
		}
		return OnImage;
	}
	bool ShiftThetaR(cv::Point Offset, cv::Size DeviceImageSize)
	{
		Theta_R2f = cv::Point2f(AdjustedTheta_R2f.x + (float)Offset.x, AdjustedTheta_R2f.y + (float)Offset.y);
		DeviceCenter.x = (int)round(Theta_R2f.x);
		DeviceCenter.y = (int)round(Theta_R2f.y);
		if (DeviceCenter.x < 2 || DeviceCenter.x > DeviceImageSize.width - 3
			|| DeviceCenter.y < 2 || DeviceCenter.y > DeviceImageSize.height - 3)
		{
			OnImage = false;
		}
		else
		{
			OnImage = true;
		}
		return OnImage;
	}

	bool CalculateThetaR(double ThetaShift, double RShift, double Scale, cv::Size DeviceImageSize)
	{
		Theta_R2f.x = (float)((double)AdjustedTheta_R2f.x / Scale + ThetaShift);
		Theta_R2f.y = (float)((double)AdjustedTheta_R2f.y * Scale + RShift);
		DeviceCenter.x = (int)round(Theta_R2f.x);
		DeviceCenter.y = (int)round(Theta_R2f.y);
		if (DeviceCenter.x < 2 || DeviceCenter.x > DeviceImageSize.width - 3
			|| DeviceCenter.y < 2 || DeviceCenter.y > DeviceImageSize.height - 3)
		{
			OnImage = false;
		}
		else
		{
			OnImage = true;
		}
		return OnImage;
	}

	bool CalculateThetaR(double RadialDistance, double ImageMiddle,
		double ThetaResolution, std::vector<double> Params, cv::Size DeviceImageSize)
	{
		double y = RadialDistance + InputCenter2f.y * Params[3] + Params[1];
		double x = InputCenter2f.x * Params[3] - Params[2]- ImageMiddle;
		Theta_R2f.x = (float)((atan(x / y) / ThetaResolution) + DeviceImageSize.width * 0.5 + Params[0]);
		Theta_R2f.y = (float)(sqrt(pow(x, 2) + pow(y, 2)) - RadialDistance);
		DeviceCenter.x = (int)round(Theta_R2f.x);
		DeviceCenter.y = (int)round(Theta_R2f.y);
		if (DeviceCenter.x < 2 || DeviceCenter.x > DeviceImageSize.width - 3
			|| DeviceCenter.y < 2 || DeviceCenter.y > DeviceImageSize.height - 3)
		{
			OnImage = false;
		}
		else
		{
			OnImage = true;
		}
		return OnImage;
	}

	std::string ToString()
	{
		std::ostringstream oString;
		oString.str("");
		oString.clear();
		oString << MainListIndex << "," << SelfIndex << "," << Status << "," << FilterValue << ","
			<< InputCenter2f.y << "," << InputCenter2f.x << ","
			<< InputTheta_R2f.y << "," << InputTheta_R2f.x << ","
			<< AdjustedTheta_R2f.y << "," << AdjustedTheta_R2f.x << ","
			<< Theta_R2f.y << "," << Theta_R2f.x << ","
			<< DeviceCenter.y << "," << DeviceCenter.x << ","
			<< OnImage << "," << ThetaRCalculated << ","
			<< Section << "," << SubSection << ","
			<< SubSectionPosition.x << "," << SubSectionPosition.y;
		return oString.str();
	}

	std::string Header()
	{
		return "MainIndex,SelfIndex,Status,FilterValue,InpCtr2f.y,InpCtr2f.x,InpT_R2f.y,InpT_R2f.x,AdjT_R2f.x,AdjT_R2f.y,T_R2f.y,T_R2f.x,DevCtr.y,DevCtr.x,OnImg,TRCalc,Sec,SubSec,SubSe.x,SubSec.y";
	}
};

#endif // BLOBCD_H

