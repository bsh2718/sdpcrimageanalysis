#ifndef BEFOREAFTERMATRIX_HPP
#define BEFOREAFTERMATRIX_HPP
//! [includes]

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "physicalconstants.h"

//! [includes]

//! [namespace]
// using namespace std;
//using namespace cv;
//! [namespace]

class BeforeAfterMatrix
{
	public:
		int BeforeIndex;	// AlignImage
		int AfterIndex;		// AlignImage
		int Occupied;		// CalcAfterBeforeRatioDistribution
		bool QuantStatus;		// OutputDropletComparison
		int BeforeEdgeStatus;	// AssignStatus
		int AfterEdgeStatus;	// AssignStatus
		bool EdgeExcluded;		// AssignStatus
		int RunLength;			// CalcAfterBeforeRatioDistribution
		double RunExpectancy;	// CalcAfterBeforeRatioDistribution
		bool RunExcluded;		// CalcAfterBeforeRatioDistribution
		double RelativeAveInten;	// CalcBeforeAveIntenDistribution
		int RelativeAveIntenStatus;	// CalcBeforeAveIntenDistribution
		double Shrinkage;		// CalcAfterBeforeComparisons
		int ShrinkageStatus;	// CalcAfterBeforeComparisons
		double AfterBeforeRatio;	// CalcAfterBeforeComparisons
		bool BeforeAnomalousFilling;	// CalcAfterBeforeComparisons
		bool AfterAnomalousFilling;		// CalcAfterBeforeComparisons
		bool Mismatched;		// ??
		std::string Description;
		
		BeforeAfterMatrix(): BeforeIndex(-1), AfterIndex(-1),
			Occupied(0), QuantStatus(false), BeforeEdgeStatus(0), AfterEdgeStatus(0), EdgeExcluded(false), 
			RunLength(0), RunExpectancy(1.0), RunExcluded(false), RelativeAveInten(1.0), RelativeAveIntenStatus(1),
			Shrinkage(1.0), ShrinkageStatus(1), AfterBeforeRatio(-1.0), BeforeAnomalousFilling(false), AfterAnomalousFilling(false),
			Mismatched(false)
		{ Description.clear(); }
		~BeforeAfterMatrix(){}
		
		BeforeAfterMatrix(const BeforeAfterMatrix &Source): BeforeIndex(Source.BeforeIndex),
			AfterIndex(Source.AfterIndex), Occupied(Source.Occupied),
			QuantStatus(Source.QuantStatus), BeforeEdgeStatus(Source.BeforeEdgeStatus),
			AfterEdgeStatus(Source.AfterEdgeStatus),EdgeExcluded(Source.EdgeExcluded), 
			RunLength(Source.RunLength), RunExpectancy(Source.RunExpectancy), RunExcluded(Source.RunExcluded), RelativeAveInten(Source.RelativeAveInten),
			RelativeAveIntenStatus(Source.RelativeAveIntenStatus),
			Shrinkage(Source.Shrinkage), ShrinkageStatus(Source.ShrinkageStatus),
			AfterBeforeRatio(Source.AfterBeforeRatio), BeforeAnomalousFilling(Source.BeforeAnomalousFilling), AfterAnomalousFilling(Source.AfterAnomalousFilling),
			Mismatched(Source.Mismatched), Description(Source.Description) {}
		
		BeforeAfterMatrix& operator=(const BeforeAfterMatrix &Source)
		{
			if ( this != &Source )
			{
				BeforeIndex = Source.BeforeIndex;
				AfterIndex = Source.AfterIndex;
				Occupied = Source.Occupied;
				QuantStatus = Source.QuantStatus;
				BeforeEdgeStatus = Source.BeforeEdgeStatus;
				AfterEdgeStatus = Source.AfterEdgeStatus;
				EdgeExcluded = Source.EdgeExcluded;
				RunLength = Source.RunLength;
				RunExpectancy = Source.RunExpectancy;
				RunExcluded = Source.RunExcluded;
				RelativeAveInten = Source.RelativeAveInten;
				RelativeAveIntenStatus = Source.RelativeAveIntenStatus;
				Shrinkage = Source.Shrinkage;
				ShrinkageStatus = Source.ShrinkageStatus;
				AfterBeforeRatio = Source.AfterBeforeRatio;
				BeforeAnomalousFilling = Source.BeforeAnomalousFilling;
				AfterAnomalousFilling = Source.AfterAnomalousFilling;
				Mismatched = Source.Mismatched;
				Description = Source.Description;
			}
			return *this;
		}
		
		void Clear()
		{
			BeforeIndex = -1;
			AfterIndex = -1;
			Occupied = 0;
			QuantStatus = false;
			BeforeEdgeStatus = 0;
			AfterEdgeStatus = 0;
			EdgeExcluded = false;
			RunLength = 0;
			RunExpectancy = 1.0;
			RunExcluded = false;
			RelativeAveInten = 1.0;
			RelativeAveIntenStatus = 1;
			Shrinkage = 1.0;
			ShrinkageStatus = 1;
			AfterBeforeRatio = -1.0;
			BeforeAnomalousFilling = false;
			AfterAnomalousFilling = false;
			Mismatched = false;
			Description.clear();
		}
		
		void ClearAfter()
		{
			AfterIndex = -1;
			Occupied = 0;
			QuantStatus = false;
			AfterEdgeStatus = 0;
			EdgeExcluded = false;
			RunLength = 0;
			RunExpectancy = 1.0;
			RunExcluded = false;
			RelativeAveInten = 1.0;
			RelativeAveIntenStatus = 1;
			Shrinkage = 1.0;
			ShrinkageStatus = 1;
			AfterBeforeRatio = -1.0;
			AfterAnomalousFilling = false;
			Mismatched = false;
			Description.clear();
		}
};

#endif // BEFOREAFTERMATRIX_HPP