#pragma once
//! [includes]

#include <cstdlib>
#include <vector>
#include <algorithm>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "physicalconstants.h"
#include "blob.hpp"
#include "trimRectangle.hpp"
#include "deviceOrigins.hpp"

bool AssignBlobsCD2(std::ofstream& LogFile, std::string BaseName,
	std::vector< double > Results, cv::Size ImageSize, cv::Mat& CoordsImage,
	std::vector< Blob >& BlobList, std::vector< Blob >& OtherBlobList, std::vector< Blob >& BadBlobList,
	bool TrackAssignment, std::string& ErrMsg);




	
