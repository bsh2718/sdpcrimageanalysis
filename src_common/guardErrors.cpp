///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// void SetupGuardErrorLists( std::vector< GuardInfo > &XGuards, std::vector< GuardInfo > &YGuards, cv::Size ImageSize,
//     std::vector< GuardErrors > &XGuardErrorList, std::vector< GuardErrors > &YGuardErrorList, std::vector< GuardErrors > &OffImageList )
// 
// Takes the information in XGuards, YGuards (obtained from ReadInfoFile) and the size of the image to setup the
// vectors XGuardErrorList, YGuardErrorList and OffImageList.  For each guard, it sets PrevIsEdge = true, if the area
// before the guard is off the image and NextIsEdge = true if the area after the guard is off the image.  It also sets 
// the minimum and maximum pixel values for the guard region.  
// For XGuardErrorList, the areas are arrays. Before is to the left of the guard and after is to the right
// For YGuardErrorList, the areas are sections. Before is above the guard and after is below
// For each edge of the image, the GuardErrorList is checked to see if one of the guards is adjacent to the edge. Index
// that case there can be no wells between the guard and the edge of the image.  The location of the corresponding edge
// in OffImageList is set to the location of the edge of the guard which is farthest into the image and the other location
// is set to a point arbitrarily far off the image.
//
// 		Input
// XGuards - vector of GuardInfo for X Guards (the guard areas between arrays)
// YGuards - vector of GuardInfo for Y Guards (the guard areas beetween sections)
// ImageSize - cv::Size of image
// 		Output
// XGuardErrorList - vector of GuardErrors for XGuards.  Each element contains the lower and upper bounds for the guard
// 	and whether it is the first or last guard in that direction.  (For X Guards the first guard is the far left 
// 	guard and the last is the far right guard.  For Y Guards they are the top and bottom guards.).  Each element
// 	also contains four vectors.  Each element of those vectors refers to a single well-like blob which falls into
// 	that guard region.  The four vectors are
// 		Location - Location of the blob centroid after alignment.
// 		OrigLocation - Location of the blob centroid in the original image.
// 		BlobIndex - Index number in BlobList
// 		Error - not currently used.
// YGuardErrorList - vector of GuardErrors for YGuards.
// OffImageList - vector of GuardErrors for blobs which are not in the image.
//
// class GuardErrors 
// class to store guard errors in image alignment.  There is one these for each guard in X and each guard in Y.
// class is described in guardErrors.hpp
//
// int NumbErrorsInList(std::vector< GuardErrors > &List )	
// Returns the total number of errors in vector of < GuardErrors >.  
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "statistics.hpp"
#include "readinfofile.hpp"
#include "calccentroid.hpp"
#include "guardErrors.hpp"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//
int NumbErrorsInList(std::vector< GuardErrors > &List )	
{
	int sum = 0;
	for ( int n = 1 ; n < (int)List.size() ; n++ )
		sum += (int)List[n].Location.size();
	
	return sum;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
void SetupGuardErrorLists( std::vector< GuardInfo > &XGuards, std::vector< GuardInfo > &YGuards, cv::Size ImageSize,
	std::vector< GuardErrors > &XGuardErrorList, std::vector< GuardErrors > &YGuardErrorList, std::vector< GuardErrors > &OffImageList )
{
	int numbXGuards = (int)XGuards.size();
	int numbYGuards = (int)YGuards.size();
	XGuardErrorList.resize(numbXGuards+1);
	YGuardErrorList.resize(numbYGuards+1);
	OffImageList.resize(OFFIMAGELISTSIZE);

	for ( int n = 0 ; n < numbXGuards ; n++ )
	{
		int m = XGuards[n].Index;
		XGuardErrorList[m].LowerBound = XGuards[n].Location - XGuards[n].Width/2;
		XGuardErrorList[m].UpperBound = XGuards[n].Location + XGuards[n].Width/2;
		if ( XGuards[n].PrevArea < 0 )
		{
			XGuardErrorList[m].PrevIsEdge = true;
		}
		else
		{
			XGuardErrorList[m].PrevIsEdge = false;
		}
		if ( XGuards[n].NextArea < 0 )
		{
			XGuardErrorList[m].NextIsEdge = true;
		}
		else
		{
			XGuardErrorList[m].NextIsEdge = false;
		}
		XGuardErrorList[m].Location.clear();
	}
	
	if ( XGuardErrorList[1].PrevIsEdge )
	{
		OffImageList[OFF_LEFT].PrevIsEdge = true;
		OffImageList[OFF_LEFT].LowerBound = -1.0e+37;
		if ( XGuardErrorList[1].LowerBound < 0.0 )
			OffImageList[OFF_LEFT].UpperBound = XGuardErrorList[1].LowerBound;
		else
			OffImageList[OFF_LEFT].UpperBound = 0.0;
	}
	else
	{
		OffImageList[OFF_LEFT].PrevIsEdge = false;
		OffImageList[OFF_LEFT].LowerBound = -1.0e+37;
		OffImageList[OFF_LEFT].UpperBound = -1.0e+37;
	}
	OffImageList[OFF_LEFT].NextIsEdge = false;
	OffImageList[OFF_LEFT].Location.clear();
		
	if ( XGuardErrorList[numbXGuards].NextIsEdge )
	{
		OffImageList[OFF_RIGHT].NextIsEdge = true;
		OffImageList[OFF_RIGHT].UpperBound = 1.0e+37;
		if ( XGuardErrorList[numbXGuards].UpperBound > ImageSize.width )
			OffImageList[OFF_RIGHT].UpperBound = XGuardErrorList[numbXGuards].UpperBound;
		else
			OffImageList[OFF_RIGHT].UpperBound = ImageSize.width;
	}
	else
	{
		OffImageList[OFF_RIGHT].NextIsEdge = false;
		OffImageList[OFF_RIGHT].LowerBound = +1.0e+37;
		OffImageList[OFF_RIGHT].UpperBound = +1.0e+37;
	}
	OffImageList[OFF_RIGHT].PrevIsEdge = false;
	OffImageList[OFF_RIGHT].Location.clear();

	
	for ( int n = 0 ; n < numbYGuards ; n++ )
	{
		int m = YGuards[n].Index;
		YGuardErrorList[m].LowerBound = YGuards[n].Location - YGuards[n].Width/2;
		YGuardErrorList[m].UpperBound = YGuards[n].Location + YGuards[n].Width/2;
		if ( YGuards[n].PrevArea < 0 )
		{
			YGuardErrorList[m].PrevIsEdge = true;
		}
		else
		{
			YGuardErrorList[m].PrevIsEdge = false;
		}
		if ( YGuards[n].NextArea < 0 )
		{
			YGuardErrorList[m].NextIsEdge = true;
		}
		else
		{
			YGuardErrorList[m].NextIsEdge = false;
		}
		YGuardErrorList[m].Location.clear();
	}
	// lastGuard = YGuardErrorList.size();
	if ( YGuardErrorList[1].PrevIsEdge )
	{
		OffImageList[OFF_TOP].PrevIsEdge = true;
		OffImageList[OFF_TOP].LowerBound = -1.0e+37;
		if ( YGuardErrorList[1].LowerBound < 0.0 )
			OffImageList[OFF_TOP].UpperBound = YGuardErrorList[1].LowerBound;
		else
			OffImageList[OFF_TOP].UpperBound = 0.0;
	}
	else
	{
		OffImageList[OFF_TOP].PrevIsEdge = false;
		OffImageList[OFF_TOP].LowerBound = -1.0e+37;
		OffImageList[OFF_TOP].UpperBound = -1.0e+37;
	}
	OffImageList[OFF_TOP].NextIsEdge = false;
	OffImageList[OFF_TOP].Location.clear();
		
	if ( YGuardErrorList[numbYGuards].NextIsEdge )
	{
		OffImageList[OFF_BOTTOM].NextIsEdge = true;
		OffImageList[OFF_BOTTOM].UpperBound = 1.0e+37;
		if ( YGuardErrorList[numbYGuards].UpperBound > ImageSize.height )
			OffImageList[OFF_BOTTOM].UpperBound = YGuardErrorList[numbYGuards].UpperBound;
		else
			OffImageList[OFF_BOTTOM].UpperBound = ImageSize.height;
	}
	else
	{
		OffImageList[OFF_BOTTOM].NextIsEdge = false;
		OffImageList[OFF_BOTTOM].LowerBound = +1.0e+37;
		OffImageList[OFF_BOTTOM].UpperBound = +1.0e+37;
	}
	OffImageList[OFF_BOTTOM].PrevIsEdge = false;
	OffImageList[OFF_BOTTOM].Location.clear();
}
