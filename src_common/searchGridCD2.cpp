#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "statistics.hpp"
#include "blob.hpp"
#include "searchVolumeCD2.hpp"
#include "readinfofile.hpp"
#include "assignBlobsCD.hpp"
#include "trimRectangle.hpp"
#include "searchGridCD2.hpp"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//int AlignImageCD(std::ofstream& LogFile, std::string& BaseName,
//	std::vector< Blob >& BlobList, std::vector< Blob >& OtherBlobList, std::vector< Blob >& BadBlobList,
//	cv::Mat& SectionImage, double DistToCenter, double ThetaResolution,
//	std::vector< ParameterLimit > PLimits,
//	cv::Size ImageSize, std::vector<cv::Point2f> WellSpacing, cv::Size2f WellSize, int SectionOfInterest,
//	std::vector< DeviceOrigins >& SectionOrigins,
//	std::vector< DeviceOrigins >& SubSectionOrigins, std::vector< cv::Size > SubSectionDimens,
//	std::vector< double >& Results, bool TrackAlignment, std::string& ErrMsg)
//
// Aligns image with the radial grid for CD type device 
// 
//			Input
// LogFile - ofstream connected to log filebuf
// BaseName - project name
// BlobList - vector of Blob. Includes well-like blobs which pass criteria to be used for quantitation.
// OtherBlobList - vector of Blob. Includes well-like blobs which do not pass criteria to be used for quantitation,
//		but appear to be droplets in wells.
// BadBlobList - vector of Blob.  Includes blobs which may not actually be droplets
// SectionImage - 8 bit gray scale image.  For each Section/Subsection pair in the device, a region is outlined using the
//		outside edges of the wells in each column of wells.  The outlines are the smallest regions each of which encloses
//		all of the wells of a single column in a Section/Subsection.  The interior of each of these regions is non zero.  Pixels outside
//		of this region are set to zero.  The value of the interiors indicates which Section/Subsection the column of wells
//      is a part of.
// DistToCenter - Distance (in pixels) from the top center edge of the image to the center of the CD.  This assumes that 
//      the alignment of the camera over the CD is correct.  The program has the ability to make small adjustments if
//      this distance is slightly off.
// ThetaResolution - The rows and column coordinates (y, x) of the center of each droplet are converted into radial 
//      coordinates (R, Theta).  R is in pixels with the same microns/pixel as the original image. Theta is in radians
//      with ThetaResolution radians/pixel.  R and Theta are expressed as y and x in the coordinates used to align the
//      droplets to the radial grid.
// PLimits - 
// ImageSize - size of the 
// WellSpacing - center to center distance of wells in pixels (R, Theta).  This is a vector because each subsection of
//       the device can have a different Theta spacing.
// WellSize - size of wells in pixels (in the original image) 
// SectionOfInterest - this is the one section in the image which is to be analyzed.  Droplets in wells which
//       are part of a different section are ignored.
// SectionOrigins - Coordinates in (R, Theta) of the upper left corner of each section. Only Theta is used.
// SubSectionOrigins - Coordinates in (R, Theta) of upper left corner of each section. Only R is used.
// SubSectionDimens - Number of rows and columns in a subsection.  This is the same for a given subsection regardless
//      of which section it is a part of.
// Results  - (X-Offset_Final, Y-Offset_Final, Angle, Scale_Final)
// TrackAlignment - Additional information is output if this is true
// ErrMsg - 

bool SearchGrid::Initialize(std::ofstream& LogFile, std::string BaseName, std::vector<Blob>& BlobList,
	cv::Size ImageSize, bool TrackAlignment, std::string& ErrMsg)
{
	std::ostringstream oString;
	ErrMsg = "";
	Initialized = false;
	if (SearchVolume::XYSearchLimits.x < 0.5 || SearchVolume::XYSearchLimits.y < 0.5)
	{
		oString.str("");
		oString.clear();
		oString << "Search Grid Initialization error in XYSearchLimits: " << SearchVolume::XYSearchLimits;
		ErrMsg = oString.str();
		return false;
	}
	GridSpacing.x = (int)round(2.0 * SearchVolume::XYSearchLimits.x);
	GridSpacing.y = (int)round(2.0 * SearchVolume::XYSearchLimits.y);
	if (GridSpacing.x < 0.5 || GridSpacing.y < 0.5)
	{
		oString.str("");
		oString.clear();
		oString << "Search Grid Initialization error in GridSpacing: " << GridSpacing;
		ErrMsg = oString.str();
		return false;
	}
	GridCenter.x = (int)ceil(PLimits[0].Limit / GridSpacing.x);
	GridCenter.y = (int)ceil(PLimits[1].Limit / GridSpacing.y);
	GridSize.width = GridCenter.x * 2 + 1;
	GridSize.height = GridCenter.y * 2 + 1;
	if (GridSize.width < 6 || GridSize.height < 6)
	{
		oString.str("");
		oString.clear();
		oString << "Search Grid Initialization error in GridSize: " << GridSize;
		ErrMsg = oString.str();
		return false;
	}

	XYGrid.resize(GridSize.height);
	for (int n = 0; n < XYGrid.size(); n++)
		XYGrid[n].resize(GridSize.width);
	ParameterBounds thetaBounds = ParameterBounds(-PLimits[2].Limit, PLimits[2].Limit, 21);
	ParameterBounds scaleBounds = ParameterBounds(1.0 - PLimits[3].Limit, 1.0 + PLimits[3].Limit, 11);
	LogFile << "XYGridSize (r,c) = " << XYGrid.size() << ", " << XYGrid[0].size() << std::endl;
	LogFile << "GridSize.h/w: " << GridSize.height << "/" << GridSize.width << std::endl;
	for (int gRow = 0; gRow < GridSize.height; gRow++)
	{
		for (int gCol = 0; gCol < GridSize.width; gCol++)
		{
			XYGrid[gRow][gCol].Load(cv::Point((gCol - GridCenter.x) * GridSpacing.x, (gRow - GridCenter.y) * GridSpacing.y), cv::Point(gCol, gRow), PLimits[2].BaseValue, PLimits[3].BaseValue,
				thetaBounds, scaleBounds);
		}
	}
	SearchVolume::SearchMatrix.create(GridSize, CV_8UC1);
	SearchVolume::SearchMatrix.setTo(0);
	SearchVolume::BlobImage.create(SearchVolume::WellTemplateSize, CV_8UC1);
	SearchVolume::BlobImage.setTo(0);
	cv::Point imageCenter;
	imageCenter.x = ImageSize.width / 2;
	imageCenter.y = ImageSize.height / 2;
	for (int b = 0; b < BlobList.size(); b++)
	{
		cv::Point pt;
		pt.x = (int)round(BlobList[b].DeviceCenter.x + SearchVolume::WellTemplateCenter.x) - imageCenter.x;
		pt.y = (int)round(BlobList[b].DeviceCenter.y + SearchVolume::WellTemplateCenter.y) - imageCenter.y;
		if (pt.x >= 0 && pt.x < SearchVolume::WellTemplateSize.width
			&& pt.y >= 0 && pt.y < SearchVolume::WellTemplateSize.height)
		{
			SearchVolume::BlobImage.at<uchar>(pt) = 200;
		}
	}
	LogFile << std::endl;
	Initialized = true;
	//std::cout << "Finished initializing SearchGrid" << std::endl;
	return true;
}
bool SearchGrid::AdjustBounds(std::vector< double >& Results, ParameterBounds& Bounds)
{
	bool llimitOK = true;
	double step = (Bounds.UpperBound - Bounds.LowerBound) / ((double)Bounds.NumbSteps - 1.0);
	double min = Results[0];
	double max = Results[0];
	for (int t = 1; t < 9; t++)
	{
		if (Results[t] > max)
			max = Results[t];
		else if (Results[t] < min)
			min = Results[t];
	}
	if (min > Bounds.LowerBound + step * 3.5)
	{
		min += 1.5 * step;
	}
	else if (min > Bounds.LowerBound + step * 2.5)
	{
		min += step;
	}
	else if (min > Bounds.LowerBound + step * 1.5)
	{
		min += 0.5 * step;
	}
	else
	{
		min = Bounds.LowerBound - 2.0 * step;
		llimitOK = false;
	}
	if (max < Bounds.UpperBound - step * 3.5)
	{
		max -= 1.5 * step;
	}
	else if (max < Bounds.UpperBound - step * 2.5)
	{
		max -= step;
	}
	else if (max < Bounds.UpperBound - step * 1.5)
	{
		max -= 0.5 * step;
	}
	else
	{
		max = Bounds.UpperBound + 2.0 * step;
		llimitOK = false;
	}
	if (Bounds.NumbSteps > 6 && (max - min) / step < (double)(Bounds.NumbSteps - 2) / (double)Bounds.NumbSteps)
		Bounds.NumbSteps -= 2;
	Bounds.UpperBound = max;
	Bounds.LowerBound = min;
	return llimitOK;
}

bool SortLocalScoreLow(LocalScore A, LocalScore B)
{
	return A.Score < B.Score;
}

bool SearchGrid::GridSearchGuard(std::ofstream& LogFile, std::string BaseName,
	ParameterBounds& ThetaBounds, ParameterBounds& ScaleBounds,
	LocalScore &BestGuardScore, std::vector<LocalScore>& GuardResults, bool TrackAlignment)
{
	if (!Initialized)
		return false;
	std::ostringstream oString;
	LocalScore thetaScore;
	LocalScore scaleScore;
	LocalScore bestScore = LocalScore();
	bestScore.Score = 100000;
	cv::Point limits = SearchVolume::XYSearchLimits;
	int xLim = (int)round(((double)limits.x * 0.667));
	int yLim = (int)round(((double)limits.y * 0.667));
	std::vector< cv::Point > offsetList(5);
	offsetList[0] = cv::Point(0, 0);
	offsetList[1] = cv::Point(-xLim, 0);
	offsetList[2] = cv::Point(xLim, 0);
	offsetList[3] = cv::Point(0, -yLim);
	offsetList[4] = cv::Point(0, yLim);
	
	for (int r = 0; r < XYGrid.size(); r++)
	{
		for (int c = 0; c < XYGrid[r].size(); c++)
		{
			for (int o = 0; o < 5; o++)
			{
				thetaScore = XYGrid[r][c].GuardThetaSearch(LogFile, BaseName, offsetList[o],
					ThetaBounds, ScaleBounds, TrackAlignment);

				scaleScore = XYGrid[r][c].GuardScaleSearch(LogFile, BaseName, offsetList[o],
					thetaScore, TrackAlignment);

				if (scaleScore.Score < bestScore.Score)
					bestScore = scaleScore;
				GuardResults.push_back(scaleScore);
			}
		}
	}
	BestGuardScore = bestScore;
	return true;
}

bool SearchGrid::GridSearch(std::ofstream& LogFile, std::string BaseName,
	ParameterBounds& ThBounds, ParameterBounds& ScBounds, int GuardPenalty,
	std::vector<LocalScore> GuardResults, SearchVolumeResult& BestSearchResult, bool TrackAlignment)
{
	if (!Initialized)
		return false;
	std::string sep = ", ";
	std::ostringstream oString;
	GuardPenalty = -abs(GuardPenalty);
	std::sort(GuardResults.begin(), GuardResults.end(), SortLocalScoreLow);
	int cutoff = (GuardResults[0].Score * 3) / 2;
	int lastG = 0;
	for (int g = 1; g < GuardResults.size(); g++)
	{
		if (GuardResults[g].Score > cutoff)
			break;
		lastG = g;
	}
	if (TrackAlignment)
	{
		LogFile << "\nGridSearch\n";
		for (int g = 0; g <= lastG; g++)
			LogFile << GuardResults[g].ToString() << "\n";
		LogFile << "\n";
	}
	ParameterBounds thetaBounds;
	double deltaT = (ThBounds.UpperBound - ThBounds.LowerBound) * 0.2;
	int stepsT = (ThBounds.NumbSteps - 1) / 2 + 1;
	if (stepsT % 2 == 0)
		stepsT++;
	ParameterBounds scaleBounds;
	double deltaS = (ScBounds.UpperBound - ScBounds.LowerBound) * 0.2;
	int stepsS = (ScBounds.NumbSteps - 1) / 2 + 1;
	if (stepsS % 2 == 0)
		stepsS++;
	SearchVolumeResult thetaResult;
	SearchVolumeResult scaleResult;
	SearchVolumeResult bestResult = SearchVolumeResult();
	bestResult.Score = -100000;
	LocalScore thetaScore;
	std::vector< SearchVolumeResult> templateResults;
	templateResults.clear();
	cv::Point offset;
	std::vector <std::vector <int > > resultArray;
	for ( int g = 0 ; g <= lastG ; g++)
	{
		double lowerBound = GuardResults[g].Theta - deltaT;
		double upperBound = GuardResults[g].Theta + deltaT;
		if (lowerBound < ThBounds.LowerBound)
		{
			lowerBound = ThBounds.LowerBound;
			if (upperBound - lowerBound < 3.0 * ThBounds.StepSize)
			{
				upperBound = lowerBound + 3.0 * ThBounds.StepSize;
				thetaBounds = ParameterBounds(lowerBound, upperBound, 5);
			}
			else
			{
				thetaBounds = ParameterBounds(lowerBound, upperBound, stepsT);
			}
		}
		else if (upperBound > ThBounds.UpperBound)
		{
			upperBound = ThBounds.UpperBound;
			if (upperBound - lowerBound < 3.0 * ThBounds.StepSize)
			{
				lowerBound = upperBound - 3.0 * ThBounds.StepSize;
				thetaBounds = ParameterBounds(lowerBound, upperBound, 5);
			}
			else
			{
				thetaBounds = ParameterBounds(lowerBound, upperBound, stepsT);
			}
		}
		else
			thetaBounds = ParameterBounds(lowerBound, upperBound, stepsT);

		lowerBound = GuardResults[g].Scale - deltaS;
		upperBound = GuardResults[g].Scale + deltaS;
		if (lowerBound < ScBounds.LowerBound)
		{
			lowerBound = ScBounds.LowerBound;
			if (upperBound - lowerBound < 3.0 * ScBounds.StepSize)
			{
				upperBound = lowerBound + 3.0 * ScBounds.StepSize;
				scaleBounds = ParameterBounds(lowerBound, upperBound, 5);
			}
			else
			{
				scaleBounds = ParameterBounds(lowerBound, upperBound, stepsS);
			}
		}
		else if (upperBound > ScBounds.UpperBound)
		{
			upperBound = ScBounds.UpperBound;
			if (upperBound - lowerBound < 3.0 * ScBounds.StepSize)
			{
				lowerBound = upperBound - 3.0 * ScBounds.StepSize;
				scaleBounds = ParameterBounds(lowerBound, upperBound, 5);
			}
			else
			{
				scaleBounds = ParameterBounds(lowerBound, upperBound, stepsS);
			}
		}
		else
			scaleBounds = ParameterBounds(lowerBound, upperBound, stepsS);

		LogFile << "GridSearch adjusted Bounds\n";
		LogFile << "ThetaBounds: " << thetaBounds.LowerBound << ", " << thetaBounds.UpperBound << ", " << thetaBounds.NumbSteps << ", " << thetaBounds.StepSize << "\n";
		LogFile << "ScaleBounds: " << scaleBounds.LowerBound << ", " << scaleBounds.UpperBound << ", " << scaleBounds.NumbSteps << ", " << scaleBounds.StepSize << "\n";

		int r = GuardResults[g].GridLocation.y;
		int c = GuardResults[g].GridLocation.x;
		LogFile << "GuardResults[" << g << "].GridLocation: " << GuardResults[g].GridLocation << ", r/c " << r << "/" << c << "\n";
		LogFile << "     GuardResults[" << g << "].GlobalXYLocation: " << XYGrid[r][c].GlobalXYLocation << std::endl;
		int xL = SearchVolume::XYSearchLimits.x;
		int xStep = SearchVolume::XYStepSize.x;
		int yL = SearchVolume::XYSearchLimits.y;
		int yStep = SearchVolume::XYStepSize.y;
		LogFile << "\nxL/xStep: " << xL << "/" << xStep << " | yL/yStep: " << yL << "/" << yStep << "\n" << std::endl;
		int numbX = 2 * xL / xStep;
		int numbY = 2 * yL / yStep;
		resultArray.resize(numbX);
		for ( int res = 0 ; res < resultArray.size() ; res++)
		{
			resultArray[res].resize(numbY);
			std::fill(resultArray[res].begin(), resultArray[res].end(), 0);
		}
		int xCounter = 0;
		for (int x = -xL; x <= xL; x += xStep)
		{
			int yCounter = 0;
			for (int y = -yL; y <= yL; y += yStep)
			{
				offset = cv::Point(x, y);
				thetaResult = XYGrid[r][c].TemplateThetaSearch(LogFile, BaseName,
					GuardResults[g], thetaBounds, scaleBounds, GuardPenalty, offset, TrackAlignment);

				scaleResult = XYGrid[r][c].TemplateScaleSearch(LogFile, BaseName,
					thetaResult, scaleBounds, GuardPenalty, offset, TrackAlignment);

				if (scaleResult.Score > bestResult.Score)
					bestResult = scaleResult;
				templateResults.push_back(scaleResult);

				if ( TrackAlignment && yCounter < numbY && xCounter < numbX)
				{ 
					resultArray[xCounter][yCounter] = scaleResult.Score;
				}
				yCounter++;
			}
			xCounter++;
			if (TrackAlignment)
			{
				LogFile << "Current Best (x) (" << x << ") " << bestResult.ToString() << "\n";
			}
		}
		if (TrackAlignment)
		{
			oString.str("");
			oString.clear();
			oString << BaseName << "_ResultsArray_" << g << "_" << r << "_" << c << ".csv";
			std::ofstream csvOut;
			csvOut.open(oString.str());
			csvOut << "#Loc.x, Loc.y, result\n";
			for (int x = 0; x < resultArray.size(); x++)
			{
				for (int y = 0; y < resultArray[x].size(); y++)
					csvOut << x << sep << y << sep << resultArray[x][y] << "\n";
			}
			csvOut.close();
		}
	}
	BestSearchResult = bestResult;
	return true;
}


