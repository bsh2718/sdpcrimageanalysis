///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// double CalcPerimeter(std::vector< cv::Point > &Contour)
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//! [includes]
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "calcperimeter.hpp"
//! [includes]

//! [namespace]
// using namespace std;
// using namespace cv;
//! [namespace]
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// double CalcPerimeter(std::vector< cv::Point > &Contour)
// Calculates the perimeter of a blob enclosed by the pixels in Contour.  The perimeter is defined for the path 
// connectingthe centers of the pixels in Contour. The return value is the perimeter. The pixels in Contour must be 
// eight-connected and form a closed loop.  If they do not, the return value will not make sense.
//
double CalcPerimeter(std::vector< cv::Point > &Contour)
{
	if ( Contour.size() < 2 )
		return 0.0;
	
	double peri;
	int last = (int)Contour.size() - 1;
	if ( Contour[0].x == Contour[last].x && Contour[0].y == Contour[last].y )
		peri = 0.0;
	else if ( Contour[0].x == Contour[last].x || Contour[0].y == Contour[last].y )
		peri = 1.0;
	else
		peri = 1.41421;
	
	for ( int n = 0 ; n < last ; n++ )
	{
		if ( Contour[n].x == Contour[n+1].x && Contour[n].y == Contour[n+1].y )
			;
		else if ( Contour[n].x == Contour[n+1].x || Contour[n].y == Contour[n+1].y )
			peri++;
		else
			peri += 1.41421;
	}
	
	return peri;
}
