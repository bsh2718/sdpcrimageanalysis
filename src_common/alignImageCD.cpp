#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "statistics.hpp"
#include "blob.hpp"
#include "blob_CD.hpp"
//#include "blobSortMethods.hpp"
#include "readinfofile.hpp"
#include "xyShiftCD.hpp"
#include "assignBlobsCD.hpp"
#include "trimRectangle.hpp"
#include "alignImageCD.hpp"
#include "createBlobCDLists.hpp"


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//int AlignImageCD(std::ofstream& LogFile, std::string& BaseName,
//	std::vector< Blob >& BlobList, std::vector< Blob >& OtherBlobList, std::vector< Blob >& BadBlobList,
//	cv::Mat& SectionImage, double DistToCenter, double ThetaResolution,
//	std::vector< ParameterLimit > PLimits,
//	cv::Size ImageSize, std::vector<cv::Point2f> WellSpacing, cv::Size2f WellSize, int SectionOfInterest,
//	std::vector< DeviceOrigins >& SectionOrigins,
//	std::vector< DeviceOrigins >& SubSectionOrigins, std::vector< cv::Size > SubSectionDimens,
//	std::vector< double >& Results, bool TrackAlignment, std::string& ErrMsg)
//
// Aligns image with the radial grid for CD type device 
// 
//			Input
// LogFile - ofstream connected to log filebuf
// BaseName - project name
// BlobList - vector of Blob. Includes well-like blobs which pass criteria to be used for quantitation.
// OtherBlobList - vector of Blob. Includes well-like blobs which do not pass criteria to be used for quantitation,
//		but appear to be droplets in wells.
// BadBlobList - vector of Blob.  Includes blobs which may not actually be droplets
// SectionImage - 8 bit gray scale image.  For each Section/Subsection pair in the device, a region is outlined using the
//		outside edges of the wells in each column of wells.  The outlines are the smallest regions each of which encloses
//		all of the wells of a single column in a Section/Subsection.  The interior of each of these regions is non zero.  Pixels outside
//		of this region are set to zero.  The value of the interiors indicates which Section/Subsection the column of wells
//      is a part of.
// DistToCenter - Distance (in pixels) from the top center edge of the image to the center of the CD.  This assumes that 
//      the alignment of the camera over the CD is correct.  The program has the ability to make small adjustments if
//      this distance is slightly off.
// ThetaResolution - The rows and column coordinates (y, x) of the center of each droplet are converted into radial 
//      coordinates (R, Theta).  R is in pixels with the same microns/pixel as the original image. Theta is in radians
//      with ThetaResolution radians/pixel.  R and Theta are expressed as y and x in the coordinates used to align the
//      droplets to the radial grid.
// PLimits - 
// ImageSize - size of the 
// WellSpacing - center to center distance of wells in pixels (R, Theta).  This is a vector because each subsection of
//       the device can have a different Theta spacing.
// WellSize - size of wells in pixels (in the original image) 
// SectionOfInterest - this is the one section in the image which is to be analyzed.  Droplets in wells which
//       are part of a different section are ignored.
// SectionOrigins - Coordinates in (R, Theta) of the upper left corner of each section. Only Theta is used.
// SubSectionOrigins - Coordinates in (R, Theta) of upper left corner of each section. Only R is used.
// SubSectionDimens - Number of rows and columns in a subsection.  This is the same for a given subsection regardless
//      of which section it is a part of.
// Results  - (X-Offset_Final, Y-Offset_Final, Angle, Scale_Final)
// TrackAlignment - Additional information is output if this is true
// ErrMsg - 

int AlignImageCD(std::ofstream& LogFile, std::string& BaseName,
	std::vector< Blob >& BlobList, std::vector< Blob >& OtherBlobList, std::vector< Blob >& BadBlobList,
	std::vector< Blob_CD > &Blob_CDList, std::vector< Blob_CD > &OtherBlob_CDList, std::vector< Blob_CD > BadBlob_CDList,
	cv::Mat& SectionImage, double DistToCenter, double ThetaResolution, std::vector< cv::Point > SubSectionBands,
	std::vector< ParameterLimit > PLimits,
	cv::Size ImageSize, std::vector<cv::Point2f> WellSpacing, cv::Size2f WellSize, int SectionOfInterest,
	std::vector< DeviceOrigins >& SectionOrigins,
	std::vector< DeviceOrigins >& SubSectionOrigins, std::vector< cv::Size > SubSectionDimens,
	std::vector< double >& Results, bool TrackAlignment, std::string& ErrMsg)
{
	std::ostringstream oString;
	std::ofstream outFile;
	std::string outName;
	std::vector< double > init;
	init.resize(4);
	init[0] = init[1] = init[2] = 0.0;
	init[3] = 1.0;
	std::vector< double > searchResults = init;
	int numbSections = (int)SectionOrigins.size();
	int numbSubSections = (int)SubSectionOrigins.size();
	Results = init;
	ErrMsg.clear();
	cv::Size sectionImageSize = SectionImage.size();
	cv::Point tmpPt;
	int numbBlobs = (int)BlobList.size();
	double imageMiddle = (double)(ImageSize.width) * 0.5;
	std::vector< cv::Point > subSectionRowsCols;
	//double arrayWidth = ( NumbCols - 1.0 ) * WellSpacing.x + WellSize.width;
	std::vector< cv::Point > pl;
;
	if (TrackAlignment)
	{
		oString.str("");
		oString.clear();
		oString << BaseName << "_150_BlobCD.csv";
		outFile.open(oString.str());
		if (!outFile.is_open())
		{
			ErrMsg.append("Unable to open ").append(oString.str());
		}
		else
		{
			if (Blob_CDList.size() > 0)
			{
				outFile << Blob_CDList[0].Header() << "\n";
				for (int n = 0; n < Blob_CDList.size(); n++)
					outFile << Blob_CDList[n].ToString() << "\n";
			}
			outFile.close();
		}

		oString.str("");
		oString.clear();
		oString << BaseName << "_151_OtherBlobCD.csv";
		outFile.open(oString.str());
		if (!outFile.is_open())
		{
			ErrMsg.append("Unable to open ").append(oString.str());
		}
		else
		{
			if (OtherBlob_CDList.size() > 0)
			{
				outFile << OtherBlob_CDList[0].Header() << "\n";
				for (int n = 0; n < OtherBlob_CDList.size(); n++)
					outFile << OtherBlob_CDList[n].ToString() << "\n";
			}
			outFile.close();
		}

		oString.str("");
		oString.clear();
		oString << BaseName << "_152_BadBlobCD.csv";
		outFile.open(oString.str());
		if (!outFile.is_open())
		{
			ErrMsg.append("Unable to open ").append(oString.str());
		}
		else
		{
			if (BadBlob_CDList.size() > 0)
			{
				outFile << BadBlob_CDList[0].Header() << "\n";
				for (int n = 0; n < BadBlob_CDList.size(); n++)
					outFile << BadBlob_CDList[n].ToString() << "\n";
			}
			outFile.close();
		}
	}
	//std::cout << "SubSectionOrigins.size()/SubSectionDimens.size(): " << SubSectionOrigins.size() << "/" << SubSectionDimens.size() << "\n";
	subSectionRowsCols.resize(SubSectionOrigins.size() + 1);
	for (int n = 0; n < SubSectionDimens.size(); n++)
	{
		subSectionRowsCols[(size_t)n+1].x = SubSectionDimens[n].width;
		subSectionRowsCols[(size_t)n+1].y = SubSectionDimens[n].height;
		LogFile << "subSectionsRowsCols[" << n << "].x/y: " << subSectionRowsCols[(size_t)n+1].x << "/" << subSectionRowsCols[(size_t)n+1].y << "\n";
	}
	// If Angle alignment is performed, it would be done here
	//
	// 
	///////////////////////////////////////////////////////////
	if (TrackAlignment)
	{
		LogFile << "\n>>>>> First call to XYShift()\n";
		LogFile << "PLimits[1].BaseValue: " << PLimits[1].BaseValue;
		LogFile << "\nDistanceToCenter: " << DistToCenter;
		LogFile << "\nimageMiddle: " << imageMiddle;
		LogFile << "\nScale: " << init[3];
		LogFile << "\nThetaResolution: " << ThetaResolution;
		LogFile << "\nSectionImage.size(): " << SectionImage.size() << "\n";
	}
	
	XYShiftCD(LogFile, init[3], 4,
		Blob_CDList, SectionOfInterest,
		SectionImage, PLimits,
		ThetaResolution, SubSectionBands,
		Results, TrackAlignment, 
		BaseName, ErrMsg);
	

	AssignBlobsCD assigner;
	if (!assigner.Initialize(LogFile, BaseName, Results,
		Blob_CDList, SectionOfInterest, subSectionRowsCols,
		SectionImage, WellSpacing, WellSize,
		DistToCenter, ThetaResolution,
		SectionOrigins, SubSectionOrigins,
		Results[3], PLimits[3].Limit,
		TrackAlignment, ErrMsg))
	{
		std::cout << ErrMsg << "\n";
		LogFile << "assigner.Intialize() returned false: " << ErrMsg << "\n";
	}

	if (TrackAlignment)
		LogFile << "Before ConnectNodes, SectionOfInterest: " << SectionOfInterest << "\n";

	for (int sub = 1; sub <= numbSubSections; sub++)
	{
		if (assigner.GetNumberOfNodes(SectionOfInterest, sub) > 10)
		{
			if (assigner.ConnectNodes(LogFile, BaseName, SectionOfInterest, sub, TrackAlignment))
			{
				if (assigner.EvaluateNodes(LogFile, SectionOfInterest, sub, BaseName))
				{
					cv::Point corner11;
					assigner.GetCorner11(SectionOfInterest, sub, corner11);
					if (TrackAlignment)
						LogFile << "     Position of (" << SectionOfInterest << ", " << sub << ") blob: " << corner11 << std::endl;
				}
				else
					LogFile << "\n>>>>> ConnectNodes for Section/SubSection " << SectionOfInterest << " / " << sub << " failed\n" << std::endl;
			}
			else
				LogFile << "\n>>>>> ConnectNodes failedfor Section/SubSection " << SectionOfInterest << " / " << sub << "\n" << std::endl;
		}
		else
			LogFile << "\n>>>>> Fewer than 10 nodes for Section/SubSection " << SectionOfInterest << " / " << sub << "\n" << std::endl;
	}
	//if (SectionOfInterest > 1)
	//{
	//	int s0 = SectionOfInterest - 1;
	//	for (int sub = 1; sub <= numbSubSections; sub++)
	//	{
	//		if (assigner.GetNumberOfNodes(s0, sub) > 10)
	//		{
	//			if (assigner.ConnectNodes(LogFile, BaseName, s0, TrackAlignment))
	//			{
	//				if (assigner.EvaluateNodes(LogFile, s0, sub, BaseName))
	//				{
	//					cv::Point corner11;
	//					assigner.GetCorner11(s0, sub, corner11);
	//					if (TrackAlignment)
	//						LogFile << "     Position of (" << s0 << ", " << sub << ") blob: " << corner11 << std::endl;
	//				}
	//			}
	//		}
	//	}
	//}
	//if (SectionOfInterest < numbSections)
	//{
	//	int s1 = SectionOfInterest + 1;
	//	for (int sub = 1; sub <= numbSubSections; sub++)
	//	{
	//		if (assigner.GetNumberOfNodes(s1, sub) > 10)
	//		{
	//			if (assigner.ConnectNodes(LogFile, BaseName, s1, TrackAlignment))
	//			{
	//				if (assigner.EvaluateNodes(LogFile, s1, sub, BaseName))
	//				{
	//					cv::Point corner11;
	//					assigner.GetCorner11(s1, sub, corner11);
	//					if (TrackAlignment)
	//						LogFile << "     Position of (" << s1 << ", " << sub << ") blob: " << corner11 << std::endl;
	//				}
	//			}
	//		}
	//	}
	//}

	std::string shiftString = assigner.SectionShiftsToString();
	LogFile << "==========\n";
	LogFile << "     Section Shifts\n";
	LogFile << shiftString;
	LogFile << "==========\n";
	double aveScale;
	double xScale;
	double yScale;
	assigner.EstimateScale(LogFile, SectionOfInterest, aveScale, xScale, yScale);
	Results[3] = aveScale;
	//std::cout << "AveScale: " << aveScale << std::endl;
	LogFile << "AveScale: " << aveScale << std::endl;
	assigner.RecheckBlobs(LogFile, Results, Results[3]);	
	//std::cout << "finished RecheckBlobs" << std::endl;
	assigner.UpdateBlobList(BlobList);

	std::vector<Blob_CD> otherBlob_CDList;
	//LogFile << "Assigning Other Blobs\n";
	assigner.AssignOtherBlobs(LogFile, OtherBlobList, OtherBlob_CDList, Results, Results[3]);
	//LogFile << "Assigning Bad Blobs\n";
	assigner.AssignOtherBlobs(LogFile, BadBlobList, BadBlob_CDList, Results, Results[3]);
	assigner.GetBlob_CDList(Blob_CDList);
	if (TrackAlignment)
	{
		oString.str("");
		oString.clear();
		oString << BaseName << "_160_BlobCD.csv";
		outFile.open(oString.str());
		if (!outFile.is_open())
		{
			ErrMsg.append("Unable to open ").append(oString.str());
		}
		else
		{

			if (Blob_CDList.size() > 0)
			{
				outFile << Blob_CDList[0].Header() << "\n";
				for (int n = 0; n < Blob_CDList.size(); n++)
					outFile << Blob_CDList[n].ToString() << "\n";
			}
			outFile.close();
		}

		oString.str("");
		oString.clear();
		oString << BaseName << "_161_OtherBlobCD.csv";
		outFile.open(oString.str());
		if (!outFile.is_open())
		{
			ErrMsg.append("Unable to open ").append(oString.str());
		}
		else
		{
			if (OtherBlob_CDList.size() > 0)
			{
				outFile << OtherBlob_CDList[0].Header() << "\n";
				for (int n = 0; n < OtherBlob_CDList.size(); n++)
					outFile << OtherBlob_CDList[n].ToString() << "\n";
			}
			outFile.close();
		}

		oString.str("");
		oString.clear();
		oString << BaseName << "_162_BadBlobCD.csv";
		outFile.open(oString.str());
		if (!outFile.is_open())
		{
			ErrMsg.append("Unable to open ").append(oString.str());
		}
		else
		{
			if (BadBlob_CDList.size() > 0)
			{
				outFile << BadBlob_CDList[0].Header() << "\n";
				for (int n = 0; n < BadBlob_CDList.size(); n++)
					outFile << BadBlob_CDList[n].ToString() << "\n";
			}
			outFile.close();
		}
		cv::Mat blobAssignments;
		blobAssignments.create(SectionImage.size(), CV_8UC3);
		cv::Vec3b cValue;
		cValue[0] = cValue[1] = cValue[2] = 0;
		blobAssignments.setTo(cValue);
		cv::Rect checkRect;
		checkRect.width = 5;
		checkRect.height = 5;
		for (int b = 0; b < Blob_CDList.size(); b++)
		{
			tmpPt.x = (int)round(Blob_CDList[b].AdjustedTheta_R2f.x);
			if (tmpPt.x > 1 && tmpPt.x < SectionImage.cols - 2)
			{
				tmpPt.y = (int)round(Blob_CDList[b].AdjustedTheta_R2f.y);
				if (tmpPt.y > 1 && tmpPt.y < SectionImage.rows - 2)
				{
					checkRect.x = tmpPt.x - 2;
					checkRect.y = tmpPt.y - 2;
					cValue[0] = Blob_CDList[b].Section + 50 * Blob_CDList[b].SubSection;
					cValue[1] = Blob_CDList[b].SubSectionPosition.x;
					cValue[2] = Blob_CDList[b].SubSectionPosition.y;
					blobAssignments(checkRect).setTo(cValue);
				}
			}
		}
		oString.str("");
		oString.clear();
		oString << BaseName << "_164_BlobCD_Assignments.png";
		cv::imwrite(oString.str(), blobAssignments);
	}


	//LogFile << "Before assigner.Clear()" << std::endl;
	assigner.Clear();
	LogFile << Results[0] << " / " << Results[1] << " / "<< Results[2] << " / "<< Results[3] << std::endl;
	int numbInSubImage = 0;
	int numbNotInSubImage = 0;
	for (int n = 0; n < Blob_CDList.size(); n++)
	{
		if (Blob_CDList[n].OnImage)
			numbInSubImage++;
		else
			numbNotInSubImage++;
	}
	LogFile << "Numb Blobs in subImage = " << numbInSubImage << "\n";
	LogFile << "Numb Blobs not in subImage = " << numbNotInSubImage << std::endl;
	
	return numbInSubImage;
}	
