#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "OtsuThreshold.hpp"


// bool SegmentVector(std::ofstream &LogFile, std::vector<double> &Data, 
	// std::vector<double> OtsuResults, int &BestOtsuIdx, 
	// std::vector< double > &MidPointResults, int &BestMidPointIdx, 
	// double BinWidth, std::vector< double > &BinList, std::vector< int > &Histogram, bool Track )
bool OtsuThreshold(std::ofstream &LogFile, std::vector<double> &Data, 
	AfterBeforeResults &Results )
{
	if ( Data.size() < 8 )
		return false;

	// bool track = true;
	Results.Resize(Data.size());
	sort(Data.begin(), Data.end());
	
	int dataSize = (int)Data.size();
	int dataSizeM1 = dataSize - 1;
	double sum = 0.0;
	double minValue = Data[0];
	double maxValue = Data[0];
	for ( int n = 0 ; n < dataSize ; n++ )
	{
		sum += Data[n];
		Results.Data[n] = Data[n];
		Results.Sum[n] = sum;
		if ( Data[n] > maxValue )
			maxValue = Data[n];
		if ( Data[n] < minValue )
			minValue = Data[n];
	}
	double bestOtsu = -1.0E+37;
	///////////
	double bestMidPoint = 1.0E+37; //
	///////////
	for ( int n = 0 ; n < dataSizeM1 ; n++ )
	{
		double numbAbove = dataSize - n - 1;
		double sumAbove = sum - Results.Sum[n];
		Results.BelowAve[n] = Results.Sum[n] /(double)(n + 1);
		Results.AboveAve[n] = sumAbove / numbAbove;
		Results.OtsuResults[n] = numbAbove * (n+1) * pow((Results.BelowAve[n] - Results.AboveAve[n]), 2 );
		///////////
		// Results.Middle[n] = 0.5 * ( Results.BelowAve[n] + Results.AboveAve[n] );
		// double midPoint = 0.5 * ( Results.Data[n] + Results.Data[n+1] );
		// Results.MidPointResults[n] = Results.Middle[n] - midPoint;
		///////////
		if ( Results.OtsuResults[n] > bestOtsu )
		{
			bestOtsu = Results.OtsuResults[n];
			Results.BestOtsuIdx = n;
			Results.NumbBelow = n;
		}
		///////////
		// if ( fabs(Results.MidPointResults[n]) < bestMidPoint )
		// {
			// bestMidPoint = fabs(Results.OtsuResults[n]);
			// Results.BestMidPointIdx = n;
		// }
		///////////
	}	
	Results.OtsuResults[dataSizeM1] = 0;
	
	Results.Threshold = 0.5 * ( Results.Data[Results.BestOtsuIdx] + Results.Data[Results.BestOtsuIdx+1] );
	Results.OtsuThreshold = Results.Threshold;
	Results.ThresholdIdx = -1;
	///////////
	// Results.MidPointResults[dataSizeM1] = 1.0E+37;
	///////////
	
	
	return true;
}
