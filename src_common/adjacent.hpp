#ifndef ADJACENT_H
#define ADJACENT_H
//! [includes]
#include <vector>
#include <algorithm>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//#include <iostream>
//#include <string>
//#include <cstdlib>
//! [includes]

#include "blob.hpp"
#include "statistics.hpp"

int GetListIndex(cv::Point Shift);
bool GetShiftDirection(int Indice, cv::Point &ShiftDirection);

class NodeConnection
{
public:
	int Status;
	int NodeIndex;
	cv::Point Shift;

	NodeConnection() : Status(0), NodeIndex(-10), Shift(cv::Point(0, 0))
	{}
	~NodeConnection() {}

	NodeConnection(const NodeConnection& Source) : Status(Source.Status), NodeIndex(Source.NodeIndex), 
		Shift(Source.Shift)
	{}

	NodeConnection& operator=(const NodeConnection& Source)
	{
		if (this != &Source)
		{
			Status = Source.Status;
			NodeIndex = Source.NodeIndex;
			Shift = Source.Shift;
		}
		return *this;
	}

	void Clear()
	{
		Status = 0;
		NodeIndex = -10;
		Shift = cv::Point(0,0);
	}

	std::string ToString()
	{
		std::ostringstream oString;
		oString.str("");
		oString.clear();
		oString << NodeIndex << "," << Shift.y << "," << Shift.x;
		return oString.str();
	}

	std::string ToStringHeader()
	{
		return "NodeIndex,Shift.y,Shift.x";
	}
};


bool SortConnections(const NodeConnection& A, const NodeConnection& B);

class BlobNode
{
public:
	int Index;
	int SelfIndex;
	int Status;
	int Array;
	int Section;
	cv::Point SectionPosition;
	cv::Point2f AdjustedCenter2f;
	cv::Point AdjustedCenter;
	cv::Point2f InputCenter2f;
	NodeConnection ReferenceNode;
	std::vector< NodeConnection > Connections;
	std::vector< int > ShiftDirections;
	int NumbDirections;
	/*bool Up;
	bool Down;
	bool Left;
	bool Right;
	bool Connected;*/

	BlobNode() : Index(-1), SelfIndex(-1), Status(0), Array(0), Section(0), SectionPosition(cv::Point(0, 0)),
		AdjustedCenter2f(cv::Point2f(0.0, 0.0)), AdjustedCenter(cv::Point(0, 0)), InputCenter2f(cv::Point2f(0.0, 0.0)), NumbDirections(0)/*,
		Up(false), Down(false), Left(false), Right(false), Connected(false)*/
	{
		ReferenceNode.Clear();
		Connections.clear();
		ShiftDirections.resize(8);
		std::fill(ShiftDirections.begin(), ShiftDirections.end(), 0);
	}

	~BlobNode() {}

	BlobNode(const BlobNode& Source) : Index(Source.Index), SelfIndex(Source.SelfIndex), Status(Source.Status),
		Array(Source.Array), Section(Source.Section), SectionPosition(Source.SectionPosition), 
		AdjustedCenter2f(Source.AdjustedCenter2f), AdjustedCenter(Source.AdjustedCenter), InputCenter2f(Source.InputCenter2f), ReferenceNode(Source.ReferenceNode),
		Connections(Source.Connections), ShiftDirections(Source.ShiftDirections), NumbDirections(Source.NumbDirections)
		/*,
		Up(Source.Up), Down(Source.Down), Left(Source.Left), Right(Source.Right), Connected(Source.Connected)*/
	{}

	BlobNode& operator=(const BlobNode& Source)
	{
		if (this != &Source)
		{
			Index = Source.Index;
			SelfIndex = Source.SelfIndex;
			Status = Source.Status;
			Array = Source.Array;
			Section = Source.Section;
			SectionPosition = Source.SectionPosition;
			AdjustedCenter2f = Source.AdjustedCenter2f;
			AdjustedCenter = Source.AdjustedCenter;
			InputCenter2f = Source.InputCenter2f;
			ReferenceNode = Source.ReferenceNode;
			Connections = Source.Connections;
			ShiftDirections = Source.ShiftDirections;
			NumbDirections = Source.NumbDirections;
			/*Up = Source.Up;
			Down = Source.Down;
			Left = Source.Left;
			Right = Source.Right;
			Connected = Source.Connected;*/
		}
		return *this;
	}

	BlobNode& operator=(const Blob& Source)
	{
		Index = Source.SelfIndex;
		SelfIndex = -1;
		Status = Source.Status;
		Array = Source.Array;
		Section = Source.Section;
		SectionPosition = Source.SectionPosition;
		AdjustedCenter2f = Source.AdjustedCenter2f;
		AdjustedCenter = Source.AdjustedCenter;
		AdjustedCenter = Source.AdjustedCenter;
		InputCenter2f = Source.InputCenter2f;
		ReferenceNode.Clear();
		Connections.clear();
		ShiftDirections.resize(8);
		std::fill(ShiftDirections.begin(), ShiftDirections.end(), 0);
		NumbDirections = 0;
		return *this;
	}

	void Clear()
	{
		Index = -1;
		SelfIndex = -1;
		Status = 0;
		Array = 0;
		Section = 0;
		SectionPosition = cv::Point(0, 0);
		AdjustedCenter2f = cv::Point2f(0.0, 0.0);
		AdjustedCenter = cv::Point(0, 0);
		InputCenter2f = cv::Point2f(0.0, 0.0);
		ReferenceNode.Clear();
		Connections.clear();
		ShiftDirections.resize(8);
		std::fill(ShiftDirections.begin(), ShiftDirections.end(), 0);
		NumbDirections = 0;
		/*Up = false;
		Down = false;
		Left = false;
		Right = false;
		Connected = false;*/
	}

	void Reset()
	{
		//Index = -1;
		Status = 0;
		//Array = 0;
		//Section = 0;
		SectionPosition = cv::Point(0, 0);
		//AdjustedCenter2f = cv::Point2f(0.0, 0.0);
		//InputCenter2f = cv::Point2f(0.0, 0.0);
		ReferenceNode.Clear();
		Connections.clear();
		ShiftDirections.resize(8);
		std::fill(ShiftDirections.begin(), ShiftDirections.end(), 0);
		NumbDirections = 0;
	}

	void AddConnection(int NodeIdx, cv::Point Shift)
	{
		NodeConnection newConnection;
		newConnection.Status = 0;
		newConnection.NodeIndex = NodeIdx;
		newConnection.Shift = cv::Point(Shift.x, Shift.y);
		if (ShiftDirections[GetListIndex(newConnection.Shift)] == 0)
			NumbDirections++;
		ShiftDirections[GetListIndex(newConnection.Shift)]++;
		Connections.push_back(newConnection);
	}

	/*bool SortConnections(const NodeConnection& A, const NodeConnection& B)
	{
		double a2 = pow(A.Shift.x, 2) + pow(A.Shift.y, 2);
		double b2 = pow(B.Shift.x, 2) + pow(B.Shift.y, 2);
		if (a2 != b2)
			return a2 < b2;
		else if (A.Shift.y == B.Shift.y)
			return A.Shift.x < B.Shift.x;
		else
			return A.Shift.y < B.Shift.y;
	}*/

	std::string ToString(std::string &Header)
	{
		Header = "Index,SelfIndex,Status,Array,Section,SecPos.y,SecPos.x,Adj.y,Adj.x,Inp.y,Inp.x,NumbC\n";
		std::ostringstream oString;
		oString.str("");
		oString.clear();
		
		oString << Index << "," << SelfIndex << "," << Status << "," << Array << "," << Section 
			<< "," << SectionPosition.y << "," << SectionPosition.x
			<< "," << AdjustedCenter2f.y << "," << AdjustedCenter2f.x
			<< "," << InputCenter2f.y << "," << InputCenter2f.x << "," << Connections.size() << "\n";
		return oString.str();
	}

	std::string ToString()
	{
		std::ostringstream oString;
		oString.str("");
		oString.clear();
		oString << Index << "," << SelfIndex << "," << Status << "," << Array << "," << Section 
			<< "," << SectionPosition.y << "," << SectionPosition.x
			<< "," << AdjustedCenter2f.y << "," << AdjustedCenter2f.x
			<< "," << InputCenter2f.y << "," << InputCenter2f.x << "," << Connections.size();
		if (Connections.size() > 0)
		{
			std::sort(Connections.begin(), Connections.end(), SortConnections);
			oString << "," << Connections[0].ToString();
		}
		oString << "\n";
		return oString.str();
	}

	/*void CheckConnections()
	{
		Up = Down = Left = Right = Connected = false;
		bool nw = false;
		bool ne = false;
		bool sw = false;
		bool se = false;
		if (Connections.size() > 0)
		{
			std::sort(Connections.begin(), Connections.end(), SortConnections);
			for (int n = 0; n < Connections.size(); n++)
			{
				if (Connections[n].Shift.x == 0 && Connections[n].Shift.y == 0)
				{

				}
				else if (Connections[n].Shift.x == 0 || abs(Connections[n].Shift.x) < abs(Connections[n].Shift.y))
				{
					if (Connections[n].Shift.y > 0)
						Up = true;
					else
						Down = true;
				}
				else if (Connections[n].Shift.y == 0 || abs(Connections[n].Shift.y) < abs(Connections[n].Shift.x))
				{
					if (Connections[n].Shift.x > 0)
						Right = true;
					else
						Left = true;
				}
				else if (Connections[n].Shift.x > 0 && Connections[n].Shift.y > 0)
					ne = true;
				else if (Connections[n].Shift.x < 0 && Connections[n].Shift.y > 0)
					nw = true;
				else if (Connections[n].Shift.x > 0 && Connections[n].Shift.y < 0)
					se = true;
				else
					sw = true;
			}
			if (!Up)
			{
				if (ne && nw)
					Up = true;
			}
			if (!Right)
			{
				if (ne && se)
					Right = true;
			}
			if (!Down)
			{
				if (se && sw)
					Down = true;
			}
			if (!Left)
			{
				if (sw && nw)
					Left = true;
			}
		}
	}*/
};

#endif // ADJACENT_H

