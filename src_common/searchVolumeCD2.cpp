////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// bool SearchArea::LocalSearch(std::ofstream &LogFile, std::vector< Blob > &BlobList, cv::Mat &CoordImage, 
//		std::vector< std::vector< int > > &ArrSecMatrix, cv::Size ImageSize, double Scale, 
//		double &SearchResult, ResultSingleXYOffset &LocalSearchResult, bool TrackAlignment )
//	
// int SearchArea::LocalScale(std::ofstream &LogFile, std::vector< Blob > &BlobList, cv::Mat &CoordImage, 
//		std::vector< std::vector< int > > &ArrSecMatrix, std::vector<ArraySection> &ArrSecList,
//		cv::Size ImageSize, int NumbCols, int NumbRows, cv::Point2f WellSpacing, std::pair<double, double> ScaleLimits,
//		double ScaleIn, double &ScaleOut, bool TrackAlignment )
//
// void SearchArea::EvaluateScaleShift( std::ofstream &LogFile, std::vector< Blob > &BlobList, cv::Mat &CoordImage )
//
// void SearchArea::GetBestShiftScale( double BestAngle, std::vector< double > &Results )
		
#include <iostream>
#include <ostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "blob.hpp"
#include "readinfofile.hpp"
#include "searchVolumeCD2.hpp"

/////////////////////////////////////////////////////////////////////////
// bool SearchArea::LocalSearch(std::ofstream &LogFile, std::vector< Blob > &BlobList, cv::Mat &CoordImage, 
//		std::vector< std::vector< int > > &ArrSecMatrix, cv::Size ImageSize, double Scale, 
//		int StepX, int StepY,
//		double &SearchResult, ResultSingleXYOffset &LocalSearchResult, bool TrackAlignment )
//
// 
//
//     input
// LogFile - Reference to ofstream for log file
// BlobList - vector of Blob.  Contains the droplets which will be used in quantitation
// CoordImage - 24 bit color image of the device. If a pixel is in a well, then its color
//		indicates the Array, Section, Column and Row of that well.  If a pixel is in a guard region, it
//		indicates which guard region(s) it is in.
// ArrSecMatrix - 2D vector of size [NumbArrays+1][NumbSections+1].  If a particular Array and Section are to be analyzed
//		in this image. If ArrSecMatrix[a][s] is not zero then Array a and Section s are to be analyzed.
// ImageSize - Size of input image
// Scale - Value of scale factor used in this procedure.
// StepX - Step size to search Local Area in X (if less than 2, it is set to 2)
// StepY - Step size to search Local Area in Y (if less than 2, it is set to 2)
// TrackAlignment - Output extra information for diagnostics if true
//
//     output
// SearchResult - Search metric related to the number of droplets aligned in a well and the number located in 
//		the space between the grids of wells
// LocalSearchResult - object containing information about the best X and Y offset and the results.
//

bool SortLocalScore(LocalScore A, LocalScore B)
{
	return A.Score > B.Score;
}

LocalScore SearchVolume::CalculateGuardScore(int ThIndex, double Th, int ScIndex, double Sc, cv::Mat& TransMat,
	cv::Mat& WorkImage1, cv::Mat& WorkImage2, cv::Mat& WorkImage3, cv::Mat& WorkImage4, cv::Mat& WorkImage5)
{
	cv::Mat rotMat = cv::getRotationMatrix2D(WellTemplateCenter, Th, Sc);
	cv::warpAffine(WorkImage1, WorkImage2, rotMat, WellTemplateSize, cv::INTER_NEAREST, cv::BORDER_CONSTANT, 0);
	cv::warpAffine(WorkImage2, WorkImage3, TransMat, WellTemplateSize, cv::INTER_NEAREST, cv::BORDER_CONSTANT, 0);
	WorkImage4.setTo(0);
	WellTemplateImage.copyTo(WorkImage4, WorkImage3);
	cv::threshold(WorkImage4, WorkImage5, 249.0, 255, cv::THRESH_BINARY);
	int newScore = cv::countNonZero(WorkImage5);
	return LocalScore(newScore, ThIndex, ScIndex, Th, Sc, cv::Point(0, 0));
}

LocalScore SearchVolume::GuardThetaSearch(std::ofstream& LogFile, std::string BaseName, cv::Point Offset,
	ParameterBounds ThBounds, ParameterBounds ScBounds, bool TrackAlignment)
{
	std::string sep = ", ";
	cv::Mat workImage1 = BlobImage.clone();
	cv::Mat workImage2(WellTemplateSize, CV_8UC1);
	cv::Mat workImage3(WellTemplateSize, CV_8UC1);
	cv::Mat workImage4(WellTemplateSize, CV_8UC1);
	cv::Mat workImage5(WellTemplateSize, CV_8UC1);
	
	cv::Mat transMat(cv::Size(3, 2), CV_64FC1);
	transMat.setTo(0.0);
	transMat.at<double>(cv::Point(0, 0)) = 1.0;
	transMat.at<double>(cv::Point(1, 1)) = 1.0;
	transMat.at<double>(cv::Point(2, 0)) = GlobalXYLocation.x + Offset.x;
	transMat.at<double>(cv::Point(2, 1)) = GlobalXYLocation.y + Offset.y;
	double scale = 0.5 * (ScBounds.LowerBound + ScBounds.UpperBound);
	if (TrackAlignment)
	{
		LogFile << "GuardThetaSearch in (y, x): (" << GridLocation.y << sep << GridLocation.x << "), at (" << GlobalXYLocation.y << sep
			<< GlobalXYLocation.x << "), scale: " << scale << "\n";
	}
	
	std::vector< double > thetaList;
	thetaList.clear();
	std::vector< LocalScore > thetaScores;
	thetaScores.clear();
	int minScore = 1000000;
	int minIdx = 0;
	LocalScore bestScore;
	for (int t = 0; t <= ThBounds.NumbSteps; t++)
	{
		thetaList.push_back(ThBounds.LowerBound + t * ThBounds.StepSize);
		LocalScore currentScore = CalculateGuardScore(t, thetaList[t], 0, scale, transMat,
			workImage1, workImage2, workImage3, workImage4, workImage5);
		currentScore.GridLocation = GridLocation;
		currentScore.Offset = Offset;
		thetaScores.push_back(currentScore);
		if (t == 0)
		{
			minScore = currentScore.Score;
			bestScore = currentScore;
			minIdx = 0;
		}
		else if (currentScore.Score < minScore)
		{
			minScore = currentScore.Score;
			bestScore = currentScore;
			minIdx = t;
		}
		//if (TrackAlignment)
		//{
		//	LogFile << t << sep << currentScore.Score << sep << currentScore.Theta << "\n";
		//}
	}
	ThetaLocalMax = bestScore.Theta;
	ScaleLocalMax = scale;
	if (TrackAlignment)
	{
		LogFile << ">>>> " << bestScore.ToString() << std::endl;
	}
	return bestScore;
}

LocalScore SearchVolume::GuardScaleSearch(std::ofstream& LogFile, std::string BaseName, cv::Point Offset,
	LocalScore BestGuardScore, bool TrackAlignment)
{
	cv::Mat workImage1 = BlobImage.clone();
	cv::Mat workImage2(WellTemplateSize, CV_8UC1);
	cv::Mat workImage3(WellTemplateSize, CV_8UC1);
	cv::Mat workImage4(WellTemplateSize, CV_8UC1);
	cv::Mat workImage5(WellTemplateSize, CV_8UC1);
	cv::Mat transMat(cv::Size(3, 2), CV_64FC1);
	transMat.setTo(0.0);
	transMat.at<double>(cv::Point(0, 0)) = 1.0;
	transMat.at<double>(cv::Point(1, 1)) = 1.0;
	transMat.at<double>(cv::Point(2, 0)) = GlobalXYLocation.x + Offset.x;
	transMat.at<double>(cv::Point(2, 1)) = GlobalXYLocation.y + Offset.y;
	LocalScore localBest = BestGuardScore;
	if (TrackAlignment)
	{
		LogFile << "GuardScaleSearch: input = " << localBest.ToString() << "\n";
	}
	double trialScale1 = BestGuardScore.Scale + 0.015;
	LocalScore trialScore1 = CalculateGuardScore(0, ThetaLocalMax, 1, trialScale1, transMat,
		workImage1, workImage2, workImage3, workImage4, workImage5);
	trialScore1.GridLocation = GridLocation;
	trialScore1.Offset = Offset;
	if (TrackAlignment)
	{
		LogFile << "> trial1: " << trialScore1.ToString() << "\n";
	}
	double trialScale2 = BestGuardScore.Scale - 0.015;
	LocalScore trialScore2 = CalculateGuardScore(0, ThetaLocalMax, 2, trialScale2, transMat,
		workImage1, workImage2, workImage3, workImage4, workImage5);
	trialScore2.GridLocation = GridLocation;
	trialScore2.Offset = Offset;
	if (TrackAlignment)
	{
		LogFile << ">> trial2: " << trialScore2.ToString() << "\n";
	}
	if (trialScore1.Score < localBest.Score && trialScore1.Score < trialScore2.Score)
	{
		localBest = trialScore1;
	}
	else if (trialScore2.Score < localBest.Score && trialScore2.Score < trialScore1.Score)
	{
		localBest = trialScore2;
	}
	if (TrackAlignment)
	{
		LogFile << ">>> best: " << localBest.ToString() << "\n";
	}
	return localBest;
}

SearchVolumeResult SearchVolume::CalculateTemplateScore(std::ofstream& LogFile, std::string BaseName, cv::Point Offset, int GuardPenalty,
	double Theta, double Scale, cv::Mat& WorkImage1, cv::Mat& WorkImage2, cv::Mat& WorkImage3, cv::Mat& WorkImage4,
	bool TrackAlignment)
{
	GuardPenalty = -abs(GuardPenalty);
	std::string outName;
	std::ostringstream oString;
	cv::Point GlobalXY = GlobalXYLocation + Offset;
	cv::Mat rotMat = cv::getRotationMatrix2D(WellTemplateCenter, Theta, Scale);
	cv::Mat transMat(cv::Size(3, 2), CV_64FC1);
	transMat.setTo(0.0);
	transMat.at<double>(cv::Point(0, 0)) = 1.0;
	transMat.at<double>(cv::Point(1, 1)) = 1.0;
	transMat.at<double>(cv::Point(2, 0)) = (double)GlobalXY.x;
	transMat.at<double>(cv::Point(2, 1)) = (double)GlobalXY.y;
	cv::warpAffine(WorkImage1, WorkImage2, rotMat, WellTemplateSize, cv::INTER_NEAREST, cv::BORDER_CONSTANT, 0);
	cv::warpAffine(WorkImage2, WorkImage3, transMat, WellTemplateSize, cv::INTER_NEAREST, cv::BORDER_CONSTANT, 0);
	WorkImage4.setTo(0);
	WellTemplateImage.copyTo(WorkImage4, WorkImage3);
	std::vector< cv::Point > idxList;
	cv::findNonZero(WorkImage4, idxList);
	int numbA = 0;
	int numbNotA = 0;
	int tmpScore = 0;
	int numbG = 0;
	for (int n = 0; n < idxList.size(); n++)
	{
		int itmp = WorkImage4.at<uchar>(idxList[n]);
		if (itmp > 249)
		{
			tmpScore += GuardPenalty;
			numbNotA++;
			numbG++;
		}
		else
		{
			if (itmp > 65)
				tmpScore += 65;
			else
				tmpScore += itmp;
			if (itmp > 60)
				numbA++;
			else
				numbNotA++;
		}
	}
	SearchVolumeResult currentSearchResult = SearchVolumeResult(tmpScore, GlobalXY, GridLocation, Theta, Scale, numbA, numbNotA, numbG);
	if (TrackAlignment)
	{
		LogFile << currentSearchResult.ToString() << "\n";
	}
	return currentSearchResult;
}

SearchVolumeResult SearchVolume::TemplateThetaSearch(std::ofstream& LogFile, std::string BaseName,
	LocalScore GuardLocalResult, ParameterBounds ThBounds, ParameterBounds ScBounds, int GuardPenalty, cv::Point Offset,
	bool TrackAlignment)
{
	GuardPenalty = -abs(GuardPenalty);
	SearchVolumeResult bestResult = SearchVolumeResult();
	bestResult.Score = GuardLocalResult.Score;
	bestResult.Theta = GuardLocalResult.Theta;
	bestResult.Scale = GuardLocalResult.Scale;
	bestResult.GlobalPosition = GuardLocalResult.GridLocation;
	bestResult.GridLocation = GuardLocalResult.GridLocation;

	cv::Mat workImage1 = BlobImage.clone();
	cv::Mat workImage2(WellTemplateSize, CV_8UC1);
	cv::Mat workImage3(WellTemplateSize, CV_8UC1);
	cv::Mat workImage4(WellTemplateSize, CV_8UC1);
	cv::Mat workImage5(WellTemplateSize, CV_8UC1);
	double scale = 0.5 * (ScBounds.LowerBound + ScBounds.UpperBound);
	std::vector< SearchVolumeResult > thetaScores;

	int maxScore = -1000000;
	int maxIdx = 0;
	std::vector< double > thetaList;
	thetaList.clear();
	std::vector< SearchVolumeResult > thetaResults;
	thetaResults.clear();
	for (int t = 0; t <= ThBounds.NumbSteps; t++)
	{
		thetaList.push_back(ThBounds.LowerBound + t * ThBounds.StepSize);
		SearchVolumeResult currentResult = CalculateTemplateScore(LogFile, BaseName,
			Offset, GuardPenalty, thetaList[t], scale,
			workImage1, workImage2, workImage3, workImage4, TrackAlignment);
		currentResult.GridLocation = GridLocation;
		thetaResults.push_back(currentResult);
		if (t == 0)
		{
			maxScore = currentResult.Score;
			maxIdx = t;
			bestResult = currentResult;
		}
		else if (currentResult.Score > maxScore)
		{
			maxScore = currentResult.Score;
			maxIdx = t;
			bestResult = currentResult;
		}
	}
	ThetaLocalMax = bestResult.Theta;
	ScaleLocalMax = scale;
	return bestResult;
}

SearchVolumeResult SearchVolume::TemplateScaleSearch(std::ofstream& LogFile, std::string BaseName,
	SearchVolumeResult ThetaResult, ParameterBounds ScBounds, int GuardPenalty, cv::Point Offset,
	bool TrackAlignment)
{
	cv::Mat workImage1 = BlobImage.clone();
	cv::Mat workImage2(WellTemplateSize, CV_8UC1);
	cv::Mat workImage3(WellTemplateSize, CV_8UC1);
	cv::Mat workImage4(WellTemplateSize, CV_8UC1);
	cv::Mat workImage5(WellTemplateSize, CV_8UC1);

	cv::Point location = GlobalXYLocation + Offset;
	std::vector< double > scaleList(ScBounds.NumbSteps);
	for (int n = 0; n < ScBounds.NumbSteps; n++)
		scaleList[n] = ScBounds.UpperBound - n * ScBounds.StepSize;
	SearchVolumeResult bestResult = ThetaResult;
	int minIdx = 0;
	int maxResult = ThetaResult.Score;
	std::vector< SearchVolumeResult > scaleScores;
	scaleScores.clear();
	SearchVolumeResult currentResult;
	for (int s = 0; s < ScBounds.NumbSteps; s++)
	{
		currentResult = CalculateTemplateScore(LogFile, BaseName, Offset, GuardPenalty,
			ThetaResult.Theta, scaleList[s], workImage1, workImage2, workImage3, workImage4,
			TrackAlignment);

		if (currentResult.Score > maxResult)
		{
			bestResult = currentResult;
		}
	}
	ScaleLocalMax = bestResult.Scale;
	return bestResult;
}

int SearchVolume::ExtraOutput(std::ofstream& LogFile, std::string BaseName, LocalScore BestScore, LocalScore& ScoreOut, bool TrackAlignment)
{
	std::ostringstream oString;
	cv::Mat workImage1 = BlobImage.clone();
	double scale = BestScore.Scale;
	cv::Mat rotMat = cv::getRotationMatrix2D(WellTemplateCenter, BestScore.Theta, BestScore.Scale);
	cv::Mat transMat(cv::Size(3, 2), CV_64FC1);
	transMat.setTo(0.0);
	transMat.at<double>(cv::Point(0, 0)) = 1.0;
	transMat.at<double>(cv::Point(1, 1)) = 1.0;
	transMat.at<double>(cv::Point(2, 0)) = GlobalXYLocation.x + BestScore.Offset.x;
	transMat.at<double>(cv::Point(2, 1)) = GlobalXYLocation.y + BestScore.Offset.y;
	cv::Mat workImage2(WellTemplateSize, CV_8UC1);
	cv::Mat workImage3(WellTemplateSize, CV_8UC1);
	cv::Mat workImage4(WellTemplateSize, CV_8UC1);
	cv::Mat workImage5(WellTemplateSize, CV_8UC1);
	cv::Mat workImage6(WellTemplateSize, CV_8UC1);
	cv::Mat workImage7(WellTemplateSize, CV_8UC1);
	cv::Mat structElement = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3), cv::Point(-1, -1));
	cv::Mat workImage10 = WellTemplateImage.clone();
	cv::Mat workImage11;
	cv::threshold(workImage10, workImage11, 150.0, 255, cv::THRESH_BINARY);

	cv::warpAffine(workImage1, workImage2, rotMat, WellTemplateSize, cv::INTER_NEAREST, cv::BORDER_CONSTANT, 0);
	cv::warpAffine(workImage2, workImage3, transMat, WellTemplateSize, cv::INTER_NEAREST, cv::BORDER_CONSTANT, 0);
	workImage4.setTo(0);
	WellTemplateImage.copyTo(workImage4, workImage3);
	cv::threshold(workImage4, workImage5, 249.0, 255, cv::THRESH_BINARY);
	int sc = cv::countNonZero(workImage5);
	ScoreOut = LocalScore(sc, 0, 0, BestScore.Theta, BestScore.Scale, BestScore.GridLocation);

	oString.str("");
	oString.clear();
	oString << BaseName << "_DilatedExtra_workImage1.png";
	cv::dilate(workImage1, workImage7, structElement);
	workImage6.setTo(0);
	workImage6.setTo(100, workImage11);
	workImage6.setTo(200, workImage7);
	cv::imwrite(oString.str(), workImage6);

	oString.str("");
	oString.clear();
	oString << BaseName << "_DilatedExtra_workImage2.png";
	cv::dilate(workImage2, workImage7, structElement);
	workImage6.setTo(0);
	workImage6.setTo(100, workImage11);
	workImage6.setTo(200, workImage7);
	cv::imwrite(oString.str(), workImage6);
	oString.str("");
	oString.clear();
	oString << BaseName << "_DilatedExtra_workImage3.png";
	cv::dilate(workImage3, workImage7, structElement);
	workImage6.setTo(0);
	workImage6.setTo(100, workImage11);
	workImage6.setTo(200, workImage7);
	cv::imwrite(oString.str(), workImage6);

	oString.str("");
	oString.clear();
	oString << BaseName << "_DilatedExtra_workImage5.png";
	cv::dilate(workImage5, workImage7, structElement);
	workImage6.setTo(0);
	workImage6.setTo(100, workImage11);
	workImage6.setTo(200, workImage7);
	cv::imwrite(oString.str(), workImage6);

	return sc;
}