#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>


#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "rollingball32F.hpp"

const float PresumedLargestValueInImage = 6553500.;
/** Implements a rolling-ball algorithm for the removal of smooth continuous background
	from a two-dimensional gel image.  It rolls the ball (actually a square patch on the
	top of a sphere) on a low-resolution (by a factor of 'shrinkFactor' times) copy of
	the original image in order to increase speed with little loss in accuracy.  It uses
	interpolation and extrapolation to blow the shrunk image to full size.
*/
// The use of the square patch means that the surface moved underneath the image is a square
// with the ball protruding up from it.  The diameter of the portion of the ball protruding
// equals the size of the square.  The corners of the square still contribute which could
// make any features in the background have square corners.  If this is not desirable, there
// is a version of this program (in rollingball2.cpp) which ignores the corners of the square
// and results in more rounded edges to any features in the background.
bool SubtractBackground32F(cv::Mat &Image, int BallRadius, cv::Mat &ImageOut )
{
	cv::Size imageSize = Image.size();
	if ( imageSize.width < 2 * BallRadius || imageSize.height < 2 * BallRadius )
		return false;
	uchar depth = Image.depth() & CV_MAT_DEPTH_MASK;
	// std::string smallName("smallImage.png");
	// cv::imwrite( smallName, smallImage);
	cv::Mat background;
	ImageOut = Image.clone();
	
	if ( depth == CV_32F || depth == CV_32FC1 )
	{
		RollingBall32F ball(BallRadius);
		cv::Mat smallImage;
		ShrinkImage32F(Image, ball.ShrinkFactor, smallImage);
		RollBall32F(ball, Image, smallImage, background);
		InterpolateBackground32F(background, ball);
		ExtrapolateBackground32F(background, ball);
		cv::subtract(Image, background, ImageOut);
	}
	else
		return false;
	
	return true;
}
// This version of SubtractBackground returns the background image
bool SubtractBackground32F(cv::Mat &Image, int BallRadius, cv::Mat &ImageOut, cv::Mat &Background )
{
	cv::Size imageSize = Image.size();
	if ( imageSize.width < 2 * BallRadius || imageSize.height < 2 * BallRadius )
		return false;
	uchar depth = Image.depth() & CV_MAT_DEPTH_MASK;

	// std::string smallName("smallImage.png");
	// cv::imwrite( smallName, smallImage);

	if ( depth == CV_32F || depth == CV_32FC1 )
	{
		ImageOut = Image.clone();
		RollingBall32F ball(BallRadius);
		cv::Mat smallImage;
		ShrinkImage32F(Image, ball.ShrinkFactor, smallImage);
		// std::vector< double > smallImageData;
		// smallImageData.resize(250);
		// for ( int n = 0 ; n < smallImageData.size() ; n++ )
			// smallImageData[n] = 0;
		// for ( int n = 0 ; n < smallImage.total() ; n++ )
		// {
			// double tmp = smallImage.at<float>(n);
			// int idx = round(4.0 * tmp);
			// if ( idx <= 0 )
				// smallImageData[0]++;
			// else if ( idx >= 250 )
				// smallImageData[249]++;
			// else
				// smallImageData[idx]++;
		// }
		// std::clog << "smallImage histogram\n";
		// for ( int n = 0 ; n < smallImageData.size() ; n++ )
		// {
			// double tmp = (double)n * (0.25);
			// std::clog << tmp << " - " << smallImageData[n] << "\n";
		// }
		RollBall32F(ball, Image, smallImage, Background);
		InterpolateBackground32F(Background, ball);
		ExtrapolateBackground32F(Background, ball);
		cv::subtract(Image, Background, ImageOut);
		
		// for ( int n = 0 ; n < Image.total() ; n++ )
		// {
			// if ( ImageOut.at<float>(n) < 0 )
			// {
				// ImageOut.at<float>(n) = 0;
				// Background.at<float>(n) = Image.at<float>(n);
			// }
		// }
		// std::string smallName("smallImage.png");
		// cv::imwrite( smallName, smallImage);
	}
	else
		return false;
	
	return true;
}

// Creates a lower resolution image for ball-rolling.
bool ShrinkImage32F(cv::Mat &Image, int ShrinkFactor, cv::Mat &SmallImage) 
{
	cv::Size imageSize = Image.size();
	cv::Size smallSize = cv::Size(imageSize.width/ShrinkFactor, imageSize.height/ShrinkFactor);
	// bool unsignedChar;
	uchar depth = Image.depth() & CV_MAT_DEPTH_MASK;
	if ( depth == CV_32F || depth == CV_32FC1 )
	{
		SmallImage.create(smallSize, CV_32F);
	}
	else
		return false;
 
	cv::Mat image2 = Image.clone();
	// cv::blur(Image, image2, cv::Size(3,3) );
	int xmaskmin;
	int ymaskmin;
	float min;
	float thispixel;
	for (int y = 0; y < smallSize.height; y++)
	{
		for (int x = 0; x < smallSize.width; x++)
		{
			xmaskmin = ShrinkFactor * x;
			ymaskmin = ShrinkFactor * y;
			min = PresumedLargestValueInImage;
			for (int j = 0; j < ShrinkFactor; j++) 
			{
				for (int k = 0; k < ShrinkFactor; k++) 
				{
					thispixel = image2.at<float>(ymaskmin+k, xmaskmin+j);
					if (thispixel < min)
						min = thispixel;
				}
			}
			// std::clog << "(" << y << ", " << x << ") " << min << "\n";
			SmallImage.at<float>(y,x) = min; // each point in small image is minimum of its neighborhood
		}
	}
	return true;
}

/** 'Rolls' a filtering object over a (shrunken) image in order to find the
	image's smooth continuous background.  For the purpose of explaining this
	algorithm, imagine that the 2D grayscale image has a third (height) dimension
	defined by the intensity value at every point in the image.  The center of
	the filtering object, a patch from the top of a sphere having radius BallRadius,
	is moved along each scan line of the image so that the patch is tangent to the
	image at one or more points with every other point on the patch below the
	corresponding (x,y) point of the image.  Any point either on or below the patch
	during this process is considered part of the background.  Shrinking the image
	before running this procedure is advised due to the fourth-degree complexity
	of the algorithm.
*/                                                                                                               
void RollBall32F(RollingBall32F &Ball, cv::Mat &ImageIn, cv::Mat &SmallImage, cv::Mat &Background )
{
	// ImageIn and SmallImage are assumed to be <float>
	//
	int halfpatchwidth;     //distance in x or y from patch center to any edge
	int ptsbelowlastpatch;  //number of points we may ignore because they were below last patch
	int xpt2, ypt2;         // current (x,y) point in the patch relative to upper left corner
	int xval, yval;         // location in ball in shrunken image coordinates
	float zdif;               // difference in z (height) between point on ball and point on image
	float zmin;               // smallest zdif for ball patch with center at current point
	float zctr;               // current height of the center of the sphere of which the patch is a part
	float zadd;               // height of a point on patch relative to the xy-plane of the shrunken image
	int ballpt;             // index to array storing the precomputed ball patch
	int imgpt;              // index to array storing the shrunken image
	int backgrpt;           // index to array storing the calculated background
	int ybackgrpt;          // displacement to current background scan line
	int p1, p2;             // temporary indexes to background, ball, or small image
	int ybackgrinc;         // distance in memory between two shrunken y-points in background
	int smallimagewidth;    // length of a scan line in shrunken image
	int left, right, top, bottom;
	
	cv::Size imageSize = ImageIn.size();
	cv::Size smallImageSize = SmallImage.size();
	Background.create(imageSize, CV_32FC1);
	Background.setTo(0.0);
	
	int leftroll = 0;
	int rightroll = imageSize.width/Ball.ShrinkFactor - 1;
	int toproll = 0;
	int bottomroll = imageSize.height/Ball.ShrinkFactor - 1;
	
	left = 1;
	right = rightroll - leftroll - 1;
	top = 1;
	bottom = bottomroll - toproll - 1;
	smallimagewidth = smallImageSize.width;
	int patchwidth = Ball.PatchWidth;
	halfpatchwidth = patchwidth / 2;
	ybackgrinc = Ball.ShrinkFactor * imageSize.width; // real dist btwn 2 adjacent (dy=1) shrunk pts
	zctr = 0; // start z-center in the xy-plane
	for (int ypt = top; ypt <= (bottom + patchwidth); ypt++) 
	{
		for (int xpt = left; xpt <= (right + patchwidth); xpt++) // while patch is tangent to edges or within image...
		{
			// xpt is far right edge of ball patch
			// do we have to move the patch up or down to make it tangent to but not above image?...
			zmin = PresumedLargestValueInImage; // presumed largest possible value
			ballpt = 0;
			ypt2 = ypt - patchwidth; // ypt2 is top edge of ball patch
			imgpt = ypt2 * smallimagewidth + xpt - patchwidth;
			while (ypt2 <= ypt) 
			{
				xpt2 = xpt - patchwidth; // xpt2 is far left edge of ball patch
				while (xpt2 <= xpt)  // check every point on ball patch
				{
					// only examine points on
					if ((xpt2 >= left) && (xpt2 <= right) && (ypt2 >= top) && (ypt2 <= bottom) && Ball.Data[ballpt] > 0 ) 
					{
						p1 = ballpt;
						p2 = imgpt;
						
						zdif = SmallImage.at<float>(p2) - (zctr + Ball.Data[p1]);  //curve - circle points
						if (zdif<zmin) // keep most negative, since ball should always be below curve
							zmin = zdif;
					} // if xpt2,ypt2
					ballpt++;
					xpt2++;
					imgpt++;
				} // while xpt2
				ypt2++;
				imgpt = imgpt - patchwidth - 1 + smallimagewidth;
			}  // while ypt2
			if (zmin != 0)
				zctr += zmin; // move ball up or down if we find a new minimum
			if (zmin < 0)
				ptsbelowlastpatch = halfpatchwidth; // ignore left half of ball patch when dz < 0
			else
				ptsbelowlastpatch = 0;
			// now compare every point on ball with background,  and keep highest number
			yval = ypt - patchwidth;
			ypt2 = 0;
			ballpt = 0;
			ybackgrpt = (yval - top + 1) * ybackgrinc;
			while (ypt2 <= patchwidth) 
			{
				xval = xpt - patchwidth + ptsbelowlastpatch;
				xpt2 = ptsbelowlastpatch;
				ballpt += ptsbelowlastpatch;
				backgrpt = ybackgrpt + (xval - left + 1) * Ball.ShrinkFactor;
				while (xpt2 <= patchwidth) { // for all the points in the ball patch
					if ((xval >= left) && (xval <= right) && (yval >= top) && (yval <= bottom)) {
						p1 = ballpt;
						zadd = zctr + Ball.Data[p1];
						p1 = backgrpt;
						//if (backgrpt>=backgroundpixels.length) backgrpt = 0; //(debug)
						if (zadd > Background.at<float>(p1) ) //keep largest adjustment}
							Background.at<float>(p1) = zadd;
					}
					ballpt++;
					xval++;
					xpt2++;
					backgrpt += Ball.ShrinkFactor; // move to next point in x
				} // while xpt2
				yval++;
				ypt2++;
				ybackgrpt += ybackgrinc; // move to next point in y
			} // while ypt2
		} // for xpt
	} // for ypt
}

/** Uses bilinear interpolation to find the points in the full-scale background
	given the points from the shrunken image background.  Since the shrunken background
	is found from an image composed of minima (over a sufficiently large mask), it
	is certain that no point in the full-scale interpolated background has a higher
	pixel value than the corresponding point in the original image
*/                                 
void InterpolateBackground32F(cv::Mat &Background, RollingBall32F &Ball) 
{
	int hloc, vloc;				// position of current pixel in calculated background
	int vinc;					// memory offset from current calculated pos to current interpolated pos
	int lastvalue, nextvalue;   // calculated pixel values between which we are interpolating
	int p;						// pointer to current interpolated pixel value
	int bglastptr, bgnextptr;   // pointers to calculated pixel values between which we are interpolating

	cv::Size backSize = Background.size();
	int leftroll = 0;
	int rightroll = backSize.width/Ball.ShrinkFactor-1;
	int toproll = 0;
	int bottomroll = backSize.height/Ball.ShrinkFactor-1;
	
	vloc = 0;
	for (int j = 1; j <= (bottomroll - toproll - 1); j++) //interpolate to find background interior
	{
		hloc = 0;
		vloc += Ball.ShrinkFactor;
		for (int i=1; i<=(rightroll-leftroll); i++)
		{
			hloc += Ball.ShrinkFactor;
			bgnextptr = vloc * backSize.width + hloc;
			bglastptr = bgnextptr - Ball.ShrinkFactor;

			nextvalue = (int)Background.at<float>(bgnextptr);
			lastvalue = (int)Background.at<float>(bglastptr);
			for (int ii = 1; ii <= (Ball.ShrinkFactor - 1); ii++) //interpolate horizontally
			{
				p = bgnextptr - ii;
				Background.at<float>(p) = (float)(lastvalue + (Ball.ShrinkFactor - ii) * (nextvalue - lastvalue)/Ball.ShrinkFactor);
			}
			for (int ii=0; ii<=(Ball.ShrinkFactor - 1); ii++) //interpolate vertically
			{
				bglastptr = (vloc - Ball.ShrinkFactor) * backSize.width + hloc - ii;
				bgnextptr = vloc * backSize.width + hloc - ii;
				lastvalue = (int)Background.at<float>(bgnextptr);
				nextvalue = (int)Background.at<float>(bglastptr);
				vinc = 0;
				for (int jj = 1; jj <= (Ball.ShrinkFactor - 1); jj++) {
					vinc = vinc - backSize.width;
					p = bgnextptr + vinc;
					Background.at<float>(p) = (float)(lastvalue + (Ball.ShrinkFactor - jj) * (nextvalue - lastvalue)/Ball.ShrinkFactor);
				} // for jj
			} // for ii
		} // for i
	} // for j
}

/** Uses linear extrapolation to find pixel values on the top, left, right,
	and bottom edges of the background.  First it finds the top and bottom
	edge points by extrapolating from the edges of the calculated and
	interpolated background interior.  Then it uses the edge points on the
	new calculated, interpolated, and extrapolated background to find all
	of the left and right edge points.  If extrapolation yields values
	below zero or above the PresumedLargestValueInImage, then they are set to 
	zero or PresumedLargestValueInImage respectively.
*/                                             
void ExtrapolateBackground32F(cv::Mat &Background, RollingBall32F &Ball) 
{
	int edgeslope;          	// difference of last two consecutive pixel values on an edge
	int pvalue;             	// current extrapolated pixel value
	int lastvalue, nextvalue;   //calculated pixel values from which we are extrapolating
	int p;                  	// pointer to current extrapolated pixel value
	int bglastptr, bgnextptr;   // pointers to calculated pixel values from which we are extrapolating

	cv::Size backSize = Background.size();
	int leftroll = 0;
	int rightroll = backSize.width/Ball.ShrinkFactor-1;
	int toproll = 0;
	int bottomroll = backSize.height/Ball.ShrinkFactor-1;
	
	for (int hloc = Ball.ShrinkFactor; hloc <= (Ball.ShrinkFactor * (rightroll - leftroll) - 1); hloc++) 
	{
		// extrapolate on top and bottom
		bglastptr = Ball.ShrinkFactor * backSize.width+hloc;
		bgnextptr = (Ball.ShrinkFactor + 1) * backSize.width+hloc;
		lastvalue = (int)Background.at<float>(bglastptr);
		nextvalue = (int)Background.at<float>(bgnextptr);
		edgeslope = nextvalue-lastvalue;
		p = bglastptr;
		pvalue = lastvalue;
		for (int jj=1; jj<=Ball.ShrinkFactor; jj++) 
		{
			p = p-backSize.width;
			pvalue = pvalue-edgeslope;
			if (pvalue < 0)
			{
				Background.at<float>(p) = 0;
			}
			else if (pvalue > PresumedLargestValueInImage)
			{
				Background.at<float>(p) = PresumedLargestValueInImage;
			}
			else
			{
				Background.at<float>(p) = (float)pvalue;
			}
		} // for jj
		bglastptr = (Ball.ShrinkFactor * (bottomroll - toproll - 1) - 1) * backSize.width + hloc;
		bgnextptr = Ball.ShrinkFactor * (bottomroll - toproll - 1) * backSize.width + hloc;
		lastvalue = (int)Background.at<float>(bglastptr);
		nextvalue = (int)Background.at<float>(bgnextptr);
		edgeslope = nextvalue-lastvalue;
		p = bgnextptr;
		pvalue = nextvalue;
		for (int jj = 1; jj <= ((backSize.height - 1) - Ball.ShrinkFactor * (bottomroll - toproll - 1)); jj++)
		{
			p += backSize.width;
			pvalue += edgeslope;
			if (pvalue < 0)
			{
				Background.at<float>(p) = 0;
			}
			else if (pvalue > PresumedLargestValueInImage)
			{
				Background.at<float>(p) = PresumedLargestValueInImage;
			}
			else
			{
				Background.at<float>(p) = (float)pvalue;
			}
		} // for jj
	} // for hloc
	for (int vloc=0; vloc < backSize.height; vloc++) 
	{
		// extrapolate on left and right
		bglastptr = vloc * backSize.width + Ball.ShrinkFactor;
		bgnextptr = bglastptr + 1;
		lastvalue = (int)Background.at<float>(bglastptr);
		nextvalue = (int)Background.at<float>(bgnextptr);
		edgeslope = nextvalue - lastvalue;
		p = bglastptr;
		pvalue = lastvalue;
		for (int ii = 1; ii <= Ball.ShrinkFactor; ii++) {
			p--;
			pvalue = pvalue - edgeslope;
			if (pvalue < 0)
			{
				Background.at<float>(p) = 0;
			}
			else if (pvalue > PresumedLargestValueInImage)
			{
				Background.at<float>(p) = PresumedLargestValueInImage;
			}
			else
			{
				Background.at<float>(p) = (float)pvalue;
			}
		} // for ii
		bgnextptr = vloc * backSize.width + Ball.ShrinkFactor * (rightroll - leftroll - 1) - 1;
		bglastptr = bgnextptr - 1;
		lastvalue = (int)Background.at<float>(bglastptr);
		nextvalue = (int)Background.at<float>(bgnextptr);
		edgeslope = nextvalue-lastvalue;
		p = bgnextptr;
		pvalue = nextvalue;
		for (int ii = 1; ii <= ((backSize.width - 1) - Ball.ShrinkFactor * (rightroll - leftroll - 1) + 1); ii++)
		{
			p++;
			pvalue = pvalue + edgeslope;
			if (pvalue < 0)
			{
				Background.at<float>(p) = 0;
			}
			else if (pvalue > PresumedLargestValueInImage)
			{
				Background.at<float>(p) = PresumedLargestValueInImage;
			}
			else
			{
				Background.at<float>(p) = (float)pvalue;
			}
		} // for ii
	} // for vloc
}
