#ifndef CREATEMASKS_HPP
#define CREATEMASKS_HPP
//! [includes]
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//#include <iostream>
//#include <string>
//#include <cstdlib>
//! [includes]

//#include "chamber.h"

//! [namespace]
//using namespace std;
//using namespace cv;
//! [namespace]

class WellMaskInfo
{
	public:
		cv::Point2f Center;			// Center of well
		int Array;					// Array that well is in
		int Section;				// Section that the well is in
		cv::Point SectionPosition;	// Col and Row (SectionPosition.x and SectionPosition.y) of well in Section
		cv::Point UpperLeft;		// Upper left position of well (not really used)
		cv::Point LowerRight;		// Lower Right position of well (not really used)
		bool ZFilled;				// Is well mostly filled in Z?  In the simulated fluorescence images, a blob in a well can have two different amounts of
									//		completeness of filling in Z.  If this is true for a blob, the more completely filled amount is used for that blob
		bool XYFilled;				// Is well mostly filled in X and Y? In the simulated fluorescence images, a blob in a well may not completely fill the
									//		well area. If this is true for a blob, then the blob area (before blurring) equals the well area.  If it is false
									//		then blob area is smaller by a random amount than the the well area.
		bool Empty;					// If true, the simulated image has no blob in this well, if false there is a blob
		double LengthFrac;			// If XYFilled is false, then the blob's horizontal size is reduced.  The reduced size of the blob is express as the fraction
									//		well's horizontal size.)
		double FillingFrac;			// equals IntegratedIntensity / FilledIntensity	
		double IntegratedIntensity;	// total integrated intensity of blob
		double FilledIntensity;		// total integrated intensity if blob filled the well in Z over its entire area (not just in the middle)
		
		WellMaskInfo(): Array(0), Section(0), ZFilled(true), XYFilled(true), Empty(false), LengthFrac(1.0), FillingFrac(1.0), IntegratedIntensity(0.0), FilledIntensity(0.0) {}
		~WellMaskInfo(){}

		WellMaskInfo& operator= ( const WellMaskInfo &Source )
		{
			if ( this !=&Source )
			{
				Center = Source.Center;
				Array = Source.Array;
				Section = Source.Section;
				SectionPosition = Source.SectionPosition;
				UpperLeft = Source.UpperLeft;
				LowerRight = Source.LowerRight;
				ZFilled = Source.ZFilled;
				XYFilled = Source.XYFilled;
				Empty = Source.Empty;
				LengthFrac = Source.LengthFrac;
				FillingFrac = Source.FillingFrac;
				IntegratedIntensity = Source.IntegratedIntensity;
				FilledIntensity = Source.FilledIntensity;
			}
			return *this;
		}
		
		WellMaskInfo( const WellMaskInfo &Source )
		{
			Center = Source.Center;
			Array = Source.Array;
			Section = Source.Section;
			SectionPosition = Source.SectionPosition;
			UpperLeft = Source.UpperLeft;
			LowerRight = Source.LowerRight;
			ZFilled = Source.ZFilled;
			XYFilled = Source.XYFilled;
			Empty = Source.Empty;
			LengthFrac = Source.LengthFrac;
			FillingFrac = Source.FillingFrac;
			IntegratedIntensity = Source.IntegratedIntensity;
			FilledIntensity = Source.FilledIntensity;
		}
};

// class DeviceOrigins
// {
	// public:
		// int Index;				// Array or Section index
		// cv::Point2f Origin;		// Location of upper left well in array or section.  
								// // If object is being used for an array, Origin.y is ignored
								// // If object is being used for a section, Origin.x is ignored
		// DeviceOrigins(){}
		// ~DeviceOrigins(){}
		
		// DeviceOrigins& operator= ( const DeviceOrigins &Source )
		// {
			// if ( this !=&Source )
			// {
				// Index = Source.Index;
				// Origin = Source.Origin;
			// }
			// return *this;
		// }
		
		// DeviceOrigins( const DeviceOrigins &Source )
		// {
			// Index = Source.Index;
			// Origin = Source.Origin;
		// }
// };

// class GuardInfo
// {
	// public:
		// int Index;			// Guard index
		// double Width;		// Width of Guard
		// double Location;	// Location of center of guard (this is column coordinate for XGuards and row coordinate for YGuards)
		// int PrevArea;		// Index of previous guard, -1 if this is first guard
		// int NextArea;		// Index of next guard, -1 if this is the last guard
		
		// GuardInfo(){}
		// ~GuardInfo(){}
		
		// GuardInfo& operator= ( const GuardInfo &Source )
		// {
			// if ( this !=&Source )
			// {
				// Index = Source.Index;
				// Width = Source.Width;
				// Location = Source.Location;
				// PrevArea = Source.PrevArea;
				// NextArea = Source.NextArea;
			// }
			// return *this;
		// }
		
		// GuardInfo( const GuardInfo &Source )
		// {
			// Index = Source.Index;
			// Width = Source.Width;
			// Location = Source.Location;
			// PrevArea = Source.PrevArea;
			// NextArea = Source.NextArea;
		// }
// };

#endif // CREATEMASKS_HPP

