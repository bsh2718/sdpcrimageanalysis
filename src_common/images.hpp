#ifndef IMAGES_H
#define IMAGES_H

//! [includes]
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "statistics.hpp"
#include "blob.hpp"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Uses results of alignment procedures and values of ImageSection and ImageAreaOffset to create shifted image and 
// also to crop the shifted image to create image which includes only the array/section pairs of interest.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool ShiftImage(const std::vector< double > &Results, cv::Mat &ImageIn, cv::Mat &ImageOut, double &ImageScale, double &ImageMinimum, std::string &ErrMsg );

bool CreateRegImage(const std::vector< double > &Results, cv::Rect SubImageArea, cv::Mat &ImageIn, bool Convert, 
	cv::Mat &ImageOut, cv::Mat &ShiftedImageOut, double &Scale, double &Minimum, std::string &ErrMsg );
	
#endif  // IMAGES_H