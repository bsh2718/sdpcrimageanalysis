#ifndef CALCCENTROID_HPP
#define CALCCENTROID_HPP

#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
	
int CalcCentroid(cv::Mat &Image32, std::vector< cv::Point> &Contour, cv::Mat &WorkImage, cv::Rect &BoundingRect, cv::Point2f &Center, 
	std::vector< cv::Point > &PixelList, std::vector<double> &Intensities);

#endif // CALCCENTROID_HPP