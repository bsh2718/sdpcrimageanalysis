////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 		These are member function of class AnalysisParameters.
//
// bool AnalysisParameters::ReadFile(std::string FileName, double WellArea, 
//		cv::Size2f WellSize2f, std::string &ErrorMsg)
//
// 	Input
// FileName - Name of file with filter information
// WellArea - Area of well (a).
// WellSize2f - Dimensions of well (a).

//
// (a) Information to be stored in member variable which is not in the file.
//
// 	Output
// ErrorMsg - Error message if there is a problem reading the Filter file.
//
// 	Example parameter File
//
// 150             Rolling Ball Radius
// 0.45            Background Fraction
// 0.2             Top Fraction (used for sizing)
// # Set 0
// 4 7             Well Height Deltas (minus, plus)
// 4 7             Well Width Deltas (minus, plus)
// 0.2 1.90        Well Area Fraction Limits (min, max)
// 0.65 1.1        Circularity Limits (min, max)
// 0.7 3           Aspect Ratio Limits (min, max)
// # Set 1
// 4 6             Well Height Deltas (minus, plus)
// 4 6             Well Width Deltas (minus, plus)
// 0.5 1.65        Well Area Fraction Limits (min, max)
// 0.65 1.1        Circularity Limits (min, max)
// 0.7 2           Aspect Ratio Limits (min, max)
// # Set 2
// 4 6.7           Well Height Deltas (minus, plus)
// 4 6.7           Well Width Deltas (minus, plus)
// 0.35 1.65       Well Area Fraction Limits (min, max)
// 0.65 1.1        Circularity Limits (min, max)
// 0.7 2           Aspect Ratio Limits (min, max)
// # Set 3
// 4 7             Well Height Deltas (minus, plus)
// 4 7             Well Width Deltas (minus, plus)
// 0.20 1.90       Well Area Fraction Limits (min, max)
// 0.65 1.1        Circularity Limits (min, max)
// 0.7 3           Aspect Ratio Limits (min, max)
//
// RollingBallRadius - (int) Radius of ball used for rolling ball background subtraction.
// Background Fraction - (double) After removing any unevenness in the background, the program performs a simple flat
// 	background subtraction.  In the initial step, the program needs an estimate of how much of the image should be
// 	background.  This is the estimate of the fraction of the image which is expected to be background.  It would be
// 	better if this fraction is too small than too large.
// Top Fraction - (double) Fraction of area of blob which needs to be both large and flat (relative to rest of blob)
//	before the filling fraction is considered reliaable. 
//
// There then follows a series of sets of filter parameters used to determine if a blob is acceptable as a droplet.
// Each set consists of a line with a '#' on it followed by 5 lines, each with two numbers on it.  The text following
// the '#' or the numbers is optional. The pairs of values are
//
// Well Height Delta (minus, plus) - Determines the minimum and maximum allowed height of a droplet.  The two values are
//		subtracted from and added to WellSize2f.height to obtain a range of allowed heights for a droplet.
// Well Width Delta (minus, plus) - Determines the minimum and maximum allowed width of a droplet.  The two values are
//		subtracted from and added to the WellSize2f.width to obtain a range of allowed widths for a droplet.
// Well Area Fraction Limits (min, max) - The area of a blob must be between (WellArea * min value) and (WellArea * max value)
//		to be accepted as a droplet.
// Circularity Limits (min, max) - The minimum and maximum acceptable values of a droplet's circularity.  Often, only the
//		lower end of the range is important, so the max value is set > 1.
// Aspect Ratio Limits (min, max) - The minimum and maximum acceptable values of a droplet's aspect ratio. The aspect
//		ratio is defined as the (bounding box width)/(bounding box height), where the dimensions of the bounding box
//		for the blob are used.
//
// The first set (Set 0) is used only during threshold determination.  The program will adjust the threshold to maximize
// the number of droplets as defined using Set 0.  Set 0 is not used elsewhere.
// The remaining sets are used to classify the droplet shape and must be listed starting with the most restrictive (Set 1)
// to the least restrictive (Set 3).  The droplets which satisfy Set 1 will be used in quantification.  The droplets which 
// satisfy only one of the other Sets (2 or 3 in this example) are retained to help align and index the droplets but are 
// not used in quantification.  The last set may be a repetition of Set 0 (as it is in this case).
// The program will create an additional Set from Set 1, which is used when analyzing droplets which were separated from
// blurred (or fused) columns of droplets.  For these droplets, the max height and area restrictions are relaxed.
// 
// int AnalysisParameters::TestBlob(int Set1, int SetN, BlobShape &Shape )
// Tests blob a blob to determine if it passes tests
// 	    Input
// Set1, SetN - The procedure will test the blob against sets Set1 through SetN inclusive.
// Shape - BlobShape from Blob to be tested.
//	    Output
// Return value is Set1 if the blob passes Set 1, Set2 if it passes Set 2, but not Set 1, etc.
//                 a number > 100 if the blob is potential a string of droplets blurred along a column.
//                 0 otherwise.
//                 -1 if there is a problem with the values of Set1 and/or SetN
//
// int TestBlob(int Set1, int SetN, BlobShape &Shape, unsigned int &ResultCode, std::string &ResultString );
// Tests blob a blob to determine if it passes tests
// 	    Input
// Set1, SetN - The procedure will test the blob against sets Set1 through SetN inclusive.
// Shape - BlobShape from Blob to be tested.
//	    Output
// ResultCode is a binary number where the bits set indicaes which tests the blob failed. Result is sum of
//		codes for all the failed tests
//			1 - failed height test
//			2 - failed width test
//			4 - failed area test
//			8 - failed circularity test
//			16 - failed aspect ratio test
// ResultString is a string which contains the same information as ResultCode.  If the blob passed all tests
//		then ResultString is set to "_____".  Otherwise the string takes on the form "HWACR", where the presence
//		of a letter indicates that the blob failed the (H)eight, (W)idth, (A)rea, (C)ircularity or Aspect (R)atio
//		tests.  If the blob passed a particular test then that position in the string will have a "_" instead of 
//		a letter.  For example, "H_A_R" means the blob failed the height, area and aspect ratio tests, but passed
//		the width and circularity tests.
// Return value is Set1 if the blob passes Set 1, Set2 if it passes Set 2, but not Set 1, etc.
//                 0 otherwise.
//                 -1 if there is a problem with the values of Set1 and/or SetN//
//
// int AnalysisParameters::TestBlob(int Set, BlobShape &Shape, int Area )
// Tests blob to see if it passes Set.
//	    Input
// Set - set of parameters used in set.
// Shape - BlobShape from Blob to be tested. As used, the Shape does not contain the area of the blob at this point.
// Area - Area of the blob
//	    Output
// Return value is 1 if the blob passes and 0 if it does not. If there is a problem with the value of Set, the 
//		procedure will return -1
//
// void Output(std::ofstream &LogFile) const
// Writes information about parameters to LogFile
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//! [includes]
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//#include <iostream>
//#include <string>
//#include <cstdlib>
//! [includes]

#include "physicalconstants.h"
#include "blob.hpp"
#include "analysisParameters.hpp"

bool AnalysisParameters::ReadFile(std::string FileName, double WellAreaIn, 
	cv::Size2f WellSize2f, std::string &ErrorMsg)
{
	std::istringstream token;
	std::string line;
	std::ostringstream oString;
	std::ifstream filterFile(FileName.c_str());
	if( ! filterFile ) 
	{
		oString.str("");
		oString.clear();
		oString << "Error opening " << FileName;
		ErrorMsg = oString.str();
		return false;
	}
	
	if ( WellSize2f.width < 3 || WellSize2f.height < 3 )
	{
		oString.str("");
		oString.clear();
		oString << "Invalid Well Dimensions " << WellSize2f;
		ErrorMsg = oString.str();
		return false;
	}
	WellSize = WellSize2f;
	WellArea = WellAreaIn;
	int i;
	// int j;
	double x;
	double y;
	ErrorMsg.clear();
	getline( filterFile, line);
	token.clear(); token.str(line); token >> i ;
	// std::clog << line << " - " << i << std::endl;
	if ( token.fail() )
	{
		filterFile.close();
		oString.str("");
		oString.clear();
		oString << "Error reading Rolling Ball Radius from " << FileName;
		ErrorMsg = oString.str();
		return false;
	}
	if ( i < 5 )
	{
		filterFile.close();
		oString.str("");
		oString.clear();
		oString << "Incorrect Rolling Ball Radius in " << FileName << ", value must be > 5";
		ErrorMsg = oString.str();
		return false;
	}
	RollingBallRadius = i;
	
	getline( filterFile, line);
	token.clear(); token.str(line); token >> x ;
	// std::clog << line << " - " << x << std::endl;
	if ( token.fail() )
	{
		filterFile.close();
		oString.str("");
		oString.clear();
		oString << "Error reading Background Fraction from " << FileName;
		ErrorMsg = oString.str();
		return false;
	}
	if ( x <= 0.01 )
	{
		filterFile.close();
		oString.str("");
		oString.clear();
		oString << "Incorrect Background Fraction in " << FileName << ", value must be > 0.01";
		ErrorMsg = oString.str();
		return false;
	}
	BackgroundFrac = x;

	getline( filterFile, line);
	token.clear(); token.str(line); token >> x ;
	// std::clog << line << " - " << x << std::endl;
	if ( token.fail() )
	{
		filterFile.close();
		oString.str("");
		oString.clear();
		oString << "Error reading Top Fraction from " << FileName;
		ErrorMsg = oString.str();
		return false;
	}
	if ( x <= 0.001 )
	{
		filterFile.close();
		oString.str("");
		oString.clear();
		oString << "Incorrect Top Fraction in " << FileName << ", value must be > 0.001";
		ErrorMsg = oString.str();
		return false;
	}
	TopFraction = x;
	
	ParameterSet.clear();
	MinMaxGroup newSet;
	bool cont = true;
	while ( cont )
	{
		getline( filterFile, line);
		if ( !filterFile.good() || line.find('#') == std::string::npos )
			break;

		getline( filterFile, line);
		token.clear(); token.str(line); token >> x >> y ;
		if ( token.fail() )
		{
			oString.str("");
			oString.clear();
			oString << "Error reading Well Height Deltas from " << FileName << ", set " << ParameterSet.size() << "\n";
			ErrorMsg += oString.str();
			break;
		}
		if ( x < 0 || y < 0 )
		{
			filterFile.close();
			oString.str("");
			oString.clear();
			oString << "Incorrect Well Height Deltas in " << FileName << ", set " << ParameterSet.size() << ". values must be >= 0\n";
			ErrorMsg += oString.str();
			break;
		}
		newSet.WellHeightDelta.Min = x;
		newSet.WellHeightDelta.Max = y;
		newSet.BlobHeight.Min = WellSize.height - x;
		newSet.BlobHeight.Max = WellSize.height + y;
		
		getline( filterFile, line);
		token.clear(); token.str(line); token >> x >> y ;
		if ( token.fail() )
		{
			oString.str("");
			oString.clear();
			oString << "Error reading Well Width Deltas from " << FileName << ", set " << ParameterSet.size() << "\n";
			ErrorMsg += oString.str();
			break;
		}
		if ( x < 0 || y < 0 )
		{
			filterFile.close();
			oString.str("");
			oString.clear();
			oString << "Incorrect Well Width Deltas in " << FileName << ", set " << ParameterSet.size() << ". values must be >= 0\n";
			ErrorMsg += oString.str();
			break;
		}
		newSet.WellWidthDelta.Min = x;
		newSet.WellWidthDelta.Max = y;
		newSet.BlobWidth.Min = WellSize.width - x;
		newSet.BlobWidth.Max = WellSize.width + y;
		
		getline( filterFile, line);
		token.clear(); token.str(line); token >> x >> y ;
		if ( token.fail() )
		{
			oString.str("");
			oString.clear();
			oString << "Error reading Well Area Fractions from " << FileName << ", set " << ParameterSet.size() << "\n";
			ErrorMsg += oString.str();
			break;
		}
		if ( x < 0 || y < x )
		{
			filterFile.close();
			oString.str("");
			oString.clear();
			oString << "Incorrect Well Area Fractions in " << FileName << ", set " << ParameterSet.size() << ". values must be >= 0\n";
			ErrorMsg += oString.str();
			break;
		}
		newSet.WellFraction.Min = x;
		newSet.WellFraction.Max = y;
		newSet.BlobArea.Min = WellArea * x;
		newSet.BlobArea.Max = WellArea * y;

		getline( filterFile, line);
		token.clear(); token.str(line); token >> x >> y ;
		// std::clog << line << " - " << x  << ", " << y << std::endl;
		if ( token.fail() )
		{
			filterFile.close();
			oString.str("");
			oString.clear();
			oString << "Error reading Circularity limits from " << FileName << "\n";
			ErrorMsg += oString.str();
			break;
		}
		if ( x < 0 || y < x )
		{
			filterFile.close();
			oString.str("");
			oString.clear();
			oString << "Incorrect Circularity values in " << FileName << ", values must be >= 0\n";
			ErrorMsg += oString.str();
			break;
		}
		newSet.Circularity.Min = x;
		newSet.Circularity.Max = y;

		getline( filterFile, line);
		token.clear(); token.str(line); token >> x >> y ;
		// std::clog << line << " - " << x  << ", " << y << std::endl;
		if ( token.fail() )
		{
			filterFile.close();
			oString.str("");
			oString.clear();
			oString << "Error reading Aspect Ratio limits from " << FileName << "\n";
			ErrorMsg = oString.str();
			break;
		}
		if ( x < 0 || y < 0 || x >= y )
		{
			filterFile.close();
			oString.str("");
			oString.clear();
			oString << "Incorrect Aspect Ratio limits in " << FileName << ", values must be >= 0\n";
			ErrorMsg = oString.str();
			break;
		}
		newSet.AspectRatio.Min = x;
		newSet.AspectRatio.Max = y;
		
		ParameterSet.push_back(newSet);
	}
	
	if ( ParameterSet.size() < 1 )
	{
		newSet.Default();
		ParameterSet.push_back(newSet);
	}
	if ( ParameterSet.size() < 2 )
	{
		newSet = ParameterSet[0];
		ParameterSet.push_back(newSet);
	}
	filterFile.close();
	if ( ErrorMsg.size() < 2 )
		return true;
	else
		return false;
}

// int AnalysisParameters::TestBlob(int Set1, int SetN, BlobShape &Shape )
// {
	// if ( Set1 < 1 || SetN < Set1 || SetN >= ParameterSet.size() )
		// return -1;
	// int res = 0;
	// // double fc = 0;
	// bool pass = true;
	// for ( int s = Set1 ; s <= SetN ; s++ )
	// {	
		// if ( Shape.BoundingRect.width < ParameterSet[s].BlobWidth.Min || Shape.BoundingRect.width > ParameterSet[s].BlobWidth.Max )
			// pass = false;
		
		// if ( Shape.BoundingRect.height < ParameterSet[s].BlobHeight.Min || Shape.BoundingRect.height > ParameterSet[s].BlobHeight.Max )
			// pass = false;
	
		// if ( Shape.PixelList.size() < ParameterSet[s].BlobArea.Min || Shape.PixelList.size() > ParameterSet[s].BlobArea.Max )
			// pass = false;

		// if ( Shape.Circularity < ParameterSet[s].Circularity.Min || Shape.Circularity > ParameterSet[s].Circularity.Max )
			// pass = false;

		// if ( Shape.AspectRatio < ParameterSet[s].AspectRatio.Min || Shape.AspectRatio > ParameterSet[s].AspectRatio.Max )
			// pass = false;
		
		// if ( pass )
		// {
			// res = s;
			// break;
		// }
		// else
			// pass = true;
	// }
	
	// if ( res == 0 )
	// {
		// // std::clog << "\nRect = " << Shape.BoundingRect << ", AspectRatio = " << Shape.AspectRatio << "\n";
		// // std::clog << "ParameterSet[" << Set1 << "].BlobWidth.Max/Min = " << ParameterSet[Set1].BlobWidth.Max << "/" << ParameterSet[Set1].BlobWidth.Min << "\n";
		// if ( Shape.AspectRatio < 1.0 ) 
		// {
			// double hAdjust = (double)Shape.BoundingRect.height * 0.05 + 0.5 * ParameterSet[Set1].WellWidthDelta.Max;
			// double maxWidth;
			// if ( hAdjust < ParameterSet[Set1].WellWidthDelta.Max )
				// maxWidth = ParameterSet[Set1].BlobWidth.Max;
			// else
				// maxWidth = WellSize.width + hAdjust;
			// }
		// }
	// }
	
	// return res;
// }

int AnalysisParameters::TestBlob(int Set1, int SetN, BlobShape &Shape, unsigned int &ResultCode, std::string &ResultString )
{
	if ( Set1 < 1 || SetN < Set1 || SetN >= ParameterSet.size() )
		return -1;
	int res = 0;
	// double fc = 0;
	bool pass = true;
	ResultString.clear();
	ResultCode = 0;
	
	if ( Shape.BoundingRect.height < ParameterSet[Set1].BlobHeight.Min || Shape.BoundingRect.height > ParameterSet[Set1].BlobHeight.Max )
	{
		pass = false;
		ResultCode += 1;
		ResultString = "H";
	}
	else
		ResultString = "_";


	if ( Shape.BoundingRect.width < ParameterSet[Set1].BlobWidth.Min || Shape.BoundingRect.width > ParameterSet[Set1].BlobWidth.Max )
	{
		pass = false;
		ResultCode += 2;
		ResultString.append("W");
	}
	else
		ResultString.append("_");
	
	if ( Shape.PixelList.size() < ParameterSet[Set1].BlobArea.Min || Shape.PixelList.size() > ParameterSet[Set1].BlobArea.Max )
	{
		pass = false;
		ResultCode += 4;
		ResultString.append("A");
	}
	else
		ResultString.append("_");

	if ( Shape.Circularity < ParameterSet[Set1].Circularity.Min || Shape.Circularity > ParameterSet[Set1].Circularity.Max )
	{
		pass = false;
		ResultCode += 8;
		ResultString.append("C");
	}
	else
		ResultString.append("_");

	if ( Shape.AspectRatio < ParameterSet[Set1].AspectRatio.Min || Shape.AspectRatio > ParameterSet[Set1].AspectRatio.Max )
	{
		pass = false;
		ResultCode += 16;
		ResultString.append("R");
	}
	else
		ResultString.append("_");
	
	if ( pass )
	{
		res = Set1;
	}
	else
	{
		for ( int s = Set1+1 ; s <= SetN ; s++ )
		{	
			if ( Shape.BoundingRect.height < ParameterSet[s].BlobHeight.Min || Shape.BoundingRect.height > ParameterSet[s].BlobHeight.Max )
				pass = false;
				
			if ( Shape.BoundingRect.width < ParameterSet[s].BlobWidth.Min || Shape.BoundingRect.width > ParameterSet[s].BlobWidth.Max )
			pass = false;
			
			if ( Shape.PixelList.size() < ParameterSet[s].BlobArea.Min || Shape.PixelList.size() > ParameterSet[s].BlobArea.Max )
				pass = false;

			if ( Shape.Circularity < ParameterSet[s].Circularity.Min || Shape.Circularity > ParameterSet[s].Circularity.Max )
				pass = false;

			if ( Shape.AspectRatio < ParameterSet[s].AspectRatio.Min || Shape.AspectRatio > ParameterSet[s].AspectRatio.Max )
				pass = false;
			
			if ( pass )
			{
				res = s;
				break;
			}
			else
				pass = true;
		}
	}
	
	// if ( res == 0 )
	// {
		// // std::clog << "\nRect = " << Shape.BoundingRect << ", AspectRatio = " << Shape.AspectRatio << "\n";
		// // std::clog << "ParameterSet[" << Set1 << "].BlobWidth.Max/Min = " << ParameterSet[Set1].BlobWidth.Max << "/" << ParameterSet[Set1].BlobWidth.Min << "\n";
		// if ( Shape.AspectRatio < 1.0 ) 
		// {
			// double hAdjust = (double)Shape.BoundingRect.height * 0.05 + 0.5 * ParameterSet[Set1].WellWidthDelta.Max;
			// double maxWidth;
			// if ( hAdjust < ParameterSet[Set1].WellWidthDelta.Max )
				// maxWidth = ParameterSet[Set1].BlobWidth.Max;
			// else
				// maxWidth = WellSize.width + hAdjust;
		// }
	// }
	
	return res;
}

int AnalysisParameters::TestBlob(int Set, BlobShape &Shape, int Area )
{
	if ( Set < 0 || Set >= ParameterSet.size() )
		return -1;
	
	int res = 1;
	if ( Shape.BoundingRect.width < ParameterSet[Set].BlobWidth.Min || Shape.BoundingRect.width > ParameterSet[Set].BlobWidth.Max )
		res = 0;
	
	if ( Shape.BoundingRect.height < ParameterSet[Set].BlobHeight.Min || Shape.BoundingRect.height > ParameterSet[Set].BlobHeight.Max )
		res = 0;
	
	if ( Area < ParameterSet[Set].BlobArea.Min || Area > ParameterSet[Set].BlobArea.Max )
		res = 0;
	
	if ( Shape.Circularity < ParameterSet[Set].Circularity.Min || Shape.Circularity > ParameterSet[Set].Circularity.Max )
		res = 0;

	if ( Shape.AspectRatio < ParameterSet[Set].AspectRatio.Min || Shape.AspectRatio > ParameterSet[Set].AspectRatio.Max )
		res = 0;
	
	if ( res > 0 )
		return 1;
	
	return 0;
}

// int AnalysisParameters::TestBlob(int Set, BlobShape &Shape, std::vector< bool > &Results )
// {
	// if ( Set < 0 || Set >= ParameterSet.size() )
		// return -1;
	
	// Results.resize(NUMBFILTERS);
	// int res = 1;
	
	// if ( Shape.BoundingRect.height < ParameterSet[Set].BlobHeight.Min || Shape.BoundingRect.height > ParameterSet[Set].BlobHeight.Max )
	// {
		// res = 0;
		// Results[0] = false;
	// }
	// else
		// Results[0] = true;
	
	// if ( Shape.BoundingRect.width < ParameterSet[Set].BlobWidth.Min || Shape.BoundingRect.width > ParameterSet[Set].BlobWidth.Max )
	// {
		// res = 0;
		// Results[1] = false;
	// }
	// else
		// Results[1] = true;
	
	// if ( Shape.BlobIntensities.Count() < ParameterSet[Set].BlobArea.Min || Shape.BlobIntensities.Count() > ParameterSet[Set].BlobArea.Max )
	// {
		// res = 0;
		// Results[2] = false;
	// }
	// else
		// Results[2] = true;
	
	// if ( Shape.Circularity < ParameterSet[Set].Circularity.Min || Shape.Circularity > ParameterSet[Set].Circularity.Max )
	// {
		// res = 0;
		// Results[3] = false;
	// }
	// else
		// Results[3] = true;

	// if ( Shape.AspectRatio < ParameterSet[Set].AspectRatio.Min || Shape.AspectRatio > ParameterSet[Set].AspectRatio.Max )
	// {
		// res = 0;
		// Results[4] = false;
	// }
	// else
		// Results[4] = true;
	
	// return res;
// }

// int AnalysisParameters::TestBlob(int Set, BlobShape &Shape, unsigned int &Result )
// {
	// if ( Set < 0 || Set >= ParameterSet.size() )
		// return -1;
	
	// Result = 0;
	// int res = 1;
	
	// if ( Shape.BoundingRect.height < ParameterSet[Set].BlobHeight.Min || Shape.BoundingRect.height > ParameterSet[Set].BlobHeight.Max )
	// {
		// res = 0;
		// Result += 1;
	// }
	
	// if ( Shape.BoundingRect.width < ParameterSet[Set].BlobWidth.Min || Shape.BoundingRect.width > ParameterSet[Set].BlobWidth.Max )
	// {
		// res = 0;
		// Result += 2;
	// }
	
	// if ( Shape.BlobIntensities.Count() < ParameterSet[Set].BlobArea.Min || Shape.BlobIntensities.Count() > ParameterSet[Set].BlobArea.Max )
	// {
		// res = 0;
		// Result += 4;
	// }
	
	// if ( Shape.Circularity < ParameterSet[Set].Circularity.Min || Shape.Circularity > ParameterSet[Set].Circularity.Max )
	// {
		// res = 0;
		// Result += 8;
	// }

	// if ( Shape.AspectRatio < ParameterSet[Set].AspectRatio.Min || Shape.AspectRatio > ParameterSet[Set].AspectRatio.Max )
	// {
		// res = 0;
		// Result += 16;
	// }
	
	// return res;
// }

void AnalysisParameters::Output(std::ofstream &LogFile) const
{
	LogFile << "\n\n     Blob Analysis Parameters\n";
	LogFile << "Rolling Ball Radous: " << RollingBallRadius << "\n";
	LogFile << "Background Fraction: " << BackgroundFrac << "\n";
	LogFile << "Top Fraction: " << TopFraction << "\n";
	LogFile << "\nBlob Filter Parameters\n";
	for ( int n = 0 ; n < ParameterSet.size() ; n++ )
	{
		LogFile << "---------- Set " << n << " ----------\n";
		LogFile << "    Description (Minimum/Maximum)\n";
		LogFile << "0 - Blob Width (" << ParameterSet[n].BlobWidth.Min << "/" << ParameterSet[n].BlobWidth.Max << ")\n";
		LogFile << "1 - Blob Height (" << ParameterSet[n].BlobHeight.Min << "/" << ParameterSet[n].BlobHeight.Max << ")\n";
		LogFile << "2 - Blob Area (" << ParameterSet[n].BlobArea.Min << "/" << ParameterSet[n].BlobArea.Max << ")\n";
		LogFile << "3 - Circularity (" << ParameterSet[n].Circularity.Min << "/" << ParameterSet[n].Circularity.Max << ")\n";
		LogFile << "4 - Aspect Ratio (" << ParameterSet[n].AspectRatio.Min << "/" << ParameterSet[n].AspectRatio.Max << ")\n";
		LogFile << "\n";
	}
	LogFile << "----------       ----------\n";
	LogFile << "     Parameters loaded from main file\n";
	LogFile << "WellSize (w, h): (" << WellSize.width << ", " << WellSize.height << ")\n";
	LogFile << "Well Area: " << WellArea << "\n";
	LogFile << "\n";
}


