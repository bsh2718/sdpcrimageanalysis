#ifndef CALCULATEWELLTEMPLATES_HPP
#define CALCULATEWELLTEMPLATES_HPP

#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"


bool CalculateWellTemplates( cv::Size2f WellSize, double WellArea, double TopFraction, int MaxNumber,
	std::vector< std::vector< cv::Point > > &TemplatePixels, std::vector< double > &TemplatePixelFraction );

#endif  // CALCULATEWELLTEMPLATES_HPP
