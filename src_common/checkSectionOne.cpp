//! [includes]
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//#include <iostream>
//#include <string>
//#include <cstdlib>
//! [includes]

#include "blob.hpp"
#include "trimRectangle.hpp"
#include "checkSectionOne.hpp"

///////////////////////////////////////////////////////////////////////////////////////////////////
//
// bool CheckSectionOne(std::ofstream& LogFile, cv::Size ImageSize, cv::Point2f WellSpacing, int FirstRowSum, cv::Mat &WorkImage,
//     std::vector<Blob>& BlobList, std::vector<Blob>& OtherBlobList, std::vector<Blob>& BadBlobList, bool Track)
// 
// Remove blobs which are close to the top of the image and appear to be blobs associated with reservoirs.
//
// FirstRowSum is the product of the number of colums and the number of arrays.  it is assumed that the number of droplets
// in the first row of all the arrays combined will be at least FirstRowSum/4.  The image is broken into horizontal strips of
// 1.5 * WellSpacing.y.  The first strip which contains at least FirstRowSum/4 blobs (from BlobList) is considered to be the
// top of the well grid.  Any blobs above that strip are identified as not droplets and are removed from BlobList and 
// OtherBlobList to BadBlobList.
// 
// Its not clear if this is necessary given that assignBlobs works by creating a network of connections.
//
bool CheckSectionOne(std::ofstream& LogFile, cv::Size ImageSize, cv::Point2f WellSpacing, int FirstRowSum, cv::Mat &WorkImage,
	std::vector<Blob>& BlobList, std::vector<Blob>& OtherBlobList, std::vector<Blob>& BadBlobList, bool Track)
{
	double topSpacing = WellSpacing.y * 1.5;
	int topMargin = (int)round(500.0 / topSpacing);
	std::vector< int > topCount;
	topCount.resize(topMargin + 1);
	WorkImage.create(ImageSize, CV_8U);
	WorkImage.setTo(0);
	for (int n = 0; n < BlobList.size(); n++)
	{
		int y = (int)round(BlobList[n].InputCenter2f.y / topSpacing);
		if (y < topCount.size())
		{
			topCount[y]++;
		}
		WorkImage.at<uchar>(BlobList[n].InputCenter2f) = 255;
	}
	if (Track)
	{
		LogFile << " TopCount\n";
		for (int n = 0; n < topCount.size(); n++)
			LogFile << n << " / " << n * topMargin << " / " << topCount[n] << "\n";
		LogFile << "==========";
	}
	int limit = 0;
	while (limit < topCount.size() && (topCount[limit] < FirstRowSum / 4))
		limit++;
	int first = limit;
	while (first > 0 && topCount[first] > 0 && (limit - first) < 5)
	{
		if ( Track)
			LogFile << "Count for " << first << " = " << topCount[first] << "\n";
		first--;
	}
	double cutoff2 = first * topSpacing;
	double cutoff1 = limit * topSpacing;
	if ( Track)
		LogFile << "limit/first/cutoff1/2 = " << limit << " / " << first << " / " << cutoff1 << " / " << cutoff2 << "\n";
	std::vector< int > transferList;
	std::vector< int > checkList;
	transferList.clear();
	checkList.clear();
	cv::Size recSize;
	recSize.width = (int)round(WellSpacing.x * 8.0);
	recSize.height = (int)round(WellSpacing.y * 3.0);
	if (recSize.width % 2 == 0)
		recSize.width++;
	if (recSize.height % 2 == 0)
		recSize.height++;
	cv::Point offset;
	offset.x = -(recSize.width -1)/2;
	offset.y = -(int)round(WellSpacing.y + 1.0);
	for (int b = 0; b < BlobList.size(); b++)
	{
		if (BlobList[b].InputCenter2f.y < cutoff2)
			transferList.push_back(b);
		else if (BlobList[b].InputCenter2f.y < cutoff1)
			checkList.push_back(b);
	}
	if (checkList.size() > 0)
	{
		cv::Rect checkRect;
		for (int t = (int)checkList.size() - 1; t >= 0; t--)
		{
			int idx = checkList[t];
			checkRect.x = (int)round(BlobList[idx].InputCenter2f.x) + offset.x;
			checkRect.y = (int)round(BlobList[idx].InputCenter2f.y) + offset.y;
			TrimRectangle(checkRect, recSize, ImageSize);
			int found = cv::countNonZero(WorkImage(checkRect));
			if (found < 3)
				transferList.push_back(idx);
		}
	}
	
	if (transferList.size() > 0)
	{
		std::sort(transferList.begin(), transferList.end());
		for (int t = (int)transferList.size() - 1; t >= 0; t--)
		{
			int idx = transferList[t];
			BadBlobList.push_back(BlobList[idx]);
			if ( Track)
				LogFile << "Removing blob " << idx << " at " << BlobList[idx].InputCenter2f << "\n";
			BlobList.erase(BlobList.begin() + idx);
		}
		for (int b = 0; b < BlobList.size(); b++)
			BlobList[b].SelfIndex = b;

		LogFile << "CheckSectionOne removed " << transferList.size() << " blobs from BlobList which were above y = " << cutoff1 << std::endl;
	}
	int numbRemoved = (int)transferList.size();
	transferList.clear();
	checkList.clear();
	for (int b = 0; b < OtherBlobList.size(); b++)
	{
		if (OtherBlobList[b].InputCenter2f.y < cutoff2)
			transferList.push_back(b);
		else if (OtherBlobList[b].InputCenter2f.y < cutoff1)
			checkList.push_back(b);
	}
	if (checkList.size() > 0)
	{
		cv::Rect checkRect;
		for (int t = (int)checkList.size() - 1; t >= 0; t--)
		{
			int idx = checkList[t];
			checkRect.x = (int)round(BlobList[idx].InputCenter2f.x) + offset.x;
			checkRect.y = (int)round(BlobList[idx].InputCenter2f.y) + offset.y;
			TrimRectangle(checkRect, recSize, ImageSize);
			int found = cv::countNonZero(WorkImage(checkRect));
			if (found < 3)
				transferList.push_back(idx);
		}
	}
	if (transferList.size() > 0)
	{
		std::sort(transferList.begin(), transferList.end());
		for (int t = (int)transferList.size() - 1; t >= 0; t--)
		{
			int idx = transferList[t];
			BadBlobList.push_back(OtherBlobList[idx]);
			if ( Track)
				LogFile << "Removing other blob " << idx << " at " << OtherBlobList[idx].InputCenter2f << "\n";
			OtherBlobList.erase(OtherBlobList.begin() + idx);
		}
		for (int b = 0; b < OtherBlobList.size(); b++)
			OtherBlobList[b].SelfIndex = b;

		LogFile << "CheckSectionOne removed " << transferList.size() << " blobs from OtherBlobList which were above y = " << cutoff1 << std::endl;
	}
	numbRemoved += (int)transferList.size();
	if (numbRemoved == 0 )
		return true;
	else
		return false;
}
