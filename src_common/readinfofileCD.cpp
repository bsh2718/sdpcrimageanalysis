///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// bool ReadDeviceInfo(std::string InfoFileName,
// 	std::vector< DeviceOrigins > &ArrayOrigins,
// 	std::vector< DeviceOrigins > &SectionOrigins, std::vector< cv::Size > &SectionDimens,
// 	std::vector< GuardInfo > &TGuards, std::vector< GuardInfo > &RGuards,
// 	std::string &ErrorMsg)
//
// bool ReadSubImageData(std::string SubImageFileName, int NumbArrays, std::vector<ArraySection> &ArrSecList,
//		int &NumbArraysUsed, cv::Rect &ImageAnalysisArea, cv::Rect &SubImageArea,
// 		cv::Size2f &WellSize, double &WellArea, cv::Point2f &WellSpacing, //double &WellVolume,
// 		std::vector< ParameterLimit > &PLimits, double &BlockSizeMult, double &MinimumPeak,
// 		double &MaxFillingRatio, std::string &ErrorMsg )
//
// bool WriteDeviceInfo(std::string InfoFileName, 
//		std::vector< DeviceOrigins > &ArrayOrigins,
//		std::vector< DeviceOrigins > &SectionOrigins, std::vector< cv::Size > &SectionDimens,
//		std::vector< GuardInfo > &TGuards, std::vector< GuardInfo > &RGuards, 
//		std::string &ErrorMsg)
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//! [includes]
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <string>
#include <sstream>
//#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
//#include <cmath>
#include <algorithm>
#include <vector>
//#include <iomanip>

#include "defines.h"
//#include "createmasks.hpp"
#include "readinfofile.hpp"
#include "readinfofileCD.hpp"
//! [includes]

//! [namespace]
//using namespace cv;
//! [namespace]
//using namespace std;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// bool ReadDeviceInfo()
//
// Reads info file, which contains device specific information about the position and size of arrays and sections and
// X and Y guards on device.  
//          Input Arguments:
// InfoFileName - string object with name of info file
//          Output Arguments:
// ArrayOrigins - Index and the location of the left edge of each array (the y coordinate should be zero)    
// SectionOrigins - Index and location of the top of each section (the x coordinate should be zero)
// SectionDimens - Width and Height of section in wells. it is assumed that all sections have the same dimensions
// TGuards - vector of XGuard information (see stage1Alignment.cpp for description)
// RGuards - vector of YGuard information
// ErrorMsg - If something goes wrong, then the procedure returns false and this will contain an error message.
//
bool ReadDeviceInfoCD(std::string InfoFileName, double& DistanceToCenter, double &ThetaResolution,
	std::vector< DeviceOrigins > &SectionOrigins,
	std::vector< DeviceOrigins > &SubSectionOrigins, std::vector< cv::Size > &SubSectionDimens, std::vector<cv::Point2f> &SubSectionSpacing,
	std::vector< GuardInfo > &TGuards, std::vector< GuardInfo > &RGuards, 
	std::string &ErrorMsg)
{
	//! [load]
	std::istringstream token;
	std::string line;
	std::ostringstream oString;
	int numbInFile;
	std::ifstream infoFile(InfoFileName.c_str());
	if( ! infoFile ) 
	{
		oString.str("");
		oString.clear();
		oString << "Error opening " << InfoFileName;
		ErrorMsg = oString.str();
		return false;
	}

	getline(infoFile, line);
	token.clear(); token.str(line); token >> DistanceToCenter;
	if (token.fail())
	{
		infoFile.close();
		InputError(InfoFileName, "Error reading Distance to Center", ErrorMsg);
		return false;
	}

	getline(infoFile, line);
	token.clear(); token.str(line); token >> ThetaResolution;
	if (token.fail())
	{
		infoFile.close();
		InputError(InfoFileName, "Error reading ThetaResolution", ErrorMsg);
		return false;
	}
	getline( infoFile, line);
	token.clear(); token.str(line); token >> numbInFile;
	if ( token.fail() )
	{
		infoFile.close();
		InputError(InfoFileName, "Error reading Number of Sections", ErrorMsg);
		return false;
	}
	SectionOrigins.resize(numbInFile);
	// std::clog << "\nNumb Arrays: " << numbInFile << std::endl;
	for (int n = 0 ; n < numbInFile ; n++ )
	{
		double x;
		double y;
		int idx;
		getline( infoFile, line);
		token.clear(); token.str(line); token >> idx >> x >> y  ;
		if ( token.fail() )
		{
			infoFile.close();
			InputError(InfoFileName, "Error reading Section Data", ErrorMsg);
			return false;
		} 
		SectionOrigins[n].Index = idx;
		SectionOrigins[n].Origin.x = (float)x;
		SectionOrigins[n].Origin.y = (float)y;
		// std::clog << "\t" << n << "\t" << idx << "\t" << x << "\t" << y << std::endl;
	}
	
	getline( infoFile, line);
	token.clear(); token.str(line); token >> numbInFile;
	if ( token.fail() )
	{
		infoFile.close();
		InputError(InfoFileName, "Error reading Number of Sub-Sections", ErrorMsg);
		return false;
	} 
	SubSectionOrigins.resize(numbInFile);
	SubSectionDimens.resize(numbInFile);
	SubSectionSpacing.resize(numbInFile);
	int idx;
	double x;
	double y;
	int w;
	int h;
	double xSp;
	double ySp;
	// std::clog << "\nNumb sections: " << numbInFile << std::endl;
	for (int n = 0 ; n < numbInFile ; n++ )
	{
		getline( infoFile, line);
		token.clear(); token.str(line); token >> idx >> x >> y >> w >> h >> xSp >> ySp; 
		if ( token.fail() )
		{
			infoFile.close();
			InputError(InfoFileName, "Error reading Sub-Section data", ErrorMsg);
			return false;
		} 
		SubSectionOrigins[n].Index = idx;
		SubSectionOrigins[n].Origin.x = (float)x;
		SubSectionOrigins[n].Origin.y = (float)y;
		SubSectionDimens[n].width = w;
		SubSectionDimens[n].height = h;
		SubSectionSpacing[n].x = (float)(xSp * (3.14159265359 / 180.0) /ThetaResolution);
		SubSectionSpacing[n].y = (float)ySp;
		// std::clog << "\t" << n << "\t" << idx << "\t" << x << "\t" << y << "\t" << w << "\t" << h << std::endl;
	}
	
	getline( infoFile, line);
	token.clear(); token.str(line); token >> numbInFile; 
	if ( token.fail() )
	{
		infoFile.close();
		InputError(InfoFileName, "Error reading Number of Theta Guards", ErrorMsg);
		return false;
	}
	TGuards.resize(numbInFile);
	// XGuardAveWidth = 0.0;
	// std::clog << "\nNumb TGuards: " << numbInFile << std::endl;
	for (int n = 0 ; n < numbInFile ; n++ )
	{
		int ii;
		int jj;
		int kk;
		double x;
		double y;
		getline( infoFile, line);
		token.clear(); token.str(line); token >> ii >> x >> y >> jj >> kk ;
		if ( token.fail() )
		{
			infoFile.close();
			InputError(InfoFileName, "Error reading Theta Guard data", ErrorMsg);
			return false;
		} 
		TGuards[n].Index = ii;
		TGuards[n].Width = x;
		TGuards[n].Location = y;
		TGuards[n].PrevArea = jj;
		TGuards[n].NextArea = kk;
		// XGuardAveWidth += x;
		// std::clog << "\t" << n << "\t" << ii << "\t" << x << "\t" << y << std::endl;
	}
	// XGuardAveWidth /= (double)numbInFile;
	
	getline( infoFile, line);
	token.clear(); token.str(line); token >> numbInFile;
	if ( token.fail() )
	{
		infoFile.close();
		InputError(InfoFileName, "Error reading Number of Radial Guards", ErrorMsg);
		return false;
	} 
	RGuards.resize(numbInFile);
	// std::clog << "\nNumb RGuards: " << numbInFile << std::endl;
	// YGuardAveWidth = 0.0;
	for (int n = 0 ; n < numbInFile ; n++ )
	{
		int ii;
		int jj;
		int kk;
		double x;
		double y;
		getline( infoFile, line);
		token.clear(); token.str(line); token >> ii >> x >> y >> jj >> kk ;
		if ( token.fail() )
		{
			infoFile.close();
			InputError(InfoFileName, "Error reading Radial Guard data", ErrorMsg);
			return false;
		} 
		RGuards[n].Index = ii;
		RGuards[n].Width = x;
		RGuards[n].Location = y;
		RGuards[n].PrevArea = jj;
		RGuards[n].NextArea = kk;
		// YGuardAveWidth += x;
		// std::clog << "\t" << n << "\t" << ii << "\t" << x << "\t" << y << std::endl;
	}
	// YGuardAveWidth /= (double)numbInFile;
	infoFile.close();
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// bool ReadSubImageData(std::string SubImageFileName, int NumbArrays, std::vector<ArraySection> &ArrSecList,
//		int &NumbArraysUsed, cv::Rect &ImageAnalysisArea, cv::Rect &SubImageArea,
// 		cv::Size2f &WellSize, double &WellArea, cv::Point2f &WellSpacing, //double &WellVolume,
// 		std::vector< ParameterLimit > &PLimits, double &BlockSizeMult, double &MinimumPeak,
// 		double &MaxFillingRatio, std::string &ErrorMsg )
//
// Reads SubImage file, which contains information about a particular subimage.  The information depends on the portion
// of the device in the image and the magnification.  
//          Input Arguments:
// SubImageFileName - string object with name of subimage file
// NumbArrays - Number of Arrays in device (not all of the arrays may be in image)
//          Output Arguments:
// ArrSecList - List of Array/Section pairs expected to be in image
// NumbArraysUsed - Number of different arrays in ArrSecList
// ImageAnalysisArea - 
// SubImageArea - Area of device of containing the Array/Section pairs of interest and excluding wells which are
//            not of interest in this image.  This is a cv::Rect variable with the x and y coordinates of the
//            upper left corner and the width and height of the rectangle.
// WellSize - Size in pixels of well
// WellArea - Area in pixel^2 of well
// WellSpacing - Center to Center distance between wells in same Section
// PLimits - Structure of information about each parameter used to locate image on device.  The four variable
//           parameters are (X-Offset, Y-Offset, Angle, Scale)  The offsets are translations and angle is the rotation
//           used to align the image with the device. The device is represented by an 8 bit greyscale error image which
//           is zero near center of each well and non-zero elsewhere.  The scale variable is used to adjust the position
//           of the blob centers because the magnification might be different between the image and the error image.
//           The value of the error image rises smoothly as the distance to the closest well center increases and is
//           larger in the guard regions.  Stage2Alignment and Stage3Alignment take a list of blob centers and shifts them
//           using the values of the parameters.  The error is then the sum of the error image values at the shifted
//           values of the blob centers.
//           The structure has five variables (Use, BaseValue, Limit, ErrorCoeff, ErrorExponent).  Use is a boolean, 
//           ErrorExponent is an integer, the others are doubles.  Three of these are entered here for each variable
//           parameter (BaseValue, Limit, and ErrorExponent).
//           BaseValue - Original estimate of the parameter.
//           Limit - The range of values for the parameter extends fom (BaseValue - Limit) to (BaseValue + Limit)
//           ErrorExponent - If the fitting program tries a value outside of the range described above, then a term
//           equal to (ErrorCoeff)(Difference^ErrorExponent) is added to the error if Use is true.  Difference is the
//           difference between the current trial value of the parameter and it BaseValue.
//           Use and ErrorCoeff are set by the program.
// BlockSizeMult - This is the block size used for adaptive thresholding
// MinimumPeak - The peak intensityof a blob found by adaptive thresholding is compared with the average intensity on the
//           blob's border.  If difference is less than MinimumPeak, then the blob is considered to be park of the 
//           background.
// MaxFillingRatio - If the filling ratio or filling fraction is greater than this number, then it is assumed that
//           the filling is anonmalous and the blob is ignored.
// ErrorMsg - If something goes wrong, then the procedure returns false and this will contain an error message.
//
bool ReadSectionData(std::string SectionInfoFileName, int &SectionOfInterest,
	cv::Rect &ImageAnalysisArea, cv::Rect &SubImageArea,
	cv::Size2f &WellSize, double &WellArea, cv::Point2f &WellRegionSize, //double &WellVolume,
	std::vector< ParameterLimit > &PLimits, double &BlockSizeMult, double &MinimumPeak,
	double &MaxFillingRatio, std::string &ErrorMsg )
{
	std::istringstream token;
	std::string line;
	std::ostringstream oString;
	//int numbInFile;
	int ii;
	int jj;
	int kk;
	int mm;
	double xx;
	double yy;
	PLimits.clear();
	ParameterLimit newPLimit;
	std::ifstream sectFile(SectionInfoFileName.c_str());
	if( !sectFile ) 
	{
		oString.str("");
		oString.clear();
		oString << "Error opening " << SectionInfoFileName;
		ErrorMsg = oString.str();
		return false;
	}
	
	getline( sectFile, line);
	token.clear(); token.str(line); token >> SectionOfInterest;
	if ( token.fail() || SectionOfInterest <= 0)
	{
		sectFile.close();
		InputError(SectionInfoFileName, "Error reading Section of Interest", ErrorMsg);
		return false;
	}
	// std::clog << "\nNumb Array/Section pairs: " << numbInFile << std::endl;

	getline( sectFile, line);
	token.clear(); token.str(line); token >> ii >> jj >> kk >> mm ;
	if ( token.fail() )
	{
		sectFile.close();
		InputError(SectionInfoFileName, "Error reading Image Analysis Area info", ErrorMsg);
		return false;
	} 
	ImageAnalysisArea.x = ii;
	ImageAnalysisArea.y = jj;
	ImageAnalysisArea.width = kk;
	ImageAnalysisArea.height = mm;

	getline( sectFile, line);
	token.clear(); token.str(line); token >> ii >> jj >> kk >> mm ;
	if ( token.fail() )
	{
		sectFile.close();
		InputError(SectionInfoFileName, "Error reading SubImage Area", ErrorMsg);
		return false;
	} 
	SubImageArea.x = ii;
	SubImageArea.y = jj;
	SubImageArea.width = kk;
	SubImageArea.height = mm;

	getline( sectFile, line);
	token.clear(); token.str(line); token >> xx >> yy  ;
	if ( token.fail() )
	{
		sectFile.close();
		InputError(SectionInfoFileName, "Error reading WellSize", ErrorMsg);
		return false;
	} 
	WellSize.width = (float)xx;
	WellSize.height = (float)yy;
	
	getline( sectFile, line);
	token.clear(); token.str(line); token >> xx;
	if ( token.fail() )
	{
		sectFile.close();
		InputError(SectionInfoFileName, "Error reading WellArea", ErrorMsg);
		return false;
	} 
	WellArea = xx;
	
	getline( sectFile, line);
	token.clear(); token.str(line); token >> xx >> yy;
	if ( token.fail() )
	{
		sectFile.close();
		InputError(SectionInfoFileName, "Error reading WellSpacing", ErrorMsg);
		return false;
	} 
	WellRegionSize.x = (float)xx;
	WellRegionSize.y = (float)yy;

	for ( int n = 0 ; n < NUMBPARAMETERS ; n++ )
	{
		getline( sectFile, line);
		token.clear(); token.str(line); token >> xx >> yy;
		if ( token.fail() )
		{
			sectFile.close();
			InputError(SectionInfoFileName, "Error reading Initial Info", n+1, ErrorMsg);
			return false;
		}
		newPLimit.BaseValue = xx;
		if ( yy <= 0 )
		{
			sectFile.close();
			InputError(SectionInfoFileName, "Bad Limit Value", n, ErrorMsg);
			return false;
		}
		newPLimit.Limit = yy;
	
		newPLimit.Use = true;			// Not currently used
		PLimits.push_back(newPLimit);
	}
	
	getline( sectFile, line);
	token.clear(); token.str(line); token >> xx;
	if ( token.fail() )
	{
		sectFile.close();
		InputError(SectionInfoFileName, "Error reading BlockSizeMult", ErrorMsg);
		return false;
	}
	if ( xx <= 0.0 )
	{
		sectFile.close();
		InputError(SectionInfoFileName, "Bad value of BlockSizeMult (x - double) ", ErrorMsg);
		return false;
	}
	BlockSizeMult = xx;
	
	getline( sectFile, line);
	token.clear(); token.str(line); token >> xx;
	if ( token.fail() )
	{
		sectFile.close();
		InputError(SectionInfoFileName, "Error reading MinimumPeak", ErrorMsg);
		return false;
	}
	if ( xx <= 0.0 )
	{
		sectFile.close();
		InputError(SectionInfoFileName, "Bad value of MinimumPeak (x - double) ", ErrorMsg);
		return false;
	}
	MinimumPeak = xx;
	
	getline( sectFile, line);
	token.clear(); token.str(line); token >> xx;
	if ( token.fail() )
	{
		sectFile.close();
		InputError(SectionInfoFileName, "Error reading Max Filling Ratio", ErrorMsg);
		return false;
	}
	if ( xx <= 0.0 )
	{
		sectFile.close();
		InputError(SectionInfoFileName, "Bad value of MaxFillingRatio (x - double) ", ErrorMsg);
		return false;
	}
	MaxFillingRatio = xx;
	
	sectFile.close();
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// int WriteDeviceInfo(std::string InfoFileName, 
//	std::vector< DeviceOrigins > &ArrayOrigins,
//	std::vector< DeviceOrigins > &SectionOrigins, std::vector< cv::Size > &SectionDimens,
//	std::vector< GuardInfo > &TGuards, std::vector< GuardInfo > &RGuards, 
//	std::string &ErrorMsg)
//
// Writes the information into an info file.  Is the reverse of ReadDeviceInfo() where the variable names have the
// same meaning.
//
bool WriteDeviceInfoCD(std::string InfoFileName,
	std::vector< DeviceOrigins > &SectionOrigins,
	std::vector< DeviceOrigins > &SubSectionOrigins, std::vector< cv::Size > &SubSectionDimens, std::vector<cv::Point2f>& SubSectionSpacing,
	std::vector< GuardInfo > &TGuards, std::vector< GuardInfo > &RGuards, 
	std::string &ErrorMsg)
{
	//! [load]
	std::ofstream outFile;
	std::ostringstream oString;
	outFile.open(InfoFileName.c_str());
	if ( !outFile.is_open() )
	{
		oString.str("");
		oString.clear();
		oString << "Problems opening new info file: " << InfoFileName ;
		std::clog << oString.str();
		ErrorMsg = oString.str();
		return false;
	}
	std::istringstream token;
	std::string line;

	int numbSections = (int)SectionOrigins.size();
	outFile << numbSections << "\n";
	for ( int n = 0 ; n < numbSections ; n++ )
	{
		outFile << SectionOrigins[n].Index << "\t" << SectionOrigins[n].Origin.x << "\t" << SectionOrigins[n].Origin.y << "\n";
	}
	int numbSubSections = (int)SubSectionOrigins.size();
	outFile << numbSubSections << "\n";
	for ( int n = 0 ; n < numbSubSections ; n++ )
	{
		outFile << SubSectionOrigins[n].Index << "\t" << SubSectionOrigins[n].Origin.x << "\t" << SubSectionOrigins[n].Origin.y
			<< "\t" << SubSectionDimens[n].width << "\t" << SubSectionDimens[n].height 
			<< "\t" << SubSectionSpacing[n].x << "\t" << SubSectionSpacing[n].y
			<< "\n";
	}
	int numbXGuards = (int)TGuards.size();
	outFile << numbXGuards << "\n";
	for ( int n = 0 ; n < numbXGuards ; n++ )
	{
		outFile << TGuards[n].Index << "\t" << TGuards[n].Width << "\t" << TGuards[n].Location
			<< "\t" << TGuards[n].PrevArea << "\t" << TGuards[n].NextArea << "\n";
	}
	int numbYGuards = (int)RGuards.size();
	outFile << numbYGuards << "\n";
	for ( int n = 0 ; n < numbYGuards ; n++ )
	{
		outFile << RGuards[n].Index << "\t" << RGuards[n].Width << "\t" << RGuards[n].Location
			<< "\t" << RGuards[n].PrevArea << "\t" << RGuards[n].NextArea << "\n";
	}
	
	outFile.close();
	return true;
}



