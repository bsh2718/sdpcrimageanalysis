#pragma once
//! [includes]

#include <cstdlib>
#include <vector>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <ostream>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "blob.hpp"
#include "deviceOrigins.hpp"
#include "readinfofile.hpp"
#include "searchVolumeCD2.hpp"

struct GridGuardScores
{
	int GuardScore;
	cv::Point Location;
	double Theta;
	double Scale;

	GridGuardScores()
	{
		GuardScore = 1000000;
		Location = cv::Point(0, 0);
		Theta = 0.0;
		Scale = 1.0;
	}
	GridGuardScores(int Gs, cv::Point Loc, double Th, double Sc)
	{
		GuardScore = Gs;
		Location = Loc;
		Theta = Th;
		Scale = Sc;
	}
	std::string ToString()
	{
		std::ostringstream oString;
		oString.str("");
		oString.clear();
		oString << GuardScore << ", " << Location.y << ", " << Location.x << ", " << Theta << ", " << Scale;
		return oString.str();
	}
};
class SearchGrid
{
public:
	std::vector< ParameterLimit > PLimits;
	cv::Size ImageSize;
	std::vector<cv::Point2f> WellSpacing;
	cv::Size2f WellSize;
	int SectionOfInterest;
	cv::Mat WellImage;
	cv::Size WellImageSize;
	std::vector< std::vector< SearchVolume > > XYGrid;
	cv::Point GridSpacing;
	cv::Size GridSize;
	cv::Point GridCenter;
	std::vector< cv::Point > DeltaList;
	bool Initialized;
	SearchGrid(std::vector< ParameterLimit > ParamLim,
		cv::Size ImageSz, std::vector<cv::Point2f> WellSp, cv::Size2f WellSz, int SecOfInterest, cv::Mat &WellTemplate)
		: PLimits(ParamLim), ImageSize(ImageSz),
			WellSpacing(WellSp), WellSize(WellSz), SectionOfInterest(SecOfInterest), Initialized(false)
	{
		DeltaList.resize(8);
		DeltaList[0] = cv::Point(0, 1);
		DeltaList[1] = cv::Point(1, 1);
		DeltaList[2] = cv::Point(1, 0);
		DeltaList[3] = cv::Point(1, -1);
		DeltaList[4] = cv::Point(0, -1);
		DeltaList[5] = cv::Point(-1, -1);
		DeltaList[6] = cv::Point(-1, 0);
		DeltaList[7] = cv::Point(-1, 1);
	}
	~SearchGrid(){}

	bool Initialize(std::ofstream& LogFile, std::string BaseName, 
		std::vector<Blob>& BlobList, cv::Size ImageSize, bool TrackAlignment, std::string &ErrMsg);

	bool AdjustBounds(std::vector< double >& Results, ParameterBounds& Bounds);

	bool GridSearchGuard(std::ofstream& LogFile, std::string BaseName,
		ParameterBounds& ThetaBounds, ParameterBounds& ScaleBounds,
		LocalScore &BestGuardScore, std::vector<LocalScore>& GuardResults, bool TrackAlignment);

	bool GridSearch(std::ofstream& LogFile, std::string BaseName,
		ParameterBounds& ThetaBounds, ParameterBounds& ScaleBounds, int GuardPenalty,
		std::vector<LocalScore> GuardResults, SearchVolumeResult& BestSearchResult, bool TrackAlignment);




};

