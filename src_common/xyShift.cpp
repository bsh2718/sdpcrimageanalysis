//////////////////////////////////////////////////////////////////////////////////
// void XYShift( std::ofstream &LogFile, double ScaleIn, int Step,
//		std::vector< Blob > &BlobList, std::vector< std::vector< int > > &ArrSecMatrix,
//		cv::Mat &ArrSecImage, std::vector< ParameterLimit > PLimits, 
//		std::vector< double > &Results, bool TrackAlignment, std::string &ErrMsg )
//
// Takes the geometric centers of the blobs in BlobList and shifts then in X and Y
// to produce the best alignment of their coordinates with non zero regions in
// ArrSecImage.  The procedure assumes that the image has been corrected for angular
// offset and uses RotatedCenter2f of each blob as its center.
// ArrSecImage contains rectangles, each of which encloses all of the wells of a 
// particular Array/Section.  Best alignment means the X and Y offsets which 
// maximize the number of blobs coordinates in one of the rectangles. 
// The procedure iterates the values of the X and Y offsets over the range specified
// in PLimits. The step size for this iteration is given by Step.
//
//     input
// LogFile - reference to log file
// ScaleIn - Multiplicative scale factor to be applied to coordinates of blob.  This
//		procedure does not vary the scale factor.
// Step - Step size for iterating the X and Y offsets ( += Step ) when finding the
//		best value of the offsets.
// BlobList - vector of <Blob>
// ArrSecMatrix - 2D vector of int. Dimensions are (NumbArrays + 1) by (NumbSections + 1)
//      If a particular Array/Section pair is to be analyzed, then value of ArrSecMatrix
//      is 1, otherwise it is 0. (Arrays and Sections are numbered starting at 1, so
//      the elements where either index is 0 are not used.
// ArrSecImage - Eight bit image which is non zero in regions which enclose the wells
//		of a particular array/section.  The boundaries of the rectangle extend to the
//		outside edges of the wells on the edges of a particular array/section.
// PLimits - vector of limit information for the offsets. (Also anlge and scale, though
//		those are not used in this procedure.)  The offsets are varied over the ranges
//
// X: PLimits[0].BaseValue-PLimits[0].Limit) to (PLimits[0].BaseValue+PLimits[0].Limit)
// Y: PLimits[1].BaseValue-PLimits[1].Limit) to (PLimits[1].BaseValue+PLimits[1].Limit)
//
//		in steps of Step.
//
//     output
// Results - vector<double> (X-Offset, Y-Offset, not used, Scale ).  The and X and Y 
//		Offset are determined here.  Scale is set to ScaleIn.  the third item, which
//		corresponds to the angle is not touched here.
// TrackAlignment - If true, extra output is printed to LogFile.
// ErrMsg - Error message
//
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "statistics.hpp"
#include "blob.hpp"
#include "readinfofile.hpp"
// #include "writeBlobFile.hpp"

#include "xyShift.hpp"


void XYShift( std::ofstream &LogFile, double ScaleIn, int Step,
		std::vector< Blob > &BlobList, std::vector< std::vector< int > > &ArrSecMatrix,
		cv::Mat &ArrSecImage, std::vector< ParameterLimit > PLimits, 
		std::vector< double > &Results, bool TrackAlignment, std::string &ErrMsg )
{
	cv::Size imageSize = ArrSecImage.size();
	int colLimit = imageSize.width - 1;
	int rowLimit = imageSize.height - 1;
	int row = (int)(PLimits[1].BaseValue);
	int maxCol = (int)round(PLimits[0].BaseValue);
	int maxRow;
	int maxRowBelow = 1 ;
	int maxRowAbove = rowLimit;
	int maxAligned = 0;
	int numbBlobs = (int)BlobList.size();
	for ( int b = 0 ; b < numbBlobs ; b++ )
	{
		int rCheck = row + (int)round(BlobList[b].AdjustedCenter2f.y * ScaleIn);
		if ( rCheck < 1 && rCheck < maxRowBelow )
			maxRowBelow = rCheck;
		else if ( rCheck > rowLimit && rCheck > maxRowAbove )
			maxRowAbove = rCheck;
	}
	if ( maxRowBelow < 1 )
		row += ( 1 - maxRowBelow );
	else if ( maxRowAbove > rowLimit )
		row += ( rowLimit - maxRowAbove );
	if ( TrackAlignment )
		LogFile << "Approx. alignment Col\n ";
	cv::Point tmpPt;
	cv::Point offset = cv::Point((int)PLimits[0].BaseValue, row );
	maxRow = row;
	if ( TrackAlignment )
	{
		LogFile << "   offset = " << offset << " - ScaleIn = " << ScaleIn << " - Step = " << Step << "\n";
		LogFile << "Col\tAlig\tMaxCol\tMaxAlig\tScale\tOffset\n";
	}
	cv::Point shiftedTenthPt = cv::Point(0,0);
	for ( int b = 0 ; b < numbBlobs ; b++ )
	{
		tmpPt.x = (int)round(BlobList[b].AdjustedCenter2f.x * ScaleIn + offset.x);
		if ( tmpPt.x > 0 && tmpPt.x < colLimit )
		{
			tmpPt.y = (int)round(BlobList[b].AdjustedCenter2f.y * ScaleIn + offset.y);
			if ( tmpPt.y > 0 && tmpPt.y < rowLimit )
			{
				int gValue = ArrSecImage.at<uchar>(tmpPt);
				if ( gValue > 0 )
				{
					int s = (int)floor(gValue/10);
					int a = gValue % 10;
					if ( ArrSecMatrix[a][s] > 0 )
						maxAligned++;
				}
			}
		}
	}
	int lastMax = maxCol;
	for ( int n = 1; n < PLimits[0].Limit ; n += Step )
	{
		int c = (int)PLimits[0].BaseValue + n;
		offset = cv::Point(c, row);
		int aligned = 0;
		for ( int b = 0 ; b < numbBlobs ; b++ )
		{
			tmpPt.x = (int)round(BlobList[b].AdjustedCenter2f.x * ScaleIn + offset.x);
			if ( tmpPt.x > 0 && tmpPt.x < colLimit )
			{
				tmpPt.y = (int)round(BlobList[b].AdjustedCenter2f.y * ScaleIn + offset.y);
				if ( tmpPt.y > 0 && tmpPt.y < rowLimit )
				{
					int gValue = ArrSecImage.at<uchar>(tmpPt);
					if ( gValue > 0 )
					{
						int s = (int)floor(gValue/10);
						int a = gValue % 10;
						if ( ArrSecMatrix[a][s] > 0 )
							aligned++;
					}
				}
			}
		}
		if ( aligned > maxAligned )
		{
			maxAligned = aligned;
			maxCol = c;
			lastMax = c;
		}
		else if ( aligned == maxAligned )
		{
			lastMax = c;
		}
		if ( TrackAlignment )
		{
			LogFile << c << "\t" << aligned << "\t" << maxCol << "\t" << maxAligned
				<< "\t" << ScaleIn << "\t" << offset << "\n";
		}
		
		c = (int)PLimits[0].BaseValue - n;
		offset = cv::Point(c, row);
		aligned = 0;
		for ( int b = 0 ; b < numbBlobs ; b++ )
		{
			tmpPt.x = (int)round(BlobList[b].AdjustedCenter2f.x * ScaleIn + offset.x);
			if ( tmpPt.x > 0 && tmpPt.x < colLimit )
			{
				tmpPt.y = (int)round(BlobList[b].AdjustedCenter2f.y * ScaleIn + offset.y);
				if ( tmpPt.y > 0 && tmpPt.y < rowLimit )
				{
					int gValue = ArrSecImage.at<uchar>(tmpPt);
					if ( gValue > 0 )
					{
						int s = (int)floor(gValue/10);
						int a = gValue % 10;
						if ( ArrSecMatrix[a][s] > 0 )
							aligned++;
					}
				}
			}
		}
		if ( aligned > maxAligned )
		{
			maxAligned = aligned;
			maxCol = c;
			lastMax = c;
		}
		else if ( aligned == maxAligned )
		{
			lastMax = c;
		}
		if ( TrackAlignment )
		{
			LogFile << c << "\t" << aligned << "\t" << maxCol << "\t" << maxAligned 
				<< "\t" << ScaleIn << "\t" << offset << "\n";
		}
	}
	/*if ( lastMax > maxCol )
		maxCol = ( 2 * maxCol + lastMax ) / 3;
	else
		maxCol = ( maxCol + 2 * lastMax ) / 3;*/
	LogFile << "XYShift: maxCol = " << maxCol << "\n";
	LogFile << "Approx. alignment Row\n";
	lastMax = row;
	if ( TrackAlignment )
	{
		LogFile << "   offset = " << offset << " - ScaleIn = " << ScaleIn << " - Step = " << Step << "\n";
		LogFile << "Row\tAlig\tMaxRow\tMaxAlig\tScale\tOffset\n";
	}
	for ( int n = 2; n < PLimits[1].Limit ; n += Step )
	{
		int r = (int)PLimits[1].BaseValue + n;
		offset = cv::Point(maxCol, r);
		int aligned = 0;
		
		for ( int b = 0 ; b < numbBlobs ; b++ )
		{
			tmpPt.x = (int)round(BlobList[b].AdjustedCenter2f.x * ScaleIn + offset.x);
			tmpPt.y = (int)round(BlobList[b].AdjustedCenter2f.y * ScaleIn + offset.y);
			if ( tmpPt.y > 0 && tmpPt.y < rowLimit && tmpPt.x > 0 && tmpPt.x < colLimit )
			{
				int gValue = ArrSecImage.at<uchar>(tmpPt);
				if ( gValue > 0 )
				{
					int s = (int)floor(gValue/10);
					int a = gValue % 10;
					if ( ArrSecMatrix[a][s] > 0 )
						aligned++;
				}
			}
		}
		if ( aligned > maxAligned )
		{
			maxAligned = aligned;
			maxRow = r;
			lastMax = r;
		}
		else if ( aligned == maxAligned )
		{
			lastMax = r;
		}
		if ( TrackAlignment )
		{
			LogFile << r << "\t" << aligned << "\t" << maxRow << "\t" << maxAligned 
				<< "\t" << ScaleIn << "\t" << offset << "\n";
		}
		r = (int)PLimits[1].BaseValue - n;
		offset = cv::Point(maxCol, r);
		aligned = 0;
		for ( int b = 0 ; b < numbBlobs ; b++ )
		{
			tmpPt.x = (int)round(BlobList[b].AdjustedCenter2f.x * ScaleIn + offset.x);
			tmpPt.y = (int)round(BlobList[b].AdjustedCenter2f.y * ScaleIn + offset.y);
			if ( tmpPt.y > 0 && tmpPt.y < rowLimit && tmpPt.x > 0 && tmpPt.x < colLimit )
			{
				int gValue = ArrSecImage.at<uchar>(tmpPt);
				if ( gValue > 0 )
				{
					int s = (int)floor(gValue/10);
					int a = gValue % 10;
					if ( ArrSecMatrix[a][s] > 0 )
						aligned++;
				}
			}
		}
		if ( aligned > maxAligned )
		{
			maxAligned = aligned;
			maxRow = r;
			lastMax = r;
		}
		else if ( aligned == maxAligned )
		{
			lastMax = r;
		}
		if ( TrackAlignment )
		{
			LogFile << r << "\t" << aligned << "\t" << maxRow << "\t" << maxAligned
				<< "\t" << ScaleIn << "\t" << offset << "\n";
		}
	}
	LogFile << " lastMax/maxRow = " << lastMax << "/" << maxRow << "\n";
	if ( lastMax > maxRow )
		maxRow = ( 2 * maxRow + lastMax )/3;
	else if ( lastMax < maxRow )
		maxRow = ( maxRow + 2 * lastMax )/3;

	int chkAligned = 0;
	for (int b = 0; b < numbBlobs; b++)
	{
		tmpPt.x = (int)round(BlobList[b].AdjustedCenter2f.x * ScaleIn + maxCol);
		tmpPt.y = (int)round(BlobList[b].AdjustedCenter2f.y * ScaleIn + maxRow);
		if (tmpPt.y > 0 && tmpPt.y < rowLimit && tmpPt.x > 0 && tmpPt.x < colLimit)
		{
			int gValue = ArrSecImage.at<uchar>(tmpPt);
			if (gValue > 0)
			{
				int s = (int)floor(gValue / 10);
				int a = gValue % 10;
				BlobList[b].Array = a;
				BlobList[b].Section = s;
				chkAligned++;
			}
		}
	}
	LogFile << "XYShift: maxRow = " << maxRow << "\n";
	LogFile << "Numb blobs = " << BlobList.size() << ", maxAligned = " << maxAligned << ", chkAligned = " << chkAligned << std::endl;
	LogFile << "maxCol/Row = " << maxCol << " / " << maxRow << std::endl;
	Results[0] = maxCol;
	Results[1] = maxRow;
	Results[3] = ScaleIn;
}
