#ifndef WRITEBLOBFILE_HPP
#define WRITEBLOBFILE_HPP
//! [includes]

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <string>
#include <sstream>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>

//! [includes]

#include "blob.hpp"
#include "readinfofile.hpp"

//
// Group of procedures to output information from BlobList to a file called BlobFileName.  The different
// procedures sort the blobs based on different values in the elements of BlobList and output different
// values.
//

bool WriteBlobListInput(std::string BlobFileName, std::vector<Blob> &BlobList, double HalfWellHeight, std::string &ErrorMsg );

bool WriteBlobListRotated(std::string BlobFileName, std::vector<Blob> &BlobList, double HalfWellHeight, std::string &ErrorMsg );

bool WriteBlobListDevice(std::string BlobFileName, std::vector<Blob> &BlobList, double HalfWellHeight, int SectionOfInterest,
	std::string &ErrorMsg );
	
// bool WriteBlobListShifted(std::string BlobFileName, std::vector<Blob> &BlobList, double HalfWellHeight,
	// std::string &ErrorMsg );

#endif // WRITEBLOBFILE_HPP

