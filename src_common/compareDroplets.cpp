/////////////////////////////////////////////////////////////////////////////////////////
//
// void AssignStatus(std::ofstream &LogFile, int Array, int Section, cv::Size SectionDimens,
//	std::vector< WellExclusionRules > ExclusionRules, 
//	std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter)
//
// void CalcAfterBeforeComparisons(std::ofstream &LogFile, int Array, int Section, cv::Size SectionDimens,
//	double ShrinkageLimit, std::vector< BlobSummary > &BeforeSizingList, std::vector< BlobSummary > &AfterSizingList, 
//	std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter)
//
// int CalcBeforeAveIntenDistribution(std::ofstream &LogFile, int Array, int Section, cv::Size SectionDimens,
//	std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter,
//	std::vector< BlobSummary > &BeforeSizingList, double BeforeExclusionFactor, 
//	std::vector< std::pair< double, int > > &AveIntenHistogram, Statistics1 &AveIntenStats, 
//	double &AveIntenThreshold, int &NumbAcceptable )
//
// int CalcAfterBeforeRatioDistribution(std::ofstream &LogFile, int Array, int Section, cv::Size SectionDimens,	
//	std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter,
//	double PosNegRatio, double RatioDefaultDivider,
//	std::vector< std::pair< double, int > > &RatioHistogram, Statistics1 &RatioStats, 
//	Statistics1 &BelowStats, Statistics1 &AboveStats, AfterBeforeResults &RatioResults)
//
// bool OutputDropletComparisonImages(std::ofstream &LogFile, std::string BaseName,
//	int Array, int Section, int ImageNumber, double Time, cv::Size SectionDimens,
//	std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter,
//	std::vector< BlobSummary > &BeforeSizingList, 
//	std::vector< BlobSummary > &AfterSizingList, 
//	cv::Mat &BeforeImage, cv::Mat &BeforeMask, cv::Mat &BeforeContourMask, cv::Mat &BeforeMap, cv::Mat &BeforeArrayCheck,
//	cv::Mat &AfterImage, cv::Mat &AfterMask, cv::Mat &AfterContourMask, cv::Mat &AfterMap, cv::Mat &AfterArrayCheck,
//	cv::Point WellSpacing2f, bool CreateMap, std::vector< std::string > &MapText, 
//	bool OutputImages, std::string &ErrorMsg)
//
// bool OutputDropletRatioData(std::ofstream &LogFile, std::string &BaseName, 
//	int Array, int Section, int ImageNumber, double Time, cv::Size SectionDimens, double PosNegRatio, double RatioDefaultDivider,
//	std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter,
//	std::vector< BlobSummary > &BeforeSizingList, 
//	std::vector< std::pair< double, int > > &RatioHistogram, Statistics1 &RatioStats, 
//	Statistics1 &BelowStats, Statistics1 &AboveStats, AfterBeforeResults &RatioResults)
//
// bool OutputDropletAveIntensityData(std::ofstream &LogFile, std::string &BaseName, 
//	int Array, int Section, cv::Size SectionDimens, 
//	std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter,
//	std::vector< BlobSummary > &BeforeSizingList, std::vector< double> BeforeResults, double BeforeThreshold, 
//	MinMaxGroup &BeforeParameters, double BeforeExclusionFactor, 
//	std::vector< std::pair< double, int > > &AveIntenHistogram, Statistics1 &AveIntenStats, 
//	double AveIntenThreshold, int NumbAcceptableBeforeDroplets)
//
// bool OutputDropletComparison(std::ofstream &LogFile, std::string BaseName,
//	int Array, int Section, int ImageNumber, double Time, cv::Size SectionDimens, double WellArea, cv::Size2f WellSize2f,
//	std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter,
//	std::vector< BlobSummary > &BeforeSizingList, std::vector< double> BeforeResults, double BeforeThreshold, 
//	MinMaxGroup &BeforeParameters, double ShrinkageLimit, double BeforeExclusionFactor, 
//	std::vector< BlobSummary > &AfterSizingList, std::vector< double> AfterResults, double AfterThreshold, 
//	MinMaxGroup &AfterParameters, double PosNegRatio, double RatioDefaultDivider, double RunSigmaMultiplier, 
//	AfterBeforeResults &RatioResults, RunInformation &RunInfo,
//	cv::Point ArraySectionShift, QuantitationResult &QResult, std::string &ErrorMsg )
//
// bool TrimRectangle(cv::Rect &Rectangle, cv::Size RectSize, cv::Size ImageSize )
//
// Internal subroutines
//
// void ApplyOtsuThreshold(std::ofstream &LogFile, std::vector< double > Data,
//		std::vector< std::pair< double, int > > &RatioHistogram, int MaxBin, double RatioDefaultDivider, double PosNegRatio,
//		AfterBeforeResults &RatioResults, Statistics1 &BelowStats, Statistics1 &AboveStats, 
//		int &BelowMaxCount, int &BelowMaxIdx, int &AboveMaxCount, int &AboveMaxIdx )//
//
// void SetUpMaskSymbols(std::vector< cv::Vec3b > &LineColors, std::vector< std::vector<cv::Point> > &Symbols )
//
// void CreateDropletImageStrings(int BeforeIndex, int AfterIndex, 
//	BlobShapeSummary &BeforeShape, BlobShapeSummary &AfterShape, 
//	BeforeAfterMatrix &CurrentElement, 
//	std::string &BeforeResultString, std::string &AfterResultString )
//
// void CreateDropletBeforeImageStrings(int BeforeIndex, 
//	BlobShapeSummary &BeforeShape, 
//	BeforeAfterMatrix &CurrentElement, 
//	std::string &BeforeResultString )
//
// void CreateDropletAfterImageStrings(int AfterIndex, 
//	BlobShapeSummary &AfterShape, 
//	BeforeAfterMatrix &CurrentElement, 
//	std::string &AfterResultString )
//
// 
//
//
//
//! [includes]
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "defines.h"
#include "physicalconstants.h"
#include "statistics.hpp"
#include "blob.hpp"
#include "readinfofile.hpp"
#include "wellExclusionRules.hpp"
#include "dropletMatch.hpp"
// #include "segmentVector.hpp"
#include "OtsuThreshold.hpp"
#include "analysisParameters.hpp"
#include "beforeAfterMatrix.hpp"
#include "ww.hpp"
#include "droplet.hpp"
#include "calcConcentration.hpp"
#include "compareDroplets.hpp"
#include "trimRectangle.hpp"
//! [includes]

//! [namespace]
// using namespace std;
//using namespace cv;
//! [namespace]
void CreateDropletImageStrings(int BeforeIndex, int AfterIndex, 
	BlobShapeSummary &BeforeShape, BlobShapeSummary &AfterShape, 
	BeforeAfterMatrix &CurrentElement, 
	std::string &BeforeResultString, std::string &AfterResultString );
	
void CreateDropletAfterImageStrings(int AfterIndex, 
	BlobShapeSummary &AfterShape, 
	BeforeAfterMatrix &CurrentElement, 
	std::string &AfterResultString );

void CreateDropletBeforeImageStrings(int BeforeIndex, 
	BlobShapeSummary &BeforeShape, 
	BeforeAfterMatrix &CurrentElement, 
	std::string &BeforeResultString );	
	
int BoolToInt( bool Boolean )
{
	if ( Boolean )
		return true;
	else
		return false;
}
				
void AssignStatus(std::ofstream &LogFile, int Array, int Section, cv::Size SectionDimens,
	std::vector< WellExclusionRules > ExclusionRules, 
	std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter)
{
	WellExclusionRules currentRules = ExclusionRules[0];
	if ( ExclusionRules.size() > 1 )
	{
		for ( int n = 1 ; n < ExclusionRules.size() ; n++ )
		{
			if ( Array == ExclusionRules[n].Array )
			{
				if ( Section == ExclusionRules[n].Section || ExclusionRules[n].Section == 0 )
				{
					currentRules = ExclusionRules[n];
					break;
				}
			}
			else if ( ExclusionRules[n].Array == 0 && ExclusionRules[n].Section == Section )
			{
				currentRules = ExclusionRules[n];
				break;
			}
		}
	}
	int sectionWidth = SectionDimens.width;
	int sectionHeight = SectionDimens.height;
	for ( int j = 1 ; j <= sectionHeight ; j++ )
	{
		BeforeAfter[Array][Section][j][1].BeforeEdgeStatus = currentRules.LeftCol;
		BeforeAfter[Array][Section][j][1].AfterEdgeStatus = currentRules.LeftCol;
		BeforeAfter[Array][Section][j][sectionWidth].BeforeEdgeStatus = currentRules.RightCol;
		BeforeAfter[Array][Section][j][sectionWidth].AfterEdgeStatus = currentRules.RightCol;
	
		if ( currentRules.ExcludeSecondIfFirstMissing > 0 )
		{
			int jStart = j - 2;
			int jEnd = j + 2;
			if ( jStart < 1 )
				jStart = 1;
			if ( jEnd > sectionHeight )
				jEnd = sectionHeight;
			
			bool excludeSecond = true;
			if ( BeforeAfter[Array][Section][j][1].BeforeIndex < 0 && currentRules.LeftCol > 0 )
			{
				for ( int jj = jStart ; jj <= jEnd ; jj++ )
				{
					if ( BeforeAfter[Array][Section][j][1].BeforeIndex >= 0 )
					{
						excludeSecond = false;
						break;
					}
				}
				if ( excludeSecond )
					BeforeAfter[Array][Section][j][1].BeforeEdgeStatus += currentRules.ExcludeSecondIfFirstMissing;
			}
			excludeSecond = true;
			if ( BeforeAfter[Array][Section][j][sectionWidth].BeforeIndex < 0 && currentRules.RightCol > 0 )
			{
				for ( int jj = jStart ; jj <= jEnd ; jj++ )
				{
					if ( BeforeAfter[Array][Section][j][sectionWidth].BeforeIndex >= 0 )
					{
						excludeSecond = false;
						break;
					}
				}
				if ( excludeSecond )
					BeforeAfter[Array][Section][j][sectionWidth].BeforeEdgeStatus += currentRules.ExcludeSecondIfFirstMissing;
			}
			excludeSecond = true;
			if ( BeforeAfter[Array][Section][j][1].AfterIndex < 0 && currentRules.LeftCol > 0 )
			{
				for ( int jj = jStart ; jj <= jEnd ; jj++ )
				{
					if ( BeforeAfter[Array][Section][j][1].AfterIndex >= 0 )
					{
						excludeSecond = false;
						break;
					}
				}
				if ( excludeSecond )
					BeforeAfter[Array][Section][j][1].AfterEdgeStatus += currentRules.ExcludeSecondIfFirstMissing;
			}
			excludeSecond = true;
			if ( BeforeAfter[Array][Section][j][sectionWidth].AfterIndex < 0 && currentRules.RightCol > 0 )
			{
				for ( int jj = jStart ; jj <= jEnd ; jj++ )
				{
					if ( BeforeAfter[Array][Section][j][sectionWidth].AfterIndex >= 0 )
					{
						excludeSecond = false;
						break;
					}
				}
				if ( excludeSecond )
					BeforeAfter[Array][Section][j][sectionWidth].AfterEdgeStatus += currentRules.ExcludeSecondIfFirstMissing;
			}	BeforeAfter[Array][Section][j][sectionWidth].AfterEdgeStatus += currentRules.ExcludeSecondIfFirstMissing;
		}
	}
	
	for ( int k = 1 ; k <= sectionWidth ; k++ )
	{
		BeforeAfter[Array][Section][1][k].BeforeEdgeStatus = currentRules.TopRow;
		BeforeAfter[Array][Section][1][k].AfterEdgeStatus = currentRules.TopRow;
		BeforeAfter[Array][Section][sectionHeight][k].BeforeEdgeStatus = currentRules.BotRow;
		BeforeAfter[Array][Section][sectionHeight][k].AfterEdgeStatus = currentRules.BotRow;
	
		if ( currentRules.ExcludeSecondIfFirstMissing > 0 )
		{
			int kStart = k - 2;
			int kEnd = k + 2;
			if ( kStart < 1 )
				kStart = 1;
			if ( kEnd > sectionWidth )
				kEnd = sectionWidth;
			
			bool excludeSecond = true;
			if ( BeforeAfter[Array][Section][1][k].BeforeIndex < 0 && currentRules.TopRow > 0 )
			{
				for ( int kk = kStart ; kk <= kEnd ; kk++ )
				{
					if ( BeforeAfter[Array][Section][1][k].BeforeIndex >= 0 )
					{
						excludeSecond = false;
						break;
					}
				}
				if ( excludeSecond )
					BeforeAfter[Array][Section][1][k].BeforeEdgeStatus += currentRules.ExcludeSecondIfFirstMissing;
			}
			excludeSecond = true;
			if ( BeforeAfter[Array][Section][sectionHeight][k].BeforeIndex < 0 && currentRules.BotRow > 0 )
			{
				for ( int kk = kStart ; kk <= kEnd ; kk++ )
				{
					if ( BeforeAfter[Array][Section][sectionHeight][k].BeforeIndex >= 0 )
					{
						excludeSecond = false;
						break;
					}
				}
				if ( excludeSecond )
					BeforeAfter[Array][Section][sectionHeight][k].BeforeEdgeStatus += currentRules.ExcludeSecondIfFirstMissing;
			}
			excludeSecond = true;
			if ( BeforeAfter[Array][Section][1][k].AfterIndex < 0 && currentRules.TopRow > 0 )
			{
				for ( int kk = kStart ; kk <= kEnd ; kk++ )
				{
					if ( BeforeAfter[Array][Section][1][k].AfterIndex >= 0 )
					{
						excludeSecond = false;
						break;
					}
				}
				if ( excludeSecond )
					BeforeAfter[Array][Section][1][k].AfterEdgeStatus += currentRules.ExcludeSecondIfFirstMissing;
			}
			excludeSecond = true;
			if ( BeforeAfter[Array][Section][sectionHeight][k].AfterIndex < 0 && currentRules.BotRow > 0 )
			{
				for ( int kk = kStart ; kk <= kEnd ; kk++ )
				{
					if ( BeforeAfter[Array][Section][sectionHeight][k].AfterIndex >= 0 )
					{
						excludeSecond = false;
						break;
					}
				}
				if ( excludeSecond )
					BeforeAfter[Array][Section][sectionHeight][k].AfterEdgeStatus += currentRules.ExcludeSecondIfFirstMissing;
			}	BeforeAfter[Array][Section][sectionHeight][k].AfterEdgeStatus += currentRules.ExcludeSecondIfFirstMissing;
		}
	}
	int maxStatus = 0;
	if ( currentRules.Corner > 0 )
	{
		int cornerRule =  currentRules.Corner + currentRules.ExcludeSecondIfFirstMissing;
		if ( BeforeAfter[Array][Section][1][1].BeforeEdgeStatus < cornerRule )
			BeforeAfter[Array][Section][1][1].BeforeEdgeStatus = cornerRule;
		
		if ( BeforeAfter[Array][Section][1][sectionWidth].BeforeEdgeStatus < cornerRule )
			BeforeAfter[Array][Section][1][sectionWidth].BeforeEdgeStatus = cornerRule;
		
		if ( BeforeAfter[Array][Section][sectionHeight][1].BeforeEdgeStatus < cornerRule )
			BeforeAfter[Array][Section][sectionHeight][1].BeforeEdgeStatus = cornerRule;
		
		if ( BeforeAfter[Array][Section][sectionHeight][sectionWidth].BeforeEdgeStatus < cornerRule )
			BeforeAfter[Array][Section][sectionHeight][sectionWidth].BeforeEdgeStatus = cornerRule;
		
		if ( BeforeAfter[Array][Section][1][1].AfterEdgeStatus < cornerRule )
			BeforeAfter[Array][Section][1][1].AfterEdgeStatus = cornerRule;
		
		if ( BeforeAfter[Array][Section][1][sectionWidth].AfterEdgeStatus < cornerRule )
			BeforeAfter[Array][Section][1][sectionWidth].AfterEdgeStatus = cornerRule;
		
		if ( BeforeAfter[Array][Section][sectionHeight][1].AfterEdgeStatus < cornerRule )
			BeforeAfter[Array][Section][sectionHeight][1].AfterEdgeStatus = cornerRule;
		
		if ( BeforeAfter[Array][Section][sectionHeight][sectionWidth].AfterEdgeStatus < cornerRule )
			BeforeAfter[Array][Section][sectionHeight][sectionWidth].AfterEdgeStatus = cornerRule;
		
		maxStatus = cornerRule;
	}
	if ( currentRules.LeftCol > maxStatus )
		maxStatus = currentRules.LeftCol;
	if ( currentRules.LeftCol > maxStatus )
		maxStatus = currentRules.LeftCol;
	if ( currentRules.RightCol > maxStatus )
		maxStatus = currentRules.RightCol;
	if ( currentRules.TopRow > maxStatus )
		maxStatus = currentRules.TopRow;
	if ( currentRules.BotRow > maxStatus )
		maxStatus = currentRules.BotRow;
	
	for ( int s = maxStatus ; s > 1 ; s-- )
	{
		int sMinus1 = s - 1;
		for ( int j = 1 ; j < sectionHeight ; j++ )
		{
			for ( int k = 1 ; k < sectionWidth ; k++ )
			{
				if ( BeforeAfter[Array][Section][j][k].BeforeEdgeStatus == s )
				{
					for ( int jj = j - 1 ; jj < j + 2 ; jj++ )
					{
						for ( int kk = k - 1 ; kk < k + 2 ; kk++ )
						{
							if ( BeforeAfter[Array][Section][j][k].BeforeEdgeStatus < sMinus1 )
								BeforeAfter[Array][Section][j][k].BeforeEdgeStatus = sMinus1;
						}
					}
				}
				if ( BeforeAfter[Array][Section][j][k].AfterEdgeStatus == s )
				{
					for ( int jj = j - 1 ; jj < j + 2 ; jj++ )
					{
						for ( int kk = k - 1 ; kk < k + 2 ; kk++ )
						{
							if ( BeforeAfter[Array][Section][j][k].AfterEdgeStatus < sMinus1 )
								BeforeAfter[Array][Section][j][k].AfterEdgeStatus = sMinus1;
						}
					}
				}
			}
		}
		
		for ( int j = 1 ; j < sectionHeight ; j++ )
		{
			for ( int k = 1 ; k < sectionWidth ; k++ )
			{
				if ( BeforeAfter[Array][Section][j][k].BeforeEdgeStatus == 0 && BeforeAfter[Array][Section][j][k].AfterEdgeStatus == 0 )
					BeforeAfter[Array][Section][j][k].EdgeExcluded = false;
				else
					BeforeAfter[Array][Section][j][k].EdgeExcluded = true;
			}
		}
	}
}


void CalcAfterBeforeComparisons(std::ofstream &LogFile, int Array, int Section, cv::Size SectionDimens,
	double ShrinkageLimit, std::vector< BlobSummary > &BeforeSizingList, std::vector< BlobSummary > &AfterSizingList, 
	std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter)
{
	int sectionWidth = SectionDimens.width;
	int sectionHeight = SectionDimens.height;
	for ( int j = 1 ; j <= sectionHeight ; j++ )
	{
		for ( int k = 1 ; k <= sectionWidth ; k++ )
		{
			if ( BeforeAfter[Array][Section][j][k].BeforeIndex >= 0 && BeforeAfter[Array][Section][j][k].AfterIndex >= 0 && !BeforeAfter[Array][Section][j][k].Mismatched )
			{
				BeforeAfter[Array][Section][j][k].Shrinkage = 
					(double)AfterSizingList[BeforeAfter[Array][Section][j][k].AfterIndex].ShapeSummary.Area /
					(double)BeforeSizingList[BeforeAfter[Array][Section][j][k].BeforeIndex].ShapeSummary.Area;
				if ( BeforeAfter[Array][Section][j][k].Shrinkage < ShrinkageLimit )
					BeforeAfter[Array][Section][j][k].ShrinkageStatus = 0;
				else
					BeforeAfter[Array][Section][j][k].ShrinkageStatus = 1;
				
				if ( BeforeSizingList[BeforeAfter[Array][Section][j][k].BeforeIndex].ShapeSummary.BlobIntensities.Sum > 1.0 )
				{
					BeforeAfter[Array][Section][j][k].AfterBeforeRatio = 
						AfterSizingList[BeforeAfter[Array][Section][j][k].AfterIndex].ShapeSummary.BlobIntensities.Sum /
						BeforeSizingList[BeforeAfter[Array][Section][j][k].BeforeIndex].ShapeSummary.BlobIntensities.Sum;
				}
				else
					BeforeAfter[Array][Section][j][k].AfterBeforeRatio = 
						AfterSizingList[BeforeAfter[Array][Section][j][k].AfterIndex].ShapeSummary.BlobIntensities.Sum;
						
				BeforeAfter[Array][Section][j][k].BeforeAnomalousFilling = BeforeSizingList[BeforeAfter[Array][Section][j][k].BeforeIndex].ShapeSummary.AnomalousFilling;
				BeforeAfter[Array][Section][j][k].AfterAnomalousFilling = AfterSizingList[BeforeAfter[Array][Section][j][k].AfterIndex].ShapeSummary.AnomalousFilling;
			}
			else if ( BeforeAfter[Array][Section][j][k].BeforeIndex >= 0 && BeforeAfter[Array][Section][j][k].AfterIndex >= 0 && BeforeAfter[Array][Section][j][k].Mismatched )
			{
				BeforeAfter[Array][Section][j][k].Shrinkage = 0.0;
				BeforeAfter[Array][Section][j][k].ShrinkageStatus = 0;
				BeforeAfter[Array][Section][j][k].AfterBeforeRatio = -1.0;
			}
			else if ( BeforeAfter[Array][Section][j][k].BeforeIndex >= 0 )
			{
				BeforeAfter[Array][Section][j][k].Shrinkage = 0.0;
				BeforeAfter[Array][Section][j][k].ShrinkageStatus = 0;
				BeforeAfter[Array][Section][j][k].AfterBeforeRatio = -1.0;
			}
			else if ( BeforeAfter[Array][Section][j][k].AfterIndex >= 0 )
			{
				BeforeAfter[Array][Section][j][k].Shrinkage = 10.0;
				BeforeAfter[Array][Section][j][k].ShrinkageStatus = 0;
				BeforeAfter[Array][Section][j][k].AfterBeforeRatio = -1.0;
			}
			else
			{
				BeforeAfter[Array][Section][j][k].Shrinkage = 0.0;
				BeforeAfter[Array][Section][j][k].ShrinkageStatus = 0;
				BeforeAfter[Array][Section][j][k].AfterBeforeRatio = -1.0;
			}
		}
	}	
}
					
int CalcBeforeAveIntenDistribution(std::ofstream &LogFile, int Array, int Section, cv::Size SectionDimens,
	std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter,
	std::vector< BlobSummary > &BeforeSizingList, double BeforeExclusionFactor, 
	std::vector< std::pair< double, int > > &AveIntenHistogram, Statistics1 &AveIntenStats, 
	double &AveIntenThreshold, int &NumbAcceptable )
{
	int sectionWidth = SectionDimens.width;
	int sectionHeight = SectionDimens.height;
	
	int numbDroplets = 0;
	std::vector< double > data(SectionDimens.height * SectionDimens.width );
	AveIntenStats.Clear();
	double minValue = 1.0E+37;
	double maxValue = -1.0;
	for ( int j = 1 ; j <= sectionHeight ; j++ )
	{
		for ( int k = 1 ; k <= sectionWidth ; k++ )
		{ 
			if ( BeforeAfter[Array][Section][j][k].BeforeIndex >= 0 
				&& !BeforeAfter[Array][Section][j][k].BeforeAnomalousFilling 
				&& BeforeAfter[Array][Section][j][k].BeforeEdgeStatus == 0 )
			{
				data[numbDroplets] = BeforeSizingList[BeforeAfter[Array][Section][j][k].BeforeIndex].ShapeSummary.BlobIntensities.Ave;
				AveIntenStats.Accumulate(data[numbDroplets]);
				if ( data[numbDroplets] < minValue )
					minValue = data[numbDroplets];
				if ( data[numbDroplets] > maxValue )
					maxValue = data[numbDroplets];
				numbDroplets++;
			}		
		}
	}
	if ( numbDroplets == 0 )
	{
		NumbAcceptable = 0;
		return 0;
	}
	
	AveIntenStats.Analyze();
	data.resize(numbDroplets);
	sort(data.begin(), data.end() );
	
	int q1 = (int)ceil(numbDroplets / 4 );
	int q3 = (int)floor((3 * numbDroplets)/4);
	
	double iqr = data[q3] - data[q1];
	double optimalWidth = (2.0 * iqr)/pow((double)numbDroplets, 0.33333);
	// std::clog << "stats " << AveIntenStats.Count() << ", " << AveIntenStats.Ave() << ", " << AveIntenStats.Std() << std::endl;
	// std::clog << minValue << ", " << maxValue << ", " << numbDroplets << std::endl;
	// std::clog << "q1/q3 = " << q1 << "/" << q3 << ", iqr = " << iqr << ", optimalWidth = " << optimalWidth << std::endl;
	double binWidth = 5.0;
	if ( optimalWidth > binWidth )
	{
		binWidth = 6;
		if ( optimalWidth > binWidth )
			binWidth = 7;
		if ( optimalWidth > binWidth )
			binWidth = 8;
		if ( optimalWidth > binWidth )
			binWidth = 9;
		if ( optimalWidth > binWidth )
			binWidth = 10;
	}
	else if ( optimalWidth < binWidth )
	{
		binWidth = 4;
		if ( optimalWidth < binWidth)
			binWidth = 3;
		if ( optimalWidth < binWidth)
			binWidth = 2;
		if ( optimalWidth < binWidth)
			binWidth = 1;
	}
	
	double start = ((floor(minValue/binWidth))-1) * binWidth;
	if ( start <= 0.0 )
		start = binWidth;
	double finish = (ceil(maxValue/binWidth)) * binWidth;
	int numbBins = (int)((finish - start)/binWidth) + 2;
	// std::clog << binWidth << "," << start << ", " << finish << ", " << numbBins << std::endl;
	// std::clog << "numbBins " << numbBins << std::endl;
	AveIntenHistogram.resize(numbBins);
	for ( int n = 0 ; n < numbBins ; n++ )
	{
		AveIntenHistogram[n].first = start + n * binWidth;
		AveIntenHistogram[n].second = 0;
	}
	
	for ( int n = 0 ; n < data.size() ; n++ )
	{
		int m = 0;
		while ( AveIntenHistogram[m].first < data[n] && m < numbBins-1 )
			m++;
		
		AveIntenHistogram[m].second++;
	}
	
	// double lowMult = ( BeforeExclusionFactor - 1.0 );
	// double highMult = 2.0 * lowMult;
	// if ( 2.5 * AveIntenStats.Std() < AveIntenStats.Ave() * lowMult )
		// AveIntenThreshold = BeforeExclusionFactor * AveIntenStats.Ave();
	// else if ( 2.5 * AveIntenStats.Std() < AveIntenStats.Ave() * highMult )
		// AveIntenThreshold = AveIntenStats.Ave() + 2.5 * AveIntenStats.Std();
	// else
		// AveIntenThreshold = (1.0 + highMult ) * AveIntenStats.Ave();
	// if ( ( BeforeExclusionFactor - 1.0) * AveIntenStats.Ave() > 2.0 * AveIntenStats.Std() )
		// AveIntenThreshold = BeforeExclusionFactor * AveIntenStats.Ave();
	// else
	// {
		// double tmp = (BeforeExclusionFactor - 1.0) * AveIntenStats.Ave();
		// AveIntenThreshold = 0.5 * ( tmp + 2.0 * AveIntenStats.Std() ) + AveIntenStats.Ave();
	// }
	AveIntenThreshold = AveIntenStats.Ave() + 2.5 * AveIntenStats.Std();
	NumbAcceptable = 0;
	for ( int j = 1 ; j <= sectionHeight ; j++ )
	{
		for ( int k = 1 ; k <= sectionWidth ; k++ )
		{ 
			if ( BeforeAfter[Array][Section][j][k].BeforeIndex >= 0 )
			{
				BeforeAfter[Array][Section][j][k].RelativeAveInten =
					BeforeSizingList[BeforeAfter[Array][Section][j][k].BeforeIndex].ShapeSummary.BlobIntensities.Ave / AveIntenStats.Ave();
				if ( BeforeSizingList[BeforeAfter[Array][Section][j][k].BeforeIndex].ShapeSummary.BlobIntensities.Ave < AveIntenThreshold )
				{
					BeforeAfter[Array][Section][j][k].RelativeAveIntenStatus = 1;
					NumbAcceptable++;
				}
				else
					BeforeAfter[Array][Section][j][k].RelativeAveIntenStatus = 0;
			}
		}
	}
	return NumbAcceptable;
}

double HistogramFitParabola(std::vector< std::pair< double, int > >::iterator Begin, int Center, int Numb, double &RValue, double &Slope, double &YIntercept )
{
	LinearRegression regress;
	regress.Clear();
	for ( int n = 0; n < Numb ; n++ )
	{
		regress.Accumulate(pow(n - Center,2), (*(Begin + n)).second );	
	}
	regress.Analyze();
	RValue = regress.RValue();
	double chisq = 0;
	double ycalc;
	for ( int n = 0 ; n < Numb ; n++ )
	{
		regress.Solve(pow(n - Center,2), ycalc);
		chisq += pow(ycalc - (*(Begin + n)).second, 2);
	}
	Slope = regress.Slope();
	YIntercept = regress.YIntercept();
	return chisq;
}

void ApplyOtsuThreshold(std::ofstream &LogFile, std::vector< double > Data,
		std::vector< std::pair< double, int > > &RatioHistogram, int MaxBin, double RatioDefaultDivider, double PosNegRatio,
		AfterBeforeResults &RatioResults, Statistics1 &BelowStats, Statistics1 &AboveStats, 
		int &BelowMaxCount, int &BelowMaxIdx, int &AboveMaxCount, int &AboveMaxIdx )
{
	int dividerIdx = -1;
	for ( int n = 1 ; n < RatioHistogram.size() - 1 ; n++  )
	{
		if ( RatioHistogram[n-1].first < RatioResults.Threshold && RatioResults.Threshold <= RatioHistogram[n].first )
		{
			RatioResults.ThresholdIdx = n;
			break;
		}
	}
	
	std::pair<int, int> histLimits;
	std::pair<int, int> newHistLimits;
	std::vector< std::pair< double, int > >::iterator begin;
	double slope;
	double yIntercept;
	if ( RatioResults.ThresholdIdx > 3 && RatioResults.ThresholdIdx < RatioHistogram.size() - 4 && RatioHistogram.size() > 12 )
	{
		LogFile << "\nTrying Parabola Fit, numbBins = " << RatioHistogram.size() << "\n";
		histLimits.first = RatioResults.ThresholdIdx - 3;
		histLimits.second = RatioResults.ThresholdIdx + 3;
		begin = RatioHistogram.begin() + RatioResults.ThresholdIdx - 3;
		double rValue0;
		double chisq0 = HistogramFitParabola(begin, 3, 7, rValue0, slope, yIntercept );
		double count0 = RatioHistogram[RatioResults.ThresholdIdx].second;
		double count = count0;
		LogFile << "0 - thresholdIdx/chisq/RV/Count/Slope/YInter:  " 
			<< RatioResults.ThresholdIdx << " / " << chisq0 << " / " << rValue0 << " / " << count0 << " / " << slope << " / " << yIntercept << "\n";
		double chisq = chisq0;
		double rValue = rValue0;
		int thresholdIdx = RatioResults.ThresholdIdx;
		int newThresholdIdx; 
		double newCount;
		double newChisq;
		double newRValue;
		double updatedThreshold;
		double newSlope;
		double newYIntercept;
		// double altThreshold = -1.0;
		int altThresholdIdx = -1;
		int initThresholdIdx = RatioResults.ThresholdIdx;
		if ( slope < 0.1 )
		{
			int altThresholdIdx1 = -1;
			double chisq1 = 1.0E+37;
			int count1;
			double slope1 = -100000.;
			double yIntercept1;
			int altThresholdIdx2;
			double chisq2 = 2.0E+37;
			int count2;
			double slope2 = -10000.0;
			double yIntercept2;
			if ( RatioResults.Threshold < RatioDefaultDivider )
			{
				altThresholdIdx1 = (int)RatioHistogram.size();
			}
			for ( int n = 1 ; n < RatioHistogram.size() - 1 ; n++  )
			{
				if ( RatioHistogram[n-1].first < RatioDefaultDivider && RatioDefaultDivider <= RatioHistogram[n].first )
				{
					altThresholdIdx1 = n;
					break;
				}
			}
			if ( altThresholdIdx1 < 4 )
				altThresholdIdx1 = 4;
			else if ( altThresholdIdx1 > RatioHistogram.size() - 5 )
				altThresholdIdx1 = (int)RatioHistogram.size() - 5;
			
			if( RatioHistogram[altThresholdIdx1] < RatioHistogram[RatioResults.ThresholdIdx] )
			{
				begin = RatioHistogram.begin() + altThresholdIdx1 - 3;
				chisq1 = HistogramFitParabola(begin, 3, 7, newRValue, slope1, yIntercept1 );
				count1 = RatioHistogram[altThresholdIdx1].second;
				LogFile << "aliThresholdIdx1 = " << altThresholdIdx1 << ", chisq1/count1/slope1: " << chisq1 << " / " << count1 << " / " << slope1 << "\n";
			}
			else 
			{
				chisq1 = 1.0e+37;
				count1 = 1000000000;
				slope1 = -10000.0;
			}
			if ( abs(altThresholdIdx1 - RatioResults.ThresholdIdx) > 4 )
			{
				int tmpI = (int)floor(abs(altThresholdIdx1 - RatioResults.ThresholdIdx)/2);
				if ( RatioResults.Threshold > RatioDefaultDivider )
					altThresholdIdx2 = altThresholdIdx1 + tmpI;
				else
					altThresholdIdx2 = altThresholdIdx1 - tmpI;
				
				begin = RatioHistogram.begin() + altThresholdIdx2 - 3;
				chisq2 = HistogramFitParabola(begin, 3, 7, newRValue, slope2, yIntercept2 );
				count2 = RatioHistogram[altThresholdIdx2].second;
				LogFile << "aliThresholdIdx2 = " << altThresholdIdx2 << ", chisq2/count2/slope2: " << chisq2 << " / " << count2 << " / " << slope2 << "\n";
			}
			else
			{
				chisq2 = 1.0e+37;
				slope2 = -10000.0;
				count2 = 1000000000;
			}
			
			if ( slope1 > slope && slope1 > 0.1 )
			{
				if ( chisq1 < chisq2 || slope2 <= 0.1 )
				{
					histLimits.first = altThresholdIdx1 - 3;
					histLimits.second = altThresholdIdx1 + 3;
					chisq = chisq1;
					count = count1;
					initThresholdIdx = altThresholdIdx1;
					slope = slope1;
				}
				else if ( slope2 > 0.1)
				{
					histLimits.first = altThresholdIdx2 - 3;
					histLimits.second = altThresholdIdx2 + 3;
					chisq = chisq2;
					count = count2;
					initThresholdIdx = altThresholdIdx2;
					slope = slope2;
				}		
			}
			else if ( slope2 > slope && slope2 > 0.1 )
			{
				histLimits.first = altThresholdIdx2 - 3;
				histLimits.second = altThresholdIdx2 + 3;
				chisq = chisq2;
				count = count2;
				initThresholdIdx = altThresholdIdx2;
				slope = slope2;
			}
			else
			{
				if ( count2 < count1 && count2 < count0 )
				{
					histLimits.first = altThresholdIdx2 - 3;
					histLimits.second = altThresholdIdx2 + 3;
					chisq = chisq2;
					count = count2;
					initThresholdIdx = altThresholdIdx2;
					slope = slope2;
				}
				else if ( count1 < count2 && count1 < count0 )
				{
					histLimits.first = altThresholdIdx1 - 3;
					histLimits.second = altThresholdIdx1 + 3;
					chisq = chisq1;
					count = count1;
					initThresholdIdx = altThresholdIdx1;
					slope = slope1;
				}
			}
		}
		thresholdIdx = initThresholdIdx;
		LogFile << "initThresholdIdx = " << initThresholdIdx << "\n";
		LogFile << "1 - thresholdIdx/chisq/Count/Slope:  " 
				<< initThresholdIdx << " / " << chisq << " / " << count << " / " << slope << "\n";
		while ( histLimits.first > 0 )
		{
			newHistLimits.first = histLimits.first - 1;
			newHistLimits.second = histLimits.second - 1;
			newThresholdIdx = newHistLimits.first + 3 ;
			begin = RatioHistogram.begin() + newThresholdIdx - 3;
			newChisq = HistogramFitParabola(begin, 3, 7, newRValue, newSlope, newYIntercept );
			newCount = RatioHistogram[newThresholdIdx].second;
			// LogFile << "thresholdIdx/chisq/RV/Count:  " << newThresholdIdx << " / " << newChisq << " / " << newRValue << " / " << newCount << "\n";
			LogFile << "2 - thresholdIdx/chisq/RV/Count/Slope/YInter:  " 
				<< newThresholdIdx << " / " << newChisq << " / " << newRValue << " / " << newCount << " / " << newSlope << " / " << newYIntercept << "\n";
			if ( newChisq < chisq && newCount < count + ceil(sqrt(count)) && ( newSlope > 0.1 || newSlope > slope ) )
			{
				chisq = newChisq;
				rValue = newRValue;
				slope = newSlope;
				histLimits = newHistLimits;
				thresholdIdx = newThresholdIdx;
				if ( newCount < count )
					count = newCount;
			}	
			else
				break;	
		}
		LogFile << "thresholdIdx = " << thresholdIdx << "\n";
		if ( thresholdIdx == initThresholdIdx )
		{
			while ( histLimits.second < RatioHistogram.size() - 2 )
			{
				newHistLimits.first = histLimits.first + 1;
				newHistLimits.second = histLimits.second + 1;
				newThresholdIdx = newHistLimits.first + 3 ;
				begin = RatioHistogram.begin() + newThresholdIdx;
				newChisq = HistogramFitParabola(begin, 3, 7, newRValue, newSlope, newYIntercept );
				newCount = RatioHistogram[newThresholdIdx].second;
				LogFile << "3 - thresholdIdx/chisq/RV/Count/Slope/YInter:  " 
					<< newThresholdIdx << " / " << newChisq << " / " << newRValue << " / " << newCount << " / " << newSlope << " / " << newYIntercept << "\n";
				if ( newChisq < chisq && newCount < count + ceil(sqrt(count)) && ( newSlope > 0.1 || newSlope > slope ) )  //* 1.25 )
				{
					chisq = newChisq;
					rValue = newRValue;
					histLimits = newHistLimits;
					thresholdIdx = newThresholdIdx;
					if ( newCount < count )
						count = newCount;
				}	
				else
					break;	
			}
		}
		if ( thresholdIdx != RatioResults.ThresholdIdx )
		{
			updatedThreshold = 0.5 * ( RatioHistogram[thresholdIdx].first + RatioHistogram[thresholdIdx-1].first );
			LogFile << "Parabola Fit shifts Otsu threshold from bin " << RatioResults.ThresholdIdx << " to " << thresholdIdx << "\n";
			LogFile << "     Threshold shifts from " << RatioResults.Threshold << " to " << updatedThreshold << "\n";
			LogFile << "     Chisq and RValue change from (" << chisq0 << ", " << rValue0 << ") to (" << chisq << ", " << rValue << ")\n";
			RatioResults.Threshold = updatedThreshold;
			RatioResults.ThresholdIdx = thresholdIdx;	
		}
		else
		{
			LogFile << "Parabola fit does not change Otsu threshold\n";
		}
	}
	else
	{
		LogFile << "Otsu Threshold too close to edge of distribution, Parabola fit was not performed\n";
	}


	for ( int n = 0 ; n < Data.size() ; n++ )
	{
		if ( Data[n] < RatioResults.Threshold )
			BelowStats.Accumulate(Data[n]);
		else
			AboveStats.Accumulate(Data[n]);
	}
	BelowStats.Analyze();
	AboveStats.Analyze();

	for ( int n = 0 ; n < RatioHistogram.size() ; n++  )
	{
		if ( RatioHistogram[n].first < RatioResults.Threshold )
		{
			if ( RatioHistogram[n].second > BelowMaxCount )
			{
				BelowMaxCount = RatioHistogram[n].second;
				BelowMaxIdx = n;
			}
		}
		else
		{
			if ( dividerIdx < 0 )
			{
				dividerIdx = n - 1;
				AboveMaxCount = RatioHistogram[n-1].second;
				AboveMaxIdx = n-1;
			}
			
			if ( RatioHistogram[n].second > AboveMaxCount )
			{
				AboveMaxCount = RatioHistogram[n].second;
				AboveMaxIdx = n;
			}

		}
	}
	bool zScoreOK = false;
	double tailFraction;
	double zScore;
	if ( RatioHistogram[MaxBin].first <= RatioResults.Threshold && RatioHistogram[MaxBin].first <= RatioDefaultDivider )
	{
		if ( BelowStats.Count() > 100 )
		{
			tailFraction = (double)AboveStats.Count() / ( (double)AboveStats.Count() + (double)BelowStats.Count());
			zScore = (RatioResults.Threshold - RatioHistogram[MaxBin].first)/BelowStats.Std();
			if ( zScore > 3.0 )
				zScoreOK = true;
			else if ( zScore > 2.5 && tailFraction < 0.02 )
				zScoreOK = true;
			else if ( zScore > 2.0 && tailFraction < 0.005 )
				zScoreOK = true;
		}
		LogFile << "\nCheck ZScore on Otsu Threshold, Max Bin/Value " << MaxBin << "/ " << RatioHistogram[MaxBin].first 
			<< "\n     Threshold Idx/Value " << RatioResults.ThresholdIdx << " / " << RatioResults.Threshold << ", tailFraction: " << tailFraction
			<< "\n Z-Score: " << zScore << ", Std: " << BelowStats.Std() << "\n";
	}
	else if ( RatioHistogram[MaxBin].first > RatioResults.Threshold && RatioHistogram[MaxBin].first > RatioDefaultDivider )
	{
		if ( AboveStats.Count() > 100 )
		{
			tailFraction = (double)BelowStats.Count() / ( (double)AboveStats.Count() + (double)BelowStats.Count());
			zScore = (RatioHistogram[MaxBin].first - RatioResults.Threshold)/AboveStats.Std();
			if ( zScore > 3.0 )
				zScoreOK = true;
			else if ( zScore > 2.5 && tailFraction < 0.02 )
				zScoreOK = true;
			else if ( zScore > 2.0 && tailFraction < 0.005)
				zScoreOK = true;
		}
		LogFile << "\nCheck ZScore on Otsu Threshold, Max Bin/Value " << MaxBin << "/ " << RatioHistogram[MaxBin].first 
			<< "\n     Threshold Idx/Value " << RatioResults.ThresholdIdx << " / " << RatioResults.Threshold << ", tailFraction: " << tailFraction
			<< "\n Z-Score: " << zScore << ", Std: " << BelowStats.Std() << "\n";
	}

	if ( !zScoreOK && RatioResults.Threshold > RatioHistogram[MaxBin].first * (1.0 + 1.0/PosNegRatio ) * 0.5 &&
			RatioResults.Threshold < RatioHistogram[MaxBin].first * ( 1.0 + PosNegRatio ) * 0.5 )
	{
		double ratio = 0;
		if ( BelowStats.Count() < 21 || AboveStats.Count() < 21 || (BelowStats.Ave() + 1.5 * BelowStats.Std() > AboveStats.Ave() - 1.5 * AboveStats.Std() ))
			RatioResults.ThresholdMethod = 1;
		else if ( BelowStats.Ave() + BelowStats.Std() > AboveStats.Ave() - AboveStats.Std() )
			RatioResults.ThresholdMethod = 1;
		else if ( BelowMaxIdx == dividerIdx || AboveMaxCount == dividerIdx )
			RatioResults.ThresholdMethod = 1;
		else if ( RatioResults.Threshold < BelowStats.Ave() + BelowStats.Std() || RatioResults.Threshold > AboveStats.Ave() - AboveStats.Std() )
			RatioResults.ThresholdMethod = 1;
		else
		{
			double ratio = ( RatioHistogram[AboveMaxIdx].first/RatioHistogram[BelowMaxIdx].first );
			if ( ratio < 0.7 * PosNegRatio )
				RatioResults.ThresholdMethod = 1;
		}
		// LogFile << "\nOtsu Threshold checked for Array " << Array << ", Section " << Section << "\n";
		
		LogFile << "bin-Val-Cnt\t" << BelowMaxIdx << "\t" << RatioHistogram[BelowMaxIdx].first << "\t" << RatioHistogram[BelowMaxIdx].second
			<< "\t" << AboveMaxIdx << "\t" << RatioHistogram[AboveMaxIdx].first << "\t" << RatioHistogram[AboveMaxIdx].second << "\n";
		LogFile << "Cnt-Ave-Std\t" << BelowStats.Count() << "\t" << BelowStats.Ave() << "\t" << BelowStats.Std() << "\t"
			<< AboveStats.Count() << "\t" << AboveStats.Ave() << "\t" << AboveStats.Std() << "\n";
		LogFile << "above/belowMaxIdx " << AboveMaxIdx << " / " << BelowMaxIdx << "\n";
		LogFile << "above/belowCount " << RatioHistogram[AboveMaxIdx].second << " / " << RatioHistogram[BelowMaxIdx].second << "\n";
		LogFile << "dividerIdx\t" << dividerIdx << "\tratio\t" << ratio << "\n";
		LogFile << "Method chosen " << RatioResults.ThresholdMethod << "\n";	
	}
}

				
int CalcAfterBeforeRatioDistribution(std::ofstream &LogFile, int Array, int Section, cv::Size SectionDimens,	
	std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter,
	double PosNegRatio, double RatioDefaultDivider,
	std::vector< std::pair< double, int > > &RatioHistogram, Statistics1 &RatioStats, 
	Statistics1 &BelowStats, Statistics1 &AboveStats, AfterBeforeResults &RatioResults)
{
	int numbDroplets = 0;
	RatioResults.Clear();
	BelowStats.Clear();
	AboveStats.Clear();

	std::vector< double > data(SectionDimens.height * SectionDimens.width );
	RatioStats.Clear();
	double minValue = 1.0E+37;
	double maxValue = -1.0;
	int sectionHeight = SectionDimens.height;
	int sectionWidth = SectionDimens.width;
	for ( int j = 1 ; j <= sectionHeight ; j++ )
	{
		for ( int k = 1 ; k <= sectionWidth ; k++ )
		{ 
			if ( BeforeAfter[Array][Section][j][k].AfterBeforeRatio > 0 
				&& BeforeAfter[Array][Section][j][k].ShrinkageStatus > 0 
				&& BeforeAfter[Array][Section][j][k].RelativeAveIntenStatus > 0 
				&& !BeforeAfter[Array][Section][j][k].BeforeAnomalousFilling 
				&& !BeforeAfter[Array][Section][j][k].AfterAnomalousFilling 
				&& !BeforeAfter[Array][Section][j][k].EdgeExcluded 
				&& !BeforeAfter[Array][Section][j][k].Mismatched )
			{
				data[numbDroplets] = BeforeAfter[Array][Section][j][k].AfterBeforeRatio;
				RatioStats.Accumulate(data[numbDroplets]);
				if ( data[numbDroplets] < minValue )
					minValue = data[numbDroplets];
				if ( data[numbDroplets] > maxValue )
					maxValue = data[numbDroplets];
				numbDroplets++;
			}		
		}
	}
	if ( numbDroplets == 0 )
		return 0;
	
	RatioStats.Analyze();
	
	double cutoff = RatioStats.Ave() + 4.0 * RatioStats.Std();
	if ( 2.0 * PosNegRatio > cutoff )
		cutoff = 2.0 * PosNegRatio;
	LogFile << "\nCalcAfterBeforeRatioDistribution\n     RatioStats: Ave/Std = " << RatioStats.Ave() << " / " << RatioStats.Std() 
		<< ", cutoff = " << cutoff << ", PosNegRatio = " << PosNegRatio << "\n\n";
	if ( maxValue >= cutoff )
	{
		minValue = 1.0E+37;
		maxValue = -1.0;
		RatioStats.Clear();
		numbDroplets = 0;
		for ( int j = 1 ; j <= sectionHeight ; j++ )
		{
			for ( int k = 1 ; k <= sectionWidth ; k++ )
			{ 
				if ( BeforeAfter[Array][Section][j][k].AfterBeforeRatio > 0 
					&& BeforeAfter[Array][Section][j][k].ShrinkageStatus > 0 
					&& BeforeAfter[Array][Section][j][k].RelativeAveIntenStatus > 0 
					&& !BeforeAfter[Array][Section][j][k].BeforeAnomalousFilling 
					&& !BeforeAfter[Array][Section][j][k].AfterAnomalousFilling 
					&& !BeforeAfter[Array][Section][j][k].EdgeExcluded 
					&& !BeforeAfter[Array][Section][j][k].Mismatched )
				{
					if ( BeforeAfter[Array][Section][j][k].AfterBeforeRatio < cutoff )
					{
						data[numbDroplets] = BeforeAfter[Array][Section][j][k].AfterBeforeRatio;
						RatioStats.Accumulate(data[numbDroplets]);
						if ( data[numbDroplets] < minValue )
							minValue = data[numbDroplets];
						if ( data[numbDroplets] > maxValue )
							maxValue = data[numbDroplets];
						numbDroplets++;
					}
					else
					{
						BeforeAfter[Array][Section][j][k].Mismatched = true;
					}
				}		
			}
		}
		if ( numbDroplets == 0 )
			return 0;
	
		RatioStats.Analyze();
	}
	
	data.resize(numbDroplets);
	sort( data.begin(), data.end() );
	
	int q1 = (int)ceil(numbDroplets / 4 );
	int q3 = (int)floor((3 * numbDroplets)/4);
	
	double iqr = data[q3] - data[q1];
	double optimalWidth = (2.0 * iqr)/pow((double)numbDroplets, 0.33333 );
	// LogFile << "q1/q3 = " << q1 << "/" << q3 << ", iqr = " << iqr << ", optimalWidth = " << optimalWidth << std::endl;
	double binWidth;
	if ( optimalWidth > 0.3 )
		binWidth = 0.15;
	else if (optimalWidth > 0.2 )
		binWidth = 0.1;
	else if ( optimalWidth > 0.15 )
		binWidth = 0.075;
	else
		binWidth = 0.05;
	if ( optimalWidth < binWidth )
	{
		binWidth = 0.04;
		if ( optimalWidth < binWidth )
		{
			binWidth = 0.03;
			if ( optimalWidth < binWidth )
			{
				binWidth = 0.02;
				if ( optimalWidth < binWidth )
				{
					binWidth = 0.01;	
				}
			}
		}
	}
	
	double start = ((floor(minValue/binWidth))-1) * binWidth;
	if ( start <= 0.0 )
		start = binWidth;
	double finish = (ceil(maxValue/binWidth)) * binWidth;
	int numbBins = (int)((finish - start)/binWidth) + 2;
	RatioHistogram.resize(numbBins);
	for ( int n = 0 ; n < numbBins ; n++ )
	{
		RatioHistogram[n].first = start + n * binWidth;
		RatioHistogram[n].second = 0;
	}
	
	for ( int n = 0 ; n < data.size() ; n++ )
	{
		int m = 0;
		while ( RatioHistogram[m].first < data[n] && m < numbBins-1 )
			m++;
		
		RatioHistogram[m].second++;
	}
	int numbOccupied = 0;
	if ( numbDroplets > 100 )
	{
		int maxCount = 0;
		int maxBin = 0;
		for ( int n = 0 ; n < RatioHistogram.size() ; n++ )
		{
			if ( RatioHistogram[n].second > maxCount )
			{
				maxCount = RatioHistogram[n].second;
				maxBin = n;
			}
		}

		RatioResults.ThresholdMethod = 0;
		int belowMaxCount = 0;
		int belowMaxIdx = 0;
		int aboveMaxCount = 0;
		int aboveMaxIdx = 0;
		// int dividerIdx = -1;
		// bool middlePresent = true;
		if (OtsuThreshold(LogFile, data, 
			RatioResults ))
			RatioResults.ThresholdMethod = 2;
		else
			RatioResults.ThresholdMethod = 0;
		
		// bool shiftThresholdIn = false;
		// bool shiftThresholdOut = false;
		// bool shiftThreshold = false;
		if ( RatioResults.ThresholdMethod == 2 )
			ApplyOtsuThreshold(LogFile, data, 
				RatioHistogram, maxBin, RatioDefaultDivider, PosNegRatio,
				RatioResults, BelowStats, AboveStats, 
				belowMaxCount, belowMaxIdx, aboveMaxCount, aboveMaxIdx);
		
		double initThreshold = RatioResults.Threshold;
		int initThresholdIdx = RatioResults.ThresholdIdx;
		// double altThresholdVal = 10000000.;
		int altThresholdIdx = 0;
		Statistics1 altAboveStats;
		Statistics1 altBelowStats;
		altAboveStats.Clear();
		altBelowStats.Clear();
		bool useTrialThreshold;
		if ( RatioResults.ThresholdMethod == 1 )
		{
			double altThreshold;
			if ( RatioHistogram[maxBin].first <= RatioDefaultDivider )
				altThreshold = RatioHistogram[maxBin].first * ( 1.0 + PosNegRatio ) * 0.5;
			else 
				altThreshold = RatioHistogram[maxBin].first * (1.0 + 1.0/PosNegRatio ) * 0.5;

			for ( int n = 0 ; n < data.size() ; n++ )
			{
				if ( data[n] < altThreshold )
					altBelowStats.Accumulate(data[n]);
				else
					altAboveStats.Accumulate(data[n]);
			}
			altBelowStats.Analyze();
			altAboveStats.Analyze();
			
			// double tailFraction;
			// double trialThreshold;
			useTrialThreshold = false;
			double baseThreshold = 0.5 * ( RatioHistogram[maxBin].first + RatioHistogram[maxBin-1].first );
			// double zScore = -1.0;
			double altThresholdH;
			double altThresholdL;
			if ( RatioHistogram[maxBin].first <= RatioDefaultDivider )
			{
				altThresholdH = baseThreshold + altBelowStats.Std() * 3.0;
				altThresholdL = baseThreshold + altBelowStats.Std() * 2.0;
			}
			else if ( RatioHistogram[maxBin].first > RatioDefaultDivider )
			{				
				altThresholdL = RatioHistogram[maxBin].first - altAboveStats.Std() * 3.0;
				altThresholdH = RatioHistogram[maxBin].first - altAboveStats.Std() * 2.0;
			}
			double altThreshold1;
			double altThreshold2;
			if ( altThreshold >= altThresholdL && altThreshold <= altThresholdH )
			{
				altThreshold1 = altThresholdL;
				altThreshold2 = altThresholdH;
			}
			else if ( altThreshold < altThresholdL )
			{
				altThreshold1 = altThreshold * 0.95;
				altThreshold2 = 0.5 * ( altThresholdL + altThresholdH );
			}
			else
			{
				altThreshold1 = 0.5 * ( altThresholdL + altThresholdH );
				altThreshold2 = altThreshold * 1.05;
			}
			int altIdx1 = 0;
			while ( RatioResults.Data[altIdx1] < altThreshold1 && altIdx1 < RatioResults.Data.size() - 1 )
				altIdx1++;
			
			int altIdx = altIdx1;
			int bestAltIdx = altIdx;
			double bestLocalOtsu = -1.0E+37;
			while ( RatioResults.Data[altIdx] < altThreshold2 && altIdx < RatioResults.Data.size() - 1)
			{
				if ( RatioResults.OtsuResults[altIdx] > bestLocalOtsu )
				{
					bestAltIdx = altIdx;
					bestLocalOtsu = RatioResults.OtsuResults[altIdx];
				}
				altIdx++;
			}

			double bestAltThreshold = RatioResults.Data[bestAltIdx];
			for ( int n = 1 ; n < RatioHistogram.size() ; n++  )
			{
				if ( RatioHistogram[n-1].first < bestAltThreshold && bestAltThreshold <= RatioHistogram[n].first )
				{
					altThresholdIdx = n;
					break;
				}
			}
			RatioResults.Threshold = altThreshold;
			RatioResults.ThresholdIdx = altThresholdIdx;
		}
		LogFile << "\nChecked Threshold, final method = " << RatioResults.ThresholdMethod << ", Final Threshold = " << RatioResults.Threshold << "\n";
		LogFile << "init = " << initThreshold << ", initVal = " << RatioHistogram[initThresholdIdx].second << ", initIdx = " << initThresholdIdx << "\n";
		LogFile << "alt = " << RatioResults.Threshold << ", altVal = " << RatioHistogram[altThresholdIdx].second << ", altIdx = " << altThresholdIdx << "\n\n";

		AboveStats.Clear();
		BelowStats.Clear();

		numbOccupied = 0;
		for ( int j = 1 ; j <= sectionHeight ; j++ )
		{
			for ( int k = 1 ; k <= sectionWidth ; k++ )
			{ 
				
				if ( BeforeAfter[Array][Section][j][k].AfterBeforeRatio > 0.0 
					&& BeforeAfter[Array][Section][j][k].ShrinkageStatus > 0 
					&& BeforeAfter[Array][Section][j][k].RelativeAveIntenStatus > 0 
					&& !BeforeAfter[Array][Section][j][k].Mismatched )
				{
					if ( BeforeAfter[Array][Section][j][k].AfterBeforeRatio > RatioResults.Threshold )
					{
						BeforeAfter[Array][Section][j][k].Occupied = true;
						AboveStats.Accumulate(BeforeAfter[Array][Section][j][k].AfterBeforeRatio);
						numbOccupied++;
					}
					else
					{
						BeforeAfter[Array][Section][j][k].Occupied = false;
						BelowStats.Accumulate(BeforeAfter[Array][Section][j][k].AfterBeforeRatio);
					}
				}
				else
					BeforeAfter[Array][Section][j][k].Occupied = false;

			}
		}
		
		BelowStats.Analyze();
		AboveStats.Analyze();
		// double prob = (double)numbOccupied / (double)numbDroplets;
		RatioResults.NumbBelow = numbDroplets - numbOccupied;
	}
	else
		RatioResults.ThresholdMethod = 0;
	
	return numbDroplets;
}

void SetUpMaskSymbols(std::vector< cv::Vec3b > &LineColors, std::vector< std::vector<cv::Point> > &Symbols )
{
	LineColors.resize(3);
	LineColors[0][1] = 255;
	LineColors[0][0] = LineColors[0][2] = 0;
	LineColors[1][0] = 255;
	LineColors[1][1] = LineColors[1][1] = 0;
	LineColors[2][2] = 255;
	LineColors[2][0] = LineColors[2][1] = 0;
	
	Symbols.resize(3);
	Symbols[0].resize(5);
	Symbols[0][0] = cv::Point(0,0);
	Symbols[0][1] = cv::Point(0,1);
	Symbols[0][2] = cv::Point(0,-1);
	Symbols[0][3] = cv::Point(1,0);
	Symbols[0][4] = cv::Point(-1,0);
	
	Symbols[1].resize(3);
	Symbols[1][0] = cv::Point(0,-1);
	Symbols[1][1] = cv::Point(0, 0);
	Symbols[1][2] = cv::Point(0, 1);
	
	Symbols[2].resize(3);
	Symbols[2][0] = cv::Point(-1,0);
	Symbols[2][1] = cv::Point(0, 0);
	Symbols[2][2] = cv::Point(1, 0);
	
}
	

bool OutputDropletComparisonImages(std::ofstream &LogFile, std::string BaseName,
	int Array, int Section, int ImageNumber, double Time, cv::Size SectionDimens,
	std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter,
	std::vector< BlobSummary > &BeforeSizingList, 
	std::vector< BlobSummary > &AfterSizingList, 
	cv::Mat &BeforeImage, cv::Mat &BeforeMask, cv::Mat &BeforeContourMask, cv::Mat &BeforeMap, cv::Mat &BeforeArrayCheck,
	cv::Mat &AfterImage, cv::Mat &AfterMask, cv::Mat &AfterContourMask, cv::Mat &AfterMap, cv::Mat &AfterArrayCheck,
	cv::Point WellSpacing2f, bool CreateMap, std::vector< std::string > &MapText, 
	bool OutputImages, std::string &ErrorMsg)
{
	std::ostringstream oString;
	bool dropletsInList = true;
	if ( BeforeSizingList.size() == 0 && AfterSizingList.size() == 0 )
	{
		LogFile << "BeforeSizingList and AfterSizingList are empty for Array " << Array << ", Section " << Section << "\n";
		dropletsInList = false;
	}
	LogFile << "BeforeSizingList.size() / AfterSizingList.size(): " << BeforeSizingList.size() << " / " << AfterSizingList.size() << "\n";
	if ( !dropletsInList && !OutputImages )
		return false;
	cv::Size boxSize;
	boxSize.width = (int)round(WellSpacing2f.x+4);
	boxSize.height = (int)round(WellSpacing2f.y+4);
	if ( boxSize.width % 2 == 0 )
	{
		if ( boxSize.width > WellSpacing2f.x+4 )
			boxSize.width--;
		else
			boxSize.width++;
	}
	if ( boxSize.height % 2 == 0 )
	{
		if ( boxSize.height > WellSpacing2f.y+4 )
			boxSize.height--;
		else
			boxSize.height++;
	}
	cv::Point offset;
	offset.x = (boxSize.width - 1)/2;
	offset.y = (boxSize.height -1)/2;
	cv::Size imageSize = BeforeMask.size();
	// std::clog << "Image output offset: " << offset.x << ", " << offset.y 
		// << ", WellSpacing: " << WellSpacing2f.x << ", " << WellSpacing2f.y 
		// << ", boxSize: " << boxSize.width << ", " << boxSize.height << std::endl;
	
	cv::Rect box;
	cv::Point center;
	cv::Mat workImage;
	cv::Mat imageRect;
	cv::Mat mapRect;
	cv::Mat checkRect;
	
	cv::Vec3b cont;
	cv::Vec3b quantColor;
	
	std::vector< cv::Vec3b > lineColors;  // 0 - Green, 1 - Blue, 2 - Red
	std::vector< std::vector<cv::Point> > symbols;	// 0 - Plus, 1 - Vert Bar, 2 - Horz Bar
	SetUpMaskSymbols(lineColors, symbols );
	std::vector< std::vector< cv::Point > > trialContours;
	int sectionHeight = SectionDimens.height;
	int sectionWidth = SectionDimens.width;
	// std::string mapString;
	// int symbolCount = 0;
	quantColor[0] = 0;
	quantColor[1] = 0;
	quantColor[2] = 0;
	
	for ( int j = 1 ; j <= sectionHeight ; j++ )
	{
		for ( int k = 1 ; k <= sectionWidth ; k++ )
		{ 
			int beforeColor = -1;
			int beforeSym = -1;
			int beforeSymColor = -1;
			int afterColor = -1;
			int afterSym = -1;
			int afterSymColor = -1;
			int beforeIndex = BeforeAfter[Array][Section][j][k].BeforeIndex;
			int afterIndex = BeforeAfter[Array][Section][j][k].AfterIndex;
			BeforeAfterMatrix current = BeforeAfter[Array][Section][j][k];
			if ( beforeIndex >= 0 )
			{
				if ( current.Mismatched )
				{
					beforeColor = 1;
					beforeSym = -1;
					beforeSymColor = -1;
				}
				else if ( BeforeSizingList[beforeIndex].ShapeSummary.FilterValue == 1 )
				{
					if ( current.QuantStatus )
					{
						beforeColor = 0;
						beforeSym = 0;
						beforeSymColor = 0;
					}
					else if ( current.RelativeAveIntenStatus == 0 )
					{
						beforeColor = 1;
					}
					else if ( current.BeforeAnomalousFilling && !current.AfterAnomalousFilling )
					{
						beforeColor = 1;
						beforeSym = 1;
						beforeSymColor = 1;
					}
					else if ( current.BeforeAnomalousFilling && current.AfterAnomalousFilling )
					{
						beforeColor = 1;
						beforeSym = 0;
						beforeSymColor = 1;
					}
					else if ( afterIndex >= 0 && AfterSizingList[afterIndex].ShapeSummary.FilterValue == 1 )
					{
						beforeColor = 0;
						beforeSym = 2;
						beforeSymColor = 2;
					}
					else
					{
						beforeColor = 2;
						beforeSym = 2;
						beforeSymColor = 2;
					}
				}
				else
				{
					beforeColor = 2;
					beforeSymColor = 2;
					if ( afterIndex < 0 || AfterSizingList[afterIndex].ShapeSummary.FilterValue != 1  )
					{
						beforeSym = 0;
					}
					else
					{
						beforeSym = 1;
					}
				}
			}
			
			if ( afterIndex >= 0 )
			{
				if ( current.Mismatched )
				{
					afterColor = 1;
					afterSym = -1;
					afterSymColor = -1;
				}
				else if ( current.QuantStatus )
				{
					afterColor = 0;
					afterSym = 0;
					if ( current.Occupied )
					{
						afterSymColor = 0;
					}
					else
					{
						afterSymColor = 2;
					}
				}
				else if ( current.EdgeExcluded )
				{
					afterColor = 1;
					afterSym = 2;
					afterSymColor = 2;
				}
				else if ( current.RunExcluded )
				{
					afterColor = 1;
					afterSym = 1;
					afterSymColor = 2;
				}
				else if ( current.AfterAnomalousFilling && !current.BeforeAnomalousFilling )
				{
					afterColor = 1;
					afterSym = 2;
					afterSymColor = 1;
				}
				else if ( current.AfterAnomalousFilling && current.BeforeAnomalousFilling )
				{
					afterColor = 1;
					afterSym = 0;
					afterSymColor = 1;
				}
				else
				{
					afterColor = 2;
					afterSymColor = 2;
					// oString << ",0";
					if ( AfterSizingList[afterIndex].ShapeSummary.FilterValue != 1 )
						afterSym = 1;
					else
						afterSym = -1;
					
					if ( beforeIndex < 0 || BeforeSizingList[beforeIndex].ShapeSummary.FilterValue != 1 
						|| current.RelativeAveIntenStatus == 0 )
					{
						if ( afterSym == 1 )
						{
							afterSym = 0;
						}
						else
						{
							afterSym = 2;
						}
					}
				}
			}
			if ( beforeIndex >= 0 || afterIndex >= 0 )
			{
				oString.str("");
				oString.clear();
				oString << Array << "," << Section << "," << j << "," << k;
				if ( beforeIndex < 0 )
					oString << ",-1,-1,-1,-1,-1";
				else
					oString << "," << beforeIndex
						<< "," << BeforeSizingList[beforeIndex].InputCenter2f.y << "," << BeforeSizingList[beforeIndex].InputCenter2f.x
						<< "," << BeforeSizingList[beforeIndex].ShapeSummary.FilterValue << "," << BeforeSizingList[beforeIndex].ShapeSummary.FillRatio ;
				
				if ( afterIndex < 0 )
					oString << ",-1,-1,-1,-1,-1";
				else
					oString << "," << afterIndex
						<< "," << AfterSizingList[afterIndex].InputCenter2f.y << "," << AfterSizingList[afterIndex].InputCenter2f.x
						<< "," << AfterSizingList[afterIndex].ShapeSummary.FilterValue << "," << AfterSizingList[afterIndex].ShapeSummary.FillRatio;
				
				oString << "," << current.QuantStatus << "," << current.EdgeExcluded << "," << current.BeforeEdgeStatus << "," << current.AfterEdgeStatus
					<< "," << current.RunExcluded << "," 
					<< "," << current.BeforeAnomalousFilling <<"," << current.AfterAnomalousFilling
					<< "," << current.RelativeAveIntenStatus << "," << current.RelativeAveInten
					<< "," << current.ShrinkageStatus << "," << current.Shrinkage
					<< "," << current.AfterBeforeRatio
					<< "," << beforeColor << "," << beforeSym << "," << beforeSymColor << "," << afterColor << "," << afterSym << "," << afterSymColor ;
				MapText.push_back(oString.str());
			}
			
			// Before image
				// Green outline - passed filter tests
				// 		Green plus: used in quantitation
				// 		Red plus: failed shrinkage
				// Blue outline
				//		Empty: Before blob failed RelativeAveIntenStatus
				//		Blue vert bar: Before blob passed filter test, but had anomalous shape
				//		Blue plus: Before and after blobs passed filter test, but had anomalous shapes
				// Red outline
				// 		Red vert bar: Before image failed filter value
				// 		Red horz bar: after image failed filter value or not present
			// After Image
				// Green outline - droplet used
				// 		Green plus: occupied
				// 		Red plus: unoccupied
				// Blue outline
				// 		Red Vertical bar: Edge Excluded
				// 		Red Horz bar: Run excluded
				//		Blue horz bar: After blob passed filter test, but had anomalous shape
				//		Blue plus: Before and after blobs passed filter test, but had anomalous shapes
				// Red outline
				// 		Red vert bar: After image failed filter value
				// 		Red horz bar: Before image failed filter value or was not present
			// int checkValue;
			if ( current.QuantStatus )
			{
				quantColor[1] = 200;
				quantColor[2] = 0;
			}
			else
			{
				quantColor[1] = 0;
				quantColor[2] = 200;
			}
			
			if ( BeforeAfter[Array][Section][j][k].BeforeIndex >= 0 )
			{
				int idx = BeforeAfter[Array][Section][j][k].BeforeIndex;
				center.x = (int)round(BeforeSizingList[idx].InputCenter2f.x);
				center.y = (int)round(BeforeSizingList[idx].InputCenter2f.y);
				
				if ( BeforeMask.at<short unsigned int>(center) != 0 )
				{
					box.x = center.x-offset.x;
					box.y = center.y-offset.y;
					TrimRectangle(box, boxSize, imageSize);
					int target = BeforeMask.at<short unsigned int>(center);
					// std::clog << "Before " << Array << "/" << Section << "/" << j << "/" << k << " - target " << target << ", center " << center << std::endl;
					// LogFile << "Before " << Array << "/" << Section << "/" << j << "/" << k << " - target " << target << ", center " << center << std::endl;
					cv::Mat searchBox = BeforeMask(box);
					cont[0] = 20 * Array + Section;
					cont[1] = j;
					cont[2] = k;
					mapRect = BeforeMap(box);
					for ( int n = 0 ; n < searchBox.total() ; n++ )
					{
						if ( searchBox.at<short unsigned int>(n) == target )
							mapRect.at<cv::Vec3b>(n) = cont;
					}

					if ( beforeColor >= 0 )
					{
						cont = lineColors[beforeColor];
						searchBox = BeforeContourMask(box);
						imageRect = BeforeImage(box);
						checkRect = BeforeArrayCheck(box);
						for ( int n = 0 ; n < searchBox.total() ; n++ )
						{
							if ( searchBox.at<short unsigned int>(n) == target )
							{
								imageRect.at<cv::Vec3b>(n) = cont;
								checkRect.at<cv::Vec3b>(n) = quantColor;
							}
						}
						if ( beforeSym >= 0 && beforeSymColor >= 0 )
						{
							// std::clog << "Before symbol drawn at " << center << ", " << beforeColor << "/" << beforeSym << "/" << beforeSymColor << std::endl;
							// std::clog << "symbols[beforeSym][0]=" << symbols[beforeSym][0] << ", symbols[afterSym][1]=" << symbols[beforeSym][1] << ", cont = " << cont << std::endl;
							// std::clog << " box = " << box << std::endl;
							cont = lineColors[beforeSymColor];
							for ( int n = 0 ; n < symbols[beforeSym].size() ; n++ )
							{
								cv::Point pTmp;
								pTmp.x = center.x + symbols[beforeSym][n].x;
								pTmp.y = center.y + symbols[beforeSym][n].y;
								BeforeImage.at< cv::Vec3b >(pTmp) = cont;	
							}
						}
					}
				}
				else
				{
					// std::clog << "!!!Before " << Array << "/" << Section << "/" << j << "/" << k << " - target " << BeforeMaskImage.at<short unsigned int>(center) << ", center " << center << std::endl;
					// LogFile << "!!!Before " << Array << "/" << Section << "/" << j << "/" << k << " - target " << BeforeMaskImage.at<short unsigned int>(center) << ", center " << center << std::endl;
				}
			}
			// symbolCount = 0;
			if ( BeforeAfter[Array][Section][j][k].AfterIndex >= 0 )
			{
				int idx = BeforeAfter[Array][Section][j][k].AfterIndex;
				center.x = (int)round(AfterSizingList[idx].InputCenter2f.x);
				center.y = (int)round(AfterSizingList[idx].InputCenter2f.y);
				if ( AfterMask.at<short unsigned int>(center) != 0 )
				{
					box.x = center.x-offset.x;
					box.y = center.y-offset.y;
					TrimRectangle(box, boxSize, imageSize);
					int target = AfterMask.at<short unsigned int>(center);
					// std::clog << "After " << Array << "/" << Section << "/" << j << "/" << k << " - target " << target << ", center " << center << std::endl;
					// LogFile << "After " << Array << "/" << Section << "/" << j << "/" << k << " - target " << target << ", center " << center << std::endl;
					cv::Mat searchBox = AfterMask(box);
					cont[0] = 20 * Array + Section;
					cont[1] = j;
					cont[2] = k;
					mapRect = AfterMap(box);
					for ( int n = 0 ; n < searchBox.total() ; n++ )
					{
						if ( searchBox.at<short unsigned int>(n) == target )
							mapRect.at<cv::Vec3b>(n) = cont;
					}
	
					if ( afterColor >= 0 )
					{
						searchBox = AfterContourMask(box);
						cont = lineColors[afterColor];
						imageRect = AfterImage(box);
						checkRect = AfterArrayCheck(box);
						for ( int n = 0 ; n < searchBox.total() ; n++ )
						{
							if ( searchBox.at<short unsigned int>(n) == target )
							{
								imageRect.at<cv::Vec3b>(n) = cont;
								checkRect.at<cv::Vec3b>(n) = quantColor;
							}
						}
						if ( afterSym >= 0 && afterSymColor >= 0 )
						{
							// std::clog << "After symbol drawn at " << center << ", " << afterColor << "/" << afterSym << "/" << afterSymColor << std::endl;
							// std::clog << "symbols[afterSym][0]=" << symbols[afterSym][0] << ", symbols[afterSym][1]=" << symbols[afterSym][1] << ", cont = " << cont << std::endl;
							// std::clog << " box = " << box << std::endl;
							cont = lineColors[afterSymColor];
							for ( int n = 0 ; n < symbols[afterSym].size() ; n++ )
							{
								cv::Point pTmp;
								pTmp.x = center.x + symbols[afterSym][n].x;
								pTmp.y = center.y + symbols[afterSym][n].y;
								AfterImage.at< cv::Vec3b >(pTmp) = cont;	
							}
						}
					}
					
				}
			}
			else
			{
				// std::clog << "!!!After " << Array << "/" << Section << "/" << j << "/" << k << " - target " << AfterMask.at<short unsigned int>(center) << ", center " << center << std::endl;
				// LogFile << "!!!After " << Array << "/" << Section << "/" << j << "/" << k << " - target " << AfterMask.at<short unsigned int>(center) << ", center " << center << std::endl;
			}
		}
	}
	if ( OutputImages )
	{
		oString.str("");
		oString.clear();
		oString << BaseName << "_" << ImageNumber << "_211_Before.png";
		// std::string outName = BaseName;
		// outName.append("_211_Before.png");
		cv::imwrite( oString.str(), BeforeImage);

		// outName = BaseName;
		// outName.append("_221_After.png");
		oString.str("");
		oString.clear();
		oString << BaseName << "_" << ImageNumber << "_221_After.png";
		cv::imwrite( oString.str(), AfterImage);
		
		// outName = BaseName;
		// outName.append("_212_Bf_AChk.png");
		oString.str("");
		oString.clear();
		oString << BaseName << "_" << ImageNumber << "_212_Bf_AChk.png";
		cv::imwrite( oString.str(), BeforeArrayCheck);
		
		// outName = BaseName;
		// outName.append("_222_Af_AChk.png");
		oString.str("");
		oString.clear();
		oString << BaseName << "_" << ImageNumber << "_222_Af_AChk.png";
		cv::imwrite( oString.str(), AfterArrayCheck);
	}
	return true;
}

bool OutputDropletRatioData(std::ofstream &LogFile, std::string &BaseName, 
	int Array, int Section, int ImageNumber, double Time, cv::Size SectionDimens, double PosNegRatio, double RatioDefaultDivider,
	std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter,
	std::vector< BlobSummary > &BeforeSizingList, 
	std::vector< std::pair< double, int > > &RatioHistogram, Statistics1 &RatioStats, 
	Statistics1 &BelowStats, Statistics1 &AboveStats, AfterBeforeResults &RatioResults)
{
	if ( BeforeSizingList.size() == 0 )
	{
		LogFile << "BeforeSizingList is empty for Array " << Array << ", Section " << Section << "\n";
		return false;
	}
	
	int sectionHeight = SectionDimens.height;
	int sectionWidth = SectionDimens.width;

	std::ostringstream oString;
	oString.clear();

	// oString << BaseName << "_235_" << Array << "_" << Section << "_AB_Ratio.txt";
	oString.str("");
	oString.clear();
	oString << BaseName << "_" << ImageNumber << "_236_" << Array << "_" << Section << "_AB_Ratio.csv";
	std::string abRatioOutputName(oString.str());

	std::ofstream abRatioOutput;	
	abRatioOutput.open(abRatioOutputName.c_str());
	if ( !abRatioOutput.is_open() )
	{
		oString.str("");
		oString.clear();
		oString << "Problems opening: " <<  abRatioOutputName;
		return false;
	}
	int numbPositive = (int)(RatioResults.Data.size() - RatioResults.NumbBelow);
	Statistics1 shrinkageStats;
	shrinkageStats.Clear();
	Statistics1 volumeStats;
	for ( int j = 1 ; j <= sectionHeight ; j++ )
	{
		for ( int k = 1 ; k <= sectionWidth ; k++ )
		{ 
			if ( BeforeAfter[Array][Section][j][k].BeforeIndex >= 0 
				&& BeforeAfter[Array][Section][j][k].AfterIndex >= 0 
				&& BeforeAfter[Array][Section][j][k].QuantStatus )
			{
				shrinkageStats.Accumulate(BeforeAfter[Array][Section][j][k].Shrinkage);
				int iTmp = BeforeAfter[Array][Section][j][k].BeforeIndex;
				volumeStats.Accumulate(BeforeSizingList[iTmp].ShapeSummary.FillRatio);
			}
		}
	}
	shrinkageStats.Analyze();
	volumeStats.Analyze();
	
	abRatioOutput << "=====,Quantitation Results - After/Before Ratio\n";
	abRatioOutput << abRatioOutputName.c_str() << "\n";
	abRatioOutput << "Array,Section\n";
	abRatioOutput << Array << "," << Section << "\n\n";
	abRatioOutput << "Number of Droplets\n";
	abRatioOutput << "Total,Positive\n";
	abRatioOutput << RatioResults.Data.size() << "," << numbPositive << "\n\n";
	abRatioOutput << "Total,Filling Ratio,,Pos/Neg,Shrinkage,,,Threshold,Pos/Neg,Ratio Def.,Otsu\n";
	abRatioOutput << "Volume,Ave,Std Dev,Threshold,Ave,Std Dev,,Method,Ratio,Divider.,Threshold\n";
	abRatioOutput << volumeStats.Sum() << "," << volumeStats.Ave() << "," << volumeStats.Std() 
		<< "," << RatioResults.Threshold 
		<< "," << shrinkageStats.Ave() << "," << shrinkageStats.Std() << ",," << RatioResults.ThresholdMethod 
		<< "," << PosNegRatio << "," << RatioDefaultDivider << "," << RatioResults.OtsuThreshold << "\n\n";
	abRatioOutput << "Ratio stats for All, Occupied and Unoccupied droplets\n";
	abRatioOutput << ",Numb,Average,Std Dev\n";
	abRatioOutput << "All," << RatioStats.Count() << "," << RatioStats.Ave() << "," << RatioStats.Std() << "\n";
	abRatioOutput << "Unoccupied," << BelowStats.Count() << "," << BelowStats.Ave() << "," << BelowStats.Std() << "\n";
	abRatioOutput << "Occupied," << AboveStats.Count() << "," << AboveStats.Ave() << "," << AboveStats.Std() << "\n\n";
	abRatioOutput << "Histogram of Ratios,,(Bin Value is upper bound)\nBinValue,Count\n";
	for ( int n = 0 ; n < RatioHistogram.size() ; n++)
		abRatioOutput << RatioHistogram[n].first << "," << RatioHistogram[n].second << "\n";
	
	abRatioOutput.close();
	return true;		
}

bool OutputDropletAveIntensityData(std::ofstream &LogFile, std::string &BaseName, 
	int Array, int Section, cv::Size SectionDimens, 
	std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter,
	std::vector< BlobSummary > &BeforeSizingList, std::vector< double> BeforeResults, double BeforeThreshold, 
	MinMaxGroup &BeforeParameters, double BeforeExclusionFactor, 
	std::vector< std::pair< double, int > > &AveIntenHistogram, Statistics1 &AveIntenStats, 
	double AveIntenThreshold, int NumbAcceptableBeforeDroplets)
{
	if ( BeforeSizingList.size() == 0 )
	{
		LogFile << "BeforeSizingList is empty for Array " << Array << ", Section " << Section << "\n";
		return false;
	}
	
	// int sectionHeight = SectionDimens.height;
	// int sectionWidth = SectionDimens.width;
	// cv::Mat bgr[3];
	std::ostringstream oString;

	// oString << BaseName << "_230_" << Array << "_" << Section << "_B_AveInt.txt";
	oString.str("");
	oString.clear();
	oString << BaseName << "_231_" << Array << "_" << Section << "_B_AveInt.csv";
	std::string beforeAveOutName(oString.str());

	std::ofstream beforeAveOutput;
	beforeAveOutput.open(beforeAveOutName.c_str());
	if ( !beforeAveOutput.is_open() )
	{
		oString.str("");
		oString.clear();
		oString << "Problems opening: " <<  beforeAveOutName;
		LogFile << oString.str();
		return false;
	}
	
	int numbRejected = AveIntenStats.Count() - NumbAcceptableBeforeDroplets;
	beforeAveOutput << "=====,Ave Intensity of Before Droplets\n";
	beforeAveOutput << beforeAveOutName.c_str() << "\n";
	beforeAveOutput << "Array,Section\n";
	beforeAveOutput << Array << "," << Section << "\n\n";
	beforeAveOutput << "Droplets checked by Ave Intensity\n";
	beforeAveOutput << "Total,Accepted,Rejected\n";
	beforeAveOutput << AveIntenStats.Count() << "," << NumbAcceptableBeforeDroplets << "," << numbRejected << "\n\n";
	beforeAveOutput << "Ave Intensity Results\n";
	beforeAveOutput << "Count,Ave,Std\n";
	beforeAveOutput << AveIntenStats.Count() << "," << AveIntenStats.Ave() << "," << AveIntenStats.Std() << "\n\n";
	beforeAveOutput << ",,Before Exclusion\n";
	beforeAveOutput << "Threshold,,Factor\n";
	beforeAveOutput << AveIntenThreshold << ",," << BeforeExclusionFactor << "\n\n";
	beforeAveOutput << "Histogram of Ave Intensities\nBinValue,Count\n";
	for ( int n = 0 ; n < AveIntenHistogram.size() ; n++)
		beforeAveOutput << AveIntenHistogram[n].first << "," << AveIntenHistogram[n].second << "\n";
	
	beforeAveOutput.close();
	return true;
}
	
bool OutputDropletComparison(std::ofstream &LogFile, std::string BaseName,
	int Array, int Section, int ImageNumber, double Time, cv::Size SectionDimens, double WellArea, cv::Size2f WellSize2f,
	std::vector< std::vector< std::vector< std::vector< BeforeAfterMatrix > > > > &BeforeAfter,
	std::vector< BlobSummary > &BeforeSizingList, std::vector< double> BeforeResults, double BeforeThreshold, 
	MinMaxGroup &BeforeParameters, double ShrinkageLimit, double BeforeExclusionFactor, 
	std::vector< BlobSummary > &AfterSizingList, std::vector< double> AfterResults, double AfterThreshold, 
	MinMaxGroup &AfterParameters, double PosNegRatio, double RatioDefaultDivider, double RunSigmaMultiplier, 
	AfterBeforeResults &RatioResults, RunInformation &RunInfo,
	cv::Point ArraySectionShift, QuantitationResult &QResult, std::string &ErrorMsg )
{
	bool dropletsInList = true;
	if ( BeforeSizingList.size() == 0 )
	{
		LogFile << "BeforeSizingList is empty for Array " << Array << ", Section " << Section << "\n";
		dropletsInList = false;
	}
	if ( AfterSizingList.size() == 0 )
	{
		LogFile << "AfterSizingList is empty for Array " << Array << ", Section " << Section << "\n";
		dropletsInList = false;
	}
	if ( !dropletsInList )
		return false;
	
	int sectionHeight = SectionDimens.height;
	int sectionWidth = SectionDimens.width;
	// cv::Mat bgr[3];
	std::ostringstream oString;
	oString.clear();
	QResult.Clear();
	if ( dropletsInList )
	{
		oString.str("");
		oString.clear();
		oString << BaseName << "_" << ImageNumber << "_210_" << Array << "_" << Section << "_Bef_Aft.csv";
		std::string resultsBeforeAfterName(oString.str());
		

		// sortSummaryDelta = 0.5 * WellSpacing.y;
		// sortSummaryDeltaCol = 0.5 * WellSpacing.x;
		std::ofstream resultsBeforeAfter;
		
		resultsBeforeAfter.open(resultsBeforeAfterName.c_str());
		if ( !resultsBeforeAfter.is_open() )
		{
			oString.str("");
			oString.clear();
			oString << "Problems opening: " <<  resultsBeforeAfterName;
			ErrorMsg = oString.str();
			return false;
		}

		Statistics1 fillFraction;
		fillFraction.Clear();
		StatisticsXY inputDiffBefAft;
		inputDiffBefAft.Clear();
		int totalValid = 0;
		int totalValidAndOccupied = 0;
		int totalInvalid = 0;
		int totalInvalidAndOccupied = 0;
		int totalValidBeforeRunExclusion = 0;
		
		int acceptableBeforeDroplets = 0;
		int acceptableAfterDropletsMatched = 0;
		int unacceptableAfterDropletsMatched = 0;
		int beforeDropletsLost = 0;
		for ( int j = 1 ; j <= sectionHeight ; j++ )
		{
			for ( int k = 1 ; k <= sectionWidth ; k++ )
			{ 
				if ( BeforeAfter[Array][Section][j][k].BeforeIndex >= 0 )
				{
					int befIdx = BeforeAfter[Array][Section][j][k].BeforeIndex;
					if ( BeforeSizingList[befIdx].ShapeSummary.FilterValue == 1 && BeforeAfter[Array][Section][j][k].RelativeAveIntenStatus > 0 )
					{
						acceptableBeforeDroplets++;
						int aftIdx = BeforeAfter[Array][Section][j][k].AfterIndex;
						if ( aftIdx >= 0 )
						{
							if ( AfterSizingList[aftIdx].ShapeSummary.FilterValue == 1 && BeforeAfter[Array][Section][j][k].ShrinkageStatus > 0 )
								acceptableAfterDropletsMatched++;
							else
								unacceptableAfterDropletsMatched++;
								
						}
						else
							beforeDropletsLost++;
					}
				}
			}
		}
		int numbRunsExcluded = RunInfo.NumbRunsExcluded;
		int numbDropletsExcludedInRuns = RunInfo.NumbWellsExcluded;

		std::vector< Droplet > dropletList;
		dropletList.clear();
		Droplet newDroplet;
		newDroplet.Clear();
		for ( int j = 1 ; j <= sectionHeight ; j++ )
		{
			for ( int k = 1 ; k <= sectionWidth ; k++ )
			{ 
				if ( BeforeAfter[Array][Section][j][k].BeforeIndex >= 0 && BeforeAfter[Array][Section][j][k].AfterIndex >= 0 && !BeforeAfter[Array][Section][j][k].Mismatched )
				{
					int befIdx = BeforeAfter[Array][Section][j][k].BeforeIndex;
					int aftIdx = BeforeAfter[Array][Section][j][k].AfterIndex;
					BlobShapeSummary befShape = BeforeSizingList[befIdx].ShapeSummary;
					BlobShapeSummary aftShape = AfterSizingList[aftIdx].ShapeSummary;
					BeforeAfterMatrix current = BeforeAfter[Array][Section][j][k];
					cv::Point2f diff = befShape.InputCenter2f - aftShape.InputCenter2f;
					inputDiffBefAft.Accumulate(diff.x, diff.y);
					fillFraction.Accumulate(befShape.FillRatio);
					
					if ( !current.EdgeExcluded && !current.BeforeAnomalousFilling && !current.AfterAnomalousFilling
						&& current.RelativeAveIntenStatus > 0 
						&& current.ShrinkageStatus > 0 && befShape.FilterValue == 1 && aftShape.FilterValue == 1 
						&& !current.Mismatched )
					{
						totalValidBeforeRunExclusion++;
						if ( !current.RunExcluded )				
						{
							BeforeAfter[Array][Section][j][k].QuantStatus = true;
							totalValid++;
							if ( current.Occupied )
								totalValidAndOccupied++;
							QResult.FillingFraction.Accumulate(befShape.FillRatio);
							QResult.Shrinkage.Accumulate(current.Shrinkage);
							newDroplet.SelfIndex = aftIdx;
							if ( current.Occupied == 0 )
								newDroplet.Occupied = false;
							else
								newDroplet.Occupied = true;
							newDroplet.Volume = current.Shrinkage;
							dropletList.push_back(newDroplet);
						}
					}
					else
					{
						BeforeAfter[Array][Section][j][k].QuantStatus = false;
						totalInvalid++;
						if ( current.Occupied )
							totalInvalidAndOccupied++;
					}	
				}
			}
		}
		double mu;
		double sigma;
		bool lowerBound;
		bool upperBound;
		double volumeError = 0.05;
		LogFile << "compareDroplets: dropletList.size() = " << dropletList.size() << std::endl;
		if ( dropletList.size() > 10 )
			bool qLimit = CalcConcentration( LogFile, dropletList, volumeError, 
				mu, sigma, lowerBound, upperBound, false );
		else
		{
			LogFile << "Too few droplets";
			mu = 0.0;
			sigma = 1.0;
			lowerBound = true;
			upperBound = true;
		}
		QResult.Array = Array;
		QResult.Section = Section;
		QResult.NumbDroplets = totalValid;
		QResult.NumbOccupied = totalValidAndOccupied;
		QResult.FillingFraction.Analyze();
		QResult.Shrinkage.Analyze();
		QResult.Concentration = mu;
		QResult.ConcSigma = sigma;
		inputDiffBefAft.Analyze();
		fillFraction.Analyze();
		std::ostringstream hString1;
		std::ostringstream hString2;
		std::ostringstream hString3;
		std::ostringstream hString4;
		std::ostringstream hString5;
		hString1.str("");
		hString1.clear();
		hString2.str("");
		hString2.clear();
		hString3.str("");
		hString3.clear();
		hString4.str("");
		hString4.clear();
		hString5.str("");
		hString5.clear();
		resultsBeforeAfter << "=====,Sizing and Quantitation Analysis of Droplets in Before and After Images,,,,,,,,"
			<< "After Image Number,," << ImageNumber << ",Time" << Time << "\n";
		hString1 << "=====,,,,,,,,,";
		hString2 << "," << resultsBeforeAfterName.c_str() << ",,,,,,,,";
		hString3 << ",,,,,,,,,";
		hString4 << ",Number of Droplets,," << totalValid << ",,,,,,";
		hString5 << ",Number Occupied,," << totalValidAndOccupied << ",,,,,,";
		
		hString1 << ",,Numb,Fraction";
		hString2 << "Before Droplets,," << acceptableBeforeDroplets << ",";
		hString3 << "Acceptable After,," << acceptableAfterDropletsMatched << "," << (double)acceptableAfterDropletsMatched / (double)acceptableBeforeDroplets;
		hString4 << "Unacceptable After,," << unacceptableAfterDropletsMatched << "," << (double)unacceptableAfterDropletsMatched / (double)acceptableBeforeDroplets;
		hString5 << "Missing Droplets,," << beforeDropletsLost << "," << (double)beforeDropletsLost / (double)acceptableBeforeDroplets;
		
		hString1 << ",,Run Analysis (Wald - Wolfowitz)";
		hString2 << ",,,Numb Runs,Mu,Sigma";
		hString3 << ",,Initial," << RunInfo.NumbRuns[0] << "," << RunInfo.Mu[0] << "," << RunInfo.Sigma[0];
		if ( RunInfo.NumbRuns.size() > 1 )
		{
			int last = (int)RunInfo.NumbRuns.size() - 1;
			hString4 << ",,Final," << RunInfo.NumbRuns[last] << "," << RunInfo.Mu[last] << "," << RunInfo.Sigma[last];
		}
		else
			hString4 << ",,,,,";
		
		hString5 << ",,Runs Excluded:,," << RunInfo.NumbRunsExcluded << ",Wells Excluded:,," << RunInfo.NumbWellsExcluded
			<< ",Valid Before Run Exclusion:,,," << totalValidBeforeRunExclusion;
		LogFile << "RunInfo.NumbRuns.size() = " << RunInfo.NumbRuns.size() << std::endl;
		resultsBeforeAfter << hString1.str() << "\n";
		resultsBeforeAfter << hString2.str() << "\n";
		resultsBeforeAfter << hString3.str() << "\n";
		resultsBeforeAfter << hString4.str() << "\n";
		resultsBeforeAfter << hString5.str() << "\n";
		
		hString1.str("");
		hString1.clear();
		hString2.str("");
		hString2.clear();
		hString3.str("");
		hString3.clear();
		hString1 << "\nWell,,,";
		hString1 << ",Before Image";
		hString1 << ",,,,,,,Height,,Width,,Area,,Circularity,,As Ratio,,,,";
		hString1 << ",,,,,After Image";
		hString1 << ",,,,,,,Height,,Width,,Area,,Circularity,,As Ratio,,,,";
		hString1 << ",,,,,Input Pos Diff,,,,Quantitation,Aft/Bef,Accpt.,Droplets,",
		hString1 << "Shrinkage,Before,Pos-Neg,Ave Inten,Run Sigma";
		// hString1 << ",Comparison";
		resultsBeforeAfter << hString1.str() << "\n";
		hString1.str("");
		hString1.clear();
		hString1 << "Height,Width,Area,";
		hString1 << ",Numb,Results[0],Results[1],Results[2],Results[3],Threshold";
		hString1 << ",,Min,Max,Min,Max,Min,Max,Min,Max,Min,Max,,,";
		hString1 << ",,,,,Numb,Results[0],Results[1],Results[2],Results[3],Threshold";
		hString1 << ",,Min,Max,Min,Max,Min,Max,Min,Max,Min,Max,,,";
		hString1 << ",,,,,Row Ave,Row Std,Col Ave,Col Std,Method,Threshold,Numb,Occupied,";
		hString1 << "Factor,Exclusion,Ratio,Threshold,Mulitplier";
		resultsBeforeAfter << hString1.str() << "\n";
		
		resultsBeforeAfter << WellSize2f.height << "," << WellSize2f.width << "," << WellArea << ",,";
		resultsBeforeAfter << BeforeSizingList.size() << "," << BeforeResults[0] << "," << BeforeResults[1] << "," << BeforeResults[2] << "," << BeforeResults[3] 
			<< "," << BeforeThreshold << ",";
		resultsBeforeAfter << "," << BeforeParameters.BlobHeight.Min << "," << BeforeParameters.BlobHeight.Max
			<< "," << BeforeParameters.BlobWidth.Min << "," << BeforeParameters.BlobWidth.Max
			<< "," << BeforeParameters.BlobArea.Min << "," << BeforeParameters.BlobArea.Max
			<< "," << BeforeParameters.Circularity.Min << "," << BeforeParameters.Circularity.Max
			<< "," << BeforeParameters.AspectRatio.Min << "," << BeforeParameters.AspectRatio.Max;
		resultsBeforeAfter << ",,,,,,,,";
		resultsBeforeAfter << AfterSizingList.size() << "," << AfterResults[0] << "," << AfterResults[1] << "," << AfterResults[2] << "," << AfterResults[3] 
			<< "," << AfterThreshold << ",";
		resultsBeforeAfter << "," << AfterParameters.BlobHeight.Min << "," << AfterParameters.BlobHeight.Max
			<< "," << AfterParameters.BlobWidth.Min << "," << AfterParameters.BlobWidth.Max
			<< "," << AfterParameters.BlobArea.Min << "," << AfterParameters.BlobArea.Max
			<< "," << AfterParameters.Circularity.Min << "," << AfterParameters.Circularity.Max
			<< "," << AfterParameters.AspectRatio.Min << "," << AfterParameters.AspectRatio.Max;
		resultsBeforeAfter << ",,,,";
		resultsBeforeAfter << ",,,," << inputDiffBefAft.AveY() << "," << inputDiffBefAft.StdY() << "," << inputDiffBefAft.AveX() << "," << inputDiffBefAft.StdX() 
			<< "," << RatioResults.ThresholdMethod << "," << RatioResults.Threshold 
			<< "," << totalValid << "," << totalValidAndOccupied 
			<< "," << ShrinkageLimit << "," << BeforeExclusionFactor << "," << PosNegRatio << "," << RatioDefaultDivider << "," << RunSigmaMultiplier; 
		resultsBeforeAfter << "\n\n";
		
		hString2 << "Array,Section,SectionPosition,";
		hString3 << ",,Row,Col";
		hString2 << ",Index,Input Center,";
		hString3 << ",,Row,Col";
		hString2 << ",Filter Results,,Anomalous,Image";
		hString3 << ",Value,String,Filling,Legend";
		hString2 << ",Contour,Perimeter,Area,Mom00";
		hString3 << ",Length,,,";
		hString2 << ",Bounding Box,,Circularity,Aspect";
		hString3 << ",Height,Width,,Ratio";
		hString2 << ",Top Intensities,,";
		hString3 << ",Count,Ave,Std";
		hString2 << ",Fill,Fill";
		hString3 << ",Intensity,Ratio";
		hString2 << ",Blob Intensities,,";
		hString3 << ",Count,Ave,Total";
		hString2 << ",";
		hString3 << ",";
		hString2 << ",Index,Input Center,";
		hString3 << ",,Row,Col";
		hString2 << ",Filter Results,,Anomalous,Image";
		hString3 << ",Value,String,Filling,Legend";
		hString2 << ",Contour,Perimeter,Area,Mom00";
		hString3 << ",Length,,,";
		hString2 << ",Bounding Box,,Circularity,Aspect";
		hString3 << ",Height,Width,,Ratio";
		hString2 << ",Top Intensities,,";
		hString3 << ",Count,Ave,Std";
		hString2 << ",Fill,Fill";
		hString3 << ",Intensity,Ratio";
		hString2 << ",Blob Intensities,,";
		hString3 << ",Count,Ave,Total";
		hString2 << ",";
		hString3 << ",";
		hString2 << ",Input Pos. Diff,";
		hString3 << ",Row,Col";
		hString2 << ",Quant,Acceptable,All";
		hString3 << ",,Occupied,Occupied";
		hString2 << ",Aft/Bef,Shrinkage,,Bef AveInten,,Edge,Run";
		hString3 << ",Ratio,Status,Value,Status,Value,Excluded,Excluded";
		
		resultsBeforeAfter << hString2.str() << "\n";
		resultsBeforeAfter << hString3.str() << "\n";
		
		for ( int j = 1 ; j <= sectionHeight ; j++ )
		{
			for ( int k = 1 ; k <= sectionWidth ; k++ )
			{
				if ( BeforeAfter[Array][Section][j][k].BeforeIndex >= 0 
					&& BeforeAfter[Array][Section][j][k].AfterIndex >= 0 
					&& !BeforeAfter[Array][Section][j][k].Mismatched )
				{
					int befIdx = BeforeAfter[Array][Section][j][k].BeforeIndex;
					int aftIdx = BeforeAfter[Array][Section][j][k].AfterIndex;
					BlobShapeSummary befShape = BeforeSizingList[befIdx].ShapeSummary;
					BlobShapeSummary aftShape = AfterSizingList[aftIdx].ShapeSummary;
					BeforeAfterMatrix current = BeforeAfter[Array][Section][j][k];
					std::string beforeResultsString;
					std::string afterResultsString;		
					CreateDropletImageStrings(befIdx, aftIdx, befShape, aftShape, 
						current, beforeResultsString, afterResultsString );
					resultsBeforeAfter << Array << "," << Section << "," << j << "," << k;
					resultsBeforeAfter << "," << BeforeSizingList[befIdx].SelfIndex;
					resultsBeforeAfter << "," << befShape.InputCenter2f.y << "," << befShape.InputCenter2f.x << "," << befShape.FilterValue << "," << befShape.FilterString;
					resultsBeforeAfter << "," << befShape.AnomalousFilling << "," << beforeResultsString;				
					resultsBeforeAfter << "," << befShape.ContourLength << "," << befShape.Perimeter << "," << befShape.Area << "," << befShape.Mom.m00;
					resultsBeforeAfter << "," << befShape.BoundingRect.height << "," << befShape.BoundingRect.width << "," << befShape.Circularity << "," << befShape.AspectRatio;
					resultsBeforeAfter << "," << befShape.TopIntensities.Count << "," << befShape.TopIntensities.Ave << "," << befShape.TopIntensities.Std;
					resultsBeforeAfter << "," << befShape.FillIntensity << "," << befShape.FillRatio;
					resultsBeforeAfter << "," << befShape.BlobIntensities.Count << "," << befShape.BlobIntensities.Ave << "," << befShape.BlobIntensities.Sum;
					resultsBeforeAfter << ",";
					resultsBeforeAfter << "," << AfterSizingList[aftIdx].SelfIndex;
					resultsBeforeAfter << "," << aftShape.InputCenter2f.y << "," << aftShape.InputCenter2f.x << "," << aftShape.FilterValue << "," << aftShape.FilterString;
					resultsBeforeAfter << "," << aftShape.AnomalousFilling << "," << afterResultsString;					
					resultsBeforeAfter << "," << aftShape.ContourLength << "," << aftShape.Perimeter << "," 	<< aftShape.Area << "," << aftShape.Mom.m00;
					resultsBeforeAfter << "," << aftShape.BoundingRect.height << "," << aftShape.BoundingRect.width << "," << aftShape.Circularity << "," << aftShape.AspectRatio;
					resultsBeforeAfter << "," << aftShape.TopIntensities.Count << "," << aftShape.TopIntensities.Ave << "," << aftShape.TopIntensities.Std;
					resultsBeforeAfter << "," << aftShape.FillIntensity << "," << aftShape.FillRatio;
					resultsBeforeAfter << "," << aftShape.BlobIntensities.Count << "," << aftShape.BlobIntensities.Ave << "," << aftShape.BlobIntensities.Sum;
					resultsBeforeAfter << ",";
					cv::Point2f diff = befShape.InputCenter2f - aftShape.InputCenter2f;
					
					int quantStatus = BoolToInt(BeforeAfter[Array][Section][j][k].QuantStatus);
					resultsBeforeAfter << "," << diff.y << "," << diff.x;
					resultsBeforeAfter << "," << quantStatus << "," << quantStatus * current.Occupied << "," << current.Occupied;
					resultsBeforeAfter << "," << current.AfterBeforeRatio << "," << current.ShrinkageStatus << "," << current.Shrinkage;
					resultsBeforeAfter << "," << current.RelativeAveIntenStatus << "," << current.RelativeAveInten;
					resultsBeforeAfter << "," << BoolToInt(current.EdgeExcluded) << "," << BoolToInt(current.RunExcluded);
					
					resultsBeforeAfter << "\n";
				}		
			}
		}

		resultsBeforeAfter << "\n\n";
		
		for ( int j = 1 ; j <= sectionHeight ; j++ )
		{
			for ( int k = 1 ; k <= sectionWidth ; k++ )
			{ 
				if ( BeforeAfter[Array][Section][j][k].BeforeIndex >= 0 
					&& BeforeAfter[Array][Section][j][k].AfterIndex >= 0  
					&& BeforeAfter[Array][Section][j][k].Mismatched )
				{
					int befIdx = BeforeAfter[Array][Section][j][k].BeforeIndex;
					int aftIdx = BeforeAfter[Array][Section][j][k].AfterIndex;
					BlobShapeSummary befShape = BeforeSizingList[befIdx].ShapeSummary;
					BlobShapeSummary aftShape = AfterSizingList[aftIdx].ShapeSummary;
					BeforeAfterMatrix current = BeforeAfter[Array][Section][j][k];
					std::string beforeResultsString;
					std::string afterResultsString;		
					CreateDropletImageStrings(befIdx, aftIdx, befShape, aftShape, 
						current, beforeResultsString, afterResultsString );
					resultsBeforeAfter << Array << "," << Section << "," << j << "," << k;
					resultsBeforeAfter << "," << BeforeSizingList[befIdx].SelfIndex;
					resultsBeforeAfter << "," << befShape.InputCenter2f.y << "," << befShape.InputCenter2f.x << "," << befShape.FilterValue << "," << befShape.FilterString;
					resultsBeforeAfter << "," << befShape.AnomalousFilling << "," << beforeResultsString;				
					resultsBeforeAfter << "," << befShape.ContourLength << "," << befShape.Perimeter << "," << befShape.Area << "," << befShape.Mom.m00;
					resultsBeforeAfter << "," << befShape.BoundingRect.height << "," << befShape.BoundingRect.width << "," << befShape.Circularity << "," << befShape.AspectRatio;
					resultsBeforeAfter << "," << befShape.TopIntensities.Count << "," << befShape.TopIntensities.Ave << "," << befShape.TopIntensities.Std;
					resultsBeforeAfter << "," << befShape.FillIntensity << "," << befShape.FillRatio;
					resultsBeforeAfter << "," << befShape.BlobIntensities.Count << "," << befShape.BlobIntensities.Ave << "," << befShape.BlobIntensities.Sum;
					resultsBeforeAfter << ",";
					resultsBeforeAfter << "," << AfterSizingList[aftIdx].SelfIndex;
					resultsBeforeAfter << "," << aftShape.InputCenter2f.y << "," << aftShape.InputCenter2f.x << "," << aftShape.FilterValue << "," << aftShape.FilterString;
					resultsBeforeAfter << "," << aftShape.AnomalousFilling << "," << afterResultsString;					
					resultsBeforeAfter << "," << aftShape.ContourLength << "," << aftShape.Perimeter << "," 	<< aftShape.Area << "," << aftShape.Mom.m00;
					resultsBeforeAfter << "," << aftShape.BoundingRect.height << "," << aftShape.BoundingRect.width << "," << aftShape.Circularity << "," << aftShape.AspectRatio;
					resultsBeforeAfter << "," << aftShape.TopIntensities.Count << "," << aftShape.TopIntensities.Ave << "," << aftShape.TopIntensities.Std;
					resultsBeforeAfter << "," << aftShape.FillIntensity << "," << aftShape.FillRatio;
					resultsBeforeAfter << "," << aftShape.BlobIntensities.Count << "," << aftShape.BlobIntensities.Ave << "," << aftShape.BlobIntensities.Sum;
					resultsBeforeAfter << ",";
					cv::Point2f diff = befShape.InputCenter2f - aftShape.InputCenter2f;
					
					int quantStatus = BoolToInt(BeforeAfter[Array][Section][j][k].QuantStatus);
					resultsBeforeAfter << "," << diff.y << "," << diff.x;
					resultsBeforeAfter << "," << quantStatus << "," << quantStatus * current.Occupied << "," << current.Occupied;
					resultsBeforeAfter << "," << current.AfterBeforeRatio << "," << current.ShrinkageStatus << "," << current.Shrinkage;
					resultsBeforeAfter << "," << current.RelativeAveIntenStatus << "," << current.RelativeAveInten;
					resultsBeforeAfter << "," << BoolToInt(current.EdgeExcluded) << "," << BoolToInt(current.RunExcluded);
					
					resultsBeforeAfter << "\n";
				}		
			}
		}

		resultsBeforeAfter << "\n\n";
		
		for ( int j = 1 ; j <= sectionHeight ; j++ )
		{
			for ( int k = 1 ; k <= sectionWidth ; k++ )
			{ 
				if ( BeforeAfter[Array][Section][j][k].BeforeIndex >= 0 && BeforeAfter[Array][Section][j][k].AfterIndex < 0 )
				{
					int befIdx = BeforeAfter[Array][Section][j][k].BeforeIndex;
					BlobShapeSummary befShape = BeforeSizingList[befIdx].ShapeSummary;
					BeforeAfterMatrix current = BeforeAfter[Array][Section][j][k];
					std::string beforeResultsString;
					CreateDropletBeforeImageStrings(befIdx, befShape,  
						current, beforeResultsString );
					
					resultsBeforeAfter << Array << "," << Section << "," << j << "," << k;
					resultsBeforeAfter << "," << BeforeSizingList[befIdx].SelfIndex;
					resultsBeforeAfter << "," << befShape.InputCenter2f.y << "," << befShape.InputCenter2f.x << "," << befShape.FilterValue << "," << befShape.FilterString;
					resultsBeforeAfter << "," << befShape.AnomalousFilling << "," << beforeResultsString;
					resultsBeforeAfter << "," << befShape.ContourLength << "," << befShape.Perimeter << "," << befShape.Area << "," << befShape.Mom.m00;
					resultsBeforeAfter << "," << befShape.BoundingRect.height << "," << befShape.BoundingRect.width << "," << befShape.Circularity << "," << befShape.AspectRatio;
					resultsBeforeAfter << "," << befShape.TopIntensities.Count << "," << befShape.TopIntensities.Ave << "," << befShape.TopIntensities.Std;
					resultsBeforeAfter << "," << befShape.FillIntensity << "," << befShape.FillRatio;
					resultsBeforeAfter << "," << befShape.BlobIntensities.Count << "," << befShape.BlobIntensities.Ave << "," << befShape.BlobIntensities.Sum;
					
					resultsBeforeAfter << "\n";
				}
			}
		}
		
		resultsBeforeAfter << "\n";
		for ( int j = 1 ; j <= sectionHeight ; j++ )
		{
			for ( int k = 1 ; k <= sectionWidth ; k++ )
			{ 
				if ( BeforeAfter[Array][Section][j][k].BeforeIndex < 0 && BeforeAfter[Array][Section][j][k].AfterIndex >= 0 )
				{
					int aftIdx = BeforeAfter[Array][Section][j][k].AfterIndex;
					BlobShapeSummary aftShape = AfterSizingList[aftIdx].ShapeSummary;
					BeforeAfterMatrix current = BeforeAfter[Array][Section][j][k];
					std::string afterResultsString;							
					CreateDropletAfterImageStrings(aftIdx, aftShape, 
						current, afterResultsString );
					
					resultsBeforeAfter << Array << "," << Section << "," << j << "," << k;
					
					resultsBeforeAfter << ",,,," << ",," << ",,,," << ",,,,"<< ",,," << ",," << ",,,";
					resultsBeforeAfter << ",,";
					resultsBeforeAfter << "," << AfterSizingList[aftIdx].SelfIndex;
					resultsBeforeAfter << "," << aftShape.InputCenter2f.y << "," << aftShape.InputCenter2f.x << "," << aftShape.FilterValue << "," << aftShape.FilterString;
					resultsBeforeAfter << "," << aftShape.AnomalousFilling << "," << afterResultsString;
					resultsBeforeAfter << "," << aftShape.ContourLength << "," << aftShape.Perimeter << "," 	<< aftShape.Area << "," << aftShape.Mom.m00;
					resultsBeforeAfter << "," << aftShape.BoundingRect.height << "," << aftShape.BoundingRect.width << "," << aftShape.Circularity << "," << aftShape.AspectRatio;
					resultsBeforeAfter << "," << aftShape.TopIntensities.Count << "," << aftShape.TopIntensities.Ave << "," << aftShape.TopIntensities.Std;
					resultsBeforeAfter << "," << aftShape.FillIntensity << "," << aftShape.FillRatio;
					resultsBeforeAfter << "," << aftShape.BlobIntensities.Count << "," << aftShape.BlobIntensities.Ave << "," << aftShape.BlobIntensities.Sum;
					resultsBeforeAfter << "\n";
				}
			}
		}
		resultsBeforeAfter.close();
	}
	return true;
}


// Before image
	// Green outline - passed filter tests
	// 		Green plus: used in quantitation
	// 		Red plus: failed shrinkage
	// Blue outline
	//		Empty: Before blob failed RelativeAveIntenStatus
	//		Blue vert bar: Before blob passed filter test, but had anomalous shape
	//		Blue plus: Before and after blobs passed filter test, but had anomalous shapes
	// Red outline
	// 		Red vert bar: Before image failed filter value
	// 		Red horz bar: after image failed filter value or not present
// After Image
	// Green outline - droplet used
	// 		Green plus: occupied
	// 		Red plus: unoccupied
	// Blue outline
	// 		Red Vertical bar: Edge Excluded
	// 		Red Horz bar: Run excluded
	//		Blue horz bar: After blob passed filter test, but had anomalous shape
	//		Blue plus: Before and after blobs passed filter test, but had anomalous shapes
	// Red outline
	// 		Red vert bar: After image failed filter value
	// 		Red horz bar: Before image failed filter value or was not present		
	//		Red plus: After image failed filter value AND Before image failed filter value or was not present
	//
// Colors: 0 = Green, 1 = Blue, 2 = Red
// Symbols: -1 = No Symbol, 0 = Plus, 1 = Vert Bar, 2 = Horz Bar
//
void CreateDropletImageStrings(int BeforeIndex, int AfterIndex, 
	BlobShapeSummary &BeforeShape, BlobShapeSummary &AfterShape, 
	BeforeAfterMatrix &CurrentElement, 
	std::string &BeforeResultString, std::string &AfterResultString )
{
	std::ostringstream oString;
	int color = 0;
	int symbol = 0;
	int symbolColor = 0;
	if ( BeforeIndex >= 0 )
	{
		oString.str("");
		oString.clear();
		if ( CurrentElement.Mismatched )
		{
			color = 1;
			symbol = -1;
			symbolColor = -1;
		}
		else if ( BeforeShape.FilterValue == 1 )
		{
			if ( CurrentElement.QuantStatus )
			{
				color = 0;
				symbol = 0;
				symbolColor = 0;
			}
			else if ( CurrentElement.RelativeAveIntenStatus == 0 )
			{
				color = 1;
				symbol = 2;
				symbolColor = 1;
			}
			else if ( CurrentElement.BeforeAnomalousFilling && !CurrentElement.AfterAnomalousFilling )
			{
				color = 1;
				symbol = 1;
				symbolColor = 1;
			}
			else if ( CurrentElement.BeforeAnomalousFilling && CurrentElement.AfterAnomalousFilling )
			{
				color = 1;
				symbol = 0;
				symbolColor = 1;
			}
			else if ( AfterShape.FilterValue == 1 )
			{
				color = 0;
				symbol = 2;
				symbolColor = 2;
			}
			else
			{
				color = 2;
				symbol = 2;
				symbolColor = 2;
			}
		}
		else
		{
			color = 2;
			symbolColor = 2;
			if ( AfterIndex < 0 || AfterShape.FilterValue != 1  )
				symbol = 0;
			else
				symbol = 1;
		}
		if ( color == 0 )
			oString	<< "G /";
		else if ( color == 1 )
			oString << "B /";
		else
			oString << "R /";
		
		if ( symbol == 0 )
			oString << " + /";
		else if ( symbol == 1 )
			oString << " | /";
		else if ( symbol == 2 )
			oString << " - /";
		else
			oString << "  /";
		
		if ( symbolColor == 0 )
			oString << " G";
		else if ( symbolColor == 1 )
			oString << " B";
		else if ( symbolColor == 2 )
			oString << " R";
		else
			oString << " ";
		
		BeforeResultString = oString.str();
	}
	else
		BeforeResultString = " / /";
	
	
	if ( AfterIndex >= 0 )
	{
		color = 0;
		symbol = 0;
		symbolColor = 0;
		oString.str("");
		oString.clear();
		if ( CurrentElement.Mismatched )
		{
			color = 1;
			symbol = -1;
			symbolColor = -1;
		}
		else if ( CurrentElement.QuantStatus )
		{
			color = 0;
			symbol = 0;
			if ( CurrentElement.Occupied )
				symbolColor = 0;
			else
				symbolColor = 2;
		}
		else if ( CurrentElement.EdgeExcluded )
		{
			color = 1;
			symbol = 2;
			symbolColor = 2;
		}
		else if ( CurrentElement.RunExcluded )
		{
			color = 1;
			symbol = 1;
			symbolColor = 2;
		}
		else if ( CurrentElement.AfterAnomalousFilling && !CurrentElement.BeforeAnomalousFilling )
		{
			color = 1;
			symbol = 2;
			symbolColor = 1;
		}
		else if ( CurrentElement.AfterAnomalousFilling && CurrentElement.BeforeAnomalousFilling )
		{
			color = 1;
			symbol = 0;
			symbolColor = 1;
		}
		else
		{
			color = 2;
			symbolColor = 2;
			if ( AfterShape.FilterValue != 1 )
				symbol = 1;
			else
				symbol = -1;

			if ( BeforeIndex < 0 || BeforeShape.FilterValue != 1 
				|| CurrentElement.RelativeAveIntenStatus == 0 )
			{
				if ( symbol == 1 )
					symbol = 0;
				else
					symbol = 2;
			}
			
			if ( symbol == -1 )
				symbolColor = -1;
		}
		
		if ( color == 0 )
			oString	<< "G /";
		else if ( color == 1 )
			oString << "B /";
		else
			oString << "R /";
		
		if ( symbol == 0 )
			oString << " + /";
		else if ( symbol == 1 )
			oString << " | /";
		else if ( symbol == 2 )
			oString << " - /";
		else
			oString << "  /";
		
		if ( symbolColor == 0 )
			oString << " G";
		else if ( symbolColor == 1 )
			oString << " B";
		else if ( symbolColor == 2 )
			oString << " R";
		else
			oString << " ";
		
		AfterResultString = oString.str();
	}
	else
		AfterResultString = " / /";
}

// Before image
	// Green outline - passed filter tests
	// 		Green plus: used in quantitation
	// 		Red plus: failed shrinkage
	// Blue outline - failed RelativeAveIntenStatus
	// Red outline
	// 		Red vert bar: Before image failed filter value
	// 		Red horz bar: after image failed filter value or not present
// After Image
	// Green outline - droplet used
	// 		Green plus: occupied
	// 		Red plus: unoccupied
	// Blue outline
	// 		Red Vertical bar: Edge Excluded
	// 		Red Horz bar: Run excluded
	// Red outline
	// 		Red vert bar: After image failed filter value
	// 		Red horz bar: Before image failed filter value or was not present		
	//		Red plus: After image failed filter value AND Before image failed filter value or was not present
void CreateDropletBeforeImageStrings(int BeforeIndex, 
	BlobShapeSummary &BeforeShape, 
	BeforeAfterMatrix &CurrentElement, 
	std::string &BeforeResultString )
{
	std::ostringstream oString;
	int color = 0;
	int symbol = 0;
	int symbolColor = 0;
	if ( BeforeIndex >= 0 )
	{
		oString.str("");
		oString.clear();
		if ( BeforeShape.FilterValue == 1 )
		{
			if ( CurrentElement.QuantStatus )
			{
				color = 0;
				symbol = 0;
				symbolColor = 0;
			}
			else if ( CurrentElement.RelativeAveIntenStatus == 0 )
			{
				color = 1;
				symbol = -1;
				symbolColor = -1;
			}
			else if ( CurrentElement.BeforeAnomalousFilling && !CurrentElement.AfterAnomalousFilling )
			{
				color = 1;
				symbol = 1;
				symbolColor = 1;
			}
			else if ( CurrentElement.BeforeAnomalousFilling && CurrentElement.AfterAnomalousFilling )
			{
				color = 1;
				symbol = 0;
				symbolColor = 1;
			}
			else
			{
				color = 2;
				symbol = 2;
				symbolColor = 2;
			}
		}
		else
		{
			color = 2;
			symbolColor = 2;
			symbol = 0;

		}
		if ( color == 0 )
			oString	<< "G /";
		else if ( color == 1 )
			oString << "B /";
		else
			oString << "R /";
		
		if ( symbol == 0 )
			oString << " + /";
		else if ( symbol == 1 )
			oString << " | /";
		else if ( symbol == 2 )
			oString << " - /";
		else
			oString << "  /";
		
		if ( symbolColor == 0 )
			oString << " G";
		else if ( symbolColor == 1 )
			oString << " B";
		else if ( symbolColor == 2 )
			oString << " R";
		else
			oString << " ";
		
		BeforeResultString = oString.str();
	}
	else
		BeforeResultString = " / /";
}

void CreateDropletAfterImageStrings(int AfterIndex, 
	BlobShapeSummary &AfterShape, 
	BeforeAfterMatrix &CurrentElement, 
	std::string &AfterResultString )
{
	std::ostringstream oString;
	int color = 0;
	int symbol = 0;
	int symbolColor = 0;
	
	if ( AfterIndex >= 0 )
	{
		color = 0;
		symbol = 0;
		symbolColor = 0;
		oString.str("");
		oString.clear();
		
		if ( CurrentElement.QuantStatus )
		{
			color = 0;
			symbol = 0;
			if ( CurrentElement.Occupied )
				symbolColor = 0;
			else
				symbolColor = 2;
		}
		else if ( CurrentElement.EdgeExcluded )
		{
			color = 1;
			symbol = 2;
			symbolColor = 2;
		}
		else if ( CurrentElement.RunExcluded )
		{
			color = 1;
			symbol = 1;
			symbolColor = 2;
		}
		else if ( CurrentElement.AfterAnomalousFilling && !CurrentElement.BeforeAnomalousFilling )
		{
			color = 1;
			symbol = 2;
			symbolColor = 1;
		}
		else if ( CurrentElement.AfterAnomalousFilling && CurrentElement.BeforeAnomalousFilling )
		{
			color = 1;
			symbol = 0;
			symbolColor = 1;
		}
		else
		{
			color = 2;
			symbolColor = 2;
			if ( AfterShape.FilterValue != 1 )
				symbol = 0;
			else
				symbol = 2;
		}
		
		if ( color == 0 )
			oString	<< "G /";
		else if ( color == 1 )
			oString << "B /";
		else
			oString << "R /";
		
		if ( symbol == 0 )
			oString << " + /";
		else if ( symbol == 1 )
			oString << " | /";
		else if ( symbol == 2 )
			oString << " - /";
		else
			oString << "  /";
		
		if ( symbolColor == 0 )
			oString << " G";
		else if ( symbolColor == 1 )
			oString << " B";
		else if ( symbolColor == 2 )
			oString << " R";
		else
			oString << " ";
		
		AfterResultString = oString.str();
	}
	else
		AfterResultString = " / /";
}
