#ifndef TRIMRECTANGLE_HPP
#define TRIMRECTANGLE_HPP
//! [includes]
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

bool TrimRectangle(cv::Rect &Rectangle, cv::Size RectSize, cv::Size ImageSize );

bool TrimRectangle(cv::Rect &Rectangle, cv::Size ImageSize );

#endif // TRIMRECTANGLE_HPP

