#ifndef MEDIAN_ROTATE_HPP
#define MEDIAN_ROTATE_HPP

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <cstdio>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

void MedianFilter_RotateImage(cv::Mat &WorkImage1, cv::Mat &Image, bool UseMedianFilter, bool TransposeImage, bool Track, bool EightBit, std::string BaseName );

#endif  // 	MEDIAN_ROTATE_HPP
