#ifndef ROLLINGBALL32F_H
#define ROLLINGBALL32F_H

#include <vector>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdio>
// This code is a port from the imageJ plugin (written in java) to c++.  This code 
// uses the opencv object Mat instead of the java object ImageProcessor.

//  Notice in original java code.
/**	Based on the NIH Image Pascal version by Michael Castle and Janice 
	Keller of the University of Michigan Mental Health Research
	Institute. Rolling ball algorithm inspired by Stanley 
	Sternberg's article, "Biomedical Image Processing",
	IEEE Computer, January 1983.
*/

// class definition for Rolling ball.  The ball radius can be changed via the
// BuildRollingBall member function.  If the default constructor is used then
// the radius is 50 pixels.
class RollingBall32F
{
	public:
		std::vector<float> Data;
		int PatchWidth;
		int ShrinkFactor;
	
		RollingBall32F(int radius) 
		{
			int arcTrimPer;
			if (radius<=16) {
				ShrinkFactor = 1;
				arcTrimPer = 8; // trim 16% in x and y
			} else if (radius<=32) {
				ShrinkFactor = 2;
				arcTrimPer = 12; // trim 24% in x and y
			} else if (radius<=100) {
				ShrinkFactor = 4;
				arcTrimPer = 16; // trim 32% in x and y
			} else {
				ShrinkFactor = 8;
				arcTrimPer = 20; // trim 40% in x and y
			}
			// if (radius<=10) {
				// ShrinkFactor = 1;
				// arcTrimPer = 12; // trim 24% in x and y
			// } else if (radius<=30) {
				// ShrinkFactor = 2;
				// arcTrimPer = 12; // trim 24% in x and y
			// } else if (radius<=100) {
				// ShrinkFactor = 4;
				// arcTrimPer = 16; // trim 32% in x and y
			// } else {
				// ShrinkFactor = 8;
				// arcTrimPer = 20; // trim 40% in x and y
			// }
			// if (radius<=40) {
				// ShrinkFactor = 1;
				// arcTrimPer = 12; // trim 24% in x and y
			// } else if (radius<=80) {
				// ShrinkFactor = 2;
				// arcTrimPer = 16; // trim 32% in x and y
			// } else if (radius<=160) {
				// ShrinkFactor = 4;
				// arcTrimPer = 18; // trim 36% in x and y
			// } else {
				// ShrinkFactor = 8;
				// arcTrimPer = 20; // trim 40% in x and y
			// }
			BuildRollingBall32F(radius, arcTrimPer);
		}
		RollingBall32F()
		{
			ShrinkFactor = 1;
			BuildRollingBall32F(80, 16);
		}
		~RollingBall32F(){}
		
		// Computes the location of each point on the rolling ball patch relative to the 
		// center of the sphere containing it.  The patch is located in the top half 
		// of this sphere.  The vertical axis of the sphere passes through the center of 
		// the patch.  The projection of the patch in the xy-plane below is a square.
		void BuildRollingBall32F(int BallRadius, int ArcTrimPer)
		{
			float rSquare;        // rolling ball radius squared
			int xtrim;          // # of pixels trimmed off each end of ball to make patch
			int xval, yval;     // x,y-values on patch relative to center of rolling ball
			float smallBallRadius;// radius of rolling ball on small image.
			float diam; 			// diameter of rolling ball on small image.
			float temp;           // value must be >=0 to take square root
			int halfPatchWidth; // distance in x or y from center of patch to any edge
			int ballSize;       // size of rolling ball array (area in small image)
			
			smallBallRadius = (float)BallRadius/(float)ShrinkFactor;
			if (smallBallRadius < 1)
				smallBallRadius = 1;
			rSquare = smallBallRadius*smallBallRadius;
			diam = smallBallRadius * 2;
			xtrim = (int)( ArcTrimPer*diam )/100; // only use a patch of the rolling ball
			PatchWidth = (int)round(diam - xtrim - xtrim);
			halfPatchWidth = (int)round(smallBallRadius - xtrim);
			ballSize = (PatchWidth+1)*(PatchWidth+1);
			Data.resize(ballSize);

			for (int i=0; i<ballSize; i++) {
				xval = i % (PatchWidth+1) - halfPatchWidth;
				yval = i / (PatchWidth+1) - halfPatchWidth;
				temp = rSquare - (xval*xval) - (yval*yval);
				if (temp >= 0)
					Data[i] = sqrt(temp);
				else
					Data[i] = 0;
			}
			// std::clog << "\nBuildRollingBall32F\n";
			// std::clog << "smallBallRadius = " << smallBallRadius << "\n";
			// std::clog << "rSquare = " << rSquare << "\n";
			// std::clog << "diam = " << diam << "\n";
			// std::clog << "xtrim = " << xtrim << "\n";
			// std::clog << "PatchWidth = " << PatchWidth << "\n";
			// std::clog << "halfPatchWidth = " << halfPatchWidth << "\n";
			// std::clog << "ballSize = " << ballSize << "\n";
			// std::clog << "Data.size() = " << Data.size() << "\n";
			// for ( int n = 0 ; n < Data.size() ; n++ )
				// std::clog << n << " - " << Data[n] << "\n";
		}
		
		
};


/** Implements a rolling-ball algorithm for the removal of smooth continuous background
	from a two-dimensional gel image.  It rolls the ball (actually a square patch on the
	top of a sphere) on a low-resolution (by a factor of 'shrinkfactor' times) copy of
	the original image in order to increase speed with little loss in accuracy.  It uses
	interpolation and extrapolation to blow the shrunk image to full size.
*/
bool SubtractBackground32F(cv::Mat &Image, int BallRadius, cv::Mat &ImageOut );
bool SubtractBackground32F(cv::Mat &Image, int BallRadius, cv::Mat &ImageOut, cv::Mat &Background );

// Creates a lower resolution image for ball-rolling.
bool ShrinkImage32F(cv::Mat &Image, int ShrinkFactor, cv::Mat &SmallImage);

/** 'Rolls' a filtering object over a (shrunken) image in order to find the
	image's smooth continuous background.  For the purpose of explaining this
	algorithm, imagine that the 2D grayscale image has a third (height) dimension
	defined by the intensity value at every point in the image.  The center of
	the filtering object, a patch from the top of a sphere having radius BallRadius,
	is moved along each scan line of the image so that the patch is tangent to the
	image at one or more points with every other point on the patch below the
	corresponding (x,y) point of the image.  Any point either on or below the patch
	during this process is considered part of the background.  Shrinking the image
	before running this procedure is advised due to the fourth-degree complexity
	of the algorithm.
*/                                                                                                               
void RollBall32F(RollingBall32F &Ball, cv::Mat &ImageIn, cv::Mat &SmallImage, cv::Mat &Background );

/** Uses bilinear interpolation to find the points in the full-scale background
	given the points from the shrunken image background.  Since the shrunken background
	is found from an image composed of minima (over a sufficiently large mask), it
	is certain that no point in the full-scale interpolated background has a higher
	pixel value than the corresponding point in the original image
*/                                 
void InterpolateBackground32F(cv::Mat &Background, RollingBall32F &Ball);

/** Uses linear extrapolation to find pixel values on the top, left, right,
	and bottom edges of the background.  First it finds the top and bottom
	edge points by extrapolating from the edges of the calculated and
	interpolated background interior.  Then it uses the edge points on the
	new calculated, interpolated, and extrapolated background to find all
	of the left and right edge points.  If extrapolation yields values
	below zero or above 255, then they are set to zero and 255 respectively.
*/                                             
void ExtrapolateBackground32F(cv::Mat &Background, RollingBall32F &Ball);

#endif  // ROLLINGBALL32F_H