//
// bool CalcConcentration( std::ofstream &LogFile, std::vector<Droplet> &DropletList, double VolumeError, 
// double &Mu, double &Sigma, bool &LowerBound, bool &UpperBound,
// bool Debug )
// 
// (1) It calculates the concentration of analyte in units of molecules per well volume (Mu).
//
// (2) It also estimates an error in the calculated concentration.  There are two parts to this.  It generates
// a new set of occupancies using the calculated concentration from (1).  However, it also generates a simulated
// set of volumes using VolumeError as the standard deviation in the distribution of volumes (with the measured
// volume as the mean). So the calculation uses droplet occupancies simulated using the measured volumes, but
// simulated volumes for the droplets which differ from the measured ones.  This is repeated numbMC (=50) times
// and the standard deviation of the results is reported as Sigma.
//
//
// LogFile - reference to log file for the program
// DropletList - vector of <Droplet> which includes volume of droplet (filling fraction) and whether the
//        droplet is occupied.
// VolumeError - error used in simulating volume measurement errors. It is the standared deviation of the 
//        volume (filling fraction) distribution for each droplet, its volume is shifted by a gaussian 
//        distributed random deviate times VolumeError.  The simulation algorithm will not let this reduce
//        the volume to less than 0.5 times the actual measured values or increase to more than 1.5 times
//        its measured value.
//
// Mu - Concentration
// Sigma - Uncertainty in concentration
// LowerBound - true if the number of unoocupied droplets is less than fitGuard (= 2).  Concentration is
//         determined assuming there are two unoccupied droplets and yields a lower bound to the actual
//         concentration.
// UpperBound - true if the number of occupied droplets is less than fitGuard (= 2). Concentration is
//         determined assuming there are two occupied droplets and yields an upper bound to the actual
//         concentration
// Debug - If true additional information about the fitting process is writen into LogFile.
//
#include <string>
#include <sstream>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cmath>
#include <algorithm>
#include <vector>
#include <random>

#include "droplet.hpp"
#include "statistics.hpp"
#include "calcConcentration.hpp"

double NewtonRhapsonStep( double Numb, std::vector<std::pair< double, bool > > &Data,
	double Concentration0, double &ProbNOut, bool Debug );
	
double MLNStep( double Numb, double VTotal, std::vector<std::pair< double, bool > > &Data,
	double &Conc, double &CLow, double &CHigh, double Epsilon, double &FuncOut, double &ProbNOut, bool Debug);

double MLNStep( double Numb, double VTotal, std::vector<std::pair< double, bool > > &Data,
	double &Conc, double &CLow, double &CHigh, double Epsilon, 
	std::vector<double>CList, std::vector<double>FList, double &FuncOut, double &ProbNOut, bool Debug);

double FitData(std::vector< std::pair< double, bool> > &Data, int MaxIter, double Epsilon, 
		int &Iter, std::ostringstream &OString, bool Debug );

bool SortPairSmallToLarge(std::pair< double, bool> A, std::pair< double, bool> B)
{
	return A.first < B.first;
}
bool SortPairLargeToSmall(std::pair< double, bool> A, std::pair< double, bool> B)
{
	return A.first > B.first;
}
bool CalcConcentration( std::ofstream &LogFile, std::vector<Droplet> &DropletList, double VolumeError, 
	double &Mu, double &Sigma, bool &LowerBound, bool &UpperBound,
	bool Debug )
{
	int fitGuard = 2;
	int maxIter = 94;
	double epsilon = 1.0e-05;
	int numbMC = 50;
	std::ostringstream oString;
	oString.str("");
	oString.clear();
	unsigned int seed;
	std::random_device rd;  //Will be used to obtain a seed for the random number engine
	seed = rd();
	// seed = 129834765;
	std::mt19937 mtGen(seed);
	std::uniform_real_distribution<double> uniDistribution(0.0, 1.0);
	std::normal_distribution<double> normDistribution(0.0, 1.0);
	double totalVolume = 0.0;
	int numbOccupied = 0;
	int numbDroplets = DropletList.size();
	for ( int n = 0 ; n < numbDroplets ; n++ )
	{
		if ( DropletList[n].Occupied )
			numbOccupied++;	
		totalVolume += DropletList[n].Volume;
	}	
	int fitNumb = numbOccupied;
	std::vector< std::pair<double, bool> > data(numbDroplets);
	for (int n = 0; n < numbDroplets; n++)
	{
		data[n].first = DropletList[n].Volume;
		data[n].second = DropletList[n].Occupied;
	}
	if ( fitNumb < fitGuard )
	{
		LogFile << "!!! Number of Occupied Droplets = " << fitNumb << " of " << numbDroplets << " droplets is too small.\n"
			<< "Number of occupied droplets reset to " << fitGuard << ". Result is upper bound to actual concentration\n" << std::endl;
		
		int needed = fitGuard - fitNumb;
		std::sort(data.begin(), data.end(), SortPairSmallToLarge);
		int idx = 0;
		while (needed > 0)
		{
			if (!data[idx].second)
			{
				data[idx].second = true;
				needed--;
			}
		}
		/*int idx1 = 0;
		for ( int m = 0 ; m < needed ; m++ )
		{
			double vol1 = -1.0;
			for (int n = 0; n < numbDroplets; n++)
			{
				if (!DropletList[n].Occupied)
				{
					double vol3 = DropletList[n].Volume;
					if (vol3 > vol1)
					{
						vol1 = vol3;
						idx1 = n;
					}
				}
				DropletList[idx1].Occupied = true;
				fitNumb++;
			}
		}*/
		UpperBound = true;
		LowerBound = false;
	}
	else if ( fitNumb > numbDroplets - fitGuard )
	{
		LogFile << "!!! Number of Occupied Droplets = " << fitNumb << " of " << numbDroplets << " droplets is too large.\n"
			<< "Number of occupied droplets reset to " << numbDroplets - fitGuard << ". Result is lower bound to actual concentration\n" << std::endl;
		int needed = fitNumb - numbDroplets + fitGuard;
		int idx1 = 0;
		std::sort(data.begin(), data.end(), SortPairLargeToSmall);
		int idx = 0;
		while (needed > 0)
		{
			if (data[idx].second)
			{
				data[idx].second = false;
				needed--;
			}
		}
		/*for ( int m = 0 ; m < needed ; m++ )
		{
			double vol1 = 1.0E+37;
			for (int n = 0; n < numbDroplets; n++)
			{
				if (DropletList[n].Occupied)
				{
					double vol3 = DropletList[n].Volume;
					if (vol3 < vol1)
					{
						vol1 = vol3;
						idx1 = n;
					}
				}
				DropletList[idx1].Occupied = false;
				fitNumb--;
			}
		}*/
		LowerBound = true;
		UpperBound = false;
	}
	else
	{
		LowerBound = false;
		UpperBound = false;
	}
	
	/*std::vector< std::pair<double, bool> > data(numbDroplets);	
	for ( int n = 0 ; n < numbDroplets ; n++ )
	{
		data[n].first = DropletList[n].Volume;
		data[n].second = DropletList[n].Occupied;
	}*/
	std::sort(data.begin(), data.end(), SortPairSmallToLarge);
	int iter = 0;
	oString.str("");
	oString.clear();
	double concentration = FitData(data, maxIter, epsilon, 
		iter, oString, Debug );
	LogFile << oString.str();	
	Mu = concentration;
	for ( int n = 0 ; n < numbDroplets ; n++ )
	{
		DropletList[n].Simulate( mtGen,
			uniDistribution,
			normDistribution,
			VolumeError, concentration, numbMC );
	}
	
	Statistics1 simConcentrations;
	simConcentrations.Clear();
	for ( int m = 0 ; m < numbMC ; m++ )
	{
		for ( int n = 0 ; n < numbDroplets ; n++ )
		{
			data[n].first = DropletList[n].SimulVolumes[m];
			data[n].second = DropletList[n].SimulOccupied[m];
		}
		oString.str("");
		oString.clear();
		double concMC = FitData( data,  maxIter, epsilon, 
			iter, oString, Debug );
		simConcentrations.Accumulate(concMC);
	}
	
	simConcentrations.Analyze();
	Sigma = simConcentrations.Std();
	
	return (!LowerBound && !UpperBound );
}

double FitData(std::vector< std::pair< double, bool> > &Data, int MaxIter, double Epsilon, 
		int &Iter, std::ostringstream &OString, bool Debug )
{
	OString << "----- Preliminary Fit ----- (epsilon = " << Epsilon << ")\n";
	double totalVolume = 0.0;
	int fitNumb = 0;
	double numbDroplets = Data.size();
	// std::vector< double > tmpList(Data.size());
	for ( int n = 0 ; n < Data.size() ; n++ )
	{
		totalVolume += Data[n].first;
		if ( Data[n].second )
			fitNumb++;
	}
	double aveVolume = totalVolume / numbDroplets;
	double initConc = -log(1.0 - fitNumb/numbDroplets )/aveVolume;
	double conc = initConc;
	double probNOut;
	Iter = 0;
	for ( int i = 0; i < MaxIter ; i++)
	{
		OString << "+++++ Iter = " << i 
			<< ", Numb Droplets = " << numbDroplets
			<< ", Numb Occupied = " << fitNumb
			<< ", conc = " << conc << "\n";
		double newConc = NewtonRhapsonStep( fitNumb, Data, conc, probNOut, Debug);
		double delta = conc - newConc;
		if ( Debug )
		{
			double tmp = fabs(delta)/conc;
			OString << "      newConc = " << newConc << ", ratio = " << tmp << "\n";
		}
		if ( fabs(delta)/conc < Epsilon )
		{
			Iter = i + 1;
			conc = newConc;
			break;
		}
		conc = newConc;
	}
	double cLow = conc * 0.33;
	double cHigh = conc * 3.0;
	std::vector< double > cList(3);
	std::vector< double > fList(3);
	cList[0] = -1.0;
	cList[1] = 1.0e+37;
	conc *= 0.97;
	double volEpsilon = totalVolume * Epsilon;
	double funcOut;
	int notifyIter = 20;
	Iter = 0;
	OString << "----- Most Likely Number Fit -----\n";
	for ( int i = 0; i < MaxIter ; i++)
	{
		OString << "+++++ Iter = " << i 
			<< ", Numb Droplets = " << numbDroplets
			<< ", Numb Occupied = " << fitNumb
			<< ", conc = " << conc << std::endl;

		double newConc = MLNStep( fitNumb, totalVolume, Data,
			conc, cLow, cHigh, volEpsilon, funcOut, probNOut, Debug);
		double delta = conc - newConc;
		Iter = i + 1;
		if ( fabs(delta)/conc < Epsilon )
		{
			conc = newConc;
			break;
		}
		if ( funcOut < 0.0 && newConc > cList[0] )
		{
			cList[0] = newConc;
			fList[0] = funcOut;
		}
		else if ( funcOut > 0.0 && newConc < cList[1] )
		{
			cList[1] = newConc;
			fList[1] = funcOut;
		}
		
		if ( Iter >= notifyIter )
		{
			OString << "Iter = " << Iter << ", old conc = " << conc
				<< ", new conc = " << newConc << "\n";

			if ( Iter % 5 == 0 && cList[0] > 0.0 && cList[1] < 9.0E+36 )
			{
				double tryC = newConc;
				newConc = MLNStep( fitNumb, totalVolume, Data,
					tryC, cLow, cHigh, volEpsilon, cList, fList, funcOut, probNOut, Debug);
				if ( funcOut < 0.0 && newConc > cList[0] )
				{
					cList[0] = newConc;
					fList[0] = funcOut;
				}
				else if ( funcOut > 0.0 && newConc < cList[1] )
				{
					cList[1] = newConc;
					fList[1] = funcOut;
				}
			}
		}
		conc = newConc;
	}
	return conc;
}
	
double NewtonRhapsonStep( double Numb, std::vector<std::pair< double, bool > > &Data, double Concentration0, double &ProbNOut, bool Debug )
{
	double probN = 0.0;
	double probNDeriv = 0.0;
	for ( int d = 0 ; d < (int)Data.size() ; d++ )
	{
		double tmp = exp(-Data[d].first * Concentration0);
		probN += (1.0 - tmp);
		probNDeriv += Data[d].first * tmp;
	}
	if ( Debug )
	{
		std::cout << "Inside NR: probN = " 
			<< probN << ", probNDeriv = " << probNDeriv << ", Numb = " << Numb << std::endl;
	}
	ProbNOut = probN;
	return Concentration0 - ( probN - Numb )/probNDeriv;
}	

double MLNStep( double Numb, double VTotal, std::vector<std::pair< double, bool > > &Data,
	double &Conc, double &CLow, double &CHigh, double Epsilon, double &FuncOut, double &ProbNOut, bool Debug)
{
	double probN = 0.0;
	double fsum = 0.0;
	double fpsum = 0.0;
	double tmp1 = 0.0;
	double tmp2 = 0.0;
	double tmp3 = 0.0;
	bool somethingIsNan = false;
	for ( int d = 0 ; d < (int)Data.size() ; d++ )
	{
		tmp1 = exp(-Data[d].first*Conc);
		probN += (1.0 - tmp1);
		if ( Data[d].second )
		{
			tmp2 = (Data[d].first)/(1.0-tmp1);
			tmp3 = tmp2*tmp2*tmp1;
			if (isnan(tmp2) || isnan(tmp3))
			{
				somethingIsNan = true;
				tmp2 = 0.0;
				tmp3 = 0.0;
			}
			else
			{
				fsum += tmp2;
				fpsum += tmp3;
			}
		}
	}
	FuncOut = VTotal - fsum; 
	if ( Debug )
	{
		std::cout << "Inside MLN(1): probN = " << probN 
			<< ", Numb = " << Numb 
			<< ", VTotal = " << VTotal 
			<< ", FuncOut = " << FuncOut << std::endl;
		std::cout << "     "
			<< "Conc = " << Conc
			<< ", CLow = " << CLow
			<< ", CHigh = " << CHigh 
			<< ", Epsilon= " << Epsilon << std::endl;
		std::cout << "     "
			<< ", fsum = " << fsum 
			<< ", fpsum = " << fpsum 
			<< ", tmp1 = " << tmp1 
			<< ", tmp2 = " << tmp2 
			<< ", tmp3 = " << tmp3 << std::endl;
	}
	ProbNOut = probN;
	if (isnan(FuncOut / fpsum))
	{
		return Conc;
	}
	double cNew = Conc - (FuncOut/fpsum);
	
	if ( cNew < CLow )
	{
		cNew = 0.1 * Conc + 0.9 * CLow;
	}
	else if ( cNew > CHigh )
	{
		cNew = 0.1 * Conc + 0.9 * CHigh ;
	}
	else
		return cNew;

	fsum = 0.0;
	probN = 0.0;
	for ( int d = 0 ; d < (int)Data.size() ; d++ )
	{
		tmp1 = exp(-Data[d].first*cNew);
		probN += (1.0 - tmp1);
		if ( Data[d].second )
		{
			tmp2 = (Data[d].first)/(1.0-tmp1);
			fsum += tmp2;
		}
	}
	FuncOut = VTotal - fsum; 
	if ( Debug )
	{
		std::cout << "     "
			<< "  fsum = " << fsum 
			<< ", tmp1 = " << tmp1 
			<< ", tmp2 = " << tmp2 
			<< ", FuncOut = " << FuncOut << std::endl;
	}

	ProbNOut = probN;
	return cNew;
}

double MLNStep( double Numb, double VTotal, std::vector<std::pair< double, bool > > &Data,
	double &Conc, double &CLow, double &CHigh, double Epsilon, 
	std::vector<double>CList, std::vector<double>FList, double &FuncOut, double &ProbNOut, bool Debug)
{
	ProbNOut = 0.0;
	double fsum = 0.0;
	double tmp1;
	double tmp2;
	double cNew;
	std::vector<double> coeff(3);

	for ( int d = 0 ; d < (int)Data.size() ; d++ )
	{
		if ( Data[d].second )
		{
			tmp1 = exp(-Data[d].first*Conc);
			tmp2 = (Data[d].first)/(1.0-tmp1);
			fsum += tmp2;
		}
	}
	if ( Debug )
	{
		std::cout << "Inside MLN(2):"
			<< "  Numb = " << Numb 
			<< ", VTotal = " << VTotal  << std::endl;
		std::cout << "     "
			<< "Conc = " << Conc
			<< ", CLow = " << CLow
			<< ", CHigh = " << CHigh 
			<< ", Epsilon= " << Epsilon << std::endl;
		std::cout << "     "
			<< "  fsum = " << fsum 
			<< ", tmp1 = " << tmp1 
			<< ", tmp2 = " << tmp2 << std::endl;
	}

	CList[2] = Conc;
	FList[2] = VTotal - fsum; 

	coeff[0] = (CList[1] * CList[2] * FList[2] - FList[1] * CList[1] - FList[1] * CList[2] + FList[0]) 
		/ (CList[0] * CList[0] - CList[0] * CList[1] - CList[0] * CList[2] + CList[1] * CList[2]);

	coeff[1] = -(CList[0] * CList[2] * FList[2] - FList[1] * CList[0] - FList[1] * CList[2] + FList[0]) 
		/ (CList[0] * CList[1] - CList[0] * CList[2] - CList[1] * CList[1] + CList[1] * CList[2]);

	coeff[2] = (CList[0] * CList[1] * FList[2] - FList[1] * CList[0] - FList[1] * CList[1] + FList[0]) 
		/ (CList[0] * CList[1] - CList[0] * CList[2] - CList[1] * CList[2] + CList[2] * CList[2]);

	if ( Debug )
	{
		std::cout << "     "
			<< "coeff[] = " << coeff[0] << ", " << coeff[1] << ", " << coeff[2] << std::endl;
		std::cout << "     "
			<< "CList[] = " << CList[0] << ", " << CList[1] << ", " << CList[2] << std::endl;
		std::cout << "     "
			<< "FList[] = " << FList[0] << ", " << FList[1] << ", " << FList[2] << std::endl;
	}
	double descr = coeff[1] * coeff[1] - 4.0 * coeff[0] * coeff[2];
	if ( descr > 0.0 )
		cNew = ( -coeff[1] + sqrt(descr) )/(2.0 * coeff[0] );
	if ( cNew < CLow || cNew > CHigh )
	{
		if ( FList[2] < 0.0 )
			CLow = Conc;
		else if ( FList[2] > 0.0 )
			CHigh = Conc;
		cNew = 0.5 * ( CLow + CHigh );
	}
	for ( int d = 0 ; d < (int)Data.size() ; d++ )
	{
		tmp1 = exp(-Data[d].first*cNew);
		ProbNOut += (1.0 - tmp1);
		if ( Data[d].second )
		{
			tmp2 = (Data[d].first)/(1.0-tmp1);
			fsum += tmp2;
		}
	}
	FuncOut = VTotal - fsum;
	if ( Debug )
	{
		std::cout << "     "
			<< "descr = " << descr << ", FuncOut = " << FuncOut << ", cNew = " << cNew << std::endl;
	}
	return cNew;
}
